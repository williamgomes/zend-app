<?php
class Paymentgateway_View_Helper_Price extends Zend_View_Helper_Abstract 
{
	public function price($price, $global_conf = null, $margine = '1') 
	{
		return Settings_Service_Price::price($price, $global_conf, $margine);
	}	
}