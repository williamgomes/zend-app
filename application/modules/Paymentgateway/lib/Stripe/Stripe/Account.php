<?php

class Paymentgateway_Controller_Helper_Stripe_Stripe_Account extends Paymentgateway_Controller_Helper_Stripe_Stripe_SingletonApiResource
{
  /**
    * @param string|null $apiKey
    *
    * @return Stripe_Account
    */
  public static function retrieve($apiKey=null)
  {
    $class = get_class();
    return self::_scopedSingletonRetrieve($class, $apiKey);
  }
}
