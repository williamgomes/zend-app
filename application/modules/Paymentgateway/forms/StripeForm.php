<?php
class Paymentgateway_Form_StripeForm  extends Zend_Form
{
	protected $_option;
	protected $_editor;

	public function __construct($options = null)
	{
		$translator = Zend_Registry::get('translator');
		$this->_option	= $options;
		$config = (file_exists( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.StripeForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.StripeForm.ini', 'stripe') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/en_US.StripeForm.ini', 'stripe');
		parent::__construct($config->stripe );
	}


	public function init()
	{
		 $this->createForm();
	}

	public function createForm ()
	{
	 $this->elementDecorator();
	 $this->loadSelectZeroOptions ();
	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
	}


	private function loadSelectZeroOptions ()
	{
		$translator = Zend_Registry::get('translator');

	}
}
?>