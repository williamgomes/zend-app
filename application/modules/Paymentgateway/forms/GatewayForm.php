<?php
class Paymentgateway_Form_GatewayForm  extends Zend_Form
{
	protected $_option;
	protected $_editor;

	public function __construct($options = null)
	{

		$this->_option	= $options;
	}

	public function loadForm ($gatewayName){
	    $translator = Zend_Registry::get('translator');
	    $config = (file_exists( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile(). $gatewayName.'.Form.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().$gatewayName.'.Form.ini', 'gateway') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/en_US.' .$gatewayName. 'Form.ini', 'gateway');
	    parent::__construct($config->gateway );
	}

	public function init()
	{
		 $this->createForm();
	}

	public function createForm ()
	{
	 $this->elementDecorator();
	 $this->loadSelectZeroOptions ();
	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
	}

	public function setEditor($baseURL)
	{
		$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
		$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
		$this->_editor .= '<script type="text/javascript">
							tinyMCE.init({
							// General options
							mode : "none",
							theme : "advanced",
							plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
							skin : "o2k7",
							skin_variant : "default",
							// Theme options

							theme_advanced_buttons1 : "insertimage,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect",
							theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",
							theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl",
							theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor,fontsizeselect,|,fullscreen",
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "bottom",


							extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type],style[type],div[class]",
							convert_fonts_to_spans :  false,
							font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
							theme_advanced_default_font : "[arial|30] ",
							theme_advanced_resizing : true,

							forced_root_block : false,
							force_br_newlines : true,
							force_p_newlines : false,
							relative_urls : false,

							relative_urls : true,
							remove_script_host : true,

							document_base_url : "'.$baseURL.'/",

							// Example content CSS (should be your site CSS)
							content_css : "css/content.css",

							// Drop lists for link/image/media/template dialogs
							template_external_list_url : "lists/template_list.js",
							external_link_list_url : "lists/link_list.js",
							external_image_list_url : "lists/image_list.js",
							media_external_list_url : "lists/media_list.js",

							// Replace values for the template plugin
							template_replace_values : {
								username : "Some User",
								staffid : "991234"
							},
							template_popup_width : "500",
							template_popup_height : "400",
							template_templates : [
								{
									title : "Newsletter Template",
									src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
									description : "Adds Editors Name and Staff ID"
								}
							]

						});
						function loadTinyMCE(id)
						{
							tinyMCE.execCommand(\'mceAddControl\', false, id);
							document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
							$this->_editor .= "\''+id+'\'";
							$this->_editor .= ');"><img src="data/adminImages/commonImages/wysiwyg_close.png" width="32" height="32" border="0" title="View Normal Editor" alt="View Normal Editor" /></a>\';
						}
						function unloadTinyMCE(id)
						{
							tinyMCE.execCommand(\'mceRemoveControl\', false, id);
							document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
							$this->_editor .= "\''+id+'\'";
							$this->_editor .= ');"><img src="data/adminImages/commonImages/wysiwyg_open.png" width="32" height="32" border="0" title="Open Wysiwyg Editor" alt="Open Wysiwyg Editor" /></a>\';
						}
					</script>';
	}

	public function getEditor()
	{
		echo $this->_editor;
	}

	private function loadSelectZeroOptions ()
	{
		$translator = Zend_Registry::get('translator');
		//$this->table_id->addMultiOptions('', $translator->translator('common_delete'));
		//$this->table_id->addMultiOptions('0', $translator->translator('comment_select_page'));
	}
}
?>