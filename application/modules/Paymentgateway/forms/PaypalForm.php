<?php
class Paymentgateway_Form_PaypalForm  extends Zend_Form
{
	protected $_option;
	protected $_editor;

	public function __construct($options = null)
	{
		$translator = Zend_Registry::get('translator');
		$this->_option	= $options;
		$config = (file_exists( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.PaypalForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.PaypalForm.ini', 'paypal') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/en_US.PaypalForm.ini', 'paypal');
		parent::__construct($config->paypal );
	}




	public function init()
	{
		 $this->createForm();
	}

	public function createForm ()
	{
	 $this->elementDecorator();
	 $this->loadSelectZeroOptions ();
	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
	}


	private function loadSelectZeroOptions ()
	{
		$translator = Zend_Registry::get('translator');

	}
}
?>