<?php
class Paymentgateway_Form_GatewayuploadForm  extends Zend_Form 
{
		protected $_option;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
			$this->_option	= $options;	
            $config = (file_exists( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.UploadForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.UploadForm.ini', 'upload') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/en_US.UploadForm.ini', 'upload');
            parent::__construct($config->upload );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
		 $this->elementDecorator();		
        }		
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements'	
				));
			$this->upload_file->setDecorators(array(
					'file','file'		
				));
			//$this->upload_file->setMultiFile(3);
			$this->upload_file->setMaxFileSize('100000');			
		} 
}
?>