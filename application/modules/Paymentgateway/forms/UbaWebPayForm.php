<?php
class Paymentgateway_Form_UbaWebPayForm  extends Zend_Form
{
	protected $_option;
	protected $_editor;

	public function __construct($options = null)
	{

		$translator = Zend_Registry::get('translator');
		$this->_option	= $options;
		$config = (file_exists( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.UbaWebPayForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/'.$translator->getLangFile().'.UbaWebPayForm.ini', 'webpay') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Paymentgateway/forms/source/en_US.UbaWebPayForm.ini', 'webpay');
		parent::__construct($config->webpay );
	}


	public function init()
	{
		 $this->createForm();
	}

	public function createForm ()
	{
	 $this->elementDecorator();
	 $this->loadSelectZeroOptions ();
	 $this->loadCurrency();
	}


	public function loadCurrency ()
	{

	    $this->currCode->addMultiOption('','Select Currency Code');
	    $this->currCode->addMultiOption('840','Dollar');
	    $this->currCode->addMultiOption('826','Pound');
	    $this->currCode->addMultiOption('566','Naira');


	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
	}


	private function loadSelectZeroOptions ()
	{
		$translator = Zend_Registry::get('translator');

	}
}
?>