// Created by Larry Ullman, www.larryullman.com, @LarryUllman
// Posted as part of the series "Processing Payments with Stripe"
// http://www.larryullman.com/series/processing-payments-with-stripe/
// Last updated February 20, 2013

// This page is intended to be stored in a public "js" directory.

// This function is just used to display error messages on the page.
// Assumes there's an element with an ID of "payment-errors".
function reportError(msg) {
    // Show the error in the form:
    $('#payment-errors').text(msg).addClass('alert alert-error');
    // re-enable the submit button:
    $('#submitBtn').prop('disabled', false);
    return false;
}
function reportSuccess(msg) {
    // Show the error in the form:
    $('#payment-success').text(msg).addClass('alert alert-error');
    // re-enable the submit button:
    $('#submitBtn').prop('disabled', false);
    return false;
}

// Assumes jQuery is loaded!
// Watch for the document to be ready:

    // Watch for a form submission:
    $("#submitBtn").on('click', function(e) {

        // Flag variable:
        var error = false;
        
        $( "#span-loader" ).html( "<img style='margin: 10px 0px; float: left !important; clear:both !important;' src='data/frontImages/gateway/loading_squares.gif' id='loader_img'/>" );
        // disable the submit button to prevent repeated clicks:
        $('#submitBtn').attr("disabled", "disabled");

        // Get the values:
        var ccNum = $('.card-number').val(), cvcNum = $('.card-cvc').val(), expMonth = $('.card-expiry-month').val(), expYear = $('.card-expiry-year').val();

        // Validate the number:
        if (!Stripe.validateCardNumber(ccNum) && !error) {
            error = true;
            reportError(msgBack("payment_stripe_card_err"));
            $( "#span-loader" ).empty();
        }
        // Validate the CVC:
        else if (!Stripe.validateCVC(cvcNum)) {
            error = true;
            reportError(msgBack("payment_stripe_cvc_err"));
            $( "#span-loader" ).empty();
        }
        // Validate the expiration:
        else if (!Stripe.validateExpiry(expMonth, expYear)) {
            error = true;
            reportError(msgBack("payment_stripe_expire_date_err"));
            $( "#span-loader" ).empty();
        }
        // Validate other form elements, if needed!

        // Check for errors:
        if (!error) {

            // Get the Stripe token:
            Stripe.createToken({
                number: ccNum,
                cvc: cvcNum,
                exp_month: expMonth,
                exp_year: expYear
            }, stripeResponseHandler);

        }

        // Prevent the form from submitting:
        return false;

    }); // Form submission


// Function handles the Stripe response:
function stripeResponseHandler(status, response) {
//    alert("here");

    // Check for an error:
    if (response.error) {
        reportError(response.error.message);
        $( "#span-loader" ).empty();
    } else { // No errors, submit the form:

        var f = $("#payment-form");

        // Token contains id, last4, and card type:
        var token = response['id'];

        // Insert the token into the form so it gets submitted to the server
        f.append("<input type='hidden' id='stripeToken' value='" + token + "' />");
        

        


        if(token != "" || token != null){
            var data = new Object();
            data.ccNum = $('.card-number').val();
            data.cvcNum = $('.card-cvc').val();
            data.expMonth = $('.card-expiry-month').val();
            data.expYear = $('.card-expiry-year').val();
            data.stripeToken = $('#stripeToken').val();
            data.custEmail = $('#custEmail').val();
            data.payAmount = $('#payAmount').val();
            var url = "Paymentgateway/stripe/stripeajax";
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                success: function(response) {
                    //alert(response);
                    try{
                        var json_arr = eval("(" + response + ")");
                        if (json_arr.status == 'ok'){
                            $('#payment-errors').empty();
                            reportSuccess(json_arr.msg);
//                            $( "span.invoice-satus" ).replaceWith( "<span class='invoice-satus paid' style='border: 1px solid #93DD88 !important;background-color: #D5FFCE !important;color: #009900 !important;padding:5px 20px !important;'>Paid</span>" );
                            $("#payment-form").remove();                            
//                            $(".invoice-satus").removeClass("unpaid");
//                            $(".invoice-satus").addClass("paid");
                        } else {
                            reportError(json_arr.msg);
                            $( "#span-loader" ).empty();
                        }
                    } catch (error){
                        var error_arr = [error, response];
                        reportError(error_arr);
                        $( "#span-loader" ).empty();
                    }
                },
                error: function(xhr, status, error) {
                    var msg = "Error! " + xhr.status + " " + error;
                    reportError(msg);
                }
            });
        }
    }

} // End of stripeResponseHandler() function.