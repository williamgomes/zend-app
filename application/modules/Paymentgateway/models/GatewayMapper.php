<?php
class Paymentgateway_Model_GatewayMapper
{
	protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Paymentgateway_Model_DbTable_Gateway');
        }
        return $this->_dbTable;
    }

	public function fetchAllGateway($pageNumber)
    {
        $resultSet = $this->getDbTable()->getAllGateway();
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
	
	public function fetchAll ($pageNumber, $approve = null, $search_params = null,  $tableColumns = array())
	{
	    $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
	    $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
	    $paginator = Zend_Paginator::factory($resultSet);
	    $paginator->setItemCountPerPage($viewPageNum);
	    $paginator->setCurrentPageNumber($pageNumber);
	    return $paginator;
	}

	public function saveGateway(Paymentgateway_Model_Gateway $obj)
	{
		if ((null === ($id = $obj->getId())) || empty($id))
		{
			unset($data['id']);

			$data = array(
				'name'					=> $obj->getName(),
				'logo' 					=> $obj->getLogo(),
				'return_url'			=> $obj->getPayment_order(),
				'email'					=> $obj->getEmail(),
				'lc_locale' 			=> $obj->getData_transfer_token(),
				'curency' 				=> $obj->getCurency(),
				'test_mode' 			=> $obj->getTest_mode(),
				'active' 				=> $obj->getActive(),
				'vendor_number' 		=> $obj->getVendor_number(),
				'secret_phrase' 		=> $obj->getSecret_phrase(),
				'entry_by' 				=> $obj->getEntry_by(),
				'entry_date' 			=> $obj->getEntry_date()
			);
			try
			{
				$last_id = $this->getDbTable()->insert($data);
				$result = array('status' => 'ok' ,'id' => $last_id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' , 'msg' => $e->getMessage());
			}

		}
		else
		{
			$data = array(
				'name'					=> $obj->getName(),
				'logo' 					=> $obj->getLogo(),
				'return_url'			=> $obj->getPayment_order(),
				'email'					=> $obj->getEmail(),
				'lc_locale' 	=> $obj->getData_transfer_token(),
				'curency' 				=> $obj->getCurency(),
				'test_mode' 			=> $obj->getTest_mode(),
				'active' 				=> $obj->getActive(),
				'vendor_number' 		=> $obj->getVendor_number(),
				'secret_phrase' 		=> $obj->getSecret_phrase(),
				'entry_by' 				=> $obj->getEntry_by(),
				'entry_date' 			=> $obj->getEntry_date()
			);
			// Start the Update process
			try
			{
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' , 'msg' => $e->getMessage());
			}
		}
		return $result;
	}
}
?>