<?php
/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 ******************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 ******************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 ******************************************************************************
 */


class Paymentgateway_Model_GatewaySettings
{
    protected $_id;
    protected $_gateway_id;
    protected $_setting;
    protected $_value;
    protected $_user_id;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value)
		{
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods))
			{
				$this->$method($value);
			}
		}
		return $this;
	}

	public function saveGateway()
	{
		$mapper  = new Paymentgateway_Model_GatewaySettingsMapper();
		$result = $mapper->save($this);
		return $result;
	}



	public function getEntry_by()
	{
		if(empty($this->_entry_by))
		{
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
		}
		return $this->_entry_by;
	}

	public function getEntry_date()
	{
		if(empty($this->_entry_date))
		{
			$this->_entry_date = date('Y-m-d h:i:s');
		}
		return $this->_entry_date;
	}
	/**
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }

	/**
     * @return the $_gateway_id
     */
    public function getGateway_id ()
    {
        return $this->_gateway_id;
    }

	/**
     * @return the $_setting
     */
    public function getSetting ()
    {
        return $this->_setting;
    }

	/**
     * @return the $_value
     */
    public function getValue ()
    {
        return $this->_value;
    }

	/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }

	/**
     * @param field_type $_gateway_id
     */
    public function setGateway_id ($_gateway_id)
    {
        $this->_gateway_id = $_gateway_id;
    }

	/**
     * @param field_type $_setting
     */
    public function setSetting ($_setting)
    {
        $this->_setting = $_setting;
    }

	/**
     * @param field_type $_value
     */
    public function setValue ($_value)
    {
        $this->_value = $_value;
    }
	/**
	 * @return the $_user_id
	 */
	public function getUser_id() {
		return $this->_user_id;
	}

	/**
	 * @param field_type $_user_id
	 */
	public function setUser_id($_user_id) {
		$this->_user_id = $_user_id;
	}



}

?>