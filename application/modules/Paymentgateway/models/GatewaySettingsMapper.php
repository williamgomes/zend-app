<?php

/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 ******************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 ******************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 ******************************************************************************
 */



class Paymentgateway_Model_GatewaySettingsMapper
{
	protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Paymentgateway_Model_DbTable_GatewaySettings');
        }
        return $this->_dbTable;
    }

	public function fetchAllGateway($pageNumber)
    {
        $resultSet = $this->getDbTable()->getAllGateway();
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }


	public function save(Paymentgateway_Model_GatewaySettings $obj) {

		try
		{
			$where = array('setting = ?' => $obj->getSetting(),'user_id = ?' => $obj->getUser_id(),  'gateway_id = ?' => $obj->getGateway_id());
			$row = $this->getDbTable()->fetchRow($where);
			if(!$row)
		    {
		    	$data = array(
		    			'gateway_id' 			=> $obj->getGateway_id(),
		    			'setting'			    => $obj->getSetting(),
		    			'value'					=> $obj->getValue(),
		    			'user_id'				=> $obj->getUser_id()
		    	);

		    	$last_id = $this->getDbTable()->insert($data);
		    	$result = array('status' => 'ok' ,'id' => $last_id);
		    }
		    else {

		    	$data = array(
		    			'value'				=> $obj->getValue()
		    	);
		    	// Start the Update process

		    	$id = $this->getDbTable()->update($data,  array('setting = ?' => $obj->getSetting() ,  'gateway_id = ?' => $obj->getGateway_id(),  'user_id = ?' => $obj->getUser_id()));

		    	$result = array('status' => 'ok' ,'id' => $id);
		    }
		}
		catch (Exception $e)
		{
			$result = array('status' => 'err' , 'msg' => $e->getMessage());
		}

		return $result;
	}

}
?>