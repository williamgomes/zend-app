<?php
class Paymentgateway_Model_Currency
{
	protected $_id;
	protected $_base_unit;
	protected $_base_currency;
	protected $_target_unit;
	protected $_target_currency;
	protected $_target_currency_code;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value)
		{
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods))
			{
				$this->$method($value);
			}
		}
		return $this;
	}

	public function save()
	{
		$mapper  = new Paymentgateway_Model_CurrencyMapper();
		$return = $mapper->save($this);
		return $return;
	}
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @return the $_base_unit
	 */
	public function getBase_unit() {
		return $this->_base_unit;
	}

	/**
	 * @param field_type $_base_unit
	 */
	public function setBase_unit($_base_unit) {
		$this->_base_unit = $_base_unit;
	}

	/**
	 * @return the $_base_currency
	 */
	public function getBase_currency() {
		return $this->_base_currency;
	}

	/**
	 * @param field_type $_base_currency
	 */
	public function setBase_currency($_base_currency) {
		$this->_base_currency = $_base_currency;
	}

	/**
	 * @return the $_target_unit
	 */
	public function getTarget_unit() {
		return $this->_target_unit;
	}

	/**
	 * @param field_type $_target_unit
	 */
	public function setTarget_unit($_target_unit) {
		$this->_target_unit = $_target_unit;
	}

	/**
	 * @return the $_target_currency
	 */
	public function getTarget_currency() {
		return $this->_target_currency;
	}

	/**
	 * @param field_type $_target_currency
	 */
	public function setTarget_currency($_target_currency) {
		$this->_target_currency = $_target_currency;
	}
	/**
	 * @return the $_target_currency_code
	 */
	public function getTarget_currency_code() {		
		return $this->_target_currency_code;
	}

	/**
	 * @param field_type $_target_currency_code
	 */
	public function setTarget_currency_code($_target_currency_code) {
		$this->_target_currency_code = $_target_currency_code;
	}



}
?>