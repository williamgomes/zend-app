<?php
class Paymentgateway_Model_CurrencyMapper
{
	protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Paymentgateway_Model_DbTable_Currency');
        }
        return $this->_dbTable;
    }


	public function fetchAll ($pageNumber, $approve = null, $search_params = null,  $tableColumns = array())
	{
	    $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
	    $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
	    $paginator = Zend_Paginator::factory($resultSet);
	    $paginator->setItemCountPerPage($viewPageNum);
	    $paginator->setCurrentPageNumber($pageNumber);
	    return $paginator;
	}

	public function save(Paymentgateway_Model_Currency $obj)
	{
		$data = array(
				'base_unit'					=> $obj->getBase_unit() ,
				'base_currency' 			=> $obj->getBase_currency(),
				'target_unit'				=> $obj->getTarget_unit(),
				'target_currency'			=> $obj->getTarget_currency(),
				'target_currency_code'		=> $obj->getTarget_currency_code()
		);

		if ((null === ($id = $obj->getId())) || empty($id))
		{
			unset($data['id']);
			// Start the Insertion process
			try
			{
				$last_id = $this->getDbTable()->insert($data);
				$result = array('status' => 'ok' ,'id' => $last_id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' , 'msg' => $e->getMessage());
			}

		}
		else
		{
			// Start the Update process
			try
			{
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' , 'msg' => $e->getMessage());
			}
		}
		return $result;
	}

	public function delete ($id){
		$result = null;
		try {
			$where = array('id = ?' => $id);
			$this->getDbTable()->delete($where);
			$result = array('status' => 'ok' ,'id' => $id);
		} catch (Exception $e) {
			$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
		}
		return $result;
	}


}
?>