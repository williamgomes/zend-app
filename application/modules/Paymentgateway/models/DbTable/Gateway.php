<?php
/**
* This is the DbTable class for the comment table.
*/
class Paymentgateway_Model_DbTable_Gateway extends Eicra_Abstract_DbTable {
	/**
	 * Table name
	 */
	protected $_name = 'payment_gateway';
    protected $_cols = null;

	// Get Datas
	public function getInfo($id) {
		try {
			$row = $this->fetchRow ( 'id = ' . $id );
			$data = $row->toArray ();
			$data = is_array ( $data ) ? array_map ( 'stripslashes', $data ) : stripslashes ( $data );
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}
	public function getGatewayNameById($id) {
		try {
			$row = $this->fetchRow ( 'id = ' . $id );
			$data = $row->toArray ();
			return $data ['name'];
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}
	public function getDefaultGateway() {
		try {
			$row = $this->fetchRow ( "active = '1'" );
			$data = is_array ( $data ) ? array_map ( 'stripslashes', $data ) : stripslashes ( $data );
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}

	// Get All Active Polls
	public function getAllGateway($order = 'DESC') {
		$auth = Zend_Auth::getInstance();
		$user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'pg' => $this->_name
		), array(
                'id',
                'name',
                'title',
				'logo',
				'payment_order',
				'method'
        ))

        ->joinLeft ( array (
        		'gm' => Zend_Registry::get ( 'dbPrefix' ) . 'payment_gateway_mapper'
        ), 'pg.id = gm.gateway_id and gm.user_id =' .$user_id , array (
        		'active' => 'gm.active'
        ) )

		->order ( "pg.payment_order " . $order );

		$options = $this->fetchAll ( $select );
		if (! $options) {
			// throw new Exception("Count not find row Category");
			$options = null;
		}
		return $options;
	}
	/* public function getActiveGateway($order = 'DESC', $user_id) {
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'pg' => $this->_name
		), array (
				'id'
		) )->joinLeft ( array (
				'gm' => Zend_Registry::get ( 'dbPrefix' ) . 'payment_gateway_mapper'
		), 'pg.id = gm.	gateway_id', array (
				'active' => 'gm.active'
		) )->where ( 'gm.user_id	= ?', $user_id )->order ( "pg.payment_order " . $order );

		$options = $this->fetchAll ( $select )->toArray ();
		if (! $options) {
			$options = null;
		}
		return $options;
	} */
	public function getAllActiveGateway($order = 'DESC', $user_id = null) {
		$global_conf = Zend_Registry::get('global_conf');
		$user_id = (!empty($user_id)) ? $user_id : $global_conf['payment_user_id'];
		$select = $this->select ()->setIntegrityCheck ( false )->from ( array (
				'pg' => $this->_name
		), array (
				'id' => 'pg.id',
				'gwname' => 'pg.name',
				'logo' => 'pg.logo',
				'payment_order' => 'pg.payment_order'
		) )->order ( "pg.payment_order " . $order )
		->joinLeft ( array ( 'pgs' => Zend_Registry::get ( 'dbPrefix' ) . 'payment_gateway_settings' ), 'pgs.gateway_id = pg.id', array ( 'name' => 'pgs.value' ))
		->where ( 'pgs.setting	= ?', 'name' )
		->joinLeft ( array ( 'pgm' => Zend_Registry::get ( 'dbPrefix' ) . 'payment_gateway_mapper' ), 'pgm.gateway_id = pg.id', array (  'pgm.added_on', 'pgm.active' ))
		->where ( 'pgm.user_id	= pgs.user_id')
		->where ( 'pgm.user_id	= ?',  $user_id)
		->where ( 'pgm.active	= ?',  '1');
		
		$options = $this->fetchAll ( $select );
		if (! $options) {
			// throw new Exception("Count not find row Category");
			$options = null;
		}
		return $options;
	}
	
	public function disableAll() {
		try {
			$data = array (
					'active' => '0'
			);
			$this->update ( $data );

			$result = array (
					'status' => 'ok'
			);
		} catch ( Exception $e ) {
			$result = array (
					'status' => 'err',
					'msg' => $e->getMessage ()
			);
		}
	}

	// Activate guest
	public function setDefault($id) {
		if (! empty ( $id )) {
			try {
				$data = array (
						'active' => '0'
				);
				$where = $this->getAdapter ()->quoteInto ( 'active = ?', '1' );
				$this->update ( $data, $where );

				$data = array (
						'active' => '1'
				);
				$where = $this->getAdapter ()->quoteInto ( 'id = ?', $id );
				$this->update ( $data, $where );

				$result = array (
						'status' => 'ok'
				);
			} catch ( Exception $e ) {
				$result = array (
						'status' => 'err',
						'msg' => $e->getMessage ()
				);
			}
		} else {
			$translator = Zend_Registry::get ( 'translator' );
			$result = array (
					'status' => 'err',
					'msg' => $translator->translator ( 'common_id_not_found' )
			);
		}
		return $result;
	}
	
	public function getListInfo ($approve = null, $search_params = null, $tableColumns= null)
    {
        $auth = Zend_Auth::getInstance();        
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

		$payment_gateway_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['payment_gateway'] && is_array($tableColumns['payment_gateway'])) ? $tableColumns['payment_gateway'] : array('id', 'name', 'title', 'logo', 'payment_order', 'method');
        $payment_gateway_mapper_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['payment_gateway_mapper'] && is_array($tableColumns['user_profile'])) ? $tableColumns['payment_gateway_mapper'] : array( 'active' => 'pgm.active' );
        
		$b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

		$select = $this->select()
        				->setIntegrityCheck(false)
        				->from(array( 'pg' => $this->_name ), $payment_gateway_column_arr) ;


        if (($payment_gateway_mapper_column_arr &&  is_array($payment_gateway_mapper_column_arr) && count($payment_gateway_mapper_column_arr) > 0)) {
            $select->joinLeft(array(
                'pgm' => Zend_Registry::get('dbPrefix') . 'payment_gateway_mapper'
            ), 'pg.id = pgm.gateway_id and pgm.user_id =' .$user_id, $payment_gateway_mapper_column_arr);
        }
        
        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("pg.payment_order ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {

                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params); 
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("pg.payment_order ASC");
        }

		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}
				
        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }		
        return $options;
    }
	
	public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'pg.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'active':
                $operatorFirstPart = " pgm.active ";
                break;          
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(buy.invoice_create_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(buy.invoice_create_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(buy.invoice_create_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
			case 'hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'deal_hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' <= "' . $operator_arr['value'] . '" AND TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' > "0" ';
                break;
			case 'deal_day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' <= "' . $operator_arr['value'] . '" AND DATEDIFF(' . $operator_arr['field'] . ', now())' . ' > "0" ';
                break;
			case 'deal_month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' > "0" ';
                break;
			case 'deal_year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' > "0" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }
	
    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}
?>