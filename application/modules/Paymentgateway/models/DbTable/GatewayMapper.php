<?php
/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 ******************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 ******************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 ******************************************************************************
 */
class Paymentgateway_Model_DbTable_GatewayMapper extends Eicra_Abstract_DbTable

{
	protected $_name = 'payment_gateway_mapper';
	public function getInfo($id) {
		try {
			$select = $this->select ()->from ( array (
					'pw' => $this->_name
			), array (
					'*'
			) )->where ( 'pw.gateway_id = ?', $id );

			$rowSet = $this->fetchAll ( $select );
			$data = $rowSet->toArray ();
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}
	public function getIdIfExists($key, $gatewayId) {
		try {

			$select = $this->select ()->from ( $this->_name, 'id' )->where ( 'gateway_id = ?', $gatewayId )->where ( 'setting = ?', $key );
			$data = $this->fetchRow ( $select );
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}
	public function getInfoByUserID($user_id) {
		try {
			$select = $this->select ()->from ( array (
					'gp' => $this->_name
			), array (
					'*'
			) )->where ( 'gp.user_id = ?', $user_id );

			$rowSet = $this->fetchAll ( $select )->toArray ();
			$data = $rowSet->toArray ();
		} catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$data = $e->getMessage ();
		}
		return $data;
	}

	public function setStatus($gateway_id, $active) {
		$auth = Zend_Auth::getInstance ();
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity ()->user_id : '';
		$option = null;
		$data = array (
				'gateway_id' => $gateway_id,
				'user_id' => $user_id,
				'active' => $active
		);

		try {

			$where = array (
					'user_id = ?' => $user_id,
					'gateway_id = ?' => $gateway_id
			);
			$row = $this->fetchRow($where);

			if ($row) {

				$option = $this->update($data, $where);
			}

			else {
				$option = $this->insert($data);
			}
			$result = array('status' => 'ok' ,'id' => $option,'active' => $active);
		}

		catch ( Exception $e ) {
			// throw new Exception("Count not find row $id");
			$result = array('status' => 'err' ,'msg' => $e->getMessage (), 'active' => $active);
		}

		return $result;
	}
}

?>