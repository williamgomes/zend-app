<?php
/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 ******************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 ******************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 ******************************************************************************
 */
class Paymentgateway_Model_DbTable_GatewaySettings extends Eicra_Abstract_DbTable

{

    protected $_name    =  'payment_gateway_settings';



    public function getInfo($id , $user_id = null)
    {
        try
        {
            $select = $this->select()
                        ->from(array('pw' => $this->_name),array('*'))
                        ->where('pw.gateway_id = ?', $id);

			if ($user_id){
				$select->where('pw.user_id = ?', $user_id);
			}
            $rowSet = $this->fetchAll($select);
            $data = $rowSet->toArray();


        }
        catch(Exception $e)
        {
            // throw new Exception("Count not find row $id");
            $data = $e->getMessage();
        }
        return $data;
    }


    public function getIdIfExists($key , $gatewayId)
    {
        try
        {


            $select  = $this->select()
                                ->from($this->_name, 'id')
                                ->where('gateway_id = ?',$gatewayId)
                                ->where('setting = ?',$key);
            $data = $this->fetchRow($select ) ;

        }
        catch(Exception $e)
        {
            // throw new Exception("Count not find row $id");
            $data = $e->getMessage();
        }
         return $data;
    }

}



?>