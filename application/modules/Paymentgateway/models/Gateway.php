<?php
class Paymentgateway_Model_Gateway
{
	protected $_id;
	protected $_name;
	protected $_logo;
	protected $_return_url;
	protected $_email;
	protected $_lc_locale;
	protected $_curency;
	protected $_test_mode;
	protected $_active;
	protected $_vendor_number;
	protected $_secret_phrase;
	protected $_entry_by;
	protected $_entry_date;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}			
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) 
		{
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) 
			{
				$this->$method($value);
			}
		}		
		return $this;
	}
	
	public function saveGateway()
	{
		$mapper  = new Paymentgateway_Model_GatewayMapper();
		$return = $mapper->saveGateway($this);
		return $return;
	}
	
	public function setId($text)
	{
		$this->_id = $text;
		return $this;
	}
	
	public function setName($text)
	{
		$this->_name = $text;
		return $this;
	}
	
	public function setLogo($text)
	{
		$this->_logo = $text;
		return $this;
	}
	
	public function setPayment_order($text)
	{
		$this->_return_url = $text;
		return $this;
	}
	
	public function setEmail($text)
	{
		$this->_email = $text;
		return $this;
	}
	
	public function setData_transfer_token($text)
	{
		$this->_lc_locale = $text;
		return $this;
	}
	
	public function setCurency($text)
	{
		$this->_curency = $text;
		return $this;
	}
	
	public function setTest_mode($text)
	{
		$this->_test_mode = $text;
		return $this;
	}	
	
	public function setActive($text)
	{
		$this->_active = $text;
		if($this->_active == '1')
		{
			$payment_db = new Paymentgateway_Model_DbTable_Gateway();
			$payment_db->disableAll();
		}
		return $this;
	}
		
	public function setVendor_number($text)
	{
		$this->_vendor_number = $text;
		return $this;
	}
	
	public function setSecret_phrase($text)
	{
		$this->_secret_phrase = $text;
		return $this;
	}
	
	public function setEntry_by($text)
	{
		$this->_entry_by = $text;
		return $this;
	}
	
	public function setEntry_date($text)
	{
		$this->_entry_date = date('Y-m-d h:i:s',strtotime($text));
		return $this;
	}
	
	// Get values
	public function getId()
	{
		return $this->_id;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function getLogo()
	{
		return $this->_logo;
	}
	
	public function getPayment_order()
	{
		return $this->_return_url;
	}
	
	public function getEmail()
	{
		return $this->_email;
	}
	
	public function getData_transfer_token()
	{
		return $this->_lc_locale;
	}
	
	public function getCurency()
	{
		return $this->_curency;
	}
	
	public function getTest_mode()
	{
		return $this->_test_mode;
	}
		
	public function getActive()
	{
		return $this->_active;
	}
		
	public function getVendor_number()
	{
		return $this->_vendor_number;
	}
	
	public function getSecret_phrase()
	{
		return $this->_secret_phrase;
	}
	
	public function getEntry_by()
	{
		if(empty($this->_entry_by))
		{
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();				
				$this->_entry_by = $globalIdentity->user_id;				
			}
		}
		return $this->_entry_by;
	}
	
	public function getEntry_date()
	{
		if(empty($this->_entry_date))
		{
			$this->_entry_date = date('Y-m-d h:i:s');
		}
		return $this->_entry_date;
	}
}
?>