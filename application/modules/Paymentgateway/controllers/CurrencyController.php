<?php
class Paymentgateway_CurrencyController extends Zend_Controller_Action {
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	private $global_local;

	public function init() {
		/* Initialize action controller here */
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity () : '';
		$this->view->auth = $auth;
		// Initialize Cache
		$cache = new Eicra_View_Helper_Cache ();
		$this->_controllerCache = $cache->getCache ();
	}
	public function preDispatch() {
		$this->_helper->layout->setLayout ( 'layout' );
		$this->_helper->layout->setLayoutPath ( MODULE_PATH . '/Administrator/layouts/scripts' );
		$this->translator = Zend_Registry::get ( 'translator' );
		$this->view->assign ( 'translator', $this->translator );
		$this->view->setEscape ( 'stripslashes' );
		$getModule = $this->_request->getModuleName ();
		$this->view->assign ( 'getModule', $getModule );
		$getAction = $this->_request->getActionName ();
		$this->view->assign ( 'getAction', $getAction );
		$getController = $this->_request->getControllerName ();
		$this->view->assign ( 'getController', $getController );
		if ($getAction != 'uploadfile') {
			$url = Zend_Registry::get ( 'config' )->eicra->params->domain . $this->view->baseUrl () . '/Administrator/login';
			Eicra_Global_Variable::checkSession ( $this->_response, $url );
			/* Check Module License */
			$modules_license = new Administrator_Controller_Helper_ModuleLoader ();
			$modules_license->getModulesLicenseMsg ( $this->_request->getModuleName () );
		}

		$global_conf = Zend_Registry::get ( 'global_conf' );
		$this->global_local = (empty ( $global_conf ['default_locale'] )) ? 'en_US' : $global_conf ['default_locale'];
	}
	public function listAction() {
		$posted_data = $this->_request->getParams ();
		$this->view->assign ( 'posted_data', $posted_data );
		$json_arr = null;

		if ($this->view->allow ()) {
			$currency = new Zend_Currency ($this->global_local);
			$currency->setLocale ($this->global_local);

			$default_currency = $currency->getSymbol () . '  - ' . $currency->getName () . ' [' . $currency->getShortName () . ']';
			$this->view->assign ( 'default_currency', $default_currency );
			$this->view->assign ( 'default_local', $this->global_local);

			if ($this->_request->isPost ()) {

				try {
					$this->_helper->layout->disableLayout ();
					$this->_helper->viewRenderer->setNoRender ();
					$global_conf = Zend_Registry::get ( 'global_conf' );
					$user_id = (! empty ( $user_id )) ? $user_id : $global_conf ['default_locale'];

					// action body
					$pageNumber = ($this->_request->getPost ( 'page' )) ? $this->_request->getPost ( 'page' ) : $this->_request->getParam ( 'page' );
					// $approve = $this->getRequest()->getParam('approve');
					$create = $this->_request->getParam ( 'create' );

					$getViewPageNum = $this->_request->getParam ( 'pageSize' );
					$posted_data ['browser_url'] = $this->view->url ( array (
							'module' => $this->view->getModule,
							'controller' => $this->view->getController,
							'action' => $this->view->getAction,
							'create' => $create,
							'page' => ($pageNumber == '1' || empty ( $pageNumber )) ? null : $pageNumber
					), 'adminrout', true );

					$viewPageNumSes = Eicra_Global_Variable::getSession ()->viewPageNum;

					$viewPageNum = (! empty ( $getViewPageNum )) ? $getViewPageNum : $viewPageNumSes;
					Eicra_Global_Variable::getSession ()->viewPageNum = $viewPageNum;

					$encode_params = Zend_Json_Encoder::encode ( $posted_data );
					$encode_auth_obj = Zend_Json_Encoder::encode ( $this->_auth_obj );
					$uniq_id = md5 ( preg_replace ( '/[^a-zA-Z0-9_]/', '_', $this->view->url () . '_' . $encode_params . '_' . $encode_auth_obj ) );
					if (($view_datas = $this->_controllerCache->load ( $uniq_id )) === false) {
						$list_mapper = new Paymentgateway_Model_CurrencyMapper ();
						$list_datas = $list_mapper->fetchAll ( $pageNumber, null, $posted_data );
						$view_datas = array (
								'data_result' => array (),
								'total' => 0
						);

						if ($list_datas) {
							$key = 0;
							foreach ( $list_datas as $entry ) {
								$entry_arr = $entry->toArray ();
								$entry_arr = is_array ( $entry_arr ) ? array_map ( 'stripslashes', $entry_arr ) : stripslashes ( $entry_arr );
								$entry_arr ['target_currency_field'] = array (
										'target_currency_id' => $entry_arr ['target_currency'],
										'target_currency_name' => $entry_arr ['target_currency']
								);
								$entry_arr ['base_unit'] = '1';

								if($this->global_local == $entry_arr['base_currency']){
									$entry_arr ['mismatch'] = false;
									$entry_arr ['equals_field'] = '=';
								}
								else {
									$entry_arr ['mismatch'] = true;
									$entry_arr ['equals_field'] = '<span class=\"invoice-satus unpaid\">'.$this->translator->translator('currency_rate_mismatch').'</span>';
								}

								$entry_arr['base_currency'] = $default_currency;
								$entry_arr ['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1') ? true : false;
								$view_datas ['data_result'] [$key] = $entry_arr;
								$key ++;
							}
							$view_datas ['total'] = $list_datas->getTotalItemCount ();
						}
						$this->_controllerCache->save ( $view_datas, $uniq_id );
					}

					$json_arr = array (
							'status' => 'ok',
							'data_result' => $view_datas ['data_result'],
							'total' => $view_datas ['total'],
							'posted_data' => $posted_data
					);
				} catch ( Exception $e ) {
					$json_arr = array (
							'status' => 'err',
							'data_result' => '',
							'msg' => $e->getMessage ()
					);
				}
				// Convert To JSON ARRAY
				$res_value = Zend_Json_Encoder::encode ( $json_arr );
				$this->_response->setBody ( $res_value );
			}
		} else {

			$msg = $this->translator->translator ( "Member_Access_deny_desc" );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg);
		}
		

	}
	public function editAction() {
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$status = null;
		if ($this->view->allow ()) {
			if ($this->_request->isPost ()) {
				$posted_data = $this->_request->getParams ();
				if ($posted_data ['models']) {
					
					foreach ( $posted_data ['models'] as $posted_data_key => $posted_data_value ) {

						$modelRecordSet = new Paymentgateway_Model_Currency ();
						$modelRecordSet->setId ( $posted_data_value ['id'] );
						$modelRecordSet->setBase_unit ( $posted_data_value ['base_unit'] );
						$modelRecordSet->setBase_currency($this->global_local);
						$modelRecordSet->setTarget_unit ( $posted_data_value ['target_unit'] );
						$modelRecordSet->setTarget_currency ( $posted_data_value ['target_currency_field'] ['target_currency_id'] );
						if($posted_data_value ['target_currency_field'] && $posted_data_value ['target_currency_field'] ['target_currency_id'])
						{
							$currency = new Zend_Currency ($posted_data_value ['target_currency_field'] ['target_currency_id']);
							$currency_format = ($currency->getName()) ? $currency->getShortName().' - '.$currency->getName() : $currency->getShortName();
							$modelRecordSet->setTarget_currency_code ( $currency_format );
						}

						$save_result = $modelRecordSet->save ();
						if ($save_result ['status'] == 'ok') {
							$posted_data ['models'] [$posted_data_key] ['id'] = $save_result ['id'];
							$posted_data ['models'] [$posted_data_key] ['target_currency_code'] = $currency_format;
							$posted_data ['models'] [$posted_data_key] ['equals_field'] =  '=';							
							$posted_data ['models'] [$posted_data_key] ['mismatch'] =  false;
							$status = 'ok';
						} else {
							$status = 'err';
							$msg = $this->translator->translator ( "record_save_err" );
							$json_arr = array (
									'status' => 'err',
									'msg' => $msg . " " . $save_result ['msg']
							);
							break;
						}
					}

					if ($status == 'ok') {
						$msg = $this->translator->translator ( "record_save_success" );
						$json_arr = array (
								'status' => 'ok',
								'msg' => $msg,
								'data_result' => $posted_data ['models']
						);
					}
				} else {
					$msg = $this->translator->translator ( "record_save_err" );
					$json_arr = array (
							'status' => 'err',
							'msg' => $msg
					);
				}
			} else {
				$msg = $this->translator->translator ( "record_save_err" );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
			}
		} else {
			$Msg = $this->translator->translator ( "record_action_deny_desc" );
			$json_arr = array (
					'status' => 'errP',
					'msg' => $Msg
			);
		}
		$res_value = Zend_Json_Encoder::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function addAction() {
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$status = null;
		if ($this->view->allow ()) {
			if ($this->_request->isPost ()) {
				$posted_data = $this->_request->getParams ();
				if ($posted_data ['models']) {
					foreach ( $posted_data ['models'] as $posted_data_key => $posted_data_value ) {

						$modelRecordSet = new Paymentgateway_Model_Currency ();
						$modelRecordSet->setBase_unit ( '1' );
						$modelRecordSet->setBase_currency ( $this->global_local );
						$modelRecordSet->setTarget_unit ( $posted_data_value ['target_unit'] );
						$modelRecordSet->setTarget_currency ( $posted_data_value ['target_currency_field'] ['target_currency_id'] );
						if($posted_data_value ['target_currency_field'] && $posted_data_value ['target_currency_field'] ['target_currency_id'])
						{
							$currency = new Zend_Currency ($posted_data_value ['target_currency_field'] ['target_currency_id']);
							$currency_format = ($currency->getName()) ? $currency->getShortName().' - '.$currency->getName() : $currency->getShortName();
							$modelRecordSet->setTarget_currency_code ( $currency_format );
						}

						$save_result = $modelRecordSet->save ();
						if ($save_result ['status'] == 'ok') {
							$posted_data ['models'] [$posted_data_key] ['id'] = $save_result ['id'];
							$posted_data ['models'] [$posted_data_key] ['target_currency_code'] = $currency_format;
							$posted_data ['models'] [$posted_data_key] ['equals_field'] =  '=';
							$posted_data ['models'] [$posted_data_key] ['mismatch'] =  false;
							$status = 'ok';
						} else {
							$status = 'err';
							$msg = $this->translator->translator ( "record_save_err" );
							$json_arr = array (
									'status' => 'err',
									'msg' => $msg . " " . $save_result ['msg']
							);
							break;
						}
					}

					if ($status == 'ok') {
						$msg = $this->translator->translator ( "record_save_success" );
						$json_arr = array (
								'status' => 'ok',
								'msg' => $msg,
								'data_result' => $posted_data ['models']
						);
					}
				} else {
					$msg = $this->translator->translator ( "record_save_err" );
					$json_arr = array (
							'status' => 'err',
							'msg' => $msg
					);
				}
			} else {
				$msg = $this->translator->translator ( "record_save_err" );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
			}
		} else {
			$Msg = $this->translator->translator ( "record_action_deny_desc" );
			$json_arr = array (
					'status' => 'errP',
					'msg' => $Msg
			);
		}
		$res_value = Zend_Json_Encoder::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	
	public function localetocurrencyAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		try
		{
			if ($this->_request->isPost ()) 
			{
				$currency_locale	=		$this->_request->getPost ('currency_locale');
				$currency = new Zend_Currency ($currency_locale);
				$currency_arr	=	array('currency_name' => $currency->getName (), 'currency_symbol' => $currency->getSymbol (), 'currency_short_name' => $currency->getShortName ());
				$json_arr = array ( 'status' => 'ok', 'data_result' => $currency_arr );
			}
		}
		catch(Exception $e)
		{
			$json_arr = array ( 'status' => 'err', 'msg' => $e->getMessage() );
		}
		
		// Convert To JSON ARRAY
		$res_value = Zend_Json_Encoder::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	
	public function localAction() {

		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$global_conf = Zend_Registry::get ( 'global_conf' );
		$memory = $global_conf ['memory_management'];
		$posted_data = $this->_request->getParams ();
		$this->view->assign ( 'posted_data', $posted_data );

		if ($this->_request->isPost ()) {

			$encode_params = Zend_Json_Encoder::encode ( $posted_data );
			$encode_auth_obj = Zend_Json_Encoder::encode ( $this->_auth_obj );
			$uniq_id = md5 ( preg_replace ( '/[^a-zA-Z0-9_]/', '_', $this->view->url () . '_' . $encode_params . '_' . $encode_auth_obj ) );
			if (($local_array = $this->_controllerCache->load ( $uniq_id )) === false) {
				$locals = new Zend_Locale ();
				$locales_options = $locals->getLocaleList ();
				$currency = new Zend_Currency ($this->global_local);
				$counter = 0;
				$local_array = array (array('target_currency_id' => 'en_GB', 'target_currency_name' => 'GBP'), array('target_currency_id' => 'fr_FR', 'target_currency_name' => 'EUR'), array('target_currency_id' => 'en_US', 'target_currency_name' => 'USD'));
				foreach ( $locales_options as $key => $value ) {
					try {

						$position = strrpos ( $key, "_" );
						if ($position === false) { // note: three equal signs
							continue;
						}

						$locals->setLocale ( $key );
						$region = $locals->getRegion ();
						if (! empty ( $region )) {
							if ($memory == '3'){
								$currency->setLocale ( $key );
								$currency_view = ($currency->getName ()) ? $currency->getSymbol () . '  - ' . $currency->getName () . ' [' . $currency->getShortName () . ']' : $currency->getSymbol () . ' [' . $currency->getShortName () . ']';
								$local_array [$counter] = array (
										'target_currency_id' => $key,
										'target_currency_name' => $currency_view
								);
							}
							else {
								$local_array [$counter] = array (
										'target_currency_id' => $key,
										'target_currency_name' => $key
								);
							}
						}
						$counter ++;
					} catch ( Exception $e ) {
						// I let the process to run although the Exception occer.
					}
				}
				$local_array = array_values($local_array);
				$this->_controllerCache->save ( $local_array, $uniq_id );
			}
			$json_arr = array (
					'status' => 'ok',
					'data_result' => $local_array
			);

			// Convert To JSON ARRAY
			$res_value = Zend_Json_Encoder::encode ( $json_arr );
			$this->_response->setBody ( $res_value );
		}
	}
	public function deleteAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		$json_arr = null;
		if ($this->_request->isPost ()) {
			$permission = new Paymentgateway_View_Helper_Allow ();
			if ($permission->allow ()) {
				$id = $this->_request->getPost ( 'id' );
				$modeMapperlRecordSet = new Paymentgateway_Model_CurrencyMapper ();
				try {
					$result = $modeMapperlRecordSet->delete ( $id );
					if ($result ['status'] == 'ok') {
						$msg = $translator->translator ( "common_delete_success" );
						$json_arr = array (
								'status' => 'ok',
								'msg' => $msg
						);
					} else {
						$msg = $translator->translator ( "common_delete_failed" );
						$json_arr = array (
								'status' => 'err',
								'msg' => $msg . " " . $result ['msg']
						);
					}
				} catch ( Exception $e ) {
					$json_arr = array (
							'status' => 'err',
							'msg' => $e->getMessage ()
					);
				}
			} else {
				$msg = $translator->translator ( "page_access_restrictions" );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
				$res_value = Zend_Json::encode ( $json_arr );
				$this->_response->setBody ( $res_value );
			}
		} else {
			$msg = $translator->translator ( 'common_selected_err' );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function deleteallAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );

		if ($this->_request->isPost ()) {
			$id_str = $this->_request->getPost ( 'id_st' );
			$permission = new Paymentgateway_View_Helper_Allow ();
			if ($permission->allow ()) {
				if (! empty ( $id_str )) {
					$id_arr = explode ( ',', $id_str );

					foreach ( $id_arr as $id ) {
						try {
							$modeMapperlRecordSet = new Paymentgateway_Model_CurrencyMapper ();
							$result = $modeMapperlRecordSet->delete ( $id );
							if ($result ['status'] == 'ok') {
								$msg = $translator->translator ( "common_delete_success" );
								$json_arr = array (
										'status' => 'ok',
										'msg' => $msg
								);
							} else {
								$msg = $translator->translator ( "common_delete_failed" );
								$json_arr = array (
										'status' => 'err',
										'msg' => $msg . " " . $result ['msg']
								);
							}
						} catch ( Exception $e ) {
							$json_arr = array (
									'status' => 'err',
									'msg' => $e->getMessage () . ' ' . $id
							);
						}
					}
				} else {
					$msg = $translator->translator ( "common_selected_err" );
					$json_arr = array (
							'status' => 'err',
							'msg' => $msg
					);
				}
			} else {
				$msg = $translator->translator ( 'record_action_deny_desc' );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
			}
		} else {
			$msg = $translator->translator ( 'common_selected_err' );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
}