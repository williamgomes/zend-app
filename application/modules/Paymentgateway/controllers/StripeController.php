<?php

class Paymentgateway_StripeController extends Zend_Controller_Action {

    private $_controllerCache;
    private $user_id = null;
    private $_auth_obj = null;
    private $_translator;

    public function init() {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;

        $getAction = $this->_request->getActionName();
        if ($getAction != 'uploadfile') {
            //Check Login
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
        }
        //Initialize translator
        $this->_translator = Zend_Registry::get ( 'translator' );
        $this->view->assign('translator', $translator);
        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        $this->user_id = ($this->view->auth->hasIdentity()) ? $this->view->auth->getIdentity()->user_id : '';
    }

    public function stripeajaxAction() {
        if ($this->_request->isPost()) {
            try {

                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                $strpToken = $this->_request->getPost('stripeToken');
                $priceDollar = $this->_request->getPost('payAmount');
                $custEmail = $this->_request->getPost('custEmail');
                //converting dollar into cents
                $priceCents = $priceDollar * 100;


                if ($strpToken != '') {
//                    $stripeHelper = new Paymentgateway_Controller_Helper_Stripe_Stripe();
                    $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
//                    $staticStripeHelper = $stripeHelper->Stripe;
                    $stripeSettings = $api_table->getInfo(11, $this->user_id);
                    $testMode = '';
                    $testSecretKey = '';
                    $liveSecretKey = '';

                    for ($i = 0; $i < count($stripeSettings); $i++) {
                        if ($stripeSettings[$i]['setting'] == "test_mode") {
                            $testMode = $stripeSettings[$i]['value'];
                        }

                        if ($stripeSettings[$i]['setting'] == "live_secret_key") {
                            $liveSecretKey = $stripeSettings[$i]['value'];
                        }

                        if ($stripeSettings[$i]['setting'] == "test_secret_key") {
                            $testSecretKey = $stripeSettings[$i]['value'];
                        }
                    }


                    if ($testMode == 1) {
                        Paymentgateway_Controller_Helper_Stripe_Stripe::setApiKey($testSecretKey);
                    } else {
                        Paymentgateway_Controller_Helper_Stripe_Stripe::setApiKey($liveSecretKey);
                    }
                    
                    
                    
                    $charge = Paymentgateway_Controller_Helper_Stripe_Charge::create(array(
                                "amount" => $priceCents, // amount in cents, again
                                "currency" => "usd",
                                "card" => $strpToken,
                                "description" => $custEmail
                                    )
                    );

                    

                    // Check that it was paid:
                    if ($charge->paid == true) {
                        $json_arr = array('status' => 'ok', 'msg' => $this->_translator->translator('payment_stripe_successfull', '', 'Paymentgateway'));
                    } else { // Charge was not paid!	
                        $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('payment_stripe_failed', '', 'Paymentgateway'));
                    }
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('payment_stripe_token_error', '', 'Paymentgateway'));
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }
    
}

?>