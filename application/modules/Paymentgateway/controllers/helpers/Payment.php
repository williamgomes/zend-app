<?php

class Paymentgateway_Controller_Helper_Payment {

    private $_pay_link;
    private $_invoice_info;
    private $url_and = '&';
    private $_translator;

    public function __construct($invoice_info = null) {
        $this->_translator = Zend_Registry::get('translator');
        $this->_invoice_info = $invoice_info;
        $this->generateLink();
    }

    private function generateLink() {
        if (!empty($this->_invoice_info)) {
            $global_conf = Zend_Registry::get('global_conf');
            $currency = new Zend_Currency($global_conf['default_locale']);
            $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
            switch (strtolower(str_replace(' ', '', $this->_invoice_info['payment_info']['name']))) {
                case 'paypal':
                    $total_price = $this->_invoice_info['total_amount'] + $this->_invoice_info['total_tax'];
                    $currency_name = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $currency->getShortName();
                    $this->_pay_link = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $email . '&item_name=Invoice-' . $this->_invoice_info['invoice_id'] . '&item_number=1&amount=' . round($total_price, 2) . '&no_shipping=0&no_note=1&currency_code=' . $currency_name . '&bn=PP%2dBuyNowBF&charset=UTF%2d8 target=_blank';
                    break;
                case '2checkout':
                    $total_price = $this->_invoice_info['total_amount'] + $this->_invoice_info['total_tax'];
                    $currency_name = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $currency->getShortName();
                    $this->_pay_link = 'https://www.2checkout.com/checkout/purchase?sid=' . trim($this->_invoice_info['payment_info']['secret_phrase']) . '&mode=2CO&li_0_type=product&li_0_price=' . round($total_price, 2) . '&li_0_quantity=1&li_0_name=Invoice-' . $this->_invoice_info['invoice_id'] . '';
                    break;
                default:
                    $total_price = $this->_invoice_info['total_amount'] + $this->_invoice_info['total_tax'];
                    $currency_name = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $currency->getShortName();
                    $this->_pay_link = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $email . '&item_name=Invoice-' . $this->_invoice_info['invoice_id'] . '&item_number=1&amount=' . round($total_price, 2) . '&no_shipping=0&no_note=1&currency_code=' . $currency_name . '&bn=PP%2dBuyNowBF&charset=UTF%2d8 target=_blank';
                    break;
            }
        }
    }

    public function getLink() {
        return $this->_pay_link;
    }

    public function getPaypal($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $logo_url = $data['logo'];
        $quantity = $data['quantity'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();

        if ($user_id) {
            $paypal_setting = $api_table->getInfo(1, $user_id);
        } else {
            $paypal_setting = $api_table->getInfo(1, $global_conf['payment_user_id']);
        }

        $paypal = array();
        $findValue = 0;
        $paypal_link = '';
        while ($findValue < count($paypal_setting)) {
            $rowSet = $paypal_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $paypal[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('test_mode', $paypal)) {
            if ($paypal['test_mode']) {
                $paypal_link = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick';
            } else {
                $paypal_link = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick';
            }
        } else {
            $paypal_link = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick';
        }
        if (array_key_exists('email', $paypal)) {
            $paypalEmail = $paypal['email'];
            if (!empty($paypalEmail)) {
                $paypal_link .= $this->url_and . 'business=' . $paypalEmail;
            }
        } else {
            if (!empty($email)) {
                $paypal_link .= $this->url_and . 'business=' . $email;
            }
        }

        if (!empty($invoice_id)) {
            //$item_name = html_entity_decode($this->_translator->translator('invoice_number_title'), ENT_QUOTES, "UTF-8").' - '.$invoice_id;
            $item_name = 'Invoice Number - ' . $invoice_id;
            if (!empty($item_name)) {
                $paypal_link .= $this->url_and . 'item_name=' . $item_name;
            }
        }

        if (array_key_exists('currency', $paypal)) {
            $curency = $paypal['currency'];
            if (!empty($curency)) {
                $paypal_link .= $this->url_and . 'currency_code=' . $curency;
            }
        }
        if (!empty($price)) {
            $paypal_link .= $this->url_and . 'amount=' . $price;
        }
        if (array_key_exists('success_url', $paypal)) {
            $success_url = $paypal['success_url'];
            if (!empty($success_url)) {
                $paypal_link .= $this->url_and . 'return=' . $success_url;
            }
        }
        if (array_key_exists('notify_url', $paypal)) {
            $return_url = $paypal['notify_url'];
            if (!empty($return_url)) {
                $paypal_link .= $this->url_and . 'notify_url=' . $return_url . '&rm=2';
            }
        }
        if (array_key_exists('cancel_url', $paypal)) {
            $cancel_url = $paypal['cancel_url'];
            if (!empty($cancel_url)) {
                $paypal_link .= $this->url_and . 'cancel_return=' . $cancel_url;
            }
        }
        if (array_key_exists('status_url', $paypal)) {
            $status_url = $paypal['status_url'];
            if (!empty($status_url)) {
                $paypal_link .= $this->url_and . 'status_url=' . $status_url;
            }
        }
        if (array_key_exists('lc_locale', $paypal)) {
            $lc_locale = $paypal['lc_locale'];
            if (!empty($lc_locale)) {
                $paypal_link .= $this->url_and . 'lc=' . $lc_locale;
            }
        }
        if (!empty($global_conf['site_url'])) {
            $paypal_link .= $this->url_and . 'site_url=' . $global_conf['site_url'];
        }
        if (!empty($invoice_id)) {
            $paypal_link .= $this->url_and . 'invoice=' . $invoice_id;
        }
        if (!empty($logo_url)) {
            $paypal_link .= $this->url_and . 'image_url=' . $logo_url;
        }
        if (!empty($quantity)) {
            $paypal_link .= $this->url_and . 'no_shipping=' . $quantity;
        }
        return $paypal_link .= '&bn=PP%2dBuyNowBF&charset=UTF%2d8 target=_blank';
        ;
    }

    public function gettocheckout($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$toCheckout_setting = $api_table->getInfo(2);

        if ($user_id) {
            $toCheckout_setting = $api_table->getInfo(2, $user_id);
        } else {
            $toCheckout_setting = $api_table->getInfo(2, $global_conf['payment_user_id']);
        }
        $toCheckout = array();
        $findValue = 0;
        $toCheckout_link = 'https://www.2checkout.com/checkout/purchase';
        while ($findValue < count($toCheckout_setting)) {
            $rowSet = $toCheckout_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $toCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('vendor_number', $toCheckout)) {
            $vendor_id = $toCheckout['vendor_number'];
            if (!empty($vendor_id)) {
                $toCheckout_link .= '?c_tangible=N&x_login=' . $vendor_id;
            }
        }
        if (!empty($invoice_id)) {
            $toCheckout_link .= $this->url_and . 'x_invoice_num=' . $invoice_id;
        }
        if (array_key_exists('test_mode', $toCheckout)) {
            if ($toCheckout['test_mode']) {
                $toCheckout_link .= $this->url_and . 'demo=Y';
            }
        }
        if (array_key_exists('curency', $toCheckout)) {
            $tco_curency = $toCheckout['curency'];
            if (!empty($tco_curency)) {
                $toCheckout_link .= $this->url_and . 'currency_code=' . $tco_curency;
            }
        } else {
            $toCheckout_link .= $this->url_and . 'currency_code=' . $currency;
        }
        if (!empty($price)) {
            $toCheckout_link .= $this->url_and . 'x_amount=' . $price;
        }
        if (array_key_exists('success_url', $toCheckout)) {
            $success_url = $toCheckout['success_url'];
            if (!empty($success_url)) {
                $toCheckout_link .= $this->url_and . 'x_receipt_link_url=' . $success_url;
            }
        }
        if (array_key_exists('language', $toCheckout)) {
            $language = $toCheckout['language'];
            if (!empty($language)) {
                $toCheckout_link .= $this->url_and . 'lang=' . $language;
            }
        }
        if (array_key_exists('return_url', $toCheckout)) {
            $return_url = $toCheckout['return_url'];
            if (!empty($return_url)) {
                $toCheckout_link .= $this->url_and . 'return_url=' . $return_url;
            }
        }
        if (!empty($global_conf['site_title'])) {
            $toCheckout_link .= $this->url_and . 'c_name=' . $global_conf['site_title'] . ' - Invoice #' . $invoice_id;
        }
        if (!empty($global_conf['site_name'])) {
            $toCheckout_link .= $this->url_and . 'c_description=' . $global_conf['site_name'] . ' - Invoice #' . $invoice_id;
        }
        return $toCheckout_link .= '&return_method=2&fixed=Y  target=_blank';
        ;
    }

    public function getmoneybookers($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $pay_from_email = $data['client_email'];
        $logo_url = $data['logo'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();

        if ($user_id) {
            $moneybookers_setting = $api_table->getInfo(4, $user_id);
        } else {
            $moneybookers_setting = $api_table->getInfo(4, $global_conf['payment_user_id']);
        }
        $mbCheckout = array();
        $findValue = 0;
        $mbCheckout_link = 'https://www.moneybookers.com/app/payment.pl';
        while ($findValue < count($moneybookers_setting)) {
            $rowSet = $moneybookers_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $mbCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('test_mode', $mbCheckout)) {
            if ($mbCheckout['test_mode']) {
                $mbCheckout_link = 'http://www.moneybookers.com/app/test_payment.pl';
            } else {
                $mbCheckout_link = 'https://www.moneybookers.com/app/payment.pl';
            }
        }
        if (array_key_exists('pay_to_email', $mbCheckout)) {
            $pay_to_email = $mbCheckout['pay_to_email'];
            if (!empty($pay_to_email)) {
                $mbCheckout_link .= '?pay_to_email=' . $pay_to_email;
            }
        } else {
            if (!empty($email)) {
                $mbCheckout_link .= '?pay_to_email=' . $email;
            } else {
                throw new Exception('Pay To email is Missing which is a required parameter. Please make sure you have added a valid Merchant Email address');
            }
        }
        if (!empty($pay_from_email)) {
            $mbCheckout_link .= $this->url_and . 'pay_from_email=' . $pay_from_email;
        }
        if (!empty($invoice_id)) {
            $mbCheckout_link .= $this->url_and . 'transaction_id=' . $invoice_id;
            $mbCheckout_link .= $this->url_and . 'detail1_text=' . $invoice_id;
        }
        if (array_key_exists('curency', $mbCheckout)) {
            $mbCurency = $mbCheckout['curency'];
            if (!empty($mbCurency)) {
                $mbCheckout_link .= $this->url_and . 'currency=' . $mbCurency;
            }
        }
        if (!empty($price)) {
            $mbCheckout_link .= $this->url_and . 'amount=' . $price;
        }
        if (array_key_exists('language', $mbCheckout)) {
            $language = $mbCheckout['language'];
            if (!empty($language)) {
                $mbCheckout_link .= $this->url_and . 'language=' . $language;
            }
        }
        if (array_key_exists('return_url', $mbCheckout)) {
            $return_url = $mbCheckout['return_url'];
            if (!empty($return_url)) {
                $mbCheckout_link .= $this->url_and . 'return_url=' . $return_url;
            }
        }
        if (array_key_exists('status_url', $mbCheckout)) {
            $status_url = $mbCheckout['status_url'];
            if (!empty($status_url)) {
                $mbCheckout_link .= $this->url_and . 'status_url=' . $status_url;
            }
        }
        if (array_key_exists('cancel_url', $mbCheckout)) {
            $cancel_url = $mbCheckout['cancel_url'];
            if (!empty($cancel_url)) {
                $mbCheckout_link .= $this->url_and . 'cancel_url=' . $cancel_url;
            }
        }
        if (!empty($global_conf['site_title'])) {
            $mbCheckout_link .= $this->url_and . 'detail1_description=' . $global_conf['site_title'] . ' - Invoice #' . $invoice_id;
        }
        if (!empty($global_conf['site_name'])) {
            $mbCheckout_link .= $this->url_and . 'recipient_description=' . $global_conf['site_name'] . ' - Invoice #' . $invoice_id;
        }
        if (!empty($logo_url)) {
            $mbCheckout_link .= $this->url_and . 'logo_url=' . $logo_url;
        }
        return $mbCheckout_link .= ' target=_blank';
    }

    public function getgooglecheckout($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $quantity = $data['quantity'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$googlecheckout_setting = $api_table->getInfo(6);

        if ($user_id) {
            $googlecheckout_setting = $api_table->getInfo(6, $user_id);
        } else {
            $googlecheckout_setting = $api_table->getInfo(6, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        $googleCheckout_link = '';
        while ($findValue < count($googlecheckout_setting)) {
            $rowSet = $googlecheckout_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $googleCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('test_mode', $googleCheckout)) {
            if ($googleCheckout['test_mode']) {
                $googleCheckout_link = '<form action="https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/' . $googleCheckout['merchant_id'] . '"id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm">';
            } else {
                $googleCheckout_link = '<form action="https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/' . $googleCheckout['merchant_id'] . '"id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm">';
            }
        }
        if (!empty($invoice_id)) {
            $googleCheckout_link .= '<input name="item_merchant_id_1" type="hidden" value="' . $invoice_id . '"/>';
        }
        if (array_key_exists('item_currency_1', $googleCheckout)) {
            $googleCheckout_link .= '<input name="item_currency_1" type="hidden" value="' . $googleCheckout['item_currency_1'] . '"/>';
        }
        if (!empty($price)) {
            $googleCheckout_link .= '<input name="item_price_1" type="hidden" value="' . $price . '"/>';
        }
        if (array_key_exists('language', $googleCheckout)) {
            $googleCheckout_link .= '<input name="language" type="hidden" value="' . $googleCheckout['language'] . '"/>';
        }
        if (array_key_exists('continue_url', $googleCheckout)) {
            $googleCheckout_link .= '<input name="continue_url" type="hidden" value="' . $googleCheckout['continue_url'] . '"/>';
        }
        if (array_key_exists('_charset_', $googleCheckout)) {
            $googleCheckout_link .= '<input name="_charset_" type="hidden" value="' . $googleCheckout['_charset_'] . '"/>';
        }
        if (array_key_exists('loc', $googleCheckout)) {
            $googleCheckout_link .= '<input name="loc" type="hidden" value="' . $googleCheckout['loc'] . '"/>';
        }
        if (!empty($global_conf['site_title'])) {
            $googleCheckout_link .= '<input name="item_description_1" type="hidden" value=" ' . $global_conf['site_title'] . ' Invoice #' . $invoice_id . '"/>';
        }
        if (!empty($quantity)) {
            $googleCheckout_link .= '<input name="item_quantity_1" type="hidden" value="' . $quantity . '"/>';
        }
        $googleCheckout_link .= '<input name="item_name_1" type="hidden" value="Invoice Payment"/>';
        $googleCheckout_link .= '<input type="hidden" name="shopping-cart.items.item-1.digital-content.email-delivery" value="true" />';
        $googleCheckout_link .= '<input name="item_quantity_1" type="hidden" value="1"/>';
        if (array_key_exists('merchant_id', $googleCheckout)) {
            if ($googleCheckout['test_mode']) {
                $googleCheckout_link .= '<input type="image" name="Google Checkout" alt="Fast checkout through Google" src="http://sandbox.google.com/checkout/buttons/checkout.gif?merchant_id=' . $googleCheckout['merchant_id'] . '&w=180&h=46&style=white&variant=text&loc=en_US" height="46" width="180"/>';
            } else {
                $googleCheckout_link .= '<input type="image" name="Google Checkout" alt="Fast checkout through Google" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=' . $googleCheckout['merchant_id'] . '&w=180&h=46&style=white&variant=text&loc=en_US" height="46" width="180"/>';
            }
        }
        return $googleCheckout_link .= ' </form>';
    }

    public function getbank($data, $user_id = null) {
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$bank_setting = $api_table->getInfo(7);
        if ($user_id) {
            $bank_setting = $api_table->getInfo(7, $user_id);
        } else {
            $bank_setting = $api_table->getInfo(7, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        $bank_link = '<br /> ';
        while ($findValue < count($bank_setting)) {
            $rowSet = $bank_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $bank[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        if (array_key_exists('name', $bank)) {
            $bank_link .= '<strong>' . $bank['name'] . '</strong> <br /> <hr>';
        }
        if (array_key_exists('bank_details', $bank)) {
            $bank_link .= nl2br($bank['bank_details']);
        }
        return $bank_link .= '<hr>';
    }

    public function getworldpay($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $pay_from_email = $data['client_email'];
        $logo_url = $data['logo'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$worldpay_setting = $api_table->getInfo(5);

        if ($user_id) {
            $worldpay_setting = $api_table->getInfo(5, $user_id);
        } else {
            $worldpay_setting = $api_table->getInfo(5, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        $worldpayCheckout_link = 'https://secure.worldpay.com/wcc/purchase';
        while ($findValue < count($worldpay_setting)) {
            $rowSet = $worldpay_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $worldpayCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('installation_id', $worldpayCheckout)) {
            $worldpayCheckout_link .= '?instId=' . $worldpayCheckout['installation_id'];
        } else {
            throw new Exception('installation ID IS Missing which is a required parameter for World Pay API. Please make sure you have added a valid Merchant installation_id');
        }
        if (array_key_exists('test_mode', $worldpayCheckout)) {
            if ($worldpayCheckout['test_mode']) {
                $worldpayCheckout_link .= 'testMode=100';
            } else {
                $worldpayCheckout_link .= 'testMode=0';
            }
        }
        if (!empty($pay_from_email)) {
            $worldpayCheckout_link .= $this->url_and . 'email=' . $pay_from_email;
        }
        if (!empty($invoice_id)) {
            $worldpayCheckout_link .= $this->url_and . 'cartId=' . $invoice_id;
        }
        if (array_key_exists('currency', $worldpayCheckout)) {
            $worldpayCheckout_link .= $this->url_and . 'currency=' . $worldpayCheckout['currency'];
        }
        if (!empty($price)) {
            $worldpayCheckout_link .= $this->url_and . 'amount=' . $price;
        }
        if (array_key_exists('language', $worldpayCheckout)) {
            $worldpayCheckout_link .= $this->url_and . 'language=' . $worldpayCheckout['language'];
        }
        if (array_key_exists('MC_callback', $worldpayCheckout)) {
            $worldpayCheckout_link .= $this->url_and . 'MC_callback=' . $worldpayCheckout['MC_callback'];
        }
        if (!empty($global_conf['site_title'])) {
            $worldpayCheckout_link .= $this->url_and . 'desc=' . $global_conf['site_title'] . ' - Invoice #' . $invoice_id;
        }
        return $worldpayCheckout_link .= ' target=_blank';
    }

    public function getccAvenue($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $pay_from_email = $data['client_email'];
        $clilent_name = $data['client_name'];
        $logo_url = $data['logo'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$ccAvenue_setting = $api_table->getInfo(8);

        $invoice_db = new Invoice_Model_DbTable_Invoices();
        $select = $invoice_db->select()
                ->setIntegrityCheck(false)
                ->from(array('inv' => Zend_Registry::get('dbPrefix') . 'invoice_table'), array())
                ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'up.user_id = inv.user_id', array('billing_zip' => 'up.postalCode', 'billing_address' => 'up.address', 'billing_city' => 'up.city', 'billing_country' => 'up.country', 'billing_state' => 'up.state', 'billing_tel' => 'up.mobile', 'billing_name' => "CONCAT(up.title, ' ', up.firstName, ' ', up.lastName)"))
                ->where('inv.id = ' . $invoice_id);
        $options = $invoice_db->fetchAll($select);
        $userInformation = (!is_array($options)) ? $options->toArray() : $options;

//        if (!is_array($options)) 
//            { $options->toArray() ;}
//        else {$options;}
//        print_r($userInformation[0]);
//        echo '<strong>'.count($userInformation).'</strong>';




        if ($user_id) {
            $ccAvenue_setting = $api_table->getInfo(8, $user_id);
        } else {
            $ccAvenue_setting = $api_table->getInfo(8, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        while ($findValue < count($ccAvenue_setting)) {
            $rowSet = $ccAvenue_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $worldpayCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }

//        print_r($worldpayCheckout);
//        print_r($data);


        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];


        //pushing fields inside the string
        $merchant_data = '';
        if (count($userInformation[0]) > 0) {
            foreach ($userInformation[0] as $key => $value) {
                $merchant_data.=$key . '=' . urlencode($value) . '&';
            }
        }

        //pushing merchant id inside the string
        if (array_key_exists('merchant_id', $worldpayCheckout)) {
            $merchant_data.='merchant_id=' . urlencode($worldpayCheckout['merchant_id']) . '&';
        } else {
            throw new Exception('Merchant ID IS Missing which is a required parameter for CCAvenue API. Please make sure you have added a valid Merchant ID');
        }

        //pushing order id inside the string
        if (!empty($invoice_id)) {
            $merchant_data.='order_id=' . urlencode($invoice_id) . '&';
        }

        //pushing currency inside the string
        if (array_key_exists('currency', $worldpayCheckout)) {
            $merchant_data.='currency=' . urlencode($worldpayCheckout['currency']) . '&';
        }

        //pushing amount inside the string
        if (!empty($price)) {
            $merchant_data.='amount=' . urlencode($price) . '&';
        }

        //pushing title inside the string
        if (!empty($global_conf['site_title'])) {
            $merchant_data.='currency=' . urlencode($worldpayCheckout['currency']) . '&';
        }

        //pushing redirect url inside the string
        if (array_key_exists('Redirect_Url', $worldpayCheckout)) {
            $merchant_data.='redirect_url=' . urlencode($worldpayCheckout['Redirect_Url']) . '&';
        }

        //pushing ActionID and TxnType inside the string
        $merchant_data.='TxnType=' . urlencode('A') . '&';
        $merchant_data.='ActionID=' . urlencode('TXN') . '&';



        //rendering the form to submit
        $ccAvenue_link = '';

        if (array_key_exists('Sandbox', $worldpayCheckout)) :
            if ($worldpayCheckout['Sandbox'] == 1):
                $ccAvenue_link .= '<form id="ccAvenueForm" name="ccAvenueForm" method="post" action="https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction">';
            else:
                $ccAvenue_link .= '<form id="ccAvenueForm" name="ccAvenueForm" method="post" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">';
            endif;
        else:
            throw new Exception('Sandbox Mode does not exist in database which is a required parameter for CCAvenue API.');
        endif;

        //encripting whole URL with encryption code
        $encryptCode = '';
        $encryptedData = '';
        if (array_key_exists('merchant_key', $worldpayCheckout)) {
            $encryptCode = $worldpayCheckout['merchant_key'];
        } else {
            throw new Exception('Access Code IS Missing which is a required parameter for CCAvenue API. Please make sure you have added a valid Access Code');
        }

        $encryptedData = $this->encrypt($merchant_data, $encryptCode); // Method for encrypting the data.

        if ($encryptedData != '') {
            $ccAvenue_link .= '<input type="hidden" name="encRequest" value="' . $encryptedData . '"/>';
        } else {
            throw new Exception('Encryption process failed.');
        }

        //generating hidden field for access code
        if (array_key_exists('Access_code', $worldpayCheckout)) {
            $ccAvenue_link .= '<input type="hidden" name="access_code" value="' . $worldpayCheckout['Access_code'] . '"/>';
        } else {
            throw new Exception('Access Code IS Missing which is a required parameter for CCAvenue API. Please make sure you have added a valid Access Code');
        }


        $ccAvenue_link .= "<img src=\"" . $data['logo'] . "\" id=\"ccAvenue_submit\" width=\"140\" style=\"cursor:pointer\"></form>";

        $ccAvenue_link .= "<script type=\"text/javascript\">
								$(document).ready(function() {
									$('#ccAvenue_submit').click(function(){
											$('#ccAvenueForm').submit();
										});
								});</script>";
        return $ccAvenue_link;
    }

    /* New function for encrypting post request for CCAvenue */

    private function encrypt($plainText, $key) {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad = $this->pkcs5_pad($plainText, $blockSize);
        if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) {
            $encryptedText = mcrypt_generic($openMode, $plainPad);
            mcrypt_generic_deinit($openMode);
        }
        return bin2hex($encryptedText);
    }

    private function decrypt($encryptedText, $key) {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
    }

    //*********** Padding Function *********************

    private function pkcs5_pad($plainText, $blockSize) {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    //********** Hexadecimal to Binary function for php 4.0 version ********

    private function hextobin($hexString) {
        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length) {
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString.=$packedString;
            }

            $count+=2;
        }
        return $binString;
    }

    /* //New function for encrypting post request for CCAvenue */

    public function getWebPay($data, $user_id = null) {
        $locale = Eicra_Global_Variable::getSession()->sess_lang;
        $locale_arr = explode('_', $locale);
        $lang = strtoupper($locale_arr[0]);
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $pay_from_email = $data['client_email'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$webpay_setting = $api_table->getInfo(9);
        if ($user_id) {
            $webpay_setting = $api_table->getInfo(9, $user_id);
        } else {
            $webpay_setting = $api_table->getInfo(9, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        $webpayCheckout_link = "<a href='javascript: void(0);' class='UPay_Pay'><img src='https://www.ubacipg.com/MerchantServices/MerchantButtonImager.ashx?MERCHANT_ID=";
        while ($findValue < count($webpay_setting)) {
            $rowSet = $webpay_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $webpayCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('mercId', $webpayCheckout)) {
            $webpayCheckout_link .= $webpayCheckout['mercId'] . "' border='0' alt='Pay'/></a>";
            $webpayCheckout_link .= '<div id="upay_div">  <form method="POST" id="upay_form" name="upay_form" action="https://www.ubacipg.com/MerchantServices/MakePayment.aspx" target="_top">';
            $webpayCheckout_link .= '<input type="hidden" name="mercId" value="' . $webpayCheckout['mercId'] . '" >';
        } else {
            throw new Exception('Merchant ID IS Missing which is a required parameter for UBA WebPay API. Please make sure you have added a valid Merchant ID');
        }
        if (array_key_exists('currCode', $webpayCheckout)) {
            $webpayCheckout_link .= '<input type="hidden" name="currCode" value="' . $webpayCheckout['currCode'] . '">';
        } else {
            $webpayCheckout_link .= '<input type="hidden" name="currCode" value="566">';
        }
        if (!empty($invoice_id)) {
            $webpayCheckout_link .= '<input type="hidden" name="orderId" value="' . $invoice_id . '">';
        }
        if (!empty($price)) {
            $webpayCheckout_link .= '<input type="hidden" name="amt" value="' . $price . '">';
        }
        if (array_key_exists('prod', $webpayCheckout)) {
            $webpayCheckout_link .= '<input type="hidden" name="prod" value="' . $webpayCheckout['prod'] . '">';
        }
        if (array_key_exists('email', $webpayCheckout)) {
            $webpayCheckout_link .= '<input type="hidden" name="email" value="' . $webpayCheckout['email'] . '">';
        } else {
            $webpayCheckout_link .= '<input type="hidden" name="email" value="' . $pay_from_email . '">';
        }
        return $webpayCheckout_link .= '<input type="hidden" name="SecurityInfo" value="">  </form> </div>';
    }

    private function getchecksum($MerchantId, $Amount, $OrderId, $URL, $WorkingKey) {
        $str = "$MerchantId|$OrderId|$Amount|$URL|$WorkingKey";
        $adler = 1;
        $adler = $this->adler32($adler, $str);
        return $adler;
    }

    private function adler32($adler, $str) {
        $BASE = 65521;
        $s1 = $adler & 0xffff;
        $s2 = ($adler >> 16) & 0xffff;
        for ($i = 0; $i < strlen($str); $i ++) {
            $s1 = ($s1 + Ord($str[$i])) % $BASE;
            $s2 = ($s2 + $s1) % $BASE;
            // echo "s1 : $s1 <BR> s2 : $s2 <BR>";
        }
        return $this->leftshift($s2, 16) + $s1;
    }

    private function leftshift($str, $num) {
        $str = DecBin($str);
        for ($i = 0; $i < (64 - strlen($str)); $i ++)
            $str = "0" . $str;
        for ($i = 0; $i < $num; $i ++) {
            $str = $str . "0";
            $str = substr($str, 1);
            // echo "str : $str <BR>";
        }
        return $this->cdec($str);
    }

    private function cdec($num) {
        for ($n = 0; $n < strlen($num); $n ++) {
            $temp = $num[$n];
            $dec = $dec + $temp * pow(2, strlen($num) - $n - 1);
        }
        return $dec;
    }

    public function getpayza($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $quantity = $data['quantity'];
        $global_conf = Zend_Registry::get('global_conf');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$payzaCheckout_setting = $api_table->getInfo(3);

        if ($user_id) {
            $payzaCheckout_setting = $api_table->getInfo(3, $user_id);
        } else {
            $payzaCheckout_setting = $api_table->getInfo(3, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        $googleCheckout_link = '<form action="https://secure.payza.com/checkout" method="post">';
        while ($findValue < count($payzaCheckout_setting)) {
            $rowSet = $payzaCheckout_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $pazyCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $email = ($this->_invoice_info['payment_info']['curency']) ? $this->_invoice_info['payment_info']['curency'] : $global_conf['payment_email'];
        if (array_key_exists('test_mode', $pazyCheckout)) {
            if ($pazyCheckout['test_mode']) {
                $pazyCheckout_link = '<form action="https://sandbox.Payza.com/sandbox/payprocess.aspx" method="post">';
                $pazyCheckout_link .= '<input type="hidden" name="ap_purchasetype" value="item">';
            } else {
                $pazyCheckout_link = '<form action="https://secure.payza.com/checkout" method="post">';
                $pazyCheckout_link .= '<input type="hidden" name="ap_purchasetype" value="item">';
            }
        }
        if (array_key_exists('ap_merchant', $pazyCheckout)) {
            $ap_merchant = $pazyCheckout['ap_merchant'];
            if (!empty($ap_merchant)) {
                $pazyCheckout_link .= '<input name="ap_merchant" type="hidden" value="' . $ap_merchant . '"/>';
            }
        }
        if (!empty($invoice_id)) {
            $pazyCheckout_link .= '<input name="apc_1" type="hidden" value="' . $invoice_id . '"/>';
        }
        if (array_key_exists('ap_currency', $pazyCheckout)) {
            $currency = $pazyCheckout['ap_currency'];
            if (!empty($currency)) {
                $pazyCheckout_link .= '<input name="ap_currency" type="hidden" value="' . $pazyCheckout['ap_currency'] . '"/>';
            }
        }
        if (!empty($price)) {
            $pazyCheckout_link .= '<input name="ap_amount" type="hidden" value="' . $price . '"/>';
        }
        if (array_key_exists('ap_returnurl', $pazyCheckout)) {
            $returnurl = $pazyCheckout['ap_returnurl'];
            if (!empty($returnurl)) {
                $pazyCheckout_link .= '<input name="ap_returnurl" type="hidden" value="' . $returnurl . '"/>';
            }
        }
        if (array_key_exists('ap_cancelurl', $pazyCheckout)) {
            $cancelurl = $pazyCheckout['ap_cancelurl'];
            if (!empty($cancelurl)) {
                $pazyCheckout_link .= '<input name="ap_cancelurl" type="hidden" value="' . $cancelurl . '"/>';
            }
        }
        if (!empty($global_conf['site_title'])) {
            $pazyCheckout_link .= '<input name="ap_itemname" type="hidden" value= "' . $global_conf['site_title'] . ' Invoice #' . $invoice_id . '"/>';
        }
        if (!empty($quantity)) {
            $pazyCheckout_link .= '<input name="ap_quantity" type="hidden" value="' . $quantity . '"/>';
        }
        return $pazyCheckout_link .= '<input type="image" name="ap_image" src="https://www.payza.com/images/payza-buy-now.png"/>   </form>';
    }

    public function getAuthorizenet($data, $user_id = null) {
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $global_conf = Zend_Registry::get('global_conf');
        $invoice_db = new Invoice_Model_DbTable_Invoices();
        $invoice_info = ($invoice_id) ? $invoice_db->getInfo($invoice_id) : null;

        $x_cust_id = $data['client_name'];
        $x_description = ($invoice_info && $invoice_info['invoice_subject']) ? strip_tags($invoice_info['invoice_subject']) : '';
        $authorizeNetForm = null;
        $sequence = rand(1, 1000);
        $timeStamp = time();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();
        //$authorizeNet_setting = $api_table->getInfo(10);

        if ($user_id) {
            $authorizeNet_setting = $api_table->getInfo(10, $user_id);
        } else {
            $authorizeNet_setting = $api_table->getInfo(10, $global_conf['payment_user_id']);
        }

        $findValue = 0;
        while ($findValue < count($authorizeNet_setting)) {
            $rowSet = $authorizeNet_setting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $authorizeNet_Checkout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        $loginID = $authorizeNet_Checkout['loginid'];
        $transactionKey = $authorizeNet_Checkout['transactionKey'];

        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $price . "^", $transactionKey);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $price . "^", $transactionKey));
        }
        if (array_key_exists('test_mode', $authorizeNet_Checkout)) {
            if ($authorizeNet_Checkout['test_mode']) {
                $authorizeNetForm = "<form name='authorizeNetForm' id='authorizeNetForm' action='https://test.authorize.net/gateway/transact.dll' method='post'>";
                $authorizeNetForm .= "<input type='hidden' name='x_test_request' value='false' />";
            } else {
                $authorizeNetForm = "<form name='authorizeNetForm' id='authorizeNetForm' action='https://secure.authorize.net/gateway/transact.dll' method='post'>";
            }
        }
        if (array_key_exists('loginid', $authorizeNet_Checkout)) {
            $ap_merchant = $authorizeNet_Checkout['loginid'];
            if (!empty($ap_merchant)) {
                $authorizeNetForm .= '<input name="x_login" type="hidden" value="' . $ap_merchant . '"/>';
            }
        }
        if (!empty($invoice_id)) {
            $authorizeNetForm .= "<input type='hidden' name='x_invoice_num' value='" . $invoice_id . "'/>";
        }
        if (array_key_exists('x_currency_code', $authorizeNet_Checkout)) {
            $currency = $authorizeNet_Checkout['x_currency_code'];
            if (!empty($currency)) {
                $authorizeNetForm .= "<input name='x_currency_code' type='hidden' value='' . $currency . ''/>";
            }
        }
        if (!empty($price)) {
            $authorizeNetForm .= "<input type='hidden' name='x_amount'  value='" . $price . "'/>";
        }
        if (array_key_exists('x_return_policy_url', $authorizeNet_Checkout)) {
            $returnurl = $authorizeNet_Checkout['x_return_policy_url'];
            if (!empty($returnurl)) {
                $authorizeNetForm .= "<input name='x_return_policy_url' type='hidden' value='" . $returnurl . "'>";
            }
        }
        if (array_key_exists('x_cancel_url', $authorizeNet_Checkout)) {
            $cancelurl = $authorizeNet_Checkout['x_cancel_url'];
            if (!empty($cancelurl)) {
                $authorizeNetForm .= '<input name="x_cancel_url" type="hidden" value="' . $cancelurl . '"/>';
            }
        }
        if (!empty($x_cust_id)) {
            $authorizeNetForm .= "<input type='hidden' name='x_cust_id' value='" . $x_cust_id . "' /> ";
        }
        if (!empty($x_description)) {
            $authorizeNetForm .= "<input type='hidden' name='x_description' value='" . $x_description . "' /> ";
        }
        $authorizeNetForm .= "<input type='hidden' name='x_fp_sequence' value='" . $sequence . "' /> ";
        $authorizeNetForm .= "<input type='hidden' name='x_fp_hash' value='" . $fingerprint . "' /> ";
        $authorizeNetForm .= "<input type='hidden' name='x_fp_timestamp' value='" . $timeStamp . "' /> ";
        $authorizeNetForm .= "<input type='hidden' name='x_method' value='CC' /> ";
        $authorizeNetForm .= "<input type='hidden' name='x_show_form' value='PAYMENT_FORM' /> ";
        $authorizeNetForm .= "<img src=\"" . $data['logo'] . "\" id=\"authorizenet_submit\" width=\"140\" style=\"cursor:pointer\"></form>";
        $authorizeNetForm .= "<script type=\"text/javascript\">
								$(document).ready(function() {
									$('#authorizenet_submit').click(function(){
											$('#authorizeNetForm').submit();
										});
								});</script>";
        return $authorizeNetForm;
    }
    
    
    
    /*Function for Stripe.com Payment Gateway*/
    
    
    public function getStripe($data, $user_id = null) {
		$translator = Zend_Registry::get ( 'translator' );
        $price = $data['payble'];
        $invoice_id = $data['invoice'];
        $pay_from_email = $data['client_email'];
        $clilent_name = $data['client_name'];
        $logo_url = $data['logo'];
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings();

        $invoice_db = new Invoice_Model_DbTable_Invoices();
        $select = $invoice_db->select()
                ->setIntegrityCheck(false)
                ->from(array('inv' => Zend_Registry::get('dbPrefix') . 'invoice_table'), array())
                ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'up.user_id = inv.user_id', array('billing_zip' => 'up.postalCode', 'billing_address' => 'up.address', 'billing_city' => 'up.city', 'billing_country' => 'up.country', 'billing_state' => 'up.state', 'billing_tel' => 'up.mobile', 'billing_name' => "CONCAT(up.title, ' ', up.firstName, ' ', up.lastName)"))
                ->where('inv.id = ' . $invoice_id);
        $options = $invoice_db->fetchAll($select);
        $userInformation = (!is_array($options)) ? $options->toArray() : $options;


        if ($user_id) {
            $stripeSetting = $api_table->getInfo(11, $user_id);
        } else {
            $stripeSetting = $api_table->getInfo(11, $global_conf['payment_user_id']);
        }

        $mbCheckout = array();
        $findValue = 0;
        while ($findValue < count($stripeSetting)) {
            $rowSet = $stripeSetting[$findValue];
            $fieldName = $rowSet['setting'];
            $fieldValue = $rowSet['value'];
            if (!empty($fieldName)) {
                $worldpayCheckout[$fieldName] = $fieldValue;
            }
            $findValue ++;
        }
        
        
        //checking if in Test Mode or Live Mode
        $testMode = $worldpayCheckout['test_mode'];
        $secretKey = '';
        $publishKey = '';
        
        if($testMode == 0){ //set to Live Mode
            
            if (!array_key_exists('live_secret_key', $worldpayCheckout) OR !array_key_exists('live_publishable_key', $worldpayCheckout)) {
                throw new Exception($translator->translator('payment_stripe_live_secret_key_err', '', 'Paymentgateway'));
            } else {
                $secretKey = $worldpayCheckout['live_secret_key'];
                $publishKey = $worldpayCheckout['live_publishable_key'];
            }
        } else { //set to Test Mode
            if (!array_key_exists('test_secret_key', $worldpayCheckout) OR !array_key_exists('test_publishable_key', $worldpayCheckout)) {
                throw new Exception($translator->translator('payment_stripe_test_key_err', '', 'Paymentgateway'));
            } else {
                $secretKey = $worldpayCheckout['test_secret_key'];
                $publishKey = $worldpayCheckout['test_publishable_key'];
            }
        }
        
        $imageURL = "data/frontImages/gateway/loading_squares.gif";
        $stripeFormHtml = '';
        if($secretKey != "" AND $publishKey != ""){
           $stripeFormHtml = '   
               
                <!-- Required scripts for Stripe Payment -->
                <script type="text/javascript" src="application/modules/Paymentgateway/js/stripe.js"></script>
                <script type="text/javascript" src="application/modules/Paymentgateway/js/script.js"></script>
                <script type="text/javascript">
				
		function msgBack(key){
                    var msg = "";
                    if(key == "payment_stripe_card_err"){
                        msg = "'.$translator->translator('payment_stripe_card_err', '', 'Paymentgateway').'";
                    } else if(key == "payment_stripe_cvc_err"){
                        msg = "'.$translator->translator('payment_stripe_cvc_err', '', 'Paymentgateway').'";
                    } else if(key == "payment_stripe_expire_date_err"){
                        msg = "'.$translator->translator('payment_stripe_expire_date_err', '', 'Paymentgateway').'";
                    } 
                    
                    return msg;
                }
				
				Stripe.setPublishableKey("'. $publishKey .'");</script>
                <!-- Required scripts for Stripe Payment -->
                
                <!-- Form Generation for Stripe Payment Gateway-->
                

                <div id="payment-errors" style="float: left !important; clear:both !important; color: red; font-weight:bolder;"></div>
                <div id="payment-success" style="float: left !important; clear:both !important; color: green; font-weight:bolder;"></div>
                
                
                
                <form method="POST" id="payment-form">

		
                
                <div style="float: left !important; width: 100% !important;">
                    <span class="help-block" style="float: left !important; font-weight: bolder;">You can pay using: Mastercard, Visa, American Express, JCB, Discover, and Diners Club.</span>
                    
                    
                    
                    <div  style="float: left !important; clear:both !important; margin: 10px 0px;">
                    <label>Card Number:</label>
                    <input type="text" size="20" autocomplete="off" class="card-number input-medium" style="margin-left: 50px;">
                    <span class="help-block">Enter the number without spaces or hyphens.</span>
                    </div>
                    
                    <div  style="float: left !important; clear: both !important; margin: 10px 0px;">
                    <label>CVC:</label>
                    <input type="text" size="4" autocomplete="off" class="card-cvc input-mini" style="margin-left: 100px;">
                    </div><br>
                    
                    <div  style="float: left !important; clear:both !important; margin: 10px 0px;">
                    <label>Expiration (MM/YYYY)</label>
                    <input type="text" size="2" class="card-expiry-month input-mini">
                    <span> / </span>
                    <input type="text" size="4" class="card-expiry-year input-mini">
                    </div><br>
                    

                    <input type="hidden" value="'. $pay_from_email .'" id="custEmail">
                    <input type="hidden" value="'. $price .'" id="payAmount">
                    
                    
                </div>
                
		<button type="button" class="btn" id="submitBtn" style="margin: 10px 0px; float: left !important; clear:both !important;">Submit Payment</button>
                <span id="span-loader" style="float:left; margin-left: 20px;"></span>
                </form>
                
                <!-- Form Generation for Stripe Payment Gateway-->';
        }
        
        return $stripeFormHtml;
    }
    
    
    /*Function for Stripe.com Payment Gateway*/

}

?>