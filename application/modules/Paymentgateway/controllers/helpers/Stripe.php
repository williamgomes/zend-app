<?php

class Paymentgateway_Controller_Helper_Stripe {

    
    private $_translator;

    public function __construct() {
        $this->_translator = Zend_Registry::get('translator');
        $this->getAllFiles();
        
    }

    public function getAllFiles() {
        
        $fullPath = APPLICATION_PATH . '/modules/Paymentgateway/lib/Stripe/';
        // This snippet (and some of the curl code) due to the Facebook SDK.
        if (!function_exists('curl_init')) {
            throw new Exception('Stripe needs the CURL PHP extension.');
        }
        if (!function_exists('json_decode')) {
            throw new Exception('Stripe needs the JSON PHP extension.');
        }
        if (!function_exists('mb_detect_encoding')) {
            throw new Exception('Stripe needs the Multibyte String PHP extension.');
        }

        // Stripe singleton
        require_once $fullPath . 'Stripe.php';

        // Utilities
        require_once $fullPath . 'Stripe/Util.php';
        require_once $fullPath . 'Stripe/Util/Set.php';

        // Errors
        require_once $fullPath . 'Stripe/Error.php';
        require_once $fullPath . 'Stripe/ApiError.php';
        require_once $fullPath . 'Stripe/ApiConnectionError.php';
        require_once $fullPath . 'Stripe/AuthenticationError.php';
        require_once $fullPath . 'Stripe/CardError.php';
        require_once $fullPath . 'Stripe/InvalidRequestError.php';
        require_once $fullPath . 'Stripe/RateLimitError.php';

        // Plumbing
        require_once $fullPath . 'Stripe/Object.php';
        require_once $fullPath . 'Stripe/ApiRequestor.php';
        require_once $fullPath . 'Stripe/ApiResource.php';
        require_once $fullPath . 'Stripe/SingletonApiResource.php';
        require_once $fullPath . 'Stripe/AttachedObject.php';
        require_once $fullPath . 'Stripe/List.php';

        // Stripe API Resources
        require_once $fullPath . 'Stripe/Account.php';
        require_once $fullPath . 'Stripe/Card.php';
        require_once $fullPath . 'Stripe/Balance.php';
        require_once $fullPath . 'Stripe/BalanceTransaction.php';
        require_once $fullPath . 'Stripe/Charge.php';
        require_once $fullPath . 'Stripe/Customer.php';
        require_once $fullPath . 'Stripe/Invoice.php';
        require_once $fullPath . 'Stripe/InvoiceItem.php';
        require_once $fullPath . 'Stripe/Plan.php';
        require_once $fullPath . 'Stripe/Subscription.php';
        require_once $fullPath . 'Stripe/Token.php';
        require_once $fullPath . 'Stripe/Coupon.php';
        require_once $fullPath . 'Stripe/Event.php';
        require_once $fullPath . 'Stripe/Transfer.php';
        require_once $fullPath . 'Stripe/Recipient.php';
        require_once $fullPath . 'Stripe/Refund.php';
        require_once $fullPath . 'Stripe/ApplicationFee.php';
        require_once $fullPath . 'Stripe/ApplicationFeeRefund.php';
        
    }

}

?>