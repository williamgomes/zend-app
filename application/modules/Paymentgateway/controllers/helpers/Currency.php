<?php
class Paymentgateway_Controller_Helper_Currency
{
    private $_global_conf;
	
    public function __construct ($global_conf = null)
    {
        $this->_global_conf = ($global_conf) ? $global_conf : Zend_Registry::get('global_conf');       
    }
   
    public function checkCurrency ($currency_locale)
    {  
		if(empty($currency_locale))
		{
			$check = true;	
		}
		else
		{
			if($currency_locale == $this->_global_conf['default_locale'])
			{
				$check = true;
			}
			else
			{
				$currency_helper = new Paymentgateway_Model_DbTable_Currency();
				if($currency_helper->hasCurrency($currency_locale, $this->_global_conf))
				{
					$check = true;
				}
				else
				{
					$check = false;	
				}
			}
		}
		return $check;
    }
	
	public function convert ($currency_locale, $price)
    {
		if(!empty($currency_locale) && ($currency_locale != $this->_global_conf['default_locale']))
		{
			$currency_helper = new Paymentgateway_Model_DbTable_Currency();
			if($currency_helper->hasCurrency($currency_locale, $this->_global_conf))
			{
				$search_params['filter']['logic'] = 'and';
				$search_params['filter']['filters'] =array(
															0 => array( 'field' => 'base_currency', 	'operator' => 'eq', 'value' => $this->_global_conf['default_locale']),
															1 => array( 'field' => 'target_currency', 	'operator' => 'eq', 'value' => $currency_locale),
														);
				$info = $currency_helper->getListInfo(1, $search_params);
				if($info)
				{
					$info_arr	=	(!is_array($info)) ? $info->toArray() : $info;
					$return_price = ($info_arr[0]['base_unit'] / $info_arr[0]['target_unit']) * $price;
					$return_price = round($return_price, 2);
				}
				else
				{
					$return_price = $price;
				}
			}
			else
			{
				$return_price = $price;
			}
		}
		else
		{
			$return_price = $price;	
		}
		return $return_price;
	}
}
?>