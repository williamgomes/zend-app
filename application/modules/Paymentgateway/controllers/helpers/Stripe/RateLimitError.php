<?php

class Paymentgateway_Controller_Helper_Stripe_RateLimitError extends Paymentgateway_Controller_Helper_Stripe_InvalidRequestError
{
  public function __construct($message, $param, $httpStatus=null,
      $httpBody=null, $jsonBody=null
  )
  {
    parent::__construct($message, $httpStatus, $httpBody, $jsonBody);
  }
}
