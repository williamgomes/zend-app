<?php

class Paymentgateway_Controller_Helper_Stripe_CardError extends Paymentgateway_Controller_Helper_Stripe_Error
{
  public function __construct($message, $param, $code, $httpStatus, 
      $httpBody, $jsonBody
  )
  {
    parent::__construct($message, $httpStatus, $httpBody, $jsonBody);
    $this->param = $param;
    $this->code = $code;
  }
}
