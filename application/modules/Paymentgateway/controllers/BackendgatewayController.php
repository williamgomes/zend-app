<?php

class Paymentgateway_BackendgatewayController extends Zend_Controller_Action {

    private $_gatewayForm = null;
    private $_uploadForm = null;
    private $_paypalForm = null;
    private $_2checkoutForm = null;
    private $_moneybookers = null;
    private $_googleCheckout = null;
    private $_bank = null;
    private $_worldpay = null;
    private $_ccAvenue = null;
    private $_paza = null;
    private $_controllerCache;
    private $_webpay = null;
    private $_authorizeNet = null;
    private $user_id = null;
    private $_auth_obj = null;

    public function init() {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;

        $getAction = $this->_request->getActionName();
        if ($getAction != 'uploadfile') {
            //Check Login
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
        }
        //Initialize translator
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        $this->user_id = ($this->view->auth->hasIdentity()) ? $this->view->auth->getIdentity()->user_id : '';
    }

    public function listAction() {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $approve = $this->_request->getParam('approve');

                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');

                $frontendRoute = 'adminrout';
                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                        ), $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_mapper = new Paymentgateway_Model_GatewayMapper();
                    //$buyingSql = new B2b_Controller_Helper_BuyingSql();
                    // $tableColumns = $buyingSql->getBackendList();
                    $view_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace('_', '-', $entry_arr['name']);

                        $entry_arr['primary_file_field_format'] = ($this->view->escape($entry_arr['logo'])) ? 'data/frontImages/gateway/gateway_logo/' . $this->view->escape($entry_arr['logo']) : '';
                        $entry_arr['edit_enable'] = ($this->view->allow(strtolower($entry_arr['name']))) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function upAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $article_id = $this->_request->getPost('id');

        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $articleOrderObj = new Paymentgateway_Controller_Helper_Orders($article_id);
        $returnV = $articleOrderObj->decreaseOrder();
        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $article_id = $this->_request->getPost('id');

        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $articleOrderObj = new Paymentgateway_Controller_Helper_Orders($article_id);
        $returnV = $articleOrderObj->increaseOrder();
        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $article_id_str = $this->_request->getPost('id_arr');
            $order_str = $this->_request->getPost('payment_order_arr');
            $article_id_arr = explode(',', $article_id_str);
            $order_arr = explode(',', $order_str);
            $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
            $checkOrder = $order_numbers->checkOrderNumbers($order_arr);
            if ($checkOrder['status'] == 'err') {
                $json_arr = array('status' => 'err', 'msg' => $checkOrder['msg']);
            } else {
                //Save Menu Order
                $i = 0;
                foreach ($article_id_arr as $article_id) {
                    $articleOrderObj = new Paymentgateway_Controller_Helper_Orders($article_id);
                    $result = $articleOrderObj->saveOrder($order_arr[$i]);
                    if ($result['status'] == 'err') {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                        break;
                    }
                    $i++;
                }
                $msg = $translator->translator("gateway_set_order_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg);
            }
            //Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function addAction() {
        $id = $this->_request->getParam('gateway_id');
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $this->noRenderForAjax();
            $perm = new Paymentgateway_View_Helper_Allow();
            if ($perm->allow()) {
                switch ($id) {
                    case '1':
                        $json_arr = $this->saveGateway($this->getPaypalForm(), $id);
                        break;
                    case '2':
                        $json_arr = $this->saveGateway($this->get2CheckoutForm(), $id);
                        break;
                    case '3':
                        $json_arr = $this->saveGateway($this->getPayzaForm(), $id);
                        break;
                    case '4':
                        $json_arr = $this->saveGateway($this->getMoneybookersForm(), $id);
                        break;
                    case '5':
                        $json_arr = $this->saveGateway($this->getWorldPayForm(), $id);
                        break;
                    case '6':
                        $json_arr = $this->saveGateway($this->getGoogleForm(), $id);
                        break;
                    case '7':
                        $json_arr = $this->saveGateway($this->getBankForm(), $id);
                        break;
                    case '8':
                        $json_arr = $this->saveGateway($this->getCCAvenueForm(), $id);
                        break;
                    case '9':
                        $json_arr = $this->saveGateway($this->getWebPayForm(), $id);
                        break;
                    case '10':
                        $json_arr = $this->saveGateway($this->getAuthorizenet(), $id);
                        break;
                    case '11':
                        $json_arr = $this->saveGateway($this->getStripe(), $id);
                        break;
                    default:
                        $json_arr = array('status' => 'err', 'msg' => $this->_request->getParams());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator("Member_Access_deny_desc"));
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->gatewayForm = $this->_gatewayForm;
        }
    }

    public function setdefaultAction() {
        if ($this->_request->isPost('id')) {
            $this->noRenderForAjax();
            $id = $this->_request->getPost('id');
            $gatewayDbObj = new Paymentgateway_Model_DbTable_Gateway();
            $result = $gatewayDbObj->setDefault($id);
            if ($result['status'] == 'ok') {
                $json_arr = array('status' => 'ok');
            } else {
                $json_arr = array('status' => 'err');
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function editAction() {
        $payment_gw = new Paymentgateway_Controller_Helper_Payment();
        $data = array('payble' => 150, 'invoice' => 1002, 'logo' => 'http://www.eicra.com/images/logo.gif',
            'client_email' => 'payoer@gmail.com', 'quantity' => 2);
        print_r($payment_gw->gettocheckout($data));
        echo '<br /> <br /> <br /> \n\n\r';
        print_r($payment_gw->getPaypal($data));
        echo '<br /> <br /> <br /> \n\n\r';
        print_r($payment_gw->getmoneybookers($data));
        echo '<br /> <br /> <br /> \n\n\r';
        print_r($payment_gw->getgooglecheckout($data));
        echo '<br /> <br /> <br /> \n\n\r';
        //print_r( $payment_gw->getmoneybookers($data));
        print_r($payment_gw->getpayza($data));
        exit;
    }

    public function publishAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id = $this->_request->getPost('id');
            $payment_name = $this->_request->getPost('payment_name');
            $paction = $this->_request->getPost('paction');
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            try {
                if ($active) {
                    if ($this->checkParameters()) {
                        $gatewayMapper = new Paymentgateway_Model_DbTable_GatewayMapper();
                        $json_arr = $gatewayMapper->setStatus($id, $active);
                    } else {
                        $msg = $translator->translator('gateway_list_publish_err', $this->_debug) . $payment_name;
                        $json_arr = array('status' => 'err', 'msg' => $msg, 'active' => $active);
                    }
                } else {
                    $gatewayMapper = new Paymentgateway_Model_DbTable_GatewayMapper();
                    $json_arr = $gatewayMapper->setStatus($id, $active);
                }
            } catch (Exception $e) {
                //$return = false;
                $msg = $e->getMessage();
                $json_arr = array('status' => 'err', 'msg' => $msg, 'active' => $active);
            }
        }

        //Convert To JSON ARRAY
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function paypalAction() {

        $paypalForm = $this->getPaypalForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $paypal_setting = $api_table->getInfo($id, $this->user_id);
        $paypalForm->getElement('user_id')->setValue($this->user_id);
        $this->view->id = $id;
        $this->view->gatewayForm = $paypalForm;
        $this->view->paypal_setting = $paypal_setting;
        $findValue = 0;

        while ($findValue < count($paypal_setting)) {
            $rowSet = $paypal_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $paypalForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function tocheckoutAction() {
        $tocheckoutForm = $this->get2CheckoutForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $tocheckout_setting = $api_table->getInfo($id, $this->user_id);
        $tocheckoutForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $tocheckoutForm;
        $findValue = 0;

        while ($findValue < count($tocheckout_setting)) {
            $rowSet = $tocheckout_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $tocheckoutForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function moneybookersAction() {
        $moneybookersForm = $this->getMoneybookersForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $moneybookers_setting = $api_table->getInfo($id, $this->user_id);
        $moneybookersForm->getElement('user_id')->setValue($this->user_id);
        $this->view->id = $id;

        $this->view->gatewayForm = $moneybookersForm;
        $findValue = 0;
        while ($findValue < count($moneybookers_setting)) {
            $rowSet = $moneybookers_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $moneybookersForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function googlecheckoutAction() {
        $googleCheckout = $this->getGoogleForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $googleCheckout->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $googleCheckout;
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $googleCheckout->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function worldpayAction() {
        $worldPayForm = $this->getWorldPayForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $worldpay_setting = $api_table->getInfo($id, $this->user_id);
        $worldPayForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $worldPayForm;
        $findValue = 0;
        while ($findValue < count($worldpay_setting)) {
            $rowSet = $worldpay_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $worldPayForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function ccavenueAction() {
        $ccAvenueForm = $this->getCCAvenueForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $ccavenue_setting = $api_table->getInfo($id, $this->user_id);
        $ccAvenueForm->getElement('user_id')->setValue($this->user_id);
        $this->view->id = $id;
        $this->view->gatewayForm = $ccAvenueForm;
        $findValue = 0;
        while ($findValue < count($ccavenue_setting)) {
            $rowSet = $ccavenue_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $ccAvenueForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function payzaAction() {
        $pazaForm = $this->getPayzaForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $payza_setting = $api_table->getInfo($id, $this->user_id);
        $pazaForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $pazaForm;
        $findValue = 0;
        while ($findValue < count($payza_setting)) {
            $rowSet = $payza_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $pazaForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function bankAction() {
        $bankForm = $this->getBankForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $bank_setting = $api_table->getInfo($id, $this->user_id);
        $bankForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $bankForm;
        $findValue = 0;
        while ($findValue < count($bank_setting)) {
            $rowSet = $bank_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $bankForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function webpayAction() {
        $webpayForm = $this->getWebPayForm();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $webpay_setting = $api_table->getInfo($id, $this->user_id);
        $webpayForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $webpayForm;
        $findValue = 0;
        while ($findValue < count($webpay_setting)) {
            $rowSet = $webpay_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $webpayForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function stripeAction() {
        $stripeForm = $this->getStripe();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_info = $api_table->getInfo($id, $this->user_id);
        $stripeForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $stripeForm;
        $findValue = 0;
        while ($findValue < count($gateway_info)) {
            $rowSet = $gateway_info[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $stripeForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function authorizenetAction() {
        $authorizenetForm = $this->getAuthorizenet();
        $id = $this->_request->getParam('id');
        $translator = Zend_Registry::get('translator');
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_info = $api_table->getInfo($id, $this->user_id);
        $authorizenetForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;
        $this->view->gatewayForm = $authorizenetForm;
        $findValue = 0;
        while ($findValue < count($gateway_info)) {
            $rowSet = $gateway_info[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $authorizenetForm->$fieldName->setValue($fieldValue);
            }
            $findValue++;
        }
    }

    public function noRenderForAjax() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
    }

    private function saveGateway($gatewayForm, $gateway_id) {
        $translator = Zend_Registry::get('translator');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $json_arr = null;
        try {
            if ($gatewayForm->isValid($this->_request->getPost())) {
                $forms_Vaules = $this->_request->getPost();
                $paymentGw = new Paymentgateway_Model_GatewaySettings ();

                foreach ($forms_Vaules as $key => $val) {
                    $paymentGw->setGateway_id($gateway_id);
                    $paymentGw->setSetting($key);
                    $paymentGw->setValue($val);
                    $paymentGw->setUser_id($user_id);
                    $result = $paymentGw->saveGateway();
                }

                if ($result['status'] == 'ok') {
                    $json_arr = array('status' => 'ok', 'msg' => $translator->translator('gateway_add_success'));
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                }
            } else {
                $validatorMsg = $gatewayForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
        } catch (Exception $e) {
            $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $json_arr;
    }

    private function getPayzaForm() {
        if (empty($this->_paza) || $this->_paza == null) {
            $this->_paza = new Paymentgateway_Form_PayzaForm ();
            return $this->_paza;
        } else {
            return $this->_paza;
        }
    }

    private function getPaypalForm() {
        if (empty($this->_paypalForm) || $this->_paypalForm == null) {
            $this->_paypalForm = new Paymentgateway_Form_PaypalForm();
            return $this->_paypalForm;
        } else {
            return $this->_paypalForm;
        }
    }

    private function get2CheckoutForm() {
        if (empty($this->_2checkoutForm) || $this->_2checkoutForm == null) {
            $this->_2checkoutForm = new Paymentgateway_Form_tocheckoutForm();
            return $this->_2checkoutForm;
        } else {
            return $this->_2checkoutForm;
        }
    }

    private function getWorldPayForm() {
        if (empty($this->_worldpay) || $this->_worldpay == null) {
            $this->_worldpay = new Paymentgateway_Form_WorldpayForm();
            return $this->_worldpay;
        } else {
            return $this->_worldpay;
        }
    }

    private function getGoogleForm() {
        if (empty($this->_googleCheckout) || $this->_googleCheckout == null) {
            $this->_googleCheckout = new Paymentgateway_Form_GoogleCheckoutForm();
            return $this->_googleCheckout;
        } else {
            return $this->_googleCheckout;
        }
    }

    private function getMoneybookersForm() {
        if (empty($this->_moneybookers) || $this->_moneybookers == null) {
            $this->_moneybookers = new Paymentgateway_Form_Moneybookers();
            return $this->_moneybookers;
        } else {
            return $this->_moneybookers;
        }
    }

    private function getCCAvenueForm() {
        if (empty($this->_ccAvenue) || $this->_ccAvenue == null) {
            $this->_ccAvenue = new Paymentgateway_Form_CcAvenueForm();
            return $this->_ccAvenue;
        } else {
            return $this->_ccAvenue;
        }
    }

    private function getBankForm() {
        if (empty($this->_bank) || $this->_bank == null) {
            $this->_bank = new Paymentgateway_Form_BankForm();
            return $this->_bank;
        } else {
            return $this->_bank;
        }
    }

    private function getWebPayForm() {
        if (empty($this->_webpay) || $this->_webpay == null) {
            $this->_webpay = new Paymentgateway_Form_UbaWebPayForm();
            return $this->_webpay;
        } else {
            return $this->_webpay;
        }
    }

    private function getAuthorizenet() {
        if (empty($this->_authorizeNet) || $this->_authorizeNet == null) {
            $this->_authorizeNet = new Paymentgateway_Form_AuthorizeNetForm();
            return $this->_authorizeNet;
        } else {
            return $this->_authorizeNet;
        }
    }

    private function getStripe() {
        if (empty($this->_stripe) || $this->_stripe == null) {
            $this->_stripe = new Paymentgateway_Form_StripeForm();
            return $this->_stripe;
        } else {
            return $this->_stripe;
        }
    }

    private function checkParameters() {
        $id = $this->_request->getPost('id');
        switch ($id) {
            case '1':
                return $this->paypalValidation();
                break;
            case '2':
                return $this->toCheckoutValidation();
                break;
            case '3':
                return $this->payzaValidation();
                break;
            case '4':
                return $this->moneyBookersValidation();
                break;
            case '5':
                return $this->worldPayValidation();
                break;
            case '6':
                return $this->googleCheckoutValidation();
                break;
            case '7':
                return $this->bankValidation();
                break;
            case '8':
                return $this->ccAvenueValidation();
                break;
            case '9':
                return $this->webpayValidation();
                break;
            case '10':
                return $this->authorizenetValidation();
                break;
            case '11':
                return $this->stripeValidation();
                break;
            default:
                throw new Exception('No Valid Payment Gateway is found');
        }
    }

    private function paypalValidation() {
        $id = $this->_request->getParam('id');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Paypal Display Name ';
            return false;
        } else if (!array_key_exists('email', $gwParam)) {
            $this->_debug = 'Paypal Email ';
            return false;
        } else if (!array_key_exists('currency', $gwParam)) {
            $this->_debug = 'Paypal currency ';
            return false;
        } else {
            return true;
        }
    }

    private function toCheckoutValidation() {
        $id = $this->_request->getParam('id');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';


        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = '2Checkout Display Name ';
            return false;
        } else if (!array_key_exists('vendor_number', $gwParam)) {
            $this->_debug = '2Checkout Vendor ID ';
            return false;
        } else if (!array_key_exists('language', $gwParam)) {
            $this->_debug = '2Checkout language ';
            return false;
        } else {
            return true;
        }
    }

    private function worldPayValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'WorldPay Display Name ';
            return false;
        } else if (!array_key_exists('installation_id', $gwParam)) {
            $this->_debug = 'WorldPay Installation ID ';
            return false;
        } else if (!array_key_exists('currency', $gwParam)) {
            $this->_debug = 'WorldPay currency ';
            return false;
        } else {
            return true;
        }
    }

    private function payzaValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Payza Display Name ';
            return false;
        } else if (!array_key_exists('ap_merchant', $gwParam)) {
            $this->_debug = 'Payza ID / Username ';
            return false;
        } else {
            return true;
        }
    }

    private function moneyBookersValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Moneybookers Display Name ';
            return false;
        } else if (!array_key_exists('pay_to_email', $gwParam)) {
            $this->_debug = 'Moneybookers Merchant Email ';
            return false;
        } else if (!array_key_exists('curency', $gwParam)) {
            $this->_debug = 'Moneybookers Currency Code ';
            return false;
        } else {
            return true;
        }
    }

    private function googleCheckoutValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Google Checkout Display Name ';
            return false;
        } else if (!array_key_exists('merchant_id', $gwParam)) {
            $this->_debug = 'Google Checkout Merchant ID ';
            return false;
        } else if (!array_key_exists('item_currency_1', $gwParam)) {
            $this->_debug = 'Google Checkout Currency Code ';
            return false;
        } else {
            return true;
        }
    }

    private function ccAvenueValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'CCAvenue Display Name ';
            return false;
        } else if (!array_key_exists('merchant_id', $gwParam)) {
            $this->_debug = 'CCAvenue Merchant ID ';
            return false;
        } else if (!array_key_exists('merchant_key', $gwParam)) {
            $this->_debug = 'CCAvenue Working Key ';
            return false;
        } else if (!array_key_exists('currency', $gwParam)) {
            $this->_debug = 'CCAvenue Currency Code ';
            return false;
        } else {
            return true;
        }
    }

    private function webpayValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('mercId', $gwParam)) {
            $this->_debug = 'UBA WebPay Merchant ID ';
            return false;
        } else if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'UBA WebPay Display Name ';
            return false;
        } else if (!array_key_exists('currCode', $gwParam)) {
            $this->_debug = 'UBA WebPay currency code ';
            return false;
        } else if (!array_key_exists('prod', $gwParam)) {
            $this->_debug = 'UBA WebPay Product Name ';
            return false;
        } else {
            return true;
        }
    }

    private function bankValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Bank Title ';
            return false;
        } else if (!array_key_exists('bank_details', $gwParam)) {
            $this->_debug = 'Bank Details ';
            return false;
        } else {
            return true;
        }
    }

    private function authorizenetValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('loginid', $gwParam)) {
            $this->_debug = 'Authorize.net Login ID ';
            return false;
        } else if (!array_key_exists('transactionKey', $gwParam)) {
            $this->_debug = 'Authorize.net Transaction Key ';
            return false;
        } else if (!array_key_exists('name', $gwParam)) {
            $this->_debug = 'Authorize.net Display Name';
            return false;
        } else {
            return true;
        }
    }

    private function stripeValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);
        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }

        if (!array_key_exists('test_secret_key', $gwParam)) {
            $this->_debug = 'Test Secret Key ';
            return false;
        } else if (!array_key_exists('test_publishable_key', $gwParam)) {
            $this->_debug = 'Test Publishable Key ';
            return false;
        } else if (!array_key_exists('live_secret_key', $gwParam)) {
            $this->_debug = 'Live Secret Key';
            return false;
        } else if (!array_key_exists('live_publishable_key', $gwParam)) {
            $this->_debug = 'Live Publishable Key';
            return false;
        } else if (!array_key_exists('test_mode', $gwParam)) {
            $this->_debug = 'Testing Mode';
            return false;
        } else {
            return true;
        }
    }

    public function testAction() {
        $test = new Paymentgateway_Controller_Helper_Payment();
        $data = array(
            'payble' => '19.99',
            'invoice' => 'ESL-8520',
            'client_name' => 'Md Nazrul Islam',
            'client_email' => 'client@eicra.com',
        );
        $form = $test->gettocheckout($data);

        echo $form;

        exit;
    }

}

?>