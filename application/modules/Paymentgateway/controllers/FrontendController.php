<?php
class Paymentgateway_FrontendController extends Zend_Controller_Action
{
	private $_translator;
	private $_controllerCache;
	public function init ()
	{
		/* Initialize action controller here */
		$translator = Zend_Registry::get('translator');
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);
		$this->view->setEscape('stripslashes');
		// Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = $cache->getCache();

	}

	public function preDispatch ()
	{

	}

	public function localAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$global_conf = Zend_Registry::get ( 'global_conf' );
		$memory = $global_conf ['memory_management'];
		$posted_data = $this->_request->getParams ();
		$this->view->assign ( 'posted_data', $posted_data );

		if ($this->_request->isPost ()) {

			$encode_params = Zend_Json_Encoder::encode ( $posted_data );
			$encode_auth_obj = Zend_Json_Encoder::encode ( $this->_auth_obj );
			$uniq_id = md5 ( preg_replace ( '/[^a-zA-Z0-9_]/', '_', $this->view->url () . '_' . $encode_params . '_' . $encode_auth_obj ) );
			if (($local_array = $this->_controllerCache->load ( $uniq_id )) === false) {
				$locals = new Zend_Locale ();
				$locales_options = $locals->getLocaleList ();
				$currency = new Zend_Currency ($this->global_local);
				$counter = 0;
				$local_array = array ();
				foreach ( $locales_options as $key => $value ) {
					try {

						$position = strrpos ( $key, "_" );
						if ($position === false) { // note: three equal signs
							continue;
						}

						$locals->setLocale ( $key );
						$region = $locals->getRegion ();
						if (! empty ( $region )) {
							if ($memory == '3'){
								$currency->setLocale ( $key );
								$currency_view = ($currency->getName ()) ? $currency->getSymbol () . '  - ' . $currency->getName () . ' [' . $currency->getShortName () . ']' : $currency->getSymbol () . ' [' . $currency->getShortName () . ']';
								$local_array [$counter] = array (
										'target_currency_id' => $key,
										'target_currency_name' => $currency_view
								);
							}
							else {
								$local_array [$counter] = array (
										'target_currency_id' => $key,
										'target_currency_name' => $key
								);
							}
						}
						$counter ++;
					} catch ( Exception $e ) {
						// I let the process to run although the Exception occer.
					}
				}
				$local_array = array_values($local_array);
				$this->_controllerCache->save ( $local_array, $uniq_id );
			}
			$json_arr = array (
					'status' => 'ok',
					'data_result' => $local_array
			);

			// Convert To JSON ARRAY
			$res_value = Zend_Json_Encoder::encode ( $json_arr );
			$this->_response->setBody ( $res_value );
		}
	}


	public function localetocurrencyAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		try
		{
			if ($this->_request->isPost ())
			{
				$currency_locale	=		$this->_request->getPost ('currency_locale');
				$currency = new Zend_Currency ($currency_locale);
				$currency_arr	=	array('currency_name' => $currency->getName (), 'currency_symbol' => $currency->getSymbol (), 'currency_short_name' => $currency->getShortName ());
				$json_arr = array ( 'status' => 'ok', 'data_result' => $currency_arr );
			}
		}
		catch(Exception $e)
		{
			$json_arr = array ( 'status' => 'err', 'msg' => $e->getMessage() );
		}

		// Convert To JSON ARRAY
		$res_value = Zend_Json_Encoder::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}

}