<?php
class Paymentgateway_MembersController extends Zend_Controller_Action
{
	private $_translator;
	private $_controllerCache;
	private $_auth_obj;
	private $user_id = null;

	public function init()
	{
		Eicra_Global_Variable::getSession ()->returnLink = $this->view->url ();

		$url = $this->view->serverUrl () . $this->view->baseUrl () .
		'/Frontend-Login';

		Eicra_Global_Variable::checkSession ( $this->_response, $url );

		/* Initialize action controller here */
		$this->_translator = Zend_Registry::get ( 'translator' );
		$this->view->assign ( 'translator', $this->_translator );
		$this->view->setEscape ( 'stripslashes' );

		// Initialize Cache

		$cache = new Eicra_View_Helper_Cache ();

		$this->_controllerCache = $cache->getCache ();

		/* Initialize action controller here */

		$getModule = $this->_request->getModuleName ();

		$this->view->assign ( 'getModule', $getModule );

		$getAction = $this->_request->getActionName ();

		$this->view->assign ( 'getAction', $getAction );

		$getController = $this->_request->getControllerName ();

		$this->view->assign ( 'getController', $getController );

		$auth = Zend_Auth::getInstance ();

		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity () : '';

		$this->view->auth = $auth;


	}
	public function preDispatch()
	{
		$template_obj = new Eicra_View_Helper_Template ();

		$template_obj->setFrontendTemplate ();

		$front_template = Zend_Registry::get ( 'front_template' );

		$this->_helper->layout->setLayoutPath (
		APPLICATION_PATH . '/layouts/scripts/' .
		$front_template ['theme_folder'] );
		$this->_helper->layout->setLayout ( $template_obj->getLayout ( true ) );
		$this->user_id = ($this->view->auth->hasIdentity()) ? $this->view->auth->getIdentity()->user_id : '';
	}

	public function listAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
                array(
                        $this->_translator->translator(
                                'members_frontend_breadcrumb_dashboard_title', '', 'Members'),
                        'Members-Dashboard'),
                array(
                        $this->_translator->translator(
                                'members_payment_gateway'),
                        $this->view->url()));
						
		$posted_data = $this->_request->getParams ();
		$this->view->assign ( 'posted_data', $posted_data );

		if ($this->_request->isPost ())
		{

			try
			{

				$this->_helper->layout->disableLayout ();

				$this->_helper->viewRenderer->setNoRender ();

				$approve = $this->_request->getParam ( 'approve' );

				$pageNumber = ($this->_request->getPost ( 'page' )) ? $this->_request->getPost ( 'page' ) : $this->_request->getParam ( 'page' );

				$getViewPageNum = $this->_request->getParam ( 'pageSize' );

				$frontendRoute = 'Members-Payment-Gateway-List/*';

				$posted_data ['browser_url'] = $this->view->url ( array (

						'module' => $this->view->getModule,
						'controller' => $this->view->getController,
						'action' => $this->view->getAction,
						'approve' => $approve,
						'displayble' => $displayble,
						'page' => ($pageNumber == '1' || empty ( $pageNumber )) ? null : $pageNumber
				)
				, $frontendRoute, true );

				$viewPageNumSes = Eicra_Global_Variable::getSession ()->viewPageNum;

				$viewPageNum = (! empty ( $getViewPageNum )) ? $getViewPageNum : $viewPageNumSes;

				Eicra_Global_Variable::getSession ()->viewPageNum = $viewPageNum;

				$encode_params = Zend_Json::encode ( $posted_data );

				$encode_auth_obj = Zend_Json::encode ( $this->_auth_obj );

				$uniq_id = md5 ( preg_replace ( '/[^a-zA-Z0-9_]/', '_', $this->view->url () . '_' . $encode_params . '_' . $encode_auth_obj ) );

				if (($view_datas = $this->_controllerCache->load ( $uniq_id )) === false)
				{

					$list_mapper = new Paymentgateway_Model_GatewayMapper ();
					$view_datas = $list_mapper->fetchAll ( $pageNumber, null, $posted_data );
					$this->_controllerCache->save ( $view_datas, $uniq_id );
				}

				$data_result = array ();

				$total = 0;

				if ($view_datas) {

					$key = 0;

					foreach ( $view_datas as $entry ) {

						$entry_arr = (! is_array ( $entry )) ? $entry->toArray () : $entry;

						$entry_arr = is_array ( $entry_arr ) ? array_map ( 'stripslashes', $entry_arr ) : stripslashes ( $entry_arr );

						$entry_arr ['publish_status_page_name'] = str_replace ( '_', '-', $entry_arr ['name'] );

						$entry_arr ['primary_file_field_format'] = ($this->view->escape ( $entry_arr ['logo'] )) ? 'data/frontImages/gateway/gateway_logo/' . $this->view->escape ( $entry_arr ['logo'] ) : '';

						$entry_arr['edit_enable'] = ($this->view->allow(strtolower($entry_arr['name']))) ? true : false;

						$data_result [$key] = $entry_arr;

						$key ++;
					}

					$total = $view_datas->getTotalItemCount ();
				}

				$json_arr = array (

						'status' => 'ok',

						'data_result' => $data_result,

						'total' => $total,

						'posted_data' => $posted_data
				)
				;
			} catch ( Exception $e ) {

				$json_arr = array (

						'status' => 'err',

						'data_result' => '',

						'msg' => $e->getMessage ()
				)
				;
			}

			// Convert To JSON ARRAY

			$res_value = Zend_Json::encode ( $json_arr );

			$this->_response->setBody ( $res_value );
		}

		/*
		 * $auth = Zend_Auth::getInstance(); $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : ''; $perm = new Paymentgateway_View_Helper_Allow(); if($perm->allow()) { $pageNumber = $this->getRequest()->getParam('page'); $getViewPageNum = $this->getRequest()->getParam('viewPageNum'); $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum; if(empty($getViewPageNum) && empty($viewPageNumSes)) { $viewPageNum = '30'; } else if(!empty($getViewPageNum) && empty($viewPageNumSes)) { $viewPageNum = $getViewPageNum; } else if(empty($getViewPageNum) && !empty($viewPageNumSes)) { $viewPageNum = $viewPageNumSes; } else if(!empty($getViewPageNum) && !empty($viewPageNumSes)) { $viewPageNum = $getViewPageNum; } Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum; $pages = new Paymentgateway_Model_GatewayMapper(); $this->view->datas = $pages->fetchAllGateway($pageNumber); }
		 */
	}

	public function setupAction()
	{


	}

	public function publishAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$payment_name = $this->_request->getPost('payment_name');
			$paction = $this->_request->getPost('paction');
			switch($paction)
			{
			    case 'publish':
			    	$active = '1';
			    	break;
			    case 'unpublish':
			    	$active = '0';
			    	break;
			}
			try
			{
				if ($active){
					if ($this->checkParameters())
					{
						$gatewayMapper = new Paymentgateway_Model_DbTable_GatewayMapper();
						$json_arr = $gatewayMapper->setStatus($id, $active);
					}
					else
					{
						$msg =  $translator->translator('gateway_list_publish_err',$this->_debug) .  $payment_name  ;
						$json_arr = array('status' => 'err','msg' => $msg, 'active' => '0');
					}
				}
				else {
					$gatewayMapper = new Paymentgateway_Model_DbTable_GatewayMapper();
					$json_arr = $gatewayMapper->setStatus($id, $active);
				}

			}
			catch (Exception $e)
			{
				//$return = false;
				$msg = $e->getMessage();
				$json_arr = array('status' => 'err','msg' => $msg, 'active' => $active);
			}

		}

		//Convert To JSON ARRAY
		$res_value = Zend_Json_Encoder::encode($json_arr);
		$this->_response->setBody($res_value);
	}

	public function paypalAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_paypal_setting'), $this->view->url()));
		
		$paypalForm = new Paymentgateway_Form_PaypalForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$paypal_setting = $api_table->getInfo ( $id, $this->user_id );

		$paypalForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $paypalForm;

		$this->view->paypal_setting = $paypal_setting;

		$findValue = 0;

		while ( $findValue < count ( $paypal_setting ) ) {

			$rowSet = $paypal_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$paypalForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function tocheckoutAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_2co_setting'), $this->view->url()));
		
		$tocheckoutForm = new Paymentgateway_Form_tocheckoutForm();;

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$tocheckout_setting = $api_table->getInfo ( $id, $this->user_id );

		$tocheckoutForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $tocheckoutForm;

		$findValue = 0;

		while ( $findValue < count ( $tocheckout_setting ) ) {

			$rowSet = $tocheckout_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$tocheckoutForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function moneybookersAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('permisson_payment_edit_moneybookers'), $this->view->url()));
		
		$moneybookersForm = new Paymentgateway_Form_Moneybookers();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$moneybookers_setting = $api_table->getInfo ( $id, $this->user_id );

		$moneybookersForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $moneybookersForm;

		$findValue = 0;

		while ( $findValue < count ( $moneybookers_setting ) ) {

			$rowSet = $moneybookers_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$moneybookersForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function googlecheckoutAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_google_setting'), $this->view->url()));
		
		$googleCheckout = new Paymentgateway_Form_GoogleCheckoutForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$gateway_setting = $api_table->getInfo ( $id, $this->user_id );

		$googleCheckout->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $googleCheckout;

		$findValue = 0;

		while ( $findValue < count ( $gateway_setting ) ) {

			$rowSet = $gateway_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$googleCheckout->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function worldpayAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_worldpay_setting'), $this->view->url()));
		
		$worldPayForm = new Paymentgateway_Form_WorldpayForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$worldpay_setting = $api_table->getInfo ( $id, $this->user_id );

		$worldPayForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $worldPayForm;

		$findValue = 0;

		while ( $findValue < count ( $worldpay_setting ) ) {

			$rowSet = $worldpay_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$worldPayForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function ccavenueAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_ccavenue_setting'), $this->view->url()));
		
		$ccAvenueForm = new Paymentgateway_Form_CcAvenueForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$ccavenue_setting = $api_table->getInfo ( $id, $this->user_id );

		$ccAvenueForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $ccAvenueForm;

		$findValue = 0;

		while ( $findValue < count ( $ccavenue_setting ) ) {

			$rowSet = $ccavenue_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$ccAvenueForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function payzaAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_payza_setting'), $this->view->url()));
		
		$pazaForm = new Paymentgateway_Form_PayzaForm ();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$payza_setting = $api_table->getInfo ( $id, $this->user_id );

		$pazaForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $pazaForm;

		$findValue = 0;

		while ( $findValue < count ( $payza_setting ) ) {

			$rowSet = $payza_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$pazaForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function bankAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_bank_setting'), $this->view->url()));
		
		$bankForm = new Paymentgateway_Form_BankForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$bank_setting = $api_table->getInfo ( $id, $this->user_id );

		$bankForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $bankForm;

		$findValue = 0;

		while ( $findValue < count ( $bank_setting ) ) {

			$rowSet = $bank_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$bankForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function webpayAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_webpay_setting'), $this->view->url()));
		
		$webpayForm = new Paymentgateway_Form_UbaWebPayForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$gateway_options = new Paymentgateway_Model_DbTable_Gateway ();

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$webpay_setting = $api_table->getInfo ( $id, $this->user_id );

		$webpayForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $webpayForm;

		$findValue = 0;

		while ( $findValue < count ( $webpay_setting ) ) {

			$rowSet = $webpay_setting [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$webpayForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
	public function authorizenetAction()
	{
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_authorizenet_setting'), $this->view->url()));
		
		$authorizenetForm = new Paymentgateway_Form_AuthorizeNetForm();

		$id = $this->_request->getParam ( 'id' );

		$translator = Zend_Registry::get ( 'translator' );

		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

		$gateway_info = $api_table->getInfo ( $id, $this->user_id );

		$authorizenetForm->getElement ( 'user_id' )->setValue ( $this->user_id );

		$this->view->id = $id;

		$this->view->gatewayForm = $authorizenetForm;

		$findValue = 0;

		while ( $findValue < count ( $gateway_info ) ) {

			$rowSet = $gateway_info [$findValue];

			$fieldName = $rowSet ['setting'];

			$fieldValue = $rowSet ['value'];

			if (! empty ( $fieldName )) {

				$authorizenetForm->$fieldName->setValue ( $fieldValue );
			}

			$findValue ++;
		}
	}
    
    public function stripeAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->_translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->_translator->translator('members_payment_gateway'), 'Members-Payment-Gateway-List'), array($this->_translator->translator('payment_gateway_authorizenet_setting'), $this->view->url()));

        $stripeForm = new Paymentgateway_Form_StripeForm();

        $id = $this->_request->getParam('id');

        $translator = Zend_Registry::get('translator');

        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();

        $gateway_info = $api_table->getInfo($id, $this->user_id);

        $stripeForm->getElement('user_id')->setValue($this->user_id);

        $this->view->id = $id;

        $this->view->gatewayForm = $stripeForm;

        $findValue = 0;

        while ($findValue < count($gateway_info)) {

            $rowSet = $gateway_info [$findValue];

            $fieldName = $rowSet ['setting'];

            $fieldValue = $rowSet ['value'];

            if (!empty($fieldName)) {

                $stripeForm->$fieldName->setValue($fieldValue);
            }

            $findValue ++;
        }
    }

    
	private function checkParameters (){
		$id = $this->_request->getPost('id');
		switch ($id){
		    case '1':
		    	return $this->paypalValidation();
		    	break;
		    case '2':
		    	return $this->toCheckoutValidation();
		    	break;
		    case '3':
		    	return $this->payzaValidation();
		    	break;
		    case '4':
		    	return $this->moneyBookersValidation() ;
		    	break;
		    case '5':
		    	return  $this->worldPayValidation() ;
		    	break;
		    case '6':
		    	return  $this->googleCheckoutValidation() ;
		    	break;
		    case '7':
		    	return  $this->bankValidation();
		    	break;
		    case '8':
		    	return  $this->ccAvenueValidation();
		    	break;
		    case '9':
		    	return  $this->webpayValidation();
		    	break;
		    case '10':
		    	return  $this->authorizenetValidation();
		    	break;
            case '11':
                return $this->stripeValidation();
                break;
		    default:
		    	throw new Exception('No Valid Payment Gateway is found');
		}
	}
	private function paypalValidation (){
		$id = $this->_request->getParam('id');
		$auth = Zend_Auth::getInstance ();
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity ()->user_id : '';

		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Paypal Display Name '   ;
			return false;
		}
		else if (!array_key_exists('email', $gwParam)){
			$this->_debug =  'Paypal Email ' ;
			return false;
		}
		else if (!array_key_exists('currency', $gwParam)){
			$this->_debug =  'Paypal currency ';
			return false;
		}
		else {
			return true;
		}
	}
	private function toCheckoutValidation (){
		$id = $this->_request->getParam('id');
		$auth = Zend_Auth::getInstance ();
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity ()->user_id : '';


		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  '2Checkout Display Name '   ;
			return false;
		}
		else if (!array_key_exists('vendor_number', $gwParam)){
			$this->_debug =  '2Checkout Vendor ID ' ;
			return false;
		}
		else if (!array_key_exists('language', $gwParam)){
			$this->_debug =  '2Checkout language ';
			return false;
		}
		else {
			return true;
		}
	}
	private function worldPayValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'WorldPay Display Name '   ;
			return false;
		}
		else if (!array_key_exists('installation_id', $gwParam)){
			$this->_debug =  'WorldPay Installation ID ' ;
			return false;
		}
		else if (!array_key_exists('currency', $gwParam)){
			$this->_debug =  'WorldPay currency ';
			return false;
		}
		else {
			return true;
		}
	}
	private function payzaValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Payza Display Name '   ;
			return false;
		}
		else if (!array_key_exists('ap_merchant', $gwParam)){
			$this->_debug =  'Payza ID / Username ' ;
			return false;
		}
		else {
			return true;
		}
	}
	private function moneyBookersValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Moneybookers Display Name '   ;
			return false;
		}
		else if (!array_key_exists('pay_to_email', $gwParam)){
			$this->_debug =  'Moneybookers Merchant Email ' ;
			return false;
		}
		else if (!array_key_exists('curency', $gwParam)){
			$this->_debug =  'Moneybookers Currency Code ';
			return false;
		}
		else {
			return true;
		}
	}
	private function googleCheckoutValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Google Checkout Display Name '   ;
			return false;
		}
		else if (!array_key_exists('merchant_id', $gwParam)){
			$this->_debug =  'Google Checkout Merchant ID ' ;
			return false;
		}
		else if (!array_key_exists('item_currency_1', $gwParam)){
			$this->_debug =  'Google Checkout Currency Code ';
			return false;
		}
		else {
			return true;
		}
	}
	private function ccAvenueValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'CCAvenue Display Name '   ;
			return false;
		}
		else if (!array_key_exists('merchant_id', $gwParam)){
			$this->_debug =  'CCAvenue Merchant ID ' ;
			return false;
		}
		else if (!array_key_exists('merchant_key', $gwParam)){
			$this->_debug =  'CCAvenue Working Key ';
			return false;
		}
		else if (!array_key_exists('currency', $gwParam)){
			$this->_debug =  'CCAvenue Currency Code ';
			return false;
		}
		else {
			return true;
		}
	}
	private function webpayValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('mercId', $gwParam)){
			$this->_debug =  'UBA WebPay Merchant ID '   ;
			return false;
		}
		else if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'UBA WebPay Display Name ' ;
			return false;
		}
		else if (!array_key_exists('currCode', $gwParam)){
			$this->_debug =  'UBA WebPay currency code ';
			return false;
		}
		else if (!array_key_exists('prod', $gwParam)){
			$this->_debug =  'UBA WebPay Product Name ';
			return false;
		}
		else {
			return true;
		}
	}

	private function bankValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);
		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Bank Title '   ;
			return false;
		}
		else if (!array_key_exists('bank_details', $gwParam)){
			$this->_debug =  'Bank Details ' ;
			return false;
		}
		else {
			return true;
		}
	}

	private function authorizenetValidation (){
		$id = $this->_request->getParam('id');
		$gateway_options = new Paymentgateway_Model_DbTable_Gateway();
		$api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
		$gateway_setting = $api_table->getInfo ($id , $this->user_id);

		/*$fp = fopen('file111111111.txt', 'w');
		fwrite($fp, print_r($gateway_setting, TRUE));
		fclose($fp);*/

		$gwParam = array();
		$findValue = 0 ;
		while ($findValue < count($gateway_setting) ){
			$rowSet = $gateway_setting[$findValue] ;
			$fieldName =   $rowSet ['setting'];
			$fieldValue =  $rowSet ['value'] ;
			if (!empty($fieldName)){
				$gwParam [$fieldName] = $fieldValue ;
			}
			$findValue++;
		}
		if (!array_key_exists('loginid', $gwParam)){
			$this->_debug =  'Authorize.net Login ID '   ;
			return false;
		}
		else if (!array_key_exists('transactionKey', $gwParam)){
			$this->_debug =  'Authorize.net Transaction Key ' ;
			return false;
		}
		else if (!array_key_exists('name', $gwParam)){
			$this->_debug =  'Authorize.net Display Name';
			return false;
		}

		else {
			return true;
		}
	}
    
    
    
    private function stripeValidation() {
        $id = $this->_request->getParam('id');
        $gateway_options = new Paymentgateway_Model_DbTable_Gateway();
        $api_table = new Paymentgateway_Model_DbTable_GatewaySettings ();
        $gateway_setting = $api_table->getInfo($id, $this->user_id);

        /*$fp = fopen('file111111111.txt', 'w');
        fwrite($fp, print_r($gateway_setting, TRUE));
        fclose($fp);*/

        $gwParam = array();
        $findValue = 0;
        while ($findValue < count($gateway_setting)) {
            $rowSet = $gateway_setting[$findValue];
            $fieldName = $rowSet ['setting'];
            $fieldValue = $rowSet ['value'];
            if (!empty($fieldName)) {
                $gwParam [$fieldName] = $fieldValue;
            }
            $findValue++;
        }
        if (!array_key_exists('test_secret_key', $gwParam)) {
            $this->_debug = 'Test Secret Key ';
            return false;
        } else if (!array_key_exists('test_publishable_key', $gwParam)) {
            $this->_debug = 'Test Publishable Key ';
            return false;
        } else if (!array_key_exists('live_secret_key', $gwParam)) {
            $this->_debug = 'Live Secret Key';
            return false;
        } else if (!array_key_exists('live_publishable_key', $gwParam)) {
            $this->_debug = 'Live Publishable Key';
            return false;
        } else if (!array_key_exists('test_mode', $gwParam)) {
            $this->_debug = 'Testing Mode';
            return false;
        } else {
            return true;
        }
    }
}