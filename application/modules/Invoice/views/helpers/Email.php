<?php
class Invoice_View_Helper_Email extends Zend_View_Helper_Abstract 
{	
	private $_translator;
	private $_view;
	
	public function __construct($view = null) 
	{
		$this->_translator =  Zend_Registry::get('translator');
		$this->_view =  ($view) ? $view : new Zend_View();
	}
	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?','invoice_template')
								->limit(1);
		}
		else
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.id = ?',$letter_id)
								->limit(1);
		}
						
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$templates_arr = $row ;
			}
		}
		else
		{
			$templates_arr['templates_desc'] = '';
		}	
	
		foreach($datas as $key=>$value)
		{
			$pattern = '/%'.$key.'%/i';
						
			$templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? preg_replace($pattern, $value, $templates_arr['templates_desc']) : preg_replace($pattern, '&nbsp;', $templates_arr['templates_desc']);
			$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? preg_replace($pattern, $value, $templates_arr['templates_title']) : preg_replace($pattern, '&nbsp;', $templates_arr['templates_title']);
		}
		
		return $templates_arr;
	}
	
	public function generateInvoice($invoice_id)
	{
		try
		{			
			if ($invoice_id)
			{				
				$invoice_db = new Invoice_Model_DbTable_Invoices();
				$member_db = new Members_Model_DbTable_MemberList();
				$invoice_info = $invoice_db->getInfo($invoice_id);				
				
				if($invoice_info)
				{
					switch($invoice_info['status'])
					{
						case '0' :
							$invoice_info['payment_status'] = '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#FF9900">'.$this->_translator->translator('common_cancelled_language').'</h1>';
						break;
						case '1' :
							$invoice_info['payment_status'] = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#006600">'.$this->_translator->translator('common_paid_language').'</h1>';
						break;
						case '2' :
							$invoice_info['payment_status'] = '<h1 style="border:1px solid #DF8F8F; background-color:#FFCECE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#CC0000;">'.$this->_translator->translator('common_unpaid_language').'</h1>';	
						break;
					}
					$member_info = $member_db->getMemberInfo($invoice_info['user_id']);
					$invoice_info['invoice_id'] = $invoice_id;
					$invoice_info['invoiceToEmail'] = $member_info['username'];
					
					$return = array('status' => 'ok','invoice_arr' => $invoice_info);
				}
				else
				{
					$return = array('status' => 'err');
				}
			}
			else
			{
				$return = array('status' => 'err');
			}
		}
		catch(Exception $e)
		{
			$return = array('status' => 'err','msg' => $e->getMessage());
		}
		return 	$return;	
	}
	
	public function generateInvoiceLater($invoice_id)
	{
		try
		{
			$invoice_db = new Invoice_Model_DbTable_Invoices();
			$invoice_info = $invoice_db->getInfo($invoice_id);
						
			$invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
			$invoice_item_info	=	$invoice_item_db->getInvoiceItems($invoice_id);
			
			$invoice_later_arr = array();
			
			$mem_db = new Members_Model_DbTable_MemberList();
			$mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);
			
			$global_conf = Zend_Registry::get('global_conf');
			$currency = new Zend_Currency($global_conf['default_locale']);
			$currencySymbol = $currency->getSymbol(); 
			$currencyShortName = $currency->getShortName();
			
			switch($invoice_info['status'])
			{
				case '1':								
					$action_name = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#006600">'.$this->_translator->translator("common_paid_language").'</h1>';
					break;
				case '2':
					$action_name = '<h1 style="border:1px solid #DF8F8F; background-color:#FFCECE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#CC0000;">'.$this->_translator->translator("common_unpaid_language").'</h1>';
					break;
				case '0':
					$action_name =  '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#FF9900">'.$this->_translator->translator("common_cancelled_language").'</h1>';
					break;
			}
			
			$invoice_later_arr[Eicra_File_Constants::INVOICE_ID] = $invoice_info['id'];
			$invoice_later_arr[Eicra_File_Constants::INVOICE_SUBJECT] = $invoice_info['invoice_subject'];
			$invoice_later_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = $action_name;
			$invoice_later_arr[Eicra_File_Constants::INVOICE_STATUS] = $invoice_later_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS];
			$invoice_later_arr[Eicra_File_Constants::INVOICE_TO] = $this->_view->escape($mem_info['title']).' '.$this->_view->escape($mem_info['firstName']).' '.$this->_view->escape($mem_info['lastName']).'<br />'.$this->_view->escape($mem_info['mobile']).'<br />'.$this->_view->escape($mem_info['city']).', '.$this->_view->escape($mem_info['state']).', '.$this->_view->escape($mem_info['postalCode']).'<br />'.$this->_view->escape($mem_info['country_name']).'<BR />'.$this->_view->escape($mem_info['username']);
			$invoice_later_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
			$invoice_later_arr[Eicra_File_Constants::INVOICE_LOGO] = $this->_view->escape($global_conf['payment_logo_url']);
			$invoice_later_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]	= $this->_view->escape($invoice_info['invoice_create_date']);
			$invoice_later_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y",strtotime($invoice_later_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
			$invoice_later_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
			
			//invoice_later_arr assigning started		
			$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
												<tbody>
												<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
													<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>'.$this->_translator->translator("invoice_send_email_desc_title").'</strong></td>
													<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>'.$this->_translator->translator("invoice_send_email_amount_title").'</strong></td>
												</tr>';
			if($invoice_item_info)
			{
				foreach($invoice_item_info as $invoice_item_info_key => $dataInfo)
				{
					$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= $dataInfo['item_details'];
				}
			}
			$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
							<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">
							'.$this->_translator->translator("invoice_send_email_total_title").'</div>
							</td>
							<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>'.$currencySymbol.' '.($invoice_info['total']+$invoice_info['tax']).' '.$currencyShortName.'</strong></td>
						</tr>';	
			$services_charge = 0;
			if(!empty($invoice_info['service_charge']))
			{
				$services_charge = $invoice_info['service_charge'];
				$services_charge_margine = Settings_Service_Price::getMargine('4');
				$services_charge_margine_show	=	(preg_match("/%/i", $services_charge_margine)) ? $currencySymbol.' '.$services_charge.' '.$services_charge_margine : $services_charge_margine;	
				$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("invoice_send_email_service_charge").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$services_charge_margine_show.'</strong></td>'.
				'</tr>';
			}
			
			$now_payable = 0;
			$deposit_charge = 0;
			if(!empty($invoice_info['deposit_charge']))
			{	
				$deposit_charge		= $invoice_info['deposit_charge'];
				$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("invoice_send_email_deposit_charge", Settings_Service_Price::getMargine('5')).'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($deposit_charge.$currencyShortName, 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
				'</tr>';
				
				$now_payable	=	$invoice_info['now_payable'];
				$now_payable_paid_status = ($invoice_info['now_payable_paid_status'] == '1') ? $this->_translator->translator("invoice_list_invoice_now_payable_paid_language") :  $this->_translator->translator("invoice_list_invoice_now_payable_unpaid_language");
				$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">'.$this->_translator->translator("invoice_send_email_deposit_payable").'</div></td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($now_payable, 2, '.', ',').' '.$currencyShortName.' ('.$now_payable_paid_status.')</strong></td>'.
				'</tr>';
			}
			
			if(!empty($invoice_info['service_charge']))
			{	
				$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("invoice_send_email_grand_total").'</td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format(($invoice_info['total']+$invoice_info['tax']+$services_charge), 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
				'</tr>';
			}	
			
			if(!empty($invoice_info['deposit_charge']) && !empty($now_payable))
			{
				$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">'.
					'<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right;" height="40" colspan="2"><strong>'.$this->_translator->translator("invoice_send_email_later_payable", $currencySymbol.' '.number_format((($invoice_info['total']+$invoice_info['tax']+$services_charge) - $now_payable), 2, '.', ',').' '.$currencyShortName).'</strong></td>'.
				'</tr>';
			}
			
			$invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
																	</table>';
			$return = array('status' => 'ok','invoice_arr' => $invoice_later_arr, 'invoice_info' => $invoice_info);
		}
		catch(Exception $e)
		{
			$return = array('status' => 'err','msg' => $e->getMessage());
		}
		return $return;
	}
	
	public function sendMail($datas, $attach_file_arr = null)
	{		
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  
		  $template_arr = $this->getLetterBody($datas,$datas['letter_id']);
		  
		  $invoice_desc	=	$template_arr['templates_desc'];
		
		  $newsletter = stripslashes($invoice_desc);
		  $subject =  strip_tags(stripslashes($template_arr['templates_title']));
		  
		  $email_name_admin = $global_conf['global_email'];				  
		  $email_name = ($datas[Eicra_File_Constants::INVOICE_TO_EMAIL] && ($datas[Eicra_File_Constants::INVOICE_TO_EMAIL] != $email_name_admin)) ? $datas[Eicra_File_Constants::INVOICE_TO_EMAIL] : null;
		  $email_name .= ($datas[Eicra_File_Constants::MARCHENT_EMAIL_ID]) ? ( ($email_name) ? ','.$datas[Eicra_File_Constants::MARCHENT_EMAIL_ID] : $datas[Eicra_File_Constants::MARCHENT_EMAIL_ID] ) : '';
		 
		  $mailing = new Zend_Mail();		
		  $mailing->setFrom($email_name_admin, $global_conf['site_name']);
		  $mailing->addTo($email_name_admin, $global_conf['site_name']);
		  if($email_name)
		  {
			$email_name_arr = explode(',', $email_name); 
			foreach($email_name_arr as $email_name_key => $email_name_value)
			{
		  		$mailing->addBcc($email_name_value);
			}
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);
			$return = array('status' => 'ok', 'invoice_desc' => $template_arr);
		  }
		  catch (Exception $e) 
		  {			  	
		  	try
		  	{
				$mailing->send();
				$return = array('status' => 'ok', 'invoice_desc' => $template_arr);
			}
			catch (Exception $e1) 
			{
				$return = array('status' => 'err', 'msg' => $e1->getMessage().'<br />'.$e->getMessage(), 'invoice_desc' => $template_arr);
			}
		  } 		
		return $return;
	}	
	
	public function sendCommonMail($datas, $attach_file_arr = null)
	{		
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  
		  $template_arr = $this->getLetterBody($datas,$datas['letter_id']);
		  
		  $invoice_desc	=	$template_arr['templates_desc'].$datas[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA];
		
		  $newsletter = stripslashes($invoice_desc);
		  $subject =  ($datas[Eicra_File_Constants::INVOICE_SUBJECT]) ? $datas[Eicra_File_Constants::INVOICE_SUBJECT] : strip_tags(stripslashes($template_arr['templates_title']));
		  
		  $email_name_admin = $global_conf['global_email'];				  
		  $email_name = $datas[Eicra_File_Constants::INVOICE_TO_EMAIL];
		  $mailing = new Zend_Mail();		
		  $mailing->setFrom($email_name_admin, $global_conf['site_name']);
		  if($email_name)
		  {
		  	$mailing->addTo($email_name, $global_conf['site_name']);
		  }
		  if($datas[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL])
		  {
		 	 $mailing->addBcc($datas[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL]);
		  }
		  if($datas[Eicra_File_Constants::INVOICE_TO_CC_EMAIL])
		  {
			  $mailing->addCc($datas[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]);
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);
			$return = array('status' => 'ok', 'invoice_desc' => $template_arr);
		  }
		  catch (Exception $e) 
		  {			  	
		  	try
		  	{
				$mailing->send();
				$return = array('status' => 'ok', 'invoice_desc' => $template_arr);
			}
			catch (Exception $e) 
			{
				$return = array('status' => 'err', 'msg' => $e->getMessage(), 'invoice_desc' => $template_arr);
			}
		  } 		
		return $return;
	}
}

