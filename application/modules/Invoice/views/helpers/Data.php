<?php
class Invoice_View_Helper_Data extends Zend_View_Helper_Abstract 
{
	public function data($payble = null, $invoice = null, $client_email = null, $quantity = 1, $payment_info, $view) 
	{		
		$data = array ('payble' => round($payble, 2), 'payment_user_id'	=>  $payment_info['payment_user_id']  , 'invoice' => $invoice , 'logo' => $view->serverUrl().$view->baseUrl().'/data/frontImages/gateway/gateway_logo/'.$payment_info['logo'], 'client_email' => $client_email , 'quantity' => $quantity );
		
		$payment_function	=	'get'.$payment_info['name'];
		$payment_helper	=	new	Paymentgateway_Controller_Helper_Payment();
		$payment_data	=	$payment_helper->$payment_function($data, $payment_info['payment_user_id']);
		return $payment_data;
	}	
}