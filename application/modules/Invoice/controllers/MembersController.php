<?php
//Members Frontend controller
class Invoice_MembersController extends Zend_Controller_Action
{
	private $_page_id;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	
    public function init()
    {	
		$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
		Eicra_Global_Variable::checkSession($this->_response,$url);
		Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();
		
		/* Initialize Template */
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout(true));
		$this->translator =	Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->translator);	
	
        /* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();			
		
		
		$this->view->menu_id = $this->_request->getParam('menu_id');
		$getAction = $this->_request->getActionName();
		
			
		if($this->_request->getParam('menu_id'))
		{			
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
			
    }
	
	public function preDispatch() 
	{	
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;
		$this->view->setEscape('stripslashes');					
	}
	
	
	public function listAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('invoice_frontend_list_page_name'), $this->view->url()));
				
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$status = $this->getRequest()->getParam('status');				
									
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontendRout =  'Members-Invoice-Item-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'status' => $status, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRout,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Invoice_Model_InvoicesListMapper();
					$list_datas =  $list_mapper->fetchAll($pageNumber, $status, $posted_data);
					
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;	
						$status_class = new Invoice_Controller_Helper_Status($this->translator);			
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_invoice'] = str_replace('_', '-', $entry_arr['id']);	
								unset($entry_arr['invoice_desc']); 
								$entry_arr['invoice_create_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['invoice_create_date']));
								$entry_arr['total_amount_format']				=	$this->view->numbers(number_format($entry_arr['total_amount'], 2, '.', ','));
								$entry_arr['now_payable_format']				=	$this->view->numbers(number_format($entry_arr['now_payable'], 2, '.', ','));
								$entry_arr['invoice_status_format']	=	$status_class->getStatus($entry_arr['status']);
								$entry_arr['invoice_now_paid_status_format']	=	$status_class->getNowPaidStatus();
								$entry_arr['invoice_now_unpaid_status_format']	=	$status_class->getNowUnPaidStatus();							
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	public function editAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('invoice_frontend_list_page_name'), 'Members-Invoice-Item-List'), array($this->translator->translator('invoice_edit_page_name'), $this->view->url()));
		
		$invoice_id = $this->_request->getParam('id');
		$invoiceForm = new Invoice_Form_InvoiceForm();
		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();			
			
			if($invoiceForm->isValid($this->_request->getPost())) 
			{	
				$invoices = new Invoice_Model_Invoices($invoiceForm->getValues());
				$invoice_id = $this->_request->getPost('invoice_id');
				$invoices->setId($invoice_id);
					
				$perm = new Invoice_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $invoices->saveInvoices();
					if($result['status'] == 'ok')
					{
						$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						$msg = $this->translator->translator("invoice_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $result['msg'];
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $this->translator->translator("invoice_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}
			
			}
			else
			{
				$validatorMsg = $invoiceForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$mem_db = new Members_Model_DbTable_MemberList();
			$invoiceData = new Invoice_Model_DbTable_Invoices();
			$invoice_info = $invoiceData->getInfo($invoice_id);	
			if($invoice_info)
			{			
				$invoiceForm->populate($invoice_info);	
				$mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);
				
				switch($invoice_info['status'])
				{				
					case '0' :
						$this->view->invoice_status  = $this->translator->translator('common_cancelled_language');	
					break;
					case '1' :
						$this->view->invoice_status  = $this->translator->translator('common_paid_language');	
					break;
					case '2' :
						$this->view->invoice_status  = $this->translator->translator('common_unpaid_language');	
					break;
				}
				
				$this->view->invoice_info = $invoice_info;
				$this->view->invoice_id = $invoice_id;
				$this->view->invoiceForm = $invoiceForm;
				$this->view->mem_info = $mem_info;	
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Invoice-Item-List', array());
			}
		}	
    }
	
	public function detailsAction()
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('invoice_frontend_list_page_name'), 'Members-Invoice-Item-List'), array($this->translator->translator('invoice_details_invoice_page_name'), $this->view->url()));
		
		$invoice_id = $this->_request->getParam('invoice_id');
		if($invoice_id)
		{
			$invoice_db = new Invoice_Model_DbTable_Invoices();
			$invoice_arr = $invoice_db->getInfo($invoice_id);
			if($invoice_arr)
			{
				$invoice_arr['invoice_id'] = $invoice_id;
			
				switch($invoice_arr['status'])
				{
					case '0' :
						$invoice_arr['payment_status'] = $this->translator->translator('common_cancelled_language');	
					break;
					case '1' :
						$invoice_arr['payment_status'] = $this->translator->translator('common_paid_language');	
					break;
					case '2' :
						$invoice_arr['payment_status'] = $this->translator->translator('common_unpaid_language');	
					break;
				}			
				$this->view->assign('invoice_arr', $invoice_arr);
			}
		}
		if(!$invoice_arr)
		{
			$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Invoice-Item-List', array());	
		}
	}
}