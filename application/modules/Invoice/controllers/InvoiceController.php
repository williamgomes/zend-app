<?php

class Invoice_InvoiceController extends Zend_Controller_Action {

    private $_translator;
    private $_controllerCache;

    public function init() {
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
        //Eicra_Global_Variable::checkSession($this->_response,$url);
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template;
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath(APPLICATION_PATH . '/layouts/scripts/' . $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout());
    }

    public function servicesAction() {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            $user_id = $globalIdentity->user_id;
            $invoice_db = new Flight_Model_DbTable_Invoices();
            $invoice_info = $invoice_db->getInvoiceInfo($user_id);
            $this->view->assign('invoice_info', $invoice_info);
        }
    }

    public function printAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $invoice_id = $this->_request->getParam('invoice_id');
        if ($invoice_id) {
            $invoice_db = new Invoice_Model_DbTable_Invoices();
            $invoice_arr = $invoice_db->getInfo($invoice_id);
            if ($invoice_arr) {
                switch ($invoice_arr['status']) {
                    case '0' :
                        $invoice_arr['payment_status'] = '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator('common_cancelled_language') . '</h1>';
                        break;
                    case '1' :
                        $invoice_arr['payment_status'] = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator('common_paid_language') . '</h1>';
                        break;
                    case '2' :
                        $invoice_arr['payment_status'] = '<h1 style="border:1px solid #DF8F8F; background-color:#FFCECE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator('common_unpaid_language') . '</h1>';
                        break;
                }
                $invoice_desc = str_replace('%' . Eicra_File_Constants::INVOICE_ID . '%', $invoice_id, str_replace('%' . Eicra_File_Constants::INVOICE_PAYMENT_STATUS . '%', $invoice_arr['payment_status'], $invoice_arr['invoice_desc']));
                $body = '<body onload="print();"><a href="javascript:print();">' . $this->_translator->translator('common_print') . '</a><br />' . stripslashes($invoice_desc) . '</body>';
            } else {
                $body = $this->_translator->translator("invoice_no_invoice_found");
            }
        } else {
            $body = $this->_translator->translator("invoice_no_invoice_found");
        }
        $this->_response->setBody($body);
    }

    public function createAction() {
        $invoice_arr = Eicra_Global_Variable::getSession()->invoice_arr;
        
        if (!empty($invoice_arr)) {
            $global_conf = Zend_Registry::get('global_conf');
            $currency = new Zend_Currency($global_conf['default_locale']);
            $currencySymbol = $currency->getSymbol();
            $currencyShortName = $currency->getShortName();
            $invoices = new Invoice_Model_Invoices();
            $invoices->setInvoice_subject($invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT]);
            $invoices->setInvoice_desc($invoice_arr[Eicra_File_Constants::INVOICE_DESC]);
            $invoices->setInvoice_create_date(date("Y-m-d h:i:s"));
            $invoices->setInvoice_update_date(date("Y-m-d h:i:s"));
            $invoices->setSub_total($invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT]);
            $invoices->setTotal($invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT]);
            $invoices->setService_charge($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE]);
            $invoices->setDeposit_charge($invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE]);
            $invoices->setNow_payable($invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE]);
            $invoices->setTax($invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX]);
            if ($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] && $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO]['name']) {
                $invoices->setPayment_method($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO]['name']);
            }

            if ($invoice_arr[Eicra_File_Constants::INVOICE_USER_ID] && $invoice_arr[Eicra_File_Constants::INVOICE_USER_ID] != '0') {
                $invoices->setUser_id($invoice_arr[Eicra_File_Constants::INVOICE_USER_ID]);
            }
            $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID] = ($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID] && !empty($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID])) ? $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID] : $global_conf['payment_user_id'];
            $invoices->setPayment_user_id($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]);

            $invoices->setModule_name($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
            $invoices->setCreate_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION]);
            $invoices->setUpdate_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION]);
            $invoices->setDelete_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION]);
            $invoices->setQuery_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION]);
            $invoices->setPaid_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION]);
            $invoices->setUnpaid_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION]);
            $invoices->setCancel_action($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION]);
            $invoices->setStatus('2');

            $result_arr = $invoices->saveInvoices();
            if ($result_arr['status'] == 'ok') {
                $mem_db = new Members_Model_DbTable_MemberList();
                $mem_info = $mem_db->getMemberInfo($invoices->getUser_id());
                foreach ($mem_info as $globalIdentity_key => $globalIdentity_value) {
                    $invoice_arr[$globalIdentity_key] = $globalIdentity_value;
                }
                $invoice_arr[Eicra_File_Constants::INVOICE_ID] = $result_arr['id'];
                $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = '<h1  style="border:1px solid #DF8F8F; background-color:#FFCECE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#FA1919;">' . $this->_translator->translator('common_unpaid_language') . '</h1>';
                $invoice_arr[Eicra_File_Constants::INVOICE_STATUS] = '2';
                $invoice_arr[Eicra_File_Constants::INVOICE_USER_ID] = $invoices->getUser_id();
                $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoices->getTotal() + $invoices->getTax() + $invoices->getService_charge()), 2, '.', ',') . ' ' . $currencyShortName;

                $template_id_field = 'default_template_id';
                $settings_db = new Invoice_Model_DbTable_Setting();
                $template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                if ($template_info) {
                    $invoice_arr['letter_id'] = ($template_info && $template_info[$template_id_field]) ? $template_info[$template_id_field] : null;
                }

                $this->view->assign('invoice_arr', $invoice_arr);

                $this->setOrders($invoice_arr);
                $this->setInvoiceItems($invoice_arr);
                $this->setInvoiceServices($invoice_arr);

                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                Eicra_Global_Variable::getSession()->invoice_email_arr = $invoice_arr;

                Eicra_Global_Variable::getSession()->cart_result = null;
                Eicra_Global_Variable::getSession()->invoice_arr = null;
                Eicra_Global_Variable::getSession()->returnLink = null;
            } else {
                $this->view->assign('msg', $result_arr['msg']);
            }
        }
    }

    public function emailAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->_request->isPost()) {
            $send_mail = $this->_request->getPost('send_mail');
            if (strtolower($send_mail) == 'yes') {
                try {
                    $invoice_arr = (Eicra_Global_Variable::getSession()->invoice_email_arr) ? Eicra_Global_Variable::getSession()->invoice_email_arr : '';
                    if (!empty($invoice_arr)) {
                        $email_class = new Invoice_View_Helper_Email($this->view);
                        $send_mail_result = $email_class->sendMail($invoice_arr, null);
                        if ($send_mail_result['status'] == 'ok') {
                            $json_arr = array('status' => 'ok', 'msg' => $this->_translator->translator('invoice_create_page_email_processing_success'));
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $send_mail_result['msg']);
                        }
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
                Eicra_Global_Variable::getSession()->invoice_email_arr = '';
                Eicra_Global_Variable::getSession()->returnLink = '';
                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    private function setInvoiceServices($invoice_arr) {
        try {
            if ($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]) {
                $module_arr = explode(',', $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                foreach ($module_arr as $$module_key => $module) {
                    $services_class_name = $module . '_Service_Invoice';
                    if (class_exists($services_class_name)) {
                        $services = new $services_class_name($invoice_arr);
                        if ($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION]) {
                            $method_arr = explode(',', $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION]);
                            foreach ($method_arr as $key => $method) {
                                if (method_exists($services, $method)) {
                                    $services->$method();
                                }
                            }
                        }
                    }
                }
                $return_arr = array('status' => 'ok');
            }
        } catch (Exception $e) {
            $return_arr = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    private function setOrders($invoice_arr) {
        try {
            $orders = new Invoice_Model_Orders();
            $orders->setOrder_date(date("Y-m-d h:i:s"));
            $orders->setOrder_amount($invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT]);
            if ($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] && $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO]['name']) {
                $orders->setPayment_method($invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO]['name']);
            }
            $orders->setInvoice_id($invoice_arr[Eicra_File_Constants::INVOICE_ID]);
            $orders->setIpaddress($_SERVER['REMOTE_ADDR']);
            $orders->setStatus('2');
            $result = $orders->saveOrders();
            if ($result['status'] == 'err') {
                $return = array('status' => 'err', 'msg' => $result['msg']);
            } else {
                $return = array('status' => 'ok');
            }
        } catch (Exception $e) {
            $return = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $return;
    }

    private function setInvoiceItems($invoice_arr) {
        try {
            $invoiceItems = new Invoice_Model_InvoiceItems();
            $invoiceItems->setInvoice_id($invoice_arr[Eicra_File_Constants::INVOICE_ID]);

            if ($invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT]) {
                foreach ($invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] as $invoiceItem) {
                    $invoiceItems->setObject_value($invoiceItem['object_value']);
                    $invoiceItems->setItem_details($invoiceItem['item_details']);
                    $invoiceItems->setItem_total($invoiceItem['item_total']);
                    $invoiceItems->setItem_tax($invoiceItem['item_tax']);

                    $result = $invoiceItems->saveInvoiceItems();

                    if ($result['status'] == 'err') {
                        break;
                        echo $result['msg'];
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    private function getLetterBody($datas, $letter_id = null) {
        //DB Connection
        $conn = Zend_Registry::get('msqli_connection');
        $conn->getConnection();
        if (empty($letter_id)) {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.templates_page = ?', 'invoice_template')
                    ->limit(1);
        } else {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.id = ?', $letter_id)
                    ->limit(1);
        }

        $rs = $select->query()->fetchAll();
        if ($rs) {
            foreach ($rs as $row) {
                $templates_arr = $row;
            }
        } else {
            $templates_arr['templates_desc'] = '';
        }
        foreach ($datas as $key => $value) {
            $templates_arr['templates_desc'] = (!empty($value)) ? str_replace('%' . $key . '%', $value, $templates_arr['templates_desc']) : str_replace('%' . $key . '%', '&nbsp;', $templates_arr['templates_desc']);
        }
        return $templates_arr;
    }

    public function quoteAction() {
        $invoice_arr = Eicra_Global_Variable::getSession()->invoice_arr;
        if ($invoice_arr) {
            $quotes = new Invoice_Model_Quotes();
            $quotes->setQuote_customer_notes($invoice_arr['quote_customer_notes']);
            $quotes->setInvoice_subject($invoice_arr['invoice_subject']);
            $quotes->setInvoice_desc($invoice_arr['invoice_desc']);
            $quotes->setQuote_create_date(date("Y-m-d h:i:s"));
            $quotes->setSub_total($invoice_arr['total_amount']);
            $quotes->setTotal($invoice_arr['total_amount']);
            $quotes->setTax($invoice_arr['total_tax']);
            $quotes->setPayment_method($invoice_arr['payment_info']['name']);
            $quotes->setStage('1');
            $result = $quotes->saveQuotes();

            if ($result['status'] == 'ok') {
                $invoice_arr['quote_id'] = $result['id'];
                $this->setQuoteItems($invoice_arr);
                $return = array('status' => 'ok', 'msg' => '');
                Eicra_Global_Variable::getSession()->invoice_arr = null;
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            } else {
                $return = array('status' => 'err', 'msg' => $result['msg']);
            }
        } else {
            $return = array('status' => 'err', 'msg' => $result['msg']);
        }
        $this->view->assign('return_arr', $return);
    }

    public function frontpayAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        Eicra_Global_Variable::getSession()->returnLink = null;
        if ($this->_request->isPost()) {
            try {
                $payment_id = $this->_request->getPost('payment_id');
                $invoice_id = $this->_request->getPost('invoice_id');

                $payment_db = new Paymentgateway_Model_DbTable_Gateway();
                $payment_info = $payment_db->getInfo($payment_id);
                $invoice_db = new Invoice_Model_DbTable_Invoices();
                $invoice_info = $invoice_db->getInfo($invoice_id);
                $payment_info['payment_user_id'] = $invoice_info['payment_user_id'];
                $mem_db = new Members_Model_DbTable_MemberList();
                $mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);

                //Set Data variables
                $payble = (empty($invoice_info['now_payable'])) ? ( ($invoice_info['tax'] + $invoice_info['total'] + $invoice_info['service_charge']) - $invoice_info['now_payable'] ) : ($invoice_info['now_payable']);
                $invoice = (int) $invoice_id;
                $client_email = $mem_info['username'];
                $quantity = 1;
                switch ($payment_info['method']) {
                    case '1':
                        $input_type = 'link';
                        break;
                    case '2':
                        $input_type = 'html';
                        break;
                    default:
                        $input_type = 'link';
                        break;
                }

                $payment_data = $this->view->data($payble, $invoice, $client_email, $quantity, $payment_info, $this->view);
                $json_arr = array('status' => 'ok', 'payment_data' => $payment_data, 'payment_info' => $payment_info, 'input_type' => $input_type);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function setQuoteItems($invoice_arr) {
        try {
            $invoiceItems = new Invoice_Model_QuoteItems();
            $invoiceItems->setQuote_id($invoice_arr['quote_id']);

            foreach ($invoice_arr['invoice_items'] as $invoiceItem) {
                $invoiceItems->setObject_value($invoiceItem['object_value']);
                $invoiceItems->setItem_details($invoiceItem['item_details']);
                $invoiceItems->setItem_total($invoiceItem['item_total']);
                $invoiceItems->setItem_tax($invoiceItem['item_tax']);

                $result = $invoiceItems->saveQuoteItems();

                if ($result['status'] == 'err') {
                    break;
                    echo $result['msg'];
                    exit;
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

}

?>