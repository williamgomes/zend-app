<?php
class Invoice_Controller_Helper_Marchent
{
	private $_global_conf;
	
	public function __construct($global_conf = null) 
	{
		$this->_global_conf 	=  ($global_conf) ? $global_conf : Zend_Registry::get('global_conf');
	}
	
	public function getPaymentType() 
	{		
		return $this->_global_conf['payment_system_type'];
	}
	
	public function isMarchentPaymentEnable($info_arr)
	{
		if($info_arr['item_count'] == '1' && ( $this->_global_conf['payment_system_type'] == '2' || $this->_global_conf['payment_system_type'] == '3' ) && ($this->_global_conf['payment_system'] == 'On'))
		{
			if($info_arr['dataInfo'] && $info_arr['dataInfo']['item'] && $info_arr['dataInfo']['item']['eCommerce'] && $info_arr['dataInfo']['item']['eCommerce'] == 'YES')
			{
				$result = true;
			}
			else
			{
				$result = false;
			}
		}
		else
		{
			$result = false;
		}
		return $result;
	}
}
?>