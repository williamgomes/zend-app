<?php
class Invoice_Controller_Helper_Status
{
	private $_paid;
	private $_unpaid;
	private	$_cancelled;
	private	$_now_payable_paid;
	private	$_now_payable_unpaid;
	private	$_translator;
	
	public function __construct($translator) 
	{
		$this->_translator				=	$translator;
		$this->_paid					=	$this->_translator->translator('common_paid_language', '', 'Invoice');
		$this->_unpaid					=	$this->_translator->translator('common_unpaid_language', '', 'Invoice');		
		$this->_cancelled				=	$this->_translator->translator('common_cancelled_language', '', 'Invoice');
		$this->_now_payable_paid		=	$this->_translator->translator('invoice_list_invoice_now_payable_paid_language', '', 'Invoice');		
		$this->_now_payable_unpaid		=	$this->_translator->translator('invoice_list_invoice_now_payable_unpaid_language', '', 'Invoice');
	}
	
	public function getStatus($status) 
	{
		$status_format = '';
		switch($status)
		{
			case '0':
					$status_format	=	$this->_cancelled;
				break;
			case '1':
					$status_format	=	$this->_paid;
				break;
			case '2':
					$status_format	=	$this->_unpaid;
				break;
		}
		return $status_format;
	}
	
	public function getPaidStatus()
	{
		return $this->_paid;
	}
	
	public function getUnPaidStatus()
	{
		return $this->_unpaid;
	}
	
	public function getCancelledStatus()
	{
		return $this->_cancelled;
	}
	
	public function getNowPaidStatus()
	{
		return $this->_now_payable_paid;
	}
	
	public function getNowUnPaidStatus()
	{
		return $this->_now_payable_unpaid;
	}	
}
?>