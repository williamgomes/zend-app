<?php
class Invoice_SettingController extends Zend_Controller_Action
{
	private $_SettingForm;
	private $_controllerCache;		
	private $_translator;
	private $_auth_obj;	
	
    public function init()
    {		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
    }
	
	public function preDispatch() 
	{
		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);
		$this->view->setEscape('stripslashes');
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		if($getAction != 'uploadfile')
		{	
			$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}		
	}
	
		
	//AIRPORT FUNCTIONS START
	
	public function listAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$this->_SettingForm = new Invoice_Form_SettingForm();	
		$this->view->settingForm	=	$this->_SettingForm;
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();	
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');						
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{										
					$list_mapper = new Invoice_Model_SettingListMapper();
					$list_datas =  $list_mapper->fetchAll($pageNumber, null, $posted_data);
					
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;	
						$status_class = new Invoice_Controller_Helper_Status($this->_translator);			
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								
								$entry_arr['id_format']	=$this->view->numbers($entry_arr['id']);								
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}		
	}	
	
	
	public function addAction()
	{ 
		$this->_SettingForm = new Invoice_Form_SettingForm();
		if ($this->_request->getPost()) 
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			
			try
			{
				if($this->_SettingForm->isValid($this->_request->getPost())) 
				{
					$perm = new Invoice_View_Helper_Allow();
					if($perm->allow())
					{
						$Setting = new Invoice_Model_Setting($this->_SettingForm->getValues());
						$result = $Setting->saveSetting();
						if($result['status'] == 'ok')
						{
							$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
							$last_id = $result['id'];						
							$msg = $this->_translator->translator("invoice_setting_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $this->_translator->translator("invoice_setting_save_err");
							$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
						}
					}
					else
					{
						$msg =  $this->_translator->translator("invoice_setting_add_action_deny_desc");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$validatorMsg = $this->_SettingForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $err_arr)
					{					
						foreach($err_arr as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$this->view->assign('settingForm', $this->_SettingForm);
		}
	}
	
	public function editAction()
	{ 
		$this->_SettingForm = new Invoice_Form_SettingForm();
		$id = $this->_request->getParam('id');
		if ($this->_request->getPost()) 
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			
			try
			{
				if($this->_SettingForm->isValid($this->_request->getPost())) 
				{
					$id = $this->_request->getPost('id');
					$perm = new Invoice_View_Helper_Allow();
					if($perm->allow())
					{
						$Setting = new Invoice_Model_Setting($this->_SettingForm->getValues());
						$Setting->setId($id);
						$result = $Setting->saveSetting();
						if($result['status'] == 'ok')
						{
							$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
							$last_id = $result['id'];						
							$msg = $this->_translator->translator("invoice_setting_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $this->_translator->translator("invoice_setting_save_err");
							$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
						}
					}
					else
					{
						$msg =  $this->_translator->translator("invoice_setting_add_action_deny_desc");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$validatorMsg = $this->_SettingForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $err_arr)
					{					
						foreach($err_arr as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$Setting_db = new Invoice_Model_DbTable_Setting();
			$Setting_info	=	$Setting_db->getInfo($id);		
			
			$this->_SettingForm->populate($Setting_info);
			$this->view->assign('id', $id);
			$this->view->assign('settingForm', $this->_SettingForm);
		}
	}
	
	public function deleteAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		
		if ($this->_request->isPost()) 
		{	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();			
							
			try
			{							
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				$conn->delete(Zend_Registry::get('dbPrefix').'invoice_email_template_settings', $where);							
				$json_arr = array('status' => 'ok','msg' => '');				
			}
			catch (Exception $e) 
			{
				$msg = 	$e->getMessage();			
				$json_arr = array('status' => 'err','msg' => $msg);
			}			
		}
		else
		{
			$msg = 	$translator->translator('property_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();	
		
		if ($this->_request->isPost()) 
		{	
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();					
				
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{											
					try
					{												
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						$conn->delete(Zend_Registry::get('dbPrefix').'invoice_email_template_settings', $where);										
						$json_arr = array('status' => 'ok', 'msg' => '', 'non_del_arr' => $non_del_arr);
					}
					catch (Exception $e) 
					{
						$non_del_arr[$k] = $id;
						$k++;										
						$json_arr = array('status' => 'ok', 'msg' => '', 'non_del_arr' => $non_del_arr);
					}					
				}
			}
			else
			{
				$msg = 	$this->_translator->translator('invoice_Setting_delete_err');
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$this->_translator->translator('invoice_Setting_delete_err');		
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}	
}

