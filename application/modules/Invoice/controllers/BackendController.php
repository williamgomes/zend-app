<?php

class Invoice_BackendController extends Zend_Controller_Action {

    private $_controllerCache;
    private $_translator;
    private $_auth_obj;

    public function init() {
        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {

        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');

        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);

            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    //View Invoice
    public function viewAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $invoice_id = $this->_request->getParam('invoice_id');
        if ($invoice_id) {
            $invoice_db = new Invoice_Model_DbTable_Invoices();
            $invoice_arr = $invoice_db->getInfo($invoice_id);
        }
        switch ($invoice_arr['status']) {
            case '0' :
                $invoice_arr['payment_status'] = $this->_translator->translator('common_unpaid_language');
                break;
            case '1' :
                $invoice_arr['payment_status'] = $this->_translator->translator('common_paid_language');
                break;
            case '2' :
                $invoice_arr['payment_status'] = $this->_translator->translator('common_unpaid_language');
                break;
        }
        $invoice_desc = str_replace('%invoice_id%', $invoice_id, str_replace('%payment_status%', $invoice_arr['payment_status'], $invoice_arr['invoice_desc']));
        $invoice_desc = '<a href="javascript:void(0);" style="font-size:20px" class="print_btn" rel="' . $this->view->escape($invoice_id) . '">' . $this->_translator->translator('common_print') . '</a><br />' . $invoice_desc;
        $this->_response->setBody($invoice_desc);
    }

    public function detailsAction() {
        $invoice_id = $this->_request->getParam('invoice_id');
        if ($invoice_id) {
            $invoice_db = new Invoice_Model_DbTable_Invoices();
            $invoice_arr = $invoice_db->getInfo($invoice_id);
            if ($invoice_arr) {
                $invoice_arr['invoice_id'] = $invoice_id;

                switch ($invoice_arr['status']) {
                    case '0' :
                        $invoice_arr['payment_status'] = $this->_translator->translator('common_cancelled_language');
                        break;
                    case '1' :
                        $invoice_arr['payment_status'] = $this->_translator->translator('common_paid_language');
                        break;
                    case '2' :
                        $invoice_arr['payment_status'] = $this->_translator->translator('common_unpaid_language');
                        break;
                }
                $this->view->assign('invoice_arr', $invoice_arr);
            }
        }
        if (!$invoice_arr) {
            $this->_helper->redirector('list', $this->view->getController, $this->view->getModule, array());
        }
    }

    //Invoice List
    public function listAction() {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $status = $this->getRequest()->getParam('status');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'status' => $status, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_mapper = new Invoice_Model_InvoicesListMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, $status, $posted_data);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        $status_class = new Invoice_Controller_Helper_Status($this->_translator);
                        foreach ($list_datas as $entry) {
                            $entry_arr = $entry->toArray();
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['publish_status_invoice'] = str_replace('_', '-', $entry_arr['id']);
                            unset($entry_arr['invoice_desc']);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['invoice_create_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['invoice_create_date'])));
                            $entry_arr['total_amount_format'] = $this->view->numbers(number_format($entry_arr['total_amount'], 2, '.', ','));
                            $entry_arr['now_payable_format'] = $this->view->numbers(number_format($entry_arr['now_payable'], 2, '.', ','));
                            $entry_arr['invoice_status_format'] = $status_class->getStatus($entry_arr['status']);
                            $entry_arr['invoice_now_paid_status_format'] = $status_class->getNowPaidStatus();
                            $entry_arr['invoice_now_unpaid_status_format'] = $status_class->getNowUnPaidStatus();
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function gatewayAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->_request->isPost()) {
            try {
                $global_conf = Zend_Registry::get('global_conf');
                $user_id = ($this->_request->getPost('payment_user_id')) ? $this->_request->getPost('payment_user_id') : $global_conf['payment_user_id'];
                $gateway_db = new Paymentgateway_Model_DbTable_Gateway();
                $view_datas = $gateway_db->getAllActiveGateway('ASC', $user_id);
                $data_result = array();
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $data_result[$key] = $entry_arr;
                        $key++;
                    }
                }
                $json_arr = array('status' => 'ok', 'gateway_list' => $data_result);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'gateway_list' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function editAction() {
        $invoice_id = $this->_request->getParam('id');
        $invoiceForm = new Invoice_Form_InvoiceForm();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            if ($invoiceForm->isValid($this->_request->getPost())) {
                $invoices = new Invoice_Model_Invoices($invoiceForm->getValues());
                $invoice_id = $this->_request->getPost('invoice_id');
                $invoices->setId($invoice_id);

                $perm = new Invoice_View_Helper_Allow();
                if ($perm->allow()) {
                    $result = $invoices->saveInvoices();
                    if ($result['status'] == 'ok') {
                        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
                        $msg = $this->_translator->translator("invoice_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $result['msg'];
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                    }
                } else {
                    $Msg = $this->_translator->translator("invoice_add_action_deny_desc");
                    $json_arr = array('status' => 'errP', 'msg' => $Msg);
                }
            } else {
                $validatorMsg = $invoiceForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $mem_db = new Members_Model_DbTable_MemberList();
            $invoiceData = new Invoice_Model_DbTable_Invoices();
            $invoice_info = $invoiceData->getInfo($invoice_id);
            if ($invoice_info) {
                $invoiceForm->populate($invoice_info);
                $mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);

                switch ($invoice_info['status']) {
                    case '0' :
                        $this->view->invoice_status = $this->_translator->translator('common_cancelled_language');
                        break;
                    case '1' :
                        $this->view->invoice_status = $this->_translator->translator('common_paid_language');
                        break;
                    case '2' :
                        $this->view->invoice_status = $this->_translator->translator('common_unpaid_language');
                        break;
                }

                $this->view->invoice_info = $invoice_info;
                $this->view->invoice_id = $invoice_id;
                $this->view->invoiceForm = $invoiceForm;
                $this->view->mem_info = $mem_info;
            } else {
                $this->_helper->redirector('list', $this->view->getController, $this->view->getModule, array());
            }
        }
    }

    public function emailAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->_request->isPost()) {
            try {
                $invoice_id = $this->_request->getPost('invoice_id');
                $email_template_id = $this->_request->getPost('email_template_id');

                $invoice_db = new Invoice_Model_DbTable_Invoices();
                $invoice_info = $invoice_db->getInfo($invoice_id);


                $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
                $invoice_item_info = $invoice_item_db->getInvoiceItems($invoice_id);


                $invoice_later = $this->generateInvoiceLater($invoice_info, $invoice_item_info);
                $email_class = new Members_Controller_Helper_Registers();
                $send_result = $email_class->sendInvoiceEmail($invoice_later, $email_template_id, null, null, $invoice_later[Eicra_File_Constants::INVOICE_TO_EMAIL], null);
                if ($send_result['status'] == 'ok') {
                    $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
                    $msg = $this->_translator->translator("invoice_send_email_success");
                    $json_arr = array('status' => 'ok', 'msg' => $msg, 'invoice_later' => $send_result);
                } else {
                    $msg = $send_result['msg'];
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function generateInvoiceLater($invoice_info, $invoice_item_info) {
        $invoice_later_arr = array();

        $mem_db = new Members_Model_DbTable_MemberList();
        $mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);

        foreach ($mem_info as $globalIdentity_key => $globalIdentity_value) {
            $invoice_later_arr[$globalIdentity_key] = $globalIdentity_value;
        }

        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $currencySymbol = $currency->getSymbol();
        $currencyShortName = $currency->getShortName();

        switch ($invoice_info['status']) {
            case '1':
                $action_name = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator("common_paid_language") . '</h1>';
                break;
            case '2':
                $action_name = '<h1 style="border:1px solid #DF8F8F; background-color:#FFCECE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator("common_unpaid_language") . '</h1>';
                break;
            case '0':
                $action_name = '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left;">' . $this->_translator->translator("common_cancelled_language") . '</h1>';
                break;
        }

        $invoice_later_arr[Eicra_File_Constants::INVOICE_ID] = $invoice_info['id'];
        $invoice_later_arr[Eicra_File_Constants::INVOICE_SUBJECT] = $invoice_info['invoice_subject'];
        $invoice_later_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = $action_name;
        $invoice_later_arr[Eicra_File_Constants::INVOICE_STATUS] = $invoice_later_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS];
        $invoice_later_arr[Eicra_File_Constants::INVOICE_TO] = $this->view->escape($mem_info['title']) . ' ' . $this->view->escape($mem_info['firstName']) . ' ' . $this->view->escape($mem_info['lastName']) . '<br />' . $this->view->escape($mem_info['mobile']) . '<br />' . $this->view->escape($mem_info['city']) . ', ' . $this->view->escape($mem_info['state']) . ', ' . $this->view->escape($mem_info['postalCode']) . '<br />' . $this->view->escape($mem_info['country_name']) . '<BR />' . $this->view->escape($mem_info['username']);
        $invoice_later_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
        $invoice_later_arr[Eicra_File_Constants::INVOICE_LOGO] = $this->view->escape($global_conf['payment_logo_url']);
        $invoice_later_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = $this->view->escape($invoice_info['invoice_create_date']);
        $invoice_later_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y", strtotime($invoice_later_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
        $invoice_later_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];


        //invoice_later_arr assigning started		
        $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
											<tbody>
											<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_send_email_desc_title") . '</strong></td>
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_send_email_amount_title") . '</strong></td>
		
											</tr>';
        if ($invoice_item_info) {
            foreach ($invoice_item_info as $invoice_item_info_key => $dataInfo) {
                $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= $dataInfo['item_details'];
            }
        }
        if ($invoice_info['module_name'] == 'Members') {
            $packages = new Members_Controller_Helper_Packages();
            $general_db = new Members_Model_DbTable_General();
            if ($packages->isPackageFrontend($mem_info['user_id'], $mem_info['role_id'])) {

                $mem_user_id = $mem_info['user_id'];
                $mem_role_id = $mem_info['role_id'];
                $role_db = new Members_Model_DbTable_Role();
                $role_info = $role_db->getRoleInfo($mem_role_id);
                $mem_info['form_id'] = $role_info['form_id'];
                $dynamic_valu_db = new Members_Model_DbTable_FieldsValue();
                $mem_info = $dynamic_valu_db->getFieldsValueInfo($mem_info, $mem_user_id);
                foreach ($mem_info as $key => $value) {
                    $field_name_arr = explode('_', $key);
                    if (in_array('package', $field_name_arr)) {
                        $package_info = $packages->getFormAllDatas($value);
                    }
                }

                if ($package_info) {

                    $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr><td colspan="2" style="height:1px"></td></tr>
					<tr>
						<td id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
							' . $this->view->escape($package_info[0]['field_value']) . '
						</td>
						<td style="font-size:17px; margin:10px auto 2px auto; padding:10px 10px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0; text-align:right;">
							' . $currencySymbol . ' ' . $package_info[1]['field_value'] . ' ' . $currencyShortName . '
						</td>
					</tr>';
                }
            }
        }
        $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
						<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">
						<div>' . $this->_translator->translator("invoice_send_email_total_title") . '</div>
						</td>
						<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format(($invoice_info['total'] + $invoice_info['tax']), 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>
					</tr>';
        $services_charge = 0;
        if (!empty($invoice_info['service_charge'])) {
            $services_charge = $invoice_info['service_charge'];
            $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("invoice_send_email_service_charge") . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . Settings_Service_Price::getMargine('4') . '</strong></td>' .
                    '</tr>';
        }

        $now_payable = 0;
        $deposit_charge = 0;
        if (!empty($invoice_info['deposit_charge'])) {
            $deposit_charge = $invoice_info['deposit_charge'];
            $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("invoice_send_email_deposit_charge", Settings_Service_Price::getMargine('5')) . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($deposit_charge, 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';

            $now_payable = $invoice_info['now_payable'];
            $now_payable_paid_status = ($invoice_info['now_payable_paid_status'] == '1') ? $this->_translator->translator("invoice_list_invoice_now_payable_paid_language") : $this->_translator->translator("invoice_list_invoice_now_payable_unpaid_language");
            $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">' . $this->_translator->translator("invoice_send_email_deposit_payable") . '</div></td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($now_payable, 2, '.', ',') . ' ' . $currencyShortName . ' (' . $now_payable_paid_status . ')</strong></td>' .
                    '</tr>';
        }

        if (!empty($invoice_info['service_charge'])) {
            $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("invoice_send_email_grand_total") . '</td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format(($invoice_info['total'] + $invoice_info['tax'] + $services_charge), 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';
        }

        if (!empty($invoice_info['deposit_charge']) && !empty($now_payable)) {
            $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">' .
                    '<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right;" height="40" colspan="2"><strong>' . $this->_translator->translator("invoice_send_email_later_payable", $currencySymbol . ' ' . number_format((($invoice_info['total'] + $invoice_info['tax'] + $services_charge) - $now_payable), 2, '.', ',') . ' ' . $currencyShortName) . '</strong></td>' .
                    '</tr>';
        }

        $invoice_later_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
																</table>';
        $invoice_later_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoice_info['total'] + $invoice_info['tax'] + $services_charge), 2, '.', ',') . ' ' . $currencyShortName;

        return $invoice_later_arr;
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new Invoice_View_Helper_Allow();
            if ($perm->allow('delete', 'backend', 'Invoice')) {
                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    foreach ($id_arr as $id) {
                        //Remove Price Plan						
                        try {
                            $invoice_arr[Eicra_File_Constants::INVOICE_ID] = $id;
                            $this->setInvoiceServices($id, $invoice_arr, 'delete_action');

                            $whereO = array();
                            $whereO[] = 'invoice_id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_order', $whereO);

                            $whereI = array();
                            $whereI[] = 'invoice_id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_items', $whereI);

                            $where = array();
                            $where[] = 'id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_table', $where);

                            $json_arr = array('status' => 'ok');
                        } catch (Exception $e) {
                            $msg = $e->getMessage();
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                        }
                    }
                } else {
                    $msg = $translator->translator("flight_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function setInvoiceServices($invoice_id, $invoice_arr, $action_field = 'query_action') {
        try {
            $invoices = new Invoice_Model_DbTable_Invoices();
            $invoice_info = $invoices->getInfo($invoice_id);

            if ($invoice_info['module_name']) {
                $module_arr = explode(',', $invoice_info['module_name']);
                foreach ($module_arr as $$module_key => $module) {
                    $services_class_name = $module . '_Service_Invoice';
                    if (class_exists($services_class_name)) {
                        $services = new $services_class_name($invoice_arr);
                        if ($invoice_info[$action_field]) {
                            $method_arr = explode(',', $invoice_info[$action_field]);
                            foreach ($method_arr as $key => $method) {
                                if (method_exists($services, $method)) {
                                    file_put_contents('method.txt', $method);
                                    $services->$method();
                                    
                                }
                            }
                        }
                    }
                }
                $return_arr = array('status' => 'ok');
            }
        } catch (Exception $e) {
            $return_arr = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    public function paidAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $template_id_field = $paction . '_template_id';
            switch ($paction) {
                case 'paid':
                    $active = '1';
                    $action_name = 'paid_action';
                    break;
                case 'unpaid':
                    $active = '2';
                    $action_name = 'unpaid_action';
                    break;
                case 'cancel':
                    $active = '0';
                    $action_name = 'cancel_action';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);

                //DB Connection
                $conn = Zend_Registry::get('msqli_connection');
                $conn->getConnection();

                foreach ($id_arr as $id) {
                    $invoice_arr[Eicra_File_Constants::INVOICE_ID] = $id;
//                    print_r($invoice_arr);
                    $this->setInvoiceServices($id, $invoice_arr, $action_name);
                    // Update Article status
                    $where = array();
                    $where[] = 'id = ' . $conn->quote($id);
                    try {
                        $conn->update(Zend_Registry::get('dbPrefix') . 'invoice_table', array('status' => $active, 'now_payable_paid_status' => $active), $where);
                        $email_class = new Invoice_View_Helper_Email($this->view);
                        $get_invoice = $email_class->generateInvoiceLater($id);
                        if ($get_invoice['status'] == 'ok') {
                            $global_conf = Zend_Registry::get('global_conf');
                            $currency = new Zend_Currency($global_conf['default_locale']);
                            $currencySymbol = $currency->getSymbol();
                            $currencyShortName = $currency->getShortName();
                            $invoice_info = $get_invoice['invoice_info'];
                            $invoice_arr = $get_invoice['invoice_arr'];



                            $mem_db = new Members_Model_DbTable_MemberList();
                            $mem_info = $mem_db->getMemberInfo($get_invoice['invoice_info']['user_id']);
                            foreach ($mem_info as $globalIdentity_key => $globalIdentity_value) {
                                $invoice_arr[$globalIdentity_key] = $globalIdentity_value;
                            }
                            $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($get_invoice['invoice_info']['total'] + $get_invoice['invoice_info']['service_charge'] + $get_invoice['invoice_info']['tax']), 2, '.', ',') . ' ' . $currencyShortName;
                            $settings_db = new Invoice_Model_DbTable_Setting();
                            $template_info = $settings_db->getInfoByModule($invoice_info[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                            if ($template_info) {
                                $invoice_arr['letter_id'] = ($template_info) ? $template_info[$template_id_field] : null;
                                $send_mail_result = ($get_invoice['status'] == 'ok') ? $email_class->sendMail($invoice_arr, null) : null;
                            }
                        }
                        $return = true;
                    } catch (Exception $e) {
                        $return = false;
                        $msg = $e->getMessage();
                    }
                }

                if ($return) {
                    $json_arr = array('status' => 'ok', 'active' => $active);
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator("flight_selected_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function nowpaidAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');


        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'paid':
                    $active = '1';
                    $action_name = 'paid_action';
                    break;
                case 'unpaid':
                    $active = '2';
                    $action_name = 'unpaid_action';
                    break;
                case 'cancel':
                    $active = '0';
                    $action_name = 'cancel_action';
                    break;
            }

            if (!empty($id)) {
                try {
                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    // Update Article status
                    $where = array();
                    $where[] = 'id = ' . $conn->quote($id);

                    $conn->update(Zend_Registry::get('dbPrefix') . 'invoice_table', array('now_payable_paid_status' => $active), $where);

                    $json_arr = array('status' => 'ok');
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator("flight_selected_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    //Quote List
    public function quotelistAction() {
        // action body
        $pageNumber = $this->getRequest()->getParam('page');
        $getViewPageNum = $this->getRequest()->getParam('viewPageNum');
        $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

        if (empty($getViewPageNum) && empty($viewPageNumSes)) {
            $viewPageNum = '30';
        } else if (!empty($getViewPageNum) && empty($viewPageNumSes)) {
            $viewPageNum = $getViewPageNum;
        } else if (empty($getViewPageNum) && !empty($viewPageNumSes)) {
            $viewPageNum = $viewPageNumSes;
        } else if (!empty($getViewPageNum) && !empty($viewPageNumSes)) {
            $viewPageNum = $getViewPageNum;
        }
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_' . $viewPageNum . '_' . $user_id;
        if (($datas = $this->_controllerCache->load($uniq_id)) === false) {
            $type_id = null;
            $invoices = new Invoice_Model_QuotesListMapper();
            $datas = $invoices->fetchAll($pageNumber, $type_id);
            $this->_controllerCache->save($datas, $uniq_id);
        }
        $this->view->datas = $datas;
    }

    public function quoteaddAction() {
        $quoteForm = new Invoice_Form_QuoteForm();
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            if ($quoteForm->isValid($this->_request->getPost())) {
                $quotes = new Invoice_Model_Quotes($quoteForm->getValues());
                $quotes->setSub_total($quoteForm->total->getValue());
                $quotes->setStage('1');
                $quotes->setQuote_create_date(date("Y-m-d h:i:s"));
                $quote_result = $quotes->saveQuotes();
                if ($quote_result['status'] == 'ok') {
                    $msg = $this->_translator->translator('invoice_quote_save_success');
                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $quote_result['msg']);
                }
            } else {
                $validatorMsg = $quoteForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        $this->view->assign('quoteForm', $quoteForm);
    }

    public function quoteeditAction() {
        $quoteForm = new Invoice_Form_QuoteForm();
        $quote_id = $this->_request->getParam('quote_id');
        $tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            if ($quoteForm->isValid($this->_request->getPost())) {
                $quote_id = $this->_request->getPost('quote_id');
                if ($quote_id) {
                    $quotes = new Invoice_Model_Quotes();
                    $quotes->setId($quote_id);
                    $quotes->setQuote_customer_notes($this->_request->getPost('quote_customer_notes'));
                    $quotes->setQuote_admin_notes($this->_request->getPost('quote_admin_notes'));
                    $quotes->setInvoice_subject($this->_request->getPost('invoice_subject'));
                    $quotes->setInvoice_desc($this->_request->getPost('invoice_desc'));
                    $quotes->setQuote_update_date(date("Y-m-d h:i:s"));
                    $quotes->setTotal($this->_request->getPost('total'));
                    $quotes->setSub_total($this->_request->getPost('total'));
                    $quotes->setTax($this->_request->getPost('tax'));
                    $quotes->setPayment_method($this->_request->getPost('payment_method'));

                    $quote_result = $quotes->saveQuotes();
                    if ($quote_result['status'] == 'ok') {
                        $quoteItems_db = new Invoice_Model_DbTable_QuoteItems();
                        $quoteItems_info = $quoteItems_db->getQuoteItemsInfoByQuoteId($quote_id);

                        $quoteItem_model = new Invoice_Model_QuoteItems();

                        if ($quoteItems_info) {
                            foreach ($quoteItems_info as $key => $quoteItems) {
                                $quoteItem_model->setId($quoteItems['id']);
                                $quoteItem_model->setItem_details($this->_request->getPost('item_details_' . $quoteItems['id']));
                                $quoteItem_model->setItem_total($this->_request->getPost('item_total_' . $quoteItems['id']));
                                $quoteItem_model->setItem_tax($this->_request->getPost('item_tax_' . $quoteItems['id']));
                                $item_result = $quoteItem_model->saveQuoteItems();
                            }
                        }
                        $msg = $this->_translator->translator('invoice_quote_save_success');
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $quote_result['msg']);
                    }
                } else {
                    $msg = $this->_translator->translator('invoice_quote_id_empty');
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $validatorMsg = $quoteForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        if ($quote_id) {
            $quote_db = new Invoice_Model_DbTable_Quotes();
            $quote_arr = $quote_db->getInfo($quote_id);
        }
        $quoteForm->populate($quote_arr);
        $this->view->assign('quoteForm', $quoteForm);
        $this->view->assign('tab', $tab);
        $this->view->assign('quote_id', $quote_id);
        $this->view->assign('quote_arr', $quote_arr);
    }

    public function deletequoteAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();


        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new Invoice_View_Helper_Allow();
            if ($perm->allow('deletequote', 'backend', 'Invoice')) {
                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    foreach ($id_arr as $id) {
                        //Remove Price Plan						
                        try {
                            $whereI = array();
                            $whereI[] = 'quote_id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_quote_items', $whereI);

                            $where = array();
                            $where[] = 'id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_quote', $where);
                            $json_arr = array('status' => 'ok');
                        } catch (Exception $e) {
                            $msg = $e->getMessage();
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                        }
                    }
                } else {
                    $msg = $this->_translator->translator("invoice_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $this->_translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $this->_translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function stageAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'valid':
                    $active = '1';
                    break;
                case 'delivered':
                    $active = '2';
                    break;
                case 'expire':
                    $active = '0';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);

                //DB Connection
                $conn = Zend_Registry::get('msqli_connection');
                $conn->getConnection();

                foreach ($id_arr as $id) {
                    // Update Article status
                    $where = array();
                    $where[] = 'id = ' . $conn->quote($id);
                    try {
                        $conn->update(Zend_Registry::get('dbPrefix') . 'flight_quote', array('status' => $active), $where);
                        /* $email_class = new Invoice_View_Helper_Email($this->view);
                          $get_invoice = $email_class->generateInvoice($id);
                          $send_mail_result = $email_class->sendMail($get_invoice['invoice_arr'],null); */
                        $return = true;
                    } catch (Exception $e) {
                        $return = false;
                        $msg = $e->getMessage();
                    }
                }

                if ($return) {
                    $json_arr = array('status' => 'ok');
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator("flight_selected_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function convertAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new Invoice_View_Helper_Allow();
            if ($perm->allow('convert', 'backend', 'Invoice')) {
                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);

                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    $quote_db = new Invoice_Model_DbTable_Quotes();
                    $quote_items_db = new Invoice_Model_DbTable_QuoteItems();

                    foreach ($id_arr as $key => $quote_id) {
                        $quote_info = $quote_db->getInfo($quote_id);
                        $result = $this->saveInvoice($quote_info);
                        if ($result['status'] == 'ok') {
                            $result_order = $this->saveOrder($quote_info, $result['id']);
                            if ($result_order['status'] == 'ok') {
                                $quote_items_info = $quote_items_db->getQuoteItemsInfo($quote_info['user_id'], $quote_info['quote_id']);
                                $result_item = $this->saveInvoiceItems($quote_items_info, $result['id']);
                                if ($result_item['status'] == 'ok') {
                                    $whereQ = array();
                                    $whereQ[] = 'id = ' . $conn->quote($quote_id);
                                    $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_quote', $whereQ);

                                    $whereI = array();
                                    $whereI[] = 'quote_id = ' . $conn->quote($quote_id);
                                    $conn->delete(Zend_Registry::get('dbPrefix') . 'invoice_quote_items', $whereI);

                                    $msg = $this->_translator->translator('invoice_quote_convert_success');
                                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                                } else {
                                    $json_arr = array('status' => 'err', 'msg' => $result_item['msg']);
                                    break;
                                }
                            } else {
                                $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                            }
                        }
                    }
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('invoice_quote_convert_invalid_arg'));
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('invoice_quote_convert_invalid_arg'));
            }
        }

        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function saveInvoiceItems($quote_items_info_arr, $invoice_id) {
        $invoicesItems = new Invoice_Model_InvoiceItems();
        foreach ($quote_items_info_arr as $quote_items_info) {
            $invoicesItems->setUser_id($quote_items_info['user_id']);
            $invoicesItems->setInvoice_id($invoice_id);
            $invoicesItems->setItem_details($quote_items_info['item_details']);
            $invoicesItems->setObject_value($quote_items_info['object_value']);
            $invoicesItems->setItem_total($quote_items_info['item_total']);
            $invoicesItems->setItem_tax($quote_items_info['item_tax']);
            $result = $invoicesItems->saveInvoiceItems();
            if ($result['status'] == 'ok') {
                $json_arr = array('status' => 'ok', 'id' => $result['id']);
            } else {
                $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                break;
            }
        }
        return $json_arr;
    }

    private function saveInvoice($quote_info) {
        $invoices = new Invoice_Model_Invoices();
        $invoices->setUser_id($quote_info['user_id']);
        $invoices->setInvoice_subject($quote_info['invoice_subject']);
        $invoices->setInvoice_desc($quote_info['invoice_desc']);
        $invoices->setInvoice_create_date(date("Y-m-d h:i:s"));
        $invoices->setInvoice_update_date(date("Y-m-d h:i:s"));
        $invoices->setSub_total($quote_info['sub_total']);
        $invoices->setTax($quote_info['tax']);
        $invoices->setTotal($quote_info['total']);
        $invoices->setPayment_method($quote_info['payment_method']);
        $invoices->setStatus('1');
        $result = $invoices->saveInvoices();
        if ($result['status'] == 'ok') {
            $json_arr = array('status' => 'ok', 'id' => $result['id']);
        } else {
            $json_arr = array('status' => 'err', 'msg' => $result['msg']);
        }
        return $json_arr;
    }

    private function saveOrder($quote_info, $invoice_id) {
        $orders = new Invoice_Model_Orders();
        $orders->setUser_id($quote_info['user_id']);
        $orders->setOrder_date(date("Y-m-d h:i:s"));
        $orders->setOrder_amount($quote_info['total']);
        $orders->setPayment_method($quote_info['payment_method']);
        $orders->setIpaddress($quote_info['ipaddress']);
        $orders->setInvoice_id($invoice_id);
        $orders->setStatus('1');
        $result = $orders->saveOrders();
        if ($result) {
            $json_arr = array('status' => 'ok', 'id' => $result['id']);
        } else {
            $json_arr = array('status' => 'err', 'msg' => $result['msg']);
        }
        return $json_arr;
    }

    public function fullpayAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->_request->isPost()) {
            try {
                $payment_id = $this->_request->getPost('payment_id');
                $invoice_id = $this->_request->getPost('invoice_id');

                $payment_db = new Paymentgateway_Model_DbTable_Gateway();
                $payment_info = $payment_db->getInfo($payment_id);
                $invoice_db = new Invoice_Model_DbTable_Invoices();
                $invoice_info = $invoice_db->getInfo($invoice_id);
                $payment_info['payment_user_id'] = $invoice_info['payment_user_id'];
                $mem_db = new Members_Model_DbTable_MemberList();
                $mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);

                //Set Data variables
                $payble = ($invoice_info['now_payable_paid_status'] != '1') ? ($invoice_info['tax'] + $invoice_info['total'] + $invoice_info['service_charge']) : ( ($invoice_info['tax'] + $invoice_info['total'] + $invoice_info['service_charge']) - $invoice_info['now_payable'] );
                $invoice = (int) $invoice_id;
                $client_email = $mem_info['username'];
                $quantity = 1;
                switch ($payment_info['method']) {
                    case '1':
                        $input_type = 'link';
                        break;
                    case '2':
                        $input_type = 'html';
                        break;
                    default:
                        $input_type = 'link';
                        break;
                }

                $payment_data = $this->view->data($payble, $invoice, $client_email, $quantity, $payment_info, $this->view);
                $json_arr = array('status' => 'ok', 'payment_data' => $payment_data, 'payment_info' => $payment_info, 'input_type' => $input_type);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function nowpayAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->_request->isPost()) {
            try {
                $payment_id = $this->_request->getPost('payment_id');
                $invoice_id = $this->_request->getPost('invoice_id');

                $payment_db = new Paymentgateway_Model_DbTable_Gateway();
                $payment_info = $payment_db->getInfo($payment_id);
                $invoice_db = new Invoice_Model_DbTable_Invoices();
                $invoice_info = $invoice_db->getInfo($invoice_id);
                $payment_info['payment_user_id'] = $invoice_info['payment_user_id'];
                $mem_db = new Members_Model_DbTable_MemberList();
                $mem_info = $mem_db->getMemberInfo($invoice_info['user_id']);

                if ($invoice_info['now_payable_paid_status'] != '1') {
                    //Set Data variables
                    $payble = $invoice_info['now_payable'];
                    $invoice = (int) $invoice_id;
                    $client_email = $mem_info['username'];
                    $quantity = 1;
                    switch ($payment_info['method']) {
                        case '1':
                            $input_type = 'link';
                            break;
                        case '2':
                            $input_type = 'html';
                            break;
                        default:
                            $input_type = 'link';
                            break;
                    }

                    $payment_data = $this->view->data($payble, $invoice, $client_email, $quantity, $payment_info, $this->view);
                    $json_arr = array('status' => 'ok', 'payment_data' => $payment_data, 'payment_info' => $payment_info, 'input_type' => $input_type);
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('gateway_now_payable_paid_already'));
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

}

?>