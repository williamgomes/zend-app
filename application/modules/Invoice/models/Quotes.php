<?php
class Invoice_Model_Quotes
{
	protected $_id;
	protected $_user_id;
	protected $_quote_customer_notes;	
	protected $_quote_admin_notes;
	protected $_invoice_subject;
	protected $_invoice_desc;
	protected $_quote_create_date;
	protected $_quote_update_date;
	protected $_ipaddress;
	protected $_sub_total;
	protected $_tax;
	protected $_total;
	protected $_payment_method;
	protected $_stage;
	
	
 
    	public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveQuotes()
		{
			try
			{
				$mapper  = new Invoice_Model_QuotesMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = array('status' => 'err', 'msg' => $e->getMessage());
			}
			return $return;
		}
		
		
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setUser_id($text = null)
		{
			if($text)
			{
				$this->_user_id = $text;
			}
			else
			{
				$auth = Zend_Auth::getInstance ();
				if ($auth->hasIdentity ())
				{
					$globalIdentity = $auth->getIdentity ();
					$this->_user_id = $globalIdentity->user_id;
				}
				else
				{
					$this->_user_id = ($text) ? $text : '1';
				}
			}
			return $this;
		}
		
		public function setQuote_customer_notes($text)
		{
			$this->_quote_customer_notes = $text;
			return $this;
		}
		
		public function setQuote_admin_notes($text)
		{
			$this->_quote_admin_notes = $text;
			return $this;
		}
		
		public function setInvoice_subject($text)
		{
			$this->_invoice_subject = $text;
			return $this;
		}
		
		public function setInvoice_desc($text)
		{
			$this->_invoice_desc = $text;
			return $this;
		}		
		
		public function setQuote_create_date($text)
		{
			$this->_quote_create_date = $text;
			return $this;
		}		
		
		public function setQuote_update_date($text)
		{
			$this->_quote_update_date = $text;
			return $this;
		}
		
		public function setSub_total($text)
		{
			$this->_sub_total = $text;
			return $this;
		}
		
		public function setTax($text)
		{
			$this->_tax = $text;
			return $this;
		}
		
		public function setTotal($text)
		{
			$this->_total = $text;
			return $this;
		}
		
		public function setPayment_method($text)
		{
			$this->_payment_method = $text;
			return $this;
		}
		
		public function setStage($text)
		{
			$this->_stage = $text;
			return $this;
		}		
		
		public function setIpaddress($text = null)
		{
			if($text)
			{
				$this->_ipaddress = $text;
			}
			else
			{
				$this->_ipaddress = $_SERVER['REMOTE_ADDR'];
			}
			return $this;
		}
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getUser_id()
		{  
			if(empty($this->_user_id))
			{
				$this->setUser_id(); 
			}        
			return $this->_user_id;
		}
		
		public function getQuote_customer_notes()
		{
			return $this->_quote_customer_notes;
		}
		
		public function getQuote_admin_notes()
		{
			return $this->_quote_admin_notes;
		}
		
		public function getInvoice_subject()
		{
			return $this->_invoice_subject;
		}
		
		public function getInvoice_desc()
		{
			return $this->_invoice_desc;
		}
		
		public function getQuote_create_date()
		{
			return $this->_quote_create_date;
		}
		
		public function getQuote_update_date()
		{
			return $this->_quote_update_date;
		}
		
		public function getSub_total()
		{         
			return $this->_sub_total;
		}
		
		public function getTax()
		{
			return $this->_tax;
		}
		
		public function getTotal()
		{         
			return $this->_total;
		}
		
		public function getPayment_method()
		{
			return $this->_payment_method;
		}
		
		public function getStage()
		{
			return $this->_stage;
		}
		
		public function getIpaddress()
		{
			if(empty($this->_ipaddress))
			{
				$this->setIpaddress();
			}
			return $this->_ipaddress;
		}	
}
?>