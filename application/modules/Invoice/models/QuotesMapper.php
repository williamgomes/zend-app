<?php
class Invoice_Model_QuotesMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Invoice_Model_DbTable_Quotes');
        }
        return $this->_dbTable;
    }
	
	public function save(Invoice_Model_Quotes $obj)
	{
			 
		if ((null === ($id = $obj->getId())) || empty($id)) 
		{
			try 
			{
				unset($data['id']);
				
				$data = array(					
					'user_id' 					=> 	$obj->getUser_id(),
					'quote_customer_notes' 		=>	$obj->getQuote_customer_notes(),
					'quote_admin_notes' 		=>	$obj->getQuote_admin_notes(),				
					'invoice_subject' 			=>	$obj->getInvoice_subject(),
					'invoice_desc' 				=>	$obj->getInvoice_desc(),
					'quote_create_date' 		=>	$obj->getQuote_create_date(),						
					'sub_total'	 				=>	$obj->getSub_total(),
					'tax' 						=>	$obj->getTax(),
					'total'						=>	$obj->getTotal(),
					'payment_method' 			=>	$obj->getPayment_method(),
					'stage' 					=>	$obj->getStage(),
					'ipaddress' 				=>	$obj->getIpaddress()
				);				
				$last_id = $this->getDbTable()->insert($data);	
				$result = array('status' => 'ok' ,'id' => $last_id);	
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
			}
						
		} 
		else 
		{
			try 
			{
				$data = array(		
					'quote_customer_notes' 		=>	$obj->getQuote_customer_notes(),
					'quote_update_date' 		=>	$obj->getQuote_update_date(),
					'quote_admin_notes' 		=>	$obj->getQuote_admin_notes(),				
					'invoice_subject' 			=>	$obj->getInvoice_subject(),
					'invoice_desc' 				=>	$obj->getInvoice_desc(),	
					'sub_total'	 				=>	$obj->getSub_total(),
					'tax' 						=>	$obj->getTax(),
					'total'						=>	$obj->getTotal(),
					'payment_method' 			=>	$obj->getPayment_method()						
				);
				
				// Start the Update process
				
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);	
			} 
			catch (Exception $e) 
			{
				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
			}				
		}
		return $result;
	}
}
?>