<?php
class Invoice_Model_SettingMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Invoice_Model_DbTable_Setting');
        }
        return $this->_dbTable;
    }
	
	public function save(Invoice_Model_Setting $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{				
				try 
				{
					unset($data['id']);
				
					$data = array(					
						'module_name' 						=> 	$obj->getModule_name(),
						'role_id' 							=>	$obj->getRole_id(),
						'default_template_id' 				=>	$obj->getDefault_template_id(),
						'paid_template_id' 					=>	$obj->getPaid_template_id(),
						'unpaid_template_id' 				=>	$obj->getUnpaid_template_id(),
						'cancel_template_id' 				=>	$obj->getCancel_template_id(),
						'delete_template_id' 				=>	$obj->getDelete_template_id(),
						'refund_template_id' 				=>	$obj->getRefund_template_id()									
					);
					
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{					
				try 
				{
					$data = array(		
						'module_name' 						=> 	$obj->getModule_name(),
						'role_id' 							=>	$obj->getRole_id(),
						'default_template_id' 				=>	$obj->getDefault_template_id(),
						'paid_template_id' 					=>	$obj->getPaid_template_id(),
						'unpaid_template_id' 				=>	$obj->getUnpaid_template_id(),
						'cancel_template_id' 				=>	$obj->getCancel_template_id(),
						'delete_template_id' 				=>	$obj->getDelete_template_id(),
						'refund_template_id' 				=>	$obj->getRefund_template_id()						
					);	
								
					// Start the Update process					
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>