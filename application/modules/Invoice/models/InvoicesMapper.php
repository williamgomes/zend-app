<?php

class Invoice_Model_InvoicesMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Invoice_Model_DbTable_Invoices');
        }
        return $this->_dbTable;
    }

    public function save(Invoice_Model_Invoices $obj) {

        if ((null === ($id = $obj->getId())) || empty($id)) {
            try {
                unset($data['id']);

                $data = array(
                    'user_id' => $obj->getUser_id(),
                    'payment_user_id' => $obj->getPayment_user_id(),
                    'invoice_subject' => $obj->getInvoice_subject(),
                    'invoice_desc' => $obj->getInvoice_desc(),
                    'invoice_create_date' => $obj->getInvoice_create_date(),
                    'invoice_update_date' => $obj->getInvoice_update_date(),
                    'invoice_due_date' => $obj->getInvoice_due_date(),
                    'sub_total' => $obj->getSub_total(),
                    'tax' => $obj->getTax(),
                    'total' => $obj->getTotal(),
                    'service_charge' => $obj->getService_charge(),
                    'deposit_charge' => $obj->getDeposit_charge(),
                    'now_payable' => $obj->getNow_payable(),
                    'payment_method' => $obj->getPayment_method(),
                    'module_name' => $obj->getModule_name(),
                    'create_action' => $obj->getCreate_action(),
                    'update_action' => $obj->getUpdate_action(),
                    'delete_action' => $obj->getDelete_action(),
                    'query_action' => $obj->getQuery_action(),
                    'paid_action' => $obj->getPaid_action(),
                    'unpaid_action' => $obj->getUnpaid_action(),
                    'cancel_action' => $obj->getCancel_action()
                );
                
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            try {
                $data = array(
                    'invoice_subject' => $obj->getInvoice_subject(),
                    'invoice_desc' => $obj->getInvoice_desc(),
                    'invoice_update_date' => $obj->getInvoice_update_date()
                );

                // Start the Update process

                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>