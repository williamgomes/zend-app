<?php
/**
* This is the DbTable class for the invoice_table table.
*/
class Invoice_Model_DbTable_Invoices extends Eicra_Abstract_DbTable
{
	 /** Table name */
    protected $_name    =  'invoice_table';	
	protected $_cols	=	null;
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('inv' => $this->_name),array('inv.*', 'total_amount' => "(inv.total + inv.tax + inv.service_charge)"))
						->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'up.user_id = inv.user_id', array( 'firstName' => 'up.firstName', 'lastName' => 'up.lastName', 'title' => 'up.title', 'full_name' => "CONCAT(up.title, ' ', up.firstName, ' ', up.lastName)" ));
						   
		if($user_id && $auth->getIdentity()->access_other_user_article != '1')
		{
			$select->where('inv.user_id = ?', $user_id);
		}
		
		if($approve != null)
		{
			$select->where("inv.status = ?", $approve);
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("inv.invoice_create_date DESC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{						
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("inv.invoice_create_date DESC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options; 
	}
	
	public function getInvoiceInfo($user_id = null) 
    {	
		if(empty($user_id))
		{
			$select = $this->select()
					   ->from(array('inv' => $this->_name),array('*'));
		}
		else
		{
			$select = $this->select()
					   ->from(array('inv' => $this->_name),array('*'))					  
					   ->where("inv.user_id = ?",$user_id); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	public function getInvoiceInfos($type_id = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		if($type_id == '' || $type_id == null)
		{	
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{			
				$select = $this->select()
						   ->from(array('inv' => $this->_name),array('*'))
						   ->where('inv.user_id = ?',$user_id)					  
						   ->order("inv.invoice_create_date DESC"); 	
			}
			else
			{
				$select = $this->select()
						   ->from(array('inv' => $this->_name),array('*'))					  
						   ->order("inv.invoice_create_date DESC"); 
			}
		}
		else
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{			
				$select = $this->select()
						   ->from(array('inv' => $this->_name),array('*'))
						   ->where('inv.user_id = ?',$user_id)	
						   ->where("inv.status = ?",$type_id)				  
						   ->order("inv.invoice_create_date DESC"); 	
			}
			else
			{
				$select = $this->select()
						   ->from(array('inv' => $this->_name),array('*'))	
						   ->where("inv.status = ?",$type_id)				  
						   ->order("inv.invoice_create_date DESC");	
			}	
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'inv.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'total_amount' :
					$operatorFirstPart = " (".$table_prefix."total + ".$table_prefix."tax +  ".$table_prefix."service_charge) ";
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(inv.invoice_create_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(inv.invoice_create_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(inv.invoice_create_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
}

?>