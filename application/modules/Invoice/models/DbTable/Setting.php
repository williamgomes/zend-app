<?php
/**
* This is the DbTable class for the invoice_email_template_settings table.
*/
class Invoice_Model_DbTable_Setting extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'invoice_email_template_settings';	
	protected $_cols	=	null;
	
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }
	
	public function getAllInfo() 
    { 		
		      
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array( 'iets' => $this->_name), array('id' => 'iets.id', 'module_name' => 'iets.module_name', 'default_template_id' => 'iets.default_template_id', 'paid_template_id' => 'iets.paid_template_id', 'unpaid_template_id' => 'iets.unpaid_template_id', 'cancel_template_id' => 'iets.cancel_template_id', 'delete_template_id' => 'iets.delete_template_id', 'refund_template_id' => 'iets.refund_template_id'))
						->joinLeft(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt.id = iets.default_template_id', array('default_template' => 'nt.templates_name'))
						->joinLeft(array('nt1' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt1.id = iets.paid_template_id', array('paid_template' => 'nt1.templates_name'))
						->joinLeft(array('nt2' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt2.id = iets.unpaid_template_id', array('unpaid_template' => 'nt2.templates_name'))
						->joinLeft(array('nt3' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt3.id = iets.cancel_template_id', array('cancel_template' => 'nt3.templates_name'))
						->joinLeft(array('nt4' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt4.id = iets.delete_template_id', array('delete_template' => 'nt4.templates_name'))
						->joinLeft(array('nt5' => Zend_Registry::get('dbPrefix').'newsletter_templates'), 'nt5.id = iets.refund_template_id', array('refund_template' => 'nt5.templates_name'))
						->order('iets.module_name ASC');
		
		$options = $this->fetchAll($select);
        if ($options) 
		{
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$i = 0 ;
			$datas	=	array();
			foreach($options as $key => $value)
			{
				if($modules_license->checkModulesLicense($value['module_name']))
				{
					$datas[$i]	=  $value;
					$i++;
				}
			}
		}
		else
		{
           // throw new Exception("Count not find row=$id");
		   $datas = null;
        }
        return $datas;  		  
    }
	
	public function getInfoByModule($module_name) 
    { 
		try
		{		      
			$select = $this->select()
							->from(array( 'iets' => $this->_name), array('id' => 'iets.id', 'role_id' => 'iets.role_id', 'module_name' => 'iets.module_name', 'default_template_id' => 'iets.default_template_id', 'paid_template_id' => 'iets.paid_template_id', 'unpaid_template_id' => 'iets.unpaid_template_id', 'cancel_template_id' => 'iets.cancel_template_id', 'delete_template_id' => 'iets.delete_template_id', 'refund_template_id' => 'iets.refund_template_id'))
							->where('module_name = ?', $module_name)
							->limit(1);
			
			$options = $this->fetchAll($select);
			if ($options) 
			{           
			   $options = $options[0];
			}
			else
			{
				$options = null;
			}
		}
		catch(Exception $e)
		{
			$options = null;
		}
        return $options;  		  
    }
	
	public function getListInfo ($approve = null, $search_params = null, $tableColumns= null)
    {
		$invoice_email_template_settings_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['invoice_email_template_settings'] && is_array($tableColumns['invoice_email_template_settings'])) ? $tableColumns['invoice_email_template_settings'] : array('iets.*');
        $b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

		$select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array(
            'iets' => $this->_name
        ), $invoice_email_template_settings_column_arr) ;
      

        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("iets.id ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {

                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params); 
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("iets.id ASC");
        }

		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}

        $options = $this->fetchAll($select);
		$datas	=	array();
        if ( $options) 
		{
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$i = 0 ;			
			foreach($options as $key => $value)
			{
				if($modules_license->checkModulesLicense($value['module_name']))
				{
					$datas[$i]	=  $value;
					$i++;
				}
			}
        }
        return $datas;
    }
	
	public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'iets.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'product_group_name':
                $operatorFirstPart = " pgp.name ";
                break;
			case 'package_name':
					$operatorFirstPart = " pkg.name ";
				break;
			case 'package_id':
					$operatorFirstPart = " usr.package_id ";
				break;
			case 'user_country_id':
					$operatorFirstPart = " usr.country ";
				break;
			case 'user_country_name':
				    $operatorFirstPart = " tld.value ";
				    break;
			case 'category_name':
				    $operatorFirstPart = " cat.name ";
				    break;
			case 'category_displayble':
                $operatorFirstPart = " ( iets.category = '0' OR cat.displayble = '".$operator_arr['value']."'  ) AND 1 ";
				$operator_arr['value'] = "1";;
                break;
			case 'category_active':
                $operatorFirstPart = " cat.active ";
                break;
			case 'category_isShowProfile':
                $operatorFirstPart = " cat.isShowProfile ";
                break;
            case 'cpy_active':
                $operatorFirstPart = " cpy.active ";
                break;
			case 'cpy_active':
				    $operatorFirstPart = " cpy.active ";
				    break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(iets.invoice_create_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(iets.invoice_create_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(iets.invoice_create_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
			case 'hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'deal_hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' <= "' . $operator_arr['value'] . '" AND TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' > "0" ';
                break;
			case 'deal_day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' <= "' . $operator_arr['value'] . '" AND DATEDIFF(' . $operator_arr['field'] . ', now())' . ' > "0" ';
                break;
			case 'deal_month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' > "0" ';
                break;
			case 'deal_year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' > "0" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }
	
    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}

?>