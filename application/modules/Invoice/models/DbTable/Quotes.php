<?php
/**
* This is the DbTable class for the invoice_quote table.
*/
class Invoice_Model_DbTable_Quotes extends Eicra_Abstract_DbTable
{
	 /** Table name */
    protected $_name    =  'invoice_quote';
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }	
	
	public function getQuoteInfos($type_id) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1')
		{
			if(empty($type_id))
			{	
				$select = $this->select()
							   ->from(array('qt' => $this->_name),array('*'))		
							   ->orwhere('qt.user_id = ? ',$user_id)					  
							   ->order("qt.quote_create_date DESC"); 
			}
			else
			{
				$select = $this->select()
							   ->from(array('qt' => $this->_name),array('*'))			
							   ->orwhere('qt.user_id = ? ',$user_id)
							   ->where("qt.status = ?",$type_id)				  
							   ->order("qt.quote_create_date DESC");		
			}
		}
		else
		{
			if(empty($type_id))
			{	
				$select = $this->select()
							   ->from(array('qt' => $this->_name),array('*'))			  
							   ->order("qt.quote_create_date DESC"); 
			}
			else
			{
				$select = $this->select()
							   ->from(array('qt' => $this->_name),array('*'))	
							   ->where("qt.status = ?",$type_id)				  
							   ->order("qt.quote_create_date DESC");		
			}
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
}

?>