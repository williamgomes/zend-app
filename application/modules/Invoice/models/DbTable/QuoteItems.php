<?php
/**
* This is the DbTable class for the invoice_quote_items table.
*/
class Invoice_Model_DbTable_QuoteItems extends Eicra_Abstract_DbTable
{
	 /** Table name */
    protected $_name    =  'invoice_quote_items';
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }
	
	public function getQuoteItemsInfo($user_id,$quote_id = null) 
    {	
		if(empty($quote_id))
		{
			$select = $this->select()
						   ->from(array('qIt' => $this->_name),array('*'))					  
						   ->where("qIt.user_id = ?",$user_id); 
		}
		else
		{
			$select = $this->select()
						   ->from(array('qIt' => $this->_name),array('*'))					  
						   ->where("qIt.user_id = ?",$user_id)
						   ->where("qIt.quote_id = ?",$quote_id); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	public function getQuoteItemsInfoByQuoteId($quote_id) 
    {	
		
			$select = $this->select()
						   ->from(array('qIt' => $this->_name),array('*'))		
						   ->where("qIt.quote_id = ?",$quote_id); 
	
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
}
?>