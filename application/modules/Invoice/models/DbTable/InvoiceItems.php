<?php
/**
* This is the DbTable class for the invoice_items table.
*/
class Invoice_Model_DbTable_InvoiceItems extends Eicra_Abstract_DbTable
{
	 /** Table name */
    protected $_name    =  'invoice_items';
	
	public function getInvoiceItemsInfo($user_id,$invoice_id = null) 
    {	
		if(empty($invoice_id))
		{
			$select = $this->select()
						   ->from(array('invIt' => $this->_name),array('*'))					  
						   ->where("invIt.user_id = ?",$user_id); 
		}
		else
		{
			$select = $this->select()
						   ->from(array('invIt' => $this->_name),array('*'))					  
						   ->where("invIt.user_id = ?",$user_id)
						   ->where("invIt.invoice_id = ?",$invoice_id); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	public function getInvoiceItems($invoice_id = null) 
    {	
		if(empty($invoice_id))
		{
			$select = $this->select()
						   ->from(array('invIt' => $this->_name),array('*')); 
		}
		else
		{
			$select = $this->select()
						   ->from(array('invIt' => $this->_name),array('*'))
						   ->where("invIt.invoice_id = ?",$invoice_id); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
}
?>