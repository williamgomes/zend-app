<?php

class Invoice_Model_Invoices {

    protected $_id;
    protected $_user_id;
    protected $_payment_user_id;
    protected $_invoice_subject;
    protected $_invoice_desc;
    protected $_invoice_create_date;
    protected $_invoice_update_date;
    protected $_invoice_due_date;
    protected $_sub_total;
    protected $_tax;
    protected $_total;
    protected $_service_charge;
    protected $_deposit_charge;
    protected $_now_payable;
    protected $_status;
    protected $_payment_method;
    protected $_module_name;
    protected $_create_action;
    protected $_update_action;
    protected $_delete_action;
    protected $_query_action;
    protected $_paid_action;
    protected $_unpaid_action;
    protected $_cancel_action;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveInvoices() {
        try {
            $mapper = new Invoice_Model_InvoicesMapper();
            $return = $mapper->save($this);
        } catch (Exception $e) {
            $return = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $return;
    }

    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setUser_id($text = null) {
        if ($text) {
            $this->_user_id = $text;
        } else {
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $globalIdentity = $auth->getIdentity();
                $this->_user_id = $globalIdentity->user_id;
            } else {
                $this->_user_id = ($text) ? $text : '1';
            }
        }
        return $this;
    }

    public function setPayment_user_id($text) {
        $this->_payment_user_id = $text;
        return $this;
    }

    public function setInvoice_subject($text) {
        $this->_invoice_subject = $text;
        return $this;
    }

    public function setInvoice_desc($text) {
        $this->_invoice_desc = $text;
        return $this;
    }

    public function setInvoice_create_date($text) {
        $this->_invoice_create_date = $text;
        return $this;
    }

    public function setInvoice_update_date($text) {
        $this->_invoice_update_date = $text;
        return $this;
    }

    public function setInvoice_due_date($text) {
        $this->_invoice_due_date = $text;
        return $this;
    }

    public function setSub_total($text) {
        $this->_sub_total = $text;
        return $this;
    }

    public function setTax($text) {
        $this->_tax = $text;
        return $this;
    }

    public function setTotal($text) {
        $this->_total = $text;
        return $this;
    }

    public function setService_charge($text) {
        $this->_service_charge = (empty($text)) ? 0 : $text;
        return $this;
    }

    public function setDeposit_charge($text) {
        $this->_deposit_charge = (empty($text)) ? 0 : $text;
        return $this;
    }

    public function setNow_payable($text) {
        $this->_now_payable = (empty($text)) ? 0 : $text;
        return $this;
    }

    public function setStatus($text) {
        $this->_status = $text;
        return $this;
    }

    public function setPayment_method($text) {
        $this->_payment_method = $text;
        return $this;
    }

    public function setModule_name($text) {
        $this->_module_name = $text;
        return $this;
    }

    public function setCreate_action($text) {
        $this->_create_action = $text;
        return $this;
    }

    public function setUpdate_action($text) {
        $this->_update_action = $text;
        return $this;
    }

    public function setDelete_action($text) {
        $this->_delete_action = $text;
        return $this;
    }

    public function setQuery_action($text) {
        $this->_query_action = $text;
        return $this;
    }

    public function setPaid_action($text) {
        $this->_paid_action = $text;
        return $this;
    }

    public function setUnpaid_action($text) {
        $this->_unpaid_action = $text;
        return $this;
    }

    public function setCancel_action($text) {
        $this->_cancel_action = $text;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getUser_id() {
        if (empty($this->_user_id)) {
            $this->setUser_id();
        }
        return $this->_user_id;
    }

    public function getPayment_user_id() {
        return $this->_payment_user_id;
    }

    public function getInvoice_subject() {
        return $this->_invoice_subject;
    }

    public function getInvoice_desc() {
        return $this->_invoice_desc;
    }

    public function getInvoice_create_date() {
        return $this->_invoice_create_date;
    }

    public function getInvoice_update_date() {
        return $this->_invoice_update_date;
    }

    public function getInvoice_due_date() {
        return $this->_invoice_due_date;
    }

    public function getSub_total() {
        return $this->_sub_total;
    }

    public function getTax() {
        return $this->_tax;
    }

    public function getTotal() {
        return $this->_total;
    }

    public function getService_charge() {
        return $this->_service_charge;
    }

    public function getDeposit_charge() {
        return $this->_deposit_charge;
    }

    public function getNow_payable() {
        return $this->_now_payable;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getPayment_method() {
        return $this->_payment_method;
    }

    public function getModule_name() {
        return $this->_module_name;
    }

    public function getCreate_action() {
        return $this->_create_action;
    }

    public function getUpdate_action() {
        return $this->_update_action;
    }

    public function getDelete_action() {
        return $this->_delete_action;
    }

    public function getQuery_action() {
        return $this->_query_action;
    }

    public function getPaid_action() {
        return $this->_paid_action;
    }

    public function getUnpaid_action() {
        return $this->_unpaid_action;
    }

    public function getCancel_action() {
        return $this->_cancel_action;
    }

}

?>