<?php
class Invoice_Model_Orders
{
	protected $_id;
	protected $_user_id;
	protected $_order_date;	
	protected $_order_amount;
	protected $_payment_method;
	protected $_invoice_id;
	protected $_ipaddress;	
	protected $_status;	
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}			
	}
	
	 public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Auth property');
		}
		$this->$method($value);
	}
	
	
	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Auth property');
		}
		return $this->$method();
	}
 
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function saveOrders()
	{
		try
		{
			$mapper  = new Invoice_Model_OrdersMapper();
			$return = $mapper->save($this);
		}
		catch(Exception $e)
		{
			$return = array('status' => 'err', 'msg' => $e->getMessage());
		}
		return $return;
	}
	
	
	
	public function setId($text)
	{
		$this->_id = $text;
		return $this;
	}
	
	public function setUser_id($text)
	{
		if($text)
		{
			$this->_user_id = $text;
		}
		else
		{
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_user_id = $globalIdentity->user_id;
			}
			else
			{
				$this->_user_id = '1';
			}
		}
		return $this;
	}
	
	public function setOrder_date($text)
	{
		$this->_order_date = $text;
		return $this;
	}		
	
	public function setOrder_amount($text)
	{
		$this->_order_amount = $text;
		return $this;
	}		
	
	public function setPayment_method($text)
	{
		$this->_payment_method = $text;
		return $this;
	}
	
	public function setInvoice_id($text)
	{
		$this->_invoice_id = $text;
		return $this;
	}
	
	public function setIpaddress($text)
	{
		$this->_ipaddress = $text;
		return $this;
	}
	
	public function setStatus($text)
	{
		$this->_status = $text;
		return $this;
	}
			
	
	
	public function getId()
	{         
		return $this->_id;
	}
	
	public function getUser_id()
	{  
		if(empty($this->_user_id))
		{
			$this->setUser_id(null); 
		}        
		return $this->_user_id;
	}
	
	public function getOrder_date()
	{
		return $this->_order_date;
	}
	
	public function getOrder_amount()
	{
		return $this->_order_amount;
	}
	
	public function getPayment_method()
	{
		return $this->_payment_method;
	}
	
	public function getInvoice_id()
	{
		return $this->_invoice_id;
	}
	
	public function getIpaddress()
	{         
		return $this->_ipaddress;
	}
	
	public function getStatus()
	{         
		return $this->_status;
	}		
}
?>