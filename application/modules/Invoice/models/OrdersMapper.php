<?php
class Invoice_Model_OrdersMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Invoice_Model_DbTable_Orders');
        }
        return $this->_dbTable;
    }
	
	public function save(Invoice_Model_Orders $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				try 
				{
					unset($data['id']);
					
					$data = array(					
						'user_id' 					=> 	$obj->getUser_id(),
						'order_date' 				=>	$obj->getOrder_date(),
						'order_amount' 				=>	$obj->getOrder_amount(),						
						'payment_method'	 		=>	$obj->getPayment_method(),
						'invoice_id' 				=>	$obj->getInvoice_id(),
						'ipaddress'					=>	$obj->getIpaddress(),
						'status' 					=>	$obj->getStatus()
					);				
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{
				try 
				{
					$data = array(					
						'user_id' 					=> 	$obj->getUser_id(),
						'order_date' 				=>	$obj->getOrder_date(),
						'order_amount' 				=>	$obj->getOrder_amount(),						
						'payment_method'	 		=>	$obj->getPayment_method(),
						'invoice_id' 				=>	$obj->getInvoice_id(),
						'ipaddress'					=>	$obj->getIpaddress()						
					);
					
					// Start the Update process
					
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>