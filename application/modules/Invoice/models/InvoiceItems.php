<?php
class Invoice_Model_InvoiceItems
{
	protected $_id;
	protected $_user_id;
	protected $_invoice_id;	
	protected $_object_value;
	protected $_item_details;
	protected $_item_total;
	protected $_item_tax;
	
	
 
    	public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveInvoiceItems()
		{
			try
			{
				$mapper  = new Invoice_Model_InvoiceItemsMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = array('status' => 'err', 'msg' => $e->getMessage());
			}
			return $return;
		}
		
		
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setUser_id($text = null)
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_user_id = $globalIdentity->user_id;
			}
			else
			{
				$this->_user_id = '1';
			}
			return $this;
		}
		
		public function setInvoice_id($text)
		{
			$this->_invoice_id = $text;
			return $this;
		}
		
		public function setObject_value($text)
		{
			$this->_object_value = $text;
			return $this;
		}		
		
		public function setItem_details($text)
		{				
			$this->_item_details = 	$text;			
			return $this;
		}
		
		public function setItem_total($text)
		{
			$this->_item_total = $text;
			return $this;
		}
		
		public function setItem_tax($text)
		{
			$this->_item_tax = $text;
			return $this;
		}		
	
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getUser_id()
		{  
			if(empty($this->_user_id))
			{
				$this->setUser_id(); 
			}        
			return $this->_user_id;
		}
		
		public function getInvoice_id()
		{
			return $this->_invoice_id;
		}
		
		public function getObject_value()
		{
			return $this->_object_value;
		}
		
		public function getItem_details()
		{
			return $this->_item_details;
		}
		
		public function getItem_total()
		{
			return $this->_item_total;
		}
		
		public function getItem_tax()
		{
			return $this->_item_tax;
		}
			
}
?>