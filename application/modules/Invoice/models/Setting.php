<?php
class Invoice_Model_Setting
{
	protected $_id;
	protected $_module_name;
	protected $_role_id;
	protected $_default_template_id;
	protected $_paid_template_id;
	protected $_unpaid_template_id;
	protected $_cancel_template_id;
	protected $_delete_template_id;
	protected $_refund_template_id;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth Setting');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth Setting');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveSetting()
		{
			$mapper  = new Invoice_Model_SettingMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}		
		
		public function setModule_name($text)
		{
			$this->_module_name = $text;
			return $this;
		}
		
		public function setRole_id($text)
		{
			$this->_role_id = $text;
			return $this;
		}
		
		public function setUnpaid_template_id($text)
		{
			$this->_unpaid_template_id = $text;
			return $this;
		}
		
		public function setCancel_template_id($text)
		{
			$this->_cancel_template_id = $text;
			return $this;
		}
		
		public function setDelete_template_id($text)
		{
			$this->_delete_template_id = $text;
			return $this;
		}			
		
		public function setDefault_template_id($text)
		{
			$this->_default_template_id = $text;
			return $this;
		}
		
		public function setPaid_template_id($text)
		{
			$this->_paid_template_id = $text;
			return $this;
		}
		
		public function setRefund_template_id($text)
		{
			$this->_refund_template_id = $text;
			return $this;
		}
		
		
		
		
		public function getId()
		{         
			return $this->_id;
		}
		public function getModule_name()
		{
			return $this->_module_name;
		}
		
		public function getRole_id()
		{
			return $this->_role_id;
		}
		
		public function getDefault_template_id()
		{
			return $this->_default_template_id;
		}
		
		public function getPaid_template_id()
		{
			return $this->_paid_template_id;
		}
		
		public function getUnpaid_template_id()
		{
			return $this->_unpaid_template_id;
		}
		
		public function getCancel_template_id()
		{
			return $this->_cancel_template_id;
		}
		
		public function getDelete_template_id()
		{
			return $this->_delete_template_id;
		}
		
		public function getRefund_template_id()
		{
			return $this->_refund_template_id;
		}
			
}
?>