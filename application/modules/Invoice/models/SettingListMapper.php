<?php
class Invoice_Model_SettingListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Invoice_Model_DbTable_Setting');
        }
        return $this->_dbTable;
    }
	
	public function fetchAll($pageNumber,$status = null, $search_params = null, $tableColumns= null)
    {				
        $resultSet = $this->getDbTable()->getListInfo($status, $search_params, $tableColumns); 				     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
}
?>