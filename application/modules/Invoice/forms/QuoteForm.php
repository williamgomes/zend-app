<?php

class Invoice_Form_QuoteForm  extends Zend_Form {

		protected $_options;
		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
			$this->_options = $options;			
            $config = (file_exists( APPLICATION_PATH.'/modules/Invoice/forms/source/'.$translator->getLangFile().'.QuoteForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Invoice/forms/source/'.$translator->getLangFile().'.QuoteForm.ini', 'quote') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Invoice/forms/source/en_US.QuoteForm.ini', 'quote');
            parent::__construct($config->quote );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->loadUsers ($this->user_id);
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}		
		
		private function setFileDecorators($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_File')
			{
				$element->setDecorators(array('file','file'));			
				$element->setMaxFileSize($this->_options['attach_file_max_size']);
			}			
		}	
		
		public function loadUsers ($element)
		{			
			$memberList = new Members_Model_DbTable_MemberList();			
        	$memberList_options = $memberList->getAllMembers();	
			$translator = Zend_Registry::get('translator');
			$element->setRegisterInArrayValidator(false);			
			$element->addMultiOption('',$translator->translator('common_select'));
			$element->addMultiOptions($memberList_options);	 								 
		}
		
		public function setEditor($baseURL)
		{
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "none",
								theme : "advanced",
								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "insertimage,image,media,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,insertdate,inserttime,preview,print,|,ltr,rtl",
								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help",
								theme_advanced_buttons3 : "tablecontrols,hr,removeformat,visualaid,sub,sup,charmap,emotions,advhr",
								theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,|,styleprops,del,ins,attribs,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor,|,fullscreen",
								theme_advanced_buttons5 : "fontsizeselect,formatselect,fontselect,styleselect,|,code,iespell",
								theme_advanced_toolbar_location : "top",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								
								
								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type]",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
        						remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});							
							function loadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceAddControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\';
								
							}
							function unloadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceRemoveControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\';
							}
						</script>';
		}
		
		public function getEditor()
		{
			echo $this->_editor;
		} 
		
		public function addDisplayGroups(array $groups_arr)
		{
			foreach($groups_arr as $groups)
			{
			
				foreach ($groups as $key => $spec) {
					$name = null;
					if (!is_numeric($key)) {
						$name = $key;
					}
		
					if ($spec instanceof Zend_Form_DisplayGroup) {
						$this->_addDisplayGroupObject($spec);
					}
		
					if (!is_array($spec) || empty($spec)) {
						continue;
					}
		
					$argc    = count($spec);
					$options = array();
		
					if (isset($spec['elements'])) {
						$elements = $spec['elements'];
						if (isset($spec['name'])) {
							$name = $spec['name'];
						}
						if (isset($spec['options'])) {
							$options = $spec['options'];
						}
						$this->addDisplayGroup($elements, $name, $options);
					} else {
						switch ($argc) {
							case (1 <= $argc):
								$elements = array_shift($spec);
								if (!is_array($elements) && (null !== $name)) {
									$elements = array_merge((array) $elements, $spec);
									$this->addDisplayGroup($elements, $name);
									break;
								}
							case (2 <= $argc):
								if (null !== $name) {
									$options = array_shift($spec);
									$this->addDisplayGroup($elements, $name, $options);
									break;
								}
								$name = array_shift($spec);
							case (3 <= $argc):
								$options = array_shift($spec);
							default:
								$this->addDisplayGroup($elements, $name, $options);
						}
					}
				}
			}
			return $this;
		} 		 
}