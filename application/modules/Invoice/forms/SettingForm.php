<?php
class Invoice_Form_SettingForm  extends Zend_Form {

		protected $_options;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
			$this->_options = $options;
            $config = (file_exists(APPLICATION_PATH.'/modules/Invoice/forms/source/'.$translator->getLangFile().'.SettingForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Invoice/forms/source/'.$translator->getLangFile().'.SettingForm.ini', 'settings') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Invoice/forms/source/en_US.SettingForm.ini', 'settings');
            parent::__construct($config->settings );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->setElementTrueValue($this->module_name);	
			 $this->loadTemplateList($this->default_template_id);	
			 $this->loadTemplateList($this->paid_template_id);		
			 $this->loadTemplateList($this->unpaid_template_id);		
			 $this->loadTemplateList($this->cancel_template_id);		
			 $this->loadTemplateList($this->delete_template_id);	
			 $this->loadUserGroup ();		
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}	
		
		private function setElementTrueValue($element)
		{
			$element->setRegisterInArrayValidator(false);			
			$options = $element->getMultiOptions();
			$element->clearMultiOptions();
			foreach($options as $key=>$value)
			{
				if($key == '_')
				{
					$element->addMultiOption('',$value);
				}
				else
				{
					$element->addMultiOption($key,$value);
				}
			}
			$element->setRegisterInArrayValidator(false);
		} 	
		
		private function loadTemplateList($element)
		{
			$templates_db = new Newsletter_Model_DbTable_Templates();
			$templates_list	=	$templates_db->getTemplatesData('invoice_template') ;
			
			$translator = Zend_Registry::get('translator');
			
			$element->addMultiOption('',$translator->translator('common_select'));
			foreach($templates_list as $key=>$value)
			{
				$element->addMultiOption($key,$value);
			}
			$element->setRegisterInArrayValidator(false);
		}	
		
		//Load File User Group List in combobox
		public function loadUserGroup ()
		{	
			$translator = Zend_Registry::get('translator');
			$group = new Members_Model_DbTable_Role();						
			$group_options = $group->getOptions();
			if($this->role_id->getType() == 'Zend_Form_Element_Select')
			{
				$this->role_id->addMultiOption("",$translator->translator('common_select_user_group'));	
			}			
			$this->role_id->addMultiOptions($group_options);									 
		}		  
}