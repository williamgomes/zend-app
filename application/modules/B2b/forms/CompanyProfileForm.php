<?php
class B2b_Form_CompanyProfileForm extends Zend_Form
{
    public function __construct ($options = null)
    {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(APPLICATION_PATH . '/modules/B2b/forms/source/' . $translator->getLangFile() . '.CompanyProfileForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/B2b/forms/source/' . $translator->getLangFile() . '.CompanyProfileForm.ini', 'profile') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/B2b/forms/source/en_US.CompanyProfileForm.ini', 'profile');
        parent::__construct($config->profile);
    }
    public function init ()
    {
        $this->createForm();
    }
    public function createForm ()
    {
        $this->elementDecorator();
        $this->loadUserGroup();
        $this->setElementTrueValue($this->main_markets);
        $this->setElementTrueValue($this->delivery_terms);
        $this->setElementTrueValue($this->payment_currency);
        $this->setElementTrueValue($this->payment_type);
        $this->setElementTrueValue($this->language);
        $this->setElementTrueValue($this->certification);
        $this->setElementTrueValue($this->businesstype_id);
        $this->setElementTrueValue($this->annual_sales);
        //$this->setElementTrueValue($this->compliances);
        $this->setElementTrueValue($this->export_percentage);
        $this->setElementTrueValue($this->group_id);
        $this->loadReviewGroup();
        $this->loadBusinessType();
        $this->loadCompliances();
        $this->loadGroups();
        $this->loadCountries();
        $this->doSecurityFiltering();
    }
    public function doSecurityFiltering ()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(array(
            'match' => Eicra_File_Constants::FILTER_PATTERN,
            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT
        ));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    private function setElementTrueValue ($element)
    {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach ($options as $key => $value) {
            if ($key == '_') {
                $element->addMultiOption('', $value);
            } else {
                $element->addMultiOption($value, $value);
            }
        }
        $element->setRegisterInArrayValidator(false);
    }
    public function addElementFilters (array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }
    private function elementDecorator ()
    {
        $this->setElementDecorators(array(
            'ViewHelper',
            'FormElements'
        ));
    }
    private function loadReviewGroup ()
    {
        $reviewGroup = new Review_Model_DbTable_Setting();
        $reviewGroup_options = $reviewGroup->getAllReviews();
    }
    private function loadUserGroup ()
    {
        $userGroup = new Members_Model_DbTable_Role();
        $userGroup_options = $userGroup->getOptions();
    }
    public function loadBusinessType ()
    {
        $bizGroup = new B2b_Model_DbTable_Group();
        $bizDefaultGroup = $bizGroup->getDefaultGroupId();
        $translator = Zend_Registry::get('translator');
        $bizType = new B2b_Model_DbTable_BusinessType();
        $bizTypeList = $bizType->getSelectOptions();
        //$this->businesstype_id->addMultiOption('', $translator->translator('b2b_select_businessType'));
        $this->businesstype_id->addMultiOptions($bizTypeList);
    }
    public function loadCompliances  ()
    {
        $translator = Zend_Registry::get('translator');
        $compliancesTbl = new B2b_Model_DbTable_Compliances();
        $bizTypeList = $compliancesTbl->getAllCompliances();
       // $this->compliances->addMultiOption('', $translator->translator('b2b_select_compliances'));
        $this->compliances->addMultiOptions($bizTypeList);
		$this->compliances->setRegisterInArrayValidator(false);
    }
    public function loadCountries ()
    {
        $translator = Zend_Registry::get('translator');
        $this->country->setRegisterInArrayValidator(false);
        $global_conf = Zend_Registry::get('global_conf');
        $countries = new Eicra_Model_DbTable_Country();
        $countries_options = $countries->getOptions();
        $selected = $global_conf['default_country'];
        $this->country->addMultiOption('', $translator->translator('common_select_country'));
        foreach ($countries_options as $key => $value) {
            if ($selected == $key) {
                $this->country->addMultiOption($key, $value);
                $this->country->setValue($selected);
            }
            else
            {
                $this->country->addMultiOption($key, $value);
            }
        }
    }
    public function loadGroups ()
    {
        $bizGroup = new B2b_Model_DbTable_Group();
        $allGroups = $bizGroup->getGroupInfo();
        $this->group_id->addMultiOption('','Please Select ');
        if (!empty($allGroups)){
            $this->group_id->addMultiOptions($allGroups);
        }
    }
}