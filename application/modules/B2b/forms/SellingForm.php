<?php
class B2b_Form_SellingForm  extends Zend_Form
{
	private $_auth;
	public function __construct($options = null)
	{
		$this->_auth = $options;
		$translator = Zend_Registry::get('translator');
		$config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.SellingForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.SellingForm.ini', 'selling') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.SellingForm.ini', 'selling');
		parent::__construct($config->selling );
	}
	public function init()
	{
		$this->createForm();
	}
	public function createForm ()
	{
		$this->elementDecorator();
		$this->setElementTrueValue($this->price_currency_locale);
		$this->setElementTrueValue($this->qty_per_unit);
		$this->setElementTrueValue($this->primary_file_field);
		$this->setElementTrueValue($this->unit_size);
		$this->setElementTrueValue($this->price_per_unit);
		$this->setElementTrueValue($this->fob_price);
		$this->setElementTrueValue($this->delivery_leadtime);
		$this->setElementTrueValue($this->payment_type);
		//$this->setElementTrueValue($this->type);
		$this->setElementTrueValue($this->related_items);
		//$this->setElementTrueValue($this->product_grp_id);
		$this->setElementTrueValue($this->user_id);
 		$this->setElementTrueValue($this->group_id);
		$this->doSecurityFiltering();
		$this->loadRelatedItems ();
		$this->loadOwner ($this->user_id);
		$this->loadGroups();
	}
	private function setElementTrueValue($element)
	{
	    $element->setRegisterInArrayValidator(false);
	    $options = $element->getMultiOptions();
	    $element->clearMultiOptions();

	    foreach($options as $key=>$value)
	    {
	        if($key == '_')

	        {
	            $element->addMultiOption('',$value);
	        }

	        else

	        {

	            $element->addMultiOption($value,$value);

	        }

	    }

	    $element->setRegisterInArrayValidator(false);

	}

	//set Filters
	public function doSecurityFiltering()
	{
		$filters = array();
		$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
				'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
		$filters[0] = $filter;
		$this->addElementFilters($filters);
	}
	//Add Global Filters
	public function addElementFilters(array $filters)
	{
		foreach ($this->getElements() as $element) {
			$element->addFilters($filters);
		}
		return $this;
	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
	}

	public function loadB2bGroup ()
	{
	    $b2bGroup = new B2b_Model_DbTable_Group();
	    $allGroups = $b2bGroup->getGroupInfo();
	    $translator = Zend_Registry::get('translator');
	    $this->group_id->addMultiOption('',$translator->translator('b2b_select'));
	    $this->group_id->addMultiOptions($allGroups);
	    $this->group_id->setValue($translator->translator('b2b_select'));


	}
	
	private function loadRelatedItems ()
	{
		
	    $this->related_items->setRegisterInArrayValidator(false);
		if($this->_auth && $this->_auth->hasIdentity ())
		{
			$user_id = ($this->_auth->getIdentity()->access_other_user_article != '1') ? $this->_auth->getIdentity()->user_id : null;
			$relatedItems = new B2b_Model_DbTable_Selling();
			$relatedItems_options = $relatedItems->getExistingLeads($user_id);
			if($relatedItems_options)
			{				
				foreach($relatedItems_options as $options)
				{					
					$this->related_items->addMultiOption($options['id'], $options['name']);
				}
			}
		}
	}
	
	public function loadOwner ($element)
	{			
		$memberList = new Members_Model_DbTable_MemberList();			
		$memberList_options = $memberList->getAllMembers();	
		$translator = Zend_Registry::get('translator');
		$element->setRegisterInArrayValidator(false);			
		$element->addMultiOption('',$translator->translator('common_select'));
		$element->addMultiOptions($memberList_options);	
		if($this->_auth && $this->_auth->hasIdentity ())
		{ 
			$element->setValue($this->_auth->getIdentity()->user_id);
		}
	}
	
 	public function loadGroups ()
 	{
 		$translator = Zend_Registry::get('translator');
 		$bizGroup = new B2b_Model_DbTable_Group();
 		$allGroups = $bizGroup->getGroupInfo();
 		$this->group_id->addMultiOption('',$translator->translator('b2b_select'));
 		if (!empty($allGroups))
		{
 			$this->group_id->addMultiOptions($allGroups);
 		} 		
 	}
}