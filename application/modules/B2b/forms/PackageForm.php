<?php

class B2b_Form_PackageForm extends Zend_Form
{

    public function __construct ($options = null)
    {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(APPLICATION_PATH . '/modules/B2b/forms/source/' . $translator->getLangFile() . '.PackageForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/B2b/forms/source/' . $translator->getLangFile() . '.PackageForm.ini', 'package') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/B2b/forms/source/en_US.PackageForm.ini', 'package');
        parent::__construct($config->package);
    }

    public function init ()
    {
        $this->createForm();
    }

    public function createForm ()
    {
        $this->elementDecorator();

        $this->doSecurityFiltering();
    }

    private function setElementTrueValue ($element)
    {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach ($options as $key => $value) {
            if ($key == '_') {
                $element->addMultiOption('', $value);
            } else {
                $element->addMultiOption($value, $value);
            }
        }
        $element->setRegisterInArrayValidator(false);
    }
    // set Filters
    public function doSecurityFiltering ()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(array(
            'match' => Eicra_File_Constants::FILTER_PATTERN,
            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT
        ));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    // Add Global Filters
    public function addElementFilters (array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }
    // Element Decorator
    private function elementDecorator ()
    {
        $this->setElementDecorators(array(
            'ViewHelper',
            'FormElements'
        ));
    }

    public function loadGroups ()
    {
        $bizGroup = new B2b_Model_DbTable_Group();
        $allGroups = $bizGroup->getGroupInfo();
        $this->group_id->addMultiOption('', 'Please Select ');
        if (! empty($allGroups)) {
            $this->group_id->addMultiOptions($allGroups);
        }
    }
}