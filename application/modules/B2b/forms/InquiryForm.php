<?php
class B2b_Form_InquiryForm  extends Zend_Form
{
	public function __construct($options = null)
	{
		$translator = Zend_Registry::get('translator');
		$config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.InquiryForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.InquiryForm.ini', 'inquiry') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.InquiryForm.ini', 'inquiry');
		parent::__construct($config->inquiry );
	}
	public function init()
	{
		$this->createForm();
	}
	public function createForm ()
	{
		$this->elementDecorator();
		$this->setElementTrueValue($this->reply_with);
		$this->setElementTrueValue($this->trade_type);
		$this->setElementTrueValue($this->feedbacks);
 		$this->setElementTrueValue($this->trade_type);
		$this->doSecurityFiltering();
	}

	private function setElementTrueValue($element)
	{
	    $element->setRegisterInArrayValidator(false);
	    $options = $element->getMultiOptions();
	    $element->clearMultiOptions();

	    foreach($options as $key=>$value)
	    {
	        if($key == '_')

	        {
	            $element->addMultiOption('',$value);
	        }

	        else

	        {

	            $element->addMultiOption($value,$value);

	        }

	    }

	    $element->setRegisterInArrayValidator(false);

	}

	//set Filters
	public function doSecurityFiltering()
	{
		$filters = array();
		$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
				'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
		$filters[0] = $filter;
		$this->addElementFilters($filters);
	}
	//Add Global Filters
	public function addElementFilters(array $filters)
	{
		foreach ($this->getElements() as $element) {
			$element->addFilters($filters);
		}
		return $this;
	}

	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array('ViewHelper','FormElements'));
		$this->security_captcha->setDecorators(array());
	}

	public function loadB2bGroup ()
	{
	    $b2bGroup = new B2b_Model_DbTable_Group();
	    $allGroups = $b2bGroup->getGroupInfo();
	    $translator = Zend_Registry::get('translator');
	    $this->group_id->addMultiOption('',$translator->translator('b2b_select'));
	    $this->group_id->addMultiOptions($allGroups);

	}

}