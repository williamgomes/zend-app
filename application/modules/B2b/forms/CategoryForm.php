<?php

class B2b_Form_CategoryForm extends Zend_Form
{

    public function __construct ($options = null)
    {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(
                APPLICATION_PATH . '/modules/B2b/forms/source/' .
                         $translator->getLangFile() . '.CategoryForm.ini')) ? new Zend_Config_Ini(
                APPLICATION_PATH . '/modules/B2b/forms/source/' .
                 $translator->getLangFile() . '.CategoryForm.ini', 'category') : new Zend_Config_Ini(
                APPLICATION_PATH .
                 '/modules/B2b/forms/source/en_US.CategoryForm.ini', 'category');
        parent::__construct($config->category);
    }

    public function init ()
    {
        $this->createForm();
    }

    public function createForm ()
    {
        $this->elementDecorator();
        $this->setElementTrueValue($this->group_id);
        $this->loadGroups();
        $this->loadPackages();
        $this->doSecurityFiltering();
    }

    private function setElementTrueValue ($element)
    {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach ($options as $key => $value) {
            if ($key == '_') {
                $element->addMultiOption('', $value);
            } else {
                $element->addMultiOption($value, $value);
            }
        }
        $element->setRegisterInArrayValidator(false);
    }
    // set Filters
    public function doSecurityFiltering ()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(
                array('match' => Eicra_File_Constants::FILTER_PATTERN,
                        'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    // Add Global Filters
    public function addElementFilters (array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }

    // Element Decorator
    private function elementDecorator ()
    {
        $this->setElementDecorators(array('ViewHelper', 'FormElements'));
    }

    private function loadDynamicForms ()
    {
        $dynamicForm = new Members_Model_DbTable_Forms();
        $dynamicForm_options = $dynamicForm->getAllForms();
        if ($dynamicForm_options) {
            $this->dynamic_form->addMultiOptions($dynamicForm_options);
        }
    }

    private function loadReviewGroup ()
    {
        $reviewGroup = new Review_Model_DbTable_Setting();
        $reviewGroup_options = $reviewGroup->getAllReviews();
        $this->review_id->addMultiOptions($reviewGroup_options);
    }

    private function loadUserGroup ()
    {
        $userGroup = new Members_Model_DbTable_Role();
        $userGroup_options = $userGroup->getOptions();
        $this->role_id->addMultiOptions($userGroup_options);
    }

    public function loadGroups ()
    {
        $bizGroup = new B2b_Model_DbTable_Group();
        $allGroups = $bizGroup->getGroupInfo();
        if (! empty($allGroups)) {
            $this->group_id->addMultiOptions($allGroups);
        }
    }

    public function loadCategories ()
    {
        $categoryTbl = new B2b_Model_DbTable_Category();
        $categories = $categoryTbl->getCategoryInfo();
        $this->parent_id->addMultiOption('0', 'Root');
        if (! empty($categories)) {
            $this->parent_id->addMultiOptions($categories);
        }
    }

    public function loadPackages ()
    {
        $translator = Zend_Registry::get('translator');
        $package_table = new B2b_Model_DbTable_Package();
        $packages = $package_table->getPackagesPairs();
        $this->package_id->addMultiOption('0',
                $translator->translator('b2b_category_form_select_all'));

        if (! empty($packages)) {
            $this->package_id->addMultiOptions($packages);
        }
    }
}