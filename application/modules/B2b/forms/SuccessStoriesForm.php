<?php
class B2b_Form_SuccessStoriesForm  extends Zend_Form
{
    public function __construct($options = null)
    {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.SuccessStoriesForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.SuccessStoriesForm.ini', 'sstories') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.SuccessStoriesForm.ini', 'sstories');
        parent::__construct($config->sstories );
    }
    public function init()
    {
        $this->createForm();
        $this->loadUsers();
    }

    public function createForm ()
    {
        $this->elementDecorator();
        $this->doSecurityFiltering();
    }


    //set Filters
    public function doSecurityFiltering()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    //Add Global Filters
    public function addElementFilters(array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }
    //Element Decorator
    private function elementDecorator()
    {
        $this->setElementDecorators(array('ViewHelper','FormElements'));
    }


    public function loadUsers ()
    {
        $translator = Zend_Registry::get('translator');
        $memberList = new Members_Model_DbTable_MemberList();
        $allMembers = $memberList->getMembers()->toArray();
        if (count($allMembers) && ! empty($allMembers)) {
            $userList = array();
            $userList[''] = $translator->translator('b2b_select');
            foreach ($allMembers as $row) {
                $userList[$row['user_id']] = $row['title'] . '  ' . $row['firstName'] . '  ' . $row['lastName'] . '  ';
            }
            $this->user_id->setMultiOptions($userList);

        }
    }
}