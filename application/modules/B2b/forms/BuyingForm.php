<?php
class B2b_Form_BuyingForm extends Zend_Form
{
	private $_auth;

    public function __construct ($options = null)
    {
		$this->_auth = $options;
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(
                APPLICATION_PATH . '/modules/B2b/forms/source/' .
                         $translator->getLangFile() . '.BuyingForm.ini')) ? new Zend_Config_Ini(
                APPLICATION_PATH . '/modules/B2b/forms/source/' .
                 $translator->getLangFile() . '.BuyingForm.ini', 'buying') : new Zend_Config_Ini(
                APPLICATION_PATH .
                 '/modules/B2b/forms/source/en_US.BuyingForm.ini', 'buying');
        parent::__construct($config->buying);
    }

    public function init ()
    {
        $this->createForm();
    }

    public function createForm ()
    {
        $this->elementDecorator();
        $this->setElementTrueValue($this->price_currency_locale);
        $this->setElementTrueValue($this->qty_per_unit);
        $this->setElementTrueValue($this->price_per_unit);
        $this->setElementTrueValue($this->max_buying_unit);
        $this->setElementTrueValue($this->delivery_leadtime);
        $this->setElementTrueValue($this->payment_type);
        $this->loadCountries($this->country);
        $this->setElementTrueValue($this->shipping_price);
        $this->setElementTrueValue($this->user_id);
        $this->setElementTrueValue($this->group_id);
		$this->loadOwner ($this->user_id);
		$this->loadGroups();
        $this->doSecurityFiltering();
    }

    private function setElementTrueValue ($element)
    {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach ($options as $key => $value) {
            if ($key == '_') {
                $element->addMultiOption('', $value);
            } else {
                $element->addMultiOption($value, $value);
            }
        }
        $element->setRegisterInArrayValidator(false);
    }
    // set Filters
    public function doSecurityFiltering ()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(
                array('match' => Eicra_File_Constants::FILTER_PATTERN,
                        'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    // Add Global Filters
    public function addElementFilters (array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }
    // Element Decorator
    private function elementDecorator ()
    {
        $this->setElementDecorators(array('ViewHelper', 'FormElements'));
    }

    public function loadCountries ($element)
    {
        $translator = Zend_Registry::get('translator');
        $element->setRegisterInArrayValidator(false);
        $global_conf = Zend_Registry::get('global_conf');
        $countries = new Eicra_Model_DbTable_Country();
        $countries_options = $countries->getOptions();
        $selected = $global_conf['default_country'];
        $element->addMultiOption('0',
                $translator->translator('select_a_Country'));
        foreach ($countries_options as $key => $value) {
            if ($selected == $key) {
                $element->addMultiOption($key, $value);
                $element->setValue($selected);
            } else {
                $element->addMultiOption($key, $value);
            }
        }
    }

    public function loadAllCountries ()
    {
    	$translator = Zend_Registry::get('translator');
    	$this->country->setRegisterInArrayValidator(false);
    	$global_conf = Zend_Registry::get('global_conf');
    	$countries = new Eicra_Model_DbTable_Country();
    	$countries_options = $countries->getOptions();
    	$selected = $global_conf['default_country'];
    	$this->country->addMultiOption('0',
    			$translator->translator('select_a_Country'));
    	foreach ($countries_options as $key => $value) {
    		if ($selected == $key) {
    			$this->country->addMultiOption($key, $value);
    			$this->country->setValue($selected);
    		} else {
    			$this->country->addMultiOption($key, $value);
    		}
    	}
    }

    public function loadB2bGroup ()
    {
        $b2bGroup = new B2b_Model_DbTable_Group();
        $allGroups = $b2bGroup->getGroupInfo();
        $translator = Zend_Registry::get('translator');
        $this->group_id->addMultiOption('',
                $translator->translator('b2b_select'));
        $this->group_id->addMultiOptions($allGroups);
        $this->group_id->setValue($translator->translator('b2b_select'));
    }
	
	public function loadOwner ($element)
	{			
		$memberList = new Members_Model_DbTable_MemberList();			
		$memberList_options = $memberList->getAllMembers();	
		$translator = Zend_Registry::get('translator');
		$element->setRegisterInArrayValidator(false);			
		$element->addMultiOption('',$translator->translator('common_select'));
		$element->addMultiOptions($memberList_options);	
		if($this->_auth && $this->_auth->hasIdentity ())
		{ 
			$element->setValue($this->_auth->getIdentity()->user_id);
		}
	}
	
 	public function loadGroups ()
 	{
 		$translator = Zend_Registry::get('translator');
 		$bizGroup = new B2b_Model_DbTable_Group();
 		$allGroups = $bizGroup->getGroupInfo();
 		$this->group_id->addMultiOption('',$translator->translator('b2b_select'));
 		if (!empty($allGroups))
		{
 			$this->group_id->addMultiOptions($allGroups);
 		} 		
 	}
}