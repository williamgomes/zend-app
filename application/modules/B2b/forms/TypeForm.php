<?php
class B2b_Form_TypeForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null)
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.TypeForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.TypeForm.ini', 'type') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.TypeForm.ini', 'type');
            parent::__construct($config->type );
        }

        public function init()
        {
             $this->createForm();
        }

        public function createForm ()
		{
			 $this->elementDecorator();
			 $this->loadGroup();
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',
					
				));
		}
		
		public function loadGroup ()
		{
			$b2bGroup = new B2b_Model_DbTable_Group();
        	$b2bGroup_options = $b2bGroup->getGroupInfo();
			$translator = Zend_Registry::get('translator');
			$this->group_id->addMultiOption('',$translator->translator('b2b_group_add_edit_select'));
			$this->group_id->addMultiOptions($b2bGroup_options);
		}
		 	  
}