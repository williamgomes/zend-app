<?php
class B2b_Form_TradeShowForm  extends Zend_Form
{

    public function __construct($options = null)
    {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.TradeShowsForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.TradeShowsForm.ini', 'tradeshow') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.TradeShowsForm.ini', 'tradeshow');
        parent::__construct($config->tradeshow );
    }
    public function init()
    {
        $this->createForm();


    }
    public function createForm ()
    {
        $this->elementDecorator();
        $this->doSecurityFiltering();
        $this->loadCountries($this->country);
        $this->loadUsers();
    }
    private function setElementTrueValue($element)
    {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach($options as $key=>$value)
        {
            if($key == '_')
            {
                $element->addMultiOption('',$value);
            }
            else
            {
                $element->addMultiOption($value,$value);
            }
        }
        $element->setRegisterInArrayValidator(false);
    }
    //set Filters
    public function doSecurityFiltering()
    {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }
    //Add Global Filters
    public function addElementFilters(array $filters)
    {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }
    //Element Decorator
    private function elementDecorator()
    {
        $this->setElementDecorators(array('ViewHelper','FormElements'));
    }
    public function loadGroups ()
    {
    	$bizGroup = new B2b_Model_DbTable_Group();
    	$allGroups = $bizGroup->getGroupInfo();
    	$this->group_id->addMultiOption('','Please Select ');
    	if (!empty($allGroups)){
    		$this->group_id->addMultiOptions($allGroups);
    	}

    }

    public function loadCountries ($element)
    {
        $translator = Zend_Registry::get('translator');
        $element->setRegisterInArrayValidator(false);
        $global_conf = Zend_Registry::get('global_conf');
        $countries = new Eicra_Model_DbTable_Country();
        $countries_options = $countries->getOptions();
        $selected = $global_conf['default_country'];
        $element->addMultiOption('', $translator->translator('common_select_country'));

        foreach ($countries_options as $key => $value) {
            if ($selected == $key) {
                $element->addMultiOption($key, $value);
                $element->setValue($selected);
            }

            else

            {
                $element->addMultiOption($key, $value);
            }
        }
    }


    public function loadUsers ()
    {
        $translator = Zend_Registry::get('translator');
        $memberList = new Members_Model_DbTable_MemberList();
        $allMembers = $memberList->getMembers()->toArray();
        if (count($allMembers) && ! empty($allMembers)) {
            $userList = array();
            $userList[''] = $translator->translator('b2b_select');
            foreach ($allMembers as $row) {
                $userList[$row['user_id']] = $row['title'] . '  ' . $row['firstName'] . '  ' . $row['lastName'] . '  ';
            }
            $this->user_id->setMultiOptions($userList);

        }
    }

}