<?php
class B2b_Form_UserPreferencesForm  extends Zend_Form
{
	//en_US.UserPreferencesForm.ini
	public function __construct($options = null)
	{
		$translator = Zend_Registry::get('translator');

		$config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.UserPreferencesForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.UserPreferencesForm.ini', 'preferences') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.UserPreferencesForm.ini', 'preferences');

		parent::__construct($config->preferences );

	}
	
	public function init()

	{

		$this->createForm();

	}



	public function createForm ()
	{

		$this->elementDecorator();
		$this->loadUserGroup ();
		$this->doSecurityFiltering();

	}

	public function doSecurityFiltering()

	{

		$filters = array();

		$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,

				'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));

		$filters[0] = $filter;

		$this->addElementFilters($filters);

	}

	private function setElementTrueValue($element)

	{

		$element->setRegisterInArrayValidator(false);

		$options = $element->getMultiOptions();

		$element->clearMultiOptions();

		foreach($options as $key=>$value)

		{

			if($key == '_')

			{

				$element->addMultiOption('',$value);

			}

			else

			{

				$element->addMultiOption($value,$value);

			}

		}

		$element->setRegisterInArrayValidator(false);

	}



	public function addElementFilters(array $filters)



	{



		foreach ($this->getElements() as $element) {



			$element->addFilters($filters);



		}



		return $this;



	}



	private function elementDecorator()



	{



		$this->setElementDecorators(array('ViewHelper','FormElements'));



	}



	private function loadReviewGroup ()



	{

		$reviewGroup = new Review_Model_DbTable_Setting();



		$reviewGroup_options = $reviewGroup->getAllReviews();

	}

	private function loadUserGroup ()

	{

		$userGroup = new Members_Model_DbTable_Role();

		$userGroup_options = $userGroup->getOptions();

	}

	public function loadBusinessType ()

	{

		$bizGroup = new B2b_Model_DbTable_Group();

		$bizDefaultGroup = $bizGroup->getDefaultGroupId();

		$translator = Zend_Registry::get('translator');

	  

		$bizType = new  B2b_Model_DbTable_BusinessType();

		$bizTypeList = $bizType->getSelectOptions($bizDefaultGroup['id']);

	  

	  

		$this->businesstype_id->addMultiOption('',$translator->translator('b2b_select_businessType'));

		$this->businesstype_id->addMultiOptions($bizTypeList);





	}



	public function loadCountries($element)

	{



		$translator = Zend_Registry::get('translator');



		$element->setRegisterInArrayValidator(false);



		$global_conf = Zend_Registry::get('global_conf');



		$countries = new Eicra_Model_DbTable_Country();



		$countries_options = $countries->getOptions();



		$selected = $global_conf['default_country'];



		$element->addMultiOption('',$translator->translator('hotels_select_country'));



		foreach($countries_options as $key=>$value)

		{

			if($selected == $key)

			{

				$element->addMultiOption($key,$value);

				$element->setValue($selected);

			}



			else



			{

				$element->addMultiOption($key,$value);



			}



		}



	}



}