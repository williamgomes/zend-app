<?php
class B2b_Form_ProductGroupForm  extends Zend_Form
{
    
		public function __construct($options = null)
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.ProductGroupForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/'.$translator->getLangFile().'.ProductGroupForm.ini', 'product_group') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/B2b/forms/source/en_US.ProductGroupForm.ini', 'product_group');
            parent::__construct($config->productgrp );
        }
        public function init()
        {
            $this->createForm();
        }
        public function createForm ()
		{
		 	$this->elementDecorator();
	
			//$this->Image->setRegisterInArrayValidator(false);
			$this->doSecurityFiltering();
        }
		
		private function setElementTrueValue($element)
		{
			$element->setRegisterInArrayValidator(false);
			$options = $element->getMultiOptions();
			$element->clearMultiOptions();
			foreach($options as $key=>$value)
			{
				$element->addMultiOption($value,$value);
			}
			$element->setRegisterInArrayValidator(false);
		}
		
		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}
		 
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array('ViewHelper','FormElements'));
		}
		
		
		
		
}