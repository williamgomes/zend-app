<?php
class B2b_Service_Invoice
{
	protected $_invoice_obj;
	
	public function __construct($invoice_obj = null)
	{	
		$this->_invoice_obj = $invoice_obj;
		$this->_translator = Zend_Registry::get('translator');
	}
	
	
	public function upgradePackage()
	{		
			
	}
	
	public function deletePackage()
	{
		try
		{
			$invoice_id = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
			$user_id = $this->_invoice_obj['user_id'];
			$invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
			$invoice_item_info = $invoice_item_db->getInvoiceItems($invoice_id);
			if($invoice_item_info)
			{
				$mem_db = new Members_Model_DbTable_MemberList();
				foreach($invoice_item_info as $key => $item_info)
				{
					$item_info_arr = Zend_Json_Decoder::decode($item_info['object_value']);
					$user_id = $item_info['user_id'];
					$previous_package_id = $item_info_arr['previous_package_id'];
					$package_id = $item_info_arr['package_id'];
					$mem_db->update(array('package_id' => $previous_package_id), 'user_id = '.$user_id);
				}
			}
		}
		catch(Exception $e)
		{
			//file_put_contents('abc.txt', $e->getMessage(). ' '. $invoice_id. ' ' . $user_id);
		}
	}
	
	public function paidPackage()
	{
		try
		{
			$invoice_id = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
			$user_id = $this->_invoice_obj['user_id'];
			$invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
			$invoice_item_info = $invoice_item_db->getInvoiceItems($invoice_id);
			if($invoice_item_info)
			{
				$mem_db = new Members_Model_DbTable_MemberList();
				foreach($invoice_item_info as $key => $item_info)
				{
					$item_info_arr = Zend_Json_Decoder::decode($item_info['object_value']);
					$user_id = $item_info['user_id'];
					$previous_package_id = $item_info_arr['previous_package_id'];
					$package_id = $item_info_arr['package_id'];
					$mem_db->update(array('package_id' => $package_id), 'user_id = '.$user_id);
				}
			}
		}
		catch(Exception $e)
		{
			//file_put_contents('abc.txt', $e->getMessage(). ' '. $invoice_id. ' ' . $user_id);
		}
	}
	
	public function unpaidPackage()
	{
		try
		{
			$invoice_id = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
			$user_id = $this->_invoice_obj['user_id'];
			$invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
			$invoice_item_info = $invoice_item_db->getInvoiceItems($invoice_id);
			if($invoice_item_info)
			{
				$mem_db = new Members_Model_DbTable_MemberList();
				foreach($invoice_item_info as $key => $item_info)
				{
					$item_info_arr = Zend_Json_Decoder::decode($item_info['object_value']);
					$user_id = $item_info['user_id'];
					$previous_package_id = $item_info_arr['previous_package_id'];
					$package_id = $item_info_arr['package_id'];
					$mem_db->update(array('package_id' => $previous_package_id), 'user_id = '.$user_id);
				}
			}
		}
		catch(Exception $e)
		{
			//file_put_contents('abc.txt', $e->getMessage(). ' '. $invoice_id. ' ' . $user_id);
		}
	}
	
	public function cancelPackage()
	{
		try
		{
			$invoice_id = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
			$user_id = $this->_invoice_obj['user_id'];
			$invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
			$invoice_item_info = $invoice_item_db->getInvoiceItems($invoice_id);
			if($invoice_item_info)
			{
				$mem_db = new Members_Model_DbTable_MemberList();
				foreach($invoice_item_info as $key => $item_info)
				{
					$item_info_arr = Zend_Json_Decoder::decode($item_info['object_value']);
					$user_id = $item_info['user_id'];
					$previous_package_id = $item_info_arr['previous_package_id'];
					$package_id = $item_info_arr['package_id'];
					$mem_db->update(array('package_id' => $previous_package_id), 'user_id = '.$user_id);
				}
			}
		}
		catch(Exception $e)
		{
			//file_put_contents('abc.txt', $e->getMessage(). ' '. $invoice_id. ' ' . $user_id);
		}
	}
}
?>