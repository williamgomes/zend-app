<?php
class B2b_BackendpreferencesController extends Zend_Controller_Action
{
    
	private $preferencesForm;
	private $uploadForm;
	private $_controllerCache;
    
	
    public function init()
    {
        /* Initialize of Preferences Form  here */
		$this->preferencesForm =  new B2b_Form_PreferencesForm();
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch()
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);
		$this->view->setEscape('stripslashes');
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();
		$this->view->assign('getController', $getController);
		
		if($getAction != 'uploadfile')
		{
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}
	}
	
	
    public function preferencesAction()
    {
        if ($this->_request->isPost())
        {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
        
            if($this->preferencesForm->isValid($this->_request->getPost()))
            {
                $permission = new B2b_View_Helper_Allow ();
                        
                if($permission->allow())
                {
                    $setting = new B2b_Model_Preferences($this->preferencesForm->getValues());
                    $result = $setting->saveSetting();
                    if($result['status'] == 'ok')
                    {
                        $msg = $translator->translator("b2b_preferences_settings_save_success");
                        $json_arr = array('status' => 'ok','msg' => $msg);
                    }
                    else
                    {
                        $msg = ($result['msg']) ? $result['msg'] : $translator->translator("b2b_preferences_settings_save_err");
                        $json_arr = array('status' => 'err','msg' => $msg);
                    }
                }
        
                else
                {
        
                    $Msg =  $translator->translator("settings_add_action_deny_desc");
                    $json_arr = array('status' => 'errP','msg' => $Msg);
        
                }
        
            }
        
            else
            {
        
                $validatorMsg = $this->preferencesForm->getMessages();
                $vMsg = array();
                $i = 0;
      
                foreach($validatorMsg as $key => $errType)
                {
                    foreach($errType as $errkey => $value)
        
                    {
        
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
        
                        $i++;
        
                    }
        
                }
        
                $json_arr = array('status' => 'errV','msg' => $vMsg);
        
            }
        
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        
        else
        {
        
            $options = new B2b_Model_DbTable_Preferences();
            $global_options = $options->getOptions();
            $this->preferencesForm->populate($global_options);        
            $this->view->admin_header_logo = $global_options['admin_header_logo'];        
            $this->view->admin_favicon = $global_options['admin_favicon'];        
            $this->view->frontend_favicon = $global_options['frontend_favicon'];        
            $this->view->assign('preferencesForm', $this->preferencesForm);        
            $this->render();
        
        }
		
				
    }
		
}
