<?php

class B2b_ProductsController extends Zend_Controller_Action {

    private $_controllerCache;
    private $_auth_obj;

    public function init() {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');

        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);

            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    public function activeAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $productName = $this->_request->getPost('name');
            $paction = $this->_request->getPost('paction');
            $data = array();
            $json_arr = null;
            $active = 0;
            switch ($paction) {

                case 'publish':
                    $active = '1';
                    break;

                case 'unpublish':

                    $active = '0';
                    break;
            }

            try {
                $data = array('active' => $active);
                $productMapper = new B2b_Model_ProductMapper ();
                $json_arr = $productMapper->updateStatus($data, $id);
                $json_arr['active'] = $active;
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'active' => $active);
            }
        }

        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function activateallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);
                $data = array('active' => $active);
                foreach ($id_arr as $id) {
                    try {
                        $productMapper = new B2b_Model_ProductMapper();
                        $json_arr = $productMapper->updateStatus($data, $id);
                        $json_arr['active'] = $active;
                    } catch (Exception $e) {
                        $json_arr = array('status' => 'err',
                            'msg' => $e->getMessage(), 'active' => $active);
                    }
                }
            }
        } else {
            $msg = $translator->translator("page_selected_err");
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }

        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function addOldAction() {
        $productForm = new B2b_Form_ProductForm ();
        $translator = Zend_Registry::get('translator');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';
        $product_id = $this->_request->getParam('product_id');
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        // Checking if user have privilege to add more product
        $dataModel = new B2b_Model_DbTable_Products();

        // When Ajex based submit to add product
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow ();
            try {
                if ($productForm->isValid($this->_request->getPost())) {
                    $selected_user = $productForm->getValue('user_id');
                    $owner_id = $this->_request->getParam('user_id');
                    $helper_sql = new B2b_Controller_Helper_Utility();
                    $privilege_info = null;
                    $user_profile = null;

                    if ($owner_id) {
                        $privilege_info = $dataModel->getNumOfProductsByUser($owner_id);
                        $user_profile = $helper_sql->isUserProfile($owner_id);
                    } else {
                        $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
                        $user_profile = $helper_sql->isUserProfile($user_id);
                    }

                    // when a company profile is set as inactive
                    if (empty($user_profile)) {
                        $msg = $translator->translator('b2b_company_profile_not_found', $user_profile['full_name']);
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . $owner_id
                        );
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }

                    if ($owner_id) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $mem_info = $memberList->getMemberInfo($owner_id);
                        if ($mem_info) {
                            $package_id = $mem_info['package_id'];
                        } else {
                            $package_id = 0;
                        }
                    } else {
                        $package_id = $auth->getIdentity()->package_id;
                    }
                    $package_db = new B2b_Model_DbTable_Package();
                    $package_info = $package_db->getPackageById($package_id);
                    $current_products_num = $privilege_info['current_products_num'];
                    $max_products = $package_info['products'];
                    $active = $package_info['preapproval'];


                    //When a user going to add a listing without creating his/her Company profile.
                    if ((empty($privilege_info['company_status']) || empty($privilege_info['user_status']) ) && !empty($privilege_info['package_id'])) {
                        $msg = $translator->translator('b2b_company_account_suspended_anonymous', $privilege_info['full_name']);
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . $owner_id
                        );
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }


                    // Checking user permission to add products according to package.
                    // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                    // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                    // (int)$max_products != 0 checks if user has unlimited access to add product.

                    if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_products != 0) {

                        if ($current_products_num >= $max_products) {
                            $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_product_reached_anonymous', $privilege_info['package_name']);
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                    }

                    $productModel = new B2b_Model_Product($productForm->getValues());
                    $id = $productForm->getValue('id');
                    $dateTime = $productForm->getValue('time_of_expiry');

                    if ($dateTime) {
                        $productModel->setTime_of_expiry($dateTime);
                    }

                    if ($permission->allow()) {
                        $productModel->setSeo_title(
                                $productForm->getValue('name'), $user_id);
                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $productModel->setActive('1');
                        }

                        if ($selected_user) {
                            $productModel->setUser_id($selected_user);
                        } else {
                            $productModel->setUser_id($user_id);
                        }

                        $result = $productModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $productForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }


            $res_value = Zend_Json::encode($json_arr);


            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {

                $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
                $package_id = $privilege_info['package_id'];
                $current_products_num = $privilege_info['current_products_num'];
                $max_products = $privilege_info['max_products'];
                $companyProfile = new B2b_Model_DbTable_CompanyProfile ();
                $company_status = $companyProfile->isActive($user_id);

                // when a company profile is set as inactive
                if (empty($company_status['active']) || empty($user_status)) {
                    $alert = $privilege_info['company_name'] . ' ' . $translator->translator('b2b_company_account_suspended');
                    $this->view->assign('resource_exceeded', $alert);
                    return;
                }
                // Checking user permission to add products according to package.
                // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                // (int)$max_products != 0 checks if user has unlimited access to add product.

                if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_products != 0) {

                    if ($current_products_num >= $max_products) {

                        $alert = $translator->translator('b2b_max_num_of_product_reached', $privilege_info['package_name']);
                        $this->view->assign('resource_exceeded', $alert);
                        return;
                    }
                }

                $productGroup = new B2b_Model_DbTable_ProductGroup();
                $memberList = new Members_Model_DbTable_MemberList();
                $bizGroup = new B2b_Model_DbTable_Group();

                $allGroups = $bizGroup->getAllGroups();
                $allMembers = $memberList->getMembers()->toArray();
                $resultSet = $dataModel->getExistingProducts($user_id);
                $productGroups = $productGroup->getAllGroupsByUserId($user_id);

                if ($resultSet) {
                    $this->view->assign('previousListings', $resultSet);
                }

                if ($productGroups) {

                    $this->view->assign('productGroups', $productGroups);
                }

                if (count($allMembers) && !empty($allMembers)) {
                    $userList = array();
                    $userList [''] = $translator->translator('b2b_select');

                    foreach ($allMembers as $row) {
                        $userList [$row ['user_id']] = $row ['title'] . '  ' . $row ['firstName'] . '  ' . $row ['lastName'] . '  ';
                    }
                    $productForm->user_id->setMultiOptions($userList);
                    $productForm->user_id->setValue($user_id);
                }

                if ($allGroups) {

                    $groupList = array();
                    $groupList [''] = $translator->translator('b2b_select');

                    foreach ($allGroups as $row) {
                        $groupList [$row ['id']] = $row ['group_name'];
                    }
                    $productForm->group_id->setMultiOptions($groupList);
                }
                $this->userUploaderSettings($resultSet);
                $this->view->assign('productForm', $productForm);
                $this->view->assign('resource_exceeded_alert', $alert);
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_please_login_again'));
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function addAction() {
        $productForm = new B2b_Form_ProductForm($this->view->auth);
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more product
        $dataModel = new B2b_Model_DbTable_Products();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to add product
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($productForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $productModel = new B2b_Model_Product($productForm->getValues());
                        $owner_id = $productModel->getUser_id();

                        //Check Owner
                        if (!empty($owner_id)) {
                            $company_status = $companyProfile->isActive($owner_id);
                            $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                            $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                            $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;


                            //When a user going to add a listing without creating his/her Company profile.
                            if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                // when a company profile is set as inactive
                                if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                    $privilege_info = $dataModel->getNumOfProductsByUser($owner_id);
                                    $current_products_num = ($privilege_info && $privilege_info['current_products_num']) ? $privilege_info['current_products_num'] : 0;

                                    $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                    $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                    $max_products = ($package_info) ? $package_info['products'] : 0;
                                    $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                    // Checking user permission to add products according to package.
                                    if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                        // empty($package_id) check if user has unlimited access bypassing package limit.
                                        // empty($max_products)checks if user has unlimited access to add products.
                                        // Checking user permission to add products according to package.
                                        // ($package_info && ($current_products_num < $max_products))  checks if it exceeds package limit	
                                        if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_products) && !empty($package_id) ) || ($package_info && ($current_products_num < $max_products)))) {
                                            if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                $productModel->setActive('1');
                                            } else {
                                                $productModel->setActive('0');
                                            }
                                            $productModel->setSeo_title($productModel->getName());

                                            $result = $productModel->save();
                                            if ($result['status'] == 'ok') {
                                                $msg = $translator->translator("page_save_success");
                                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                                            } else {
                                                $msg = $translator->translator("page_save_err");
                                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                            }
                                        } else {
                                            $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_product_reached_anonymous', $package_info['name']);
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $msg = $translator->translator('b2b_membership_pkg_found_err');
                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                    }
                                } else {
                                    $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $this->view->escape($mem_info['full_name'] . ' ( ' . $this->view->escape($company_status['company_name']) . ' ) ')));
                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                }
                            } else {
                                $msg = $translator->translator('b2b_company_profile_not_found');
                                $json_arr = array('status' => 'err', 'msg' => $msg);
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                        }
                    } else {
                        $validatorMsg = $productForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                $company_status = $companyProfile->isActive($user_id);

                //When a user going to this page without creating his/her Company profile.
                if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                    // when a company profile is set as inactive
                    if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                        $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
                        $current_products_num = ($privilege_info && $privilege_info['current_products_num']) ? $privilege_info['current_products_num'] : 0;

                        $package_id = $auth->getIdentity()->package_id;
                        $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);
                        $max_products = ($package_info) ? $package_info['products'] : 0;

                        // empty($package_id) check if user has unlimited access bypassing package limit.
                        // empty($max_products)checks if user has unlimited access to add product.
                        // Checking user permission to add products according to package.
                        // ($package_info && ($current_products_num < $max_products))  checks if it exceeds package limit	
                        if (empty($package_id) || empty($max_products) || ($package_info && ($current_products_num < $max_products))) {
                            // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                            // (int)$max_products != 0 checks if user has unlimited access to add product.						


                            /* $productGroup = new B2b_Model_DbTable_ProductGroup();
                              $productGroups = $productGroup->getAllGroupsByUserId($user_id);
                              if ($productGroups)
                              {
                              $this->view->assign('productGroups' , $productGroups);
                              } */

                            // check if user has unlimited access to change any group from list
                            if ($auth->getIdentity()->access_other_user_article != '1') {
                                $multiOptions = $productForm->group_id->getMultiOptions();
                                $productForm->group_id->clearMultiOptions();
                                foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                    if ($company_status['group_id'] == $multiOptionKey) {
                                        $productForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                    }
                                }
                            }

                            $productForm->group_id->setValue($company_status['group_id']);
                            $this->view->assign('productForm', $productForm);
                            $this->view->assign('resource_exceeded_alert', '');
                        } else {
                            $alert = $translator->translator('b2b_max_num_of_product_reached', $privilege_info['package_name']);
                            $this->view->assign('resource_exceeded', $alert);
                        }
                    } else {
                        $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                        $this->view->assign('resource_exceeded', $alert);
                    }
                } else {
                    $alert = $translator->translator('b2b_company_profile_not_found');
                    $this->view->assign('resource_exceeded', $alert);
                }
            }
        }
    }

    public function editOLDAction() {
        $id = $this->_request->getParam('id');
        $productForm = new B2b_Form_ProductForm ();
        $translator = Zend_Registry::get('translator');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $product_id = $this->_request->getParam('product_id');
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        // Checking if user have privilege to add more product
        $dataModel = new B2b_Model_DbTable_Products();
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow ();
            try {
                if ($productForm->isValid($this->_request->getPost())) {
                    $productModel = new B2b_Model_Product($productForm->getValues());
                    $id = $productForm->getValue('id');
                    $dateTime = $productForm->getValue('time_of_expiry');
                    if ($dateTime) {
                        $productModel->setTime_of_expiry($dateTime);
                    }

                    if ($permission->allow()) {
                        $helper_sql = new B2b_Controller_Helper_Utility();
                        $selected_user = $productForm->getValue('user_id');
                        $user_profile = null;
                        $privilege_info = null;

                        if ($selected_user) {
                            $privilege_info = $dataModel->getNumOfProductsByUser($selected_user);
                            $user_profile = $helper_sql->isUserProfile($selected_user);
                        } else {
                            $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
                            $user_profile = $helper_sql->isUserProfile($user_id);
                        }

                        if ($selected_user) {
                            $memberList = new Members_Model_DbTable_MemberList();
                            $mem_info = $memberList->getMemberInfo($selected_user);
                            if ($mem_info) {
                                $package_id = $mem_info['package_id'];
                            } else {
                                $package_id = 0;
                            }
                        } else {
                            $package_id = $auth->getIdentity()->package_id;
                        }
                        $package_db = new B2b_Model_DbTable_Package();
                        $package_info = $package_db->getPackageById($package_id);
                        $current_products_num = $privilege_info['current_products_num'];
                        $max_products = $package_info['products'];
                        $active = $package_info['preapproval'];

                        //When a user going to add a listing without creating his/her Company profile.
                        if (empty($user_profile)) {
                            $msg = $translator->translator('b2b_company_profile_not_found', $user_profile['full_name']);
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg
                            );
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }

                        // when a company profile is set as inactive
                        if ((empty($privilege_info['company_status']) || empty($privilege_info['user_status']) ) && !empty($privilege_info['package_id'])) {
                            $msg = $translator->translator('b2b_company_account_suspended_anonymous', $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name'] . ' ) ');
                            $json_arr = array('status' => 'err', 'msg' => $msg . $selected_user);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                        // Checking user permission to add products according to package.
                        // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                        // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                        // (int)$max_products != 0 checks if user has unlimited access to add product.

                        if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_products != 0) {

                            if ($current_products_num > $max_products) {
                                $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_product_reached_anonymous', $privilege_info['package_name']);
                                $json_arr = array('status' => 'err', 'msg' => $msg);
                                $res_value = Zend_Json::encode($json_arr);
                                $this->_response->setBody($res_value);
                                return;
                            }
                        }

                        $productModel->setSeo_title(
                                $productForm->getValue('name'), $user_id);

                        // The Condition empty($active) is for Admin or who has '0' flag on package_id on user_profile table.
                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $productModel->setActive('1');
                        }

                        if ($selected_user) {
                            $productModel->setUser_id($selected_user);
                        } else {
                            $productModel->setUser_id($user_id);
                        }
                        $result = $productModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_update_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_update_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $productForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }


            $res_value = Zend_Json::encode($json_arr);


            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {

                if (!empty($id) || !is_numeric($id)) {
                    $dataModel = new B2b_Model_DbTable_Products();
                    $productGroup = new B2b_Model_DbTable_ProductGroup();
                    $memberList = new Members_Model_DbTable_MemberList();
                    $bizGroup = new B2b_Model_DbTable_Group();

                    $allGroups = $bizGroup->getAllGroups();
                    $allMembers = $memberList->getMembers()->toArray();
                    $productDetails = $dataModel->getProductById($id);
                    $previousListings = $dataModel->getExistingProducts($user_id);
                    $productGroups = $productGroup->getAllGroupsByUserId($user_id);

                    $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $productDetails['user_id']) ? true : false;

                    if (is_array($productDetails) && (count($productDetails) > 1 ) && $isEditable) {

                        $productForm->populate($productDetails);
                        $payment_terms = explode(',', $productDetails['payment_terms']);
                        $related_items = explode(',', $productDetails['related_items']);
                        $type = explode(',', $productDetails['type']);
                        $productForm->payment_terms->setValue($payment_terms);
                        $productForm->related_items->setValue($related_items);
                        $productForm->type->setValue($type);
                        $validity = date("l, F d, Y h:i:s A", strtotime($productDetails['time_of_expiry']));
                        $productForm->time_of_expiry->setValue($validity);
                        if ($productGroups) {
                            $this->view->assign('productGroups', $productGroups);
                        }

                        if (count($allMembers) && !empty($allMembers)) {
                            $userList = array();
                            $userList [''] = $translator->translator('b2b_select');

                            foreach ($allMembers as $row) {
                                $userList [$row ['user_id']] = $row ['title'] . '  ' . $row ['firstName'] . '  ' . $row ['lastName'] . '  ';
                            }
                            $productForm->user_id->setMultiOptions($userList);
                        }

                        if ($previousListings) {
                            $this->view->assign('previousListings', $previousListings);
                        }

                        if ($allGroups) {

                            $groupList = array();
                            $groupList [''] = $translator->translator('b2b_select');

                            foreach ($allGroups as $row) {
                                $groupList [$row ['id']] = $row ['group_name'];
                            }
                            $productForm->group_id->setMultiOptions($groupList);
                        }

                        if (!empty($productDetails['category_id'])) {
                            $category_db = new B2b_Model_DbTable_Category();
                            $category_info = $category_db->getCategoryById(
                                    $productDetails['category_id']);
                            $this->view->assign('category_info', $category_info);
                        }

                        $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
                        $company_profile_info = $company_profile_db->getUserProfile(
                                $user_id);
                        $company_profile_info['group_id'] = ($company_profile_info &&
                                $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
                        $productForm->group_id->setValue($company_profile_info['group_id']);
                        $this->userUploaderSettings($company_profile_info);
                    } else {
                        $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                    }
                } else {

                    $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                }


                $this->view->assign('productForm', $productForm);
            } else {
                $json_arr = array('status' => 'err', 'msg' => 'Login Required');
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function editAction() {
        $id = $this->_request->getParam('id');
        $productForm = new B2b_Form_ProductForm($this->view->auth);
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more product
        $dataModel = new B2b_Model_DbTable_Products();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to edit product
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($productForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $productModel = new B2b_Model_Product($productForm->getValues());
                        $owner_id = $productModel->getUser_id();
                        $id = $productModel->getId();

                        if (!empty($id) && is_numeric($id)) {
                            $productDetails = $dataModel->getProductById($id);
                            if ($productDetails) {
                                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $productDetails['user_id']) ? true : false;
                                if ($isEditable === true) {
                                    //Check Owner
                                    if (!empty($owner_id)) {
                                        $company_status = $companyProfile->isActive($owner_id);
                                        $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                                        $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                                        $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;


                                        // when a company profile is set as inactive
                                        if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                            //When a user going to edit a listing without creating his/her Company profile.
                                            if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                                $privilege_info = $dataModel->getNumOfProductsByUser($owner_id);
                                                $current_products_num = ($privilege_info && $privilege_info['current_products_num']) ? $privilege_info['current_products_num'] : 0;

                                                $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                                $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                                $max_products = ($package_info) ? $package_info['products'] : 0;
                                                $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                                // Checking user permission to edit products according to package.
                                                if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                                    // empty($package_id) check if user has unlimited access bypassing package limit.
                                                    // empty($max_lead)checks if user has unlimited access to edit products.
                                                    // Checking user permission to edit products according to package.
                                                    // ($package_info && ($current_products_num <= $max_products))  checks if it exceeds package limit	
                                                    if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_products) && !empty($package_id) ) || ($package_info && ($current_products_num <= $max_products)))) {

                                                        if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                            $productModel->setActive('1');
                                                        } else {
                                                            $productModel->setActive('0');
                                                        }
                                                        $productModel->setSeo_title($productModel->getName(), $id);

                                                        $result = $productModel->save();
                                                        if ($result['status'] == 'ok') {
                                                            $msg = $translator->translator("page_save_success");
                                                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                                                        } else {
                                                            $msg = $translator->translator("page_save_err");
                                                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                                        }
                                                    } else {
                                                        $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_product_reached_anonymous', $package_info['name']);
                                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                                    }
                                                } else {
                                                    $msg = $translator->translator('b2b_membership_pkg_found_err');
                                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                                }
                                            } else {
                                                $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $mem_info['full_name'] . ' ( ' . $this->view->escape($company_status['company_name']) . ' ) '));
                                                $json_arr = array('status' => 'err', 'msg' => $msg);
                                            }
                                        } else {
                                            $msg = $translator->translator('b2b_company_profile_not_found');
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                                    }
                                } else {
                                    $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_permission_err'));
                                }
                            } else {
                                $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                        }
                    } else {
                        $validatorMsg = $productForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                if (!empty($id) && is_numeric($id)) {
                    $productDetails = $dataModel->getProductById($id);
                    if ($productDetails) {
                        $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $productDetails['user_id']) ? true : false;
                        if ($isEditable === true) {
                            $company_status = $companyProfile->isActive($user_id);

                            //When a user going to this page without creating his/her Company profile.
                            if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                                // when a company profile is set as inactive
                                if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                                    //Get Category Info
                                    $categoryData = new B2b_Model_DbTable_Category();

                                    if (empty($productDetails['category_id'])) {
                                        $category_name = $translator->translator("common_tree_root");
                                    } else {
                                        $categoryInfo = $categoryData->getCategoryName($productDetails['category_id']);
                                        $category_name = $categoryInfo['name'];
                                    }

                                    $locale = Eicra_Global_Variable::getSession()->sess_lang;
                                    //ASSIGN TIME OF EXPIRY
//                                    $productDetails['time_of_expiry'] = date("l, F d, Y H:i:s A", strtotime($productDetails['time_of_expiry']));
                                    $date_obj = new Zend_Date($productDetails['time_of_expiry'], null, $locale);
                                    //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                                    $productDetails['time_of_expiry'] = $date_obj->get(Zend_Date::DATE_FULL . " " . Zend_Date::TIMES); //date("Y-m-d H:i:s", $time);

                                    //ASSIGN RELETED ITEMS
                                    $productForm->related_items->setIsArray(true);
                                    $productDetails['related_items'] = explode(',', $productDetails['related_items']);

                                    //ASSIGN TYPE OR STATUS
                                    $productForm->type->setIsArray(true);
                                    $productDetails['type'] = explode(',', $productDetails['type']);

                                    //ASSIGN PAYMENT TYPE 
                                    $productForm->payment_terms->setIsArray(true);
                                    $productDetails['payment_terms'] = explode(',', $productDetails['payment_terms']);

                                    //ASSIGN GROUP
                                    if ($auth->getIdentity()->access_other_user_article != '1') {
                                        $multiOptions = $productForm->group_id->getMultiOptions();
                                        $productForm->group_id->clearMultiOptions();
                                        foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                            if ($company_status['group_id'] == $multiOptionKey) {
                                                $productForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                            }
                                        }
                                    }

                                    $productForm->populate($productDetails);
                                    $this->userUploaderSettings($productDetails);

                                    $this->view->assign('category_info', $categoryInfo);
                                    $this->view->assign('category_name', $category_name);
                                    $this->view->assign('productForm', $productForm);
                                } else {
                                    $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                                    $this->view->assign('resource_exceeded', $alert);
                                }
                            } else {
                                $alert = $translator->translator('b2b_company_profile_not_found');
                                $this->view->assign('resource_exceeded', $alert);
                            }
                        } else {
                            $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                        }
                    } else {
                        $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                    }
                } else {
                    $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                }
            }
        }
    }

    public function displayableAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;

        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()) {

                $id = $this->_request->getPost('id');
                $product_name = $this->_request->getPost('product_name');
                $displayable = $this->_request->getPost('paction');

                $producMapper = new B2b_Model_ProductMapper();

                try {
                    $result = $producMapper->displayable($id, $displayable);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'displayable' => $displayable);
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'displayable' => $displayable);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'displayable' => $displayable);
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg, 'displayable' => $displayable);
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function featuredAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;

        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()) {

                $id = $this->_request->getPost('id');
                $product_name = $this->_request->getPost('product_name');
                $featured = $this->_request->getPost('paction');

                $producMapper = new B2b_Model_ProductMapper();

                try {
                    $result = $producMapper->featured($id, $featured);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'featured' => $featured);
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'featured' => $featured);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'featured' => $featured);
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg, 'featured' => $featured);
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $producMapper = new B2b_Model_ProductMapper();

                //page_info
                $proObj = new B2b_Model_DbTable_Products();
                $pro_info = $proObj->getProductById($id);

                $product_images = explode(',', $pro_info['product_images']);
                $product_images_path = 'data/frontImages/b2b/product_images';

                $brochures_file = explode(',', $pro_info['brochures']);
                $brochures_path = 'data/frontImages/b2b/brochures';

                try {
                    $result = $producMapper->delete($id);
                    if ($result['status'] == 'ok') {
                        foreach ($product_images as $key => $file) {
                            if ($file) {
                                $dir = $product_images_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                            }
                            if ($res) {
                                $msg = $translator->translator('page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator('page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            }
                        }

                        foreach ($brochures_file as $key => $file) {
                            if ($file) {
                                $dir = $brochures_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir(
                                                $dir);
                            }
                            if ($res) {
                                $msg = $translator->translator(
                                        'page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            } else {
                                $msg = $translator->translator(
                                        'page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            }
                        }

                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg);
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        } else {
            $msg = $translator->translator('b2b_common_delete_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');


        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow('delete', 'products', 'B2b')) {
                $proObj = new B2b_Model_DbTable_Products();
                $product_images_path = 'data/frontImages/b2b/product_images';
                $brochures_path = 'data/frontImages/b2b/brochures';

                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);

                    foreach ($id_arr as $id) {
                        try {
                            $pro_info = $proObj->getProductById($id);

                            $product_images = explode(',', $pro_info['product_images']);
                            $brochures_file = explode(',', $pro_info['brochures']);

                            $producMapper = new B2b_Model_ProductMapper();
                            $result = $producMapper->delete($id);
                            if ($result['status'] == 'ok') {
                                foreach ($product_images as $key => $file) {
                                    if ($file) {
                                        $dir = $product_images_path . DS . $file;
                                        $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                                    }
                                    if ($res) {
                                        $msg = $translator->translator('page_list_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    } else {
                                        $msg = $translator->translator('page_list_file_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    }
                                }



                                foreach ($brochures_file as $key => $file) {
                                    if ($file) {
                                        $dir = $brochures_path . DS . $file;
                                        $res = Eicra_File_Utility::deleteRescursiveDir(
                                                        $dir);
                                    }
                                    if ($res) {
                                        $msg = $translator->translator(
                                                'page_list_delete_success', $file);
                                        $json_arr = array('status' => 'ok',
                                            'msg' => $msg);
                                    } else {
                                        $msg = $translator->translator(
                                                'page_list_file_delete_success', $file);
                                        $json_arr = array('status' => 'ok',
                                            'msg' => $msg);
                                    }
                                }

                                $msg = $translator->translator("b2b_common_delete_success");
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator("b2b_common_delete_failed");
                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err', 'msg' => $e->getMessage() . ' ' . $id);
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function memlistAction() {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->_request->getParam('approve');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => 'list', 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($member_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $member = new B2b_Model_MemberListMapper();
                    $member_datas = $member->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($member_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($member_datas) {
                    //$product_db = new B2b_Model_DbTable_Products();
                    $key = 0;
                    foreach ($member_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_username'] = str_replace('_', '-', $entry_arr['username']);
                        $entry_arr['last_access_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['last_access']));
                        $entry_arr['register_date_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['register_date']));
                        //$entry_arr['product_count']=  $product_db->getProductNoByUserId($entry_arr['user_id']);
                        $entry_arr['edit_enable'] = (($this->_auth_obj->access_other_user_profile == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) && ($entry_arr['role_lock'] == '1' || $this->_auth_obj->role_id == $entry_arr['role_id']) ) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key++;
                    }
                    $total = $member_datas->getTotalItemCount();
                }
                $json_arr = array('status' => 'ok', 'data_result' => $data_result, 'total' => $total, 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        //Get Role List
        $roles = new Members_Model_DbTable_Role();
        $this->view->roleList = $roles->fetchAll();
    }

    public function listAction() {

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {

            try {

                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $featured = $this->_request->getParam('featured');

                if ($displayble) {

                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble',
                        'operator' => 'eq',
                        'value' => $displayble
                    );
                }

                if ($featured) {

                    $posted_data['filter']['filters'][] = array(
                        'field' => 'featured',
                        'operator' => 'eq',
                        'value' => $featured
                    );
                }

                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRout = 'adminrout';

                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                        ), $frontendRout, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));

                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $product_mapper = new B2b_Model_ProductMapper();
                    $productSql = new B2b_Controller_Helper_ProductSql();
                    $tableColumns = $productSql->getBackendList();
                    $view_datas = $product_mapper->fetchAll($pageNumber, $approve, $posted_data, $tableColumns);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }

                $data_result = array();
                $total = 0;
                if ($view_datas) {

                    $key = 0;

                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace('_', '-', $entry_arr['name']);
                        $entry_arr['category_name'] = (empty($entry_arr['category_id'])) ? $this->view->translator->translator('common_no_catagory') : $this->view->escape($entry_arr['category_name']);
                        $img_thumb_arr = explode(',', $entry_arr['product_images']);
                        $entry_arr['product_images_primary_format'] = ($this->view->escape($entry_arr['product_images_primary'])) ? 'data/frontImages/b2b/product_images/' . $this->view->escape($entry_arr['product_images_primary']) : (($this->view->escape($entry_arr['product_images'])) ? 'data/frontImages/b2b/product_images/' . $img_thumb_arr[0] : '');
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }

                    $total = $view_datas->getTotalItemCount();
                }

                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {

                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }



            // Convert To JSON ARRAY

            $res_value = Zend_Json::encode($json_arr);

            $this->_response->setBody($res_value);
        }

        $mem_db = new Members_Model_DbTable_MemberList();
        $this->view->assign('mem_data', $mem_db->getAllMembers());
    }

    public function upAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $id = $this->_request->getPost('id');
        $product_order = $this->_request->getPost('product_order');
        $table_name = 'b2b_products';
        $fields_arr = array('id', 'group_id', 'user_id', 'product_order');

        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->decreaseOrder();

        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }

        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $id = $this->_request->getPost('id');
        $product_order = $this->_request->getPost('product_order');
        $table_name = 'b2b_products';
        $fields_arr = array('id', 'group_id', 'user_id', 'product_order');

        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->increaseOrder();
        if ($returnV == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }

        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $table_name = 'b2b_products';
            $fields_arr = array('id', 'group_id', 'user_id', 'product_order');
            $id_str = $this->_request->getPost('id_arr');
            $product_order_str = $this->_request->getPost('product_order_arr');

            $id_arr = explode(',', $id_str);
            $product_order_arr = explode(',', $product_order_str);
            $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
            $checkOrder = $order_numbers->checkOrderNumbers($product_order_arr);
            if ($checkOrder['status'] == 'err') {
                $json_arr = array('status' => 'err', 'msg' => $checkOrder['msg']);
            } else {
                //Save Category Order
                $msg = $translator->translator("b2b_order_save_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg);
                $i = 0;
                foreach ($id_arr as $id) {
                    $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
                    $result = $OrderObj->saveOrder($product_order_arr[$i]);
                    if ($result['status'] == 'err') {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                        break;
                    }
                    $i++;
                }
            }
            //Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function userUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;

        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_product_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));

        /**
         * ************** For Brochures **************************
         */
        $brochures_param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => 'file_path_brochures',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model_brochures = new Portfolio_Model_Portfolio($brochures_param_fields);
        $brochures_requested_data = $portfolio_model_brochures->getRequestedData();
        $this->view->assign('brochures_settings_info', array_merge($brochures_requested_data, $settings_info));
        $this->view->assign('brochures_settings_json_info', Zend_Json::encode($this->view->brochures_settings_info));
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

}
