<?php

class B2b_BackendcategoryController extends Zend_Controller_Action
{

    private $_controllerCache;

    private $translator;

    private $_auth_obj;

    public function init ()
    {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch ()
    {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = Zend_Registry::get('config')->eicra->params->domain . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    public function groupAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->_request->isPost()) {
            $group_id = $this->_request->getPost('grp_id');
            $parant = $this->_request->getPost('id');
            $selected_id = ($this->_request->getPost('selected_id')) ? $this->_request->getPost('selected_id') : null;
            $expanded = ($this->_request->getPost('expanded')) ? $this->_request->getPost('expanded') : false;
           
		    $group_db = new B2b_Model_DbTable_Group();
			$group_info = $group_db->getGroupName($group_id);
		   
		    $category_tree = B2b_View_Helper_Categorytree::getTreeDataSource($parant, $this->view, $group_id, $selected_id, $expanded);
            $translator = Zend_Registry::get('translator');
			
			//Parameter For Uploader Settings
			$param_fields = array(
									'table_name' => 'b2b_group',
									'primary_id_field' => 'id',
									'primary_id_field_value' => $group_id,
									'file_path_field' => '',
									'file_extension_field' => 'file_type',
									'file_max_size_field' => 'file_size_max'
							);
			
            if ($category_tree) {
                $json_arr = array(
                    'status' => 'ok',
                    'TreeDataSource' => $category_tree,
                    'params' => $this->_request->getParams(), 
					'param_fields'	=> $param_fields, 
					'group_info' => $group_info
                );
            } else {
                $msg = $translator->translator('category_group_err') . ' ' . $group_id . ' ' . $parant . ' ' . $selected_id;
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg, 
					'param_fields'	=> $param_fields, 
					'group_info' => $group_info
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function addAction ()
    {
        $categoryForm = new B2b_Form_CategoryForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($categoryForm->isValid($this->_request->getPost())) {
                    $categoryModel = new B2b_Model_Category($categoryForm->getValues());
                    if ($permission->allow() && ! empty($user_id)) {
                        $categoryModel->setUser_id($user_id);
                        $result = $categoryModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $categoryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
		$this->view->assign('group_id', $this->_request->getParam('group_id'));
        $this->view->assign('categoryForm', $categoryForm);
    }

    public function editAction ()
    {
        $id = $this->_request->getParam('id');

        $categoryForm = new B2b_Form_CategoryForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($categoryForm->isValid($this->_request->getPost())) {
                    $parant_id = $categoryForm->getValue('parent_id');
                    $msg = $translator->translator("b2b_category_validation_msg");
                    if ($id == $parant_id) {
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg
                        );
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }

                    $categoryModel = new B2b_Model_Category($categoryForm->getValues());
                    if ($permission->allow() && ! empty($user_id)) {
                        $categoryModel->setUser_id($user_id);
                        $categoryModel->setId($id);
						$categoryModel->setUrl_portion($categoryForm->getValue('url_portion'), $id);
                        $result = $categoryModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $categoryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if ($id) {
                $categorytTable = new B2b_Model_DbTable_Category();
                $categoryRow = $categorytTable->getCategoryById($id);
                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $categoryRow['user_id']) ? true : false;

                if (is_array($categoryRow) && (count($categoryRow) > 1) && $isEditable) {

                    $categoryForm->populate($categoryRow);
                    $parantName = $categorytTable->getCategoryName($categoryRow['parent_id']);
                    $this->view->assign('category_info', $categoryRow);
                    $this->view->assign('parant_name', ($parantName['name']) ? $parantName['name'] : $this->translator->translator("common_tree_root"));
                } else {
                    $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                }
            } else {
                $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
            }
        }
		$this->view->assign('group_id', $this->_request->getParam('group_id'));
        $this->view->assign('categoryForm', $categoryForm);
    }

    public function deleteAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $categoryMapper = new B2b_Model_CategoryMapper();
                try {
                    $result = $categoryMapper->delete($id);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg']
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage()
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        } else {
            $msg = $translator->translator('b2b_common_delete_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow('delete', 'products', 'B2b')) {
                if (! empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $categoryMapper = new B2b_Model_CategoryMapper();
                            $result = $categoryMapper->delete($id);
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator("b2b_common_delete_success");
                                $json_arr = array(
                                    'status' => 'ok',
                                    'msg' => $msg
                                );
                            } else {
                                $msg = $translator->translator("b2b_common_delete_failed");
                                $json_arr = array(
                                    'status' => 'err',
                                    'msg' => $msg . " " . $result['msg']
                                );
                            }
                        } catch (Exception $e) {
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $e->getMessage() . ' ' . $id
                            );
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $msg
                    );
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function listAction ()
    {
        $group_id = $this->_request->getParam('group_id');
        $this->view->group_id = $group_id;

        $categoryGroupTbl = new B2b_Model_DbTable_Group();

        $group_info = ($group_id) ? $categoryGroupTbl->getGroupName($group_id) : null;
        $this->view->group_info = $group_info;

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                $approve = $this->getRequest()->getParam('approve');

                if ($group_id)

                {

                    $posted_data['filter']['filters'][] = array(
                        'field' => 'group_id',
                        'operator' => 'eq',
                        'value' => $this->_request->getParam('group_id')
                    );
                }

                $posted_data['hasChild'] = false;
                $getViewPageNum = $this->_request->getParam('pageSize');

                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'group_id' => $group_id,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                ), 'adminrout', true);

                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (! empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;

                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                // Call Class

                $categories = new B2b_Model_CategoryMapper();
                $encode_params = Zend_Json::encode($posted_data);

                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);

                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));

                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false)

                {

                    $view_datas = $categories->fetchAll($pageNumber, $approve, $posted_data);

                    $this->_controllerCache->save($view_datas, $uniq_id);
                }

                $data_result = array();

                $total = 0;

                if ($view_datas)

                {

                    $key = 0;

                    foreach ($view_datas as $entry)

                    {

                        $entry_arr = $entry->toArray();

                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);

                        $entry_arr['publish_status_category_name'] = str_replace('_', '-', $entry_arr['name']);

                        $entry_arr['view_title_format'] = $this->translator->translator('gallery_click_view_product', $this->view->escape($entry_arr['group_name']));

                        $entry_arr['cat_date_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['added_on']));

                        $entry_arr['has_child'] = $categories->getDbTable()->checkSubCategory($entry['id']);

                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;

                        $data_result[$key] = $entry_arr;

                        $key ++;
                    }

                    $total = $view_datas->getTotalItemCount();
                }

                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            }

            catch (Exception $e)

            {

                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function upAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $id = $this->_request->getPost('id');
        $product_order = $this->_request->getPost('category_order');
        $table_name = 'b2b_category';
        $fields_arr = array(
            'id',
            'user_id',
            'group_id',
            'category_order'
        );
        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->decreaseOrder();
        if ($returnV['status'] == 'err') {
            $json_arr = array(
                'status' => 'err',
                'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']
            );
        } else {
            $json_arr = array(
                'status' => 'ok',
                'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $id = $this->_request->getPost('id');
        $category_order = $this->_request->getPost('category_order');
        $table_name = 'b2b_category';
        $fields_arr = array(
            'id',
            'user_id',
            'group_id',
            'category_order'
        );
        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->increaseOrder();
        if ($returnV == 'err') {
            $json_arr = array(
                'status' => 'err',
                'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']
            );
        } else {
            $json_arr = array(
                'status' => 'ok',
                'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $table_name = 'b2b_category';
            $fields_arr = array(
                'id',
                'user_id',
                'group_id',
                'category_order'
            );
            $id_str = $this->_request->getPost('id_arr');
            $buying_order_str = $this->_request->getPost('order_arr');
            $id_arr = explode(',', $id_str);
            $order_arr = explode(',', $buying_order_str);
            $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
            $checkOrder = $order_numbers->checkOrderNumbers($order_arr);
            if ($checkOrder['status'] == 'err') {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $checkOrder['msg']
                );
            } else {
                // Save Category Order
                $msg = $translator->translator("buying_order_save_success");
                $json_arr = array(
                    'status' => 'ok',
                    'msg' => $msg
                );
                $i = 0;
                foreach ($id_arr as $id) {
                    $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
                    $result = $OrderObj->saveOrder($order_arr[$i]);
                    if ($result['status'] == 'err') {
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $result['msg']
                        );
                        break;
                    }
                    $i ++;
                }
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function publishAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $productName = $this->_request->getPost('name');
            $paction = $this->_request->getPost('paction');
            $data = array();
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            try {
                $data = array(
                    'active' => $active
                );
                $categoryMapper = new B2b_Model_CategoryMapper();
                $json_arr = $categoryMapper->updateStatus($data, $id);
                $json_arr['active'] = $active;
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage(),
                    'active' => $active
                );
            }
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishallAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            if (! empty($id_str)) {
                $id_arr = explode(',', $id_str);
                $data = array(
                    'active' => $active
                );
                foreach ($id_arr as $id) {
                    try {
                        $categoryMapper = new B2b_Model_CategoryMapper();
                        $json_arr = $categoryMapper->updateStatus($data, $id);
                        $json_arr['active'] = $active;
                    } catch (Exception $e) {
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $e->getMessage(),
                            'active' => $active
                        );
                    }
                }
            }
        } else {
            $msg = $translator->translator("page_selected_err");
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function showprofileAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                // $product_name = $this->_request->getPost('product_name');
                $isShowProfile = $this->_request->getPost('paction');
                $categoryMapper = new B2b_Model_CategoryMapper();
                try {
                    $result = $categoryMapper->showProfile($id, $isShowProfile);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'profile' => $isShowProfile
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'profile' => $isShowProfile
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'profile' => $isShowProfile
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'profile' => $isShowProfile
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function displayableAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                // $product_name = $this->_request->getPost('product_name');
                $displayable = $this->_request->getPost('paction');
                $categoryMapper = new B2b_Model_CategoryMapper();
                try {
                    $result = $categoryMapper->displayable($id, $displayable);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'displayable' => $displayable
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'displayable' => $displayable
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'displayable' => $displayable
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'displayable' => $displayable
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function featuredAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {

                $id = $this->_request->getPost('id');
                $product_name = $this->_request->getPost('product_name');
                $featured = $this->_request->getPost('paction');
                $categoryMapper = new B2b_Model_CategoryMapper();

                try {
                    $result = $categoryMapper->featured($id, $featured);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'featured' => $featured
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'featured' => $featured
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'featured' => $featured
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'featured' => $featured
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }
}