<?php
class B2b_BookController extends Zend_Controller_Action
{
	private $_translator;
	private $_controllerCache;
	private $_modules_license;
	private $_ckLicense = true;
	private	$_snopphing_cart;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');	
				
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();		
		
		/*Check Module License*/		
		//$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
		if($this->_ckLicense == true)
		{			
								
			$license = new Zend_Session_Namespace('License');
			if(!$license || !$license->license_data || !$license->license_data['modules'])
			{
				$curlObj = new Eicra_License_Version();																
				$curlObj->sendInfo(array('dm' => $this->view->serverUrl()));								
				$license->license_data = $curlObj->getArrayResult();				
			}			
		}
		$this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$this->_snopphing_cart	=	($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;
		$this->view->assign('snopphing_cart', $this->_snopphing_cart);				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout());	
				
	}
	
	public function confirmAction()
	{	
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
                array(
                        $this->_translator->translator('invoice_details_invoice_page_name', '', 'Invoice'),
                        $this->view->url()));
						
		$cart_result = Eicra_Global_Variable::getSession()->cart_result;
		if(!empty($cart_result))
		{
			$invoice_arr = $this->generateInvoice($cart_result);
			if($invoice_arr['status'] == 'ok')
			{
				Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
			}
		}
		$this->view->assign('invoice_arr', $invoice_arr);		
		$this->assignLogin();
		$this->view->logindetails = new Members_Form_LoginForm();	
		$this->dynamicUploaderSettings($this->view->form_info);		
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' 				=> 	'forms', 
								'primary_id_field'			=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'			=>	'attach_file_path', 
								'file_extension_field'		=>	'attach_file_type', 
								'file_max_size_field'		=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	private function assignLogin()
	{
		$template_id_field	=	'role_id';
		$settings_db = new Invoice_Model_DbTable_Setting();
		$template_info = $settings_db->getInfoByModule('B2b');
		if($template_info)
		{			
			$selected_role_id = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator('b2b_invoice_register_role_id');									
		}
		else
		{					
			$selected_role_id =  $this->_translator->translator('b2b_invoice_register_role_id');
		}
		
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
		{
			$registrationForm =  new Members_Form_UserForm ($role_info);
			if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
			$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';
			
			$this->view->selected_role_id	=	$selected_role_id;					
			$this->view->registrationForm = $registrationForm;
		}
		else
		{
			throw new Exception("Register page not found");
		}		
	}
	
	
	
	private function generateInvoice($cart_result)
	{
		
		
		$country_db 		= 	new Eicra_Model_DbTable_Country();
		$payment_db 		= 	new Paymentgateway_Model_DbTable_Gateway();
		$payment_info 		= 	$payment_db->getDefaultGateway();
		$auth 				= 	Zend_Auth::getInstance ();
		
		$global_conf		= 	Zend_Registry::get('global_conf');
		$marchent			=	new Invoice_Controller_Helper_Marchent($global_conf);
		$currency 			= 	new Zend_Currency($global_conf['default_locale']);
		$currencySymbol 	= 	$currency->getSymbol(); 
		$currencyShortName 	= 	$currency->getShortName();
				
		if ($auth->hasIdentity ())
		{
			
			$globalIdentity = $auth->getIdentity();
			$user_id = $globalIdentity->user_id;			
			foreach($globalIdentity as $globalIdentity_key => $globalIdentity_value)
			{
				$mem_info[$globalIdentity_key] = $globalIdentity_value;
				$invoice_arr[$globalIdentity_key] = $globalIdentity_value;
			}
			if($mem_info['country'])
			{
				$country_info = $country_db->getInfo($mem_info['country']);
				$mem_info['country_name'] = stripslashes($country_info['value']);
			}			
			
			//invoice_arr assigning started
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $global_conf['payment_user_id'];
			$invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']).' '.stripslashes($mem_info['firstName']).' '.stripslashes($mem_info['lastName']).'<br />'.stripslashes($mem_info['mobile']).'<br />'.stripslashes($mem_info['city']).', '.stripslashes($mem_info['state']).', '.stripslashes($mem_info['postalCode']).'<br />'.stripslashes($country_info['value']).'<BR />'.stripslashes($mem_info['username']);
			$invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
			$invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
			$invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]	= date("Y-m-d h:i:s");
			$invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y",strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
			$invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
			$invoice_arr['site_name']	=	stripslashes($global_conf['site_name']);
			$invoice_arr['site_url']	=	stripslashes($global_conf['site_url']);
			$this->view->mem_info = $mem_info;
		}
				
		
		//invoice_items_arr assigning started
		$total_amount = 0;
		$total_tax = 0;
		$invoice_items_arr = array();
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
																<tbody>
																<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
																	<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>'.$this->_translator->translator("b2b_invoice_desc_title").'</strong></td>
																	<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>'.$this->_translator->translator("b2b_invoice_amount_title").'</strong></td>		
																</tr>';
		$item_arr = 0;
		$marchent_email_id_arr = array();
                
		foreach($cart_result as $cart_result_key => $dataInfo)
		{	
                    
			if($dataInfo['item'])
			{					
				$dataInfo[Eicra_File_Constants::MAIN_TABLE_ID] = $dataInfo['item']['item_id'];
				$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] =   Zend_Json_Encoder::encode($dataInfo);
				$item_details = '<tr><td colspan="2" style="height:8px"></td></tr><tr>
									<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
									<strong>'.stripslashes($dataInfo['item']['name']).'</strong>
									</td>
								</tr>';
				$invoice_items_arr[$item_arr]['inv']['name']	=	stripslashes($dataInfo['item']['name']);
				$item_total = 0;
											
							$sub_total = $this->view->price($dataInfo['item']['price']) * $dataInfo['quantity'] ;
							
							$invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::INVOICE_PRICE_MARGINE_EXISTS_1] 	= 	Settings_Service_Price::is_exists('1', $global_conf);
							
							$item_details .= '<tr><td colspan="2" style="height:2px"></td></tr><tr>
									<td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6">'.
										'<table width="100%">
											<tr>
												<td><img src="'.$this->view->serverUrl().$this->view->baseUrl().'/'.$dataInfo['item']['primary_file_field_format'].'" height="60" title="'.stripslashes($dataInfo['item']['name']).'" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;"  /><span style="color:#F60; font-weight:bold; font-style:italic; text-decoration:underline;">';
												if(!empty($dataInfo['item']['model_num'])){ $item_details .= $this->_translator->translator('b2b_invoice_model_number').' '.stripslashes($dataInfo['item']['model_num']) ; } 
												if(!empty($dataInfo['item']['category_name'])){ $item_details .= ' <br /> '.$this->_translator->translator('b2b_invoice_category_name' ) .' '. stripslashes($dataInfo['item']['category_name']); } 
												/*if(!empty($b2b_info['feature_bedroom'])){ $item_details .=  ' , ' . (int)stripslashes($b2b_info['feature_bedroom']).' '.$this->_translator->translator('b2b_front_page_bedroom'); }*/
												$item_details .= '</span><br />'.
												'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("b2b_invoice_short_description").' </strong></span> '.$this->b2b_truncate($dataInfo['item']['short_description'], 0, 150, true, 'UTF-8').
												'<br  />'.
												'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("b2b_invoice_expire_time").' </strong></span> '.$dataInfo['item']['time_of_expiry'].
												'<br  />'.
												'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("b2b_invoice_member_name").' </strong></span> '.$dataInfo['item']['full_name'].
												'<br  />'.
												'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("b2b_invoice_store_url").' </strong></span> <a href="'.$this->view->serverUrl().$this->view->baseUrl().'/Company-AboutUs/'.$dataInfo['item']['company_title'].'" target="_blank">'.$this->_translator->translator("b2b_invoice_store_url_click").'</a>'.
												'<br  />'.
												'</td>'.
											'</tr>'.
										'</table>'.
									'</td>'.
									'<td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6">'.
										'<table border="0" width="100%">'.
											'<tr>'.
												'<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_total_quantity").' </span> '.$dataInfo['quantity'].'</td>'.
											'</tr>'.												
											'<tr>'.
												'<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_subtotal_cost").' </span> : '.$currencySymbol.' '.number_format($sub_total, 2, '.', ',').' '.$currencyShortName;
											if($invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::INVOICE_PRICE_MARGINE_EXISTS_1])
											{	
												$item_details .= '&nbsp;&nbsp;<span style="font-weight: bold; line-height: 22px; color: #F60;">('.$this->_translator->translator("b2b_invoice_include_price_margine").Settings_Service_Price::getMargine('1', $global_conf).')</span>';
											}
								$item_details .= '</td>'.
											'</tr>';
											if(Settings_Service_Price::is_exists('3', $global_conf, $sub_total))
											{												
												$booking_fee = Settings_Service_Price::getMargine('3');
												$booking_fee_amount	=	$this->view->price($sub_total, null, '3');
												$sub_total = $sub_total + $booking_fee_amount;
												$booking_fee_show	=	(preg_match("/%/i", $booking_fee)) ? $currencySymbol.' '.$booking_fee_amount.' '.$booking_fee : $booking_fee;
												$item_details .= '<tr>'.
													'<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_booking_fee").' : </span> : '.$booking_fee_show.'</td>'.
												'</tr>';
												$item_details .= '<tr>'.
													'<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_total_title").' : </span> : '.$currencySymbol.' '.number_format($sub_total, 2, '.', ',').' '.$currencyShortName.'</td>'.
												'</tr>';
											}
						$item_details .= '</table>'.
									'</td>
								</tr>';	
								$item_total += 	$sub_total;	
				if($b2b_info && $b2b_info['billing_item_desc'])
				{
					$item_details .= '<tr><td colspan="2" style="padding:26px 0px 15px 10px;"><strong>'.$this->_translator->translator("b2b_front_page_b2b_desc").'</strong>'.stripslashes($property_info['billing_item_desc']).'</td></tr>';
				}
				$marchent_email_id_arr[$cart_result_key] 											= 	$dataInfo['item']['username'];
				$invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::INVOICE_BOOK_FEE_SHOW] 	= 	$booking_fee_show;
				$invoice_items_arr[$item_arr]['inv']['quantity'] 									= 	$dataInfo['quantity'];
				$invoice_items_arr[$item_arr]['inv']['original_price'] 								= 	$dataInfo['item']['original_price'];
				$invoice_items_arr[$item_arr]['inv']['price'] 										= 	$dataInfo['item']['price'];
				$invoice_items_arr[$item_arr]['inv']['sub_total'] 									= 	$sub_total;
				$invoice_items_arr[$item_arr]['inv']['primary_file_field_format']					=	stripslashes($dataInfo['item']['primary_file_field_format']);					
				$invoice_items_arr[$item_arr]['inv']['model_num']									=	$dataInfo['item']['model_num'];
				$invoice_items_arr[$item_arr]['inv']['category_name']								=	$dataInfo['item']['category_name'];
				$invoice_items_arr[$item_arr]['inv']['time_of_expiry']								=	$dataInfo['item']['time_of_expiry'];
				$invoice_items_arr[$item_arr]['inv']['short_description']							=	$dataInfo['item']['short_description'];
				$invoice_items_arr[$item_arr]['inv']['full_name']									=	$dataInfo['item']['full_name'];
				$invoice_items_arr[$item_arr]['inv']['store_url']									=	$this->view->serverUrl().$this->view->baseUrl().'/Company-AboutUs/'.$dataInfo['item']['company_title'];	
				$invoice_items_arr[$item_arr]['inv']['min_order_quantity']									=	$dataInfo['item']['min_quantity'];	
						
					
				$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $item_total;
				$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
				$total_tax = 0;							
				$total_amount += $item_total;
				$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
				$item_arr++;
			}
		}	
				
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
					<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
					<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("b2b_invoice_total_title").'&nbsp;</div>
					</td>
					<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format(($total_amount + $total_tax), 2, '.', ',').' '.$currencyShortName.'</strong></td>
				</tr>';
		
		/*********ASSIGN PAYMENT INFO AND PAY TO START*******************/		
		if($marchent->isMarchentPaymentEnable(array('item_count' => $item_arr, 'dataInfo' => $dataInfo)))
		{
			$invoice_arr[Eicra_File_Constants::PAY_TO] = '<strong>'.stripslashes($dataInfo['item']['full_name']).'</strong>';
			$invoice_arr[Eicra_File_Constants::PAY_TO] .=  ($dataInfo['item']['package_name']) ? '<br /><strong>'.$dataInfo['item']['package_name'].'</strong>' : '';
			if($dataInfo['item']['is_profile'] == 'YES')
			{
				if($dataInfo['item']['is_telephone'] == 'YES')
				{
					$invoice_arr[Eicra_File_Constants::PAY_TO] .= '<br />'.$this->_translator->translator('b2b_invoice_phone').' '.stripslashes($dataInfo['item']['phone']).stripslashes($dataInfo['item']['phone_part2']).stripslashes($dataInfo['item']['phone_part3']);
				}
				$invoice_arr[Eicra_File_Constants::PAY_TO] .= '<br />'.$this->_translator->translator('b2b_invoice_address').' '.stripslashes($dataInfo['item']['city']).', '.stripslashes($dataInfo['item']['state']).', '.stripslashes($dataInfo['item']['country_name']);
			}			
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $dataInfo['item']['user_id'];
		}
		/*********ASSIGN PAYMENT INFO AND PAY TO END*******************/
		
			
		$services_charge = 0;
		if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
		{
			$services_charge = $this->view->price($total_amount, null,'4');
			$services_charge_margine = Settings_Service_Price::getMargine('4');
			$services_charge_margine_show	=	(preg_match("/%/i", $services_charge_margine)) ? $currencySymbol.' '.$services_charge.' '.$services_charge_margine : $services_charge_margine;
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("b2b_invoice_service_charge").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$services_charge_margine_show.'</strong></td>'.
			'</tr>';
		}
		$now_payable = 0;
		$deposit_charge = 0;
		if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount))
		{	
			$deposit_charge		= $this->view->price($total_amount, null, '5');
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("b2b_invoice_deposit_charge", Settings_Service_Price::getMargine('5')).'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($deposit_charge, 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
			
			$now_payable	=	$services_charge	+	$deposit_charge;
			
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">'.$this->_translator->translator("b2b_invoice_deposit_payable").'</td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($now_payable, 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
		}
		if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
		{	
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("b2b_invoice_grand_total").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format(($total_amount+$total_tax+$services_charge), 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
		}
		if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount) && !empty($now_payable))
		{
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">'.
				'<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right; font-weight:bold;" height="40" colspan="2">'.$this->_translator->translator("b2b_invoice_later_payable", $currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge) - $now_payable), 2, '.', ',').' '.$currencyShortName).'</td>'.
			'</tr>';
		}
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
														</table>';
		
		$invoice_arr[Eicra_File_Constants::MARCHENT_EMAIL_ID] 			= 	($marchent_email_id_arr[0]) ? implode(',', $marchent_email_id_arr) : '';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] 		= 	$services_charge;
		$invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] 		= 	$deposit_charge;
		$invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] 		= 	$now_payable;
		$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] 		= 	$total_amount;
		$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] 			= 	$total_tax;	
		$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] 		= 	$payment_info;	
		$invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT]		=	$currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge)), 2, '.', ',').' '.$currencyShortName;
				
		//Initialize Invoice Action
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] 			= 'B2b';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] 	= 'createItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] 	= 'updateItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] 	= 'deleteItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] 	= 'queryFunction';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] 	= 'paidItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] 	= 'unpaidItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] 	= 'cancelItinerary';
		
		//Initialize Email Template
		$template_id_field	=	'default_template_id';
		$settings_db = new Invoice_Model_DbTable_Setting();
		$template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);			
		if($template_info)
		{
			$invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("b2b_invoice_template_id");	
			
			$templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id'] );
			$invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
			$invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
		}
		else
		{
			$invoice_arr[Eicra_File_Constants::INVOICE_DESC] = '<div class="infoMess">'.$this->_translator->translator('invoice_no_invoice_template_found', '', 'Invoice').'</div>';
			$invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = $this->_translator->translator('invoice_no_invoice_template_found', '', 'Invoice');
		}
		$invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;
	
		$return = array('status' => 'ok','invoice_arr' => $invoice_arr);		
		
		return 	$return;
	}
	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?','invoice_template')
								->limit(1);
		}
		else
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.id = ?',$letter_id)
								->limit(1);
		}
							
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$templates_arr = $row ;
			}
		}
		else
		{
			$templates_arr['templates_desc'] = '';
		}			
		foreach($datas as $key=>$value)
		{
			$templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_desc']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_desc']);
			$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_title']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_title']);
		}
		return $templates_arr;
	}	
	
	private function b2b_truncate($phrase,$start_words, $max_words, $char = false, $charset = null)
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				  $dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				  $phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
				}
		}
		else
		{
		   $phrase_array = explode(' ',$phrase);
		   if(count($phrase_array) > $max_words && $max_words > 0)
			  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
	   return $phrase;
	}
	
	public function registerAction()
    {
		
		$selected_role_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $this->_request->getParam('role_id');	
		
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$role_info['display_type']	=	'display_frontend' ;
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		
		$registrationForm =  new Members_Form_UserForm ($role_info);
		if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
		$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';	
			
        if ($this->_request->isPost()) 
		{			
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				$element_name = $element->getName();
				if($element->getType() == 'Zend_Form_Element_File')
				{					
					$registrationForm->removeElement($element_name);
				}
				$element_name_arr = explode('_',$element_name);
				if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
				{
					$package_msg = 'ok';
				}
			}
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			try
			{	
				if($role_info['role_lock'] == '1' && $role_info != null)
				{			
									
					if ($registrationForm->isValid($this->_request->getPost())) 
					{
						$registrationForm =  new Members_Form_UserForm ($role_info);
						$registrationForm->populate($this->_request->getPost());
						$username = $this->_request->getPost('username');
						$user_check = new Members_Controller_Helper_Registers(); 
									
						if($user_check->getUsernameAvailable($username))
						{
							if($this->_request->getPost('password') == $this->_request->getPost('confirmPassword'))
							{
								$fromValues = $registrationForm;
								$members = new Members_Model_Members($this->_request->getPost());
								$perm = new Members_View_Helper_Allow();
								$members->setRole_id($selected_role_id);							
																															
								if($role_info)
								{
									if($role_info['auto_approve'] == '1')
									{							
										$members->setStatus(1);
									}
									else
									{
										$members->setStatus(0);
									}
								}
								else
								{
									$members->setStatus($translator->translator("set_frontend_registration_auto_publish", '', 'Members'));
								}	
														
								$members->setLoginurl($loginUrl);
										
								$result = $members->saveRegister();
								
								if($result['status'] == 'ok')
								{
									$msg = $translator->translator("member_registered_successfull", '', 'Members');
									$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
									if(!empty($role_info) && !empty($role_info['form_id']))
									{
										$field_db = new Members_Model_DbTable_Fields();
										$packageObj = new Members_Controller_Helper_Packages();
										$field_groups = $field_db->getGroupNames($form_info['id']); 
										
										//Add Data To Database
										foreach($field_groups as $group)
										{
											$group_name = $group->field_group;
											$displaGroup = $registrationForm->getDisplayGroup($group_name);
											$elementsObj = $displaGroup->getElements();
											foreach($elementsObj as $element)
											{
												$table_id = $result['id'];
												$form_id = $form_info['id'];
												$field_id	=	$element->getAttrib('rel');
												$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
												if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
												{ 
													if(is_array($field_value)) { $field_value = ''; }
												}
												if($element->getType() == 'Zend_Form_Element_Multiselect') 
												{ 
													$field_value	=	$this->_request->getPost($element->getName());
													if(is_array($field_value)) { $field_value = implode(',',$field_value); }
												}
												try
												{
													$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
													$DBconn = Zend_Registry::get('msqli_connection');
													$DBconn->getConnection();
													$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
													if($package_msg == 'ok')
													{
														$e_name = $element->getName();
														$element_name_arr = explode('_',$e_name);
														if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
														{
															$package_info = $packageObj->getFormAllDatas($field_value);
														}
													}
													$msg = $translator->translator("member_registered_successfull",'' ,'Members');
													$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
												catch(Exception $e)
												{
													$msg = $translator->translator("member_registered_err",'' ,'Members');
													$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
											}
										}
										
										//Email Send
										$register_helper = new Members_Controller_Helper_Registers();
										$field_info = $field_db->getFieldsInfo($role_info['form_id']);
										$allDatas = $members->getAllDatas();
										
										if($field_info)
										{
											$atta_count = 0;
											$attach_file_arr = array();
											foreach($field_info as $element)
											{
												if($element->field_type == 'file')
												{
													$attach_file_arr[$atta_count] = $allDatas[$element->field_name];
													$atta_count++;
												}
											}
										}
										else
										{
											$attach_file_arr = null;
										}
										
										$allDatas['status'] = ($data['status'] == '1') ? 'active' : 'inactive';
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();										
										try 
										{
											if(!empty($allDatas['member_package']))
											{	
												if($registrationForm->member_package)
												{
													$allDatas['member_package'] = $registrationForm->member_package->getMultiOption($allDatas['member_package']);
												}
												else
												{
													$allDatas['member_package'] = '';
												}
											}
											else
											{							
												$allDatas['member_package'] = '';
											}
											$register_helper->sendMail($allDatas,$form_info,$attach_file_arr);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
										//Delete Attached Files
										if($form_info['attach_file_delete'] == '1')
										{
											if($attach_file_arr != null)
											{
												foreach($attach_file_arr as $key=>$value)
												{
													if(!empty($value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}								
												}
											}
										}
										if(!empty($package_msg) && !empty($package_info))
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
										else
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
									}
									else
									{
										$register_helper = new Members_Controller_Helper_Registers();
										$allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
										$allDatas['username'] = $members->getUsername();
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();
										$allDatas['title'] = $members->getTitle();
										$allDatas['firstName'] = $members->getFirstName();
										$allDatas['lastName'] = $members->getLastName();
										$allDatas['member_package'] = '';
										try 
										{
											$register_helper->sendMail($allDatas,null,null);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
									}
									
									//Authentication Start
									
									// Get our authentication adapter and check credentials						
									$adapter = $this->getAuthAdapter(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))) ));				
									$auth = Zend_Auth::getInstance();
									$result = $auth->authenticate($adapter);
									if (!$result->isValid()) 
									{	
										$pending_error = $this->checkPendingError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password')))) );	
											
										// Invalid credentials	
										if($pending_error)
										{
											$msg = $translator->translator("member_credentials_pending_err", '', 'Members');
										}
										else if($this->checkFrontendLoginError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))))) )
										{
											$msg = $translator->translator("member_credentials_frontend_login_err", '', 'Members');
										}
										else
										{								
											$msg = $translator->translator("member_credentials_err", '', 'Members');
										}
										$datas['failedCause'] = $msg;										
										$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));					
									}
									else
									{
										
										Eicra_Global_Variable::getSession()->username= $result->getIdentity();
										$users= new Administrator_Model_DbTable_Users();
														
										$data= array ('last_access' => date('Y-m-d H:i:s'));
										$where= $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
										if (!$users->update($data,$where)) 
										{
										  $msg = $translator->translator("member_last_access_err", '', 'Members');
										  $json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
										}
										else
										{
											$userInfo = $adapter->getResultRowObject(null, array('password'));
											$authStorage = $auth->getStorage();
											$authStorage->write($userInfo);
												
											$msg = $translator->translator("member_registered_successfull",'' ,'Members').' '.$translator->translator("member_loggin_successfull",Eicra_Global_Variable::getSession()->username, 'Members').$translator->translator("member_loggin_redirecting", '', 'Members');
											$json_arr = array('status' => 'ok','msg' => $msg, 'role_id' => $userInfo->role_id, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}			
									 }
								}
								else
								{
									$msg = $translator->translator("member_registered_err",'' ,'Members');
									$json_arr = array('status' => 'err','msg' => $msg.' '.$result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
								}				
							}
							else
							{
								$msg = $translator->translator("password_not_match",'' ,'Members');
								$json_arr = array('status' => 'errP','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
							}
						}
						else
						{
							$msg = $translator->translator("member_availability",$username, 'Members');
							$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						}
					}
					else
					{
						$validatorMsg = $registrationForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}			
				}
				else
				{
					$msg = $translator->translator("member_registered_not_permitted",$role_info['role_name'],'Members');
					$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			catch(Exception $e)
			{
				$msg = $e->getMessage();
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
			{
				$this->view->selected_role_id	=	$selected_role_id;					
				$this->view->registrationForm = $registrationForm;
			}
			else
			{
				throw new Exception("Register page not found");
			}
		}			
    }
	
	protected function getAuthAdapter($values)
	{
		$tbl = Zend_Registry::get('dbPrefix').'user_profile';
		$dbAdapter  = Zend_Registry::get('msqli_connection');		
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);			
		$authAdapter = new Zend_Auth_Adapter_DbTable(
                               $dbAdapter,
								array( 'mt' => $tbl),
								'mt.username',
								'mt.password',
                        	'MD5(CONCAT(mt.salt,?))'
                             );
		$authSelect = $authAdapter->getDbSelect();
		$authSelect->joinLeft(array('r' => Zend_Registry::get('dbPrefix').'roles'), 'mt.role_id = r.role_id', array('*'));
		$authSelect->where('r.allow_login_from_frontend = ?','1');
		$authAdapter->setIdentity($values['username']);	
		$authAdapter->setCredential($values['password']);
		
		return $authAdapter;
	}
	
	private function checkPendingError($datas)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => Zend_Registry::get('dbPrefix').'user_profile',
						'field' => 'status',
						'exclude' => 'username = "'.$datas['username'].'" AND  password = MD5(CONCAT(salt,"'.$datas['password'].'"))',
					)
				);
		return $validator->isValid(0);
	}
	
	private function checkFrontendLoginError($datas)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$mem_info = $mem_db->getMemberInfoByUsername($datas['username']);
		if($mem_info && $mem_info['role_id'])
		{
			$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => Zend_Registry::get('dbPrefix').'roles',
							'field' => 'allow_login_from_frontend',
							'exclude' => 'role_id = "'.$mem_info['role_id'].'" ',
						)
					);
			return $validator->isValid(0);
		}
		else
		{
			return false;
		}
	}
}

