<?php

class B2b_MembersController extends Zend_Controller_Action {

    private $_translator;
    private $_controllerCache;
    private $_auth_obj;

    public function init() {
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() .
                '/Frontend-Login';
        Eicra_Global_Variable::checkSession($this->_response, $url);
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;

        if ($getAction != 'companyprofile') {
            $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
            $profileTable = new B2b_Model_DbTable_CompanyProfile();
            $usrProfile = $profileTable->isUserExists($user_id);
            if (empty($usrProfile['id'])) {
                $this->getRequest()->setActionName('companyprofile');
                $this->getRequest()->setParams(array('noprofile' => 1));
            }
        }
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath(
                APPLICATION_PATH . '/layouts/scripts/' .
                $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout(true));
    }

    private function companyUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array('table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '', 'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max');
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ***************************************************For
         * Primary************************************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_profile_image';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

    public function companyprofileAction() {
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile'),
                $this->view->url()));
        $profileForm = new B2b_Form_CompanyProfileForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $resultSet = nuull;
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;

            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($profileForm->isValid($this->_request->getPost())) {
                    $companyProfile = new B2b_Model_Companyprofile(
                            $profileForm->getValues());
                    if ($permission->allow()) {
                        if ($user_id) {
                            $companyProfile->setUser_id($user_id);

                            $plan = new B2b_Model_DbTable_Package();
                            $planinfo = $plan->getIDByUser($user_id);
                            $plan_id = $planinfo['id'];
                            if ($planinfo) {
                                $companyProfile->setPlan_id($plan_id);
                            } else {
                                $companyProfile->setPlan_id(0);
                            }
                            $_active = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article ==
                                    '1') ? '1' : '0';
                            $_displayble = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article ==
                                    '1') ? '1' : '0';
                            $companyProfile->setActive($_active);
                            $companyProfile->setDisplayble('1');
                            $result = $companyProfile->saveProfile();
                        } else {
                            $msg = $translator->translator(
                                    "page_access_restrictions");
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_save_err");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                } else {
                    $validatorMsg = $profileForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if ($user_id) {
                $dataModel = new B2b_Model_CompanyprofileMapper();
                $resultSet = $dataModel->getDefaultProfile($user_id);
                $this->companyUploaderSettings($resultSet);
                if (is_array($resultSet) && (count($resultSet) > 1)) {
                    $profileForm->populate($resultSet);
                    $language = explode(',', $resultSet['language']);
                    $certification = explode(',', $resultSet['certification']);
                    $main_markets = explode(',', $resultSet['main_markets']);
                    $delivery_terms = explode(',', $resultSet['delivery_terms']);
                    $payment_currency = explode(',', $resultSet['payment_currency']);
                    $payment_type = explode(',', $resultSet['payment_type']);
                    $profileForm->language->setValue($language);
                    $profileForm->certification->setValue($certification);
                    $profileForm->main_markets->setValue($main_markets);
                    $profileForm->delivery_terms->setValue($delivery_terms);
                    $profileForm->payment_currency->setValue($payment_currency);
                    $profileForm->compliances->setValue(
                            $resultSet['compliances']);
                    $profileForm->payment_type->setValue($payment_type);
                    $profileForm->annual_revenue->setValue(
                            $resultSet['annual_revenue']);
                    $profileForm->number_of_employees->setValue(
                            $resultSet['number_of_employees']);
                    $profileForm->office_size->setValue(
                            $resultSet['office_size']);
                    $profileForm->country->setValue($resultSet['country']);
                    $profileForm->businesstype_id->setValue(
                            $resultSet['businesstype_id']);
                    $profileForm->trade_type->setValue($resultSet['trade_type']);
                    $profileForm->annual_sales->setValue(
                            $resultSet['annual_sales']);

                    if (!empty($resultSet['category_id'])) {
                        $category_db = new B2b_Model_DbTable_Category();
                        $category_info = $category_db->getCategoryById(
                                $resultSet['category_id']);
                        $this->view->assign('category_info', $category_info);
                    }
                }
            }

            $noprofile = $this->_request->getParam('noprofile');
            $this->view->assign('profileForm', $profileForm);
            $this->view->assign('noprofile', $noprofile);
            $userModel = new Members_Model_DbTable_MemberList();
            $userInfo = $userModel->getMemberInfo($user_id);
            $this->view->assign('country', $userInfo['country_name']);
        }
    }

    public function dashboardAction() {
        $translator = Zend_Registry::get('translator');
        $this->view->translator = $translator;
    }

    public function productsAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_product_list_page_name'),
                $this->view->url()));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble', 'operator' => 'eq',
                        'value' => $displayble);
                }
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRout = 'Members-Frontend-Product-List/*';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontendRout, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $product_mapper = new B2b_Model_ProductMapper();
                    $productSql = new B2b_Controller_Helper_ProductSql();
                    $tableColumns = $productSql->getBackendList();

                    $view_datas = $product_mapper->fetchAll($pageNumber, $approve, $posted_data, $tableColumns);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map(
                                        'stripslashes', $entry_arr) : stripslashes(
                                        $entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace(
                                '_', '-', $entry_arr['name']);
                        // $entry_arr['last_access_format']= date('Y-m-d h:i:s
                        // A',strtotime($entry_arr['last_access']));
                        // $entry_arr['register_date_format']= date('Y-m-d h:i:s
                        // A',strtotime($entry_arr['register_date']));
                        $img_thumb_arr = explode(',', $entry_arr['product_images']);
                        $entry_arr['product_images_primary_format'] = ($this->view->escape(
                                        $entry_arr['product_images_primary'])) ? 'data/frontImages/b2b/product_images/' .
                                $this->view->escape(
                                        $entry_arr['product_images_primary']) : (($this->view->escape(
                                        $entry_arr['product_images'])) ? 'data/frontImages/b2b/product_images/' .
                                        $img_thumb_arr[0] : '');
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article ==
                                '1' || $this->_auth_obj->user_id ==
                                $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $data_result, 'total' => $total,
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    /* public function addproductOLDAction ()
      {
      // BreadCrumb
      Eicra_Global_Variable::getSession()->breadcrumb = array(
      array(
      $this->_translator->translator(
      'members_frontend_breadcrumb_dashboard_title'),
      'Members-Dashboard'),
      array(
      $this->_translator->translator(
      'members_frontend_b2b_product_list_page_name'),
      $this->view->url(
      array('module' => $this->view->getModule,
      'controller' => $this->view->getController,
      'action' => 'products'), $route, true)),
      array(
      $this->_translator->translator(
      'menu_b2b_manage_products'), $this->view->url()));

      $posted_data = $this->_request->getParams();
      $this->view->assign('posted_data', $posted_data);
      $company_profile_info = null;
      $auth = Zend_Auth::getInstance();
      $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
      $use_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';
      $productForm = new B2b_Form_ProductForm();

      if (! empty($user_id)) {
      $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
      $company_profile_info = $company_profile_db->getUserProfile(
      $user_id);
      $company_profile_info['group_id'] = ($company_profile_info &&
      $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
      $productForm->group_id->setValue($company_profile_info['group_id']);
      $this->userUploaderSettings($company_profile_info);
      } else {
      $json_arr = array('status' => 'err', 'msg' => 'Login Required');
      $res_value = Zend_Json::encode($json_arr);
      $this->_response->setBody($res_value);
      return;
      }
      // ===================================== Start of checking Package
      // privilege ======================================
      // Checking if user have privilege to add more product
      $dataModel = new B2b_Model_DbTable_Products();
      $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
      $package_id = $auth->getIdentity()->package_id ;
      $package_db = new B2b_Model_DbTable_Package();
      $package_info = $package_db->getPackageById($package_id);
      $current_products_num = $privilege_info['current_products_num'];
      $max_products = $package_info['products'];
      $active = $package_info['preapproval'];

      // when a company profile is set as inactive
      if (empty($company_profile_info['active']) || empty($use_status)) {
      $alert = $privilege_info['company_name'] . ' ' . $this->_translator->translator(
      'b2b_company_account_suspended');
      $this->view->assign('resource_exceeded', $alert);
      return;
      }
      // Checking user permission to add products according to package.
      // count($privilege_info) > 1 && (int)$package_id checks if it exceeds
      // package limit
      // (int)$package_id != 0 check if user has unlimited access bypassing
      // package limit.
      // (int)$max_products != 0 checks if user has unlimited access to add
      // product.

      if (count($privilege_info) > 1 && (int) $package_id != 0 &&
      (int) $max_products != 0) {

      if ($current_products_num > $max_products) {

      $alert = $this->_translator->translator(
      'b2b_max_num_of_product_reached',
      $package_info['package_name']);
      $this->view->assign('resource_exceeded', $alert);
      return;
      }
      }

      // ===================================== End of Package privilege check
      // ======================================

      $product_id = $this->_request->getParam('product_id');

      if ($this->_request->isPost()) {
      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->layout->disableLayout();
      // $this->_translator = Zend_Registry::get('translator');
      $this->view->translator = $this->_translator;
      $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
      $json_arr = null;
      $permission = new B2b_View_Helper_Allow();
      try {
      if ($productForm->isValid($this->_request->getPost())) {
      $productModel = new B2b_Model_Product(
      $productForm->getValues());
      $id = $productForm->getValue('id');

      if ($permission->allow()) {
      if (! empty($user_id)) {
      $productModel->setUser_id($user_id);
      if (! empty($id)) {
      $productModel->setId($id);
      }
      }
      $productModel->setSeo_title(
      $productForm->getValue('name'), $user_id);

      if (strtoupper($active) == 'YES' || empty($active)) {
      $productModel->setActive('1');
      }

      $result = $productModel->save();
      if ($result['status'] == 'ok') {
      $msg = $this->_translator->translator(
      "page_save_success");
      $json_arr = array('status' => 'ok', 'msg' => $msg);
      } else {
      $msg = $this->_translator->translator(
      "page_save_err");
      $json_arr = array('status' => 'err',
      'msg' => $msg . " " . $result['msg']);
      }
      } else {
      $msg = $this->_translator->translator(
      "page_access_restrictions");
      $res_value = Zend_Json::encode($json_arr);
      $this->_response->setBody($res_value);
      return;
      }
      } else {
      $validatorMsg = $productForm->getMessages();
      $vMsg = array();
      $i = 0;
      foreach ($validatorMsg as $key => $errType) {
      foreach ($errType as $errkey => $value) {
      $vMsg[$i] = array('key' => $key,
      'errKey' => $errkey, 'value' => $value);
      $i ++;
      }
      }
      $json_arr = array('status' => 'errV', 'msg' => $vMsg);
      }
      } catch (Exception $e) {
      $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
      }
      $res_value = Zend_Json::encode($json_arr);
      $this->_response->setBody($res_value);
      } else {
      if (! empty($user_id)) {
      if (! empty($product_id)) {
      $productTable = new B2b_Model_DbTable_Products();
      $productRow = $productTable->getProductById($product_id);
      $listAddedBy = $productRow['user_id'];
      $isEditable = ($this->_auth_obj->access_other_user_article ==
      '1' || $user_id == $productRow['user_id']) ? true : false;

      if (is_array($productRow) && (count($productRow) > 1) &&
      $isEditable) {
      $productForm->populate($productRow);
      if (! empty($productRow['category_id'])) {
      $category_db = new B2b_Model_DbTable_Category();
      $category_info = $category_db->getCategoryById(
      $productRow['category_id']);
      $this->view->assign('category_info', $category_info);
      }
      $time_of_expiry = date("l, F d, Y h:i:s A",
      strtotime($productRow['time_of_expiry']));
      $payment_terms = explode(',',
      $productRow['payment_terms']);
      $related_items = explode(',',
      $productRow['related_items']);
      $price_currency = $productRow['price_currency'];
      $min_unit_type = $productRow['min_unit_type'];
      $max_unit_type = $productRow['max_unit_type'];
      $price_per_unit = $productRow['price_per_unit'];
      $delivery_leadtime = $productRow['delivery_leadtime'];
      $unit_size = $productRow['unit_size'];
      $type = explode(',', $productRow['type']);
      $productForm->time_of_expiry->setValue($time_of_expiry);
      $productForm->related_items->setValue($related_items);
      $productForm->price_currency->setValue($price_currency);
      $productForm->payment_terms->setValue($payment_terms);
      $productForm->min_unit_type->setValue($min_unit_type);
      $productForm->max_unit_type->setValue($max_unit_type);
      $productForm->price_per_unit->setValue($price_per_unit);
      $productForm->delivery_leadtime->setValue(
      $delivery_leadtime);
      $productForm->unit_size->setValue($unit_size);
      $productForm->type->setValue($type);
      }
      }
      $dataModel = new B2b_Model_DbTable_Products();
      $productGroup = new B2b_Model_DbTable_ProductGroup();
      $resultSet = $dataModel->getExistingProducts($user_id);
      $productGroups = $productGroup->getAllGroupsByUserId($user_id);
      if ($resultSet) {
      $this->view->assign('previousListings', $resultSet);
      }
      if ($productGroups) {
      $this->view->assign('productGroups', $productGroups);
      }
      $this->view->assign('productForm', $productForm);
      } else {
      $json_arr = array('status' => 'err', 'msg' => 'Login Required');
      $res_value = Zend_Json::encode($json_arr);
      $this->_response->setBody($res_value);
      }
      }
      } */

    public function addproductAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_product_list_page_name'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'products'), 'Members-Frontend-Product-List/*', true)),
            array(
                $this->_translator->translator(
                        'menu_b2b_manage_products'), $this->view->url()));

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        $id = $this->_request->getParam('product_id');
        $productForm = new B2b_Form_ProductForm($this->view->auth);
        $productForm->addElement
                ('checkbox', 
                'accept', 
                    array
                        ('label' => $this->_translator->translator("b2b_terms_condition_accepted_label"),
                        'id' => 'accept', 
                        'title' => $this->_translator->translator("b2b_terms_condition_accepted_title"), 
                        'info' => $this->_translator->translator("b2b_terms_condition_accepted_info"), 
                        'required'    => true,
                        'uncheckedValue'=> '',
                        )
                );
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more product
        $dataModel = new B2b_Model_DbTable_Products();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($productForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $productModel = new B2b_Model_Product($productForm->getValues());
                        $owner_id = $productModel->getUser_id();
                        $id = $productModel->getId();

                        //Check Owner
                        if (!empty($owner_id)) {
                            $company_status = $companyProfile->isActive($owner_id);
                            $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                            $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                            $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;


                            // when a company profile is set as inactive
                            if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                //When a user going to add a listing without creating his/her Company profile.
                                if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                    $privilege_info = $dataModel->getNumOfProductsByUser($owner_id);
                                    $current_products_num = ($privilege_info && $privilege_info['current_products_num']) ? $privilege_info['current_products_num'] : 0;

                                    $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                    $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                    $max_products = ($package_info) ? $package_info['products'] : 0;
                                    $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                    // Checking user permission to add products according to package.
                                    if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                        // ($package_info && ($current_products_num < $max_products))  checks if it exceeds package limit	
                                        if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_products) && !empty($package_id) ) || ($package_info && ($current_products_num <= $max_products)))) {

                                            if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                $productModel->setActive('1');
                                            } else {
                                                $productModel->setActive('0');
                                            }
                                            $productModel->setSeo_title($productModel->getName(), $id);

                                            $result = $productModel->save();
                                            if ($result['status'] == 'ok') {
                                                $msg = $translator->translator("page_save_success");
                                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                                            } else {
                                                $msg = $translator->translator("page_save_err");
                                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                            }
                                        } else {
                                            $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_product_reached_anonymous', $package_info['name']);
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $msg = $translator->translator('b2b_membership_pkg_found_err');
                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                    }
                                } else {
                                    $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $mem_info['full_name']));
                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                }
                            } else {
                                $msg = $translator->translator('b2b_company_profile_not_found');
                                $json_arr = array('status' => 'err', 'msg' => $msg);
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                        }
                    } else {
                        $validatorMsg = $productForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_please_login_again'));
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                $company_status = $companyProfile->isActive($user_id);

                // when a company profile is set as inactive
                if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                    if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                        if (empty($id)) {
                            $privilege_info = $dataModel->getNumOfProductsByUser($user_id);
                            $current_products_num = ($privilege_info && $privilege_info['current_products_num']) ? $privilege_info['current_products_num'] : 0;

                            $package_id = $auth->getIdentity()->package_id;
                            $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);
                            $max_products = ($package_info) ? $package_info['products'] : 0;

                            // Checking user permission to add products according to package.
                            // ($package_info && ($current_products_num < $max_products))  checks if it exceeds package limit	
                            if (empty($package_id) || empty($max_products) || ($package_info && ($current_products_num < $max_products))) {
                                $this->view->assign('resource_exceeded', '');
                            } else {
                                $alert = $translator->translator('b2b_max_num_of_product_reached', $privilege_info['package_name']);
                                $this->view->assign('resource_exceeded', $alert);
                            }
                        } else if (!empty($id) && is_numeric($id)) {
                            $productDetails = $dataModel->getProductById($id);
                            if ($productDetails) {
                                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $productDetails['user_id']) ? true : false;
                                if ($isEditable === true) {
                                    //Get Category Info
                                    $categoryData = new B2b_Model_DbTable_Category();

                                    if (empty($productDetails['category_id'])) {
                                        $category_name = $translator->translator("common_tree_root");
                                    } else {
                                        $categoryInfo = $categoryData->getCategoryName($productDetails['category_id']);
                                        $category_name = $categoryInfo['name'];
                                    }

                                    //ASSIGN TIME OF EXPIRY
                                    $locale = Eicra_Global_Variable::getSession()->sess_lang;
//                                    $productDetails['time_of_expiry'] = date("l, F d, Y H:i:s A", strtotime($productDetails['time_of_expiry']));
                                    $date_obj = new Zend_Date($productDetails['time_of_expiry'], null, $locale);
                                    //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                                    $productDetails['time_of_expiry'] = $date_obj->get(Zend_Date::DATE_FULL . " " . Zend_Date::TIMES); //date("Y-m-d H:i:s", $time);
                                    
                                    
                                    //ASSIGN RELETED ITEMS
                                    $productForm->related_items->setIsArray(true);
                                    $productDetails['related_items'] = explode(',', $productDetails['related_items']);

                                    //ASSIGN TYPE OR STATUS
                                    $productForm->type->setIsArray(true);
                                    $productDetails['type'] = explode(',', $productDetails['type']);

                                    //ASSIGN PAYMENT TYPE 
                                    $productForm->payment_terms->setIsArray(true);
                                    $productDetails['payment_terms'] = explode(',', $productDetails['payment_terms']);

                                    $productForm->populate($productDetails);

                                    $this->view->assign('category_info', $categoryInfo);
                                    $this->view->assign('category_name', $category_name);
                                    $this->view->assign('resource_exceeded', '');
                                }
                            }
                        }

                        //ASSIGN GROUP
                        if ($auth->getIdentity()->access_other_user_article != '1') {
                            $multiOptions = $productForm->group_id->getMultiOptions();
                            $productForm->group_id->clearMultiOptions();
                            foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                if ($company_status['group_id'] == $multiOptionKey) {
                                    $productForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                }
                            }
                        }
                        $productForm->group_id->setValue($company_status['group_id']);
                        if ($auth->getIdentity()->access_file_image_manager != '1') {
                            $this->userUploaderSettings($company_status);
                        }
                        $this->view->assign('productForm', $productForm);
                    } else {
                        $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                        $this->view->assign('resource_exceeded', $alert);
                    }
                } else {
                    $alert = $translator->translator('b2b_company_profile_not_found');
                    $this->view->assign('resource_exceeded', $alert);
                }
            }
        }
    }

    private function userUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array('table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '', 'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max');
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ***************************************************For
         * Primary************************************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_product_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));


        /**
         * ************** For Brochures **************************
         */
        $brochures_param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => 'file_path_brochures',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model_brochures = new Portfolio_Model_Portfolio($brochures_param_fields);
        $brochures_requested_data = $portfolio_model_brochures->getRequestedData();
        $this->view->assign('brochures_settings_info', array_merge($brochures_requested_data, $settings_info));
        $this->view->assign('brochures_settings_json_info', Zend_Json::encode($this->view->brochures_settings_info));
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array('table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size');
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

    public function deleteproductAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $productTbl = new B2b_Model_ProductMapper();
                // page_info
                $proObj = new B2b_Model_DbTable_Products();
                $pro_info = $proObj->getProductById($id);

                $product_images = explode(',', $pro_info['product_images']);
                $product_images_path = 'data/frontImages/b2b/product_images';

                $brochures_file = explode(',', $pro_info['brochures']);
                $brochures_path = 'data/frontImages/b2b/brochures';

                try {
                    $result = $productTbl->delete($id);
                    if ($result['status'] == 'ok') {
                        foreach ($product_images as $key => $file) {
                            if ($file) {
                                $dir = $product_images_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir(
                                                $dir);
                            }
                            if ($res) {
                                $msg = $translator->translator(
                                        'page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            } else {
                                $msg = $translator->translator(
                                        'page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            }
                        }
                        foreach ($brochures_file as $key => $file) {
                            if ($file) {
                                $dir = $brochures_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir(
                                                $dir);
                            }
                            if ($res) {
                                $msg = $translator->translator(
                                        'page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            } else {
                                $msg = $translator->translator(
                                        'page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            }
                        }
                        $msg = $this->_translator->translator(
                                "b2b_common_delete_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $this->_translator->translator(
                                "b2b_common_delete_failed");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err',
                        'msg' => $e->getMessage());
                }
            } else {
                $msg = $this->_translator->translator(
                        "page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg);
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        } else {
            $msg = $this->_translator->translator('b2b_common_delete_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function changepasswordAction() {
        
    }

    public function storeAction() {
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $profileTable = new B2b_Model_DbTable_CompanyProfile();
        $usrProfile = $profileTable->isUserExists($user_id);
        $seo_title = $usrProfile['seo_title'];

        if ($seo_title) {
            $this->redirect('Company-Homepage/' . $seo_title);
        } else {
            $this->getRequest()->setActionName('companyprofile');
            $this->getRequest()->setParams(array('noprofile' => 1));
        }
    }

    public function upgradeAction() {

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'menu_members_upgrade_membership'),
                $this->view->url()));

        $this->view->upgrade_id = $this->_request->getParam('upgrade_id');

        $package_db = new B2b_Model_DbTable_Package();

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            if ($this->view->allow()) {
                if ($this->_request->getPost('upgrade_id')) {
                    $upgrade_id = $this->_request->getPost('upgrade_id');
                    $upgrade_id_arr = explode(',', $upgrade_id);
                    $invoice_arr = $this->generatePackageInvoice($upgrade_id_arr);
                    if ($invoice_arr['status'] == 'ok') {
                        Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];
                        $json_arr = array('status' => 'ok', 'msg' => $this->_translator->translator('b2b_front_membership_pkg_info_submit_success'));
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('invoice_no_invoice_found', '', 'Invoice'));
                    }
                } else {
                    $vMsg[0] = array('key' => 'upgrade_id', 'errKey' => 'upgrade_id', 'value' => $this->_translator->translator('b2b_selected_err'));
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $this->_translator->translator('Member_Access_deny_desc'));
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }


        $package_id = $this->view->auth->getIdentity()->package_id;
        if (!empty($package_id)) {
            $this->view->owner_package_info = $package_db->getPackageById($package_id);
        }
        $this->view->package_info = $package_db->getListInfo('1', null, false);
        $this->view->package_id = $package_id;
    }

    private function generatePackageInvoice($upgrade_info_arr) {
        $mem_db = new Members_Model_DbTable_MemberList();
        $country_db = new Eicra_Model_DbTable_Country();
        $payment_db = new Paymentgateway_Model_DbTable_Gateway();
        $payment_info = $payment_db->getDefaultGateway();
        $auth = $this->view->auth;

        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $currencySymbol = $currency->getSymbol();
        $currencyShortName = $currency->getShortName();
        $marchent = new Invoice_Controller_Helper_Marchent($global_conf);
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $preferences_info = $preferences_db->getOptions();

        $package_db = new B2b_Model_DbTable_Package();


        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            $user_id = $globalIdentity->user_id;
            foreach ($globalIdentity as $globalIdentity_key => $globalIdentity_value) {
                $mem_info[$globalIdentity_key] = $globalIdentity_value;
                $invoice_arr[$globalIdentity_key] = $globalIdentity_value;
            }
            if ($mem_info['country']) {
                $country_info = $country_db->getInfo($mem_info['country']);
                $mem_info['country_name'] = stripslashes($country_info['value']);
            }
            //invoice_arr assigning started
            $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID] = $global_conf['payment_user_id'];
            $invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']) . ' ' . stripslashes($mem_info['firstName']) . ' ' . stripslashes($mem_info['lastName']) . '<br />' . stripslashes($mem_info['mobile']) . '<br />' . stripslashes($mem_info['city']) . ', ' . stripslashes($mem_info['state']) . ', ' . stripslashes($mem_info['postalCode']) . '<br />' . stripslashes($country_info['value']) . '<BR />' . stripslashes($mem_info['username']);
            $invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
            $invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
            $invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = date("Y-m-d h:i:s");
            $invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y", strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
            $invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
            $invoice_arr['site_name'] = stripslashes($global_conf['site_name']);
            $invoice_arr['site_url'] = stripslashes($global_conf['site_url']);
            $this->view->mem_info = $mem_info;
        }
        $owner_package_info = $package_db->getPackageById($mem_info['package_id']);
        $package_info = $package_db->getPackageById($upgrade_info_arr[0]);
        $paid_price = ($owner_package_info && $owner_package_info['price']) ? ($package_info['price'] - $owner_package_info['price']) : $package_info['price'];
        $sub_total = $paid_price;
        //invoice_items_arr assigning started
        $total_amount = 0;
        $total_tax = 0;
        $invoice_items_arr = array();
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
											<tbody>
											<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>' . $this->_translator->translator("b2b_invoice_desc_title") . '</strong></td>
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>' . $this->_translator->translator("b2b_invoice_amount_title") . '</strong></td>
											</tr>';

        //ITEM START
        $item_arr = 0;
        $item_details = '<tr><td colspan="2" style="height:8px"></td></tr>
									 <tr>
										<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
										' . stripslashes($package_info['name']) . '
										</td>
									</tr>';
        $invoice_items_arr[$item_arr]['inv']['package_name'] = stripslashes($package_info['name']);
        $item_details .= '<tr><td colspan="2" style="height:2px"></td></tr><tr>
												<td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6;">' .
                '<table width="100%">
														<tr>
															<td><img src="' . $this->view->serverUrl() . $this->view->baseUrl() . '/data/frontImages/b2b/b2b_images/' . $package_info['b2b_images'] . '" height="60" title="' . stripslashes($package_info['name']) . '" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;"/>'
                . '<span style="color:#F60; font-weight:bold; font-style:italic; text-decoration:underline;">' . stripslashes($package_info['name']) . '</span>' .
                '<br />'
                . '<span style="color:#06C; line-height:20px; font-weight:bold;">' . $this->_translator->translator("b2b_advanced_search_price_label") . ' :</span> ' . $currencySymbol . ' ' . $this->view->numbers(number_format($package_info['price'], 2, '.', ',')) . ' ' . $currencyShortName .
                '<br  />' .
                '<span style="color:#06C; line-height:20px; font-weight:bold;">' . $this->_translator->translator("b2b_invoice_short_description") . ' :</span> ' . stripslashes($package_info['description']) .
                '<br  />' .
                //'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("b2b_invoice_room_condition").' :</span> '.$room_type_info['room_condition'].
                '</td>' .
                '</tr>' .
                '</table>' .
                '</td>' .
                '<td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6; line-height: 22px;"valign="top">' .
                '<table border="0" width="100%">';
        /* '<tr>'.
          '<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_total_night").'</span> : '.$this->view->numbers($total_night).'</td>'.
          '</tr>';

          $item_details .= '<tr>'.
          '<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_total_day").'</span> : '.$this->view->numbers(($total_night + 1)).'</td>'.
          '</tr>'; */

        $item_details .= /* '<tr>'.
                  '<td><span style="font-weight:bold;">'.$this->_translator->translator("b2b_invoice_room_no_chose").'</span> : '.$this->view->numbers(count($dataInfo['apartments_no'.$key])).'</td>'.
                  '</tr>'. */
                '<tr>' .
                '<td><span style="font-weight:bold;">' . $this->_translator->translator("b2b_front_membership_pkg_upgrade_cost_title") . '</span> : ' . $currencySymbol . ' ' . $this->view->numbers(number_format($sub_total, 2, '.', ',')) . ' ' . $currencyShortName . '</td>' .
                '</tr>';

        $item_details .= '</table>' .
                '</td>
											</tr>';
        $item_total += $sub_total;

        $invoice_items_arr[$item_arr]['inv']['package_info'] = $package_info;
        $invoice_items_arr[$item_arr]['inv']['sub_total'] = $sub_total;
        $invoice_items_arr[$item_arr]['inv']['package_id'] = stripslashes($package_info['id']);
        $invoice_items_arr[$item_arr]['inv']['package_image'] = stripslashes($package_info['b2b_images']);
        $invoice_items_arr[$item_arr]['inv']['previous_package_id'] = stripslashes($owner_package_info['id']);
        $invoice_items_arr[$item_arr]['inv']['previous_package_info'] = $owner_package_info;

        $total_amount += $sub_total;
        $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $sub_total;
        $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
        $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;
        $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] = Zend_Json_Encoder::encode($invoice_items_arr[$item_arr]['inv']);
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
        $item_arr++;
        //ITEM END


        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
						<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
						<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("b2b_invoice_total_title") . '</div>
						</td>
						<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>' . $currencySymbol . ' ' . $this->view->numbers(number_format(($total_amount + $total_tax), 2, '.', ',')) . ' ' . $currencyShortName . '</strong></td>
					</tr>';



        $services_charge = 0;

        $now_payable = 0;
        $deposit_charge = 0;

        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
																</table>';

        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;
        $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
        $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format((($total_amount + $total_tax + $services_charge)), 2, '.', ',') . ' ' . $currencyShortName;


        //Initialize Invoice Action
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] = 'B2b';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] = 'upgradePackage';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] = 'deletePackage';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] = 'paidPackage';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] = 'unpaidPackage';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] = 'cancelPackage';

        //Initialize Email Template
        $template_id_field = 'default_template_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
        $invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("b2b_invoice_template_id");

        $register_helper = new Members_Controller_Helper_Registers();
        $templates_arr = $register_helper->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
        $invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
        $invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
        $invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;

        $return = array('status' => 'ok', 'invoice_arr' => $invoice_arr);

        return $return;
    }

    public function massagesAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_product_list_page_name'),
                $this->view->url()));

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_auth_obj->allow_change_user_packages == '1') {
            $send_message = true;
        } else {
            $package_id = $this->_auth_obj->package_id;
            if (!empty($package_id)) {
                $package_db = new B2b_Model_DbTable_Package();
                $package_info = $package_db->getPackageById($package_id);
                if ($package_info) {
                    $send_message = ($package_info['massage'] == 'YES') ? true : false;
                } else {
                    $send_message = false;
                }
            } else {
                $send_message = false;
            }
        }
        $this->view->assign('send_message', $send_message);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                $locale = Eicra_Global_Variable::getSession()->sess_lang;

                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');

                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array('field' => 'displayble', 'operator' => 'eq', 'value' => $displayble);
                }

                $posted_data['hasChild'] = false;

                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRout = 'My-Messages/*';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontendRout, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);

                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj));

                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $inquiry_mapper = new B2b_Model_InquiryMapper();
                    $view_datas = $inquiry_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map(
                                        'stripslashes', $entry_arr) : stripslashes(
                                        $entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace(
                                '_', '-', $entry_arr['name']);

                        // $date = date('Y/m/d', strtotime($entry_arr['date']));
                        // $entry_arr['sent_on'] = implode('/',
                        // array_reverse(explode('/', $date)));
                        // $entry_arr['msg_date']= date('Y-m-d h:i:s
                        // A',strtotime($entry_arr['date']));
                        $date_obj = new Zend_Date($entry_arr['date'], null, $locale);
                        $entry_arr['date_format'] = $this->view->numbers($date_obj->get('dd/MM/YYYY'));
                        $entry_arr['posted_date_format'] = ($entry_arr['date']) ? $this->view->numbers($date_obj->get('dd,MMMM YYYY HH:mm:ss a')) : '';
                        // $entry_arr['register_date_format']= date('Y-m-d h:i:s
                        // A',strtotime($entry_arr['register_date']));
                        $img_thumb_arr = explode(',', $entry_arr['product_images']);
                        $entry_arr['product_images_primary_format'] = ($this->view->escape(
                                        $entry_arr['product_images_primary'])) ? 'data/frontImages/b2b/product_images/' .
                                $this->view->escape(
                                        $entry_arr['product_images_primary']) : (($this->view->escape(
                                        $entry_arr['product_images'])) ? 'data/frontImages/b2b/product_images/' .
                                        $img_thumb_arr[0] : '');
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article ==
                                '1' || $this->_auth_obj->user_id ==
                                $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $data_result, 'total' => $total,
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function replyAction() {
        // BreadCrumb						
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_product_list_page_name'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'massages'), 'My-Messages/*', true)),
            array(
                $this->_translator->translator(
                        'menu_b2b_manage_reply'), $this->view->url()));

        if ($this->_auth_obj->allow_change_user_packages == '1') {
            $send_message = true;
        } else {
            $package_id = $this->_auth_obj->package_id;
            if (!empty($package_id)) {
                $package_db = new B2b_Model_DbTable_Package();
                $package_info = $package_db->getPackageById($package_id);
                if ($package_info) {
                    $send_message = ($package_info['massage'] == 'YES') ? true : false;
                } else {
                    $send_message = false;
                }
            } else {
                $send_message = false;
            }
        }
        $this->view->assign('send_message', $send_message);

        $inquery_db = new B2b_Model_DbTable_Inquiry();

        $id = $this->_request->getParam('id');

        $inquiryForm = new B2b_Form_InquiryForm();
        $auth = $this->view->auth;
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);



        /* $companyID = $companyProfile->hasCompany($company_title);

          if (empty($company_title) || empty($companyID['user_id']) ||
          count($companyID) == 0) {
          $this->_forward('invalidrequest', 'frontend',
          $this->_request->getModuleName());
          return;
          }

          $companyDetails = $companyProfile->getCompanyDetails($company_title,
          true); */
        if (!empty($id)) {
            $search_params['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $id);
            $inquiry_info_obj = $inquery_db->getListInfo(null, $search_params, $tableColumns);
            if ($inquiry_info_obj) {
                $inquiry_info_arr = $inquiry_info_obj->toArray();
            }
            $this->view->assign('companyDetails', $inquiry_info_arr[0]);
            $this->view->assign('company_title', $inquiry_info_arr[0]['company_title']);
        } else {
            $this->_forward('message', $this->_request->getControllerName(), $this->_request->getModuleName());
        }

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;

            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($send_message == true) {
                    if ($inquiryForm->isValid($this->_request->getPost())) {
                        $inquiryModel = new B2b_Model_Inquiry($inquiryForm->getValues());
                        if (!empty($user_id)) {
                            $inquiryModel->setSender_id($user_id);
                        }

                        $inquiryModel->setRecipient_title($inquiry_info_arr[0]['recipient_title']);
                        $inquiryModel->setRecipient_id($this->_request->getPost('recipient_id'));
                        $inquiryModel->setActive('1');
                        $inquiryModel->setStatus('1');
                        $inquiryModel->setParent($inquiry_info_arr[0]['id']);
                        $inquiryModel->setRead('0');

                        $result = $inquiryModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            try {
                                $reply_with = $inquiryModel->getReply_with();
                                if ($reply_with) {
                                    $reply_with_arr = explode(',', $reply_with);
                                    if (in_array('Email', $reply_with_arr)) {
                                        // Injecting preferences to the view model.
                                        $preferences_db = new B2b_Model_DbTable_Preferences();
                                        $preferences_data = $preferences_db->getOptions();

                                        $global_conf = Zend_Registry::get('global_conf');

                                        if ($preferences_data['cron_inquiry_user_email_template_id'] && !empty($preferences_data['cron_inquiry_user_email_template_id'])) {
                                            $form_info = array(
                                                'email_template_set' => $preferences_data['cron_inquiry_user_email_template_id'],
                                                'email_to' => ($inquiry_info_arr[0]['user_email']) ? $inquiry_info_arr[0]['user_email'] : $inquiry_info_arr[0]['email'],
                                                'email_bcc' => $global_conf['global_email'],
                                                'set_from_email' => $this->_auth_obj->username
                                            );
                                            $inquiry_form = $this->getInqueryTemplate(array('inquiryModel' => $inquiryModel, 'companyProfile' => new B2b_Model_DbTable_CompanyProfile()));
                                            $datas = array(
                                                'inquery_subject' => $inquiryModel->getSubject(),
                                                'inquiry_form' => $inquiry_form
                                            );
                                            $email_class = new Members_Controller_Helper_Registers();
                                            $email_class->sendGeneralMail($datas, $form_info);
                                        } else {
                                            $msg .= ' ' . $translator->translator("menu_b2b_inquiry_email_template_not_found");
                                        }
                                    }
                                }
                            } catch (Exception $e) {
                                $msg .= ' ' . $e->getMessage();
                            }
                            $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                        }
                    } else {
                        $validatorMsg = $inquiryForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key,
                                    'errKey' => $errkey, 'value' => $value);
                                $i ++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                    }
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $translator->translator("Member_package_support_send_receive_message_err"), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
            }

            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }



        /* if (empty($companyDetails[0]['company_active']) ||
          empty($companyDetails[0]['user_status'])) {

          $this->_forward('invalidrequest', 'frontend',
          $this->_request->getModuleName());
          return;
          } */




        //$this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('inquiryForm', $inquiryForm);
        //$this->view->assign('company_title', $company_title);
    }

    private function getInqueryTemplate($data_arr) {
        $template = "";
        if ($data_arr['inquiryModel']) {
            $template .= "<table border=\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">";
            $sender_id = $data_arr['inquiryModel']->getSender_id();

            if (!empty($sender_id)) {
                $search_params['filter']['filters'][0] = array('field' => 'user_id', 'operator' => 'eq', 'value' => $sender_id);
                $companySql = new B2b_Controller_Helper_CompanySql ();
                $tableColumns = $companySql->getB2bInquiryCompanyProfileList();
                $sender_info_obj = $data_arr['companyProfile']->getListInfo('1', $search_params, $tableColumns);
                if ($sender_info_obj) {
                    $sender_info = $sender_info_obj->toArray();
                    $template .= "<tr>";
                    $template .= "<td width=\"50%\">";
                    $template .= ($sender_info['full_name']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_name') . "</strong> " . $this->view->escape($sender_info['full_name']) . "<br />\n" : '';
                    $template .= ($sender_info['usr_phone']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_phone') . "</strong> " . $this->view->escape($sender_info['usr_phone']) . "<br />\n" : (($sender_info['cp_phone']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_phone') . "</strong> " . $this->view->escape($sender_info['cp_phone']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_mobile']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_mobile') . "</strong> " . $this->view->escape($sender_info['usr_phone']) . "<br />\n" : (($sender_info['cp_mobile']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_mobile') . "</strong> " . $this->view->escape($sender_info['cp_mobile']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_fax']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_fax') . "</strong> " . $this->view->escape($sender_info['usr_fax']) . "<br />\n" : (($sender_info['cp_fax']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_fax') . "</strong> " . $this->view->escape($sender_info['cp_fax']) . "<br />\n" : '' );
                    $template .= ($data_arr['inquiryModel']->getFeedbacks()) ? "<strong>" . $this->_translator->translator('b2b_message_feedbacks') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getFeedbacks()) . "<br />\n" : '';
                    $template .= ($sender_info['country_code']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_country') . "</strong> <img src=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/data/adminImages/flagsImage/" . $sender_info['country_code'] . ".gif\" border=\"0\" title=\"" . $this->view->escape($sender_info['country_name']) . "\"   alt=\"" . $this->view->escape($sender_info['country_name']) . "\" /> <br />\n" : '';
                    $template .= "</td>";
                    $template .= "<td width=\"50%\">";
                    $template .= ($sender_info['usr_website']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_website') . "</strong> " . $this->view->escape($sender_info['usr_website']) . "<br />\n" : (($sender_info['website']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_website') . "</strong> " . $this->view->escape($sender_info['website']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_address']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_address') . "</strong> " . $this->view->escape($sender_info['usr_address']) . "<br />\n" : (($sender_info['cp_street']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_address') . "</strong> " . $this->view->escape($sender_info['cp_street']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_city']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_city') . "</strong> " . $this->view->escape($sender_info['usr_city']) . "<br />\n" : (($sender_info['cp_city']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_city') . "</strong> " . $this->view->escape($sender_info['cp_city']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_postcode']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_postalcode') . "</strong> " . $this->view->escape($sender_info['usr_postcode']) . "<br />\n" : (($sender_info['cp_postal_code']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_postalcode') . "</strong> " . $this->view->escape($sender_info['cp_postal_code']) . "<br />\n" : '' );
                    $template .= ($sender_info['profile_since']) ? "<strong>" . $this->_translator->translator('b2b_message_member_since') . "</strong> " . $this->view->escape($sender_info['profile_since']) . "<br />\n" : '';
                    $template .= ($data_arr['inquiryModel']->getReply_with()) ? "<strong>" . $this->_translator->translator('b2b_message_reply_with') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getReply_with()) . "<br />\n" : '';
                    $template .= "</td>";
                    $template .= "</tr>";
                    $template .= "<tr>";
                    $template .= "<td colspan=\"2\">";
                    $template .= ($sender_info['usr_company']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_company') . "</strong> " . $this->view->escape($sender_info['usr_company']) . "<br />\n" : (($sender_info['cp_company_name']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_company') . "</strong> " . $this->view->escape($sender_info['cp_company_name']) . "<br />\n" : '' );
                    $template .= ($data_arr['inquiryModel']->getIp()) ? "<strong>" . $this->_translator->translator('b2b_message_sender_ip') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getIp()) . "<br />\n" : '';
                    $template .= "<strong>" . $this->_translator->translator('b2b_message_sender_profile') . "</strong> " . (($sender_info['company_title']) ? "<a href=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/Company-Homepage/" . $this->view->escape($sender_info['company_title']) . "\">" . $this->_translator->translator("b2b_message_view_sender_profile") . "</a>" . "<br />\n" : $this->_translator->translator("b2b_company_profile_not_found") . "<br />\n");
                    $template .= ($sender_info['usr_email']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_email') . "</strong> " . $this->view->escape($sender_info['usr_email']) . "<br />\n" : '';
                    $template .= "<br /><strong>" . $this->_translator->translator("b2b_message_details") . " : </strong> <br />\n<hr />  " . $this->view->escape($data_arr['inquiryModel']->getMessage()) . "   <br /> \n <hr />";
                    $template .= "</td>";
                    $template .= "</tr>";
                }
            } else {
                $template .= "<tr>";
                $template .= "<td width=\"50%\">";
                $template .= ($data_arr['inquiryModel']->getFeedbacks()) ? "<strong>" . $this->_translator->translator('b2b_message_feedbacks') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getFeedbacks()) . "<br />\n" : '';
                $template .= "</td>";
                $template .= "<td width=\"50%\">";
                $template .= ($data_arr['inquiryModel']->getReply_with()) ? "<strong>" . $this->_translator->translator('b2b_message_reply_with') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getReply_with()) . "<br />\n" : '';
                $template .= "</td>";
                $template .= "</tr>";
                $template .= "<tr>";
                $template .= "<td colspan=\"2\">";
                $template .= ($data_arr['inquiryModel']->getIp()) ? "<strong>" . $this->_translator->translator('b2b_message_sender_ip') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getIp()) . "<br />\n" : '';
                $template .= "<strong>" . $this->_translator->translator('b2b_message_sender_profile') . "</strong> " . (($sender_info['company_title']) ? "<a href=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/Company-Homepage/" . $this->view->escape($sender_info['company_title']) . "\">" . $this->_translator->translator("b2b_message_view_sender_profile") . "</a>" . "<br />\n" : $this->_translator->translator("b2b_company_profile_not_found") . "<br />\n");
                $template .= "<br />\n<strong>" . $this->_translator->translator("b2b_message_details") . " : </strong> <br />\n<hr />  " . $this->view->escape($data_arr['inquiryModel']->getMessage()) . "   <br /> \n <hr />";
                $template .= "</td>";
                $template .= "</tr>";
            }
            $template .= "</table>";
        }
        return $template;
    }

    public function readmassageAction() {
        
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        // $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow('deleteall', 'members', 'B2b')) {
                if (!empty($id_str)) {
                    $productTbl = new B2b_Model_ProductMapper();
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $result = $productTbl->delete($id);
                            if ($result['status'] == 'ok') {
                                $msg = $this->_translator->translator(
                                        "b2b_common_delete_success");
                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            } else {
                                $msg = $this->_translator->translator(
                                        "b2b_common_delete_failed");
                                $json_arr = array('status' => 'err',
                                    'msg' => $msg . " " . $result['msg']);
                                break;
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err',
                                'msg' => $e->getMessage());
                            break;
                        }
                    }
                } else {
                    $msg = $this->_translator->translator(
                            "category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $this->_translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $this->_translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function preferencesAction() {
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'member_account_preferences'),
                $this->view->url()));

        $preferencesForm = new B2b_Form_UserPreferencesForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($preferencesForm->isValid($this->_request->getPost())) {
                    $userPreferences = new B2b_Model_UserPreferences(
                            $preferencesForm->getValues());
                    if ($permission->allow()) {
                        if ($user_id) {
                            $userPreferences->setUser_id($user_id);
                            $result = $userPreferences->save();
                        } else {
                            $msg = $translator->translator(
                                    "page_access_restrictions");
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_save_err");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                } else {
                    $validatorMsg = $preferencesForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {

            if ($user_id) {

                $dataModel = new B2b_Model_DbTable_UserPreferences();
                $userProfileDatabase = new B2b_Model_DbTable_CompanyProfile();
                $resultSet = $dataModel->getPreferences($user_id);

                if (is_array($resultSet) && (count($resultSet) > 1)) {
                    $preferencesForm->populate($resultSet);
                    $facebook_display = explode(',', $resultSet['facebook_display']);
                    $facebook = explode(',', $resultSet['facebook_display']);
                    $googleplus_display = explode(',', $resultSet['googleplus_display']);
                    $googleplus = explode(',', $resultSet['googleplus_display']);
                    $youtube_display = explode(',', $resultSet['youtube_display']);
                    $youtube = explode(',', $resultSet['youtube_display']);
                    $twitter_display = explode(',', $resultSet['twitter_display']);
                    $twitter = explode(',', $resultSet['twitter_display']);
                    $preferencesForm->group_id->setValue($groupinfo['group_id']);
                    $preferencesForm->facebook_display->setValue($facebook);
                    $preferencesForm->googleplus_display->setValue($googleplus);
                    $preferencesForm->youtube_display->setValue($youtube);
                    $preferencesForm->twitter_display->setValue($twitter);
                }
            }

            $company_profile_info = $userProfileDatabase->getUserProfile(
                    $user_id);

            $company_profile_info['group_id'] = ($company_profile_info &&
                    $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
            $preferencesForm->group_id->setValue(
                    $company_profile_info['group_id']);
            $preferencesForm->group_id->setValue(
                    $company_profile_info['group_id']);
            $storeurl = $this->view->serverUrl().$this->view->baseUrl() . '/Company-Store/' .
                    $company_profile_info['seo_title'];
            $preferencesForm->storeurl->setValue($storeurl);
            $this->userDefaultUploaderSettings($company_profile_info);
            $this->view->assign('preferencesForm', $preferencesForm);
        }
    }

    public function productsgroupsAction() {
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        if ($user_id) {
            $productGroup = new B2b_Model_DbTable_ProductGroup();
            $groupByUser = $productGroup->getAllGroupsByUserId($user_id);
            $this->view->assign('productGroup', $groupByUser);
        } else {
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $msg = $translator->translator("page_access_restrictions");
            $json_arr = array('status' => 'ok', 'msg' => $msg);
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function storyAction() {
        $translator = Zend_Registry::get('translator');
        $sStoryForm = new B2b_Form_SuccessStoriesForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($sStoryForm->isValid($this->_request->getPost())) {
                    $storyModel = new B2b_Model_SuccessStories(
                            $sStoryForm->getValues());


                    if ($permission->allow() && !empty($user_id)) {
                        $storyModel->setUser_id($user_id);
                        $storyModel->setSeo_title($sStoryForm->getValue('name'), $user_id);
                        $result = $storyModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator(
                                "page_access_restrictions");
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $sStoryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            $res_value = Zend_Json::encode($json_arr);

            $this->_response->setBody($res_value);
        }

        if (!empty($user_id)) {

            // image uplaoder for options
            // =======================================================================
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile(
                    $user_id);
            $company_profile_info['group_id'] = ($company_profile_info &&
                    $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;


            try {
                $storiesTable = new B2b_Model_DbTable_SuccessStories();
                $myStory = $storiesTable->getStorybyuser($user_id);

                if (is_array($myStory) && (count($myStory) > 0)) {
                    $sStoryForm->populate($myStory);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getCode());
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
            $this->userDefaultUploaderSettings($company_profile_info);
            $this->view->assign('sStoryForm', $sStoryForm);
        } else {
            $json_arr = array('status' => 'err', 'msg' => 'Login Required');
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function addproductgroupAction() {
        $productGroup = new B2b_Form_ProductGroupForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $isedit = $this->_request->getParam('pgrp_id');
        if ($isedit) {
            $groupTable = new B2b_Model_DbTable_ProductGroup();
            $grouprow = $groupTable->getGroupInfo($isedit);
            $addedBy = $grouprow['user_id'];
            if (is_array($grouprow) && (count($grouprow) > 1) &&
                    $user_id == $addedBy) {
                $productGroup->populate($grouprow);
            }
        }
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($productGroup->isValid($this->_request->getPost())) {
                    $productGrp = new B2b_Model_ProductGroup(
                            $productGroup->getValues());
                    if ($permission->allow()) {
                        if ($user_id) {
                            $productGrp->setUser_id($user_id);
                            $result = $productGrp->saveGroup();
                        } else {
                            $msg = $translator->translator(
                                    "page_access_restrictions");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_save_err");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                } else {
                    $validatorMsg = $productGroup->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->assign('productGroup', $productGroup);
        }
    }

    public function deleteproductgroupAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $this->view->translator = $translator;
        $json_arr = null;
        $id = $this->_request->getParam('id');
        $permission = new B2b_View_Helper_Allow();
        if ($permission->allow('deleteproductgroup', 'members', 'B2b')) {
            try {
                if (empty($id) || !is_numeric($id)) {
                    $msg = $translator->translator(
                            "members_control_page_deleted_group_invalid");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                } else {
                    $productGrp = new B2b_Model_ProductGroupMapper();
                    $result = $productGrp->delete($id);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator(
                                "members_control_page_deleted_group_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $translator->translator(
                                "members_control_page_deleted_group_failed");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
        } else {
            $msg = $translator->translator("page_access_restrictions");
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function userDefaultUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array('table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '', 'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max');
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ***************************************************For
         * Primary************************************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_b2b_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

    public function tradeshowslistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_trade_list_page_name'),
                $this->view->url()));

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_request->isPost()) {
            try {
                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble',
                        'operator' => 'eq',
                        'value' => $displayble
                    );
                }
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRoute = 'Members-Frontend-Trade-Show-List/*';
                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                        ), $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $categoryMapper = new B2b_Model_TradeshowMapper();
                    $view_datas = $categoryMapper->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                        $entry_arr['publish_status_page_name'] = str_replace('_', '-', $entry_arr['name']);
                        $img_thumb_arr = $entry_arr['b2b_images'];
                        $entry_arr['primary_file_field_format'] = $entry_arr['b2b_images'];
                        $entry_arr['added_on_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['added_on'])));
                        $entry_arr['added_on_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['added_on'])));
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function addtradeshowAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_trade_list_page_name'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'tradeshowslist'), 'Members-Frontend-Trade-Show-List/*', true)),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_trade_add_page_name'), $this->view->url()));
        $tradeshowForm = new B2b_Form_TradeShowForm();
        $auth = $this->view->auth;
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($tradeshowForm->isValid($this->_request->getPost())) {
                    $tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
					$memberList = new Members_Model_DbTable_MemberList();
					$package_db = new B2b_Model_DbTable_Package();
                    if ($permission->allow() && !empty($user_id)) {
						
                        $owner_id = $tradeshowForm->getValue('user_id');
                        $tradeshowModel->setSeo_title($tradeshowForm->getValue('name'), $owner_id);
                        $tradeshowModel->setUser_id($owner_id);

                        if ($owner_id) {
                            $tradeshowModel->setUser_id($owner_id);
                        } else {
                            $tradeshowModel->setUser_id($user_id);
                        }
						
						$mem_info_obj = $memberList->getMemberList(null, array('filter' => array( 'filters' => array( array('field' => 'user_id', 'operator' => 'eq', 'value' => $tradeshowModel->getUser_id()))))) ;
						$mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
						$mem_info = ($mem_info_obj_arr  && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;
						
						$package_id = ($mem_info) ? $mem_info['package_id'] : 0;
						$package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);
						$tradShowAllow	=	($package_info && $package_info['tradeshow']) ? $package_info['tradeshow'] : 'YES';
						
                        $_active = ($auth->hasIdentity() && ($auth->getIdentity ()->auto_publish_article == '1' || $tradShowAllow == 'YES')) ? '1' : '0';
                        if ($_active) {
                            $tradeshowModel->setActive($_active);
                        }


                        $result = $tradeshowModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $tradeshowForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }

        if (!empty($user_id)) {

            // image uplaoder for options
            // =======================================================================
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile($user_id);
            $company_profile_info['group_id'] = ($company_profile_info && $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;

            $this->userUploaderTrageShowSettings($company_profile_info);
            $this->view->assign('tradeshowForm', $tradeshowForm);
        } else {
            $json_arr = array(
                'status' => 'err',
                'msg' => 'Login Required'
            );
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function edittradeshowAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_trade_list_page_name'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'tradeshowslist'), 'Members-Frontend-Trade-Show-List/*', true)),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_trade_edit_page_name'), $this->view->url()));
        $id = $this->_request->getParam('id');
        $tradeshowForm = new B2b_Form_TradeShowForm();
        $auth = $this->view->auth;
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;

            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($tradeshowForm->isValid($this->_request->getPost())) {
                    $tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
                    if ($permission->allow() && !empty($user_id)) {
                        $owner_id = $tradeshowForm->getValue('user_id');
                        $tradeshowModel->setSeo_title($tradeshowForm->getValue('name'), $owner_id);
                        $_active = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article == '1') ? '1' : '0';
                        if ($_active) {
                            $tradeshowModel->setActive($_active);
                        }
                        $result = $tradeshowModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg
                        );
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $tradeshowForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if ($id) {
                $tradeshowTable = new B2b_Model_DbTable_Tradeshow();
                $tradeshowDetails = $tradeshowTable->getPackageById($id);

                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $tradeshowDetails['user_id']) ? true : false;

                if (is_array($tradeshowDetails) && (count($tradeshowDetails) > 1) && $isEditable) {
                    $tradeshowForm->populate($tradeshowDetails);
                    $tradeshowForm->getElement('status')->setValue($tradeshowDetails['displayable']);
                } else {
                    $this->_forward('addtradeshow', $this->_request->getControllerName(), $this->_request->getModuleName());
                }
            } else {
                $this->_forward('addtradeshow', $this->_request->getControllerName(), $this->_request->getModuleName());
            }
        }

        if (!empty($user_id)) {

            // image uplaoder for options
            // =======================================================================
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile($user_id);
            $company_profile_info['group_id'] = ($company_profile_info && $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;

            $this->userUploaderTrageShowSettings($company_profile_info);
            $this->view->assign('tradeshowForm', $tradeshowForm);
        } else {
            $json_arr = array(
                'status' => 'err',
                'msg' => 'Login Required'
            );
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function userUploaderTrageShowSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ************** For Primary **************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_b2b_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

}
