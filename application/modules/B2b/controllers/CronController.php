<?php

/* * ***
 *
 * 	PLEASE RUN THE FOLLOWING CRON COMMAND IN CPANEL
 * 	wget -O - -q -t 1 http://demo.realestates.com.bd/B2b/cron/run
 *
 * ** */

class B2b_CronController extends Zend_Controller_Action {

    private $_translator;
    private $_controllerCache;

    public function init() {
        /* Initialize action controller here */
        $translator = Zend_Registry::get('translator');
        $this->_translator = $translator;

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
    }

    public function runAction() {
        try {
            
        
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $today = date("Y-m-d");
        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();

        $buying_db = new B2b_Model_DbTable_Buying();
        $selling_db = new B2b_Model_DbTable_Selling();
        $product_db = new B2b_Model_DbTable_Products();
        $company_db = new B2b_Model_DbTable_CompanyProfile();
        $mem_db = new Members_Model_DbTable_MemberList();
        $role_db = new Administrator_Model_DbTable_Roles();
        $email_obj = new Invoice_View_Helper_Email();

        $letter_arr[Eicra_File_Constants::B2B_BUYING_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_SELLING_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_PRODUCT_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] = '';

        //DEACTIVE BUYING LEADS STARTS
        $search_data = array(
            'filter' => array(
                'logic' => 'and',
                'filters' => array(
                    array('field' => 'validity', 'operator' => 'lt', 'value' => $today),
                    array('field' => 'validity', 'operator' => 'neq', 'value' => '0000-00-00 00:00:00'),
                    array('field' => 'displayble', 'operator' => 'eq', 'value' => '1')
                )
            ),
        );


        $search_info = $buying_db->getListInfo('1', $search_data, array('userChecking' => false, 'user_profile' => array('role_id' => 'usr.role_id', 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)")));
        
        
        foreach ($search_info as $key => $info) {
            $letter_arr[Eicra_File_Constants::B2B_BUYING_LETTER_BODY] .= ($key + 1) . '. ' . $this->view->escape($info['name']) . "  -------------    " . $this->view->escape($info['full_name']) . "<br /> \n\n";
            if ($info['username']) {
                $role_info = $role_db->getInfo($info['role_id']);
                $to_mail = ($role_info['allow_to_send_email'] == '1') ? $info['username'] : null;
            } else {
                $to_mail = null;
            }
            $displayble = '0';
            $letter_type = 'deactivation';
            $validity = $info['validity'];
            $data = array('displayble' => $displayble, 'validity' => date('Y-m-d h:i:s'));

            // Update BUYING LEADS status
            try {
                $buying_db->update($data, 'id = ' . $info['id']);
                if ($preferences_data['cron_buying_user_email_template_id'] && $to_mail) {
                    $data['letter_id'] = $preferences_data['cron_buying_user_email_template_id'];
                    $data[Eicra_File_Constants::INVOICE_TO_EMAIL] = $to_mail;
                    $data[Eicra_File_Constants::B2B_BUYING_LETTER_BODY] = $this->view->escape($info['name']) . "\n\n";
                    $result = $email_obj->sendCommonMail($data);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        //DEACTIVE BUYING LEADS END
        try {
        //DEACTIVE SELLING LEADS STARTS
        $search_data = array(
            'filter' => array(
                'logic' => 'and',
                'filters' => array(
                    array('field' => 'validity', 'operator' => 'lt', 'value' => $today),
                    array('field' => 'validity', 'operator' => 'neq', 'value' => '0000-00-00 00:00:00'),
                    array('field' => 'displayble', 'operator' => 'eq', 'value' => '1')
                )
            ),
        );


        $search_info = $selling_db->getListInfo('1', $search_data, array('userChecking' => false, 'user_profile' => array('role_id' => 'usr.role_id', 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)")));
        }catch (Exception $e) {
                echo $e->getMessage();
                exit;
            }
        
        foreach ($search_info as $key => $info) {
            $letter_arr[Eicra_File_Constants::B2B_SELLING_LETTER_BODY] .= ($key + 1) . '. ' . $this->view->escape($info['name']) . "  -------------    " . $this->view->escape($info['full_name']) . "<br /> \n\n";
            if ($info['username']) {
                $role_info = $role_db->getInfo($info['role_id']);
                $to_mail = ($role_info['allow_to_send_email'] == '1') ? $info['username'] : null;
            } else {
                $to_mail = null;
            }
            $displayble = '0';
            $letter_type = 'deactivation';
            $validity = $info['validity'];
            $data = array('displayble' => $displayble, 'validity' => date('Y-m-d h:i:s'));

            // Update SELLING LEADS status
            try {
                $selling_db->update($data, 'id = ' . $info['id']);
                if ($preferences_data['cron_selling_user_email_template_id'] && $to_mail) {
                    $data['letter_id'] = $preferences_data['cron_selling_user_email_template_id'];
                    $data[Eicra_File_Constants::INVOICE_TO_EMAIL] = $to_mail;
                    $data[Eicra_File_Constants::B2B_SELLING_LETTER_BODY] = $this->view->escape($info['name']) . "\n\n";
                    $result = $email_obj->sendCommonMail($data);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        //DEACTIVE SELLING LEADS END
        //DEACTIVE PRODUCTS STARTS
        $search_data = array(
            'filter' => array(
                'logic' => 'and',
                'filters' => array(
                    array('field' => 'time_of_expiry', 'operator' => 'lt', 'value' => $today),
                    array('field' => 'time_of_expiry', 'operator' => 'neq', 'value' => '0000-00-00 00:00:00'),
                    array('field' => 'displayble', 'operator' => 'eq', 'value' => '1')
                )
            ),
        );


        $search_info = $product_db->getListInfo('1', $search_data, array('userChecking' => false, 'user_profile' => array('role_id' => 'usr.role_id', 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)")));

        foreach ($search_info as $key => $info) {
            $letter_arr[Eicra_File_Constants::B2B_PRODUCT_LETTER_BODY] .= ($key + 1) . '. ' . $this->view->escape($info['name']) . "  -------------    " . $this->view->escape($info['full_name']) . "<br /> \n\n";
            if ($info['username']) {
                $role_info = $role_db->getInfo($info['role_id']);
                $to_mail = ($role_info['allow_to_send_email'] == '1') ? $info['username'] : null;
            } else {
                $to_mail = null;
            }
            $displayble = '0';
            $letter_type = 'deactivation';
            $validity = $info['time_of_expiry'];
            $data = array('displayble' => $displayble, 'time_of_expiry' => date('Y-m-d h:i:s'));

            // Update PRODUCTS status
            try {
                $product_db->update($data, 'id = ' . $info['id']);
                if ($preferences_data['cron_product_user_email_template_id'] && $to_mail) {
                    $data['letter_id'] = $preferences_data['cron_product_user_email_template_id'];
                    $data[Eicra_File_Constants::INVOICE_TO_EMAIL] = $to_mail;
                    $data[Eicra_File_Constants::B2B_PRODUCT_LETTER_BODY] = $this->view->escape($info['name']) . "\n\n";
                    $result = $email_obj->sendCommonMail($data);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        //DEACTIVE PRODUCTS END
        //DEACTIVE COMPANIES STARTS
        $search_data = array(
            'filter' => array(
                'logic' => 'and',
                'filters' => array(
                    array('field' => 'added_day_num', 'operator' => 'eq', 'value' => $today),
                    array('field' => 'added_on', 'operator' => 'neq', 'value' => '0000-00-00 00:00:00')
                )
            ),
        );
        $cron_sql = array(
            'b2b_company_profile' => array(
                'id' => 'cpy.id',
                'company_name' => 'cpy.company_name',
                'company_title' => 'cpy.seo_title',
                'profile_image_primary' => 'cpy.profile_image_primary',
                'company_information' => 'cpy.company_information',
                'added_on' => 'cpy.added_on',
                '(DATEDIFF(now(), cpy.added_on)) AS added_day_num'),
            'user_profile' => array('role_id' => 'usr.role_id', 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
            'membership_packages' => array('package_name' => 'pid.name',
                'renewable' => 'pid.renewable'),
            'userChecking' => false);


        $search_info = $company_db->getListInfo('1', $search_data, $cron_sql);

        foreach ($search_info as $key => $info) {
            $letter_arr[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] .= ($key + 1) . '. ' . $this->view->escape($info['company_name']) . "  -------------    " . $this->view->escape($info['full_name']) . "<br /> \n\n";
            if ($info['username']) {
                $role_info = $role_db->getInfo($info['role_id']);
                $to_mail = ($role_info['allow_to_send_email'] == '1') ? $info['username'] : null;
            } else {
                $to_mail = null;
            }
            $active = '0';
            $letter_type = 'deactivation';
            $validity = $info['added_on'];
            $data = array('active' => $active, 'added_on' => date('Y-m-d h:i:s'));

            // Update COMPANIES status
            try {
                $company_db->update($data, 'id = ' . $info['id']);
                if ($preferences_data['cron_company_user_email_template_id'] && $to_mail) {
                    $data['letter_id'] = $preferences_data['cron_company_user_email_template_id'];
                    $data[Eicra_File_Constants::INVOICE_TO_EMAIL] = $to_mail;
                    $data[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] = $this->view->escape($info['company_name']) . "\n\n";
                    $result = $email_obj->sendCommonMail($data);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        //DEACTIVE COMPANIES END
        //SEND MAIL TO ADMINISTRATOR
        if ($preferences_data['cron_admin_email_template_id']) {
            $letter_arr['letter_id'] = $preferences_data['cron_admin_email_template_id'];
            $result = $email_obj->sendMail($letter_arr);
        }
        } catch (Exception $ex) {
            echo $e->getMessage();
            exit;
        }
        echo $letter_arr[Eicra_File_Constants::B2B_BUYING_LETTER_BODY] . '<br /><br /><br />' . $letter_arr[Eicra_File_Constants::B2B_SELLING_LETTER_BODY] . '<br /><br /><br />' . $letter_arr[Eicra_File_Constants::B2B_PRODUCT_LETTER_BODY] . '<br /><br /><br />' . $letter_arr[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] . "SuccessFull..<br />" . $result['msg'];
        exit;
    }

    public function isnot_int($property_owner) {
        $r = true;
        if (is_numeric($property_owner)) {
            if (is_int((int) $property_owner)) {
                $r = false;
            }
        }
        return $r;
    }
    
    
    
    
    
    function usercheckAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();

        $buying_db = new B2b_Model_DbTable_Buying();
        $selling_db = new B2b_Model_DbTable_Selling();
        $product_db = new B2b_Model_DbTable_Products();
        $company_db = new B2b_Model_DbTable_CompanyProfile();
        $mem_db = new B2b_Model_DbTable_MemberList();
        $role_db = new Administrator_Model_DbTable_Roles();
        $email_obj = new Invoice_View_Helper_Email();

        $letter_arr[Eicra_File_Constants::B2B_BUYING_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_SELLING_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_PRODUCT_LETTER_BODY] = '';
        $letter_arr[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] = '';



        //Deactivate User Profile
        $search_data = array(
            'filter' => array(
                'logic' => 'and',
                'filters' => array(
                    array('field' => 'status', 'operator' => 'eq', 'value' => 1)
                )
            ),
        );


        $queryCheck = TRUE;
        $searchUser = $mem_db->getListInfo('1', $search_data);
        $today = date("Y-m-d H:i:s");

        foreach ($searchUser AS $key => $val) {
            if($val['package_id'] > 0){ //only applicable for those users who are under any package
                $dateOfJoin = $val['register_date'];
                $dayOfRenewal = $val['renewable'];
                //generating date of expiry from calculation
                $dateOfJoin = date("Y-m-d H:i:s", strtotime($dateOfJoin));
                $dateOfJoin = strtotime($dateOfJoin);
                $dateOfExpiry = strtotime("+" . $dayOfRenewal . " days", $dateOfJoin);
                $dateOfExpiry = date('Y-m-d H:i:s', $dateOfExpiry);

                if ($dateOfExpiry <= $today) {
                    if ($val['username']) {
                        $to_mail = ($val['allow_to_send_email'] == '1') ? $val['username'] : null;
                    } else {
                        $to_mail = null;
                    }
                    $inactive = 0;
                    try {

                        //deactivating user profile
                        $data_member = array('status' => $inactive, 'register_date' => '0000-00-00 00:00:00');
                        $updateMember = $mem_db->update($data_member, 'user_id = ' . $val['user_id']);

                        //deactivating company profile
                        $data_company = array('active' => $inactive, 'added_on' => '0000-00-00 00:00:00');
                        $updateMember = $company_db->update($data_company, 'user_id = ' . $val['user_id']);

                        //deactivating all products
                        $data_product = array('active' => $inactive);
                        $updateMember = $product_db->update($data_product, 'user_id = ' . $val['user_id']);

                        //deactivating all buying leads
                        $data_buying = array('active' => $inactive);
                        $updateMember = $buying_db->update($data_product, 'user_id = ' . $val['user_id']);

                        //deactivating all products
                        $data_selling = array('active' => $inactive);
                        $updateMember = $selling_db->update($data_product, 'user_id = ' . $val['user_id']);


                        if ($preferences_data['cron_company_user_email_template_id'] && $to_mail) {
                            $data['letter_id'] = $preferences_data['cron_company_user_email_template_id'];
                            $data[Eicra_File_Constants::INVOICE_TO_EMAIL] = $to_mail;
                            $data[Eicra_File_Constants::B2B_COMPANY_LETTER_BODY] = "Dear " . $val['firstName'] . ", \n\n Your account expired on " . $dateOfExpiry . ". Company profile, products, buying leads, selling leads associated to this account has been deactivated also.";
                            $result = $email_obj->sendCommonMail($data);
                        }


                        if ($data_member && $data_company && $data_product && $data_buying && $data_selling) {
                            //do nothing for now
                        } else {
                            $queryCheck = FALSE;
                        }
                    } catch (Exception $e) {
                        $queryCheck = FALSE;
                        echo $e->getMessage();
                    }
                }
            }
        }

        if ($queryCheck) {
            echo "System updated successfully";
        } else {
            echo "Something went wrong.";
        }
    }

}
