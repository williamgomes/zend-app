<?php

class B2b_SuccessstoriesController extends Zend_Controller_Action
{

    private $_controllerCache;

    private $_auth_obj;

    public function init ()
    {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch ()
    {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = Zend_Registry::get('config')->eicra->params->domain . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    public function addAction ()
    {
        $translator = Zend_Registry::get('translator');
        $sStoryForm = new B2b_Form_SuccessStoriesForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($sStoryForm->isValid($this->_request->getPost())) {
                    $storyModel = new B2b_Model_SuccessStories($sStoryForm->getValues());
                    if ($permission->allow() && ! empty($user_id)) {

                        $owner_id = $sStoryForm->getValue('user_id');

                        if (empty($owner_id)) {
                            $company_status = $storyModel->setUser_id($user_id);
                            $storyModel->setSeo_title($sStoryForm->getValue('name'), $owner_id);
                        } else {
                            $storyModel->setSeo_title($sStoryForm->getValue('name'), $owner_id);
                        }

                        $result = $storyModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    }

                    else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $sStoryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            }

            catch (Exception $e) {

                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }

            $res_value = Zend_Json::encode($json_arr);

            $this->_response->setBody($res_value);
        }

        if (! empty($user_id)) {

            // image uplaoder for options
            // =======================================================================
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile($user_id);
            $company_profile_info['group_id'] = ($company_profile_info && $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;

            $this->userUploaderSettings($company_profile_info);
            $this->view->assign('sStoryForm', $sStoryForm);
        } else {
            $json_arr = array(
                'status' => 'err',
                'msg' => 'Login Required'
            );
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function editAction ()
    {
        $id = $this->_request->getParam('id');
        $sStoryForm = new B2b_Form_SuccessStoriesForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($sStoryForm->isValid($this->_request->getPost())) {
                    $storyModel = new B2b_Model_SuccessStories($sStoryForm->getValues());
                    $owner_id = $sStoryForm->getValue('user_id');
                    $storyModel->setSeo_title($sStoryForm->getValue('name'), $owner_id);

                    if ($permission->allow() && ! empty($user_id)) {

                        $result = $storyModel->save();

                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    }

                    else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $sStoryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            }

            catch (Exception $e) {

                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }

            $res_value = Zend_Json::encode($json_arr);

            $this->_response->setBody($res_value);
        }

        else {

            if ($id) {

                $storyTable = new B2b_Model_DbTable_SuccessStories();
                $storyDetails = $storyTable->getPackageById($id);
                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $storyDetails['user_id']) ? true : false;

                if (is_array($storyDetails) && (count($storyDetails) > 1) && $isEditable) {
                    $sStoryForm->populate($storyDetails);
                }

                else {

                    $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                }
            }

            else {

                $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
            }
        }

        if (! empty($user_id)) {

            // image uplaoder for options
            // =======================================================================
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile($user_id);
            $company_profile_info['group_id'] = ($company_profile_info && $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;

            $this->userUploaderSettings($company_profile_info);
            $this->view->assign('sStoryForm', $sStoryForm);
        } else {
            $json_arr = array(
                'status' => 'err',
                'msg' => 'Login Required'
            );
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function deleteAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                try {
                    $result = $successStoriesMapper->delete($id);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg']
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage()
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        } else {
            $msg = $translator->translator('b2b_common_delete_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                if (! empty($id_str)) {
                    $id_arr = explode(',', $id_str);

                    foreach ($id_arr as $id) {
                        try {
                            $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                            $result = $successStoriesMapper->delete($id);
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator("b2b_common_delete_success");
                                $json_arr = array(
                                    'status' => 'ok',
                                    'msg' => $msg
                                );
                            } else {
                                $msg = $translator->translator("b2b_common_delete_failed");
                                $json_arr = array(
                                    'status' => 'err',
                                    'msg' => $msg . " " . $result['msg']
                                );
                            }
                        } catch (Exception $e) {
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $e->getMessage() . ' ' . $id
                            );
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $msg
                    );
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function listAction ()
    {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_request->isPost()) {
            try {
                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble',
                        'operator' => 'eq',
                        'value' => $displayble
                    );
                }
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRoute = 'adminrout';
                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                ), $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (! empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                    $view_datas = $successStoriesMapper->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace('_', '-', $entry_arr['name']);
                        $img_thumb_arr = $entry_arr['b2b_images'];
                        $entry_arr['primary_file_field_format'] = $entry_arr['b2b_images'];
                        $entry_arr['added_on_format'] = date('Y-m-d h:i:s
                        A', strtotime($entry_arr['added_on']));
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function publishAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $productName = $this->_request->getPost('name');
            $paction = $this->_request->getPost('paction');
            $data = array();
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            try {
                $data = array(
                    'active' => $active
                );
                $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                $json_arr = $successStoriesMapper->updateStatus($data, $id);
                $json_arr['active'] = $active;
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage(),
                    'active' => $active
                );
            }
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishallAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            if (! empty($id_str)) {
                $id_arr = explode(',', $id_str);
                $data = array(
                    'active' => $active
                );
                foreach ($id_arr as $id) {
                    try {
                        $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                        $json_arr = $successStoriesMapper->updateStatus($data, $id);
                        $json_arr['active'] = $active;
                    } catch (Exception $e) {
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $e->getMessage(),
                            'active' => $active
                        );
                    }
                }
            }
        } else {
            $msg = $translator->translator("page_selected_err");
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function displayableAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $displayable = $this->_request->getPost('paction');
                $successStoriesMapper = new B2b_Model_SuccessStoriesMapper();
                try {
                    $result = $successStoriesMapper->displayable($id, $displayable);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'displayable' => $displayable
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'displayable' => $displayable
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'displayable' => $displayable
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'displayable' => $displayable
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function userUploaderSettings ($info)
    {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ************** For Primary **************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_b2b_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

    private function dynamicUploaderSettings ($info)
    {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }
}
