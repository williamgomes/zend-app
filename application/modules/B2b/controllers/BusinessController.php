<?php

class B2b_BusinessController extends Zend_Controller_Action
{

    private $_DBconn;

    private $_page_id;

    private $_controllerCache;

    public function init ()
    
    {
        
        /* Initialize action controller here */
        $translator = Zend_Registry::get('translator');
        
        $this->view->assign('translator', $translator);
        
        $this->view->setEscape('stripslashes');
        
        // DB Connection
        
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        
        $this->_DBconn->getConnection();
        
        // Initialize Cache
        
        $cache = new Eicra_View_Helper_Cache();
        
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch ()
    
    {
        $template_obj = new Eicra_View_Helper_Template();
        
        $template_obj->setFrontendTemplate();
        
        $front_template = Zend_Registry::get('front_template');
        
        $this->_helper->layout->setLayoutPath(
                APPLICATION_PATH . '/layouts/scripts/' .
                         $front_template['theme_folder']);
        
        $this->_helper->layout->setLayout($template_obj->getLayout(true));
        
        $this->view->assign('front_template', $front_template);
        
        if ($this->_request->getParam('menu_id')) 

        {
            
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            
            $page_id_arr = $viewHelper->_getContentId();
            
            $this->_page_id = (! empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } 

        else 

        {
            
            $this->_page_id = null;
        }
    }

    public function categorylistAction ()
    
    {}

    public function productdetailsAction ()
    
    {}

    public function productslistAction ()
    
    {}

    public function companylistAction ()
    
    {}

    public function storylistAction ()
    
    {}

    public function membershiAction ()
    
    {}

    public function tradeshowsAction ()
    
    {}

    public function tradeshowsdetailsAction ()
    
    {}

    public function storydetailsAction ()
    
    {}

    public function preferencesAction ()
    
    {}

    public function membersareaAction ()
    
    {}

    public function membershippkgsAction ()
    
    {}

    public function sendinquiryAction ()
    
    {}

    public function formAction ()
    
    {}
}