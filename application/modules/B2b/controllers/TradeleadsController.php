<?php

class B2b_TradeleadsController extends Zend_Controller_Action
{


    private $_controllerCache;

    private $_auth_obj;

    public function init ()
    {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch ()
    {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(
                MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = Zend_Registry::get('config')->eicra->params->domain .
            $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg(
                    $this->_request->getModuleName());
        }
    }



    public function addAction()
    {
        $tradeshowForm = new B2b_Form_TradeShowForm ();
        $auth = Zend_Auth::getInstance ();
        $user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost())
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator	= $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow ();
            try
            {
                if($tradeshowForm->isValid($this->_request->getPost()))
                {
                    $tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
                    if ($permission->allow() && !empty($user_id)){
                        $tradeshowModel->setUser_id($user_id);
                        $result = $tradeshowModel->save();
                        if($result['status'] == 'ok')
                        {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok','msg' => $msg);
                        }
                        else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
                        }


                    }


                    else
                    {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }


                }
                else
                {
                    $validatorMsg = $tradeshowForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach($validatorMsg as $key => $errType)
                    {
                        foreach($errType as $errkey => $value)
                        {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV','msg' => $vMsg);
                }
            }


            catch(Exception $e)
            {

                $json_arr = array('status' => 'err','msg' => $e->getMessage());
            }


            $res_value = Zend_Json::encode($json_arr);


            $this->_response->setBody($res_value);
        }

        $this->view->assign('tradeshowForm' , $tradeshowForm);
    }
    public function editAction()
    {

        $id = $this->_request->getParam('id');

        $tradeshowForm = new B2b_Form_TradeShowForm ();
        $auth = Zend_Auth::getInstance ();
        $user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost())
        {
        	$this->_helper->viewRenderer->setNoRender();
        	$this->_helper->layout->disableLayout();
        	$translator = Zend_Registry::get('translator');
        	$this->view->translator	= $translator;
        	$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        	$json_arr = null;
        	$permission = new B2b_View_Helper_Allow ();
        	try
        	{
        		if($tradeshowForm->isValid($this->_request->getPost()))
        		{
        			$tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
        			if ($permission->allow() && !empty($user_id)){
        				$result = $tradeshowModel->save();

        				if($result['status'] == 'ok')
        				{
        					$msg = $translator->translator("page_save_success");
        					$json_arr = array('status' => 'ok','msg' => $msg);
        				}
        				else {
        					$msg = $translator->translator("page_save_err");
        					$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
        				}

        			}

        			else
        			{
        				$msg = $translator->translator("page_access_restrictions");
        				$json_arr = array('status' => 'err','msg' => $msg);
        				$res_value = Zend_Json::encode($json_arr);
        				$this->_response->setBody($res_value);
        				return;
        			}

        		}
        		else
        		{
        			$validatorMsg = $tradeshowForm->getMessages();
        			$vMsg = array();
        			$i = 0;
        			foreach($validatorMsg as $key => $errType)
        			{
        				foreach($errType as $errkey => $value)
        				{
        					$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
        					$i++;
        				}
        			}
        			$json_arr = array('status' => 'errV','msg' => $vMsg);
        		}
        	}


        	catch(Exception $e)
        	{
        		$json_arr = array('status' => 'err','msg' => $e->getMessage());
        	}

        	$res_value = Zend_Json::encode($json_arr);
        	$this->_response->setBody($res_value);
        }

        else {

        	if ($id){

        		$tradeshowTable = new B2b_Model_DbTable_Tradeshow();
        		$tradeshowDetails = $tradeshowTable->getPackageById($id);

        		if ( is_array($tradeshowDetails) && (count($tradeshowDetails) > 1 ) && !empty($tradeshowDetails) ){
        			$tradeshowForm->populate($tradeshowDetails);
        		}

        		else {

        			$this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());

        		}

        	}

        	else {

        		$this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());

        	}
        }

        $this->view->assign('tradeshowForm' , $tradeshowForm);

    }
    public function deleteAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost())
        {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()){
                $id = $this->_request->getPost('id');
                $tradeshowMapper= new B2b_Model_TradeshowMapper();
                try {
                    $result = $tradeshowMapper->delete($id);
                    if($result['status'] == 'ok')
                    {
                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array('status' => 'ok','msg' => $msg);
                    }
                    else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
                    }
                }
                catch (Exception $e) {
                    $json_arr = array('status' => 'err','msg' => $e->getMessage());
                }
            }
            else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err','msg' => $msg);
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
        else
        {
            $msg = 	$translator->translator('b2b_common_delete_failed');
            $json_arr = array('status' => 'err','msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }
    public function deleteallAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $categoryTbl = new B2b_Model_DbTable_Category();
        $allData =  $categoryTbl->getTree();

        echo '<pre>';
        print_r($allData);
        echo '</pre>';
        ////$resultSet = array_filter($node, 'strlen');
    }
    public function listAction()
    {

        /* $pageNumber = $this->getRequest()->getParam('page');
        $getViewPageNum = $this->getRequest()->getParam('viewPageNum');
        $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
        if(empty($getViewPageNum) && empty($viewPageNumSes))
        {
            $viewPageNum = '30';
        }
        else if(!empty($getViewPageNum) && empty($viewPageNumSes))
        {
            $viewPageNum = $getViewPageNum;
        }
        else if(empty($getViewPageNum) && !empty($viewPageNumSes))
        {
            $viewPageNum = $viewPageNumSes;
        }
        else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
        {
            $viewPageNum = $getViewPageNum;
        }
        Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
        if( ($datas = $this->_controllerCache->load($uniq_id)) === false )
        {
            $categoryMapper = new B2b_Model_TradeshowMapper();
            $datas =  $categoryMapper->fetchAll($pageNumber);
            $this->_controllerCache->save($datas, $uniq_id);
        }
        $this->view->assign('tradeshow' , $datas); */


        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_request->isPost()) {
            try {
                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array(
                            'field' => 'displayble', 'operator' => 'eq',
                            'value' => $displayble);
                }
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                        'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRoute = 'adminrout';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                                'controller' => $this->view->getController,
                                'action' => $this->view->getAction,
                                'approve' => $approve,
                                'displayble' => $displayble,
                                'page' => ($pageNumber == '1' ||
                                        empty($pageNumber)) ? null : $pageNumber),
                        $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (! empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_',
                                $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                false) {
                    $categoryMapper = new B2b_Model_TradeshowMapper();
                    $view_datas = $categoryMapper->fetchAll($pageNumber,
                            $approve, $posted_data);
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map(
                                'stripslashes', $entry_arr) : stripslashes(
                                        $entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace(
                                '_', '-', $entry_arr['name']);
                        $img_thumb_arr = $entry_arr['b2b_images'];
                        $entry_arr['primary_file_field_format'] = $entry_arr['b2b_images'];
                        $entry_arr['added_on_format'] = date(
                                'Y-m-d h:i:s
                        A',
                                strtotime($entry_arr['added_on']));
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article ==
                                '1' ||
                                $this->_auth_obj->user_id ==
                                $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array('status' => 'ok',
                        'data_result' => $data_result, 'total' => $total,
                        'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                        'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }


    }
}
