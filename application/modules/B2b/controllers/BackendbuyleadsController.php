<?php

class B2b_BackendbuyleadsController extends Zend_Controller_Action {

    private $_controllerCache;
    private $_auth_obj;

    public function init() {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    public function addOLDAction() {
        $buyingForm = new B2b_Form_BuyingForm();
        $translator = Zend_Registry::get('translator');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);


        // Checking if user have privilege to add more buying leads.
        $dataModel = new B2b_Model_DbTable_Buying();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($buyingForm->isValid($this->_request->getPost())) {
                    $buyingModel = new B2b_Model_Buying($buyingForm->getValues());
                    $owner_id = $this->_request->getParam('user_id');
                    $companyProfile = new B2b_Model_DbTable_CompanyProfile ();
                    $privilege_info = null;
                    $company_status = null;

                    if ($owner_id) {
                        $privilege_info = $dataModel->getNumOfLeads($owner_id);
                        $company_status = $companyProfile->isActive($owner_id);
                    } else {
                        $privilege_info = $dataModel->getNumOfLeads($user_id);
                        $company_status = $companyProfile->isActive($user_id);
                    }

                    if ($owner_id) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $mem_info = $memberList->getMemberInfo($owner_id);
                        if ($mem_info) {
                            $package_id = $mem_info['package_id'];
                        } else {
                            $package_id = 0;
                        }
                    } else {
                        $package_id = $auth->getIdentity()->package_id;
                    }

                    $package_db = new B2b_Model_DbTable_Package();
                    $package_info = $package_db->getPackageById($package_id);
                    $current_leads_num = $privilege_info['current_leads_num'];
                    $max_lead = $package_info['buying'];
                    $active = $package_info['preapproval'];

                    // when a company profile is set as inactive
                    if (empty($company_status['active']) || empty($user_status)) {
                        $msg = $translator->translator('b2b_company_account_suspended_anonymous', $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name'] . ' ) ');
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                    // Checking user permission to add products according to package.
                    // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                    // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                    // (int)$max_lead != 0 checks if user has unlimited access to add product.

                    if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {

                        if ($current_leads_num >= $max_lead) {

                            $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_buying_reached_anonymous', $privilege_info['package_name']);
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                    }
                    if ($owner_id) {
                        $buyingModel->setUser_id($owner_id);
                        $buyingModel->setSeo_title($buyingForm->getValue('name'), $owner_id);
                    } else {
                        $buyingModel->setUser_id($user_id);
                        $buyingModel->setSeo_title($buyingForm->getValue('name'), $user_id);
                    }

                    if ($permission->allow()) {

                        // empty ($active) means if user be an admin or root
                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $buyingModel->setActive('1');
                        } else {
                            $buyingModel->setActive('0');
                        }

                        $result = $buyingModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $buyingForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {

                // =======================================================================
                $privilege_info = $dataModel->getNumOfLeads($user_id);
                $package_id = $privilege_info['package_id'];
                $current_leads_num = $privilege_info['current_leads_num'];
                $max_lead = $privilege_info['max_lead'];
                $companyProfile = new B2b_Model_DbTable_CompanyProfile ();
                $company_status = $companyProfile->isActive($user_id);

                // when a company profile is set as inactive
                if (empty($company_status['active']) || empty($user_status)) {
                    $msg = $privilege_info['company_name'] . ' ' . $translator->translator('b2b_company_account_suspended');
                    $this->view->assign('resource_exceeded', $msg);
                    return;
                }
                // Checking user permission to add products according to package.
                // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                // (int)$max_lead != 0 checks if user has unlimited access to add product.

                if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {

                    if ($current_leads_num >= $max_lead) {

                        $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_buying_reached_anonymous', $privilege_info['package_name']);
                        $msg = $translator->translator('b2b_max_num_of_buying_reached', $privilege_info['package_name']);
                        $this->view->assign('resource_exceeded', $msg);
                        return;
                    }
                }
                // image uplaoder for options
                // =======================================================================

                $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
                $company_profile_info = $company_profile_db->getUserProfile(
                        $user_id);
                $company_profile_info['group_id'] = ($company_profile_info &&
                        $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
                $buyingForm->group_id->setValue($company_profile_info['group_id']);
                $this->userUploaderSettings($company_profile_info);

                // =======================================================================

                $memberList = new Members_Model_DbTable_MemberList();
                $bizGroup = new B2b_Model_DbTable_Group();
                $memberList = new Members_Model_DbTable_MemberList();
                $allMembers = $memberList->getMembers()->toArray();

                if (count($allMembers) && !empty($allMembers)) {
                    $userList = array();
                    $userList[''] = $translator->translator('b2b_select');
                    foreach ($allMembers as $row) {
                        $userList[$row['user_id']] = $row['title'] . '  ' . $row['firstName'] . '  ' . $row['lastName'] . '  ';
                    }
                    $buyingForm->user_id->setMultiOptions($userList);
                    $buyingForm->user_id->setValue($user_id);
                }
                $buyingForm->loadB2bGroup();
                $buyingForm->loadAllCountries();
                $this->view->assign('buyingForm', $buyingForm);
            } else {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => 'Login Required'
                );
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function addAction() {
        $buyingForm = new B2b_Form_BuyingForm($this->view->auth);
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more buying leads.
        $dataModel = new B2b_Model_DbTable_Buying();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to add buying leads

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($buyingForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $buyingModel = new B2b_Model_Buying($buyingForm->getValues());
                        $owner_id = $buyingModel->getUser_id();
                        //Check Owner
                        if (!empty($owner_id)) {
                            $company_status = $companyProfile->isActive($owner_id);
                            $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                            $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                            $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;

                            //When a user going to add a listing without creating his/her Company profile.
                            if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                // when a company profile is set as inactive
                                if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                    $privilege_info = $dataModel->getNumOfLeads($owner_id);
                                    $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                                    $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                    $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                    $max_lead = ($package_info) ? $package_info['buying'] : 0;
                                    $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                    // Checking user permission to add products according to package.
                                    if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                        // empty($package_id) check if user has unlimited access bypassing package limit.
                                        // empty($max_lead)checks if user has unlimited access to add buying leads.
                                        // Checking user permission to add buying leads according to package.
                                        // ($package_info && ($current_leads_num < $max_lead))  checks if it exceeds package limit
                                        if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_lead) && !empty($package_id) ) || ($package_info && ($current_leads_num < $max_lead)))) {
                                            if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                $buyingModel->setActive('1');
                                            } else {
                                                $buyingModel->setActive('0');
                                            }
                                            $buyingModel->setSeo_title($buyingModel->getName());

                                            $result = $buyingModel->save();
                                            if ($result['status'] == 'ok') {
                                                $msg = $translator->translator("page_save_success");
                                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                                            } else {
                                                $msg = $translator->translator("page_save_err");
                                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                            }
                                        } else {
                                            $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_buying_reached_anonymous', $package_info['name']);
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $msg = $translator->translator('b2b_membership_pkg_found_err');
                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                    }
                                } else {
                                    $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $this->view->escape($mem_info['full_name']) . ' ( ' . $this->view->escape($company_status['company_name']) . ' ) '));
                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                }
                            } else {
                                $msg = $translator->translator('b2b_company_profile_not_found');
                                $json_arr = array('status' => 'err', 'msg' => $msg);
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                        }
                    } else {
                        $validatorMsg = $buyingForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i ++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                $company_status = $companyProfile->isActive($user_id);

                //When a user going to this page without creating his/her Company profile.
                if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                    // when a company profile is set as inactive
                    if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                        $privilege_info = $dataModel->getNumOfLeads($user_id);
                        $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                        $package_id = $auth->getIdentity()->package_id;
                        $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);
                        $max_lead = ($package_info) ? $package_info['buying'] : 0;

                        // empty($package_id) check if user has unlimited access bypassing package limit.
                        // empty($max_lead)checks if user has unlimited access to add buying leads.
                        // Checking user permission to add buying leads according to package.
                        // ($package_info && ($current_leads_num < $max_lead))  checks if it exceeds package limit
                        if (empty($package_id) || empty($max_lead) || ($package_info && ($current_leads_num < $max_lead))) {
                            // check if user has unlimited access to change any group from list
                            if ($auth->getIdentity()->access_other_user_article != '1') {
                                $multiOptions = $buyingForm->group_id->getMultiOptions();
                                $buyingForm->group_id->clearMultiOptions();
                                foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                    if ($company_status['group_id'] == $multiOptionKey) {
                                        $buyingForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                    }
                                }
                            }

                            $buyingForm->group_id->setValue($company_status['group_id']);
                            $this->view->assign('buyingForm', $buyingForm);
                            $this->view->assign('resource_exceeded', '');
                        } else {
                            $alert = $translator->translator('b2b_max_num_of_buying_reached', $privilege_info['package_name']);
                            $this->view->assign('resource_exceeded', $alert);
                        }
                    } else {
                        $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                        $this->view->assign('resource_exceeded', $alert);
                    }
                } else {
                    $alert = $translator->translator('b2b_company_profile_not_found');
                    $this->view->assign('resource_exceeded', $alert);
                }
            }
        }
    }

    public function editOLDAction() {
        $id = $this->_request->getParam('id');
        $buyingForm = new B2b_Form_BuyingForm();
        $buyingForm->loadB2bGroup();
        $buyingForm->loadAllCountries();
        $translator = Zend_Registry::get('translator');
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        // Checking if user have privilege to add more buying leads.
        $dataModel = new B2b_Model_DbTable_Buying();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($buyingForm->isValid($this->_request->getPost())) {
                    $buyingModel = new B2b_Model_Buying($buyingForm->getValues());
                    $dateTime = $buyingForm->getValue('validity');

                    if ($dateTime) {
                        $buyingModel->setValidity($dateTime);
                    }
                    $owner_id = $this->_request->getParam('user_id');
                    $privilege_info = null;

                    if ($owner_id) {
                        $privilege_info = $dataModel->getNumOfLeads($owner_id);
                    } else {
                        $privilege_info = $dataModel->getNumOfLeads($user_id);
                    }

                    $package_id = $privilege_info['package_id'];
                    $current_leads_num = $privilege_info['current_leads_num'];
                    $max_lead = $privilege_info['max_lead'];
                    $active = $privilege_info['active'];

                    // when a company profile is set as inactive
                    /* if (empty ($privilege_info['company_status'])  && !empty($privilege_info['package_id'])){
                      $msg =  $translator->translator('b2b_company_account_suspended_anonymous' , $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name']  . ' ) ' );
                      $json_arr = array('status' => 'err','msg' => $msg);
                      $res_value = Zend_Json::encode($json_arr);
                      $this->_response->setBody($res_value);
                      return;
                      } */
                    // Checking user permission to add products according to package.
                    // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                    // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                    // (int)$max_lead != 0 checks if user has unlimited access to add product.

                    if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {


                        if ($current_leads_num > $max_lead) {

                            $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_buying_reached_anonymous', $privilege_info['package_name']);
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                    }
                    if ($owner_id) {
                        $buyingModel->setUser_id($owner_id);
                        $buyingModel->setSeo_title($buyingForm->getValue('name'), $owner_id);
                    } else {
                        $buyingModel->setUser_id($user_id);
                        $buyingModel->setSeo_title($buyingForm->getValue('name'), $user_id);
                    }

                    if ($permission->allow()) {

                        // empty ($active) means if user be an admin or root
                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $buyingModel->setActive('1');
                        } else {
                            $buyingModel->setActive('0');
                        }


                        $result = $buyingModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_update_success");
                            $json_arr = array(
                                'status' => 'ok',
                                'msg' => $msg
                            );
                        } else {
                            $msg = $translator->translator("page_update_err");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg . " " . $result['msg']
                            );
                        }
                    } else {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $buyingForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array(
                                'key' => $key,
                                'errKey' => $errkey,
                                'value' => $value
                            );
                            $i ++;
                        }
                    }
                    $json_arr = array(
                        'status' => 'errV',
                        'msg' => $vMsg
                    );
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                if (!empty($id) && is_numeric($id)) {

                    $buyingTable = new B2b_Model_DbTable_Buying();
                    $memberList = new Members_Model_DbTable_MemberList();
                    $allMembers = $memberList->getMembers()->toArray();
                    $leadsDetails = $buyingTable->getLeadById($id);
                    $this->userUploaderSettings($leadsDetails);

                    $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;

                    if (is_array($leadsDetails) && (count($leadsDetails) > 1) && $isEditable) {
                        $buyingForm->populate($leadsDetails);
                        $price_currency = $leadsDetails['price_currency'];
                        $buyingForm->price_currency->setValue($price_currency);
                        $payment_type = explode(',', $leadsDetails['payment_type']);
                        $buyingForm->payment_type->setValue($payment_type);
                        $max_buying_unit = $leadsDetails['max_buying_unit'];
                        $buyingForm->max_buying_unit->setValue($max_buying_unit);
                        if (count($allMembers) && !empty($allMembers)) {
                            $userList = array();
                            $userList[''] = $translator->translator('b2b_select');
                            foreach ($allMembers as $row) {
                                $userList[$row['user_id']] = $row['title'] . '  ' . $row['firstName'] . '  ' . $row['lastName'] . '  ';
                            }
                            $buyingForm->user_id->setMultiOptions($userList);
                            $buyingForm->user_id->setMultiOptions($userList);
                            $validity = date("l, F d, Y h:i:s A", strtotime($leadsDetails['validity']));
                            $buyingForm->validity->setValue($validity);
                            if (!empty($leadsDetails['category'])) {
                                $category_db = new B2b_Model_DbTable_Category();
                                $category_info = $category_db->getCategoryById($leadsDetails['category']);
                                $this->view->assign('category_info', $category_info);
                            }
                        }
                    } else {
                        $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                    }
                } else {
                    $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                }

                $this->view->assign('buyingForm', $buyingForm);
            } else {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => 'Login Required'
                );
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function editAction() {
        $id = $this->_request->getParam('id');
        $buyingForm = new B2b_Form_BuyingForm($this->view->auth);
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more buying leads.
        $dataModel = new B2b_Model_DbTable_Buying();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to edit buying leads
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($buyingForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $buyingModel = new B2b_Model_Buying($buyingForm->getValues());
                        $owner_id = $buyingModel->getUser_id();
                        $id = $buyingModel->getId();

                        if (!empty($id) && is_numeric($id)) {
                            $leadsDetails = $dataModel->getLeadById($id);
                            if ($leadsDetails) {
                                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;
                                if ($isEditable === true) {
                                    //Check Owner
                                    if (!empty($owner_id)) {
                                        $company_status = $companyProfile->isActive($owner_id);
                                        $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                                        $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                                        $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;


                                        $privilege_info = $dataModel->getNumOfLeads($owner_id);
                                        $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                                        $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                        $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                        $max_lead = ($package_info) ? $package_info['buying'] : 0;
                                        $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                        // Checking user permission to edit buying leads according to package.
                                        if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                            // empty($package_id) check if user has unlimited access bypassing package limit.
                                            // empty($max_lead)checks if user has unlimited access to edit buying leads.
                                            // Checking user permission to edit buying leads according to package.
                                            // ($package_info && ($current_leads_num <= $max_lead))  checks if it exceeds package limit	
                                            if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_lead) && !empty($package_id) ) || ($package_info && ($current_leads_num <= $max_lead)))) {
                                                if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                    $buyingModel->setActive('1');
                                                } else {
                                                    $buyingModel->setActive('0');
                                                }
                                                $buyingModel->setSeo_title($buyingModel->getName(), $id);

                                                $result = $buyingModel->save();
                                                if ($result['status'] == 'ok') {
                                                    $msg = $translator->translator("page_save_success");
                                                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                                                } else {
                                                    $msg = $translator->translator("page_save_err");
                                                    $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                                }
                                            } else {
                                                $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $package_info['name']);
                                                $json_arr = array('status' => 'err', 'msg' => $msg);
                                            }
                                        } else {
                                            $msg = $translator->translator('b2b_membership_pkg_found_err');
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                                    }
                                } else {
                                    $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_permission_err'));
                                }
                            } else {
                                $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                        }
                    } else {
                        $validatorMsg = $buyingForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                if (!empty($id) && is_numeric($id)) {
                    $leadsDetails = $dataModel->getLeadById($id);
                    if ($leadsDetails) {
                        $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;
                        if ($isEditable === true) {
                            //Get Category Info
                            $categoryData = new B2b_Model_DbTable_Category();

                            if (empty($leadsDetails['category'])) {
                                $category_name = $translator->translator("common_tree_root");
                            } else {
                                $categoryInfo = $categoryData->getCategoryName($leadsDetails['category']);
                                $category_name = $categoryInfo['name'];
                            }

                            //ASSIGN TIME OF EXPIRY
                            $locale = Eicra_Global_Variable::getSession()->sess_lang;
//                            $leadsDetails['validity'] = date("l, F d, Y H:i:s A", strtotime($leadsDetails['validity']));
                            $date_obj = new Zend_Date($leadsDetails['validity'], null, $locale);
                            //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                            $leadsDetails['validity'] = $date_obj->get(Zend_Date::DATE_FULL . " " . Zend_Date::TIMES); //date("Y-m-d H:i:s", $time);



                            //ASSIGN TYPE OR STATUS
                            //$buyingForm->type->setIsArray(true);									
                            //$leadsDetails['type'] = explode(',', $leadsDetails['type']);											
                            //ASSIGN PAYMENT TYPE 
                            $buyingForm->payment_type->setIsArray(true);
                            $leadsDetails['payment_type'] = explode(',', $leadsDetails['payment_type']);

                            //ASSIGN GROUP
                            if ($auth->getIdentity()->access_other_user_article != '1') {
                                $multiOptions = $buyingForm->group_id->getMultiOptions();
                                $buyingForm->group_id->clearMultiOptions();
                                foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                    if ($leadsDetails['group_id'] == $multiOptionKey) {
                                        $buyingForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                    }
                                }
                            }

                            $buyingForm->populate($leadsDetails);
                            $this->userUploaderSettings($leadsDetails);

                            $this->view->assign('category_info', $categoryInfo);
                            $this->view->assign('category_name', $category_name);
                            $this->view->assign('buyingForm', $buyingForm);
                        } else {
                            $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                        }
                    } else {
                        $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                    }
                } else {
                    $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/' . $this->view->getModule . '/' . $this->view->getController . '/add', array());
                }
            }
        }
    }

    public function deleteAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $producMapper = new B2b_Model_BuyingMapper();

                //page_info
                $buyingTbl = new B2b_Model_DbTable_Buying();
                $lead_info = $buyingTbl->getLeadById($id);

                $product_images = explode(',', $lead_info['others_images']);
                $product_images_path = 'data/frontImages/b2b/buying_images';

                try {
                    $result = $producMapper->delete($id);
                    if ($result['status'] == 'ok') {


                        foreach ($product_images as $key => $file) {
                            if ($file) {
                                $dir = $product_images_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                            }
                            if ($res) {
                                $msg = $translator->translator('page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator('page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            }
                        }

                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg']
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage()
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        } else {
            $msg = $translator->translator('b2b_common_delete_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow()) {
                $buyingTbl = new B2b_Model_DbTable_Buying();
                $product_images_path = 'data/frontImages/b2b/buying_images';

                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $producMapper = new B2b_Model_BuyingMapper();
                            $lead_info = $buyingTbl->getLeadById($id);
                            $product_images = explode(',', $lead_info['others_images']);

                            $result = $producMapper->delete($id);
                            if ($result['status'] == 'ok') {

                                foreach ($product_images as $key => $file) {
                                    if ($file) {
                                        $dir = $product_images_path . DS . $file;
                                        $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                                    }
                                    if ($res) {
                                        $msg = $translator->translator('page_list_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    } else {
                                        $msg = $translator->translator('page_list_file_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    }
                                }

                                $msg = $translator->translator("b2b_common_delete_success");
                                $json_arr = array(
                                    'status' => 'ok',
                                    'msg' => $msg
                                );
                            } else {
                                $msg = $translator->translator("b2b_common_delete_failed");
                                $json_arr = array(
                                    'status' => 'err',
                                    'msg' => $msg . " " . $result['msg']
                                );
                            }
                        } catch (Exception $e) {
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $e->getMessage() . ' ' . $id
                            );
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $msg
                    );
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg
                );
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function listAction() {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        if ($this->_request->isPost()) {
            try {
                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                if ($displayble != null && $displayble != '') {
                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble',
                        'operator' => 'eq',
                        'value' => $displayble
                    );
                }
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRoute = 'adminrout';
                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                        ), $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    /*                     $buying_mapper = new B2b_Model_BuyingMapper();
                      $view_datas = $buying_mapper->fetchAll($pageNumber, $approve, $posted_data); */

                    $list_mapper = new B2b_Model_BuyingMapper();
                    $buyingSql = new B2b_Controller_Helper_BuyingSql();
                    $tableColumns = $buyingSql->getBackendList();
                    $view_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data, $tableColumns);

                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($view_datas) {
                    $key = 0;
                    foreach ($view_datas as $entry) {
                        $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                        //$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_page_name'] = str_replace('_', '-', $entry_arr['name']);

                        $img_thumb_arr = explode(',', $entry_arr['buying_images']);
                        $entry_arr['primary_file_field_format'] = ($this->view->escape($entry_arr['primary_file_field'])) ? 'data/frontImages/b2b/buying_images/' . $this->view->escape($entry_arr['primary_file_field']) : (($this->view->escape($entry_arr['others_images'])) ? 'data/frontImages/b2b/buying_images/' . $img_thumb_arr[0] : '');
                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $view_datas->getTotalItemCount();
                }
                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }

        $mem_db = new Members_Model_DbTable_MemberList();
        $this->view->assign('mem_data', $mem_db->getAllMembers());
    }

    public function memlistAction() {
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->_request->getParam('approve');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array(
                    'module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'list',
                    'approve' => $approve,
                    'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber
                        ), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($member_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $member = new B2b_Model_MemberListMapper();
                    $member_datas = $member->fetchAll($pageNumber, $approve, $posted_data);
                    $this->_controllerCache->save($member_datas, $uniq_id);
                }
                $data_result = array();
                $total = 0;
                if ($member_datas) {
                    $key = 0;
                    foreach ($member_datas as $entry) {
                        $entry_arr = $entry->toArray();
                        $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                        $entry_arr['publish_status_username'] = str_replace('_', '-', $entry_arr['username']);
                        $entry_arr['last_access_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['last_access']));
                        $entry_arr['register_date_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['register_date']));
                        $entry_arr['edit_enable'] = (($this->_auth_obj->access_other_user_profile == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) && ($entry_arr['role_lock'] == '1' || $this->_auth_obj->role_id == $entry_arr['role_id'])) ? true : false;
                        $data_result[$key] = $entry_arr;
                        $key ++;
                    }
                    $total = $member_datas->getTotalItemCount();
                }
                $json_arr = array(
                    'status' => 'ok',
                    'data_result' => $data_result,
                    'total' => $total,
                    'posted_data' => $posted_data
                );
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'data_result' => '',
                    'msg' => $e->getMessage()
                );
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        // Get Role List
        $roles = new Members_Model_DbTable_Role();
        $this->view->roleList = $roles->fetchAll();
    }

    public function upAction() {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id = $this->_request->getPost('id');
            $buying_order = $this->_request->getPost('buying_order');
            $table_name = 'b2b_buying_leads';
            $fields_arr = array(
                'id',
                'group_id',
                'user_id',
                'buying_order'
            );
            $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
            $returnV = $OrderObj->decreaseOrder();
            if ($returnV['status'] == 'err') {
                $json_arr = array(
                    'status' => 'err',
                    'id_arr' => $returnV['id_arr'],
                    'msg' => $returnV['msg']
                );
            } else {
                $json_arr = array(
                    'status' => 'ok',
                    'id_arr' => $returnV['id_arr'],
                    'msg' => $returnV['msg']
                );
            }
        } catch (Exception $e) {
            $json_arr = array(
                'status' => 'err',
                'msg' => $e->getMessage()
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction() {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id = $this->_request->getPost('id');
            $buying_order = $this->_request->getPost('buying_order');
            $table_name = 'b2b_buying_leads';
            $fields_arr = array(
                'id',
                'group_id',
                'user_id',
                'buying_order'
            );
            $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
            $returnV = $OrderObj->increaseOrder();
            if ($returnV == 'err') {
                $json_arr = array(
                    'status' => 'err',
                    'id_arr' => $returnV['id_arr'],
                    'msg' => $returnV['msg']
                );
            } else {
                $json_arr = array(
                    'status' => 'ok',
                    'id_arr' => $returnV['id_arr'],
                    'msg' => $returnV['msg']
                );
            }
        } catch (Exception $e) {
            $json_arr = array(
                'status' => 'err',
                'msg' => $e->getMessage()
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            try {
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
                $table_name = 'b2b_buying_leads';
                $fields_arr = array(
                    'id',
                    'group_id',
                    'user_id',
                    'buying_order'
                );
                $id_str = $this->_request->getPost('id_arr');
                $buying_order_str = $this->_request->getPost('buying_order_arr');
                $id_arr = explode(',', $id_str);
                $buying_order_arr = explode(',', $buying_order_str);
                $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
                $checkOrder = $order_numbers->checkOrderNumbers($buying_order_arr);
                if ($checkOrder['status'] == 'err') {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $checkOrder['msg']
                    );
                } else {
                    // Save Category Order
                    $msg = $translator->translator("buying_order_save_success");
                    $json_arr = array(
                        'status' => 'ok',
                        'msg' => $msg
                    );
                    $i = 0;
                    foreach ($id_arr as $id) {
                        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
                        $result = $OrderObj->saveOrder($buying_order_arr[$i]);
                        if ($result['status'] == 'err') {
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $result['msg']
                            );
                            break;
                        }
                        $i ++;
                    }
                }
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage()
                );
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function activeAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $productName = $this->_request->getPost('name');
            $paction = $this->_request->getPost('paction');
            $data = array();
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            try {
                $data = array(
                    'active' => $active
                );
                $buyingMapper = new B2b_Model_BuyingMapper();
                $json_arr = $buyingMapper->updateStatus($data, $id);
                $json_arr['active'] = $active;
            } catch (Exception $e) {
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $e->getMessage(),
                    'active' => $active
                );
            }
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function activateallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }
            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);
                $data = array(
                    'active' => $active
                );
                foreach ($id_arr as $id) {
                    try {
                        $buyingMapper = new B2b_Model_BuyingMapper();
                        $json_arr = $buyingMapper->updateStatus($data, $id);
                        $json_arr['active'] = $active;
                    } catch (Exception $e) {
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $e->getMessage(),
                            'active' => $active
                        );
                    }
                }
            }
        } else {
            $msg = $translator->translator("page_selected_err");
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function displayableAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                // $product_name = $this->_request->getPost('product_name');
                $displayable = $this->_request->getPost('paction');
                $buyingMapper = new B2b_Model_BuyingMapper();
                try {
                    $result = $buyingMapper->displayable($id, $displayable);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'displayable' => $displayable
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'displayable' => $displayable
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'displayable' => $displayable
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'displayable' => $displayable
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function featuredAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;

        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()) {

                $id = $this->_request->getPost('id');
                $product_name = $this->_request->getPost('product_name');
                $featured = $this->_request->getPost('paction');
                $buyingMapper = new B2b_Model_BuyingMapper();

                try {
                    $result = $buyingMapper->featured($id, $featured);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'featured' => $featured);
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'featured' => $featured);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'featured' => $featured);
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg, 'featured' => $featured);
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function userUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;
        /**
         * ************** For Primary **************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_buying_images';
        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

}
