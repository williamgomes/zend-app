<?php

class B2b_FrontendController extends Zend_Controller_Action {

    private $_page_id;
    private $_translator;
    private $_controllerCache;
    private $_modules_license;
    private $_ckLicense = true;
    private $_snopphing_cart;
    private $_category;
    private $_auth_obj;
    private $_menu_obj;
    private $_company_db;
    private $_buying_db;
    private $_selling_db;
    private $_product_db;
    private $_arrayCompany;
    private $_arrayBuyingLeads;
    private $_arraySellingLeads;
    private $_arrayProducts;
    private $_sorteCompanydArr;
    private $_sortedBuyingArr;
    private $_sortedSellingArr;
    private $_sortedProductArr;
    private $_sortedCategoryArr;
    private $_menu;
    

    public function init() {
        /* Initialize action controller here */
        $translator = Zend_Registry::get('translator');
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        
        $this->_company_db = new B2b_Model_DbTable_CompanyProfile();
        $this->_buying_db = new B2b_Model_DbTable_Buying();
        $this->_selling_db = new B2b_Model_DbTable_Selling();
        $this->_product_db = new B2b_Model_DbTable_Products();
        
        
        $tableColumns = array();
        $tableColumns['userChecking'] = false;
        $this->_arrayCompany = $this->_company_db->getListInfo('1', null, $tableColumns);
        $search_params_buying['filter']['filters'][] = array('field' => 'displayble', 'operator' => 'eq', 'value' => '1');
        $this->_arrayBuyingLeads = $this->_buying_db->getListInfo('1', $search_params_buying, null);
        $this->_arraySellingLeads = $this->_selling_db->getListInfo('1', $search_params_buying, null);
        $this->_arrayProducts = $this->_product_db->getListInfo('1', $search_params_buying, null);
        
        $this->_menu = new B2b_Model_DbTable_Category();
        $this->_menu_obj = $this->_menu->getListInfo('1', $search_params, false);
        $companyInformation = (!is_array($this->_arrayCompany)) ? $this->_arrayCompany->toArray() : $this->_arrayCompany;
        $buyingLeadsInfo = (!is_array($this->_arrayBuyingLeads)) ? $this->_arrayBuyingLeads->toArray() : $this->_arrayBuyingLeads;
        $sellingLeadsInfo = (!is_array($this->_arraySellingLeads)) ? $this->_arraySellingLeads->toArray() : $this->_arraySellingLeads;
        $productsInfo = (!is_array($this->_arrayProducts)) ? $this->_arrayProducts->toArray() : $this->_arrayProducts;
        
        
        $this->_sorteCompanydArr = array();
        for($x = 0; $x < count($companyInformation); $x++){
            if($companyInformation[$x]['user_status'] == 1){
//                if($auth->hasIdentity() && )
                $this->_sorteCompanydArr[$x]['category_id'] = $companyInformation[$x]['category_id'];
                $this->_sorteCompanydArr[$x]['id'] = $companyInformation[$x]['id'];
                $this->_sorteCompanydArr[$x]['name'] = $companyInformation[$x]['company_name'];
            }
        }
        
        $this->_sortedBuyingArr = array();
        for($x = 0; $x < count($buyingLeadsInfo); $x++){
            $this->_sortedBuyingArr[$x]['category_id'] = $buyingLeadsInfo[$x]['category'];
            $this->_sortedBuyingArr[$x]['id'] = $buyingLeadsInfo[$x]['id'];
            $this->_sortedBuyingArr[$x]['name'] = $buyingLeadsInfo[$x]['name'];
        }
        
        $this->_sortedSellingArr = array();
        for($x = 0; $x < count($sellingLeadsInfo); $x++){
            $this->_sortedSellingArr[$x]['category_id'] = $sellingLeadsInfo[$x]['category'];
            $this->_sortedSellingArr[$x]['id'] = $sellingLeadsInfo[$x]['id'];
            $this->_sortedSellingArr[$x]['name'] = $sellingLeadsInfo[$x]['name'];
        }
        
        
        $this->_sortedProductArr = array();
        for($x = 0; $x < count($productsInfo); $x++){
            $this->_sortedProductArr[$x]['category_id'] = $productsInfo[$x]['category_id'];
            $this->_sortedProductArr[$x]['id'] = $productsInfo[$x]['id'];
            $this->_sortedProductArr[$x]['name'] = $productsInfo[$x]['name'];
        }

        $menuArr = (!is_array($this->_menu_obj)) ? $this->_menu_obj->toArray() : $this->_menu_obj;

        $this->_sortedCategoryArr = array();
        for($y = 0; $y < count($menuArr); $y++){
            $this->_sortedCategoryArr[$y] = array("id" => $menuArr[$y]['id'], "parent_id" => $menuArr[$y]['parent_id']);
        }
        

        /* Check Module License */
        //$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
        if ($this->_ckLicense == true) {

            $license = new Zend_Session_Namespace('License');
            if (!$license || !$license->license_data || !$license->license_data['modules']) {
                $curlObj = new Eicra_License_Version();
                $curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
                $license->license_data = $curlObj->getArrayResult();
            }
        }
        $this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
        $this->_snopphing_cart = ($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;
    }

    public function preDispatch() {
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;


        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }

        if ($this->_request->getParam('category')) {
            try {
                $category_url = $this->_request->getParam('category');
                $category_db = new B2b_Model_DbTable_Category();
                $category_info = $category_db->getCategoryInfoByUrl($category_url);
                if ($category_info) {
                    $this->_category = $category_info['id'];
                } else {
                    $this->_category = 0;
                }
            } catch (Exception $e) {
                $this->_category = 0;
            }
        } else {
            $this->_category = 0;
        }
    }
    
    

    private function breadcrumbArray($breadcrumb_arr, $category_db, $parent_id, $route = 'All-B2b-Category-list/*') {
        $parent_info = $category_db->getCategoryName($parent_id);
        if ($parent_info) {
            if (!empty($parent_info['parent_id'])) {
                $breadcrumb_arr = $this->breadcrumbArray($breadcrumb_arr, $category_db, $parent_info['parent_id'], $route);
            }
            $breadcrumb_arr[] = array($this->view->escape($parent_info['name']),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'category' => $parent_info['url_portion']), $route, true));
        }

        return $breadcrumb_arr;
    }
    
    
    
    function checkCompanyCount($arrCompany, $catID, $allCompID) {
        foreach ($arrCompany AS $company) {
            if ($company['category_id'] == $catID) {
                $allCompID[] = $company['id'];
            }
        }
        return $allCompID;
    }

    function getAllDeepChild($arrCategory, $parentID, $valReturn) {

//    foreach($arrCategory AS $Category){
//        if($Category['parent_id'] == $parentID){
//            $valReturn[] = $Category['id'];
//            $valReturn = getAllDeepChild($arrCategory, $Category['id'],$valReturn);
//        }
//    }

        for ($i = 0; $i < count($arrCategory); $i++) {
            if ($arrCategory[$i]['parent_id'] == $parentID) {
                $valReturn[] = $arrCategory[$i]['id'];
                $valReturn = $this->getAllDeepChild($arrCategory, $arrCategory[$i]['id'], $valReturn);
            }
        }
        return $valReturn;
    }

    function getAllChildArray($arrCategory, $parentID, $CompanyArr) {
        $arrAllCat = array();
        $arrAllCat[] = $parentID;
        $allCompID = array();

//    foreach($arrCategory AS $Category){
//        if($Category['parent_id'] == $parentID){
//            $arrAllCat[] = $Category['id'];
//            $arrAllCat = getAllDeepChild($arrCategory, $Category['id'],$arrAllCat);
//        }
//    }
        for ($i = 0; $i < count($arrCategory); $i++) {
            if ($arrCategory[$i]['parent_id'] == $parentID) {
                $arrAllCat[] = $arrCategory[$i]['id'];
                $arrAllCat = $this->getAllDeepChild($arrCategory, $arrCategory[$i]['id'], $arrAllCat);
            }
        }
        foreach ($arrAllCat AS $Key => $Val) {
            $allCompID = $this->checkCompanyCount($CompanyArr, $Val, $allCompID);
        }

        return $allCompID;
    }


   
    
    public function categorylistAction() {
        try {
            
            $parent_id = ($this->_category) ? $this->_category : 0;
            $this->view->category_db = new B2b_Model_DbTable_Category();
            $mapper = new B2b_Model_CategoryMapper();
            
            //assigning initial variables 
            $this->view->arrCompany = $this->_sorteCompanydArr;
            $this->view->arrCategory = $this->_sortedCategoryArr;

            if (!empty($parent_id)) {
                $search_params_parent['filter']['filters'][0] = array(
                    'field' => 'id', 'operator' => 'eq',
                    'value' => $parent_id);
                $search_params_parent['filter']['logic'] = ($search_params_parent['filter']['logic']) ? $search_params_parent['filter']['logic'] : 'and';
                $search_params_parent['filter']['filters'][1] = array(
                    'field' => 'displayble', 'operator' => 'eq',
                    'value' => '1');

                $parent_category_info = $this->view->category_db->getListInfo(
                        '1', $search_params_parent, false);
                $parent_category_info = ($parent_category_info &&
                        !is_array($parent_category_info)) ? $parent_category_info->toArray() : $parent_category_info;
                $this->view->parent_category_info = ($parent_category_info &&
                        $parent_category_info[0]) ? $parent_category_info[0] : null;
            }

            if (($parent_category_info && $parent_category_info[0]) ||
                    empty($parent_id)) {
                $preferences_db = new B2b_Model_DbTable_Preferences();
                $preferences_data = $preferences_db->getOptions();

                $search_params['filter']['filters'][0] = array(
                    'field' => 'parent_id', 'operator' => 'eq',
                    'value' => $parent_id);
                $search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';
                $search_params['filter']['filters'][1] = array(
                    'field' => 'displayble', 'operator' => 'eq',
                    'value' => '1');

                if ($preferences_data &&
                        $preferences_data['category_on_link_page_sortby'] &&
                        $preferences_data['category_on_link_page_sortby'] != '_') {
                    $sort_arr = explode('-', $preferences_data['category_on_link_page_sortby']);
                    $search_params['sort'][0] = array('field' => $sort_arr[0],
                        'dir' => $sort_arr[1]);
                }

                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');

                Eicra_Global_Variable::getSession()->viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : ((empty(
                                $preferences_data['category_on_link_page'])) ? 99999999999999999999999 : $preferences_data['category_on_link_page']);
                $this->view->category_info = $mapper->fetchAll($pageNumber, '1', $search_params, false);
                
                
                $dbCategory = new B2b_Model_DbTable_Category();
                $arrCategory = $dbCategory->getListInfo('1', NULL, false);
                $this->view->arrAllCategory = $arrCategory;
                
                // BreadCrumb
                if ($this->_request->getParam('menu_id')) {
                    Eicra_Global_Variable::getSession()->breadcrumb = array(
                        array(
                            str_replace('-', ' ', $this->_request->getParam('menu_id')),
                            $this->view->url()),
                        array(
                            $this->_translator->translator(
                                    'b2b_category_list_all_title', '', 'B2b'),
                            $this->view->url(
                                    array(
                                'module' => $this->view->getModule,
                                'controller' => $this->view->getController,
                                'action' => $this->view->getAction), 'All-B2b-Category-list/*', true)));
                } else {
                    if (!empty($parent_id) && $parent_category_info) {
                        $breadcrumb_arr = array(
                            array(
                                $this->_translator->translator(
                                        'b2b_category_list_all_title', '', 'B2b'),
                                $this->view->url(
                                        array(
                                    'module' => $this->view->getModule,
                                    'controller' => $this->view->getController,
                                    'action' => $this->view->getAction), 'All-B2b-Category-list/*', true)));
                        $breadcrumb_arr = $this->breadcrumbArray(
                                $breadcrumb_arr, $this->view->category_db, $parent_category_info[0]['parent_id']);
                        $breadcrumb_arr[] = array(
                            $this->view->escape(
                                    $parent_category_info[0]['name']) . ' ' . $this->_translator->translator(
                                    'b2b_category_list_result_title', $this->view->numbers(
                                            number_format(
                                                    $parent_category_info[0]['company_num'])), 'B2b'),
                            $this->view->url(
                                    array(
                                'module' => $this->view->getModule,
                                'controller' => $this->view->getController,
                                'action' => $this->view->getAction,
                                'category' => $parent_category_info[0]['url_portion']), 'All-B2b-Category-list/*', true));

                        Eicra_Global_Variable::getSession()->breadcrumb = $breadcrumb_arr;
                    } else {
                        Eicra_Global_Variable::getSession()->breadcrumb = array(
                            array(
                                $this->_translator->translator(
                                        'b2b_category_list_all_title', '', 'B2b'),
                                $this->view->url(
                                        array(
                                    'module' => $this->view->getModule,
                                    'controller' => $this->view->getController,
                                    'action' => $this->view->getAction), 'All-B2b-Category-list/*', true)));
                    }
                }
            } else {
                Eicra_Global_Variable::getSession()->breadcrumb = '';
                $this->view->category_info = null;
                $this->view->err_msg = $this->_translator->translator(
                        'b2b_category_not_fount');
            }
        } catch (Exception $e) {
            Eicra_Global_Variable::getSession()->breadcrumb = '';
            $this->view->category_info = null;
            $this->view->err_msg = $e->getMessage();
        }
    }

    public function productslistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('b2b_front_product_list', '', 'B2b'), $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_front_product_list_active', '', 'B2b'),
                $this->view->url()));
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-b2b-Product-list/*';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'group_id' => $this->_request->getParam(
                            'group_id'),
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $global_conf = Zend_Registry::get('global_conf');
                    $currency_converter_helper = new Paymentgateway_Controller_Helper_Currency($global_conf);
                    $today = date('Y-m-d');
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_products', false);
                    $review_helper = new Review_View_Helper_Review();
                    $list_mapper = new B2b_Model_ProductMapper();
                    $productSql = new B2b_Controller_Helper_ProductSql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $productSql->$posted_data['sqlFunction']() : $productSql->getFontendList();
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $entry_arr['id_format'] = $this->view->numbers(
                                    $entry_arr['id']);
                            $entry_arr['business_type'] = (!empty(
                                            $business_type) && is_array($business_type)) ? implode(
                                            ', ', $business_type) : '0';
                            $img_thumb_arr = explode(',', $entry_arr['product_images']);
                            $others_images_no = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                            $entry_arr['product_images_primary'])) ? $entry_arr['images_path'] .
                                    '/' .
                                    $this->view->escape(
                                            $entry_arr['product_images_primary']) : $entry_arr['images_path'] .
                                    '/' . $img_thumb_arr[0];
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['product_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $entry_arr['product_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($others_images_no));

                            $entry_arr['original_price'] = $entry_arr['price'];
                            $entry_arr['price'] = $currency_converter_helper->convert($entry_arr['price_currency_locale'], $entry_arr['original_price']);
                            $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($entry_arr['eCommerce'] == 'YES') && ($this->_snopphing_cart == true) && ($currency_converter_helper->checkCurrency($entry_arr['price_currency_locale']) == true)) ? true : false;
                            $entry_arr['table_name'] = 'b2b_products';

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function invalidrequestAction() {
        // Showing 404 company not found link.
        $comopany_title = $this->_request->getParams();
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        implode("/", $comopany_title), '', 'B2b'),
                $this->view->url()));
    }

    public function buylistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_front_buying_offer_list', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_front_buying_offer_show_list', '', 'B2b'),
                $this->view->url()));

        // Injecting Package Information to the view model.
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-Buying-List/*';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'group_id' => $this->_request->getParam(
                            'group_id'),
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $global_conf = Zend_Registry::get('global_conf');
                    $today = date('Y-m-d');
                    $review_helper = new Review_View_Helper_Review();
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_buying_leads', false);

                    $list_mapper = new B2b_Model_BuyingMapper();
                    $buyingSql = new B2b_Controller_Helper_BuyingSql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $buyingSql->$posted_data['sqlFunction']() : $buyingSql->getFontendList();
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr['id_format'] = $this->view->numbers(
                                    $entry_arr['id']);
                            $entry_arr['business_type'] = (count(
                                            $entry_arr['business_type']) > 0 &&
                                    is_array($entry_arr['business_type'])) ? implode(
                                            ', ', $entry_arr['business_type']) : '';
                            $img_thumb_arr = explode(',', $entry_arr['others_images']);
                            $others_images_no = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                            $entry_arr['primary_file_field'])) ? $entry_arr['images_path'] .
                                    '/' .
                                    $this->view->escape(
                                            $entry_arr['primary_file_field']) : $entry_arr['images_path'] .
                                    '/' . $img_thumb_arr[0];
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['buying_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $entry_arr['others_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($others_images_no));

                            $entry_arr['original_price'] = $entry_arr['price'];
                            $entry_arr['price'] = $entry_arr['price'];
                            $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($entry_arr['eCommerce'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                            $entry_arr['table_name'] = 'b2b_buying_leads';

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function sellistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_front_selling_offer_list', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_front_selling_offer_show_list', '', 'B2b'),
                $this->view->url()));

        // Injecting Package Information to the view model.
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-Selling-list/*';
                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'group_id' => $this->_request->getParam(
                            'group_id'),
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $global_conf = Zend_Registry::get('global_conf');
                    $currency_converter_helper = new Paymentgateway_Controller_Helper_Currency($global_conf);
                    $today = date('Y-m-d');
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_selling_leads', false);
                    $review_helper = new Review_View_Helper_Review();

                    $list_mapper = new B2b_Model_SellingMapper();
                    $sellingSql = new B2b_Controller_Helper_SellingSql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $sellingSql->$posted_data['sqlFunction']() : $sellingSql->getFontendList();

                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr['id_format'] = $this->view->numbers(
                                    $entry_arr['id']);
                            $entry_arr['business_type'] = (count(
                                            $entry_arr['business_type']) > 0 &&
                                    is_array($entry_arr['business_type'])) ? implode(
                                            ', ', $entry_arr['business_type']) : '';
                            $img_thumb_arr = explode(',', $entry_arr['others_images']);
                            $others_images_no = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                            $entry_arr['primary_file_field'])) ? $entry_arr['images_path'] .
                                    '/' .
                                    $this->view->escape(
                                            $entry_arr['primary_file_field']) : $entry_arr['images_path'] .
                                    '/' . $img_thumb_arr[0];
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['selling_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $entry_arr['others_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($others_images_no));

                            $entry_arr['original_price'] = $entry_arr['price'];
                            $entry_arr['price'] = $currency_converter_helper->convert($entry_arr['price_currency_locale'], $entry_arr['original_price']);
                            $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($entry_arr['eCommerce'] == 'YES') && ($this->_snopphing_cart == true) && ($currency_converter_helper->checkCurrency($entry_arr['price_currency_locale']) == true)) ? true : false;
                            $entry_arr['table_name'] = 'b2b_selling_leads';

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function companylistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_front_company_list_active', '', 'B2b'),
                $this->view->url()));
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $company_name = $this->_request->getParam('company_name');

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());




        if (!empty($company_name)) {
            $posted_data['filter']['filters'][] = array(
                'field' => 'company_name', 'operator' => 'startswith',
                'value' => $company_name);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
        }     
        
        

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                
                
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-Company-List/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'company_name' => $company_name,
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_company_profile', false);
                    $review_helper = new Review_View_Helper_Review();
                    $list_mapper = new B2b_Model_CompanyprofileMapper();
                    $companySql = new B2b_Controller_Helper_CompanySql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $companySql->$posted_data['sqlFunction']() : $companySql->getFontendCompanyList();
                   
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $compliance_name = (!empty(
                                            $entry_arr['compliance_name']) &&
                                    is_array($entry_arr['compliance_name'])) ? array_unique(
                                            $entry_arr['compliance_name']) : '0';
                            $compliance_img = (!empty(
                                            $entry_arr['compliance_img']) &&
                                    is_array($entry_arr['compliance_img'])) ? array_unique(
                                            $entry_arr['compliance_img']) : '0';
                            $img_thumb_arr = explode(',', $entry_arr['profile_image']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['profile_image_primary_format'] = ($this->view->escape(
                                            $entry_arr['profile_image_primary'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['profile_image_primary'] : 'data/frontImages/hotels/hotels_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['profile_image_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape(
                                            $entry_arr['company_name']));
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $business_type = (count($entry_arr['business_type']) >
                                    0 && is_array($entry_arr['business_type'])) ? array_unique(
                                            $entry_arr['business_type']) : '';
                            $entry_arr['business_type'] = (is_array(
                                            $business_type)) ? implode(', ', $business_type) : '0';
                            $entry_arr['compliance_name'] = (is_array(
                                            $compliance_name)) ? implode(', ', $compliance_name) : $compliance_name;
                            $entry_arr['compliance_img'] = (is_array(
                                            $compliance_img)) ? implode(', ', $compliance_img) : $compliance_img;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function companydetailsAction() {
        $comopany_title = $this->_request->getParam('comopany_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyDetails = $companyProfile->getCompanyDetails($comopany_title, true);
        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function storylistAction() {
        /*
         * $storiesMapper = new B2b_Model_SuccessStoriesMapper(); $allStories =
         * $storiesMapper->getStories(); $this->view->assign('StoryArray',
         * $allStories); return;
         */
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_success_stories_list', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'menu_b2b_success_stories', '', 'B2b'),
                $this->view->url()));
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'Success-Stories/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                // $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_company_profile', false);
                    $review_helper = new Review_View_Helper_Review();
                    $successStoriesSql = new B2b_Controller_Helper_SuccessStoriesSql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $successStoriesSql->$posted_data['sqlFunction']() : $successStoriesSql->getFontendList();
                    $list_mapper = new B2b_Model_SuccessStoriesMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr['profile_image_primary_format'] = ($this->view->escape(
                                            $entry_arr['b2b_images'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['b2b_images'] : '0';
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function storydetailsAction() {
        $story_title = $this->_request->getParam('story_title');
        $storiesMapper = new B2b_Model_SuccessStoriesMapper();
        $fullStories = $storiesMapper->getStoryByTitle($story_title);
        $this->view->assign('fullStory', $fullStories);
    }

    public function tradeshowslistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('front_tradeshow_list', '', 'B2b'), $this->view->url()),
            array(
                $this->_translator->translator('b2b_tradeshow', '', 'B2b'), $this->view->url()));

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'Trade-Shows/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_company_profile', false);
                    $review_helper = new Review_View_Helper_Review();
                    $tradeShowSql = new B2b_Controller_Helper_TradeShowSql();
                    $tableColumns = ($posted_data['sqlFunction']) ? $tradeShowSql->$posted_data['sqlFunction']() : $tradeShowSql->getFontendList();
                    $list_mapper = new B2b_Model_TradeshowMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr['description'] = nl2br($entry_arr['description']);
                            $entry_arr['profile_image_primary_format'] = ($this->view->escape($entry_arr['b2b_images'])) ? $entry_arr['images_path'] . '/' . $entry_arr['b2b_images'] : '0';
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function tradeshowsdetailsAction() {
        $title = $this->_request->getParam('seo_title');
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('b2b_tradeshow_details', '', 'B2b'), $this->view->url()),
            array($title, $this->view->url()));
        $show_title = trim($title);
        $storiesMapper = new B2b_Model_TradeshowMapper();
        $tradeshow = $storiesMapper->getShowsByTitle($show_title);
        $this->view->assign('tradeshow', $tradeshow[0]);
    }

    public function membershippkgsAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'front_membership_package', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_front_packages_register', '', 'B2b'),
                $this->view->url()));
        $packageMapper = new B2b_Model_PackageMapper();
        $packages = $packageMapper->getAllPackages(true);
        $this->view->assign('packagesArray', $packages);
        if ($this->view->auth->hasIdentity()) {
            $package_id = $this->view->auth->getIdentity()->package_id;
            if (!empty($package_id)) {
                $this->view->owner_package_info = $packageMapper->getDbTable()->getPackageById($package_id);
            } else {
                $this->view->owner_package_info = null;
            }
        } else {
            $this->view->owner_package_info = null;
        }
    }

    public function inquiryAction() {
        $json_arr = array('status' => 'err', 'msg' => '======================');
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
        // $this->view->assign('sellingForm', $sellingForm);
    }

    public function contactAction() {
        
    }

    public function classifiedsAction() {
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $posted_data = $this->_request->getParams();
        $posted_data['category_id'] = (!empty($this->_category)) ? $this->_category : $posted_data['category_id'];
        

        $parent_id = ($posted_data['category_id']) ? $posted_data['category_id'] : 0;
        $category_db = new B2b_Model_DbTable_Category();

        if (!empty($parent_id)) {
            $search_params_parent['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq', 'value' => $parent_id);
            $search_params_parent['filter']['logic'] = ($search_params_parent['filter']['logic']) ? $search_params_parent['filter']['logic'] : 'and';
            $search_params_parent['filter']['filters'][1] = array(
                'field' => 'displayble', 'operator' => 'eq', 'value' => '1');

            $parent_category_info = $category_db->getListInfo('1', $search_params_parent, false);
            $parent_category_info = ($parent_category_info &&
                    !is_array($parent_category_info)) ? $parent_category_info->toArray() : $parent_category_info;
            $this->view->parent_category_info = ($parent_category_info &&
                    $parent_category_info[0]) ? $parent_category_info[0] : null;
            if ($this->view->parent_category_info['login'] == '1' && !$this->view->auth->hasIdentity()) {
                $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login', array());
            }
           
            $arrOfCompany = $this->getAllChildArray($this->_sortedCategoryArr, $parent_id, $this->_sorteCompanydArr);
            $companyArray = array_unique($arrOfCompany);
            $companyString = implode(',', $companyArray);
            $posted_data['company_id_in'] = $companyString;
            
            
            $arrOfBuyingLead = $this->getAllChildArray($this->_sortedCategoryArr, $parent_id, $this->_sortedBuyingArr);
            $buyingArray = array_unique($arrOfBuyingLead);
            $byuingString = implode(',', $buyingArray);
            $posted_data['buying_id_in'] = $byuingString;
            
            
            $arrOfSellingLead = $this->getAllChildArray($this->_sortedCategoryArr, $parent_id, $this->_sortedSellingArr);
            $sellingArray = array_unique($arrOfSellingLead);
            $sellingString = implode(',', $sellingArray);
            $posted_data['selling_id_in'] = $sellingString;
            
            
            $arrOfProducts = $this->getAllChildArray($this->_sortedCategoryArr, $parent_id, $this->_sortedProductArr);
            $productsArray = array_unique($arrOfProducts);
            $productString = implode(',', $productsArray);
            $posted_data['product_id_in'] = $productString;
        }
        $this->view->assign('posted_data', $posted_data);
        // BreadCrumb
        if ($this->_request->getParam('menu_id')) {
            Eicra_Global_Variable::getSession()->breadcrumb = array(
                array(
                    str_replace('-', ' ', $this->_request->getParam('menu_id')),
                    $this->view->url()),
                array(
                    $this->_translator->translator(
                            'b2b_category_list_all_title', '', 'B2b'),
                    $this->view->url(
                            array('module' => $this->view->getModule,
                        'controller' => $this->view->getController,
                        'action' => $this->view->getAction), 'All-Classified-list/*', true)));
        } else {
            if (!empty($parent_id) && $parent_category_info) {
                $breadcrumb_arr = array(
                    array(
                        $this->_translator->translator(
                                'b2b_category_list_all_title', '', 'B2b'),
                        $this->view->url(
                                array(
                            'module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction), 'All-Classified-list/*', true)));
                $breadcrumb_arr = $this->breadcrumbArray($breadcrumb_arr, $category_db, $parent_category_info[0]['parent_id'], 'All-Classified-list/*');
                $breadcrumb_arr[] = array(
                    $this->view->escape($parent_category_info[0]['name']) .
                    ' ' . $this->_translator->translator(
                            'b2b_category_list_result_title', $this->view->numbers(
                                    number_format(
                                            count($companyArray))), 'B2b'),
                    $this->view->url(
                            array(
                        'module' => $this->view->getModule,
                        'controller' => $this->view->getController,
                        'action' => $this->view->getAction,
                        'category' => $parent_category_info[0]['url_portion']), 'All-Classified-list/*', true));

                Eicra_Global_Variable::getSession()->breadcrumb = $breadcrumb_arr;
            } else {
                Eicra_Global_Variable::getSession()->breadcrumb = array(
                    array(
                        $this->_translator->translator(
                                'b2b_category_list_all_title', '', 'B2b'),
                        $this->view->url(
                                array(
                            'module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction), 'All-Classified-list/*', true)));
            }
        }
    }

    public function suppliersAction() {

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'permisson_b2b_front_supplierslist', '', 'B2b'),
                $this->view->url()));

        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-Company-List/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_company_profile', false);
                    $review_helper = new Review_View_Helper_Review();
                    $list_mapper = new B2b_Model_CompanyprofileMapper();
                    $companySql = new B2b_Controller_Helper_CompanySql();
                    $tableColumns = $companySql->getFontendsuppliersList();
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $compliance_name = (!empty(
                                            $entry_arr['compliance_name']) &&
                                    is_array($entry_arr['compliance_name'])) ? array_unique(
                                            $entry_arr['compliance_name']) : '0';
                            $compliance_img = (!empty(
                                            $entry_arr['compliance_img']) &&
                                    is_array($entry_arr['compliance_img'])) ? array_unique(
                                            $entry_arr['compliance_img']) : '0';
                            $img_thumb_arr = explode(',', $entry_arr['profile_image']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['profile_image_primary_format'] = ($this->view->escape(
                                            $entry_arr['profile_image_primary'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['profile_image_primary'] : 'data/frontImages/b2b/b2b_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['profile_image_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape(
                                            $entry_arr['company_name']));
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $business_type = (count($entry_arr['business_type']) >
                                    0 && is_array($entry_arr['business_type'])) ? array_unique(
                                            $entry_arr['business_type']) : '';
                            $entry_arr['business_type'] = (is_array(
                                            $business_type)) ? implode(', ', $business_type) : '0';
                            $entry_arr['compliance_name'] = (is_array(
                                            $compliance_name)) ? implode(', ', $compliance_name) : $compliance_name;

                            $entry_arr['compliance_img'] = (is_array(
                                            $compliance_img)) ? implode(', ', $compliance_img) : $compliance_img;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function buyerlistAction() {
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'permisson_b2b_front_buyerlist', '', 'B2b'),
                $this->view->url()));

        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);
            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }
        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');
                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'All-Company-List/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {
                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_company_profile', false);
                    $review_helper = new Review_View_Helper_Review();
                    $list_mapper = new B2b_Model_CompanyprofileMapper();

                    $companySql = new B2b_Controller_Helper_CompanySql();
                    $tableColumns = $companySql->getFontendBuyerList();

                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $compliance_name = (!empty(
                                            $entry_arr['compliance_name']) &&
                                    is_array($entry_arr['compliance_name'])) ? array_unique(
                                            $entry_arr['compliance_name']) : '0';
                            $compliance_img = (!empty(
                                            $entry_arr['compliance_img']) &&
                                    is_array($entry_arr['compliance_img'])) ? array_unique(
                                            $entry_arr['compliance_img']) : '0';
                            $img_thumb_arr = explode(',', $entry_arr['profile_image']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['profile_image_primary_format'] = ($this->view->escape(
                                            $entry_arr['profile_image_primary'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['profile_image_primary'] : 'data/frontImages/hotels/hotels_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['profile_image_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));
                            $entry_arr['profile_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape(
                                            $entry_arr['company_name']));
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $business_type = (count($entry_arr['business_type']) >
                                    0 && is_array($entry_arr['business_type'])) ? array_unique(
                                            $entry_arr['business_type']) : '';
                            $entry_arr['business_type'] = (is_array(
                                            $business_type)) ? implode(', ', $business_type) : '0';
                            $entry_arr['compliance_name'] = (is_array(
                                            $compliance_name)) ? implode(', ', $compliance_name) : $compliance_name;

                            $entry_arr['compliance_img'] = (is_array(
                                            $compliance_img)) ? implode(', ', $compliance_img) : $compliance_img;

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function searchAction() {
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $posted_data = $this->_request->getParams();
        $posted_data['category_id'] = (!empty($this->_category)) ? $this->_category : $posted_data['category_id'];
        $this->view->assign('posted_data', $posted_data);

        $parent_id = ($posted_data['category_id']) ? $posted_data['category_id'] : 0;
        $category_db = new B2b_Model_DbTable_Category();

        if (!empty($parent_id)) {
            $search_params_parent['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq', 'value' => $parent_id);
            $search_params_parent['filter']['logic'] = ($search_params_parent['filter']['logic']) ? $search_params_parent['filter']['logic'] : 'and';
            $search_params_parent['filter']['filters'][1] = array(
                'field' => 'displayble', 'operator' => 'eq', 'value' => '1');

            $parent_category_info = $category_db->getListInfo('1', $search_params_parent, false);
            $parent_category_info = ($parent_category_info &&
                    !is_array($parent_category_info)) ? $parent_category_info->toArray() : $parent_category_info;
            $this->view->parent_category_info = ($parent_category_info &&
                    $parent_category_info[0]) ? $parent_category_info[0] : null;
        }

        // BreadCrumb
        if ($this->_request->getParam('menu_id')) {
            Eicra_Global_Variable::getSession()->breadcrumb = array(
                array(
                    str_replace('-', ' ', $this->_request->getParam('menu_id')),
                    $this->view->url()),
                array(
                    $this->_translator->translator(
                            'b2b_category_list_all_title', '', 'B2b'),
                    $this->view->url(
                            array('module' => $this->view->getModule,
                        'controller' => $this->view->getController,
                        'action' => $this->view->getAction), 'Search-Results/*', true)));
        } else {
            if (!empty($parent_id) && $parent_category_info) {
                $breadcrumb_arr = array(
                    array(
                        $this->_translator->translator(
                                'b2b_category_list_all_title', '', 'B2b'),
                        $this->view->url(
                                array(
                            'module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction), 'Search-Results/*', true)));
                $breadcrumb_arr = $this->breadcrumbArray($breadcrumb_arr, $category_db, $parent_category_info[0]['parent_id'], 'Search-Results/*');
                $breadcrumb_arr[] = array(
                    $this->view->escape($parent_category_info[0]['name']) .
                    ' ' . $this->_translator->translator(
                            'b2b_category_list_result_title', $this->view->numbers(
                                    number_format(
                                            $parent_category_info[0]['company_num'])), 'B2b'),
                    $this->view->url(
                            array(
                        'module' => $this->view->getModule,
                        'controller' => $this->view->getController,
                        'action' => $this->view->getAction,
                        'category' => $parent_category_info[0]['url_portion']), 'Search-Results/*', true));

                Eicra_Global_Variable::getSession()->breadcrumb = $breadcrumb_arr;
            } else {
                Eicra_Global_Variable::getSession()->breadcrumb = array(
                    array(
                        $this->_translator->translator(
                                'b2b_category_list_all_title', '', 'B2b'),
                        $this->view->url(
                                array(
                            'module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction), 'Search-Results/*', true)));
            }
        }
    }

    public function advancedsearchAction() {
        
    }

    public function searchresultAction() {
        
    }

    public function categoriesAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = $this->_translator;
        $posted_data = $this->_request->getParams();
        if ($this->_request->isPost()) {
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
            $posted_data['filter']['filters'][] = array('field' => 'displayble',
                'operator' => 'eq', 'value' => '1');

            $category_db = new B2b_Model_DbTable_Category();
            $category_options = $category_db->getListInfo('1', $posted_data, false);
            if ($category_options) {
                $categories = array();
                $key = -1;
                foreach ($category_options as $key => $options) {
                    $categories[$key] = array('id' => $options['id'],
                        'name' => $this->view->escape($options['name']),
                        'b2b_images' => $this->view->escape(
                                $options['b2b_images']));
                }
                $categories[++$key] = array('id' => ' ',
                    'name' => $this->_translator->translator(
                            "common_tree_root"), 'b2b_images' => '');
                $json_arr = array('status' => 'ok', 'msg' => '',
                    'categories' => $categories);
            } else {
                $msg = $translator->translator("common_category_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function businesstypeAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = $this->_translator;
        $posted_data = $this->_request->getParams();
        if ($this->_request->isPost()) {
            $group_id = ($posted_data['group_id']) ? $posted_data['group_id'] : null;

            $type_db = new B2b_Model_DbTable_BusinessType();
            $type_options = $type_db->getBusinessTypeByGroup($group_id);
            if ($type_options) {
                $type_arr = array();
                foreach ($type_options as $key => $options) {
                    $type_arr[$key] = array('id' => $options['id'],
                        'business_type' => $this->view->escape(
                                $options['business_type']),
                        'group_id' => $this->view->escape(
                                $options['group_id']));
                }
                $type_arr[++$key] = array('id' => ' ',
                    'business_type' => $this->_translator->translator(
                            "common_select"), 'group_id' => '');
                $json_arr = array('status' => 'ok', 'msg' => '',
                    'type_arr' => $type_arr);
            } else {
                $msg = $translator->translator("common_type_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function lastminproductlistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_last_deal_header_title', '', 'B2b'),
                'Last-Minute-Deals'),
            array(
                $this->_translator->translator(
                        'b2b_last_deal_product_header_title', '', 'B2b'),
                $this->view->url()));
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
        $this->view->deal = ($this->view->preferences_data &&
                !empty(
                        $this->view->preferences_data['last_minute_time_on_home'])) ? $this->view->preferences_data['last_minute_time_on_home'] : '10';
        $this->view->deal_operator = ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_time_by']) &&
                $this->view->preferences_data['last_minute_time_by'] != '_') ? $this->view->preferences_data['last_minute_time_by'] : 'deal_day';
        if ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_sort_by'])) {
            $last_minute_sort_by_arr = explode('-', $this->view->preferences_data['last_minute_sort_by']);
            $this->view->deal_order = (!empty($last_minute_sort_by_arr[0]) &&
                    $last_minute_sort_by_arr[0] != '_') ? (($last_minute_sort_by_arr[0] ==
                    'deal') ? $this->view->deal_operator . '_num' : $last_minute_sort_by_arr[0]) : 'deal_day_num';
            $this->view->deal_dir = (!empty($last_minute_sort_by_arr[1]) &&
                    $last_minute_sort_by_arr[1] != '_') ? $last_minute_sort_by_arr[1] : 'ASC';
        }
    }

    public function lastminbuyinglistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_last_deal_header_title', '', 'B2b'),
                'Last-Minute-Deals'),
            array(
                $this->_translator->translator(
                        'b2b_last_deal_buying_header_title', '', 'B2b'),
                $this->view->url()));

        // Injecting Package Information to the view model.
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
        $this->view->deal = ($this->view->preferences_data &&
                !empty(
                        $this->view->preferences_data['last_minute_time_on_home'])) ? $this->view->preferences_data['last_minute_time_on_home'] : '10';
        $this->view->deal_operator = ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_time_by']) &&
                $this->view->preferences_data['last_minute_time_by'] != '_') ? $this->view->preferences_data['last_minute_time_by'] : 'deal_day';
        if ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_sort_by'])) {
            $last_minute_sort_by_arr = explode('-', $this->view->preferences_data['last_minute_sort_by']);
            $this->view->deal_order = (!empty($last_minute_sort_by_arr[0]) &&
                    $last_minute_sort_by_arr[0] != '_') ? (($last_minute_sort_by_arr[0] ==
                    'deal') ? $this->view->deal_operator . '_num' : $last_minute_sort_by_arr[0]) : 'deal_day_num';
            $this->view->deal_dir = (!empty($last_minute_sort_by_arr[1]) &&
                    $last_minute_sort_by_arr[1] != '_') ? $last_minute_sort_by_arr[1] : 'ASC';
        }
    }

    public function lastminsellinglistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_last_deal_header_title', '', 'B2b'),
                'Last-Minute-Deals'),
            array(
                $this->_translator->translator(
                        'b2b_last_deal_selling_header_title', '', 'B2b'),
                $this->view->url()));

        // Injecting Package Information to the view model.
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
        $this->view->deal = ($this->view->preferences_data &&
                !empty(
                        $this->view->preferences_data['last_minute_time_on_home'])) ? $this->view->preferences_data['last_minute_time_on_home'] : '10';
        $this->view->deal_operator = ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_time_by']) &&
                $this->view->preferences_data['last_minute_time_by'] != '_') ? $this->view->preferences_data['last_minute_time_by'] : 'deal_day';
        if ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_sort_by'])) {
            $last_minute_sort_by_arr = explode('-', $this->view->preferences_data['last_minute_sort_by']);
            $this->view->deal_order = (!empty($last_minute_sort_by_arr[0]) &&
                    $last_minute_sort_by_arr[0] != '_') ? (($last_minute_sort_by_arr[0] ==
                    'deal') ? $this->view->deal_operator . '_num' : $last_minute_sort_by_arr[0]) : 'deal_day_num';
            $this->view->deal_dir = (!empty($last_minute_sort_by_arr[1]) &&
                    $last_minute_sort_by_arr[1] != '_') ? $last_minute_sort_by_arr[1] : 'ASC';
        }
    }

    public function lastmindeallistAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'b2b_last_deal_header_title', '', 'B2b'),
                $this->view->url()));

        // Injecting Package Information to the view model.
        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting Preferences Information to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
        $this->view->deal = ($this->view->preferences_data &&
                !empty(
                        $this->view->preferences_data['last_minute_time_on_home'])) ? $this->view->preferences_data['last_minute_time_on_home'] : '10';
        $this->view->deal_operator = ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_time_by']) &&
                $this->view->preferences_data['last_minute_time_by'] != '_') ? $this->view->preferences_data['last_minute_time_by'] : 'deal_day';
        if ($this->view->preferences_data &&
                !empty($this->view->preferences_data['last_minute_sort_by'])) {
            $last_minute_sort_by_arr = explode('-', $this->view->preferences_data['last_minute_sort_by']);
            $this->view->deal_order = (!empty($last_minute_sort_by_arr[0]) &&
                    $last_minute_sort_by_arr[0] != '_') ? (($last_minute_sort_by_arr[0] ==
                    'deal') ? $this->view->deal_operator . '_num' : $last_minute_sort_by_arr[0]) : 'deal_day_num';
            $this->view->deal_dir = (!empty($last_minute_sort_by_arr[1]) &&
                    $last_minute_sort_by_arr[1] != '_') ? $last_minute_sort_by_arr[1] : 'ASC';
        }
    }

    public function buyingAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('b2b_post_buying_offer', '', 'B2b'), $this->view->url()));

        $buyingForm = new B2b_Form_WantedForm();
		$buyingForm->addElement
                ('checkbox', 
                'accept', 
                    array
                        ('label' => $this->_translator->translator("b2b_terms_condition_accepted_label"),
                        'id' => 'accept', 
                        'title' => $this->_translator->translator("b2b_terms_condition_accepted_title"), 
                        'info' => $this->_translator->translator("b2b_terms_condition_accepted_info"), 
                        'required'    => true,
                        'uncheckedValue'=> '',
                        )
                );
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $use_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';
        $company_profile_info = null;

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        if (empty($user_id)) {
            $loginForm = new Members_Form_LoginForm();
            $this->view->assign('logindetails', $loginForm);
        }

        // Checking if user have privilege to add more selling leads.
        $dataModel = new B2b_Model_DbTable_Buying();
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;

            try {
                if ($buyingForm->isValid($this->_request->getPost())) {
                    $buyingModel = new B2b_Model_Buying($buyingForm->getValues());
                    $buyingModel->setUser_id($user_id);

                    $buyingModel->setSeo_title($buyingModel->getName());
                    $buyingModel->setActive('0');
                    $buyingModel->setDisplayble('1');
                    $result = $buyingModel->save();

                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($buyingForm));
                    } else {
                        $msg = $translator->translator("page_save_err");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($buyingForm));
                    }
                } else {
                    $validatorMsg = $buyingForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg,
                        'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement(
                                $buyingForm));
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage(),
                    'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement(
                            $buyingForm));
            }
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }


        $this->view->assign('buyingForm', $buyingForm);
    }

    public function groupAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->_request->isPost()) {
            $group_id = $this->_request->getPost('grp_id');
            $parant = $this->_request->getPost('id');
            $selected_id = ($this->_request->getPost('selected_id')) ? $this->_request->getPost(
                            'selected_id') : null;
            $expanded = ($this->_request->getPost('expanded')) ? $this->_request->getPost(
                            'expanded') : false;
            $category_tree = B2b_View_Helper_Categorytree::getTreeDataSource(
                            $parant, $this->view, $group_id, $selected_id, $expanded);
            $translator = Zend_Registry::get('translator');

            $group_db = new B2b_Model_DbTable_Group();
            $group_info = $group_db->getGroupName($group_id);

            $param_fields = array("table_name" => "b2b_group",
                "primary_id_field" => "id",
                "primary_id_field_value" => $group_id,
                "file_path_field" => "file_path_buying_images",
                "file_extension_field" => "file_type",
                "file_max_size_field" => "file_size_max")

            ;
            if ($category_tree) {
                $json_arr = array('status' => 'ok',
                    'TreeDataSource' => $category_tree,
                    'group_info' => $group_info,
                    'param_fields' => $param_fields,
                    'params' => $this->_request->getParams());
            } else {
                $msg = $translator->translator('category_group_err') . ' ' .
                        $group_id . ' ' . $parant . ' ' . $selected_id;
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function buyingdetailsAction() {
        $id = $this->_request->getParam('id');
        $global_conf = Zend_Registry::get('global_conf');
        $buyingTbl = new B2b_Model_DbTable_Buying();
        $buying = $buyingTbl->getOfferById('1', $id);

        if (empty($id) || empty($buying) || count($buying) < 1) {
            $this->_forward('invalidrequest', $this->_request->getControllerName(), $this->_request->getModuleName());
            return;
        }

        if (!empty($buying[0]['company_title'])) {
            $_redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $_redirector->gotoUrl('Buyoffer-Details/' . $buying[0]['company_title'] . '/' . $buying[0]['seo_title']);
            return;
        }

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('permisson_b2b_front_buydetails', '', 'B2b'),
                $this->view->url()
            ),
            array($buying[0]['name'], $this->view->url()));
        $img_thumb_arr = explode(',', $buying[0]['others_images']);

        $buying[0]['primary_file_field_format'] = ($this->view->escape($buying[0]['primary_file_field'])) ? $buying[0]['buying_img_path'] . '/' . $this->view->escape($buying[0]['primary_file_field']) : $buying[0]['buying_img_path'] . '/' . $img_thumb_arr[0];
        $buying[0]['table_name'] = 'b2b_buying_leads';
        $buying[0]['original_price'] = $buying[0]['price'];
        $buying[0]['price'] = $buying[0]['price'];
        //$buying[0]['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($companyDetails[0]['eCommerce'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
        //$this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('buying', $buying[0]);
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';

        if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
            $group_db = new B2b_Model_DbTable_Group();
            //$group_info = $group_db->getGroupName($companyDetails[0]['group_id']);
            $this->_controllerCache->save($group_info, $uniq_id);
        }
        // Assign Review
        if (!empty($this->view->preferences_data['buying_review_id'])) {
            $group_info['review_id'] = $this->view->preferences_data['buying_review_id'];
            $review_helper = new Review_View_Helper_Review();
            $review_datas = $review_helper->getReviewList($group_info, $buying[0]['id']);
            $this->view->review_datas = $review_datas;
            $this->view->review_helper = $review_helper;
        }
        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_products', false);
        $vote_format = $vote->getButton($buying[0]['id'], $this->view->escape($buying[0]['name']));
        $this->view->assign('vote_format', $vote_format);
        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
    }

}
