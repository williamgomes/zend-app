<?php

class B2b_SellingleadsController extends Zend_Controller_Action {

    private $_translator;
    private $_controllerCache;
    private $_auth_obj;

    public function init() {
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() .
                '/Frontend-Login';
        Eicra_Global_Variable::checkSession($this->_response, $url);

        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');

        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;

        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $profileTable = new B2b_Model_DbTable_CompanyProfile();
        $usrProfile = $profileTable->isUserExists($user_id);

        if (empty($usrProfile['id'])) {
            $this->_helper->redirector('companyprofile', 'members', 'B2b', array('noprofile' => 1));
        }
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath(
                APPLICATION_PATH . '/layouts/scripts/' .
                $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout(true));
    }

    public function addOLDAction() {
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_add_selling_lead'),
                $this->view->url()));

        $sellingForm = new B2b_Form_SellingForm();
        $company_profile_info = null;
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $use_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        // Checking if user have privilege to add more selling leads.
        $dataModel = new B2b_Model_DbTable_Selling();

        if (!empty($user_id)) {
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile(
                    $user_id);
            $company_profile_info['group_id'] = ($company_profile_info &&
                    $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
            $sellingForm->group_id->setValue($company_profile_info['group_id']);
            $this->userUploaderSettings($company_profile_info);

            $sellingMapper = new B2b_Model_SellingMapper();
            $resultSet = $sellingMapper->getExistingLeads($user_id);
            if ($resultSet) {
                $this->view->assign('previousOffers', $resultSet);
            }
        } else {
            $json_arr = array('status' => 'err', 'msg' => 'Login Required');
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($sellingForm->isValid($this->_request->getPost())) {
                    $sellingModel = new B2b_Model_Selling(
                            $sellingForm->getValues());

                    $privilege_info = $dataModel->getNumOfLeads($user_id);
                    $package_id = $auth->getIdentity()->package_id;
                    $package_db = new B2b_Model_DbTable_Package();
                    $package_info = $package_db->getPackageById($package_id);
                    $current_leads_num = $privilege_info['current_leads_num'];
                    $max_lead = $package_info['selling'];
                    $active = $package_info['preapproval'];


                    // when a company profile is set as inactive
                    if (empty($privilege_info['company_status']) && !empty($privilege_info['package_id'])) {
                        $msg = $translator->translator('b2b_company_account_suspended_anonymous', $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name'] . ' ) ');
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                    // Checking user permission to add products according to package.
                    // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                    // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                    // (int)$max_lead != 0 checks if user has unlimited access to add product.

                    if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {
                        if ($current_leads_num >= $max_lead) {
                            $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $privilege_info['package_name']);
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                    }

                    if ($permission->allow() && !empty($user_id)) {

                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $sellingModel->setActive('1');
                        } else {
                            $sellingModel->setActive('0');
                        }

                        $sellingModel->setUser_id($user_id);
                        $sellingModel->setSeo_title(
                                $sellingForm->getValue('name'), $user_id);
                        $result = $sellingModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator(
                                "page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $sellingForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
        // =======================================================================
        $privilege_info = $dataModel->getNumOfLeads($user_id);
        $package_id = $privilege_info['package_id'];
        $current_leads_num = $privilege_info['current_leads_num'];
        $max_lead = $privilege_info['max_lead'];

        // when a company profile is set as inactive
        if (empty($company_profile_info['active']) || empty($use_status)) {
            $msg = $privilege_info['company_name'] . ' ' . $this->_translator->translator('b2b_company_account_suspended');
            $this->view->assign('resource_exceeded', $msg);
            return;
        }
        // Checking user permission to add products according to package.
        // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
        // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
        // (int)$max_lead != 0 checks if user has unlimited access to add product.


        if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {

            if ($current_leads_num >= $max_lead) {

                $msg = $privilege_info['full_name'] . ' ' . $this->_translator->translator('b2b_max_num_of_seling_reached_anonymous', $privilege_info['package_name']);
                $msg = $this->_translator->translator('b2b_max_num_of_seling_reached', $privilege_info['package_name']);
                $this->view->assign('resource_exceeded', $msg);
                return;
            }
        }

        // =======================================================================

        $this->view->assign('sellingForm', $sellingForm);
    }

    public function addAction() {
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_manage_selling_leads'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'list'), 'Selling-Offers/*', true)),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_add_selling_lead'),
                $this->view->url()));

        $sellingForm = new B2b_Form_SellingForm($this->view->auth);
        $sellingForm->addElement
                ('checkbox', 'accept', array
            ('label' => $this->_translator->translator("b2b_terms_condition_accepted_label"),
            'id' => 'accept',
            'title' => $this->_translator->translator("b2b_terms_condition_accepted_title"),
            'info' => $this->_translator->translator("b2b_terms_condition_accepted_info"),
            'required' => true,
            'uncheckedValue' => '',
                )
        );
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more selling leads.
        $dataModel = new B2b_Model_DbTable_Selling();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to add selling leads
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($sellingForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $sellingModel = new B2b_Model_Selling($sellingForm->getValues());
                        $owner_id = $sellingModel->getUser_id();
                        //Check Owner
                        if (!empty($owner_id)) {
                            $company_status = $companyProfile->isActive($owner_id);
                            $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                            $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                            $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;

                            //When a user going to add a listing without creating his/her Company profile.
                            if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                // when a company profile is set as inactive
                                if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                    $privilege_info = $dataModel->getNumOfLeads($owner_id);
                                    $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                                    $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                    $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                    $max_lead = ($package_info) ? $package_info['selling'] : 0;
                                    $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                    // Checking user permission to add products according to package.
                                    if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                        // empty($package_id) check if user has unlimited access bypassing package limit.
                                        // empty($max_lead)checks if user has unlimited access to add selling leads.
                                        // Checking user permission to add selling leads according to package.
                                        // ($package_info && ($current_leads_num < $max_lead))  checks if it exceeds package limit
                                        if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_lead) && !empty($package_id) ) || ($package_info && ($current_leads_num < $max_lead)))) {
                                            if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                $sellingModel->setActive('1');
                                            } else {
                                                $sellingModel->setActive('0');
                                            }
                                            $sellingModel->setSeo_title($sellingModel->getName());

                                            $result = $sellingModel->save();
                                            if ($result['status'] == 'ok') {
                                                $msg = $translator->translator("page_save_success");
                                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                                            } else {
                                                $msg = $translator->translator("page_save_err");
                                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                            }
                                        } else {
                                            $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $package_info['name']);
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $msg = $translator->translator('b2b_membership_pkg_found_err');
                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                    }
                                } else {
                                    $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $this->view->escape($mem_info['full_name']) . ' ( ' . $this->view->escape($company_status['company_name']) . ' ) '));
                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                }
                            } else {
                                $msg = $translator->translator('b2b_company_profile_not_found');
                                $json_arr = array('status' => 'err', 'msg' => $msg);
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                        }
                    } else {
                        $validatorMsg = $sellingForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i ++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                $company_status = $companyProfile->isActive($user_id);

                //When a user going to this page without creating his/her Company profile.
                if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                    // when a company profile is set as inactive
                    if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                        $privilege_info = $dataModel->getNumOfLeads($user_id);
                        $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                        $package_id = $auth->getIdentity()->package_id;
                        $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);
                        $max_lead = ($package_info) ? $package_info['selling'] : 0;

                        // empty($package_id) check if user has unlimited access bypassing package limit.
                        // empty($max_lead)checks if user has unlimited access to add selling leads.
                        // Checking user permission to add selling leads according to package.
                        // ($package_info && ($current_leads_num < $max_lead))  checks if it exceeds package limit
                        if (empty($package_id) || empty($max_lead) || ($package_info && ($current_leads_num < $max_lead))) {
                            // check if user has unlimited access to change any group from list
                            if ($auth->getIdentity()->access_other_user_article != '1') {
                                $multiOptions = $sellingForm->group_id->getMultiOptions();
                                $sellingForm->group_id->clearMultiOptions();
                                foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                    if ($company_status['group_id'] == $multiOptionKey) {
                                        $sellingForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                    }
                                }
                            }

                            $sellingForm->group_id->setValue($company_status['group_id']);
                            if ($auth->getIdentity()->access_file_image_manager != '1') {
                                $this->userUploaderSettings($company_status);
                            }
                            $this->view->assign('sellingForm', $sellingForm);
                            $this->view->assign('resource_exceeded', '');
                        } else {
                            $alert = $translator->translator('b2b_max_num_of_seling_reached', $privilege_info['package_name']);
                            $this->view->assign('resource_exceeded', $alert);
                        }
                    } else {
                        $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                        $this->view->assign('resource_exceeded', $alert);
                    }
                } else {
                    $alert = $translator->translator('b2b_company_profile_not_found');
                    $this->view->assign('resource_exceeded', $alert);
                }
            }
        }
    }

    public function editOLDAction() {
        $id = $this->_request->getParam('offer_id');
        $sellingForm = new B2b_Form_SellingForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';


        // Checking if user have privilege to add more selling leads.
        $dataModel = new B2b_Model_DbTable_Selling();
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_edit_selling_lead'),
                $this->view->url()));

        if (!empty($user_id)) {
            $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
            $company_profile_info = $company_profile_db->getUserProfile(
                    $user_id);
            $company_profile_info['group_id'] = ($company_profile_info &&
                    $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
            $sellingForm->group_id->setValue($company_profile_info['group_id']);
            $this->userUploaderSettings($company_profile_info);

            $sellingMapper = new B2b_Model_SellingMapper();
            $resultSet = $sellingMapper->getExistingLeads($user_id);
            if ($resultSet) {
                $this->view->assign('previousOffers', $resultSet);
            }
        }

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator = $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();

            try {
                if ($sellingForm->isValid($this->_request->getPost())) {
                    $sellingModel = new B2b_Model_Selling(
                            $sellingForm->getValues());


                    $privilege_info = $dataModel->getNumOfLeads($user_id);

                    $package_id = $privilege_info['package_id'];
                    $current_leads_num = $privilege_info['current_leads_num'];
                    $max_lead = $privilege_info['max_lead'];
                    $active = $privilege_info['active'];

                    // when a company profile is set as inactive
                    if (empty($privilege_info['company_status']) && !empty($privilege_info['package_id'])) {
                        $msg = $translator->translator('b2b_company_account_suspended_anonymous', $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name'] . ' ) ');
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                    // Checking user permission to add products according to package.
                    // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
                    // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
                    // (int)$max_lead != 0 checks if user has unlimited access to add product.


                    if (count($privilege_info) > 1 && (int) $package_id != 0 && (int) $max_lead != 0) {
                        if ($current_leads_num > $max_lead) {
                            $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $privilege_info['package_name']);
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                            $res_value = Zend_Json::encode($json_arr);
                            $this->_response->setBody($res_value);
                            return;
                        }
                    }



                    if ($permission->allow() && !empty($user_id)) {

                        $sellingModel->setUser_id($user_id);
                        $sellingModel->setId($sellingForm->getValue('id'));
                        $sellingModel->setSeo_title($sellingForm->getValue('name'), $user_id);

                        if (strtoupper($active) == 'YES' || empty($active)) {
                            $sellingModel->setActive('1');
                        } else {
                            $sellingModel->setActive('0');
                        }
                        $result = $sellingModel->save();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err',
                                'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator(
                                "page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }
                } else {
                    $validatorMsg = $sellingForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if ($id) {
                $sellingTable = new B2b_Model_DbTable_Selling();
                $leadsDetails = $sellingTable->getLeadById($id);

                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;

                if (is_array($leadsDetails) && (count($leadsDetails) > 1) &&
                        $isEditable) {

                    $sellingForm->populate($leadsDetails);
                    $payment_type = explode(',', $leadsDetails['payment_type']);
                    $related_items = explode(',', $leadsDetails['related_items']);
                    $price_currency = $leadsDetails['price_currency'];
                    $qty_per_unit = $leadsDetails['qty_per_unit'];
                    $price_per_unit = $leadsDetails['price_per_unit'];
                    $delivery_leadtime = $leadsDetails['delivery_leadtime'];
                    $unit_size = $leadsDetails['unit_size'];

                    $sellingForm->related_items->setValue($related_items);
                    $sellingForm->price_currency->setValue($price_currency);
                    $sellingForm->payment_type->setValue($payment_type);
                    $sellingForm->price_per_unit->setValue($price_per_unit);
                    $sellingForm->delivery_leadtime->setValue(
                            $delivery_leadtime);
                    $sellingForm->unit_size->setValue($unit_size);
                    $validity = date("l, F d, Y h:i:s A", strtotime($leadsDetails['validity']));
                    $sellingForm->validity->setValue($validity);

                    if (!empty($leadsDetails['category'])) {
                        $category_db = new B2b_Model_DbTable_Category();
                        $category_info = $category_db->getCategoryById(
                                $leadsDetails['category']);
                        $this->view->assign('category_info', $category_info);
                    }
                } else {

                    $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
                }
            } else {

                $this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
            }
        }

        $this->view->assign('sellingForm', $sellingForm);
    }

    public function editAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_manage_selling_leads'),
                $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => 'list'), 'Selling-Offers/*', true)),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_edit_selling_lead'),
                $this->view->url()));

        $id = $this->_request->getParam('offer_id');
        $sellingForm = new B2b_Form_SellingForm($this->view->auth);
        $translator = $this->view->translator;
        $auth = $this->view->auth;

        // Assign Session User Id And User Status
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $user_status = ($auth->hasIdentity()) ? $auth->getIdentity()->status : '';

        // Checking if user have privilege to add more selling leads.
        $dataModel = new B2b_Model_DbTable_Selling();

        //Assign Package Database
        $package_db = new B2b_Model_DbTable_Package();

        //Assign CompanyProfile Database
        $companyProfile = new B2b_Model_DbTable_CompanyProfile ();

        // When Ajex based submit to edit selling leads


        /* if (! empty($user_id)) {
          $company_profile_db = new B2b_Model_DbTable_CompanyProfile();
          $company_profile_info = $company_profile_db->getUserProfile(
          $user_id);
          $company_profile_info['group_id'] = ($company_profile_info &&
          $company_profile_info['group_id']) ? $company_profile_info['group_id'] : 1;
          $sellingForm->group_id->setValue($company_profile_info['group_id']);
          $this->userUploaderSettings($company_profile_info);

          $sellingMapper = new B2b_Model_SellingMapper();
          $resultSet = $sellingMapper->getExistingLeads($user_id);
          if ($resultSet) {
          $this->view->assign('previousOffers', $resultSet);
          }

          }

          if ($this->_request->isPost()) {
          $this->_helper->viewRenderer->setNoRender();
          $this->_helper->layout->disableLayout();
          $translator = Zend_Registry::get('translator');
          $this->view->translator = $translator;
          $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
          $json_arr = null;
          $permission = new B2b_View_Helper_Allow();

          try {
          if ($sellingForm->isValid($this->_request->getPost())) {
          $sellingModel = new B2b_Model_Selling(
          $sellingForm->getValues());


          $privilege_info = $dataModel->getNumOfLeads($user_id);

          $package_id = $privilege_info['package_id'] ;
          $current_leads_num = $privilege_info['current_leads_num'] ;
          $max_lead = $privilege_info['max_lead'] ;
          $active = $privilege_info['active'] ;

          // when a company profile is set as inactive
          if (empty ($privilege_info['company_status'])  && !empty($privilege_info['package_id'])){
          $msg =  $translator->translator('b2b_company_account_suspended_anonymous' , $privilege_info['full_name'] . ' ( ' . $privilege_info['company_name']  . ' ) ' );
          $json_arr = array('status' => 'err','msg' => $msg);
          $res_value = Zend_Json::encode($json_arr);
          $this->_response->setBody($res_value);
          return;
          }
          // Checking user permission to add products according to package.
          // count($privilege_info) > 1 && (int)$package_id  checks if it exceeds package limit
          // (int)$package_id != 0  check if user has unlimited access bypassing package limit.
          // (int)$max_lead != 0 checks if user has unlimited access to add product.


          if (count($privilege_info) > 1 && (int)$package_id != 0 && (int)$max_lead != 0) {
          if ($current_leads_num > $max_lead ){
          $msg = $privilege_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $privilege_info['package_name']);
          $json_arr = array('status' => 'err','msg' => $msg);
          $res_value = Zend_Json::encode($json_arr);
          $this->_response->setBody($res_value);
          return;
          }
          }



          if ($permission->allow() && ! empty($user_id)) {

          $sellingModel->setUser_id($user_id);
          $sellingModel->setId($sellingForm->getValue('id'));
          $sellingModel->setSeo_title($sellingForm->getValue('name'), $user_id);

          if (strtoupper ($active) == 'YES' || empty($active)){
          $sellingModel->setActive('1');
          }
          else {
          $sellingModel->setActive('0');
          }
          $result = $sellingModel->save();
          if ($result['status'] == 'ok') {
          $msg = $translator->translator("page_save_success");
          $json_arr = array('status' => 'ok', 'msg' => $msg);
          }

          else {
          $msg = $translator->translator("page_save_err");
          $json_arr = array('status' => 'err',

          'msg' => $msg . " " . $result['msg']);
          }
          }

          else {
          $msg = $translator->translator(
          "page_access_restrictions");
          $res_value = Zend_Json::encode($json_arr);
          $this->_response->setBody($res_value);
          return;
          }
          } else {
          $validatorMsg = $sellingForm->getMessages();
          $vMsg = array();
          $i = 0;
          foreach ($validatorMsg as $key => $errType) {
          foreach ($errType as $errkey => $value) {
          $vMsg[$i] = array('key' => $key,
          'errKey' => $errkey, 'value' => $value);
          $i ++;
          }
          }
          $json_arr = array('status' => 'errV', 'msg' => $vMsg);
          }
          }

          catch (Exception $e) {

          $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
          }

          $res_value = Zend_Json::encode($json_arr);
          $this->_response->setBody($res_value);
          }

          else {
          if ($id) {
          $sellingTable = new B2b_Model_DbTable_Selling();
          $leadsDetails = $sellingTable->getLeadById($id);

          $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;

          if (is_array($leadsDetails) && (count($leadsDetails) > 1) &&
          $isEditable ) {

          $sellingForm->populate($leadsDetails);
          $payment_type = explode(',', $leadsDetails['payment_type']);
          $related_items = explode(',',
          $leadsDetails['related_items']);
          $price_currency = $leadsDetails['price_currency'];
          $qty_per_unit = $leadsDetails['qty_per_unit'];
          $price_per_unit = $leadsDetails['price_per_unit'];
          $delivery_leadtime = $leadsDetails['delivery_leadtime'];
          $unit_size = $leadsDetails['unit_size'];

          $sellingForm->related_items->setValue($related_items);
          $sellingForm->price_currency->setValue($price_currency);
          $sellingForm->payment_type->setValue($payment_type);
          $sellingForm->price_per_unit->setValue($price_per_unit);
          $sellingForm->delivery_leadtime->setValue(
          $delivery_leadtime);
          $sellingForm->unit_size->setValue($unit_size);
          $validity = date("l, F d, Y h:i:s A",
          strtotime($leadsDetails['validity']));
          $sellingForm->validity->setValue($validity);

          if (! empty($leadsDetails['category'])) {
          $category_db = new B2b_Model_DbTable_Category();
          $category_info = $category_db->getCategoryById(
          $leadsDetails['category']);
          $this->view->assign('category_info', $category_info);
          }
          }

          else {

          $this->_forward('add', $this->_request->getControllerName(),
          $this->_request->getModuleName());
          }
          }

          else {

          $this->_forward('add', $this->_request->getControllerName(),
          $this->_request->getModuleName());
          }
          }

          $this->view->assign('sellingForm', $sellingForm); */

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->view->allow()) {
                try {
                    if ($sellingForm->isValid($this->_request->getPost())) {
                        $memberList = new Members_Model_DbTable_MemberList();
                        $sellingModel = new B2b_Model_Selling($sellingForm->getValues());
                        $owner_id = $sellingModel->getUser_id();
                        $id = $sellingModel->getId();

                        if (!empty($id) && is_numeric($id)) {
                            $leadsDetails = $dataModel->getLeadById($id);
                            if ($leadsDetails) {
                                $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;
                                if ($isEditable === true) {
                                    //Check Owner
                                    if (!empty($owner_id)) {
                                        $company_status = $companyProfile->isActive($owner_id);
                                        $mem_info_obj = $memberList->getMemberList(null, array('filter' => array('filters' => array(array('field' => 'user_id', 'operator' => 'eq', 'value' => $owner_id)))));
                                        $mem_info_obj_arr = ($mem_info_obj && !is_array($mem_info_obj)) ? $mem_info_obj->toArray() : null;
                                        $mem_info = ($mem_info_obj_arr && is_array($mem_info_obj_arr)) ? $mem_info_obj_arr[0] : null;


                                        // when a company profile is set as inactive
                                        if (($company_status || ($mem_info && $mem_info['access_other_user_article'] == '1')) || ($auth->getIdentity()->access_other_user_article == '1')) {
                                            //When a user going to edit a listing without creating his/her Company profile.
                                            if ((($company_status['active'] == '1' || $mem_info['access_other_user_article'] == '1') && $mem_info['status'] == '1') || ($auth->getIdentity()->access_other_user_article == '1')) {
                                                $privilege_info = $dataModel->getNumOfLeads($owner_id);
                                                $current_leads_num = ($privilege_info && $privilege_info['current_leads_num']) ? $privilege_info['current_leads_num'] : 0;

                                                $package_id = ($mem_info) ? $mem_info['package_id'] : 0;
                                                $package_info = (empty($package_id)) ? null : $package_db->getPackageById($package_id);

                                                $max_lead = ($package_info) ? $package_info['selling'] : 0;
                                                $active = ($package_info) ? $package_info['preapproval'] : 'NO';

                                                // Checking user permission to edit selling leads according to package.
                                                if (($auth->getIdentity()->access_other_user_article == '1') || $mem_info['access_other_user_article'] == '1' || !empty($package_id)) {
                                                    // empty($package_id) check if user has unlimited access bypassing package limit.
                                                    // empty($max_lead)checks if user has unlimited access to edit selling leads.
                                                    // Checking user permission to edit selling leads according to package.
                                                    // ($package_info && ($current_leads_num <= $max_lead))  checks if it exceeds package limit	
                                                    if (($auth->getIdentity()->access_other_user_article == '1') || ((empty($package_id) && $mem_info['access_other_user_article'] == '1' ) || (empty($max_lead) && !empty($package_id) ) || ($package_info && ($current_leads_num <= $max_lead)))) {
                                                        if (strtoupper($active) == 'YES' || ($mem_info['auto_publish_article'] == '1') || ($auth->getIdentity()->auto_publish_article == '1')) {
                                                            $sellingModel->setActive('1');
                                                        } else {
                                                            $sellingModel->setActive('0');
                                                        }
                                                        $sellingModel->setSeo_title($sellingModel->getName(), $id);

                                                        $result = $sellingModel->save();
                                                        if ($result['status'] == 'ok') {
                                                            $msg = $translator->translator("page_save_success");
                                                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                                                        } else {
                                                            $msg = $translator->translator("page_save_err");
                                                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                                        }
                                                    } else {
                                                        $msg = $mem_info['full_name'] . ' ' . $translator->translator('b2b_max_num_of_seling_reached_anonymous', $package_info['name']);
                                                        $json_arr = array('status' => 'err', 'msg' => $msg);
                                                    }
                                                } else {
                                                    $msg = $translator->translator('b2b_membership_pkg_found_err');
                                                    $json_arr = array('status' => 'err', 'msg' => $msg);
                                                }
                                            } else {
                                                $msg = $this->view->escape($translator->translator('b2b_company_account_suspended_anonymous', $mem_info['full_name'] . ' ( ' . $this->view->escape($company_status['company_name']) . ' ) '));
                                                $json_arr = array('status' => 'err', 'msg' => $msg);
                                            }
                                        } else {
                                            $msg = $translator->translator('b2b_company_profile_not_found');
                                            $json_arr = array('status' => 'err', 'msg' => $msg);
                                        }
                                    } else {
                                        $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_company_profile_not_found'));
                                    }
                                } else {
                                    $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_permission_err'));
                                }
                            } else {
                                $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                            }
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $translator->translator('b2b_edit_page_found_err'));
                        }
                    } else {
                        $validatorMsg = $sellingForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $translator->translator('page_access_restrictions'));
            }

            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if (!empty($user_id)) {
                if (!empty($id) && is_numeric($id)) {
                    $leadsDetails = $dataModel->getLeadById($id);
                    if ($leadsDetails) {
                        $isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $leadsDetails['user_id']) ? true : false;
                        if ($isEditable === true) {
                            $company_status = $companyProfile->isActive($user_id);

                            //When a user going to this page without creating his/her Company profile.
                            if ($company_status || $auth->getIdentity()->access_other_user_article == '1') {
                                // when a company profile is set as inactive
                                if (($company_status['active'] == '1' || $auth->getIdentity()->access_other_user_article == '1') && $user_status == '1') {
                                    //Get Category Info
                                    $categoryData = new B2b_Model_DbTable_Category();

                                    if (empty($leadsDetails['category'])) {
                                        $category_name = $translator->translator("common_tree_root");
                                    } else {
                                        $categoryInfo = $categoryData->getCategoryName($leadsDetails['category']);
                                        $category_name = $categoryInfo['name'];
                                    }

                                    //ASSIGN TIME OF EXPIRY
                                    $locale = Eicra_Global_Variable::getSession()->sess_lang;
//                                    $leadsDetails['validity'] = date("l, F d, Y H:i:s A", strtotime($leadsDetails['validity']));
                                    $date_obj = new Zend_Date($leadsDetails['validity'], null, $locale);
                                    //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                                    $leadsDetails['validity'] = $date_obj->get(Zend_Date::DATE_FULL . " " . Zend_Date::TIMES); //date("Y-m-d H:i:s", $time);
                                    

                                    //ASSIGN RELETED ITEMS
                                    $sellingForm->related_items->setIsArray(true);
                                    $leadsDetails['related_items'] = explode(',', $leadsDetails['related_items']);

                                    //ASSIGN TYPE OR STATUS
                                    //$sellingForm->type->setIsArray(true);									
                                    //$leadsDetails['type'] = explode(',', $leadsDetails['type']);											
                                    //ASSIGN PAYMENT TYPE 
                                    $sellingForm->payment_type->setIsArray(true);
                                    $leadsDetails['payment_type'] = explode(',', $leadsDetails['payment_type']);

                                    //ASSIGN GROUP
                                    if ($auth->getIdentity()->access_other_user_article != '1') {
                                        $multiOptions = $sellingForm->group_id->getMultiOptions();
                                        $sellingForm->group_id->clearMultiOptions();
                                        foreach ($multiOptions as $multiOptionKey => $multiOptionValue) {
                                            if ($company_status['group_id'] == $multiOptionKey) {
                                                $sellingForm->group_id->addMultiOption($multiOptionKey, $multiOptionValue);
                                            }
                                        }
                                    }

                                    $sellingForm->populate($leadsDetails);
                                    $this->userUploaderSettings($leadsDetails);

                                    $this->view->assign('category_info', $categoryInfo);
                                    $this->view->assign('category_name', $category_name);
                                    $this->view->assign('sellingForm', $sellingForm);
                                } else {
                                    $alert = $this->view->escape($company_status['company_name']) . ' ' . $translator->translator('b2b_company_account_suspended');
                                    $this->view->assign('resource_exceeded', $alert);
                                }
                            } else {
                                $alert = $translator->translator('b2b_company_profile_not_found');
                                $this->view->assign('resource_exceeded', $alert);
                            }
                        } else {
                            $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/New-Selling-Offers', array());
                        }
                    } else {
                        $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/New-Selling-Offers', array());
                    }
                } else {
                    $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/New-Selling-Offers', array());
                }
            }
        }
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $this->_helper->viewRenderer->setNoRender();

        $this->_helper->layout->disableLayout();

        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new Hotels_View_Helper_Allow();
            if ($perm->allow('deleteall', 'sellingleads', 'B2b')) {
                $sellingTbl = new B2b_Model_DbTable_Selling();
                $product_images_path = 'data/frontImages/b2b/selling_images';

                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    // DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();
                    foreach ($id_arr as $id) {
                        $sellingMapper = new B2b_Model_SellingMapper();
                        $lead_info = $sellingTbl->getLeadById($id);
                        $product_images = explode(',', $lead_info['others_images']);

                        try {
                            $result = $sellingMapper->delete($id);
                            if ($result['status'] == 'ok') {

                                foreach ($product_images as $key => $file) {
                                    if ($file) {
                                        $dir = $product_images_path . DS . $file;
                                        $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                                    }
                                    if ($res) {
                                        $msg = $translator->translator('page_list_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    } else {
                                        $msg = $translator->translator('page_list_file_delete_success', $file);
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    }
                                }


                                $msg = $translator->translator(
                                        "b2b_common_delete_success");

                                $json_arr = array('status' => 'ok',
                                    'msg' => $msg);
                            } else {

                                $msg = $translator->translator(
                                        "b2b_common_delete_failed");

                                $json_arr = array('status' => 'err',
                                    'msg' => $msg . " " . $result['msg']);
                            }
                        } catch (Exception $e) {

                            $json_arr = array('status' => 'err',
                                'msg' => $e->getMessage());
                        }
                    }
                } else {

                    $msg = $translator->translator("category_selected_err");

                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {

                $msg = $translator->translator('page_delete_perm');

                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {

            $msg = $translator->translator('category_list_delete_err');

            $json_arr = array('status' => 'err', 'msg' => $msg);
        }

        // Convert To JSON ARRAY

        $res_value = Zend_Json::encode($json_arr);

        $this->_response->setBody($res_value);
    }

    public function deleteAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $this->view->translator = $translator;
        $json_arr = null;
        $id = $this->_request->getParam('id');

        $permission = new B2b_View_Helper_Allow();
        if ($permission->allow('delete', 'sellingleads', 'B2b')) {

            try {

                if (empty($id) || !is_numeric($id)) {
                    $msg = $translator->translator("b2b_common_deleted_invalid");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                } else {

                    //page_info
                    $sellingTbl = new B2b_Model_DbTable_Selling();
                    $lead_info = $sellingTbl->getLeadById($id);

                    $product_images = explode(',', $lead_info['others_images']);
                    $product_images_path = 'data/frontImages/b2b/selling_images';

                    $buyingModel = new B2b_Model_SellingMapper();
                    $result = $buyingModel->delete($id);

                    if ($result['status'] == 'ok') {

                        foreach ($product_images as $key => $file) {
                            if ($file) {
                                $dir = $product_images_path . DS . $file;
                                $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                            }
                            if ($res) {
                                $msg = $translator->translator('page_list_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator('page_list_file_delete_success', $file);
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            }
                        }


                        $msg = $translator->translator(
                                "members_control_page_deleted_group_success");

                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {

                        $msg = $translator->translator(
                                "b2b_common_deleted_failed");

                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg']);
                    }
                }
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
        } else {

            $msg = $translator->translator("page_access_restrictions");
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }

        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function listAction() {

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'members_frontend_breadcrumb_dashboard_title'),
                'Members-Dashboard'),
            array(
                $this->_translator->translator(
                        'members_frontend_b2b_manage_selling_leads'),
                $this->view->url()));

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {

            try {

                $approve = $this->_request->getParam('approve');
                $displayble = $this->_request->getParam('displayble');
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                $approve = $this->_request->getParam('approve');

                $displayble = $this->_request->getParam('displayble');

                if ($displayble != null && $displayble != '') {

                    $posted_data['filter']['filters'][] = array(
                        'field' => 'displayble', 'operator' => 'eq',
                        'value' => $displayble);
                }

                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');

                $frontendRoute = 'Selling-Offers/*';

                $posted_data['browser_url'] = $this->view->url(
                        array('module' => $this->view->getModule,
                    'controller' => $this->view->getController,
                    'action' => $this->view->getAction,
                    'approve' => $approve,
                    'displayble' => $displayble,
                    'page' => ($pageNumber == '1' ||
                    empty($pageNumber)) ? null : $pageNumber), $frontendRoute, true);

                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;

                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);

                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);

                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj));

                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {

                    $selling_mapper = new B2b_Model_SellingMapper();

                    $sellingSql = new B2b_Controller_Helper_SellingSql();
                    $tableColumns = $sellingSql->getBackendList();
                    $view_datas = $selling_mapper->fetchAll($pageNumber, $approve, $posted_data, $tableColumns);

                    $this->_controllerCache->save($view_datas, $uniq_id);
                }

                $data_result = array();

                $total = 0;

                if ($view_datas) {

                    $key = 0;

                    foreach ($view_datas as $entry) {

                        $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                        /*  $entry_arr = is_array($entry_arr) ? array_map(
                          'stripslashes', $entry_arr) : stripslashes(
                          $entry_arr); */

                        $entry_arr['publish_status_page_name'] = str_replace(
                                '_', '-', $entry_arr['name']);
                        $img_thumb_arr = explode(',', $entry_arr['selling_images']);

                        $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                        $entry_arr['primary_file_field'])) ? 'data/frontImages/b2b/selling_images/' .
                                $this->view->escape(
                                        $entry_arr['primary_file_field']) : (($this->view->escape(
                                        $entry_arr['others_images'])) ? 'data/frontImages/b2b/selling_images/' .
                                        $img_thumb_arr[0] : '');

                        $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article ==
                                '1' || $this->_auth_obj->user_id ==
                                $entry_arr['user_id']) ? true : false;

                        $data_result[$key] = $entry_arr;

                        $key ++;
                    }

                    $total = $view_datas->getTotalItemCount();
                }

                $json_arr = array('status' => 'ok',
                    'data_result' => $data_result, 'total' => $total,
                    'posted_data' => $posted_data);
            } catch (Exception $e) {

                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }

            // Convert To JSON ARRAY

            $res_value = Zend_Json::encode($json_arr);

            $this->_response->setBody($res_value);
        }
    }

    public function upAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $id = $this->_request->getPost('id');
        $selling_order = $this->_request->getPost('selling_order');
        $table_name = 'b2b_selling_leads';
        $fields_arr = array('id', 'group_id', 'user_id', 'selling_order');

        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->decreaseOrder();

        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']);
        }

        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $id = $this->_request->getPost('id');
        $selling_order = $this->_request->getPost('selling_order');
        $table_name = 'b2b_selling_leads';
        $fields_arr = array('id', 'group_id', 'user_id', 'selling_order');

        $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
        $returnV = $OrderObj->increaseOrder();
        if ($returnV == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'],
                'msg' => $returnV['msg']);
        }

        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $table_name = 'b2b_selling_leads';
            $fields_arr = array('id', 'group_id', 'user_id',
                'selling_order');
            $id_str = $this->_request->getPost('id_arr');
            $selling_order_str = $this->_request->getPost('selling_order_arr');

            $id_arr = explode(',', $id_str);
            $selling_order_arr = explode(',', $selling_order_str);
            $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
            $checkOrder = $order_numbers->checkOrderNumbers($selling_order_arr);
            if ($checkOrder['status'] == 'err') {
                $json_arr = array('status' => 'err',
                    'msg' => $checkOrder['msg']);
            } else {
                // Save Category Order
                $msg = $translator->translator("selling_order_save_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg);
                $i = 0;
                foreach ($id_arr as $id) {
                    $OrderObj = new Administrator_Controller_Helper_GlobalOrders(
                            $table_name, $fields_arr, $id);
                    $result = $OrderObj->saveOrder($selling_order_arr[$i]);
                    if ($result['status'] == 'err') {
                        $json_arr = array('status' => 'err',
                            'msg' => $result['msg']);
                        break;
                    }
                    $i ++;
                }
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function activeAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $productName = $this->_request->getPost('name');
            $paction = $this->_request->getPost('paction');
            $data = array();
            $json_arr = null;
            $active = 0;
            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            try {
                $data = array('active' => $active);
                $sellingMapper = new B2b_Model_SellingMapper();
                $json_arr = $sellingMapper->updateStatus($data, $id);
                $json_arr['active'] = $active;
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage(),
                    'active' => $active);
            }
        }

        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function activateallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');
            $json_arr = null;
            $active = 0;

            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);
                $data = array('active' => $active);
                foreach ($id_arr as $id) {
                    try {
                        $sellingMapper = new B2b_Model_SellingMapper();
                        $json_arr = $sellingMapper->updateStatus($data, $id);
                        $json_arr['active'] = $active;
                    } catch (Exception $e) {
                        $json_arr = array('status' => 'err',
                            'msg' => $e->getMessage(), 'active' => $active);
                    }
                }
            }
        } else {
            $msg = $translator->translator("page_selected_err");
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }

        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function displayableAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;

        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {

                $id = $this->_request->getPost('id');
                // $product_name = $this->_request->getPost('product_name');
                $displayable = $this->_request->getPost('paction');
                $sellingMapper = new B2b_Model_SellingMapper();

                try {
                    $result = $sellingMapper->displayable($id, $displayable);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator(
                                "b2b_common_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg,
                            'displayable' => $displayable);
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'displayable' => $displayable);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err',
                        'msg' => $e->getMessage(),
                        'displayable' => $displayable);
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err', 'msg' => $msg,
                    'displayable' => $displayable);
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function userUploaderSettings($info) {
        $group_db = new B2b_Model_DbTable_Group();
        $info['group_id'] = ($info['group_id']) ? $info['group_id'] : 1;
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array('table_name' => 'b2b_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '', 'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max');

        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;

        /**
         * ***************************************************For
         * Primary************************************************
         */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_selling_images';

        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json::encode($this->view->primary_settings_info));
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array('table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size');
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

}
