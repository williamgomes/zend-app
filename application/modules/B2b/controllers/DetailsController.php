<?php

class B2b_DetailsController extends Zend_Controller_Action {

    private $_page_id;
    private $_translator;
    private $_controllerCache;
    private $_modules_license;
    private $_ckLicense = true;
    private $_snopphing_cart;

    public function init() {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        /* Check Module License */
        //$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
        if ($this->_ckLicense == true) {

            $license = new Zend_Session_Namespace('License');
            if (!$license || !$license->license_data || !$license->license_data['modules']) {
                $curlObj = new Eicra_License_Version();
                $curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
                $license->license_data = $curlObj->getArrayResult();
            }
        }
        $this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
        $this->_snopphing_cart = ($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;
    }

    public function preDispatch() {
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;

        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
    }

    public function buyingAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $buyingTbl = new B2b_Model_DbTable_Buying();
        $company_title = $this->_request->getParam('company_title');
        $title = $this->_request->getParam('title');
        $companyID = $companyProfile->hasCompany($company_title);


        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $inactiveCompany = (empty($companyDetails) || count($companyDetails) < 1 ) ? true : false;

        if (empty($companyDetails[0]['company_active']) || empty($companyDetails[0]['user_status']) || $inactiveCompany) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $buying = $buyingTbl->getOfferDetails('1', $title);
        if (empty($title) || empty($buying) || count($buying) < 1) {
            $this->_forward('inactive', $this->_request->getControllerName(), $this->_request->getModuleName());
            return;
        }

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('permisson_b2b_front_buydetails', '', 'B2b'),
                $this->view->url()
            ),
            array(
                $buying[0]['name'],
                $this->view->url()
            )
        );
        $img_thumb_arr = explode(',', $buying[0]['others_images']);
        $buying[0]['primary_file_field_format'] = ($this->view->escape($buying[0]['primary_file_field'])) ? $companyDetails[0]['buying_img_path'] . '/' . $this->view->escape($buying[0]['primary_file_field']) : $companyDetails[0]['buying_img_path'] . '/' . $img_thumb_arr[0];
        $buying[0]['table_name'] = 'b2b_buying_leads';
        $buying[0]['original_price'] = $buying[0]['price'];
        $buying[0]['price'] = $buying[0]['price'];
        $buying[0]['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($companyDetails[0]['eCommerce'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;


        $this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('buying', $buying[0]);
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';

        if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
            $group_db = new B2b_Model_DbTable_Group();
            $group_info = $group_db->getGroupName($companyDetails[0]['group_id']);
            $this->_controllerCache->save($group_info, $uniq_id);
        }
        // Assign Review
        if (!empty($this->view->preferences_data['buying_review_id'])) {
            $group_info['review_id'] = $this->view->preferences_data['buying_review_id'];
            $review_helper = new Review_View_Helper_Review();
            $review_datas = $review_helper->getReviewList($group_info, $buying[0]['id']);
            $this->view->review_datas = $review_datas;
            $this->view->review_helper = $review_helper;
        }
        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_buying_leads', false);
        $vote_format = $vote->getButton($buying[0]['id'], $this->view->escape($buying[0]['name']));
        $this->view->assign('vote_format', $vote_format);
    }

    public function sellingAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $currency_converter_helper = new Paymentgateway_Controller_Helper_Currency($global_conf);
        $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        $sellingTbl = new B2b_Model_DbTable_Selling();
        $title = $this->_request->getParam('title');
        $selling = $sellingTbl->getOfferDetails('1', $title);


        if (empty($title) || empty($selling) || count($selling) < 1) {
            $this->_forward('inactive', $this->_request->getControllerName(), $this->_request->getModuleName());
            return;
        }

        if (empty($selling[0]['company_id']) || trim($selling[0]['company_id']) == '' || $selling[0]['company_id'] < 1) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }
        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }


        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $inactiveCompany = (empty($companyDetails) || count($companyDetails) < 1 ) ? true : false;

        if (empty($companyDetails[0]['company_active']) || empty($companyDetails[0]['user_status']) || $inactiveCompany) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }


        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('permisson_b2b_front_selldetails', '', 'B2b'),
                $this->view->url()
            ),
            array(
                $selling[0]['name'],
                $this->view->url()
            )
        );

        $img_thumb_arr = explode(',', $selling[0]['others_images']);

        $selling[0]['primary_file_field_format'] = ($this->view->escape($selling[0]['primary_file_field'])) ? $companyDetails[0]['selling_img_path'] . '/' . $this->view->escape($selling[0]['primary_file_field']) : $companyDetails[0]['selling_img_path'] . '/' . $img_thumb_arr[0];
        $selling[0]['table_name'] = 'b2b_selling_leads';
        $selling[0]['original_price'] = $selling[0]['price'];
        $selling[0]['price'] = $currency_converter_helper->convert($selling[0]['price_currency_locale'], $selling[0]['original_price']);
        $selling[0]['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($companyDetails[0]['eCommerce'] == 'YES') && ($this->_snopphing_cart == true) && ($currency_converter_helper->checkCurrency($selling[0]['price_currency_locale']) == true)) ? true : false;

        $this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('selling', $selling[0]);
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';

        if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
            $group_db = new B2b_Model_DbTable_Group();
            $group_info = $group_db->getGroupName($companyDetails[0]['group_id']);
            $this->_controllerCache->save($group_info, $uniq_id);
        }
        // Assign Review
        if (!empty($this->view->preferences_data['selling_review_id'])) {
            $group_info['review_id'] = $this->view->preferences_data['selling_review_id'];
            $review_helper = new Review_View_Helper_Review();
            $review_datas = $review_helper->getReviewList($group_info, $selling[0]['id']);
            $this->view->review_datas = $review_datas;
            $this->view->review_helper = $review_helper;
        }

        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_selling_leads', false);
        $vote_format = $vote->getButton($selling[0]['id'], $this->view->escape($selling[0]['name']));
        $this->view->assign('vote_format', $vote_format);
    }

    public function productsAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $currency_converter_helper = new Paymentgateway_Controller_Helper_Currency($global_conf);
        $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        $productTbl = new B2b_Model_DbTable_Products();
        $title = $this->_request->getParam('title');
        $product = $productTbl->getOfferDetails('1', $title);

        if (empty($title) || empty($product) || count($product) < 1) {
            $this->_forward('inactive', $this->_request->getControllerName(), $this->_request->getModuleName());
            return;
        }

        if (empty($product[0]['company_id']) || trim($product[0]['company_id']) == '' || $product[0]['company_id'] < 1) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $inactiveCompany = (empty($companyDetails) || count($companyDetails) < 1 ) ? true : false;

        if (empty($companyDetails[0]['company_active']) || empty($companyDetails[0]['user_status']) || $inactiveCompany) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }



        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator('permisson_b2b_front_productdetails', '', 'B2b'),
                $this->view->url()
            ),
            array(
                $product[0]['name'],
                $this->view->url()
            )
        );

        $img_thumb_arr = explode(',', $product[0]['product_images']);

        $product[0]['primary_file_field_format'] = ($this->view->escape($product[0]['product_images_primary'])) ? $companyDetails[0]['product_img_path'] . '/' . $this->view->escape($product[0]['product_images_primary']) : $companyDetails[0]['product_img_path'] . '/' . $img_thumb_arr[0];
        $product[0]['table_name'] = 'b2b_products';
        $product[0]['original_price'] = $product[0]['price'];
        $product[0]['price'] = $currency_converter_helper->convert($product[0]['price_currency_locale'], $product[0]['original_price']);
        $product[0]['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($companyDetails[0]['eCommerce'] == 'YES') && ($this->_snopphing_cart == true) && ($currency_converter_helper->checkCurrency($product[0]['price_currency_locale']) == true)) ? true : false;

        $this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('product', $product[0]);
//        print_r($product[0]);
        $relatedProducts = $product[0]['related_items'];
        $arrRelatedProduct = $productTbl->getRelatedProduct('1',$relatedProducts);
        $this->view->assign('relatedProduct', $arrRelatedProduct);
//        print_r($arrRelatedProduct);
        
        
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';

        if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
            $group_db = new B2b_Model_DbTable_Group();
            $group_info = $group_db->getGroupName($companyDetails[0]['group_id']);
            $this->_controllerCache->save($group_info, $uniq_id);
        }

        // Assign Review
        if (!empty($this->view->preferences_data['product_review_id'])) {
            $group_info['review_id'] = $this->view->preferences_data['product_review_id'];
            $review_helper = new Review_View_Helper_Review();
            $review_datas = $review_helper->getReviewList($group_info, $product[0]['id']);
            $this->view->review_datas = $review_datas;
            $this->view->review_helper = $review_helper;
        }
        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_products', false);
        $vote_format = $vote->getButton($product[0]['id'], $this->view->escape($product[0]['name']));
        $this->view->assign('vote_format', $vote_format);
    }

    public function inactiveAction() {
        $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $productTbl = new B2b_Model_DbTable_Products();
        $company_title = $this->_request->getParam('company_title');
        $title = $this->_request->getParam('title');

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        $companyName = ($companyDetails[0]['company_name']) ? $companyDetails[0]['company_name'] : $company_title;
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $companyName,
                $this->view->url()
            ),
            array(
                $this->_translator->translator('b2b_inactive_listing_page', '', 'B2b'),
                $this->view->url()
            )
        );

        $this->view->assign('companyDetails', $companyDetails[0]);
    }

}
