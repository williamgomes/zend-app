<?php

class B2b_Controller_Helper_ProductSql
{

    public function getBackendList ()
    {
        $sql = array(

                'user_profile' => array('user_profile' => 'cat.name',
                        'username' => 'usr.username',
                        'firstName' => 'usr.firstName',
                        'lastName' => 'usr.lastName', 'title' => 'usr.title',
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'user_country_id' => 'usr.country'),

                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'selling_categories' => 'cpy.selling_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'priority' => 'pkg.priority'),

                'userChecking' => true);

        return $sql;
    }

    public function getB2bContentFeaturedList ($arr = array())
    {
        $sql = array(
                'b2b_products' => array('added_on' => 'pro.added_on',
                        'name' => 'pro.name','user_id' => 'pro.user_id', 'id' => 'pro.id',
                        'seo_title' => 'pro.seo_title',
                        'product_images_primary' => 'pro.product_images_primary',
                        'product_images' => 'pro.product_images',
						'price_currency_locale'	=> 'pro.price_currency_locale',
                        'price_currency' => 'pro.price_currency',
                        'price' => 'pro.price',
						'strikethrough_price'	=> 'pro.strikethrough_price',
                        'short_description' => 'pro.short_description',
                        '(SELECT SUM(vts.vote_value) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes',
                                '(SELECT COUNT(vtc.id) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vtc WHERE pro.id  = vtc.table_id) AS voter_count'),

                'user_profile' => array(
                        'username' => 'usr.username',
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'cpy_active' => 'cpy.active'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_product_images',
                        'review_id' => 'grp.review_id'),

                'userChecking' => false,

                'dataLimit' => $arr['dataLimit']);

        return $sql;
    }

    public function getB2bContentPopularList ($arr = array())
    {
        $sql = array(
                'b2b_products' => array('name' => 'pro.name','user_id' => 'pro.user_id', 'id' => 'pro.id',
                        'seo_title' => 'pro.seo_title',
                        'product_images_primary' => 'pro.product_images_primary',
                        'product_images' => 'pro.product_images',
                        'price_currency' => 'pro.price_currency',
						'price_currency_locale'	=> 'pro.price_currency_locale',
                        'price' => 'pro.price',
						'strikethrough_price'	=> 'pro.strikethrough_price',
                        'short_description' => 'pro.short_description',
                        'max_supply' => 'pro.max_supply',
                        'max_unit_type' => 'pro.max_unit_type',
                        'delivery_leadtime' => 'pro.delivery_leadtime',
                        'min_quantity' => 'pro.min_quantity',
                        'min_unit_type' => 'pro.min_unit_type',
                        '(SELECT SUM(vts.vote_value) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes',
                                '(SELECT COUNT(vtc.id) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vtc WHERE pro.id  = vtc.table_id) AS voter_count'),
                'user_profile' => array(
                        'username' => 'usr.username',
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'cpy_active' => 'cpy.active'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
						'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_product_images',
                        'review_id' => 'grp.review_id'),

                'userChecking' => false,

                'dataLimit' => $arr['dataLimit']);

        return $sql;
    }

    public function getB2bContentLatestList ($arr = array())
    {
        $sql = array(
                'b2b_products' => array('name' => 'pro.name', 'user_id' => 'pro.user_id', 'id' => 'pro.id',
                        'seo_title' => 'pro.seo_title',
                        'product_images_primary' => 'pro.product_images_primary',
                        'product_images' => 'pro.product_images',
						'price_currency_locale'	=> 'pro.price_currency_locale',
                        'price_currency' => 'pro.price_currency',
                        'price' => 'pro.price',
                        'short_description' => 'pro.short_description',
						'strikethrough_price'	=> 'pro.strikethrough_price',
                        'added_on' => 'pro.added_on',
                        '(SELECT SUM(vts.vote_value) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes',
                                '(SELECT COUNT(vtc.id) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vtc WHERE pro.id  = vtc.table_id) AS voter_count'),
                'user_profile' => array(
                        'username' => 'usr.username',
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'cpy_active' => 'cpy.active'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
						'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_product_images',
                        'review_id' => 'grp.review_id'),

                'userChecking' => false,

                'dataLimit' => $arr['dataLimit']);

        return $sql;
    }

    public function getFontendList ()
    {
        $sql = array(

                'b2b_products' => array('id', 'user_id', 'name',
                        'time_of_expiry', 'specifications', 'brand_name',
                        'short_description', 'seo_title',
                        'product_images_primary', 'product_images', 'type',
                        'min_quantity', 'min_unit_type', 'place_of_origin',
                        'model_num', 'fob_price', 'max_supply', 'max_unit_type',
                        'size', 'unit_size', 'displayble', 'payment_terms',
                        'manufacturers', 'delivery_leadtime',
						'price_currency_locale'	, 'price_currency',
                        'price', 'price_per_unit', 'category_id', 'packaging',
                        'strikethrough_price', 'added_on', 'featured', 'keyword',
                        'keyword_1', 'keyword_2', 'keyword_3'),

                'user_profile' => array(
                        'username' => 'usr.username',
						'status' => 'usr.status',
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'selling_categories' => 'cpy.selling_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'b2b_biztype_mapper' => array(
                        'biztype_mapper_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),

                'b2b_group' => array(
                        'images_path' 			=> 'grp.file_path_product_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' 			=> 'grp.review_id'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
						'priority_flag'=>	'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority',
						'directory' => 'pkg.directory'),

                'where' => array('usr.status = ?' => '1',
                        'pro.displayble = ?' => '1', 'cpy.active = ?' => '1'),

                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
	
	
	public function getFontendDirectoryList ()
    {
        $sql = array(

                'b2b_products' => array('id', 'user_id', 'name',
                        'time_of_expiry', 'specifications', 'brand_name',
                        'short_description', 'seo_title',
                        'product_images_primary', 'product_images', 'type',
                        'min_quantity', 'min_unit_type', 'place_of_origin',
                        'model_num', 'fob_price', 'max_supply', 'max_unit_type',
                        'size', 'unit_size', 'displayble', 'payment_terms',
                        'manufacturers', 'delivery_leadtime',
						'price_currency_locale'	, 'price_currency',
                        'price', 'price_per_unit', 'category_id', 'packaging',
                        'strikethrough_price', 'added_on', 'featured', 'keyword',
                        'keyword_1', 'keyword_2', 'keyword_3'),

                'user_profile' => array(
                        'username' => 'usr.username',
						'status' => 'usr.status',
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'selling_categories' => 'cpy.selling_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'b2b_biztype_mapper' => array(
                        'biztype_mapper_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),

                'b2b_group' => array(
                        'images_path' 			=> 'grp.file_path_product_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' 			=> 'grp.review_id'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
						'priority_flag'=>	'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority',
						'directory' => 'pkg.directory'),

                'where' => array('usr.status = ?' => '1',
                        'pro.displayble = ?' => '1', 'cpy.active = ?' => '1', 'pkg.directory = ?' => 'YES'),

                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
	
	public function getFontendLastMinDeal ()
    {
        $sql = array(

                'b2b_products' => array('id', 'user_id', 'name', 'time_of_expiry',
                        'time_of_expiry', 'specifications', 'brand_name',
                        'short_description', 'seo_title',
                        'product_images_primary', 'product_images', 'type',
                        'min_quantity', 'min_unit_type', 'place_of_origin',
                        'model_num', 'fob_price', 'max_supply', 'max_unit_type',
                        'size', 'unit_size', 'displayble', 'payment_terms',
                        'manufacturers', 'delivery_leadtime',
						'price_currency_locale', 'price_currency',
                        'price', 'price_per_unit', 'category_id', 'packaging',
                        'strikethrough_price', 'added_on', 'featured', 'keyword',
                        'keyword_1', 'keyword_2', 'keyword_3',						
						'(TIMESTAMPDIFF(HOUR,now(), pro.time_of_expiry)) AS deal_hour_num',
						'(DATEDIFF(pro.time_of_expiry, now())) AS deal_day_num',
						'(period_diff(date_format(pro.time_of_expiry, "%Y%m"), date_format(now(), "%Y%m"))) AS deal_month_num',
						'(period_diff(date_format(pro.time_of_expiry, "%Y"), date_format(now(), "%Y"))) AS deal_year_num'
				),

                'user_profile' => array(
                        'username' => 'usr.username',
						'status' => 'usr.status',
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'selling_categories' => 'cpy.selling_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'b2b_biztype_mapper' => array(
                        'biztype_mapper_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),

                'b2b_group' => array(
                        'images_path' 			=> 'grp.file_path_product_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' 			=> 'grp.review_id'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
						'priority_flag'=>	'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'where' => array('usr.status = ?' => '1',
                        'pro.displayble = ?' => '1', 'cpy.active = ?' => '1'),

                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
}