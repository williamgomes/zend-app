<?php

class B2b_Controller_Helper_SuccessStoriesSql
{

    public function getBackendList ()
    {
	}

   

    public function getFontendList ()
    {
        $sql = array(

                'b2b_success_stories' => array(
										'id',
										'title',
										'seo_title',
										'name',
										'description',
										'b2b_images',
										'user_id',
										'displayable',
										'added_on'
									),
                'user_profile' => array(				
										'status' => 'usr.status',
										'state' => 'usr.state',
										'city' => 'usr.city',
										'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
				),

                'countries' => array(
								'country_name' => 'tld.value',
            					'country_code' => 'tld.code'
				),
                
                'membership_packages' => array(
												'package_name' => 'pkg.name',
												'package_logo' => 'pkg.b2b_images',
												'is_feedback' => 'pkg.feedback',
												'is_profile' => 'pkg.profile',
												'priority_flag' => 'pkg.priority_flag'
										),
				'b2b_company_profile' => array(
                        		'company_name' => 'cpy.company_name',
								'establishing_year' => 'cpy.establishing_year',
								'keywords' => 'cpy.keywords',
								'main_products' => 'cpy.main_products',
								'company_title' => 'cpy.seo_title',
								'trade_type' => 'cpy.trade_type'
						),
				
					
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_b2b_images'
						),
						
                'where' => array('usr.status = ?' => '1', 'sst.displayable = ?' => '1', 'cpy.active = ?' => '1'),

                'userChecking' => false);
        return $sql;
    }
}