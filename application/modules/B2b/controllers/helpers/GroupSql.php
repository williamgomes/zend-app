<?php
class B2b_Controller_Helper_GroupSql
{

    public function getBackendList ()
    {
        $sql = array(

                'user_profile' => array(
                        'username' => 'usr.username',
                        'firstName' => 'usr.firstName',
                        'lastName' => 'usr.lastName', 'title' => 'usr.title',
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        ),
                'b2b_group' => array('group_name' => 'grp.group_name',
                        'id' => 'grp.id',
                        'role_id' => 'grp.role_id',
                        'group_type' => 'grp.group_type',
                        'active' => 'grp.active',
                        'group_type' => 'grp.group_type',
						'(SELECT COUNT(bc.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_category as bc WHERE grp.id = bc.group_id) AS category_num',
						'(SELECT COUNT(bcp.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_company_profile as bcp WHERE grp.id = bcp.group_id) AS company_profile_num',
						'(SELECT COUNT(bp.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_products as bp WHERE grp.id = bp.group_id) AS product_num',
						'(SELECT COUNT(bsl.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_selling_leads as bsl WHERE grp.id = bsl.group_id) AS selling_leads_num',
						'(SELECT COUNT(bbl.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_buying_leads as bbl WHERE grp.id = bbl.group_id) AS buying_leads_num',
						'(SELECT COUNT(bbt.id) FROM '.Zend_Registry::get('dbPrefix').'b2b_business_type as bbt WHERE grp.id = bbt.group_id) AS business_type_num'
						),
                'userChecking' => true);

        return $sql;
    }
   

    public function getFontendList ()
    {
        $sql = array();
        return $sql;
    }
	
	
}