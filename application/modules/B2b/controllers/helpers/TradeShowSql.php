<?php

class B2b_Controller_Helper_TradeShowSql
{

    public function getBackendList ()
    {
	}

   

    public function getFontendList ()
    {
        $sql = array(

                'b2b_tradeshow' => array(
										'start_date',
										'end_date',
										'name',
										'title',
										'seo_title',
										'address',
										'description',										
										'timepicker',
										'b2b_images',
                    					'country',
										'state',
										'city',
										'venue',
										'displayable',
										'added_on'
									),
                'user_profile' => array(				
										'status' => 'usr.status',
										'usr_state' => 'usr.state',
										'usr_city' => 'usr.city',
										'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
				),

                'tradeshow_countries' => array(
								'country_name' => 'tld.value',
            					'country_code' => 'tld.code'
				),

                'user_countries' => array(
								'user_country_name' => 'usrtld.value',
        						'user_country_code' => 'usrtld.code'
				),
                
                'membership_packages' => array(
												'package_name' => 'pid.name',
												'package_logo' => 'pid.b2b_images',
												'is_feedback' => 'pid.feedback',
												'is_voting' => 'pid.voting',
												'is_profile' => 'pid.profile',
												'is_telephone' => 'pid.telephone',
												'is_website' => 'pid.website',
												'priority' => 'pid.priority'
										),
				'b2b_company_profile' => array(
                        		'company_name' => 'cyp.company_name',
								'establishing_year' => 'cyp.establishing_year',
								'company_title' => 'cyp.seo_title',
								'trade_type' => 'cyp.trade_type'
						),
						
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_b2b_images'
						),
						
                'where' => array('usr.status = ?' => '1', 'ts.displayable = ?' => '1', 'cyp.active = ?' => '1'),

                'userChecking' => false);
        return $sql;
    }
}