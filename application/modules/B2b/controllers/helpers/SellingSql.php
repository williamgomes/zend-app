<?php

class B2b_Controller_Helper_SellingSql {

    public function getB2bContentSellingLatestList($arr = array()) {
        $sql = array(
            'b2b_selling_leads' => array(
                'name' => 'sel.name',
                'id' => 'sel.id',
                'seo_title' => 'sel.seo_title',
                'primary_file_field' => 'sel.primary_file_field',
                'others_images' => 'sel.others_images',
                'price_currency_locale' => 'sel.price_currency_locale',
                'price_currency' => 'sel.price_currency',
                'price' => 'sel.price',
                'strikethrough_price' => 'sel.strikethrough_price',
                'description' => 'sel.description',
                'added_on' => 'sel.added_on',
                '(SELECT SUM(vts.vote_value) FROM ' . Zend_Registry::get('dbPrefix') . 'vote_voting as vts WHERE sel.id  = vts.table_id) AS total_votes',
                '(SELECT COUNT(vtc.id) FROM ' . Zend_Registry::get('dbPrefix') . 'vote_voting as vtc WHERE sel.id  = vtc.table_id) AS voter_count'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'company_title' => 'cpy.seo_title',
                'cpy_active' => 'cpy.active'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'eCommerce' => 'pkg.eCommerce',
                'priority' => 'pkg.priority'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'review_id' => 'grp.review_id'
            ),
            'userChecking' => false,
            'dataLimit' => $arr['dataLimit']
        );

        return $sql;
    }

    public function getB2bContentSellingLastMinDealList($arr = array()) {
        $sql = array(
            'b2b_selling_leads' => array(
                'name' => 'sel.name',
                'id' => 'sel.id',
                'seo_title' => 'sel.seo_title',
                'primary_file_field' => 'sel.primary_file_field',
                'others_images' => 'sel.others_images',
                'price_currency_locale' => 'sel.price_currency_locale',
                'price_currency' => 'sel.price_currency',
                'price' => 'sel.price',
                'strikethrough_price' => 'sel.strikethrough_price',
                'description' => 'sel.description',
                'added_on' => 'sel.added_on',
                '(SELECT SUM(vts.vote_value) FROM ' . Zend_Registry::get('dbPrefix') . 'vote_voting as vts WHERE sel.id  = vts.table_id) AS total_votes',
                '(SELECT COUNT(vtc.id) FROM ' . Zend_Registry::get('dbPrefix') . 'vote_voting as vtc WHERE sel.id  = vtc.table_id) AS voter_count',
                '(TIMESTAMPDIFF(HOUR,now(), sel.validity)) AS deal_hour_num',
                '(DATEDIFF(sel.validity, now())) AS deal_day_num',
                '(period_diff(date_format(sel.validity, "%Y%m"), date_format(now(), "%Y%m"))) AS deal_month_num',
                '(period_diff(date_format(sel.validity, "%Y"), date_format(now(), "%Y"))) AS deal_year_num'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'company_title' => 'cpy.seo_title',
                'cpy_active' => 'cpy.active'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'eCommerce' => 'pkg.eCommerce',
                'priority' => 'pkg.priority'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'review_id' => 'grp.review_id'
            ),
            'userChecking' => false,
            'dataLimit' => $arr['dataLimit']
        );

        return $sql;
    }

    public function getFontendList() {
        $sql = array(
            'b2b_selling_leads' => array(
                'id',
                'user_id',
                'name',
                'validity',
                'category',
                'seo_title',
                'description',
                'primary_file_field',
                'others_images',
                'fob_price',
                'shipping',
                'quantity',				
                'qty_per_unit',
				'size',
				'unit_size',
                'displayble',
                'payment_type',
                'specifications',
                'fob_price',
                'delivery',
                'delivery_leadtime',
                'price_currency_locale',
                'price_currency',
                'price',
                'strikethrough_price',
                'price_per_unit',
                'category',
                'place_of_origin',
                'added_on',
                'featured',
                'keyword',
                'keyword_1',
                'keyword_2',
                'keyword_3',
                'featured'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city',
                'package_id' => 'usr.package_id',
                'user_country_id' => 'usr.country'
            ),
            'countries' => array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'is_feedback' => 'pkg.feedback',
                'is_voting' => 'pkg.voting',
                'is_profile' => 'pkg.profile',
                'is_telephone' => 'pkg.telephone',
                'is_website' => 'pkg.website',
                'eCommerce' => 'pkg.eCommerce',
                'priority_flag' => 'pkg.priority_flag',
                'priority' => 'pkg.priority',
                'directory' => 'pkg.directory'
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'selling_categories' => 'cpy.selling_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'
            ),
            'b2b_biztype_mapper' => array(
                'business_type_id' => 'biz.biztype_id'
            ),
            'b2b_business_type' => array(
                'business_type' => 'typ.business_type'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'file_path_b2b_images' => 'grp.file_path_b2b_images',
                'review_id' => 'grp.review_id'
            ),
            'where' => array(
                'usr.status = ?' => '1',
                'sel.displayble = ?' => '1',
                'cpy.active = ?' => '1'
            ),
            'userChecking' => false,
            'setUniqueResult' => true
        );

        return $sql;
    }

    public function getFontendDirectoryList() {
        $sql = array(
            'b2b_selling_leads' => array(
                'id',
                'user_id',
                'name',
                'validity',
                'category',
                'seo_title',
                'description',
                'primary_file_field',
                'others_images',
                'fob_price',
                'shipping',
                'quantity',
                'qty_per_unit',
                'displayble',
                'payment_type',
                'specifications',
                'delivery',
                'delivery_leadtime',
                'price_currency_locale',
                'price_currency',
                'price',
                'strikethrough_price',
                'price_per_unit',
                'category',
                'place_of_origin',
                'added_on',
                'featured',
                'keyword',
                'keyword_1',
                'keyword_2',
                'keyword_3',
                'featured'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city',
                'package_id' => 'usr.package_id',
                'user_country_id' => 'usr.country'
            ),
            'countries' => array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'is_feedback' => 'pkg.feedback',
                'is_voting' => 'pkg.voting',
                'is_profile' => 'pkg.profile',
                'is_telephone' => 'pkg.telephone',
                'is_website' => 'pkg.website',
                'eCommerce' => 'pkg.eCommerce',
                'priority_flag' => 'pkg.priority_flag',
                'priority' => 'pkg.priority',
                'directory' => 'pkg.directory'
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'selling_categories' => 'cpy.selling_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'
            ),
            'b2b_biztype_mapper' => array(
                'business_type_id' => 'biz.biztype_id'
            ),
            'b2b_business_type' => array(
                'business_type' => 'typ.business_type'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'file_path_b2b_images' => 'grp.file_path_b2b_images',
                'review_id' => 'grp.review_id'
            ),
            'where' => array(
                'usr.status = ?' => '1',
                'sel.displayble = ?' => '1',
                'cpy.active = ?' => '1',
                'pkg.directory = ?' => 'YES'
            ),
            'userChecking' => false,
            'setUniqueResult' => true
        );

        return $sql;
    }

    public function getFontendLastMinDeal() {
        $sql = array(
            'b2b_selling_leads' => array(
                'id',
                'user_id',
                'name',
                'validity',
                'category',
                'seo_title',
                'description',
                'primary_file_field',
                'others_images',
                'fob_price',
                'shipping',
                'quantity',
                'qty_per_unit',
                'displayble',
                'payment_type',
                'specifications',
                'delivery',
                'delivery_leadtime',
                'price_currency_locale',
                'price_currency',
                'price',
                'strikethrough_price',
                'price_per_unit',
                'category',
                'place_of_origin',
                'added_on',
                'featured',
                'keyword',
                'keyword_1',
                'keyword_2',
                'keyword_3',
                'featured',
                '(TIMESTAMPDIFF(HOUR,now(), sel.validity)) AS deal_hour_num',
                '(DATEDIFF(sel.validity, now())) AS deal_day_num',
                '(period_diff(date_format(sel.validity, "%Y%m"), date_format(now(), "%Y%m"))) AS deal_month_num',
                '(period_diff(date_format(sel.validity, "%Y"), date_format(now(), "%Y"))) AS deal_year_num'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city',
                'package_id' => 'usr.package_id',
                'user_country_id' => 'usr.country'
            ),
            'countries' => array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'is_feedback' => 'pkg.feedback',
                'is_voting' => 'pkg.voting',
                'is_profile' => 'pkg.profile',
                'is_telephone' => 'pkg.telephone',
                'is_website' => 'pkg.website',
                'eCommerce' => 'pkg.eCommerce',
                'priority_flag' => 'pkg.priority_flag',
                'priority' => 'pkg.priority'
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'selling_categories' => 'cpy.selling_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'
            ),
            'b2b_biztype_mapper' => array(
                'business_type_id' => 'biz.biztype_id'
            ),
            'b2b_business_type' => array(
                'business_type' => 'typ.business_type'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'file_path_b2b_images' => 'grp.file_path_b2b_images',
                'review_id' => 'grp.review_id'
            ),
            'where' => array(
                'usr.status = ?' => '1',
                'sel.displayble = ?' => '1',
                'cpy.active = ?' => '1'
            ),
            'userChecking' => false,
            'setUniqueResult' => true
        );

        return $sql;
    }

    public function getCompanyFontendList($user_id) {
        $sql = array(
            'b2b_selling_leads' => array(
                'id',
                'user_id',
                'name',
                'validity',
                'category',
                'seo_title',
                'description',
                'primary_file_field',
                'others_images',
                'fob_price',
                'shipping',
                'quantity',
                'qty_per_unit',
                'displayble',
                'payment_type',
                'specifications',
                'delivery',
                'delivery_leadtime',
                'price_currency_locale',
                'price_currency',
                'price',
                'strikethrough_price',
                'price_per_unit',
                'category',
                'place_of_origin',
                'added_on',
                'keyword',
                'keyword_1',
                'keyword_2',
                'keyword_3',
                'featured'
            ),
            'user_profile' => array(
                'username' => 'usr.username',
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city',
                'user_country_id' => 'usr.country'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'is_feedback' => 'pkg.feedback',
                'is_voting' => 'pkg.voting',
                'is_profile' => 'pkg.profile',
                'is_telephone' => 'pkg.telephone',
                'is_website' => 'pkg.website',
                'priority_flag' => 'pkg.priority_flag',
                'priority' => 'pkg.priority'
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'selling_categories' => 'cpy.selling_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'
            ),
            'b2b_biztype_mapper' => array(
                'business_type_id' => 'biz.biztype_id'
            ),
            'b2b_business_type' => array(
                'business_type' => 'typ.business_type'
            ),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'file_path_b2b_images' => 'grp.file_path_b2b_images',
                'review_id' => 'grp.review_id'
            ),
            'where' => array(
                'usr.status = ?' => '1',
                'sel.displayble = ?' => '1',
                'cpy.active = ?' => '1',
                'sel.user_id = ?' => $user_id,
            ),
            'userChecking' => false,
            'setUniqueResult' => true
        );

        return $sql;
    }

    public function getBackendList() {
        $sql = array(
            'user_profile' => array(
                'username' => 'usr.username',
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                'user_country_id' => 'usr.country'
            ),
            'membership_packages' => array(
                'package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority' => 'pkg.priority'
            ),
            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'selling_categories' => 'cpy.selling_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'),
            'countries' => array('country_name' => 'tld.value',
                'country_code' => 'tld.code'),
            'b2b_category' => array(
                'category_name' => 'cat.name',
                'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'
            ),
            'b2b_group' => array(
                'images_path' => 'grp.file_path_selling_images',
                'review_id' => 'grp.review_id'
            ),
            'userChecking' => true,
            'setUniqueResult' => true
        );

        return $sql;
    }

}
