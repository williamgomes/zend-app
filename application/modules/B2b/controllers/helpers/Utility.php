<?php
class B2b_Controller_Helper_Utility
{

	public function isUserProfile ($user_id){

		$profileTbl = new B2b_Model_DbTable_CompanyProfile ();
		$userProfile = $profileTbl->isUserExits($user_id);

		if (count($userProfile) < 1 || empty ($userProfile['company_id']) || intval($userProfile['company_id']) < 1) {
		    return null;
		}

		else {
			return $userProfile ;
		}

	}

}