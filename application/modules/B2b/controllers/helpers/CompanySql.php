<?php
class B2b_Controller_Helper_CompanySql
{
	public function getB2bInquiryCompanyProfileList ()
    {
        $sql = array(
                'b2b_company_profile' => array(
                        'cp_company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',                       
                        'cp_phone' => "CONCAT(cpy.phone, '-', cpy.phone_part2, '-', cpy.phone_part3)",
                        'cp_mobile' => "CONCAT(cpy.mobile, '-', cpy.mobile_part2, '-', cpy.mobile_part3)",
                        'cp_fax' => "CONCAT(cpy.fax, '-', cpy.fax_part2, '-', cpy.fax_part3)",
                		'website' => 'cpy.website',
						'cp_street' => 'cpy.street',
                        'cp_city' => 'cpy.city',
                		'cp_postal_code' => 'cpy.postal_code',
						'profile_since' => 'cpy.added_on'),
                'user_profile' => array(
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
            			'usr_phone' => 'usr.phone',
            			'usr_mobile' => 'usr.mobile',
            			'usr_fax' => 'usr.fax',
            			'usr_website' => 'usr.website',
            			'usr_address' => 'usr.address',
						'usr_state' => 'usr.state',
						'usr_city' => 'usr.city',
            			'usr_postcode' => 'usr.postalCode',
						'usr_company' => 'usr.companyName',
						'usr_email' => 'usr.username'),  
				'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),               
                'userChecking' => false);
        return $sql;
    }
	
    public function getB2bPremiumCompanyProfileList ($arr = array())
    {
        $sql = array(
                'b2b_company_profile' => array(
						'id' => 'cpy.id',
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'profile_image_primary' => 'cpy.profile_image_primary',
                        'company_information' => 'cpy.company_information',
                        'added_on' => 'cpy.added_on'),
                'user_profile' => array(
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'membership_packages' => array('package_name' => 'pid.name',
                        'package_logo' => 'pid.b2b_images',
                        'priority_flag' => 'pid.priority_flag',
                        'priority' => 'pid.priority',
                        'package_price' => 'pid.price'),
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_profile_image',
                        'review_id' => 'grp.review_id'),
                'userChecking' => false,
                'dataLimit' => $arr['dataLimit']);
        return $sql;
    }
    public function getBackendList ()
    {
        $sql = array(
                'b2b_company_profile' => array('id' => 'cpy.id',
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'profile_image_primary' => 'cpy.profile_image_primary',
                        'trade_type' => 'cpy.trade_type',
                        'user_id' => 'cpy.user_id', 'active' => 'cpy.active',
                        'keywords' => 'cpy.keywords',
                        'trade_type' => 'cpy.trade_type',
                        'added_on' => 'cpy.added_on'),
                'user_profile' => array(
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'user_country_id' => 'usr.country',
                        'username' => 'usr.username'),
						
				'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),
                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),
						
                'countries' => array('country' => 'tld.value',
                        'code' => 'tld.code'),
                'membership_packages' => array('package_name' => 'pid.name'),
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_profile_image',
                        'review_id' => 'grp.review_id'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'userChecking' => true, 'setUniqueResult' => true);
        return $sql;
    }
    public function getFontendCompanyList ()
    {
        $sql = array(
                'b2b_company_profile' => array('id', 'user_id', 'company_name',
                        'seo_title', 'profile_image_primary', 'profile_image',
                        'establishing_year', 'compliances',
                        'company_information', 'trade_type', 'compliances',
                        'businesstype_id', 'seo_title', 'company_information',
                        'keywords', 'added_on'),
                'user_profile' => array('state' => 'usr.state',
                        'city' => 'usr.city', 'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),
                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),
                'membership_packages' => array('package_name' => 'pid.name',
                        'package_logo' => 'pid.b2b_images',
                        'priority_flag' => 'pid.priority_flag',
                        'priority' => 'pid.priority',
                        'directory' => 'pid.directory'),
                'b2b_compliance_mapper' => array(
                        'compliance_id' => 'ucm.compliance_id'),
                'b2b_compliances' => array(
                        'compliance_name' => 'cpl.compliances',
                        'compliance_img' => 'cpl.image'),
                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),
                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_profile_image',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'where' => array('usr.status = ?' => '1',
                        'cpy.active = ?' => '1'),
                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
	
	public function getFontendCompanyDirectoryList ()
    {
        $sql = array(
                'b2b_company_profile' => array('id', 'user_id', 'company_name',
                        'seo_title', 'profile_image_primary', 'profile_image',
                        'establishing_year', 'compliances',
                        'company_information', 'trade_type', 'compliances',
                        'businesstype_id', 'seo_title', 'company_information',
                        'keywords', 'added_on'),
                'user_profile' => array('state' => 'usr.state',
                        'city' => 'usr.city', 'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),
                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),
                'membership_packages' => array('package_name' => 'pid.name',
                        'package_logo' => 'pid.b2b_images',
                        'priority_flag' => 'pid.priority_flag',
                        'priority' => 'pid.priority',
                        'directory' => 'pid.directory'),
                'b2b_compliance_mapper' => array(
                        'compliance_id' => 'ucm.compliance_id'),
                'b2b_compliances' => array(
                        'compliance_name' => 'cpl.compliances',
                        'compliance_img' => 'cpl.image'),
                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),
                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_profile_image',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'where' => array('usr.status = ?' => '1',
                        'cpy.active = ?' => '1', 'pid.directory = ?' => 'YES'),
                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
	
    public function getFontendsuppliersList ()
    {
        $sql = array(
                'b2b_company_profile' => array('id', 'user_id', 'company_name',
                        'seo_title', 'profile_image_primary', 'profile_image',
                        'establishing_year', 'compliances',
                        'company_information', 'trade_type', 'compliances',
                        'businesstype_id', 'seo_title', 'company_information',
                        'keywords', 'added_on'),
                'user_profile' => array('state' => 'usr.state',
                        'city' => 'usr.city', 'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),
                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),
                'membership_packages' => array('package_name' => 'pid.name',
                        'package_logo' => 'pid.b2b_images',
                        'priority_flag' => 'pid.priority_flag',
                        'priority' => 'pid.priority'),
                'b2b_compliance_mapper' => array(
                        'compliance_id' => 'ucm.compliance_id'),
                'b2b_compliances' => array(
                        'compliance_name' => 'cpl.compliances',
                        'compliance_img' => 'cpl.image'),
                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),
                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),
                'b2b_group' => array(
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'images_path' => 'grp.file_path_profile_image'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'where' => array('usr.status = ?' => '1',
                        'cpy.trade_type != ?' => '1'), 'userChecking' => false,
                'setUniqueResult' => true);
        return $sql;
    }
    public function getFontendBuyerList ()
    {
        $sql = array(
                'b2b_company_profile' => array('id', 'user_id', 'company_name',
                        'seo_title', 'profile_image_primary', 'profile_image',
                        'establishing_year', 'compliances',
                        'company_information', 'trade_type', 'compliances',
                        'businesstype_id', 'seo_title', 'company_information',
                        'keywords', 'added_on'),
                'user_profile' => array('state' => 'usr.state',
                        'city' => 'usr.city', 'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),
                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),
                'membership_packages' => array('package_name' => 'pid.name',
                        'package_logo' => 'pid.b2b_images',
                        'priority_flag' => 'pid.priority_flag',
                        'priority' => 'pid.priority'),
                'b2b_compliance_mapper' => array(
                        'compliance_id' => 'ucm.compliance_id'),
                'b2b_compliances' => array(
                        'compliance_name' => 'cpl.compliances',
                        'compliance_img' => 'cpl.image'),
                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),
                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),
                'b2b_group' => array(
                        'images_path' => 'grp.file_path_profile_image',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),
                'where' => array('usr.status = ?' => '1',
                        'cpy.active = ?' => '1', 'cpy.trade_type != ?' => '2'),
                'userChecking' => false, 'setUniqueResult' => true);
        return $sql;
    }
}