<?php

class B2b_Controller_Helper_BuyingSql
{

    public function getB2bContentBuyingLatestList ($arr = array())
    {
        $sql = array(
                'b2b_buying_leads' => array('name' => 'buy.name',
                        'id' => 'buy.id', 'seo_title' => 'buy.seo_title',
                        'primary_file_field' => 'buy.primary_file_field',
                        'others_images' => 'buy.others_images',
                        'price_currency' => 'buy.price_currency',
                        'price' => 'buy.price',
                        'description' => 'buy.description',
                        'added_on' => 'buy.added_on',
                        '(SELECT SUM(vts.vote_value) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vts WHERE buy.id  = vts.table_id) AS total_votes',
                                '(SELECT COUNT(vtc.id) FROM ' .
                                 Zend_Registry::get('dbPrefix') .
                                 'vote_voting as vtc WHERE buy.id  = vtc.table_id) AS voter_count'),
                'user_profile' => array(
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"),
                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'company_title' => 'cpy.seo_title',
                        'cpy_active' => 'cpy.active'),
                'b2b_category' => array('category_name' => 'cat.name',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'priority' => 'pkg.priority'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
                        'review_id' => 'grp.review_id'),

                'userChecking' => false,

                'dataLimit' => $arr['dataLimit']);

        return $sql;
    }

    public function getFontendList ()
    {
        $sql = array(
                'b2b_buying_leads' => array('id', 'user_id', 'name',
                        'validity', 'category', 'description', 'seo_title',
                        'primary_file_field', 'others_images', 'buying_capacity',
                        'max_buying_unit', 'shipping', 'quantity',
                        'qty_per_unit', 'displayble', 'payment_type',
                        'specifications', 'delivery', 'delivery_leadtime',
                        'price_currency', 'price', 'price_per_unit', 'category',
                        'added_on', 'featured', 'keyword', 'keyword_1',
                        'keyword_2', 'keyword_3'),

                'user_profile' => array(
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
						'status' => 'usr.status',
                        'state' => 'usr.state',
						'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority',
                        'directory' => 'pkg.directory'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'buying_categories' => 'cpy.buying_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),


                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),

        		// DO NOT ADD  'usr.status = ?' => '1',
        		// because we allow user to post buying lead without registration.
        		// adding 'usr.status = ?' => '1',  on where clause will never bring those buyingleads which
        		// is posted without registraiton.
                'where' => array('buy.displayble = ?' => '1',
                				 'usr.status = ?' => '1',
                				 'cpy.active = ?' => '1'),


                'userChecking' => false, 'setUniqueResult' => true);

        return $sql;
    }

    public function getFontendDirectoryList ()
    {
        $sql = array(
                'b2b_buying_leads' => array('id', 'user_id', 'name',
                        'validity', 'category', 'description', 'seo_title',
                        'primary_file_field', 'others_images', 'buying_capacity',
                        'max_buying_unit', 'shipping', 'quantity',
                        'qty_per_unit', 'displayble', 'payment_type',
                        'specifications', 'delivery', 'delivery_leadtime',
                        'price_currency', 'price', 'price_per_unit', 'category',
                        'added_on', 'featured', 'keyword', 'keyword_1',
                        'keyword_2', 'keyword_3'),

                'user_profile' => array(
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
						'status' => 'usr.status',
                        'state' => 'usr.state',
						'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority',
                        'directory' => 'pkg.directory'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'buying_categories' => 'cpy.buying_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),


                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),

        		// DO NOT ADD  'usr.status = ?' => '1',
        		// because we allow user to post buying lead without registration.
        		// adding 'usr.status = ?' => '1',  on where clause will never bring those buyingleads which
        		// is posted without registraiton.
                'where' => array('buy.displayble = ?' => '1',
                				 'usr.status = ?' => '1',
                				 'cpy.active = ?' => '1',
                				 'pkg.directory = ?' => 'YES'),


                'userChecking' => false, 'setUniqueResult' => true);

        return $sql;
    }

	public function getFontendUnRegisteredList ()
    {
        $sql = array(
                'b2b_buying_leads' => array('*'),

                'user_profile' => array(
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
						'status' => 'usr.status',
                        'state' => 'usr.state',
						'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'b2b_company_profile' => array(

                        'buying_categories' => 'cpy.buying_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),


                'b2b_category' => array('category_name' => 'cat.name',
                        'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),

        		// DO NOT ADD  'usr.status = ?' => '1',
        		// because we allow user to post buying lead without registration.
        		// adding 'usr.status = ?' => '1',  on where clause will never bring those buyingleads which
        		// is posted without registraiton.
                'where' => array('buy.user_id = ?' => '0'),


                'userChecking' => false, 'setUniqueResult' => true);

        return $sql;

    }

    public function getCompanyFontendList ($user_id)
    {
        $sql = array(

                'b2b_buying_leads' => array('id', 'user_id', 'name',
                        'validity', 'category', 'description', 'seo_title',
                        'primary_file_field', 'others_images', 'buying_capacity',
                        'max_buying_unit', 'shipping', 'quantity',
                        'qty_per_unit', 'displayble', 'payment_type',
                        'specifications', 'delivery', 'delivery_leadtime',
                        'price_currency', 'price', 'price_per_unit', 'category',
                        'added_on', 'keyword', 'keyword_1', 'keyword_2',
                        'keyword_3'),

                'user_profile' => array('status' => 'usr.status',
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'priority' => 'pkg.priority'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'buying_categories' => 'cpy.buying_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),


                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
                        'review_id' => 'grp.review_id'),

                'where' => array('usr.status = ?' => '1',
                        'buy.displayble = ?' => '1', 'cpy.active = ?' => '1',
                        'buy.user_id = ?' => $user_id),

                'userChecking' => true, 'setUniqueResult' => true);

        return $sql;
    }

	public function getFontendLastMinDeal ()
    {
        $sql = array(
                'b2b_buying_leads' => array('id', 'user_id', 'name',
                        'validity', 'category', 'description', 'seo_title',
                        'primary_file_field', 'others_images', 'buying_capacity',
                        'max_buying_unit', 'shipping', 'quantity',
                        'qty_per_unit', 'displayble', 'payment_type',
                        'specifications', 'delivery', 'delivery_leadtime',
                        'price_currency', 'price', 'price_per_unit', 'category',
                        'added_on', 'featured', 'keyword', 'keyword_1',
                        'keyword_2', 'keyword_3',
						'(TIMESTAMPDIFF(HOUR,now(), buy.validity)) AS deal_hour_num',
						'(DATEDIFF(buy.validity, now())) AS deal_day_num',
						'(period_diff(date_format(buy.validity, "%Y%m"), date_format(now(), "%Y%m"))) AS deal_month_num',
						'(period_diff(date_format(buy.validity, "%Y"), date_format(now(), "%Y"))) AS deal_year_num'),

                'user_profile' => array('status' => 'usr.status',
						'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'package_id' => 'usr.package_id',
                        'user_country_id' => 'usr.country'),

                'countries' => array('country_name' => 'tld.value',
                        'country_code' => 'tld.code'),

                'membership_packages' => array('package_name' => 'pkg.name',
                        'package_logo' => 'pkg.b2b_images',
                        'priority_flag' => 'pkg.priority_flag',
                        'is_feedback' => 'pkg.feedback',
                        'is_voting' => 'pkg.voting',
                        'is_profile' => 'pkg.profile',
                        'is_telephone' => 'pkg.telephone',
                        'is_website' => 'pkg.website',
                        'eCommerce' => 'pkg.eCommerce',
                        'priority' => 'pkg.priority'),

                'b2b_company_profile' => array(
                        'company_name' => 'cpy.company_name',
                        'buying_categories' => 'cpy.buying_categories',
                        'registered_in' => 'cpy.registered_in',
                        'logo' => 'cpy.profile_image_primary',
                        'company_title' => 'cpy.seo_title',
                        'phone' => 'cpy.phone',
                        'phone_part2' => 'cpy.phone_part2',
                        'phone_part3' => 'cpy.phone_part3',
                        'establishing_year' => 'cpy.establishing_year',
                        'company_keywords' => 'cpy.keywords'),

                'b2b_biztype_mapper' => array(
                        'business_type_id' => 'biz.biztype_id'),

                'b2b_business_type' => array(
                        'business_type' => 'typ.business_type'),


                'b2b_category' => array('category_name' => 'cat.name',
                        'category_name' => 'cat.name', 'category_id' => 'cat.id',
                        'parent_id' => 'cat.parent_id',
                        'category_isShowProfile' => 'cat.isShowProfile'),

                'b2b_group' => array(
                        'images_path' => 'grp.file_path_buying_images',
						'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                        'review_id' => 'grp.review_id'),

                'where' => array('usr.status = ?' => '1',
                        'buy.displayble = ?' => '1', 'cpy.active = ?' => '1'),

                'userChecking' => false, 'setUniqueResult' => true);

        return $sql;
    }


    public function getBackendList ()
    {
        $sql = array(

            'user_profile' => array('status' => 'usr.status',
				'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                'state' => 'usr.state', 'city' => 'usr.city',
                'package_id' => 'usr.package_id',
                'user_country_id' => 'usr.country'),

            'countries' => array('country_name' => 'tld.value',
                'country_code' => 'tld.code'),

            'membership_packages' => array('package_name' => 'pkg.name',
                'package_logo' => 'pkg.b2b_images',
                'priority_flag' => 'pkg.priority_flag',
                'is_feedback' => 'pkg.feedback',
                'is_voting' => 'pkg.voting',
                'is_profile' => 'pkg.profile',
                'is_telephone' => 'pkg.telephone',
                'is_website' => 'pkg.website',
                'priority' => 'pkg.priority'),

            'b2b_company_profile' => array(
                'company_name' => 'cpy.company_name',
                'buying_categories' => 'cpy.buying_categories',
                'registered_in' => 'cpy.registered_in',
                'logo' => 'cpy.profile_image_primary',
                'company_title' => 'cpy.seo_title',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'establishing_year' => 'cpy.establishing_year',
                'company_keywords' => 'cpy.keywords'),

            'b2b_biztype_mapper' => array(
                'business_type_id' => 'biz.biztype_id'),

            'b2b_business_type' => array(
                'business_type' => 'typ.business_type'),


            'b2b_category' => array('category_name' => 'cat.name',
                'category_name' => 'cat.name', 'category_id' => 'cat.id',
                'parent_id' => 'cat.parent_id',
                'category_isShowProfile' => 'cat.isShowProfile'),

            'b2b_group' => array(
                'images_path' => 'grp.file_path_buying_images',
                'file_path_b2b_images'	=>	'grp.file_path_b2b_images',
                'review_id' => 'grp.review_id'),

            'userChecking' => true, 'setUniqueResult' => true);

        return $sql;
    }

}
