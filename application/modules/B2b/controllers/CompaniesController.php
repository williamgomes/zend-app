<?php

class B2b_CompaniesController extends Zend_Controller_Action {
	private $_controllerCache;
	private $_auth_obj;
	public function init() {
		/* Initialize action controller here */
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity () : '';
		$this->view->auth = $auth;
		// Initialize Cache
		$cache = new Eicra_View_Helper_Cache ();
		$this->_controllerCache = $cache->getCache ();
	}
	public function preDispatch() {
		$this->_helper->layout->setLayout ( 'layout' );
		$this->_helper->layout->setLayoutPath ( MODULE_PATH . '/Administrator/layouts/scripts' );
		$translator = Zend_Registry::get ( 'translator' );
		$this->view->assign ( 'translator', $translator );
		$this->view->setEscape ( 'stripslashes' );
		$getModule = $this->_request->getModuleName ();
		$this->view->assign ( 'getModule', $getModule );
		$getAction = $this->_request->getActionName ();
		$this->view->assign ( 'getAction', $getAction );
		$getController = $this->_request->getControllerName ();
		$this->view->assign ( 'getController', $getController );
		if ($getAction != 'uploadfile') {
			$url = Zend_Registry::get ( 'config' )->eicra->params->domain . $this->view->baseUrl () . '/Administrator/login';
			Eicra_Global_Variable::checkSession ( $this->_response, $url );
			/* Check Module License */
			$modules_license = new Administrator_Controller_Helper_ModuleLoader ();
			$modules_license->getModulesLicenseMsg ( $this->_request->getModuleName () );
		}
	}
	public function deleteAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		$json_arr = null;
		if ($this->_request->isPost ()) {
			$permission = new B2b_View_Helper_Allow ();
			if ($permission->allow ()) {
				$id = $this->_request->getPost ( 'id' );
				$companyMapper = new B2b_Model_CompanyprofileMapper ();
				try {
					$result = $companyMapper->delete ( $id );
					if ($result ['status'] == 'ok') {
						$msg = $translator->translator ( "b2b_common_delete_success" );
						$json_arr = array (
								'status' => 'ok',
								'msg' => $msg
						);
					} else {
						$msg = $translator->translator ( "b2b_common_delete_failed" );
						$json_arr = array (
								'status' => 'err',
								'msg' => $msg . " " . $result ['msg']
						);
					}
				} catch ( Exception $e ) {
					$json_arr = array (
							'status' => 'err',
							'msg' => $e->getMessage ()
					);
				}
			} else {
				$msg = $translator->translator ( "page_access_restrictions" );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
				$res_value = Zend_Json::encode ( $json_arr );
				$this->_response->setBody ( $res_value );
			}
		} else {
			$msg = $translator->translator ( 'b2b_common_delete_failed' );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function deleteallAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		if ($this->_request->isPost ()) {
			$id_str = $this->_request->getPost ( 'id_st' );
			$permission = new B2b_View_Helper_Allow ();
			if ($permission->allow ()) {
				if (! empty ( $id_str )) {
					$id_arr = explode ( ',', $id_str );
					foreach ( $id_arr as $id ) {
						try {
							$companyMapper = new B2b_Model_CompanyprofileMapper ();
							$result = $companyMapper->delete ( $id );
							if ($result ['status'] == 'ok') {
								$msg = $translator->translator ( "b2b_common_delete_success" );
								$json_arr = array (
										'status' => 'ok',
										'msg' => $msg
								);
							} else {
								$msg = $translator->translator ( "b2b_common_delete_failed" );
								$json_arr = array (
										'status' => 'err',
										'msg' => $msg . " " . $result ['msg']
								);
							}
						} catch ( Exception $e ) {
							$json_arr = array (
									'status' => 'err',
									'msg' => $e->getMessage () . ' ' . $id
							);
						}
					}
				} else {
					$msg = $translator->translator ( "category_selected_err" );
					$json_arr = array (
							'status' => 'err',
							'msg' => $msg
					);
				}
			} else {
				$msg = $translator->translator ( 'page_delete_perm' );
				$json_arr = array (
						'status' => 'err',
						'msg' => $msg
				);
			}
		} else {
			$msg = $translator->translator ( 'category_list_delete_err' );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function listAction() {
		$posted_data = $this->_request->getParams ();
		$this->view->assign ( 'posted_data', $posted_data );
		if ($this->_request->isPost ()) {
			try {
				$approve = $this->_request->getParam ( 'approve' );
				$displayble = $this->_request->getParam ( 'displayble' );
				$this->_helper->layout->disableLayout ();
				$this->_helper->viewRenderer->setNoRender ();
				if ($displayble != null && $displayble != '') {
					$posted_data ['filter'] ['filters'] [] = array (
							'field' => 'displayble',
							'operator' => 'eq',
							'value' => $displayble
					);
				}
				$pageNumber = ($this->_request->getPost ( 'page' )) ? $this->_request->getPost ( 'page' ) : $this->_request->getParam ( 'page' );
				$getViewPageNum = $this->_request->getParam ( 'pageSize' );
				$frontendRoute = 'adminrout';
				$posted_data ['browser_url'] = $this->view->url ( array (
						'module' => $this->view->getModule,
						'controller' => $this->view->getController,
						'action' => $this->view->getAction,
						'approve' => $approve,
						'displayble' => $displayble,
						'page' => ($pageNumber == '1' || empty ( $pageNumber )) ? null : $pageNumber
				), $frontendRoute, true );
				$viewPageNumSes = Eicra_Global_Variable::getSession ()->viewPageNum;
				$viewPageNum = (! empty ( $getViewPageNum )) ? $getViewPageNum : $viewPageNumSes;
				Eicra_Global_Variable::getSession ()->viewPageNum = $viewPageNum;
				$encode_params = Zend_Json::encode ( $posted_data );
				$encode_auth_obj = Zend_Json::encode ( $this->_auth_obj );
				$uniq_id = md5 ( preg_replace ( '/[^a-zA-Z0-9_]/', '_', $this->view->url () . '_' . $encode_params . '_' . $encode_auth_obj ) );
				if (($view_datas = $this->_controllerCache->load ( $uniq_id )) === false) {
					$companyMapper = new B2b_Model_CompanyprofileMapper ();
					$companySql = new B2b_Controller_Helper_CompanySql ();
					$tableColumns = $companySql->getBackendList ();
					$view_datas = $companyMapper->fetchAll ( $pageNumber, $approve, $posted_data, $tableColumns );
					
					$this->_controllerCache->save ( $view_datas, $uniq_id );
				}
				$data_result = array ();
				$total = 0;
				
				if ($view_datas) {
					$key = 0;
					foreach ( $view_datas as $entry ) {
						$entry_arr = (! is_array ( $entry )) ? $entry->toArray () : $entry;
						$entry_arr ['id_format'] = $this->view->numbers($entry_arr ['id']);
						
						$entry_arr ['name']  = $this->view->escape($entry_arr ['name']);
						
						$entry_arr ['full_name']  = $this->view->escape($entry_arr ['full_name']);
						$entry_arr ['publish_status_username'] = str_replace ( '_', '-', $entry_arr ['name'] );
						$img_thumb_arr = $entry_arr ['b2b_images'];
						$entry_arr ['primary_file_field_format'] = $entry_arr ['b2b_images'];
						$entry_arr ['added_on_format'] = date ( 'Y-m-d h:i:s A', strtotime ( $entry_arr ['added_on'] ) );
						$entry_arr ['added_on_format_lang'] = $this->view->numbers($entry_arr ['added_on_format']);
						$entry_arr ['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr ['user_id']) ? true : false;
						$data_result [$key] = $entry_arr;
						$key ++;
					}
					$total = $view_datas->getTotalItemCount ();
				}
				$json_arr = array (
						'status' => 'ok',
						'data_result' => $data_result,
						'total' => $total,
						'posted_data' => $posted_data
				);
			} catch ( Exception $e ) {
				$json_arr = array (
						'status' => 'err',
						'data_result' => '',
						'msg' => $e->getMessage ()
				);
			}
			// Convert To JSON ARRAY
			$res_value = Zend_Json::encode ( $json_arr );
			$this->_response->setBody ( $res_value );
		}
	}
	
	public function editAction() 
	{
		$user_id = $this->_request->getParam ( 'user_id' );
		$id = $this->_request->getParam ( 'id' );
		
		$profileForm = new B2b_Form_CompanyProfileForm ();
		$translator = $this->view->translator;
		
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		
		$resultSet = null;
		$json_arr = null;
		
		$auth = Zend_Auth::getInstance ();
		$login_id = ($auth->hasIdentity ()) ? $auth->getIdentity ()->user_id : '';
		
		if ($this->_request->isPost ()) 
		{
			$this->_helper->viewRenderer->setNoRender ();
			$this->_helper->layout->disableLayout ();
			$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
			$permission = new B2b_View_Helper_Allow ();
			try {
				if ($profileForm->isValid ( $this->_request->getPost () )) {
					$companyProfile = new B2b_Model_Companyprofile ( $profileForm->getValues () );
					$user_id  = $profileForm->getValue('user_id') ;
					if ($permission->allow ()) {
						$isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $login_id) ? true : false;
						
						if(empty($isEditable)){
							$msg = $translator->translator("page_access_restrictions");
                            $json_arr = array(
                                'status' => 'err',
                                'msg' => $msg   );
						}
						else if (! empty ( $id ) && ! empty ( $user_id ) ) {
														
							$plan = new B2b_Model_DbTable_Package ();
							$planinfo = $plan->getIDByUser ( $user_id );
							$plan_id = $planinfo ['id'];
							if ($planinfo) {
								$companyProfile->setPlan_id ( $plan_id );
							} else {
								$companyProfile->setPlan_id ( 0 );
							}
							$_active = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';
							$_displayble = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';
							$companyProfile->setActive ( $_active );
							$companyProfile->setDisplayble ( '1' );
							$result = $companyProfile->saveProfile ();
							
					
							if ($result ['status'] == 'ok') {
								$msg = $translator->translator ( "page_save_success" );
								$json_arr = array (
										'status' => 'ok',
										'msg' => $msg
								);
							}
							else {
								$msg = $translator->translator ( "page_save_err" );
								$json_arr = array (
										'status' => 'err',
										'msg' => $msg . " " . $result ['msg']
								);
							}
						} else {
							$msg = $translator->translator ( "b2b_common_invalid_edit" );
							$json_arr = array (
									'status' => 'err',
									'msg' => $id. $msg  .$user_id
							);
						}
					} else {
						$msg = $translator->translator ( "page_save_err" );
						$json_arr = array (
								'status' => 'err',
								'msg' => $msg . " " . $result ['msg']
						);
					}
				} else {
					$validatorMsg = $profileForm->getMessages ();
					$vMsg = array ();
					$i = 0;
					foreach ( $validatorMsg as $key => $errType ) {
						foreach ( $errType as $errkey => $value ) {
							$vMsg [$i] = array (
									'key' => $key,
									'errKey' => $errkey,
									'value' => $value
							);
							$i ++;
						}
					}
					$json_arr = array (
							'status' => 'errV',
							'msg' => $vMsg
					);
				}
			} catch ( Exception $e ) {
				$json_arr = array (
						'status' => 'err',
						'msg' => $e->getMessage ()
				);
			}
			$res_value = Zend_Json::encode ( $json_arr );
			$this->_response->setBody ( $res_value );
		} 
		else 
		{
			if ($user_id && $id) 
			{
				$dataModel = new B2b_Model_CompanyprofileMapper ();
				$resultSet = $dataModel->getDefaultProfile ( $user_id );
				$this->companyUploaderSettings ( $resultSet );
				if (is_array ( $resultSet ) && (count ( $resultSet ) > 1)) {
					$profileForm->populate ( $resultSet );
					$language = explode ( ',', $resultSet ['language'] );
					$certification = explode ( ',', $resultSet ['certification'] );
					$main_markets = explode ( ',', $resultSet ['main_markets'] );
					$delivery_terms = explode ( ',', $resultSet ['delivery_terms'] );
					$payment_currency = explode ( ',', $resultSet ['payment_currency'] );
					$payment_type = explode ( ',', $resultSet ['payment_type'] );
					$profileForm->language->setValue ( $language );
					$profileForm->certification->setValue ( $certification );
					$profileForm->main_markets->setValue ( $main_markets );
					$profileForm->delivery_terms->setValue ( $delivery_terms );
					$profileForm->payment_currency->setValue ( $payment_currency );
					$profileForm->compliances->setValue ( $resultSet ['compliances'] );
					$profileForm->payment_type->setValue ( $payment_type );
					$profileForm->annual_revenue->setValue ( $resultSet ['annual_revenue'] );
					$profileForm->number_of_employees->setValue ( $resultSet ['number_of_employees'] );
					$profileForm->office_size->setValue ( $resultSet ['office_size'] );
					$profileForm->country->setValue ( $resultSet ['country'] );
					$profileForm->businesstype_id->setValue ( $resultSet ['businesstype_id'] );
					$profileForm->trade_type->setValue ( $resultSet ['trade_type'] );
					$profileForm->annual_sales->setValue ( $resultSet ['annual_sales'] );
			
					if (! empty ( $resultSet ['category_id'] )) {
						$category_db = new B2b_Model_DbTable_Category ();
						$category_info = $category_db->getCategoryById ( $resultSet ['category_id'] );
						$this->view->assign ( 'category_info', $category_info );
					}
				}
				else {
					$msg = $translator->translator ( "b2b_no_profile_found_404" );
					$this->view->assign ( 'No_Profile_Found', $msg );
					$this->view->assign ( 'profileForm', $profileForm );
					return;
				}
			}
			else {
				$msg = $translator->translator ( "b2b_no_profile_found_404" );
					$this->view->assign ( 'No_Profile_Found', $msg );
					$this->view->assign ( 'profileForm', $profileForm );
					return;
			}
			$noprofile = $this->_request->getParam ( 'noprofile' );
			$this->view->assign ( 'profileForm', $profileForm );
			$this->view->assign ( 'noprofile', $noprofile );
			$userModel = new Members_Model_DbTable_MemberList ();
			$userInfo = $userModel->getMemberInfo ( $user_id );
			$this->view->assign ( 'country', $userInfo ['country_name'] );
		}
	}
	public function publishAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		if ($this->_request->isPost ()) {
			$id = $this->_request->getPost ( 'id' );
			$productName = $this->_request->getPost ( 'name' );
			$paction = $this->_request->getPost ( 'paction' );
			$data = array ();
			$json_arr = null;
			$active = 0;
			switch ($paction) {
				case 'publish' :
					$active = '1';
					break;
				case 'unpublish' :
					$active = '0';
					break;
			}
			try {
				$data = array (
						'active' => $active
				);
				$companyMapper = new B2b_Model_CompanyprofileMapper ();
				$json_arr = $companyMapper->updateStatus ( $data, $id );
				$json_arr ['active'] = $active;
			} catch ( Exception $e ) {
				$json_arr = array (
						'status' => 'err',
						'msg' => $e->getMessage (),
						'active' => $active
				);
			}
			$member = new B2b_Model_DbTable_CompanyProfile ();
			$memberInfo = $member->getUserByID($id);
			$global_conf = Zend_Registry::get('global_conf');
			$datas = array(
			        'user' => $memberInfo[0]['full_name'],
			        'my_company' => $global_conf['site_name'],
			        'site_name' => $global_conf['site_name'],
			        'site_url' => $global_conf['site_url'],
			);
			$mailSpooler = new Members_Controller_Helper_Registers ();
			if ($active) {
    			$headers  = array(
    			        'email_template_set' => '45',
    			        'email_to' => $memberInfo['username']
    			);
    			$result = $mailSpooler->sendGeneralMail($datas,$headers);
			}
			else {
			    $headers  = array(
			            'email_template_set' => '46',
			            'email_to' => $memberInfo['username']
			    );
			    $result = $mailSpooler->sendGeneralMail($datas,$headers);
			}
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function publishallAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		if ($this->_request->isPost ()) {
			$id_str = $this->_request->getPost ( 'id_st' );
			$paction = $this->_request->getPost ( 'paction' );
			$json_arr = null;
			$active = 0;
			switch ($paction) {
				case 'publish' :
					$active = '1';
					break;
				case 'unpublish' :
					$active = '0';
					break;
			}
			if (! empty ( $id_str )) {
				$id_arr = explode ( ',', $id_str );
				$data = array (
						'active' => $active
				);
				foreach ( $id_arr as $id ) {
					try {
						$companyMapper = new B2b_Model_CompanyprofileMapper ();
						$json_arr = $companyMapper->updateStatus ( $data, $id );
						$json_arr ['active'] = $active;
					} catch ( Exception $e ) {
						$json_arr = array (
								'status' => 'err',
								'msg' => $e->getMessage (),
								'active' => $active
						);
					}
				}
			}
		} else {
			$msg = $translator->translator ( "page_selected_err" );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	public function activateallAction() {
		$this->_controllerCache->clean ( Zend_Cache::CLEANING_MODE_ALL );
		$this->_helper->viewRenderer->setNoRender ();
		$this->_helper->layout->disableLayout ();
		$translator = Zend_Registry::get ( 'translator' );
		if ($this->_request->isPost ()) {
			$id_str = $this->_request->getPost ( 'id_st' );
			$paction = $this->_request->getPost ( 'paction' );
			$json_arr = null;
			$active = 0;
			switch ($paction) {
				case 'publish' :
					$active = '1';
					break;
				case 'unpublish' :
					$active = '0';
					break;
			}
			if (! empty ( $id_str )) {
				$id_arr = explode ( ',', $id_str );
				$data = array (
						'active' => $active
				);
				foreach ( $id_arr as $id ) {
					try {
						$companyMapper = new B2b_Model_CompanyprofileMapper ();
						$json_arr = $companyMapper->updateStatus ( $data, $id );
						$json_arr ['active'] = $active;
					} catch ( Exception $e ) {
						$json_arr = array (
								'status' => 'err',
								'msg' => $e->getMessage (),
								'active' => $active
						);
					}
				}
			}
		} else {
			$msg = $translator->translator ( "page_selected_err" );
			$json_arr = array (
					'status' => 'err',
					'msg' => $msg
			);
		}
		// Convert To JSON ARRAY
		$res_value = Zend_Json::encode ( $json_arr );
		$this->_response->setBody ( $res_value );
	}
	private function companyUploaderSettings($info) {
		$group_db = new B2b_Model_DbTable_Group ();
		$info ['group_id'] = ($info ['group_id']) ? $info ['group_id'] : 1;
		$group_info = $group_db->getGroupName ( $info ['group_id'] );
		$param_fields = array (
				'table_name' => 'b2b_group',
				'primary_id_field' => 'id',
				'primary_id_field_value' => $info ['group_id'],
				'file_path_field' => '',
				'file_extension_field' => 'file_type',
				'file_max_size_field' => 'file_size_max'
		);
		$portfolio_model = new Portfolio_Model_Portfolio ( $param_fields );
		$requested_data = $portfolio_model->getRequestedData ();
		$settings_info = $group_info;
		/**
		 * ***************************************************For
		 * Primary************************************************
		 */
		$primary_requested_data = $requested_data;
		$primary_requested_data ['file_path_field'] = 'file_path_profile_image';
		$this->view->assign ( 'primary_settings_info', array_merge ( $primary_requested_data, $settings_info ) );
		$this->view->assign ( 'primary_settings_json_info', Zend_Json::encode ( $this->view->primary_settings_info ) );
	}	
}
