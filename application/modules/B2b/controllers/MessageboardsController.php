<?php
class B2b_MessageboardsController extends Zend_Controller_Action
{
    private $_translator;
    private $_controllerCache;
    private $_auth_obj;
    public function init ()
    {
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() .
                 '/Frontend-Login';
        Eicra_Global_Variable::checkSession($this->_response, $url);
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
    }
    public function preDispatch ()
    {	
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;	
    }

    public function activeAction()
    {

    }

    public function inactiveAction()
    {

    }

    public function flagAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost())
        {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()){

                $id = $this->_request->getPost('id');
                $flag = $this->_request->getPost('paction');
                $inquiryMapper = new B2b_Model_InquiryMapper();

                try {
                    $result = $inquiryMapper->flag($id, $flag);
                    if($result['status'] == 'ok')
                    {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array('status' => 'ok','msg' => $msg, 'flag' => $flag);
                    }
                    else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg'], 'flag' => $flag);
                    }
                }
                catch (Exception $e)
                {
                    $json_arr = array('status' => 'err','msg' => $e->getMessage(), 'flag' => $flag);
                }
            }
            else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err','msg' => $msg, 'flag' => $flag);
            }
        }
        else
        {
            $msg = 	$translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err','msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function flagallAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $flag = $this->_request->getPost('paction');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow()) {
                if (! empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $inquiryMapper = new B2b_Model_InquiryMapper();
                            $result = $inquiryMapper->flag($id,$flag);
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator(
                                        "b2b_message_flag_update_successful");
                                $json_arr = array('status' => 'ok',
                                        'msg' => $msg, 'flag' => $flag);
                            } else {
                                $msg = $translator->translator(
                                        "b2b_message_status_update_failed");
                                $json_arr = array('status' => 'err',
                                        'msg' => $msg . " " . $result['msg'] , 'flag' => $flag);
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err',
                                    'msg' => $e->getMessage(), 'flag' => $flag);
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg , 'flag' => $flag);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg , 'flag' => $flag);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg , 'flag' => $flag);
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function readAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost())
        {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()){

                $id = $this->_request->getPost('id');
                $read = $this->_request->getPost('paction');
                $inquiryMapper = new B2b_Model_InquiryMapper();

                try {
                    $result = $inquiryMapper->read($id, $read);
                    if($result['status'] == 'ok')
                    {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array('status' => 'ok','msg' => $msg, 'read' => $read);
                    }
                    else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg'], 'read' => $read);
                    }
                }
                catch (Exception $e)
                {
                    $json_arr = array('status' => 'err','msg' => $e->getMessage(), 'read' => $read);
                }
            }
            else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err','msg' => $msg, 'read' => $read);
            }
        }
        else
        {
            $msg = 	$translator->translator('b2b_common_save_failed');
            $json_arr = array('status' => 'err','msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function readallAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $read = $this->_request->getPost('paction');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow('delete', 'messageboards', 'B2b')) {
                if (! empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $inquiryMapper = new B2b_Model_InquiryMapper();
                            $result = $inquiryMapper->read($id,$read);
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator(
                                        "b2b_message_read_update_successful");
                                $json_arr = array('status' => 'ok',
                                        'msg' => $msg, 'read' => $read );
                            } else {
                                $msg = $translator->translator(
                                        "b2b_message_status_update_failed");
                                $json_arr = array('status' => 'err',
                                        'msg' => $msg , 'read' => $read);
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err',
                                    'msg' => $e->getMessage() . ' ' . $id);
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg , 'read' => $read);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg , 'read' => $read);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg , 'read' => $read );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }


    public function trashAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost()) {
            $permission = new B2b_View_Helper_Allow();
            if ($permission->allow()) {
                $id = $this->_request->getPost('id');
                $trash = '0';
                $inquiryMapper = new B2b_Model_InquiryMapper();
                try {
                    $result = $inquiryMapper->trash($id, $trash);
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("b2b_common_save_success");
                        $json_arr = array(
                            'status' => 'ok',
                            'msg' => $msg,
                            'trash' => $trash
                        );
                    } else {
                        $msg = $translator->translator("b2b_common_save_failed");
                        $json_arr = array(
                            'status' => 'err',
                            'msg' => $msg . " " . $result['msg'],
                            'trash' => $trash
                        );
                    }
                } catch (Exception $e) {
                    $json_arr = array(
                        'status' => 'err',
                        'msg' => $e->getMessage(),
                        'trash' => $trash
                    );
                }
            } else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array(
                    'status' => 'err',
                    'msg' => $msg,
                    'trash' => $trash
                );
            }
        } else {
            $msg = $translator->translator('b2b_common_save_failed');
            $json_arr = array(
                'status' => 'err',
                'msg' => $msg
            );
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }
    public function trashallAction()
    {

    }


    public function addAction()
    {
        $tradeshowForm = new B2b_Form_TradeShowForm ();
        $auth = Zend_Auth::getInstance ();
        $user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost())
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            $this->view->translator	= $translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow ();
            try
            {
                if($tradeshowForm->isValid($this->_request->getPost()))
                {
                    $tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
                    if ($permission->allow() && !empty($user_id)){
                        $tradeshowModel->setUser_id($user_id);
                        $result = $tradeshowModel->save();
                        if($result['status'] == 'ok')
                        {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok','msg' => $msg);
                        }
                        else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
                        }


                    }


                    else
                    {
                        $msg = $translator->translator("page_access_restrictions");
                        $res_value = Zend_Json::encode($json_arr);
                        $this->_response->setBody($res_value);
                        return;
                    }


                }
                else
                {
                    $validatorMsg = $tradeshowForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach($validatorMsg as $key => $errType)
                    {
                        foreach($errType as $errkey => $value)
                        {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV','msg' => $vMsg);
                }
            }
            catch(Exception $e)
            {

                $json_arr = array('status' => 'err','msg' => $e->getMessage());
            }


            $res_value = Zend_Json::encode($json_arr);


            $this->_response->setBody($res_value);
        }

        $this->view->assign('tradeshowForm' , $tradeshowForm);
    }

    public function editAction()
    {
        $id = $this->_request->getParam('id');
        $tradeshowForm = new B2b_Form_TradeShowForm ();
        $auth = Zend_Auth::getInstance ();
        $user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        if ($this->_request->isPost())
        {
        	$this->_helper->viewRenderer->setNoRender();
        	$this->_helper->layout->disableLayout();
        	$translator = Zend_Registry::get('translator');
        	$this->view->translator	= $translator;
        	$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        	$json_arr = null;
        	$permission = new B2b_View_Helper_Allow ();
        	try
        	{
        		if($tradeshowForm->isValid($this->_request->getPost()))
        		{
        			$tradeshowModel = new B2b_Model_Tradeshow($tradeshowForm->getValues());
        			if ($permission->allow() && !empty($user_id)){

        				$result = $tradeshowModel->save();

        				if($result['status'] == 'ok')
        				{
        					$msg = $translator->translator("page_save_success");
        					$json_arr = array('status' => 'ok','msg' => $msg);
        				}
        				else {
        					$msg = $translator->translator("page_save_err");
        					$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
        				}
        			}
        			else
        			{
        				$msg = $translator->translator("page_access_restrictions");
        				$res_value = Zend_Json::encode($json_arr);
        				$this->_response->setBody($res_value);
        				return;
        			}

        		}
        		else
        		{
        			$validatorMsg = $tradeshowForm->getMessages();
        			$vMsg = array();
        			$i = 0;
        			foreach($validatorMsg as $key => $errType)
        			{
        				foreach($errType as $errkey => $value)
        				{
        					$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
        					$i++;
        				}
        			}
        			$json_arr = array('status' => 'errV','msg' => $vMsg);
        		}
        	}

        	catch(Exception $e)
        	{

        		$json_arr = array('status' => 'err','msg' => $e->getMessage());
        	}

        	$res_value = Zend_Json::encode($json_arr);


        	$this->_response->setBody($res_value);
        }

        else {
        	if ($id){
        		$tradeshowTable = new B2b_Model_DbTable_Tradeshow();
        		$tradeshowDetails = $tradeshowTable->getPackageById($id);

        		if ( is_array($tradeshowDetails) && (count($tradeshowDetails) > 1 ) && !empty($tradeshowDetails) ){
        			$tradeshowForm->populate($tradeshowDetails);
        		}
        		else {
        			$this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
        		}
        	}

        	else {
        		$this->_forward('add', $this->_request->getControllerName(), $this->_request->getModuleName());
        	}
        }

        $this->view->assign('tradeshowForm' , $tradeshowForm);

    }
    public function deleteAction()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $json_arr = null;
        if ($this->_request->isPost())
        {
            $permission = new B2b_View_Helper_Allow ();
            if ($permission->allow()){
                $id = $this->_request->getPost('id');

                $inquiryMapper = new B2b_Model_InquiryMapper();
                try {
                    $result = $inquiryMapper->delete($id);
                    if($result['status'] == 'ok')
                    {
                        $msg = $translator->translator("b2b_common_delete_success");
                        $json_arr = array('status' => 'ok','msg' => $msg);
                    }
                    else {
                        $msg = $translator->translator("b2b_common_delete_failed");
                        $json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
                    }
                }
                catch (Exception $e) {
                    $json_arr = array('status' => 'err','msg' => $e->getMessage());
                }
            }
            else {
                $msg = $translator->translator("page_access_restrictions");
                $json_arr = array('status' => 'err','msg' => $msg);
                $res_value = Zend_Json::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
        else
        {
            $msg = 	$translator->translator('b2b_common_delete_failed');
            $json_arr = array('status' => 'err','msg' => $msg);
        }
        //Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction ()
    {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new B2b_View_Helper_Allow();
            if ($perm->allow('delete', 'messageboards', 'B2b')) {
                if (! empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    foreach ($id_arr as $id) {
                        try {
                            $inquiryMapper = new B2b_Model_InquiryMapper();
                            $result = $inquiryMapper->delete($id);
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator(
                                        "b2b_common_delete_success");
                                $json_arr = array('status' => 'ok',
                                        'msg' => $msg);
                            } else {
                                $msg = $translator->translator(
                                        "b2b_common_delete_failed");
                                $json_arr = array('status' => 'err',
                                        'msg' => $msg . " == " . $result['msg']);
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err',
                                    'msg' => $e->getMessage());
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        // Convert To JSON ARRAY
        $res_value = Zend_Json::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function listAction()
    {		
        $pageNumber = $this->getRequest()->getParam('page');
        $getViewPageNum = $this->getRequest()->getParam('viewPageNum');
        $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
        if(empty($getViewPageNum) && empty($viewPageNumSes))
        {
            $viewPageNum = '30';
        }
        else if(!empty($getViewPageNum) && empty($viewPageNumSes))
        {
            $viewPageNum = $getViewPageNum;
        }
        else if(empty($getViewPageNum) && !empty($viewPageNumSes))
        {
            $viewPageNum = $viewPageNumSes;
        }
        else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
        {
            $viewPageNum = $getViewPageNum;
        }
        Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
        $uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
        if( ($datas = $this->_controllerCache->load($uniq_id)) === false )
        {
            $inquiryMapper = new B2b_Model_TradeshowMapper();
            $datas =  $inquiryMapper->fetchAll($pageNumber);
            $this->_controllerCache->save($datas, $uniq_id);
        }
        $this->view->assign('tradeshow' , $datas);
    }
}