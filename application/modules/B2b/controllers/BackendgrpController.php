<?php
class B2b_BackendgrpController extends Zend_Controller_Action
{
	private $b2bGroupForm;
	private $_controllerCache;
	private $_auth_obj;		
	private $translator;

    public function init()
    {
        /* Initialize action controller here */
		$this->b2bGroupForm =  new B2b_Form_GroupForm ();
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }

	public function preDispatch()
	{

		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');

		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);

		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
	}

	//PROPERTY GROUP LIST FUNCTION

	public function listgrpAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');	
				$request_from	=	$this->_request->getParam('request_from');			
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				$backup 		= 	Eicra_Global_Variable::getSession()->viewPageNum;			
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new B2b_Model_GroupListMapper();	
					$groupSql = new B2b_Controller_Helper_GroupSql();
                    $tableColumns = $groupSql->getBackendList();
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data, $tableColumns);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr 									= 		(!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr 									= 		(is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']						=  		$this->view->numbers($entry_arr['id']);
								$entry_arr['group']							=  		array('id' => $entry_arr['id'], 'group_name' => $entry_arr['group_name']);
								$entry_arr['category_num_format']			=  		$this->view->numbers($entry_arr['category_num']);
								$entry_arr['company_profile_num_format']	=  		$this->view->numbers($entry_arr['company_profile_num']);
								$entry_arr['product_num_format']			=  		$this->view->numbers($entry_arr['product_num']);
								$entry_arr['selling_leads_num_format']		=  		$this->view->numbers($entry_arr['selling_leads_num']);
								$entry_arr['buying_leads_num_format']		=  		$this->view->numbers($entry_arr['buying_leads_num']);
								$entry_arr['business_type_num_format']		=  		$this->view->numbers($entry_arr['business_type_num']);
								$entry_arr['publish_status_group_name'] 	= 		str_replace('_', '-', $entry_arr['group_name']);	
								$entry_arr['edit_enable']					=   	($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]			=		$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				if($request_from == 'widget') {    Eicra_Global_Variable::getSession()->viewPageNum =  $backup;    }				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}
    }
	//PROPERTY GROUP FUNCTIONS

	public function addgrpAction()
	{

		if ($this->_request->isPost())
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

			if($this->b2bGroupForm->isValid($this->_request->getPost()))
			{
				$b2bGroup = new B2b_Model_Groups($this->b2bGroupForm->getValues());
				$b2bGroup->setActive('1');
				$b2bGroup->setFile_type($this->_request->getPost('file_type'));

				$perm = new B2b_View_Helper_Allow();
				if($perm->allow())
				{

					$result = $b2bGroup->saveGroup();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("b2b_group_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("b2b_group_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}
				}
				else
				{
					$Msg =  $translator->translator("b2b_group_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}
			}
			else
			{
				$validatorMsg = $this->b2bGroupForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}
			$res_value = Zend_Json::encode($json_arr);
			$this->_response->setBody($res_value);
		}
		else
		{
			$this->view->b2bGroupForm = $this->b2bGroupForm;
			$this->render();
		}
	}

	public function editgrpAction()
	{
		$id = $this->_request->getParam('id');
		$b2bGroupForm =  new B2b_Form_GroupForm ();
		$auth = Zend_Auth::getInstance();
		$user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

		if ($this->_request->isPost())
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

			if($b2bGroupForm->isValid($this->_request->getPost()))
			{
				$id = $b2bGroupForm->getValue('id');
				$b2bGroup = new B2b_Model_Groups($b2bGroupForm->getValues());
				$b2bGroup->setId($id);
				$b2bGroup->setFile_type($this->_request->getPost('file_type'));

				$perm = new B2b_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $b2bGroup->saveGroup();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("b2b_group_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("b2b_group_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}
				}
				else
				{
					$Msg =  $translator->translator("b2b_group_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}

			}
			else
			{
				$validatorMsg = $b2bGroupForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}
			$res_value = Zend_Json::encode($json_arr);
			$this->_response->setBody($res_value);
		}		
			$Group = new B2b_Model_DbTable_Group();
			$GroupData = $Group->getGroupName($id);
	
			if (empty($id) || empty($GroupData) ||  count($GroupData) < 1) {
				$this->_forward('addgrp', $this->_request->getControllerName(),
						$this->_request->getModuleName());
	
			   return;
			}
	
			$isEditable = ($this->_auth_obj->access_other_user_article == '1' || $user_id == $GroupData['entry_by']) ? true : false;
	
			if ($isEditable){
				$b2bGroupForm->populate($GroupData);
				$b2bGroupForm->file_type->setIsArray(true);
				$file_type_arr = explode(',',$GroupData['file_type']);
				$b2bGroupForm->file_type->setValue($file_type_arr);
				$this->view->id = $id;
				$this->view->b2bGroupForm = $b2bGroupForm;			
			}
			else {
				$this->_forward('addgrp', $this->_request->getControllerName(),
					$this->_request->getModuleName());
			}



	}

	public function publishgrpAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');

		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$group_name = $this->_request->getPost('group_name');
			$paction = $this->_request->getPost('paction');

			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}

			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();

			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'b2b_group',array('active' => $active), $where);
				$return = true;
			}
			catch (Exception $e)
			{
				$return = false;
				$e_err = $e->getMessage();
			}
			if($return)
			{
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('b2b_list_publish_err',$group_name);
				$json_arr = array('status' => 'err','msg' => $msg." ".$e_err);
			}
		}

		//Convert To JSON ARRAY
		$res_value = Zend_Json::encode($json_arr);
		$this->_response->setBody($res_value);

	}

	public function publishallgrpAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');

		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_str = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');

			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}

			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);

				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();

				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{
						$conn->update(Zend_Registry::get('dbPrefix').'b2b_group',array('active' => $active), $where);
						$return = true;
					}
					catch (Exception $e)
					{
						$return = false;
						$e_err = $e->getMessage();
					}
				}

				if($return)
				{
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('b2b_group_list_publish_err');
					$json_arr = array('status' => 'err','msg' => $msg." ".$e_err);
				}
			}
			else
			{
				$msg = $translator->translator("b2b_group_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}

		//Convert To JSON ARRAY
		$res_value = Zend_Json::encode($json_arr);
		$this->_response->setBody($res_value);

	}

	public function deletegrpAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');

		if ($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$group_name = $this->_request->getPost('group_name');

			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();

			$check_num_cat = new B2b_View_Helper_Group();

			if($check_num_cat->getNumOfB2b($id) == '0' && $check_num_cat->getNumOfProduct($id) == '0' && $check_num_cat->getNumOfB2bType($id) == '0' && $check_num_cat->getNumOfSelling($id) == '0' && $check_num_cat->getNumOfBuying($id) == '0' && $check_num_cat->getNumOfCompany($id) == '0')
			{
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'b2b_group', $where);
					$msg = 	$translator->translator('b2b_group_delete_success', $group_name);
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e)
				{
					$msg = 	$translator->translator('b2b_list_delete_err');
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('b2b_group_delete_err', $group_name);
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('b2b_list_delete_err');
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY
			$res_value = Zend_Json::encode($json_arr);
			$this->_response->setBody($res_value);
	}

	public function deleteallgrpAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');

		if ($this->_request->isPost())
		{
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);

				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();

				$check_num_cat = new B2b_View_Helper_Group();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{
					if($check_num_cat->getNumOfB2b($id) == '0' && $check_num_cat->getNumOfProduct($id) == '0' && $check_num_cat->getNumOfB2bType($id) == '0' && $check_num_cat->getNumOfB2bType($id) == '0' && $check_num_cat->getNumOfSelling($id) == '0' && $check_num_cat->getNumOfBuying($id) == '0' && $check_num_cat->getNumOfCompany($id) == '0')
					{
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'b2b_group', $where);
							$msg = 	$translator->translator('b2b_group_delete_success');
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e)
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('b2b_group_delete_success');
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('b2b_group_delete_success');
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("b2b_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('b2b_list_delete_err');
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY
			$res_value = Zend_Json::encode($json_arr);
			$this->_response->setBody($res_value);
	}

	public function cronsetupAction()
	{

	}

}
