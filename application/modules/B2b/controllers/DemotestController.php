<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class B2b_DemotestController extends Zend_Controller_Action {

   public function init() {
        /* Initialize action controller here */
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }
    
    public function demotestAction()
    {
        
        $mydata= array();
        $mydata[]='arif';
        $mydata[]='fahim';
        $mydata[]=3;
        $mydata[]=4;
        $mydata[]=5;
        
      $this->view->assign('data',$mydata);
      
      $pakagedatashow=new B2b_Model_MemberListMapper();
      $pakagedatashow2=$pakagedatashow->fetchAll(1, "1", null);
      $this->view->assign('pakagedatashow2',$pakagedatashow2);
      
    }
    
}  