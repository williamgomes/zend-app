<?php

class B2b_CompanyController extends Zend_Controller_Action {

    private $_page_id;
    private $_translator;
    private $_controllerCache;
    private $_auth_obj;

    public function init() {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');

        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;


        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());
    }

    public function detailsAction() {
        $company_title = $this->_request->getParam('company_title');
        if (empty($company_title)) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator($company_title, '', 'B2b'), $this->view->url()));

        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function invalidAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyDetails = $companyProfile->getCompanyDetails(
                'Eicra-Software-Limited-1', true);
        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function homeAction() {
        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID)) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_company_profile_welcome_note', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_company_profile_contact_us', '', 'B2b');

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function aboutusAction() {
        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID)) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_company_profile_profile', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_company_profile_contact_us', '', 'B2b');

        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function storeAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $company_title = $this->_request->getParam('company_title');
        $this->view->company_title = $company_title;
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_front_products_offer_list_of', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_front_products_offer_list', '', 'B2b');

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        } else {
            $this->view->assign('companyDetails', $companyDetails[0]);
        }
    }

    public function inquiryAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();

        $company_title = $this->_request->getParam('company_title');

        $inquiryForm = new B2b_Form_InquiryForm();
        $auth = Zend_Auth::getInstance();
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        if (empty($user_id)) {
            $loginForm = new Members_Form_LoginForm();
            $this->view->assign('logindetails', $loginForm);
        }

        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID['user_id']) ||
                count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->_translator;

            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $json_arr = null;
            $permission = new B2b_View_Helper_Allow();
            try {
                if ($inquiryForm->isValid($this->_request->getPost())) {
                    
                    
                    $inquiryModel = new B2b_Model_Inquiry($inquiryForm->getValues());
                    if (!empty($user_id)) {
                        $inquiryModel->setSender_id($user_id);
                    }

                    $inquiryModel->setRecipient_title($company_title);
                    $inquiryModel->setRecipient_id($companyID['user_id']);
                    $inquiryModel->setParent('0');
                    $inquiryModel->setActive('1');
                    $inquiryModel->setStatus('1');
                    $inquiryModel->setRead('0');
                    
                    $arrSenderPostData = array();
                    if($this->_request->getPost('email')){ //this means user not logged in and submitting his information
                        $arrSenderPostData['name'] = $this->_request->getPost('name');
                        $arrSenderPostData['company_name'] = $this->_request->getPost('company_name');
                        $arrSenderPostData['street'] = $this->_request->getPost('street');
                        $arrSenderPostData['state'] = $this->_request->getPost('state');
                        $arrSenderPostData['city'] = $this->_request->getPost('city');
                        $arrSenderPostData['phone_no'] = $this->_request->getPost('phone') . ' ' . $this->_request->getPost('phone_part2') . ' ' . $this->_request->getPost('phone_part3');
                        $arrSenderPostData['fax_no'] = $this->_request->getPost('fax') . ' ' . $this->_request->getPost('fax_part2') . ' ' . $this->_request->getPost('fax_part3');
                        $arrSenderPostData['country'] = $this->_request->getPost('country');
                        $arrSenderPostData['email'] = $this->_request->getPost('email');
                        $arrSenderPostData['website'] = $this->_request->getPost('website');
                    }
                    
                    
                    $result = $inquiryModel->save();
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("page_save_success");
                        try {
                            $reply_with = $inquiryModel->getReply_with();
                            if ($reply_with) {
                                $reply_with_arr = explode(',', $reply_with);
                                if (in_array('Email', $reply_with_arr)) {
                                    // Injecting preferences to the view model.
                                    $preferences_db = new B2b_Model_DbTable_Preferences();
                                    $preferences_data = $preferences_db->getOptions();

                                    $global_conf = Zend_Registry::get('global_conf');

                                    if ($preferences_data['cron_inquiry_user_email_template_id'] && !empty($preferences_data['cron_inquiry_user_email_template_id'])) {
                                        $form_info = array(
                                            'email_template_set' => $preferences_data['cron_inquiry_user_email_template_id'],
                                            'email_to' => $companyDetails[0]['user_email'], //($companyDetails[0]['is_massage'] == 'YES') ? $companyDetails[0]['user_email'] : $global_conf['global_email'],
                                            'email_bcc' => $global_conf['global_email'], //($companyDetails[0]['is_massage'] == 'YES') ?  $global_conf['global_email'] : '',
                                            'set_from_email' => $this->_auth_obj->username
                                        );
                                        $inquiry_form = $this->getInqueryTemplate(array('inquiryModel' => $inquiryModel, 'companyProfile' => $companyProfile, 'sendersData' => $arrSenderPostData));
                                        $datas = array(
                                            'inquery_subject' => $inquiryModel->getSubject(),
                                            'inquiry_form' => $inquiry_form
                                        );
                                        $email_class = new Members_Controller_Helper_Registers();
                                        $email_class->sendGeneralMail($datas, $form_info);
                                    } else {
                                        $msg .= ' ' . $translator->translator("menu_b2b_inquiry_email_template_not_found");
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            $msg .= ' ' . $e->getMessage();
                        }
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                    } else {
                        $msg = $translator->translator("page_save_err");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                    }
                } else {
                    $validatorMsg = $inquiryForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key,
                                'errKey' => $errkey, 'value' => $value);
                            $i ++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($inquiryForm));
            }

            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }



        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array($companyID['company_name'], $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_frontend_send_inquiry', '', 'B2b'),
                $this->view->url()));

        $this->view->assign('companyDetails', $companyDetails[0]);
        $this->view->assign('inquiryForm', $inquiryForm);
        $this->view->assign('company_title', $company_title);
    }

    private function getInqueryTemplate($data_arr) {
        $template = "";
        if ($data_arr['inquiryModel']) {
            $template .= "<table border=\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">";
            $sender_id = $data_arr['inquiryModel']->getSender_id();
            $arraySenderInfo = $data_arr['sendersData'];

            if (!empty($sender_id)) {
                $search_params['filter']['filters'][0] = array('field' => 'user_id', 'operator' => 'eq', 'value' => $sender_id);
                $companySql = new B2b_Controller_Helper_CompanySql ();
                $tableColumns = $companySql->getB2bInquiryCompanyProfileList();
                $sender_info_obj = $data_arr['companyProfile']->getListInfo('1', $search_params, $tableColumns);
                if ($sender_info_obj) {
                    $sender_info = $sender_info_obj->toArray();
                    $sender_info = $sender_info[0];
                    $template .= "<tr>";
                    $template .= "<td width=\"50%\">";
                    $template .= ($sender_info['full_name']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_name') . "</strong> " . $this->view->escape($sender_info['full_name']) . "<br />\n" : '';
                    $template .= ($sender_info['usr_phone']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_phone') . "</strong> " . $this->view->escape($sender_info['usr_phone']) . "<br />\n" : (($sender_info['cp_phone']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_phone') . "</strong> " . $this->view->escape($sender_info['cp_phone']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_mobile']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_mobile') . "</strong> " . $this->view->escape($sender_info['usr_phone']) . "<br />\n" : (($sender_info['cp_mobile']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_mobile') . "</strong> " . $this->view->escape($sender_info['cp_mobile']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_fax']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_fax') . "</strong> " . $this->view->escape($sender_info['usr_fax']) . "<br />\n" : (($sender_info['cp_fax']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_fax') . "</strong> " . $this->view->escape($sender_info['cp_fax']) . "<br />\n" : '' );
                    $template .= ($data_arr['inquiryModel']->getFeedbacks()) ? "<strong>" . $this->_translator->translator('b2b_message_feedbacks') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getFeedbacks()) . "<br />\n" : '';
                    $template .= ($sender_info['country_code']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_country') . "</strong> <img src=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/data/adminImages/flagsImage/" . $sender_info['country_code'] . ".gif\" border=\"0\" title=\"" . $this->view->escape($sender_info['country_name']) . "\"   alt=\"" . $this->view->escape($sender_info['country_name']) . "\" /> <br />\n" : '';
                    $template .= "</td>";
                    $template .= "<td width=\"50%\">";
                    $template .= ($sender_info['usr_website']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_website') . "</strong> " . $this->view->escape($sender_info['usr_website']) . "<br />\n" : (($sender_info['website']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_website') . "</strong> " . $this->view->escape($sender_info['website']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_address']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_address') . "</strong> " . $this->view->escape($sender_info['usr_address']) . "<br />\n" : (($sender_info['cp_street']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_address') . "</strong> " . $this->view->escape($sender_info['cp_street']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_city']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_city') . "</strong> " . $this->view->escape($sender_info['usr_city']) . "<br />\n" : (($sender_info['cp_city']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_city') . "</strong> " . $this->view->escape($sender_info['cp_city']) . "<br />\n" : '' );
                    $template .= ($sender_info['usr_postcode']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_postalcode') . "</strong> " . $this->view->escape($sender_info['usr_postcode']) . "<br />\n" : (($sender_info['cp_postal_code']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_postalcode') . "</strong> " . $this->view->escape($sender_info['cp_postal_code']) . "<br />\n" : '' );
                    $template .= ($sender_info['profile_since']) ? "<strong>" . $this->_translator->translator('b2b_message_member_since') . "</strong> " . $this->view->escape($sender_info['profile_since']) . "<br />\n" : '';
                    $template .= ($data_arr['inquiryModel']->getReply_with()) ? "<strong>" . $this->_translator->translator('b2b_message_reply_with') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getReply_with()) . "<br />\n" : '';
                    $template .= "</td>";
                    $template .= "</tr>";
                    $template .= "<tr>";
                    $template .= "<td colspan=\"2\">";
                    $template .= ($sender_info['usr_company']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_company') . "</strong> " . $this->view->escape($sender_info['usr_company']) . "<br />\n" : (($sender_info['cp_company_name']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_company') . "</strong> " . $this->view->escape($sender_info['cp_company_name']) . "<br />\n" : '' );
                    $template .= ($data_arr['inquiryModel']->getIp()) ? "<strong>" . $this->_translator->translator('b2b_message_sender_ip') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getIp()) . "<br />\n" : '';
                    $template .= "<strong>" . $this->_translator->translator('b2b_message_sender_profile') . "</strong> " . (($sender_info['company_title']) ? "<a href=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/Company-Homepage/" . $this->view->escape($sender_info['company_title']) . "\">" . $this->_translator->translator("b2b_message_view_sender_profile") . "</a>" . "<br />\n" : $this->_translator->translator("b2b_company_profile_not_found") . "<br />\n");
                    $template .= ($sender_info['usr_email']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_email') . "</strong> " . $this->view->escape($sender_info['usr_email']) . "<br />\n" : '';
                    $template .= "<br /><strong>" . $this->_translator->translator("b2b_message_details") . " : </strong> <br />\n<hr />  " . $this->view->escape($data_arr['inquiryModel']->getMessage()) . "   <br /> \n <hr />";
                    $template .= "</td>";
                    $template .= "</tr>";
                }
            } else {
                $template .= "<tr>";
                $template .= "<td width=\"50%\">";
                $template .= ($arraySenderInfo['name']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_name') . "</strong> " . $this->view->escape($arraySenderInfo['name']) . "<br />\n" : '';
                $template .= ($arraySenderInfo['email']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_email') . "</strong> " . $this->view->escape($arraySenderInfo['email']) . "<br />\n" : '';
                $template .= ($arraySenderInfo['phone_no']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_phone') . "</strong> " . $this->view->escape($arraySenderInfo['phone_no']) . "<br />\n" : '';
                $template .= ($arraySenderInfo['fax_no']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_fax') . "</strong> " . $this->view->escape($arraySenderInfo['fax_no']) . "<br />\n" : '';
                $template .= ($data_arr['inquiryModel']->getFeedbacks()) ? "<strong>" . $this->_translator->translator('b2b_message_feedbacks') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getFeedbacks()) . "<br />\n" : '';
                $template .= ($arraySenderInfo['country']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_country') . "</strong> " . $this->view->escape($arraySenderInfo['country']) . "<br />\n"  : '';
                $template .= "</td>";
                $template .= "<td width=\"50%\">";
                $template .= ($arraySenderInfo['website']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_website') . "</strong> " . $this->view->escape($arraySenderInfo['website']) . "<br />\n" : "";
                $template .= ($arraySenderInfo['street']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_address') . "</strong> " . $this->view->escape($arraySenderInfo['street']) . ', ' . $this->view->escape($arraySenderInfo['state']) . "<br />\n" : '';
                $template .= ($arraySenderInfo['city']) ? "<strong>" . $this->_translator->translator('b2b_message_sender_city') . "</strong> " . $this->view->escape($arraySenderInfo['city']) . "<br />\n" : '' ;
                $template .= ($data_arr['inquiryModel']->getReply_with()) ? "<strong>" . $this->_translator->translator('b2b_message_reply_with') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getReply_with()) . "<br />\n" : '';
                $template .= "</td>";
                $template .= "</tr>";
                $template .= "<tr>";
                $template .= "<td colspan=\"2\">";
                $template .= ($data_arr['inquiryModel']->getIp()) ? "<strong>" . $this->_translator->translator('b2b_message_sender_ip') . "</strong> " . $this->view->escape($data_arr['inquiryModel']->getIp()) . "<br />\n" : '';
                $template .= "<strong>" . $this->_translator->translator('b2b_message_sender_profile') . "</strong> " . (($sender_info['company_title']) ? "<a href=\"" . $this->view->serverUrl() . $this->view->baseUrl() . "/Company-Homepage/" . $this->view->escape($sender_info['company_title']) . "\">" . $this->_translator->translator("b2b_message_view_sender_profile") . "</a>" . "<br />\n" : $this->_translator->translator("b2b_company_profile_not_found") . "<br />\n");
                $template .= "<br />\n<strong>" . $this->_translator->translator("b2b_message_details") . " : </strong> <br />\n<hr />  " . $this->view->escape($data_arr['inquiryModel']->getMessage()) . "   <br /> \n <hr />";
                $template .= "</td>";
                $template .= "</tr>";
            }
            $template .= "</table>";
        }
        return $template;
    }

    public function productlistAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $company_title = $this->_request->getParam('company_title');
        $productList = $this->_request->getParam('list');
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || empty($productList)) {

            $this->_forward('
	                ', 'frontend', $this->_request->getModuleName());
            return;
        }

        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array(
                $this->_translator->translator(
                        'b2b_company_profile_product_list', '', 'B2b'),
                $this->view->url()));

        $companyDetails = $companyProfile->getCompanyDetails(
                'Eicra-Software-Limited-1', true);

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function contactAction() {
        $company_title = $this->_request->getParam('company_title');
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID)) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }
        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);
        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_company_profile_contact_with', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_company_profile_contact_us', '', 'B2b');
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $this->view->assign('companyDetails', $companyDetails[0]);
    }

    public function buyingAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $company_title = $this->_request->getParam('company_title');
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }
        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_front_buying_offer_list_of', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_front_buying_offer_list', '', 'B2b');

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        $this->view->assign('companyDetails', $companyDetails[0]);

        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $user_id = $companyID['user_id'];
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);

            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'Company-Buyoffers/:company_title/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'company_title' => $company_title,
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {

                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_buying_leads', false);

                    $review_helper = new Review_View_Helper_Review();

                    $list_mapper = new B2b_Model_BuyingMapper();
                    $buyingSql = new B2b_Controller_Helper_BuyingSql();
                    $tableColumns = $buyingSql->getCompanyFontendList($user_id);
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $img_thumb_arr = explode(',', $entry_arr['others_images']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                            $entry_arr['primary_file_field'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['primary_file_field'] : 'data/frontImages/hotels/hotels_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['others_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));

                            $entry_arr['buying_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['buying_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));

                            $entry_arr['business_type'] = (!empty(
                                            $business_type) && is_array($business_type)) ? implode(
                                            ', ', $business_type) : '0';
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function sellingAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $company_title = $this->_request->getParam('company_title');
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }
        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_front_selling_offer_list_of', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_front_buying_offer_list', '', 'B2b');

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        $this->view->assign('companyDetails', $companyDetails[0]);

        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $user_id = $companyID['user_id'];
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);

            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'Company-Selloffers/:company_title/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'company_title' => $company_title,
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {

                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_selling_leads', false);

                    $review_helper = new Review_View_Helper_Review();

                    $list_mapper = new B2b_Model_SellingMapper();
                    $sellingSql = new B2b_Controller_Helper_SellingSql();
                    $tableColumns = $sellingSql->getCompanyFontendList($user_id);
                    $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);

                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $img_thumb_arr = explode(',', $entry_arr['others_images']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['primary_file_field_format'] = ($this->view->escape(
                                            $entry_arr['primary_file_field'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['primary_file_field'] : 'data/frontImages/hotels/hotels_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['others_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));

                            $entry_arr['selling_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['selling_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $entry_arr['business_type'] = (!empty(
                                            $business_type) && is_array($business_type)) ? implode(
                                            ', ', $business_type) : '0';

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function productsAction() {
        $companyProfile = new B2b_Model_DbTable_CompanyProfile();
        $company_title = $this->_request->getParam('company_title');
        $companyID = $companyProfile->hasCompany($company_title);

        if (empty($company_title) || empty($companyID) || count($companyID) == 0) {
            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }
        $companyDetails = $companyProfile->getCompanyDetails($company_title, true);

        if (empty($companyDetails[0]['company_active']) ||
                empty($companyDetails[0]['user_status'])) {

            $this->_forward('invalidrequest', 'frontend', $this->_request->getModuleName());
            return;
        }

        $breadCrumbText = ($companyDetails[0]['company_name']) ? $this->_translator->translator(
                        'b2b_products', '', 'B2b') . '  ' .
                $companyDetails[0]['company_name'] : $this->_translator->translator(
                        'b2b_front_buying_offer_list', '', 'B2b');

        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'menu_members_view_business_profile', '', 'B2b'),
                $this->view->url()),
            array($breadCrumbText, $this->view->url()));

        $this->view->assign('companyDetails', $companyDetails[0]);

        $packageMapper = new B2b_Model_PackageMapper();
        $this->view->assign('packagesArray', $packageMapper->getPackages(true));
        $user_id = $companyID['user_id'];

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        // Injecting preferences to the view model.
        $preferences_db = new B2b_Model_DbTable_Preferences();
        $this->view->assign('preferences_data', $preferences_db->getOptions());

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam(
                        'group_id') : $this->_page_id;

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id',
                'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $group_db = new B2b_Model_DbTable_Group();
            $group_search_params['filter']['filters'][0] = array(
                'field' => 'id', 'operator' => 'eq',
                'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);

            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array(
                    'field' => $group_info[0]['file_sort'],
                    'dir' => $group_info[0]['file_order']);
            }
        }

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost(
                                'page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber ==
                        '1' || empty($pageNumber)) ? $this->_request->getParam(
                                        'menu_id') : $this->_request->getParam('menu_id') .
                                '/:page') : 'Company-Products/:company_title/*';
                $posted_data['browser_url'] = ($posted_data['browser_url'] ==
                        'no') ? '' : $this->view->url(
                                array('module' => $this->view->getModule,
                            'controller' => $this->view->getController,
                            'action' => $this->view->getAction,
                            'group_id' => $this->_request->getParam(
                                    'group_id'),
                            'company_title' => $company_title,
                            'page' => ($pageNumber == '1' ||
                            empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json::encode($posted_data);
                $encode_postValue = Zend_Json::encode($encode_postValue);
                $encode_auth_obj = Zend_Json::encode($this->_auth_obj);
                $uniq_id = md5(
                        preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' .
                                $encode_auth_obj . '_' .
                                $encode_postValue));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) ===
                        false) {

                    $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'b2b_products', false);

                    $review_helper = new Review_View_Helper_Review();

                    $list_mapper = new B2b_Model_ProductMapper();
                    $list_datas = $list_mapper->getLeadsByCompany($pageNumber, '1', $posted_data, true, $user_id);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $business_type = $entry_arr['business_type'];
                            $img_thumb_arr = explode(',', $entry_arr['product_images']);
                            $image_num = (empty($img_thumb_arr[0])) ? '0' : count(
                                            $img_thumb_arr);
                            $entry_arr['product_images_primary_format'] = ($this->view->escape(
                                            $entry_arr['product_images_primary'])) ? $entry_arr['images_path'] .
                                    '/' . $entry_arr['product_images_primary'] : 'data/frontImages/hotels/hotels_image/' .
                                    $img_thumb_arr[0];
                            $entry_arr['product_images_num_format'] = $this->_translator->translator(
                                    'b2b_num_of_photos', $this->view->numbers($image_num));
                            $entry_arr['product_vote_format'] = $vote->getButton(
                                    $entry_arr['id'], $this->view->escape($entry_arr['name']));
                            $entry_arr['review_id'] = $this->view->preferences_data['product_review_id'];
                            $entry_arr['review_no'] = (!empty(
                                            $entry_arr['review_id'])) ? $review_helper->getNumOfReviews(
                                            $entry_arr['id'], $entry_arr['review_id']) : '0';
                            $entry_arr['review_no_format'] = (!empty(
                                            $entry_arr['review_no'])) ? $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(
                                                    $entry_arr['review_no'])) : $this->_translator->translator(
                                            'common_review_no', $this->view->numbers(0));
                            $entry_arr['business_type'] = (!empty(
                                            $business_type) && is_array($business_type)) ? implode(
                                            ', ', $business_type) : '0';

                            $view_datas['data_result'][$key] = $entry_arr;
                            $key ++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }

                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok',
                    'data_result' => $view_datas['data_result'],
                    'total' => $view_datas['total'],
                    'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '',
                    'msg' => $e->getMessage());
            }

            // Convert To JSON ARRAY
            $res_value = Zend_Json::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

}
