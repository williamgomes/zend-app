<?php
class B2b_Model_Package {
	protected $_id ;
	protected $_group_id ;
	protected $_name ;
	protected $products;
	protected $_description ;
	protected $_b2b_images ;
	protected $_selling ;
	protected $_buying ;
	protected $_user_id ;
	protected $_price ;
	protected $_strikethrough_price ;
	protected $_profile ;
	protected $_telephone ;
	protected $_website ;
	protected $_renewable ;
	protected $_certification ;
	protected $_priority ;
	protected $_priority_flag ;
	protected $_feedback ;
	protected $_voting ;
	protected $_spotlight ;
	protected $_tradeshow ;
	protected $_social_network ;
	protected $_buyers_profile ;
	protected $_massage ;
	protected $_directory ;
	protected $_preapproval ;
	protected $_rm_header ;
	protected $_rm_footer ;
	protected $_gallery ;
	protected $_active ;
	protected $_displayable ;
	protected $_package_order ;
	protected $_eCommerce ;

	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    public function save()
    {
        $mapper  = new B2b_Model_PackageMapper();
        $return = $mapper->save($this);
        return $return;
    }
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @return the $_group_id
	 */
	public function getGroup_id() {
		return $this->_group_id;
	}

	/**
	 * @param field_type $_group_id
	 */
	public function setGroup_id($_group_id) {
		$this->_group_id = $_group_id;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @param field_type $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @param field_type $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @return the $_b2b_images
	 */
	public function getB2b_images() {
		return $this->_b2b_images;
	}

	/**
	 * @param field_type $_b2b_images
	 */
	public function setB2b_images($_b2b_images) {
		$this->_b2b_images = $_b2b_images;
	}

	/**
	 * @return the $_selling
	 */
	public function getSelling() {
		return $this->_selling;
	}

	/**
	 * @param field_type $_selling
	 */
	public function setSelling($_selling) {
		$this->_selling = $_selling;
	}

	/**
	 * @return the $_buying
	 */
	public function getBuying() {
		return $this->_buying;
	}

	/**
	 * @param field_type $_buying
	 */
	public function setBuying($_buying) {
		$this->_buying = $_buying;
	}

	/**
	 * @return the $_user_id
	 */
	public function getUser_id() {
		return $this->_user_id;
	}

	/**
	 * @param field_type $_user_id
	 */
	public function setUser_id($_user_id) {
		$this->_user_id = $_user_id;
	}

	/**
	 * @return the $_price
	 */
	public function getPrice() {
		return $this->_price;
	}

	/**
	 * @param field_type $_price
	 */
	public function setPrice($_price) {
		$this->_price = $_price;
	}

	/**
	 * @return the $_strikethrough_price
	 */
	public function getStrikethrough_price() {
		return $this->_strikethrough_price;
	}

	/**
	 * @param field_type $_strikethrough_price
	 */
	public function setStrikethrough_price($_strikethrough_price) {
		$this->_strikethrough_price = $_strikethrough_price;
	}

	/**
	 * @return the $_profile
	 */
	public function getProfile() {
		return $this->_profile;
	}

	/**
	 * @param field_type $_profile
	 */
	public function setProfile($_profile) {
		$this->_profile = $_profile;
	}

	/**
	 * @return the $_telephone
	 */
	public function getTelephone() {
		return $this->_telephone;
	}

	/**
	 * @param field_type $_telephone
	 */
	public function setTelephone($_telephone) {
		$this->_telephone = $_telephone;
	}

	/**
	 * @return the $_website
	 */
	public function getWebsite() {
		return $this->_website;
	}

	/**
	 * @param field_type $_website
	 */
	public function setWebsite($_website) {
		$this->_website = $_website;
	}

	/**
	 * @return the $_renewable
	 */
	public function getRenewable() {
		return $this->_renewable;
	}

	/**
	 * @param field_type $_renewable
	 */
	public function setRenewable($_renewable) {
		$this->_renewable = $_renewable;
	}

	/**
	 * @return the $_certification
	 */
	public function getCertification() {
		return $this->_certification;
	}

	/**
	 * @param field_type $_certification
	 */
	public function setCertification($_certification) {
		$this->_certification = $_certification;
	}

	/**
	 * @return the $_priority
	 */
	public function getPriority() {
		return $this->_priority;
	}

	/**
	 * @param field_type $_priority
	 */
	public function setPriority($_priority) {
		$this->_priority = $_priority;
	}

	/**
	 * @return the $_priority_flag
	 */
	public function getPriority_flag() {
		return $this->_priority_flag;
	}

	/**
	 * @param field_type $_priority_flag
	 */
	public function setPriority_flag($_priority_flag) {
		$this->_priority_flag = $_priority_flag;
	}

	/**
	 * @return the $_feedback
	 */
	public function getFeedback() {
		return $this->_feedback;
	}

	/**
     * @return the $_rm_header
     */
    public function getRm_header ()
    {
        return $this->_rm_header;
    }

	/**
     * @return the $_rm_footer
     */
    public function getRm_footer ()
    {
        return $this->_rm_footer;
    }

	/**
     * @param field_type $_rm_header
     */
    public function setRm_header ($_rm_header)
    {
        $this->_rm_header = $_rm_header;
    }

	/**
     * @param field_type $_rm_footer
     */
    public function setRm_footer ($_rm_footer)
    {
        $this->_rm_footer = $_rm_footer;
    }

	/**
	 * @param field_type $_feedback
	 */
	public function setFeedback($_feedback) {
		$this->_feedback = $_feedback;
	}

	/**
	 * @return the $_voting
	 */
	public function getVoting() {
		return $this->_voting;
	}

	/**
	 * @param field_type $_voting
	 */
	public function setVoting($_voting) {
		$this->_voting = $_voting;
	}

	/**
	 * @return the $_spotlight
	 */
	public function getSpotlight() {
		return $this->_spotlight;
	}

	/**
	 * @param field_type $_spotlight
	 */
	public function setSpotlight($_spotlight) {
		$this->_spotlight = $_spotlight;
	}

	/**
	 * @return the $_tradeshow
	 */
	public function getTradeshow() {
		return $this->_tradeshow;
	}

	/**
	 * @param field_type $_tradeshow
	 */
	public function setTradeshow($_tradeshow) {
		$this->_tradeshow = $_tradeshow;
	}

	/**
	 * @return the $_social_network
	 */
	public function getSocial_network() {
		return $this->_social_network;
	}

	/**
	 * @param field_type $_social_network
	 */
	public function setSocial_network($_social_network) {
		$this->_social_network = $_social_network;
	}

	/**
	 * @return the $_buyers_profile
	 */
	public function getBuyers_profile() {
		return $this->_buyers_profile;
	}

	/**
	 * @param field_type $_buyers_profile
	 */
	public function setBuyers_profile($_buyers_profile) {
		$this->_buyers_profile = $_buyers_profile;
	}

	/**
	 * @return the $_massage
	 */
	public function getMassage() {
		return $this->_massage;
	}

	/**
	 * @param field_type $_massage
	 */
	public function setMassage($_massage) {
		$this->_massage = $_massage;
	}

	/**
	 * @return the $_directory
	 */
	public function getDirectory() {
		return $this->_directory;
	}

	/**
	 * @param field_type $_directory
	 */
	public function setDirectory($_directory) {
		$this->_directory = $_directory;
	}

	/**
	 * @return the $_preapproval
	 */
	public function getPreapproval() {
		return $this->_preapproval;
	}

	/**
	 * @param field_type $_preapproval
	 */
	public function setPreapproval($_preapproval) {
		$this->_preapproval = $_preapproval;
	}

	/**
	 * @return the $_active
	 */
	public function getActive() {
		return $this->_active;
	}

	/**
	 * @param field_type $_active
	 */
	public function setActive($_active) {
		$this->_active = $_active;
	}

	/**
	 * @return the $_displayable
	 */
	public function getDisplayable() {
		return $this->_displayable;
	}

	/**
	 * @param field_type $_displayable
	 */
	public function setDisplayable($_displayable) {
		$this->_displayable = $_displayable;
	}

	/**
	 * @return the $_gallery
	 */
	public function getGallery() {
		return $this->_gallery;
	}

	/**
	 * @param field_type $_gallery
	 */
	public function setGallery($_gallery) {
		$this->_gallery = $_gallery;
	}
	/**
	 * @return the $_package_order
	 */
	public function getPackage_order ()
	{
	    if(empty($this->_package_order))
	    {
	        $this->setPackage_order('');
	    }
	    return $this->_package_order;
	}

	/**
	 * @param field_type $_package_order
	 */
	public function setPackage_order ($_package_order)
	{
	    if(!empty($_package_order))
	    {
	        $this->_package_order = $_package_order;
	    }
	    else
	    {
	        $group_id = $this->getGroup_id();
	        $user_id = $this->getUser_id();
	        $table_name		=	'membership_packages';
	        $fields_arr		=	array('id','user_id','group_id','package_order');
	        if(empty($this->_id))
	        {
	            $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, null);
	        }
	        else
	        {
	            $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $this->_id);
	        }
	        $OrderObj->setHeighestOrder($group_id,$user_id);
	        $last_order = $OrderObj->getHighOrder();

	        $this->_package_order = $last_order + 1;
	    }
	    return $this;
	}
	/**
     * @return the $products
     */
    public function getProducts ()
    {
        return $this->products;
    }

	/**
     * @param field_type $products
     */
    public function setProducts ($products)
    {
        $this->products = $products;
    }
	/**
	 * @return the $_eCommerce
	 */
	public function getECommerce() {
		return $this->_eCommerce;
	}

	/**
	 * @param field_type $_eCommerce
	 */
	public function setECommerce($_eCommerce) {
		$this->_eCommerce = $_eCommerce;
	}


}
