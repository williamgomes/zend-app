<?php
class B2b_Model_Category
{
    protected $_id ;
    protected $_parent_id ;
    protected $_name ;
    protected $_description ;
    protected $_active ;
    protected $_category_order;
    protected $_login ;
    protected $_b2b_images;
    protected $_added_on ;
    protected $_package_id ;
    protected $_url_portion ;
    protected $_isComment ;
    protected $_isShowProfile ;
    protected $_displayble ;
    protected $_featured ;
	protected $_user_id ;
	protected $_group_id ;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }

    }

    public function __set($name, $value)

    {
        $method = 'set' . $name;

        if (('mapper' == $name) || !method_exists($this, $method)) {

            throw new Exception('Invalid Auth property ' . $name);

        }

        $this->$method($value);

    }


    public function __get($name)
    {

        $method = 'get' . $name;

        if (('mapper' == $name) || !method_exists($this, $method)) {

            throw new Exception('Invalid Auth property');

        }

        return $this->$method();
    }



    public function setOptions(array $options)
    {

        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {

            $method = 'set' . ucfirst($key);

            if (in_array($method, $methods)) {

                $this->$method($value);
            }
        }

        return $this;

    }



    public function save()
    {

        $mapper  = new B2b_Model_CategoryMapper();
        $return = $mapper->save($this);
        return $return;

    }


    public function getId ()
    {
        return $this->_id;
    }


    public function setId ($_id)
    {
        $this->_id = $_id;
    }


    public function getUser_id()
    {
        return $this->_user_id;
    }


    public function setUser_id ($_user_id)
    {
        $this->_user_id = $_user_id;
    }



    public function getParent_id ()
    {
        return $this->_parent_id;
    }



	/**
     * @param field_type $_parent_id
     */
    public function setParent_id ($_parent_id)
    {
        $this->_parent_id = $_parent_id;
    }

    public function getName ()
    {
        return $this->_name;
    }
	/**
     * @param field_type $_name
     */
    public function setName ($_name)
    {
        $this->_name = $_name;
    }
	/**
     * @return the $_description
     */
    public function getDescription ()
    {
        return $this->_description;
    }
	/**
     * @param field_type $_description
     */
    public function setDescription ($_description)
    {
        $this->_description = $_description;
    }
	/**
     * @return the $_active
     */
    public function getActive ()
    {
        return $this->_active;
    }
	/**
     * @param field_type $_active
     */
    public function setActive ($_active)
    {
        $this->_active = $_active;
    }
	/**
     * @return the $_category_order
     */
    public function getCategory_order ()
    {
        if(empty($this->_category_order))
	    {
	        $this->setCategory_order('');
	    }
	    return $this->_category_order;
    }
	/**
     * @param field_type $_category_order
     */
    public function setCategory_order ($_category_order)
    {
        if(!empty($_category_order))
        {
            $this->_category_order = $_category_order;
        }
        else
        {
            $group_id = $this->getGroup_id();
            $user_id = $this->getUser_id();
            $table_name		=	'b2b_category';
            $fields_arr		=	array('id','user_id','group_id','category_order');
            if(empty($this->_id))
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, null);
            }
            else
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $this->_id);
            }
            $OrderObj->setHeighestOrder($group_id,$user_id);
            $last_order = $OrderObj->getHighOrder();

            $this->_category_order = $last_order + 1;
        }
        return $this;
    }
	/**
     * @return the $_login
     */
    public function getLogin ()
    {
        return $this->_login;
    }
	/**
     * @param field_type $_login
     */
    public function setLogin ($_login)
    {
        $this->_login = $_login;
    }
	/**
     * @return the $_b2b_images
     */
    public function getB2b_images ()
    {
        return $this->_b2b_images;
    }
	/**
     * @param field_type $_b2b_images
     */
    public function setB2b_images ($_b2b_images)
    {
        $this->_b2b_images = $_b2b_images;
    }
	/**
     * @return the $_added_on
     */
    public function getAdded_on()
    {
        return $this->_added_on;
    }
	/**
     * @param field_type $_added_on
     */
    public function setAdded_on($_added_on)
    {
        $this->_added_on =  date("Y-m-d H:i:s");;
    }
	/**
     * @return the $_package_id
     */
    public function getPackage_id ()
    {
        return $this->_package_id;
    }
	/**
     * @param field_type $_package_id
     */
    public function setPackage_id ($_package_id)
    {
        $this->_package_id = $_package_id;
    }
	/**
     * @return the $_url_portion
     */
    public function getUrl_portion ()
    {
        return $this->_url_portion;
    }
	/**
     * @param field_type $_url_portion
     */
    public function setUrl_portion ($text,$id = null)
    {
        $pattern = Eicra_File_Constants::TITLE_PATTERN;
		$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
		$text = preg_replace($pattern, $replacement, trim($text));
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		if(empty($id))
		{		
			$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'b2b_category'), array('m.id'))
						->where('m.url_portion = ?',$text);
		}
		else
		{
			$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'b2b_category'), array('m.id'))
						->where('m.url_portion = ?',$text)->where('m.id != ?',$id);
		}
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$ids = (int)$row['id'];
			}
		}
		if(empty($ids))
		{
			$this->_url_portion = $text;
		}
		else
		{
			$select = $conn->select()
					->from(array('m' => Zend_Registry::get('dbPrefix').'b2b_category'), array('m.id'))
					->order(array('m.id DESC'))->limit(1);
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$last_id = (int)$row['id'];
				}
			}
			if(empty($id))
			{
				$this->_url_portion = $text.'-'.($last_id+1);
			}
			else
			{
				$this->_url_portion = $text.'-'.$id;
			}
		}
		return $this;
    }
	/**
     * @return the $_isComment
     */
    public function getIsComment ()
    {
        return $this->_isComment;
    }
	/**
     * @param field_type $_isComment
     */
    public function setIsComment ($_isComment)
    {
        $this->_isComment = $_isComment;
    }
	/**
     * @return the $_isShowProfile
     */
    public function getIsShowProfile ()
    {
        return $this->_isShowProfile;
    }
	/**
     * @param field_type $_isShowProfile
     */
    public function setIsShowProfile ($_isShowProfile)
    {
        $this->_isShowProfile = $_isShowProfile;
    }
	/**
     * @return the $_displayble
     */
    public function getDisplayble ()
    {
        return $this->_displayble;
    }
	/**
     * @param field_type $_displayble
     */
    public function setDisplayble ($_displayble)
    {
        $this->_displayble = $_displayble;
    }
	/**
     * @return the $group_id
     */
    public function getGroup_id ()
    {
        return $this->_group_id ;
    }

	/**
     * @param field_type $group_id
     */
    public function setGroup_id ($_group_id )
    {
        $this->_group_id  = $_group_id ;
    }
	/**
     * @return the $_featured
     */
    public function getFeatured ()
    {
        return $this->_featured;
    }

	/**
     * @param field_type $_featured
     */
    public function setFeatured ($_featured)
    {
        $this->_featured = $_featured;
    }



}