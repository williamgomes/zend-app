<?php

class B2b_Model_SuccessStoriesMapper
{

    protected $_dbTable;

    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable ()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('B2b_Model_DbTable_SuccessStories');
        }
        return $this->_dbTable;
    }

    public function save (B2b_Model_SuccessStories $obj)
    {
        $id = $obj->getId();
        $name = $obj->getName();
        $data = array(
            'id' => $obj->getId(),
            'user_id' => $obj->getUser_id(),
            'title' => $obj->getTitle(),
            'seo_title' => $obj->getSeo_title(),
            'name' => $obj->getName(),
            'description' => $obj->getDescription(),
            'b2b_images' => $obj->getB2b_images(),
            'active' => $obj->getActive(),
            'added_on' => $obj->getAdded_on()
        );
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $last_id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage()
                );
            }
        } else {
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));
                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }
        return $result;
    }

   public function fetchAll ($pageNumber, $approve = null, $search_params = null, $tableColumns = array('userChecking' => true))
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }

    public function delete ($id)
    {
        $result = null;
        try {
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->delete($where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }

    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );
        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
            return $result;
        } else {
            throw new Exception('Oops : Invalid Product or Status Selected ' . $id);
        }
    }

    public function getStories ($pageNumber, $approve = null, $search_params = null, $userChecking = true){

        $paginator = null;
        try {
            $resultSet = $this->getDbTable()->getAllStories($approve, $search_params, $userChecking);
            $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
            $paginator = Zend_Paginator::factory($resultSet);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);
            return $paginator;

        } catch (Exception $e) {
            return $e->getMessage() ;
        }
        return $paginator;
    }


    public function getStoryByTitle ($title)
    {
        $result = null;
        try {
            if ($title) {
                $result = $this->getDbTable()->getStoryByTitle($title, '1');
            }
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }


    public function displayable ($id, $displayable)
    {
        $result = null;
        try {
            $data = array(
                    'displayable' => $displayable
            );
            $where = array(
                    'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                    'status' => 'ok',
                    'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
            );
        }
        return $result;
    }
}