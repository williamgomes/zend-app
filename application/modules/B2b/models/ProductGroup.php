<?php
class B2b_Model_ProductGroup {
	
private $_id;
private $_user_id;
private $_name;
private $_description;
private $_active;
private $_image;
	 
	 
	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth hotels');
        }
        $this->$method($value);
    }
    
    
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth hotels');
        }
        return $this->$method();
    }
    
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
	 
    public function getId ()
    {
        return $this->_id;
    }
	/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }
	/**
     * @return the $_user_id
     */
    public function getUser_id ()
    {
        return $this->_user_id;
    }
	/**
     * @param field_type $_user_id
     */
    public function setUser_id ($_user_id)
    {
        $this->_user_id = $_user_id;
    }
	/**
     * @return the $_name
     */
    public function getName ()
    {
        return $this->_name;
    }
	/**
     * @param field_type $_name
     */
    public function setName ($_name)
    {
        $this->_name = $_name;
    }
	/**
     * @return the $_description
     */
    public function getDescription ()
    {
        return $this->_description;
    }
	/**
     * @param field_type $_description
     */
    public function setDescription ($_description)
    {
        $this->_description = $_description;
    }
	/**
     * @return the $_active
     */
    public function getActive ()
    {
        return $this->_active;
    }
	/**
     * @param field_type $_active
     */
    
    public function setActive ($_active)
    {
        $this->_active = $_active;
    }
    

    public function saveGroup ()
    {
        
        $mapper  = new B2b_Model_ProductGroupMapper();
        $return = $mapper->save($this);
        return $return;
    }
	/**
     * @return the $_image
     */
    public function getImage ()
    {
        return $this->_image;
    }

	/**
     * @param field_type $_image
     */
    public function setImage ($_image)
    {
        $this->_image = $_image;
    }

        
}