<?php
class B2b_Model_SuccessStories {

	protected $_id ;
	protected $_user_id ;
	protected $_title  ;
	protected $_name  ;
	protected $_description ;
	protected $_b2b_images ;
	protected $_active ;
	protected $_added_on ;
	protected $_seo_title ;


	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    public function save()
    {
        $mapper  = new B2b_Model_SuccessStoriesMapper();
        $return = $mapper->save($this);
        return $return;
    }
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}
	/**
	 * @return the $_user_id
	 */
	public function getUser_id() {
		return $this->_user_id;
	}
	/**
	 * @param field_type $_user_id
	 */
	public function setUser_id($_user_id) {
		$this->_user_id = $_user_id;
	}
	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}
	/**
	 * @return the $_title
	 */
	public function getTitle() {
		return $this->_title;
	}
	/**
	 * @param field_type $_title
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
	}
	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}
	/**
	 * @param field_type $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}
	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}
	/**
	 * @param field_type $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}
	/**
	 * @return the $_b2b_images
	 */
	public function getB2b_images() {
		return $this->_b2b_images;
	}
	/**
	 * @param field_type $_b2b_images
	 */
	public function setB2b_images($_b2b_images) {
		$this->_b2b_images = $_b2b_images;
	}
	/**
	 * @return the $_active
	 */
	public function getActive() {
		return $this->_active;
	}
	/**
	 * @param field_type $_active
	 */
	public function setActive($_active) {
		$this->_active = $_active;
	}
	/**
	 * @return the $_added_on
	 */
	public function getAdded_on() {
		return $this->_added_on;
	}
	/**
	 * @param field_type $_added_on
	 */
	public function setAdded_on($_added_on) {
		$this->_added_on = $_added_on;
	}
	/**
     * @return the $_seo_title
     */
    public function getSeo_title ()
    {
        return $this->_seo_title;
    }

	/**
     * @param field_type $_seo_title
     */
    public function setSeo_title ($text,$user_id = null)
    {

        $pattern = Eicra_File_Constants::TITLE_PATTERN;
        $replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
        $seo_text = preg_replace($pattern, $replacement, trim($text));

        $this->_seo_title = $seo_text  . Eicra_File_Constants::TITLE_REPLACEMENT
                                              . $user_id ;
    }


}
