<?php
class B2b_Model_Tradeshow {

	protected $_id ;
    protected $_user_id ;
    protected $_name ;
    protected $_title ;
    protected $_seo_title ;
    protected $_organizer ;
    protected $_start_date;
    protected $_end_date;
    protected $_street ;
    protected $_state ;
    protected $_city ;
    protected $_country ;
    protected $_postal_code ;
    protected $_timepicker ;
    protected $_description ;
    protected $_b2b_images ;
    protected $_added_on ;
    protected $_venue ;
    protected $_address ;
    protected $_exhibitors ;
    protected $_attendees ;
    protected $_floor ;
    protected $_size ;
    protected $_phone ;
    protected $_phone_part2 ;
    protected $_phone_part3 ;
    protected $_fax ;
    protected $_fax_part2 ;
    protected $_fax_part3 ;
    protected $_website ;
    protected $_focus ;
    protected $_product_service ;
    protected $_theme ;
    protected $_homepage ;
    protected $_logo ;
    protected $_summary ;
    protected $_attendee_info ;
    protected $_exhibitor_info ;
    protected $_displayable ;
    protected $_active ;
    protected $_status;


	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    public function save()
    {
        $mapper  = new B2b_Model_TradeshowMapper();
        $return = $mapper->save($this);
        return $return;
    }
	/**
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }

	/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }

	/**
     * @return the $_user_id
     */
    public function getUser_id ()
    {
        return $this->_user_id;
    }

	/**
     * @param field_type $_user_id
     */
    public function setUser_id ($_user_id)
    {
        $this->_user_id = $_user_id;
    }

	/**
     * @return the $_name
     */
    public function getName ()
    {
        return $this->_name;
    }

	/**
     * @param field_type $_name
     */
    public function setName ($_name)
    {
        $this->_name = $_name;
    }

	/**
     * @return the $_title
     */
    public function getTitle ()
    {
        return $this->_title;
    }

	/**
     * @param field_type $_title
     */
    public function setTitle ($_title)
    {
        $this->_title = $_title;
    }

	/**
     * @return the $_organizer
     */
    public function getOrganizer ()
    {
        return $this->_organizer;
    }

	/**
     * @param field_type $_organizer
     */
    public function setOrganizer ($_organizer)
    {
        $this->_organizer = $_organizer;
    }

	/**
     * @return the $_description
     */
    public function getDescription ()
    {
        return $this->_description;
    }

	/**
     * @param field_type $_description
     */
    public function setDescription ($_description)
    {
        $this->_description = $_description;
    }


	/**
     * @return the $_added_on
     */
    public function getAdded_on ()
    {
        return $this->_added_on;
    }

	/**
     * @param field_type $_added_on
     */
    public function setAdded_on ($_added_on)
    {
        $this->_added_on = $_added_on;
    }

	/**
     * @return the $_venue
     */
    public function getVenue ()
    {
        return $this->_venue;
    }

	/**
     * @param field_type $_venue
     */
    public function setVenue ($_venue)
    {
        $this->_venue = $_venue;
    }

	/**
     * @return the $_address
     */
    public function getAddress ()
    {
        return $this->_address;
    }

	/**
     * @param field_type $_address
     */
    public function setAddress ($_address)
    {
        $this->_address = $_address;
    }

	/**
     * @return the $_exhibitors
     */
    public function getExhibitors ()
    {
        return $this->_exhibitors;
    }

	/**
     * @param field_type $_exhibitors
     */
    public function setExhibitors ($_exhibitors)
    {
        $this->_exhibitors = $_exhibitors;
    }

	/**
     * @return the $_attendees
     */
    public function getAttendees ()
    {
        return $this->_attendees;
    }

	/**
     * @param field_type $_attendees
     */
    public function setAttendees ($_attendees)
    {
        $this->_attendees = $_attendees;
    }

	/**
     * @return the $_floor
     */
    public function getFloor ()
    {
        return $this->_floor;
    }

	/**
     * @param field_type $_floor
     */
    public function setFloor ($_floor)
    {
        $this->_floor = $_floor;
    }

	/**
     * @return the $_size
     */
    public function getSize ()
    {
        return $this->_size;
    }

	/**
     * @param field_type $_size
     */
    public function setSize ($_size)
    {
        $this->_size = $_size;
    }

	/**
     * @return the $_phone
     */
    public function getPhone ()
    {
        return $this->_phone;
    }

	/**
     * @param field_type $_phone
     */
    public function setPhone ($_phone)
    {
        $this->_phone = $_phone;
    }

	/**
     * @return the $_phone_part2
     */
    public function getPhone_part2 ()
    {
        return $this->_phone_part2;
    }

	/**
     * @param field_type $_phone_part2
     */
    public function setPhone_part2 ($_phone_part2)
    {
        $this->_phone_part2 = $_phone_part2;
    }

	/**
     * @return the $_phone_part3
     */
    public function getPhone_part3 ()
    {
        return $this->_phone_part3;
    }

	/**
     * @param field_type $_phone_part3
     */
    public function setPhone_part3 ($_phone_part3)
    {
        $this->_phone_part3 = $_phone_part3;
    }

	/**
     * @return the $_fax
     */
    public function getFax ()
    {
        return $this->_fax;
    }

	/**
     * @param field_type $_fax
     */
    public function setFax ($_fax)
    {
        $this->_fax = $_fax;
    }

	/**
     * @return the $_fax_part2
     */
    public function getFax_part2 ()
    {
        return $this->_fax_part2;
    }

	/**
     * @param field_type $_fax_part2
     */
    public function setFax_part2 ($_fax_part2)
    {
        $this->_fax_part2 = $_fax_part2;
    }

	/**
     * @return the $_fax_part3
     */
    public function getFax_part3 ()
    {
        return $this->_fax_part3;
    }

	/**
     * @param field_type $_fax_part3
     */
    public function setFax_part3 ($_fax_part3)
    {
        $this->_fax_part3 = $_fax_part3;
    }

	/**
     * @return the $_website
     */
    public function getWebsite ()
    {
        return $this->_website;
    }

	/**
     * @param field_type $_website
     */
    public function setWebsite ($_website)
    {
        $this->_website = $_website;
    }

	/**
     * @return the $_focus
     */
    public function getFocus ()
    {
        return $this->_focus;
    }

	/**
     * @param field_type $_focus
     */
    public function setFocus ($_focus)
    {
        $this->_focus = $_focus;
    }

	/**
     * @return the $_product_service
     */
    public function getProduct_service ()
    {
        return $this->_product_service;
    }

	/**
     * @param field_type $_product_service
     */
    public function setProduct_service ($_product_service)
    {
        $this->_product_service = $_product_service;
    }

	/**
     * @return the $_theme
     */
    public function getTheme ()
    {
        return $this->_theme;
    }

	/**
     * @param field_type $_theme
     */
    public function setTheme ($_theme)
    {
        $this->_theme = $_theme;
    }

	/**
     * @return the $_homepage
     */
    public function getHomepage ()
    {
        return $this->_homepage;
    }

	/**
     * @param field_type $_homepage
     */
    public function setHomepage ($_homepage)
    {
        $this->_homepage = $_homepage;
    }

	/**
     * @return the $_logo
     */
    public function getLogo ()
    {
        return $this->_logo;
    }

	/**
     * @param field_type $_logo
     */
    public function setLogo ($_logo)
    {
        $this->_logo = $_logo;
    }

	/**
     * @return the $_summary
     */
    public function getSummary ()
    {
        return $this->_summary;
    }

	/**
     * @param field_type $_summary
     */
    public function setSummary ($_summary)
    {
        $this->_summary = $_summary;
    }

	/**
     * @return the $_attendee_info
     */
    public function getAttendee_info ()
    {
        return $this->_attendee_info;
    }

	/**
     * @param field_type $_attendee_info
     */
    public function setAttendee_info ($_attendee_info)
    {
        $this->_attendee_info = $_attendee_info;
    }

	/**
     * @return the $_exhibitor_info
     */
    public function getExhibitor_info ()
    {
        return $this->_exhibitor_info;
    }

	/**
     * @param field_type $_exhibitor_info
     */
    public function setExhibitor_info ($_exhibitor_info)
    {
        $this->_exhibitor_info = $_exhibitor_info;
    }


    public function getActive ()
    {
        return $this->_active;
    }

	/**
     * @param field_type $_active
     */
    public function setActive ($_active)
    {
        $this->_active = $_active;
    }


    /**
     * @return the $_seo_title
     */
    public function getSeo_title ()
    {
        return $this->_seo_title;
    }
	/**
     * @return the $_street
     */
    public function getStreet ()
    {
        return $this->_street;
    }

	/**
     * @param field_type $_street
     */
    public function setStreet ($_street)
    {
        $this->_street = $_street;
    }

	/**
     * @return the $_state
     */
    public function getState ()
    {
        return $this->_state;
    }

	/**
     * @param field_type $_state
     */
    public function setState ($_state)
    {
        $this->_state = $_state;
    }

	/**
     * @return the $_city
     */
    public function getCity ()
    {
        return $this->_city;
    }

	/**
     * @param field_type $_city
     */
    public function setCity ($_city)
    {
        $this->_city = $_city;
    }

	/**
     * @return the $_country
     */
    public function getCountry ()
    {
        return $this->_country;
    }

	/**
     * @param field_type $_country
     */
    public function setCountry ($_country)
    {
        $this->_country = $_country;
    }

	/**
     * @return the $_postal_code
     */
    public function getPostal_code ()
    {
        return $this->_postal_code;
    }

	/**
     * @param field_type $_postal_code
     */
    public function setPostal_code ($_postal_code)
    {
        $this->_postal_code = $_postal_code;
    }

	/**
     * @param string $_seo_title
     */
    public function setSeo_title ($text,$user_id = null)
    {
        $pattern = Eicra_File_Constants::TITLE_PATTERN;
        $replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
        $seo_text = preg_replace($pattern, $replacement, trim($text));

        $this->_seo_title = $seo_text  . Eicra_File_Constants::TITLE_REPLACEMENT
                                              . $user_id ;
    }
	/**
     * @return the $_displayable
     */
    public function getDisplayable ()
    {
        return $this->_displayable;
    }

	/**
     * @param field_type $_displayable
     */
    public function setDisplayable ($_displayable)
    {
        $this->_displayable = $_displayable;
    }


    public function getB2b_images ()
    {
        return $this->_b2b_images;
    }


    public function setB2b_images ($_b2b_images)
    {
        $this->_b2b_images = $_b2b_images;
    }


	/**
     * @return the $_timepicker
     */
    public function getTimepicker ()
    {
        return $this->_timepicker;
    }


	/**
     * @param field_type $_timepicker
     */
    public function setTimepicker ($_timepicker)
    {
        $this->_timepicker = $_timepicker;
    }
	/**
     * @return the $_start_date
     */
    public function getStart_date ()
    {
        return $this->_start_date;
    }

	/**
     * @param field_type $_start_date
     */
    public function setStart_date ($_start_date)
    {
//        $this->_start_date = $_start_date;
        $locale = Eicra_Global_Variable::getSession()->sess_lang;
		   
        if($_start_date)
        {
                $data_arr = preg_split('/[- :]/',$_start_date);
                if($data_arr[0])
                {
                        $date_obj = new Zend_Date($_start_date, null, $locale);
                        //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                        $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", $time);
                }
        }
        else
        {
                $date_obj = new Zend_Date((time() + (10 * 365 * 24 * 60 *60)), null, $locale);
                $this->_time_of_expiry = $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
        }
    }

	/**
     * @return the $_end_date
     */
    public function getEnd_date ()
    {
        return $this->_end_date;
    }

	/**
     * @param field_type $_end_date
     */
    public function setEnd_date ($_end_date)
    {
//        $this->_end_date = $_end_date;
        
        $locale = Eicra_Global_Variable::getSession()->sess_lang;
		   
        if($_end_date)
        {
                $data_arr = preg_split('/[- :]/',$_end_date);
                if($data_arr[0])
                {
                        $date_obj = new Zend_Date($_end_date, null, $locale);
                        //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                        $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", $time);
                }
        }
        else
        {
                $date_obj = new Zend_Date((time() + (10 * 365 * 24 * 60 *60)), null, $locale);
                $this->_time_of_expiry = $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
        }
    }
	/**
	 * @return the $_status
	 */
	public function getStatus() {
		return $this->_status;
	}

	/**
	 * @param field_type $_status
	 */
	public function setStatus($_status) {
		$this->_status = $_status;
	}




}
