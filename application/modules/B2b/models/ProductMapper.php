<?php
class B2b_Model_ProductMapper
{
    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided : ' . serialize($dbTable));
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    public function getDbTable ()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('B2b_Model_DbTable_Products');
        }
        return $this->_dbTable;
    }
    public function save (B2b_Model_Product $obj)
    {
        $id = $obj->getId();
        $data = array(
            'id' => $obj->getId(),
            'user_id' => $obj->getUser_id(),
            'product_grp_id' => $obj->getProduct_grp_id(),
            'group_id' => $obj->getGroup_id(),
            'name' => $obj->getName(),
            'keyword' => $obj->getKeyword(),
            'keyword_1' => $obj->getKeyword_1(),
            'keyword_2' => $obj->getKeyword_2(),
            'keyword_3' => $obj->getKeyword_3(),
            'seo_title' => $obj->getSeo_title(),
            'short_description' => $obj->getShort_description(),
            'detail_description' => $obj->getDetail_description(),
            'product_images_primary' => $obj->getProduct_images_primary(),
            'product_images' => $obj->getProduct_images(),
        	'price_currency_locale' => $obj->getPrice_currency_locale(),
            'price_currency' => $obj->getPrice_currency(),
            'min_unit_type' => $obj->getMin_unit_type(),
            'max_unit_type' => $obj->getMax_unit_type(),
            'price_per_unit' => $obj->getPrice_per_unit(),
            'unit_size' => $obj->getUnit_size(),
            'delivery_leadtime' => $obj->getDelivery_leadtime(),
            'size' => $obj->getSize(),
            'type' => $obj->getType(),
            'specifications' => $obj->getSpecifications(),
            'certificates' => $obj->getCertificates(),
            'packaging' => $obj->getPackaging(),
            'brand_name' => $obj->getBrand_name(),
            'model_num' => $obj->getModel_num(),
            'payment_terms' => $obj->getPayment_terms(),
            'hs_code' => $obj->getHs_code(),
            'manufacturers' => $obj->getManufacturers(),
            'place_of_origin' => $obj->getPlace_of_origin(),
            'brochures' => $obj->getBrochures(),
            'brochures_primary' => $obj->getBrochures_primary(),
            'video' => $obj->getVideo(),
            'terms_policy' => $obj->getTerms_policy(),
            'contract_period' => $obj->getContract_period(),
            'warranty' => $obj->getWarranty(),
            'guarantee' => $obj->getGuarantee(),
            'related_items' => $obj->getRelated_items(),
            'colour' => $obj->getColour(),
            'min_bidding_price' => $obj->getMin_bidding_price(),
            'materials' => $obj->getMaterials(),
            'shipment' => $obj->getShipment(),
            'addtional_key' => $obj->getAddtional_key(),
            'addtional_value' => $obj->getAddtional_value(),
            'time_of_expiry' => $obj->getTime_of_expiry(),
            'fob_price' => $obj->getFob_price(),
            'max_supply' => $obj->getMax_supply(),
            'min_quantity' => $obj->getMin_quantity(),
            'strikethrough_price' => $obj->getStrikethrough_price(),
            'price' => $obj->getPrice(),
            'displayble' => $obj->getDisplayble(),
            'category_id' => $obj->getCategory_id(),
            'negotiable' => $obj->getNegotiable(),
            'added_on' => $obj->getAdded_on(),
            'active' => $obj->getActive()
        )
        ;
		
        if ($id == null || empty($id)) {
            unset($data['id']);
            try
            {
                $data['product_order'] = $obj->getProduct_order();
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            }
            catch (Exception $e)
            {
                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage()
                );
            }
        }
        else {
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));
                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            }
            catch (Exception $e)
            {
                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }
        return $result;
    }
    public function displayable ($id, $displayble)
    {
        $result = null;
        try {
            $data = array(
                'displayble' => $displayble
            );
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }
    public function featured ($id, $featured)
    {
        $result = null;
        try {
            $data = array(
                    'featured' => $featured
            );
            $where = array(
                    'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                    'status' => 'ok',
                    'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
            );
        }
        return $result;
    }
    public function delete ($id)
    {
        $result = null;
        try {
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->delete($where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }
    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );
        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
            return $result;
        }
        else {
            throw new Exception('Invalid Product or Status Selected ' . $id);
        }
    }
    private function getActivationStatus ()
    {
        $productTable = new B2b_Model_DbTable_Preferences();
        $value = $productTable->getSettingByKey('auto_publish_product');
        $status = $value['value'];
        if ($status) {
            return '1';
        } else {
            return '0';
        }
    }
    public function fetchAll ($pageNumber, $approve = null, $search_params = null, $tableColumns)
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
    public function getProducts ($pageNumber, $approve = null, $search_params = null, $userChecking = true,$field_arr = null){
        $paginator = null;
        try {
            $resultSet = $this->getDbTable()->getOffers($approve, $search_params, $userChecking );
            $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
            $paginator = Zend_Paginator::factory($resultSet);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);
            return $paginator;
        } catch (Exception $e) {
            return null ;
        }
        return $paginator;
    }
    public function getLeadsByCompany ($pageNumber, $approve = null, $search_params = null, $userChecking = true, $user_id){
        $paginator = null;
        try {
            $resultSet = $this->getDbTable()->getOffersByCompany($approve, $search_params, $userChecking, $user_id);
            $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
            $paginator = Zend_Paginator::factory($resultSet);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);
            return $paginator;
        } catch (Exception $e) {
            return null ;
        }
        return $paginator;
    }
}