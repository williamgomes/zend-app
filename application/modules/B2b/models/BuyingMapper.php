<?php
class B2b_Model_BuyingMapper
{
	protected $_dbTable;
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('B2b_Model_DbTable_Buying');
		}
		return $this->_dbTable;
	}
	public function save(B2b_Model_Buying $obj)
	{
		$id = $obj->getId();
		$data = array(
				'id' 					=> 	$obj->getId(),
				'user_id' 				=> 	$obj->getUser_id(),
		        'group_id'              =>  $obj->getGroup_id(),
				'category' 				=> 	$obj->getCategory(),
				'name' 					=> 	$obj->getName(),
				'keyword' 				=> 	$obj->getKeyword(),
				'keyword_1' 			=> 	$obj->getKeyword_1(),
				'keyword_2' 			=> 	$obj->getKeyword_2(),
				'keyword_3' 			=> 	$obj->getKeyword_3(),
		        'seo_title'             =>  $obj->getSeo_title(),
				'description' 			=> 	$obj->getDescription(),
				'displayble' 			=> 	$obj->getDisplayble(),
				'quantity' 				=> 	$obj->getQuantity(),
				'qty_per_unit' 			=> 	$obj->getQty_per_unit(),
				'price_currency_locale' => $obj->getPrice_currency_locale(),
				'price_currency' 		=> 	$obj->getPrice_currency(),
				'price' 				=> 	$obj->getPrice(),
				'price_per_unit' 		=> 	$obj->getPrice_per_unit(),
				'buying_capacity' 		=> 	$obj->getBuying_capacity(),
				'max_buying_unit' 		=> 	$obj->getMax_buying_unit(),
				'subject' 				=> 	$obj->getSubject(),
				'validity' 				=> 	$obj->getValidity(),
				'specifications' 		=> 	$obj->getSpecifications(),
				'packaging' 			=> 	$obj->getPackaging(),
				'delivery' 				=> 	$obj->getDelivery(),
				'delivery_leadtime' 	=> 	$obj->getDelivery_leadtime(),
				'primary_file_field' 	=> 	$obj->getPrimary_file_field(),
				'others_images' 		=> 	$obj->getOthers_images(),
				'shipping' 				=> 	$obj->getShipping(),
				'shipping_price' 		=> 	$obj->getShipping_price(),
				'payment_type' 			=> 	$obj->getPayment_type(),
				'company_name' 			=> 	$obj->getCompany_name(),
				'email' 				=> 	$obj->getEmail(),
				'country' 				=> 	$obj->getCountry(),
				'address' 				=> 	$obj->getAddress(),
				'preference' 			=> 	$obj->getPreference(),
				'website' 				=> 	$obj->getWebsite(),
				'business_type' 		=> 	$obj->getBusiness_type(),
				'active' 				=> 	$obj->getActive()
		);
		if ((null === ($id = $obj->getId())) || empty($id))
		{
			unset($data['id']);
			try
			{
			    $data['buying_order'] = $obj->getBuying_order();
				$last_id = $this->getDbTable()->insert($data);
				$result = array('status' => 'ok' ,'id' => $last_id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());
			}
		}
		else
		{

			// Start the Update process
			try
			{
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
			}
		}
		return $result;
	}
	public function fetchAll ($pageNumber, $approve = null, $search_params = null,  $tableColumns = null)
	{
	    $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
	    $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
	    $paginator = Zend_Paginator::factory($resultSet);
	    $paginator->setItemCountPerPage($viewPageNum);
	    $paginator->setCurrentPageNumber($pageNumber);
	    return $paginator;
	}

	public function delete ($id){
		$result = null;
		try {
			$where = array('id = ?' => $id);
			$this->getDbTable()->delete($where);
			$result = array('status' => 'ok' ,'id' => $id);
		} catch (Exception $e) {
			$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
		}
		return $result;
	}
	public function updateStatus($data , $id){
	    $where = array('id = ?' => $id);
	    if ((!empty($id) && is_numeric($id)) && !empty($data)  ){
	        $resultSet = $this->getDbTable()->update($data, $where);
	        $result = array('status' => 'ok' ,'id' => $id);
	        return $result;
	    }
	    else {
	        throw new Exception('Invalid Product or Status Selected ' . $id);
	    }
	}
	private function getActivationStatus (){
	    $productTable = new B2b_Model_DbTable_Preferences();
	    $value =  $productTable->getSettingByKey('auto_publish_buylead');
	    $status = $value['value'];
	    if ($status){
	        return '1';
	    }
	    else {
	        return '0';
	    }
	}
	public function displayable ($id, $displayble)
	{
	    $result = null;
	    try
	    {
	        $data = array('displayble' => $displayble);
	        $where = array('id = ?' => $id);
	        $this->getDbTable()->update($data, $where);
	        $result = array('status' => 'ok' ,'id' => $id);
	    }
	    catch (Exception $e)
	    {
	        $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
	    }
	    return $result;
	}


	public function featured ($id, $featured)
	{
	    $result = null;

	    try {
	        $data = array(
	                'featured' => $featured
	        );

	        $where = array(
	                'id = ?' => $id
	        );
	        $this->getDbTable()->update($data, $where);
	        $result = array(
	                'status' => 'ok',
	                'id' => $id
	        );
	    } catch (Exception $e) {
	        $result = array(
	                'status' => 'err',
	                'id' => $id,
	                'msg' => $e->getMessage()
	        );
	    }
	    return $result;
	}
	public function getExistingLeads ($user_id){
	    $result = null;
	    try
	    {
	        $result = $this->getDbTable()->getExistingLeads($user_id);
	    }
	    catch (Exception $e)
	    {
	        $result = null;
	    }
	    return $result;
	}
}