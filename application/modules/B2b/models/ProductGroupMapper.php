<?php
class B2b_Model_ProductGroupMapper
{
    
    public function setDbTable($dbTable)
    
    {
    
        if (is_string($dbTable)) {
    
            $dbTable = new $dbTable();
  
        }
    
    
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
    
            throw new Exception('Invalid table data gateway provided : ' . serialize($dbTable));
    
        }
    
        $this->_dbTable = $dbTable;
    
        return $this;
    
    }
    
    
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
    
            $this->setDbTable('B2b_Model_DbTable_ProductGroup');
        }
    
        return $this->_dbTable;
    
    }
    
    
    public function save(B2b_Model_ProductGroup $obj)
    {
    
    
        $id = $obj->getId();
         
    
        $data = array(
                'user_id' 				=>	$obj->getUser_id(),
                'name' 		            =>	$obj->getName(),
                'image'                 =>  $obj->getImage(),
                'Description' 		    =>	$obj->getDescription(),
                'active' 		        =>	$obj->getActive()
        );
      
    
    
        if ($id ==  null|| empty($id))
        {
            unset($data['id']);
    
    
            try
    
    
            {
    
    
                $last_id = $this->getDbTable()->insert($data);
    
    
                $result = array('status' => 'ok' ,'id' => 'not id  ' . $id);
    
    
            }
    
    
    
            catch (Exception $e)
    
            {
    
                $result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage() . serialize($profileDB) );
        
            }
    
    
    
        }
    
    
    
        else
    
    
        {
    
    
            // Start the Update process
    
    
            try
            {
    
                $this->getDbTable()->update($data, array('id = ?' => $id));
    
    
                $result = array('status' => 'ok' ,'id' => $id);
    
    
            }
    
    
            catch (Exception $e)
    
    
            {
    
    
                $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
    
            }
    
        }
    
    
    
        return $result;
    
    }
    
    public function delete ($id){
        
        $result = null;
        try {
            $where = array('id = ?' => $id);
            $this->getDbTable()->delete($where);
            $result = array('status' => 'ok' ,'id' => $id);
            
        } catch (Exception $e) {
            
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
            
        }
       
        return $result;
    }
    
}