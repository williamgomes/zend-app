<?php

class B2b_Model_Product {

    private $_id;
    private $_user_id;
    private $_product_grp_id;
    private $_group_id;
    private $_name;
    private $_keyword;
    private $_keyword_1;
    private $_keyword_2;
    private $_keyword_3;
    private $_seo_title;
    private $_short_description;
    private $_detail_description;
    private $_product_images_primary;
    private $_product_images;
    private $_price_currency_locale;
    private $_price_currency;
    private $_min_unit_type;
    private $_max_unit_type;
    private $_price_per_unit;
    private $_unit_size;
    private $_delivery_leadtime;
    private $_size;
    private $_type;
    private $_specifications;
    private $_certificates;
    private $_packaging;
    private $_brand_name;
    private $_model_num;
    private $_payment_terms;
    private $_hs_code;
    private $_manufacturers;
    private $_place_of_origin;
    private $_brochures;
    private $_brochures_primary;
    private $_video;
    private $_terms_policy;
    private $_contract_period;
    private $_warranty;
    private $_guarantee;
    private $_related_items;
    private $_colour;
    private $_min_bidding_price;
    private $_materials;
    private $_shipment;
    private $_addtional_key;
    private $_addtional_value;
    private $_time_of_expiry;
    private $_fob_price;
    private $_max_supply;
    private $_min_quantity;
    private $_strikethrough_price;
    private $_price;
    private $_displayble;
    private $_Active;
    private $_category_id;
    private $_negotiable;
    private $_product_order;
    private $_added_on;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Setter Property - ' . $method);
        }
        $this->method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Getter Property - ' . $method);
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function save() {
        $mapper = new B2b_Model_ProductMapper();
        $return = $mapper->save($this);
        return $return;
    }

    /**
     * @return the $_product_order
     */
    public function getProduct_order() {
        if (empty($this->_product_order)) {
            $this->setProduct_order('');
        }
        return $this->_product_order;
    }

    /**
     * @param field_type $_id
     */
    public function setProduct_order($text) {
        if (!empty($text)) {
            $this->_product_order = $text;
        } else {
            $group_id = $this->getGroup_id();
            $category_id = $this->getProduct_grp_id();
            $table_name = 'b2b_products';
            $fields_arr = array('id', 'group_id', 'product_grp_id', 'product_order');
            if (empty($this->_id)) {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, null);
            } else {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $this->_id);
            }
            $OrderObj->setHeighestOrder($group_id, $category_id);
            $last_order = $OrderObj->getHighOrder();

            $this->_product_order = $last_order + 1;
        }
        return $this;
    }

    /**
     * @return the $_id
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param field_type $_id
     */
    public function setId($_id) {
        $this->_id = $_id;
    }

    /**
     * @return the $_user_id
     */
    public function getUser_id() {
        return $this->_user_id;
    }

    /**
     * @param field_type $_user_id
     */
    public function setUser_id($_user_id) {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            $this->_user_id = ($globalIdentity->allow_to_change_ownership != '1' || empty($_user_id)) ? $globalIdentity->user_id : $_user_id;
        } else {
            $this->_user_id = $_user_id;
        }
        return $this;
    }

    /**
     * @return the $_product_grp_id
     */
    public function getProduct_grp_id() {
        return $this->_product_grp_id;
    }

    /**
     * @param field_type $_product_grp_id
     */
    public function setProduct_grp_id($_product_grp_id) {
        $this->_product_grp_id = $_product_grp_id;
    }

    /**
     * @return the $_group_id
     */
    public function getGroup_id() {
        return $this->_group_id;
    }

    /**
     * @param field_type $_group_id
     */
    public function setGroup_id($_group_id) {
        $this->_group_id = $_group_id;
    }

    /**
     * @return the $_name
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param field_type $_name
     */
    public function setName($_name) {
        $this->_name = $_name;
    }

    /**
     * @return the $_keyword
     */
    public function getKeyword() {
        return $this->_keyword;
    }

    /**
     * @param field_type $_keyword
     */
    public function setKeyword($_keyword) {
        $this->_keyword = $_keyword;
    }

    /**
     * @return the $_keyword_1
     */
    public function getKeyword_1() {
        return $this->_keyword_1;
    }

    /**
     * @param field_type $_keyword_1
     */
    public function setKeyword_1($_keyword_1) {
        $this->_keyword_1 = $_keyword_1;
    }

    /**
     * @return the $_keyword_2
     */
    public function getKeyword_2() {
        return $this->_keyword_2;
    }

    /**
     * @param field_type $_keyword_2
     */
    public function setKeyword_2($_keyword_2) {
        $this->_keyword_2 = $_keyword_2;
    }

    /**
     * @return the $_keyword_3
     */
    public function getKeyword_3() {
        return $this->_keyword_3;
    }

    /**
     * @param field_type $_keyword_3
     */
    public function setKeyword_3($_keyword_3) {
        $this->_keyword_3 = $_keyword_3;
    }

    /**
     * @return the $_seo_title
     */
    public function getSeo_title() {
        return $this->_seo_title;
    }

    /**
     * @param field_type $_seo_title
     */
    public function setSeo_title($text, $id = null) {
        $pattern = Eicra_File_Constants::TITLE_PATTERN;
        $replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
        $text = preg_replace($pattern, $replacement, trim($text));

        // DB Connection		
        $product_db = new B2b_Model_DbTable_Products();

        if (empty($id)) {
            $select = $product_db->select()->from(array('p' => Zend_Registry::get('dbPrefix') . 'b2b_products'), array('p.id'))
                    ->where('p.seo_title = ?', $text);
        } else {
            $select = $product_db->select()->from(array('p' => Zend_Registry::get('dbPrefix') . 'b2b_products'), array('p.id'))
                            ->where('p.seo_title = ?', $text)->where('p.id != ?', $id);
        }

        $rs = $select->query()->fetchAll();
        if ($rs) {
            foreach ($rs as $row) {
                $ids = (int) $row ['id'];
            }
        }
        if (empty($ids)) {
            $this->_seo_title = $text;
        } else {
            $select = $product_db->select()->from(array('p' => Zend_Registry::get('dbPrefix') . 'b2b_products'), array('p.id'))
                    ->order(array('p.id DESC'))
                    ->limit(1);

            $rs = $select->query()->fetchAll();
            if ($rs) {
                foreach ($rs as $row) {
                    $last_id = (int) $row ['id'];
                }
            }

            if (empty($id)) {
                $this->_seo_title = $text . '-' . ($last_id + 1);
            } else {
                $this->_seo_title = $text . '-' . $id;
            }
        }
        return $this;
    }

    /**
     * @return the $_short_description
     */
    public function getShort_description() {
        return $this->_short_description;
    }

    /**
     * @param field_type $_short_description
     */
    public function setShort_description($_short_description) {
        $this->_short_description = $_short_description;
    }

    /**
     * @return the $_detail_description
     */
    public function getDetail_description() {
        return $this->_detail_description;
    }

    /**
     * @param field_type $_detail_description
     */
    public function setDetail_description($_detail_description) {
        $this->_detail_description = $_detail_description;
    }

    /**
     * @return the $_product_images_primary
     */
    public function getProduct_images_primary() {
        return $this->_product_images_primary;
    }

    /**
     * @param field_type $_product_images_primary
     */
    public function setProduct_images_primary($_product_images_primary) {
        $this->_product_images_primary = $_product_images_primary;
    }

    /**
     * @return the $_product_images
     */
    public function getProduct_images() {
        return $this->_product_images;
    }

    /**
     * @param field_type $_product_images
     */
    public function setProduct_images($_product_images) {
        $this->_product_images = $_product_images;
    }

    /**
     * @return the $_price_currency
     */
    public function getPrice_currency() {
        return $this->_price_currency;
    }

    /**
     * @param field_type $_price_currency
     */
    public function setPrice_currency($_price_currency) {
        $this->_price_currency = $_price_currency;
    }

    /**
     * @return the $_min_unit_type
     */
    public function getMin_unit_type() {
        return $this->_min_unit_type;
    }

    /**
     * @param field_type $_min_unit_type
     */
    public function setMin_unit_type($_min_unit_type) {
        $this->_min_unit_type = $_min_unit_type;
    }

    /**
     * @return the $_max_unit_type
     */
    public function getMax_unit_type() {
        return $this->_max_unit_type;
    }

    /**
     * @param field_type $_max_unit_type
     */
    public function setMax_unit_type($_max_unit_type) {
        $this->_max_unit_type = $_max_unit_type;
    }

    /**
     * @return the $_price_per_unit
     */
    public function getPrice_per_unit() {
        return $this->_price_per_unit;
    }

    /**
     * @param field_type $_price_per_unit
     */
    public function setPrice_per_unit($_price_per_unit) {
        $this->_price_per_unit = $_price_per_unit;
    }

    /**
     * @return the $_unit_size
     */
    public function getUnit_size() {
        return $this->_unit_size;
    }

    /**
     * @param field_type $_unit_size
     */
    public function setUnit_size($_unit_size) {
        $this->_unit_size = $_unit_size;
    }

    /**
     * @return the $_delivery_leadtime
     */
    public function getDelivery_leadtime() {
        return $this->_delivery_leadtime;
    }

    /**
     * @param field_type $_delivery_leadtime
     */
    public function setDelivery_leadtime($_delivery_leadtime) {
        $this->_delivery_leadtime = $_delivery_leadtime;
    }

    /**
     * @return the $_size
     */
    public function getSize() {
        return $this->_size;
    }

    /**
     * @param field_type $_size
     */
    public function setSize($_size) {
        $this->_size = $_size;
    }

    /**
     * @return the $_type
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * @param field_type $_type
     */
    public function setType($_type) {
        $this->_type = $_type;
    }

    /**
     * @return the $_specifications
     */
    public function getSpecifications() {
        return $this->_specifications;
    }

    /**
     * @param field_type $_specifications
     */
    public function setSpecifications($_specifications) {
        $this->_specifications = $_specifications;
    }

    /**
     * @return the $_certificates
     */
    public function getCertificates() {
        return $this->_certificates;
    }

    /**
     * @param field_type $_certificates
     */
    public function setCertificates($_certificates) {
        $this->_certificates = $_certificates;
    }

    /**
     * @return the $_packaging
     */
    public function getPackaging() {
        return $this->_packaging;
    }

    /**
     * @param field_type $_packaging
     */
    public function setPackaging($_packaging) {
        $this->_packaging = $_packaging;
    }

    /**
     * @return the $_brand_name
     */
    public function getBrand_name() {
        return $this->_brand_name;
    }

    /**
     * @param field_type $_brand_name
     */
    public function setBrand_name($_brand_name) {
        $this->_brand_name = $_brand_name;
    }

    /**
     * @return the $_model_num
     */
    public function getModel_num() {
        return $this->_model_num;
    }

    /**
     * @param field_type $_model_num
     */
    public function setModel_num($_model_num) {
        $this->_model_num = $_model_num;
    }

    /**
     * @return the $_payment_terms
     */
    public function getPayment_terms() {
        return $this->_payment_terms;
    }

    /**
     * @param field_type $_payment_terms
     */
    public function setPayment_terms($_payment_terms) {
        $this->_payment_terms = $_payment_terms;
    }

    /**
     * @return the $_hs_code
     */
    public function getHs_code() {
        return $this->_hs_code;
    }

    /**
     * @param field_type $_hs_code
     */
    public function setHs_code($_hs_code) {
        $this->_hs_code = $_hs_code;
    }

    /**
     * @return the $_manufacturers
     */
    public function getManufacturers() {
        return $this->_manufacturers;
    }

    /**
     * @param field_type $_manufacturers
     */
    public function setManufacturers($_manufacturers) {
        $this->_manufacturers = $_manufacturers;
    }

    /**
     * @return the $_place_of_origin
     */
    public function getPlace_of_origin() {
        return $this->_place_of_origin;
    }

    /**
     * @param field_type $_place_of_origin
     */
    public function setPlace_of_origin($_place_of_origin) {
        $this->_place_of_origin = $_place_of_origin;
    }

    /**
     * @return the $_brochures
     */
    public function getBrochures() {
        return $this->_brochures;
    }

    /**
     * @param field_type $_brochures
     */
    public function setBrochures($_brochures) {
        $this->_brochures = $_brochures;
    }

    /**
     * @return the $_brochures
     */
    public function getBrochures_primary() {
        return $this->_brochures_primary;
    }

    /**
     * @param field_type $_brochures_primary
     */
    public function setBrochures_primary($_brochures_primary) {
        $this->_brochures_primary = $_brochures_primary;
    }

    /**
     * @return the $_video
     */
    public function getVideo() {
        return $this->_video;
    }

    /**
     * @param field_type $_video
     */
    public function setVideo($_video) {
        $this->_video = $_video;
    }

    /**
     * @return the $_terms_policy
     */
    public function getTerms_policy() {
        return $this->_terms_policy;
    }

    /**
     * @param field_type $_terms_policy
     */
    public function setTerms_policy($_terms_policy) {
        $this->_terms_policy = $_terms_policy;
    }

    /**
     * @return the $_contract_period
     */
    public function getContract_period() {
        return $this->_contract_period;
    }

    /**
     * @param field_type $_contract_period
     */
    public function setContract_period($_contract_period) {
        $this->_contract_period = $_contract_period;
    }

    /**
     * @return the $_warranty
     */
    public function getWarranty() {
        return $this->_warranty;
    }

    /**
     * @param field_type $_warranty
     */
    public function setWarranty($_warranty) {
        $this->_warranty = $_warranty;
    }

    /**
     * @return the $_guarantee
     */
    public function getGuarantee() {
        return $this->_guarantee;
    }

    /**
     * @param field_type $_guarantee
     */
    public function setGuarantee($_guarantee) {
        $this->_guarantee = $_guarantee;
    }

    /**
     * @return the $_related_items
     */
    public function getRelated_items() {
        return $this->_related_items;
    }

    /**
     * @param field_type $_related_items
     */
    public function setRelated_items($_related_items) {
        $this->_related_items = (is_array($_related_items)) ? implode(",", $_related_items) : $_related_items;
    }

    /**
     * @return the $_colour
     */
    public function getColour() {
        return $this->_colour;
    }

    /**
     * @param field_type $_colour
     */
    public function setColour($_colour) {
        $this->_colour = $_colour;
    }

    /**
     * @return the $_min_bidding_price
     */
    public function getMin_bidding_price() {
        return $this->_min_bidding_price;
    }

    /**
     * @param field_type $_min_bidding_price
     */
    public function setMin_bidding_price($_min_bidding_price) {
        $this->_min_bidding_price = $_min_bidding_price;
    }

    /* @return the $_materials
     */

    public function getMaterials() {
        return $this->_materials;
    }

    /**
     * @param field_type $_materials
     */
    public function setMaterials($_materials) {
        $this->_materials = $_materials;
    }

    /**
     * @return the $_shipment
     */
    public function getShipment() {
        return $this->_shipment;
    }

    /**
     * @param field_type $_shipment
     */
    public function setShipment($_shipment) {
        $this->_shipment = $_shipment;
    }

    /**
     * @return the $_addtional_key
     */
    public function getAddtional_key() {
        return $this->_addtional_key;
    }

    /**
     * @param field_type $_addtional_key
     */
    public function setAddtional_key($_addtional_key) {
        $this->_addtional_key = $_addtional_key;
    }

    /**
     * @return the $_addtional_value
     */
    public function getAddtional_value() {
        return $this->_addtional_value;
    }

    /**
     * @param field_type $_addtional_value
     */
    public function setAddtional_value($_addtional_value) {
        $this->_addtional_value = $_addtional_value;
    }

    /**
     * @return the $_time_of_expiry
     */
    public function getTime_of_expiry() {
        return $this->_time_of_expiry;
    }

    /**
     * @param field_type $_time_of_expiry
     */
    public function setTime_of_expiry($_time_of_expiry) {
        $locale = Eicra_Global_Variable::getSession()->sess_lang;

        if ($_time_of_expiry) {
            $data_arr = preg_split('/[- :]/', $_time_of_expiry);
            if ($data_arr[0]) {
                $date_obj = new Zend_Date($_time_of_expiry, null, $locale);
                //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", $time);
            }
        } else {
            $date_obj = new Zend_Date((time() + (10 * 365 * 24 * 60 * 60)), null, $locale);
            $this->_time_of_expiry = $this->_time_of_expiry = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
        }
    }

    /**
     * @return the $_fob_price
     */
    public function getFob_price() {
        return $this->_fob_price;
    }

    /**
     * @param field_type $_fob_price
     */
    public function setFob_price($_fob_price) {
        $this->_fob_price = $_fob_price;
    }

    /**
     * @return the $_max_supply
     */
    public function getMax_supply() {
        return $this->_max_supply;
    }

    /**
     * @param field_type $_max_supply
     */
    public function setMax_supply($_max_supply) {
        $this->_max_supply = $_max_supply;
    }

    /**
     * @return the $_min_quantity
     */
    public function getMin_quantity() {
        return $this->_min_quantity;
    }

    /**
     * @param field_type $_min_quantity
     */
    public function setMin_quantity($_min_quantity) {
        $this->_min_quantity = $_min_quantity;
    }

    /**
     * @return the $_strikethrough_price
     */
    public function getStrikethrough_price() {
        return $this->_strikethrough_price;
    }

    /**
     * @param field_type $_strikethrough_price
     */
    public function setStrikethrough_price($_strikethrough_price) {
        $this->_strikethrough_price = $_strikethrough_price;
    }

    /**
     * @return the $_price
     */
    public function getPrice() {
        return $this->_price;
    }

    /**
     * @param field_type $_price
     */
    public function setPrice($_price) {
        $this->_price = $_price;
    }

    /**
     * @return the $_displayble
     */
    public function getDisplayble() {
        return $this->_displayble;
    }

    /**
     * @param field_type $_displayble
     */
    public function setDisplayble($_displayble) {
        $this->_displayble = $_displayble;
    }

    /**
     * @return the $_Active
     */
    public function getActive() {
        return $this->_Active;
    }

    /**
     * @param field_type $_Active
     */
    public function setActive($_Active) {
        $this->_Active = $_Active;
    }

    /**
     * @return the $_category_id
     */
    public function getCategory_id() {
        return $this->_category_id;
    }

    /**
     * @param field_type $_category_id
     */
    public function setCategory_id($_category_id) {
        $this->_category_id = $_category_id;
    }

    /**
     * @return the $_negotiable
     */
    public function getNegotiable() {
        return $this->_negotiable;
    }

    /**
     * @param field_type $_negotiable
     */
    public function setNegotiable($_negotiable) {
        $this->_negotiable = $_negotiable;
    }

    /**
     * @return the $_added_on
     */
    public function getAdded_on() {
        return date('Y-m-d H:i:s');
    }

    /**
     * @param field_type $_added_on
     */
    public function setAdded_on($_added_on) {
        $this->_added_on = date('Y-m-d H:i:s');
    }

    /**
     * @return the $_price_currency_locale
     */
    public function getPrice_currency_locale() {
        return $this->_price_currency_locale;
    }

    /**
     * @param field_type $_price_currency_locale
     */
    public function setPrice_currency_locale($_price_currency_locale) {
        $this->_price_currency_locale = $_price_currency_locale;
    }

}
