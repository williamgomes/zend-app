<?php

class B2b_Model_Companyprofile {

    private $_id;
    private $_group_id;
    private $_user_id;
    private $_businesstype_id;
    private $_category_id;
    private $_plan_id;
    private $_trade_type;
    private $_company_name;
    private $_registered_in;
    private $_street;
    private $_city;
    private $_profile_image_primary;
    private $_avatar_img;
    private $_banner_img;
    private $_postal_code;
    private $_company_category;
    private $_selling_categories;
    private $_buying_categories;
    private $_QA_policy;
    private $_store_name;
    private $_profile_image;
    private $_legal_owner;
    private $_office_size;
    private $_establishing_year;
    private $_number_of_employees;
    private $_annual_revenue;
    private $_main_products;
    private $_main_products_part1;
    private $_main_products_part2;
    private $_main_products_part3;
    private $_other_products;
    private $_certification;
    private $_main_markets;
    private $_annual_sales;
    private $_nearest_ports;
    private $_main_customers;
    private $_avg_lead_time;
    private $_overseas_office;
    private $_delivery_terms;
    private $_payment_currency;
    private $_payment_type;
    private $_language;
    private $_factory_location;
    private $_website;
    private $_phone;
    private $_phone_part2;
    private $_phone_part3;
    private $_fax;
    private $_fax_part2;
    private $_fax_part3;
    private $_mobile;
    private $_mobile_part2;
    private $_mobile_part3;
    private $_company_information;
    private $_policy;
    private $_terms_condition;
    private $_added_on;
    private $_certified;
    private $_seo_title;
    private $_factory_size;
    private $_export_market;
    private $_compliances;
    private $_escrow;
    private $_export_percentage;
    private $_trade_shows;
    private $_production_capacity;
    private $_country;
    private $_payment_method;
    private $_keywords;
    private $_meta_desc;
    private $_active;
    private $_displayble;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth hotels');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth hotels');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveProfile() {
        $mapper = new B2b_Model_CompanyprofileMapper();
        $return = $mapper->save($this);
        return $return;
    }

    /**
     * * @return the $_id *
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * * @param field_type $_id
     */
    public function setId($_id) {
        $this->_id = $_id;
    }

    /**
     * @return the $_group_id
     */
    public function getGroup_id() {
        return $this->_group_id;
    }

    /**
     * @param field_type $_group_id
     */
    public function setGroup_id($_group_id) {
        $this->_group_id = $_group_id;
    }

    /**
     * * @return the $_user_id
     */
    public function getUser_id() {
        return $this->_user_id;
    }

    /**
     * * @param field_type $_user_id
     */
    public function setUser_id($_user_id) {
        $this->_user_id = $_user_id;
    }

    /**
     * * @return the $_businesstype_id
     */
    public function getBusinesstype_id() {
        return $this->_businesstype_id;
    }

    /**
     * * @param field_type $_businesstype_id
     */
    public function setBusinesstype_id($_businesstype_id) {
        $this->_businesstype_id = implode(",", $_businesstype_id);
    }

    /**
     * * @return the $_plan_id
     */
    public function getPlan_id() {
        return $this->_plan_id;
    }

    /**
     * * @param field_type $_plan_id
     */
    public function setPlan_id($_plan_id) {
        $this->_plan_id = $_plan_id;
    }

    /**
     * * @return the $_trade_type
     */
    public function getTrade_type() {
        return $this->_trade_type;
    }

    /**
     * * @param field_type $_trade_type
     */
    public function setTrade_type($_trade_type) {
        $this->_trade_type = $_trade_type;
    }

    /**
     * * @return the $_company_name
     */
    public function getCompany_name() {
        return $this->_company_name;
    }

    /**
     * * @param field_type $_company_name
     */
    public function setCompany_name($_company_name) {
        $this->_company_name = $_company_name;
    }

    /**
     * * @return the $_registered_in
     */
    public function getRegistered_in() {
        return $this->_registered_in;
    }

    /**
     * * @param field_type $_registered_in
     */
    public function setRegistered_in($_registered_in) {
        $this->_registered_in = $_registered_in;
    }

    /**
     * * @return the $_street
     */
    public function getStreet() {
        return $this->_street;
    }

    /**
     * * @param field_type $_street
     */
    public function setStreet($_street) {
        $this->_street = $_street;
    }

    /**
     * * @return the $_city
     */
    public function getCity() {
        return $this->_city;
    }

    /**
     * * @param field_type $_city
     */
    public function setCity($_city) {
        $this->_city = $_city;
    }

    /**
     * * @return the $_postal_code
     */
    public function getPostal_code() {
        return $this->_postal_code;
    }

    /**
     * * @param field_type $_postal_code
     */
    public function setPostal_code($_postal_code) {
        $this->_postal_code = $_postal_code;
    }

    /**
     * * @return the $_company_category
     */
    public function getCompany_category() {
        return $this->_company_category;
    }

    /**
     * * @param field_type $_company_category
     */
    public function setCompany_category($_company_category) {
        $this->_company_category = $_company_category;
    }

    /**
     * * @return the $_selling_categories
     */
    public function getSelling_categories() {
        return $this->_selling_categories;
    }

    /**
     * * @param field_type $_selling_categories
     */
    public function setSelling_categories($_selling_categories) {
        $this->_selling_categories = $_selling_categories;
    }

    /**
     * * @return the $_buying_categories
     */
    public function getBuying_categories() {
        return $this->_buying_categories;
    }

    /**
     * * @param field_type $_buying_categories
     */
    public function setBuying_categories($_buying_categories) {
        $this->_buying_categories = $_buying_categories;
    }

    /**
     * * @return the $_QA_policy
     */
    public function getQA_policy() {
        return $this->_QA_policy;
    }

    /**
     * * @param field_type $_QA_policy
     */
    public function setQA_policy($_QA_policy) {
        $this->_QA_policy = $_QA_policy;
    }

    /**
     * * @return the $_store_name
     */
    public function getStore_name() {
        return $this->_store_name;
    }

    /**
     * * @param field_type $_store_name
     */
    public function setStore_name($_store_name) {
        $this->_store_name = $_store_name;
    }

    /**
     * * @return the $_profile_image
     */
    public function getProfile_image() {
        return $this->_profile_image;
    }

    /**
     * * @param field_type $_profile_image
     */
    public function setProfile_image($_profile_image) {
        $this->_profile_image = $_profile_image;
    }

    /**
     * * @return the $_legal_owner
     */
    public function getLegal_owner() {
        return $this->_legal_owner;
    }

    /**
     * * @param field_type $_legal_owner
     */
    public function setLegal_owner($_legal_owner) {
        $this->_legal_owner = $_legal_owner;
    }

    /**
     * * @return the $_office_size
     */
    public function getOffice_size() {
        return $this->_office_size;
    }

    /**
     * * @param field_type $_office_size
     */
    public function setOffice_size($_office_size) {
        $this->_office_size = $_office_size;
    }

    /**
     * * @return the $_establishing_year
     */
    public function getEstablishing_year() {
        return $this->_establishing_year;
    }

    /**
     * * @param field_type $_establishing_year
     */
    public function setEstablishing_year($_establishing_year) {
        $this->_establishing_year = $_establishing_year;
    }

    /**
     * * @return the $_number_of_employees
     */
    public function getNumber_of_employees() {
        return $this->_number_of_employees;
    }

    /**
     * * @param field_type $_number_of_employees
     */
    public function setNumber_of_employees($_number_of_employees) {
        $this->_number_of_employees = $_number_of_employees;
    }

    /**
     * * @return the $_annual_revenue
     */
    public function getAnnual_revenue() {
        return $this->_annual_revenue;
    }

    /**
     * * @param field_type $_annual_revenue
     */
    public function setAnnual_revenue($_annual_revenue) {
        $this->_annual_revenue = $_annual_revenue;
    }

    /**
     * * @return the $_main_products
     */
    public function getMain_products() {
        return $this->_main_products;
    }

    /**
     * * @param field_type $_main_products
     */
    public function setMain_products($_main_products) {
        $this->_main_products = $_main_products;
    }

    /**
     * * @return the $_main_products_part1
     */
    public function getMain_products_part1() {
        return $this->_main_products_part1;
    }

    /**
     * * @param field_type $_main_products_part1
     */
    public function setMain_products_part1($_main_products_part1) {
        $this->_main_products_part1 = $_main_products_part1;
    }

    /**
     * * @return the $_main_products_part2
     */
    public function getMain_products_part2() {
        return $this->_main_products_part2;
    }

    /**
     * * @param field_type $_main_products_part2
     */
    public function setMain_products_part2($_main_products_part2) {
        $this->_main_products_part2 = $_main_products_part2;
    }

    /**
     * * @return the $_main_products_part3
     */
    public function getMain_products_part3() {
        return $this->_main_products_part3;
    }

    /**
     * * @param field_type $_main_products_part3
     */
    public function setMain_products_part3($_main_products_part3) {
        $this->_main_products_part3 = $_main_products_part3;
    }

    /**
     * * @return the $_other_products
     */
    public function getOther_products() {
        return $this->_other_products;
    }

    /**
     * * @param field_type $_other_products
     */
    public function setOther_products($_other_products) {
        $this->_other_products = $_other_products;
    }

    /**
     * * @return the $_certification
     */
    public function getCertification() {
        return $this->_certification;
    }

    /**
     * * @param field_type $_certification
     */
    public function setCertification($_certification) {
        $this->_certification = $_certification;
    }

    /**
     * * @return the $_main_markets
     */
    public function getMain_markets() {
        return $this->_main_markets;
    }

    /**
     * * @param field_type $_main_markets
     */
    public function setMain_markets($_main_markets) {
        $this->_main_markets = $_main_markets;
    }

    /**
     * * @return the $_annual_sales
     */
    public function getAnnual_sales() {
        return $this->_annual_sales;
    }

    /**
     * * @param field_type $_annual_sales
     */
    public function setAnnual_sales($_annual_sales) {
        $this->_annual_sales = $_annual_sales;
    }

    /**
     * * @return the $_nearest_ports
     */
    public function getNearest_ports() {
        return $this->_nearest_ports;
    }

    /**
     * * @param field_type $_nearest_ports
     */
    public function setNearest_ports($_nearest_ports) {
        $this->_nearest_ports = $_nearest_ports;
    }

    /**
     * * @return the $_main_customers
     */
    public function getMain_customers() {
        return $this->_main_customers;
    }

    /**
     * * @param field_type $_main_customers
     */
    public function setMain_customers($_main_customers) {
        $this->_main_customers = $_main_customers;
    }

    /**
     * * @return the $_avg_lead_time
     */
    public function getAvg_lead_time() {
        return $this->_avg_lead_time;
    }

    /**
     * * @param field_type $_avg_lead_time
     */
    public function setAvg_lead_time($_avg_lead_time) {
        $this->_avg_lead_time = $_avg_lead_time;
    }

    /**
     * * @return the $_overseas_office
     */
    public function getOverseas_office() {
        return $this->_overseas_office;
    }

    /**
     * * @param field_type $_overseas_office
     */
    public function setOverseas_office($_overseas_office) {
        $this->_overseas_office = $_overseas_office;
    }

    /**
     * * @return the $_delivery_terms
     */
    public function getDelivery_terms() {
        return $this->_delivery_terms;
    }

    /**
     * * @param field_type $_delivery_terms
     */
    public function setDelivery_terms($_delivery_terms) {
        $this->_delivery_terms = $_delivery_terms;
    }

    /**
     * * @return the $_payment_currency
     */
    public function getPayment_currency() {
        return $this->_payment_currency;
    }

    /**
     * * @param field_type $_payment_currency
     */
    public function setPayment_currency($_payment_currency) {
        $this->_payment_currency = $_payment_currency;
    }

    /**
     * * @return the $_payment_type
     */
    public function getPayment_type() {
        return $this->_payment_type;
    }

    /**
     * * @param field_type $_payment_type
     */
    public function setPayment_type($_payment_type) {
        $this->_payment_type = $_payment_type;
    }

    /**
     * * @return the $_language
     */
    public function getLanguage() {
        return $this->_language;
    }

    /**
     * * @param field_type $_language
     */
    public function setLanguage($_language) {
        $this->_language = $_language;
    }

    /**
     * * @return the $_factory_location
     */
    public function getFactory_location() {
        return $this->_factory_location;
    }

    /**
     * * @param field_type $_factory_location
     */
    public function setFactory_location($_factory_location) {
        $this->_factory_location = $_factory_location;
    }

    /**
     * * @return the $_website
     */
    public function getWebsite() {
        return $this->_website;
    }

    /**
     * * @param field_type $_website
     */
    public function setWebsite($_website) {
        $this->_website = $_website;
    }

    /**
     * * @return the $_phone
     */
    public function getPhone() {
        return $this->_phone;
    }

    /**
     * * @param field_type $_phone
     */
    public function setPhone($_phone) {
        $this->_phone = $_phone;
    }

    /**
     * * @return the $_phone_part2
     */
    public function getPhone_part2() {
        return $this->_phone_part2;
    }

    /**
     * * @param field_type $_phone_part2
     */
    public function setPhone_part2($_phone_part2) {
        $this->_phone_part2 = $_phone_part2;
    }

    /**
     * * @return the $_phone_part3
     */
    public function getPhone_part3() {
        return $this->_phone_part3;
    }

    /**
     * * @param field_type $_phone_part3
     */
    public function setPhone_part3($_phone_part3) {
        $this->_phone_part3 = $_phone_part3;
    }

    /**
     * * @return the $_fax
     */
    public function getFax() {
        return $this->_fax;
    }

    /**
     * * @param field_type $_fax
     */
    public function setFax($_fax) {
        $this->_fax = $_fax;
    }

    /**
     * * @return the $_fax_part2
     */
    public function getFax_part2() {
        return $this->_fax_part2;
    }

    /**
     * * @param field_type $_fax_part2
     */
    public function setFax_part2($_fax_part2) {
        $this->_fax_part2 = $_fax_part2;
    }

    /**
     * * @return the $_fax_part3
     */
    public function getFax_part3() {
        return $this->_fax_part3;
    }

    /**
     * * @param field_type $_fax_part3
     */
    public function setFax_part3($_fax_part3) {
        $this->_fax_part3 = $_fax_part3;
    }

    /**
     * * @return the $_mobile
     */
    public function getMobile() {
        return $this->_mobile;
    }

    /**
     * * @param field_type $_mobile
     */
    public function setMobile($_mobile) {
        $this->_mobile = $_mobile;
    }

    /**
     * * @return the $_mobile_part2
     */
    public function getMobile_part2() {
        return $this->_mobile_part2;
    }

    /**
     * * @param field_type $_mobile_part2
     */
    public function setMobile_part2($_mobile_part2) {
        $this->_mobile_part2 = $_mobile_part2;
    }

    /**
     * * @return the $_mobile_part3
     */
    public function getMobile_part3() {
        return $this->_mobile_part3;
    }

    /**
     * * @param field_type $_mobile_part3
     */
    public function setMobile_part3($_mobile_part3) {
        $this->_mobile_part3 = $_mobile_part3;
    }

    /**
     * * @return the $_company_information
     */
    public function getCompany_information() {
        return $this->_company_information;
    }

    /**
     * * @param field_type $_company_information
     */
    public function setCompany_information($_company_information) {
        $this->_company_information = $_company_information;
    }

    /**
     * * @return the $_policy
     */
    public function getPolicy() {
        return $this->_policy;
    }

    /**
     * * @param field_type $_policy
     */
    public function setPolicy($_policy) {
        $this->_policy = $_policy;
    }

    /**
     * * @return the $_terms_condition
     */
    public function getTerms_condition() {
        return $this->_terms_condition;
    }

    /**
     * * @param field_type $_terms_condition
     */
    public function setTerms_condition($_terms_condition) {
        $this->_terms_condition = $_terms_condition;
    }

    /**
     * * @return the $_added_on
     */
    public function getAdded_on() {
        return date("Y-m-d h:i:s");
    }

    /**
     * * @param field_type $_added_on
     */
    public function setAdded_on($_added_on) {
        $this->_added_on = date("Y-m-d h:i:s");
    }

    /**
     * * @return the $_certified
     */
    public function getCertified() {
        return $this->_certified;
    }

    /**
     * * @param field_type $_certified
     */
    public function setCertified($_certified) {
        $this->_certified = $_certified;
    }

    /**
     * * @return the $_seo_title
     */
    public function getSeo_title() {
        return $this->_seo_title;
    }

    /**
     * * @param field_type $_seo_title
     */
    public function setSeo_title($text, $id = null) {
        $pattern = Eicra_File_Constants::TITLE_PATTERN;
        $replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
        $text = preg_replace($pattern, $replacement, trim($text));

        // DB Connection		
        $company_profile_db = new B2b_Model_DbTable_CompanyProfile();

        if (empty($id)) {
            $select = $company_profile_db->select()->from(array('cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'), array('cpy.id'))
                    ->where('cpy.seo_title = ?', $text);
        } else {
            $select = $company_profile_db->select()->from(array('cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'), array('cpy.id'))
                            ->where('cpy.seo_title = ?', $text)->where('cpy.id != ?', $id);
        }

        $rs = $select->query()->fetchAll();
        if ($rs) {
            foreach ($rs as $row) {
                $ids = (int) $row ['id'];
            }
        }
        if (empty($ids)) {
            $this->_seo_title = $text;
        } else {
            $select = $company_profile_db->select()->from(array('cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'), array('cpy.id'))
                    ->order(array('cpy.id DESC'))
                    ->limit(1);

            $rs = $select->query()->fetchAll();
            if ($rs) {
                foreach ($rs as $row) {
                    $last_id = (int) $row ['id'];
                }
            }

            if (empty($id)) {
                $this->_seo_title = $text . '-' . ($last_id + 1);
            } else {
                $this->_seo_title = $text . '-' . $id;
            }
        }
        return $this;
    }

    /**
     * * @return the $_factory_size
     */
    public function getFactory_size() {
        return $this->_factory_size;
    }

    /**
     * * @param field_type $_factory_size
     */
    public function setFactory_size($_factory_size) {
        $this->_factory_size = $_factory_size;
    }

    /**
     * * @return the $_category_id
     */
    public function getCategory_id() {
        return $this->_category_id;
    }

    /**
     * * @param field_type $_category_id
     */
    public function setCategory_id($_category_id) {
        $this->_category_id = $_category_id;
    }

    /**
     * * @return the $_compliances
     */
    public function getCompliances() {
        return $this->_compliances;
    }

    /**
     * * @param field_type $_compliances
     */
    public function setCompliances($_compliances) {
        $this->_compliances = implode(",", $_compliances);
    }

    /**
     * * @return the $_escrow
     */
    public function getEscrow() {
        return $this->_escrow;
    }

    /**
     * * @param field_type $_escrow
     */
    public function setEscrow($_escrow) {
        $this->_escrow = $_escrow;
    }

    /**
     * * @return the $_export_percentage
     */
    public function getExport_percentage() {
        return $this->_export_percentage;
    }

    /**
     * * @param field_type $_export_percentage
     */
    public function setExport_percentage($_export_percentage) {
        $this->_export_percentage = $_export_percentage;
    }

    /**
     * * @return the $_trade_shows
     */
    public function getTrade_shows() {
        return $this->_trade_shows;
    }

    /**
     * * @param field_type $_trade_shows
     */
    public function setTrade_shows($_trade_shows) {
        $this->_trade_shows = $_trade_shows;
    }

    /**
     * * @return the $_production_capacity
     */
    public function getProduction_capacity() {
        return $this->_production_capacity;
    }

    /**
     * * @param field_type $_production_capacity
     */
    public function setProduction_capacity($_production_capacity) {
        $this->_production_capacity = $_production_capacity;
    }

    /**
     *
     * @return the $_country
     */
    public function getCountry() {
        return $this->_country;
    }

    /**
     *
     * @param field_type $_country
     */
    public function setCountry($_country) {
        $this->_country = $_country;
    }

    /**
     * * @return the $_payment_method
     */
    public function getPayment_method() {
        return $this->_payment_method;
    }

    /**
     * * @param field_type $_payment_method
     */
    public function setPayment_method($_payment_method) {
        $this->_payment_method = $_payment_method;
    }

    /**
     * * @return the $_profile_image_primary
     */
    public function getProfile_image_primary() {
        return $this->_profile_image_primary;
    }

    /**
     * * @param field_type $_profile_image_primary
     */
    public function setProfile_image_primary($_profile_image_primary) {
        $this->_profile_image_primary = $_profile_image_primary;
    }

    /**
     * * @return the $_avatar_img
     */
    public function getAvatar_img() {
        return $this->_avatar_img;
    }

    /**
     * * @param field_type $_avatar_img
     */
    public function setAvatar_img($_avatar_img) {
        $this->_avatar_img = $_avatar_img;
    }

    /**
     * * @return the $_banner_img
     */
    public function getBanner_img() {
        return $this->_banner_img;
    }

    /**
     * * @param field_type $_banner_img
     */
    public function setBanner_img($_banner_img) {
        $this->_banner_img = $_banner_img;
    }

    /**
     * * @return the $_keywords
     */
    public function getKeywords() {
        return $this->_keywords;
    }

    /**
     * * @param field_type $_keywords
     */
    public function setKeywords($_keywords) {
        $this->_keywords = $_keywords;
    }

    /**
     * * @return the $_meta_desc
     */
    public function getMeta_desc() {
        return $this->_meta_desc;
    }

    /**
     * * @param field_type $_meta_desc
     */
    public function setMeta_desc($_meta_desc) {
        $this->_meta_desc = $_meta_desc;
    }

    /**
     * @param string $_seo_title
     */

    /**
     * @return the $_export_market
     */
    public function getExport_market() {
        return $this->_export_market;
    }

    /**
     * @param field_type $_export_market
     */
    public function setExport_market($_export_market) {
        $this->_export_market = $_export_market;
    }

    /**
     * @return the $_active
     */
    public function getActive() {
        return $this->_active;
    }

    /**
     * @param field_type $_active
     */
    public function setActive($_active) {
        $this->_active = $_active;
    }

    /**
     * @return the $_displayble
     */
    public function getDisplayble() {
        return $this->_displayble;
    }

    /**
     * @param field_type $_displayble
     */
    public function setDisplayble($_displayble) {
        $this->_displayble = $_displayble;
    }

}

?>
