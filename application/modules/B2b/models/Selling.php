<?php
class B2b_Model_Selling {

	protected $_id ;
	protected $_user_id ;
	protected $_product_grp_id ;
	protected $_group_id ;
	protected $_seo_title	;
	protected $_selling_order;
	protected $_category ;
	protected $_name ;
	protected $_quantity ;
	protected $_qty_per_unit ;
	protected $_price_currency_locale ;
	protected $_price_currency ;
	protected $_price ;
	protected $_strikethrough_price ;
	protected $_price_per_unit ;
	protected $_subject ;
	protected $_keyword ;
	protected $_keyword_1 ;
	protected $_keyword_2 ;
	protected $_keyword_3 ;
	protected $_description ;
	protected $_detail_description ;
	protected $_primary_file_field ;
	protected $_others_images ;
	protected $_displayable ;
	protected $_negotiable ;
	protected $_fob_price ;
	protected $_delivery ;
	protected $_delivery_leadtime ;
	protected $_size ;
	protected $_unit_size ;
	protected $_type ;
	protected $_specifications ;
	protected $_packaging ;
	protected $_brand_name ;
	protected $_model_num ;
	protected $_payment_type ;
	protected $_manufacturers ;
	protected $_place_of_origin ;
	protected $_related_items ;
	protected $_colour ;
	protected $_min_bidding_price ;
	protected $_materials ;
	protected $_shipping ;
	protected $_addtional_value ;
	protected $_validity ;
	protected $_displayble ;
	protected $_active ;

	public function __construct(array $options = null)

	{



		if (is_array($options)) {



			$this->setOptions($options);



		}



	}



	public function __set($name, $value)



	{

		$method = 'set' . $name;



		if (('mapper' == $name) || !method_exists($this, $method)) {



			throw new Exception('Invalid Auth property');



		}



		$this->$method($value);



	}





	public function __get($name)

	{



		$method = 'get' . $name;



		if (('mapper' == $name) || !method_exists($this, $method)) {



			throw new Exception('Invalid Auth property');



		}



		return $this->$method();

	}


	public function setOptions(array $options)

	{



		$methods = get_class_methods($this);



		foreach ($options as $key => $value) {



			$method = 'set' . ucfirst($key);



			if (in_array($method, $methods)) {



				$this->$method($value);

			}

		}



		return $this;



	}


	public function save()
	{
		$mapper  = new B2b_Model_SellingMapper();
		$return = $mapper->save($this);
		return $return;
	}
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @return the $_user_id
	 */
	public function getUser_id() {
		return $this->_user_id;
	}

	/**
	 * @param field_type $_user_id
	 */
	public function setUser_id($_user_id) {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$globalIdentity = $auth->getIdentity ();
		  $this->_user_id = ($globalIdentity->allow_to_change_ownership != '1' || empty($_user_id)) ? $globalIdentity->user_id : $_user_id;
		} else {
			$this->_user_id = $_user_id;
		}
		return $this; 
	}

	/**
     * @return the $_seo_title
     */
    public function getSeo_title ()
    {
        return $this->_seo_title;
    }

	/**
     * @param field_type $_seo_title
     */
    public function setSeo_title ($text, $id = null)
    {
        $pattern = Eicra_File_Constants::TITLE_PATTERN;
		$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
		$text = preg_replace ( $pattern, $replacement, trim ( $text ) );
		
		// DB Connection		
		$selling_db = new B2b_Model_DbTable_Selling();

		if (empty ( $id )) 
		{
			$select = $selling_db->select ()->from ( array ( 'sl' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_selling_leads' ), array ( 'sl.id' ) )
											->where ( 'sl.seo_title = ?', $text );
		} 
		else 
		{
			$select = $selling_db->select ()->from ( array ( 'sl' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_selling_leads' ), array ( 'sl.id' ) )
											->where ( 'sl.seo_title = ?', $text )->where ( 'sl.id != ?', $id );
		}
		
		$rs = $select->query ()->fetchAll ();
		if ($rs) 
		{
			foreach ( $rs as $row ) 
			{
				$ids = ( int ) $row ['id'];
			}
		}
		if (empty ( $ids )) 
		{
			$this->_seo_title = $text;
		} 
		else 
		{
			$select = $selling_db->select ()->from ( array ( 'sl' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_selling_leads' ), array ( 'sl.id' ) )
											->order ( array ( 'sl.id DESC' ) )
											->limit ( 1 );
			
			$rs = $select->query ()->fetchAll ();
			if ($rs) 
			{
				foreach ( $rs as $row ) 
				{
					$last_id = ( int ) $row ['id'];
				}
			}
			
			if (empty ( $id )) 
			{
				$this->_seo_title = $text . '-' . ($last_id + 1);
			} 
			else 
			{
				$this->_seo_title = $text . '-' . $id;
			}
		}
		return $this;

    }

	/**
	 * @return the $_product_grp_id
	 */
	public function getProduct_grp_id() {
		return $this->_product_grp_id;
	}

	/**
	 * @param field_type $_product_grp_id
	 */
	public function setProduct_grp_id($_product_grp_id) {
		$this->_product_grp_id = $_product_grp_id;
	}

	/**
	 * @return the $_category
	 */
	public function getCategory() {
		return $this->_category;
	}

	/**
	 * @param field_type $_category
	 */
	public function setCategory($_category) {
		$this->_category = $_category;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @param field_type $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @return the $_quantity
	 */
	public function getQuantity() {
		return $this->_quantity;
	}

	/**
	 * @param field_type $_quantity
	 */
	public function setQuantity($_quantity) {
		$this->_quantity = $_quantity;
	}

	/**
	 * @return the $_qty_per_unit
	 */
	public function getQty_per_unit() {
		return $this->_qty_per_unit;
	}

	/**
	 * @param field_type $_qty_per_unit
	 */
	public function setQty_per_unit($_qty_per_unit) {
		$this->_qty_per_unit = $_qty_per_unit;
	}

	/**
	 * @return the $_price_currency
	 */
	public function getPrice_currency() {
		return $this->_price_currency;
	}

	/**
	 * @param field_type $_price_currency
	 */
	public function setPrice_currency($_price_currency) {
		$this->_price_currency = $_price_currency;
	}

	/**
	 * @return the $_price
	 */
	public function getPrice() {
		return $this->_price;
	}

	/**
	 * @param field_type $_price
	 */
	public function setPrice($_price) {
		$this->_price = $_price;
	}

	/**
	 * @return the $_strikethrough_price
	 */
	public function getStrikethrough_price() {
		return $this->_strikethrough_price;
	}

	/**
	 * @param field_type $_strikethrough_price
	 */
	public function setStrikethrough_price($_strikethrough_price) {
		$this->_strikethrough_price = $_strikethrough_price;
	}

	/**
	 * @return the $_price_per_unit
	 */
	public function getPrice_per_unit() {
		return $this->_price_per_unit;
	}

	/**
	 * @param field_type $_price_per_unit
	 */
	public function setPrice_per_unit($_price_per_unit) {
		$this->_price_per_unit = $_price_per_unit;
	}

	/**
	 * @return the $_subject
	 */
	public function getSubject() {
		return $this->_subject;
	}

	/**
	 * @param field_type $_subject
	 */
	public function setSubject($_subject) {
		$this->_subject = $_subject;
	}

	/**
	 * @return the $_keyword
	 */
	public function getKeyword() {
		return $this->_keyword;
	}

	/**
	 * @param field_type $_keyword
	 */
	public function setKeyword($_keyword) {
		$this->_keyword = $_keyword;
	}

	/**
	 * @return the $_keyword_1
	 */
	public function getKeyword_1() {
		return $this->_keyword_1;
	}

	/**
	 * @param field_type $_keyword_1
	 */
	public function setKeyword_1($_keyword_1) {
		$this->_keyword_1 = $_keyword_1;
	}

	/**
	 * @return the $_keyword_2
	 */
	public function getKeyword_2() {
		return $this->_keyword_2;
	}

	/**
	 * @param field_type $_keyword_2
	 */
	public function setKeyword_2($_keyword_2) {
		$this->_keyword_2 = $_keyword_2;
	}

	/**
	 * @return the $_keyword_3
	 */
	public function getKeyword_3() {
		return $this->_keyword_3;
	}

	/**
	 * @param field_type $_keyword_3
	 */
	public function setKeyword_3($_keyword_3) {
		$this->_keyword_3 = $_keyword_3;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @param field_type $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @return the $_detail_description
	 */
	public function getDetail_description() {
		return $this->_detail_description;
	}

	/**
	 * @param field_type $_detail_description
	 */
	public function setDetail_description($_detail_description) {
		$this->_detail_description = $_detail_description;
	}

	/**
	 * @return the $_primary_file_field
	 */
	public function getPrimary_file_field() {
		return $this->_primary_file_field;
	}

	/**
	 * @param field_type $_primary_file_field
	 */
	public function setPrimary_file_field($_primary_file_field) {
		$this->_primary_file_field = $_primary_file_field;
	}

	/**
	 * @return the $_others_images
	 */
	public function getOthers_images() {
		return $this->_others_images;
	}

	/**
	 * @param field_type $_others_images
	 */
	public function setOthers_images($_others_images) {
		$this->_others_images = $_others_images;
	}

	/**
	 * @return the $_displayable
	 */
	public function getDisplayable() {
		return $this->_displayable;
	}

	/**
	 * @param field_type $_displayable
	 */
	public function setDisplayable($_displayable) {
		$this->_displayable = $_displayable;
	}

	/**
	 * @return the $_negotiable
	 */
	public function getNegotiable() {
		return $this->_negotiable;
	}

	/**
	 * @param field_type $_negotiable
	 */
	public function setNegotiable($_negotiable) {
		$this->_negotiable = $_negotiable;
	}

	/**
	 * @return the $_fob_price
	 */
	public function getFob_price() {
		return $this->_fob_price;
	}

	/**
	 * @param field_type $_fob_price
	 */
	public function setFob_price($_fob_price) {
		$this->_fob_price = $_fob_price;
	}

	/**
	 * @return the $_delivery
	 */
	public function getDelivery() {
		return $this->_delivery;
	}

	/**
	 * @param field_type $_delivery
	 */
	public function setDelivery($_delivery) {
		$this->_delivery = $_delivery;
	}

	/**
	 * @return the $_delivery_leadtime
	 */
	public function getDelivery_leadtime() {
		return $this->_delivery_leadtime;
	}

	/**
	 * @param field_type $_delivery_leadtime
	 */
	public function setDelivery_leadtime($_delivery_leadtime) {
		$this->_delivery_leadtime = $_delivery_leadtime;
	}

	/**
	 * @return the $_size
	 */
	public function getSize() {
		return $this->_size;
	}

	/**
	 * @param field_type $_size
	 */
	public function setSize($_size) {
		$this->_size = $_size;
	}

	/**
	 * @return the $_unit_size
	 */
	public function getUnit_size() {
		return $this->_unit_size;
	}

	/**
	 * @param field_type $_unit_size
	 */
	public function setUnit_size($_unit_size) {
		$this->_unit_size = $_unit_size;
	}

	/**
	 * @return the $_type
	 */
	public function getType() {
		return $this->_type;
	}

	/**
	 * @param field_type $_type
	 */
	public function setType($_type) {
		$this->_type = $_type;
	}

	/**
	 * @return the $_specifications
	 */
	public function getSpecifications() {
		return $this->_specifications;
	}

	/**
	 * @param field_type $_specifications
	 */
	public function setSpecifications($_specifications) {
		$this->_specifications = $_specifications;
	}

	/**
	 * @return the $_packaging
	 */
	public function getPackaging() {
		return $this->_packaging;
	}

	/**
	 * @param field_type $_packaging
	 */
	public function setPackaging($_packaging) {
		$this->_packaging = $_packaging;
	}

	/**
	 * @return the $_brand_name
	 */
	public function getBrand_name() {
		return $this->_brand_name;
	}

	/**
	 * @param field_type $_brand_name
	 */
	public function setBrand_name($_brand_name) {
		$this->_brand_name = $_brand_name;
	}

	/**
	 * @return the $_model_num
	 */
	public function getModel_num() {
		return $this->_model_num;
	}

	/**
	 * @param field_type $_model_num
	 */
	public function setModel_num($_model_num) {
		$this->_model_num = $_model_num;
	}

	/**
	 * @return the $_payment_type
	 */
	public function getPayment_type() {
		return $this->_payment_type;
	}

	/**
	 * @param field_type $_payment_type
	 */
	public function setPayment_type($_payment_type) {
		$this->_payment_type = $_payment_type;
	}

	/**
	 * @return the $_manufacturers
	 */
	public function getManufacturers() {
		return $this->_manufacturers;
	}

	/**
	 * @param field_type $_manufacturers
	 */
	public function setManufacturers($_manufacturers) {
		$this->_manufacturers = $_manufacturers;
	}

	/**
	 * @return the $_place_of_origin
	 */
	public function getPlace_of_origin() {
		return $this->_place_of_origin;
	}

	/**
	 * @param field_type $_place_of_origin
	 */
	public function setPlace_of_origin($_place_of_origin) {
		$this->_place_of_origin = $_place_of_origin;
	}

	/**
	 * @return the $_related_items
	 */
	public function getRelated_items() {
		return $this->_related_items;
	}

	/**
	 * @param field_type $_related_items
	 */
	public function setRelated_items($_related_items) {

	    if (is_array($_related_items)) {
	        $this->_related_items = implode(",", $_related_items);
	    } else {
	        $this->_related_items = $_related_items;
	    }

	}

	/**
	 * @return the $_colour
	 */
	public function getColour() {
		return $this->_colour;
	}

	/**
	 * @param field_type $_colour
	 */
	public function setColour($_colour) {
		$this->_colour = $_colour;
	}

	/**
	 * @return the $_min_bidding_price
	 */
	public function getMin_bidding_price() {
		return $this->_min_bidding_price;
	}

	/**
	 * @param field_type $_min_bidding_price
	 */
	public function setMin_bidding_price($_min_bidding_price) {
		$this->_min_bidding_price = $_min_bidding_price;
	}

	/**
	 * @return the $_materials
	 */
	public function getMaterials() {
		return $this->_materials;
	}

	/**
	 * @param field_type $_materials
	 */
	public function setMaterials($_materials) {
		$this->_materials = $_materials;
	}

	/**
	 * @return the $_shipping
	 */
	public function getShipping() {
		return $this->_shipping;
	}

	/**
	 * @param field_type $_shipping
	 */
	public function setShipping($_shipping) {
		$this->_shipping = $_shipping;
	}

	/**
	 * @return the $_addtional_value
	 */
	public function getAddtional_value() {
		return $this->_addtional_value;
	}

	/**
	 * @param field_type $_addtional_value
	 */
	public function setAddtional_value($_addtional_value) {
		$this->_addtional_value = $_addtional_value;
	}

	/**
	 * @return the $_validity
	 */
	public function getValidity() {
		return $this->_validity;
	}

	/**
	 * @param field_type $_validity
	 */
	public function setValidity($_validity) 
	{
            
            $locale = Eicra_Global_Variable::getSession()->sess_lang;
		   
            if($_validity)
            {
                    $data_arr = preg_split('/[- :]/',$_validity);
                    if($data_arr[0])
                    {
                            $date_obj = new Zend_Date($_validity, null, $locale);
                            //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                            $this->_validity = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", $time);
                    }
            }
            else
            {
                    $date_obj = new Zend_Date((time() + (10 * 365 * 24 * 60 *60)), null, $locale);
                    $this->_validity = $this->_validity = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
            }
            
            
            
//	    if($_validity)
//	    {
//	        $data_arr = preg_split('/[- :]/',$_validity);
//	        if($data_arr[0])
//	        {
//	            $time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
//	            $this->_validity = date("Y-m-d H:i:s", $time);
//	        }
//	    }
//	    else
//	    {
//	        $this->_validity = date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
//	    }

	}

	/**
	 * @return the $_displayble
	 */
	public function getDisplayble() {
		return $this->_displayble;
	}

	/**
	 * @param field_type $_status
	 */
	public function setDisplayble($_status) {
		$this->_displayble = $_status;
	}

	/**
	 * @return the $_active
	 */
	public function getActive() {
		return $this->_active;
	}

	/**
	 * @param field_type $_active
	 */
	public function setActive($_active) {
		$this->_active = $_active;
	}
	/**
     * @return the $_group_id
     */
    public function getGroup_id ()
    {
        return $this->_group_id;
    }

	/**
     * @param field_type $_group_id
     */
    public function setGroup_id ($_group_id)
    {
        $this->_group_id = $_group_id;
    }
	/**
     * @return the $_selling_order
     */
    public function getSelling_order ()
    {
		if(empty($this->_selling_order))
		{
			$this->setSelling_order('');
		}
		return $this->_selling_order;
    }

	/**
     * @param field_type $_selling_order
     */
    public function setSelling_order ($_selling_order)
    {

        if(!empty($_selling_order))
        {
            $this->_selling_order = $_selling_order;
        }
        else
        {
            $group_id = $this->getGroup_id();
            $category_id = $this->getProduct_grp_id();
            $table_name		=	'b2b_selling_leads';
            $fields_arr		=	array('id','group_id','product_grp_id','selling_order');
            if(empty($this->_id))
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, null);
            }
            else
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $this->_id);
            }
            $OrderObj->setHeighestOrder($group_id,$category_id);
            $last_order = $OrderObj->getHighOrder();

            $this->_selling_order = $last_order + 1;
        }
        return $this;
    }
	/**
	 * @return the $_price_currency_locale
	 */
	public function getPrice_currency_locale() {
		return $this->_price_currency_locale;
	}

	/**
	 * @param field_type $_price_currency_locale
	 */
	public function setPrice_currency_locale($_price_currency_locale) {
		$this->_price_currency_locale = $_price_currency_locale;
	}




}
