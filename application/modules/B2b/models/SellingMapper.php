<?php

class B2b_Model_SellingMapper
{

    protected $_dbTable;

    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable ()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('B2b_Model_DbTable_Selling');
        }
        return $this->_dbTable;
    }

    public function save (B2b_Model_Selling $obj)
    {
        $id = $obj->getId();
        $data = array(
            'id' => $obj->getId(),
            'user_id' => $obj->getUser_id(),
            'product_grp_id' => $obj->getProduct_grp_id(),
            'group_id' => $obj->getGroup_id(),
            'category' => $obj->getCategory(),
            'name' => $obj->getName(),
            'quantity' => $obj->getQuantity(),
            'qty_per_unit' => $obj->getQty_per_unit(),
        	'price_currency_locale' => $obj->getPrice_currency_locale(),
            'price_currency' => $obj->getPrice_currency(),
            'price' => $obj->getPrice(),
            'strikethrough_price' => $obj->getStrikethrough_price(),
            'price_per_unit' => $obj->getPrice_per_unit(),
            'subject' => $obj->getSubject(),
            'keyword' => $obj->getKeyword(),
            'keyword_1' => $obj->getKeyword_1(),
            'keyword_2' => $obj->getKeyword_2(),
            'keyword_3' => $obj->getKeyword_3(),
            'seo_title' => $obj->getSeo_title(),
            'description' => $obj->getDescription(),
            'detail_description' => $obj->getDetail_description(),
            'primary_file_field' => $obj->getPrimary_file_field(),
            'others_images' => $obj->getOthers_images(),
            'displayble' => $obj->getDisplayable(),
            'negotiable' => $obj->getNegotiable(),
            'fob_price' => $obj->getFob_price(),
            'delivery' => $obj->getDelivery(),
            'delivery_leadtime' => $obj->getDelivery_leadtime(),
            'size' => $obj->getSize(),
            'unit_size' => $obj->getUnit_size(),
            'type' => $obj->getType(),
            'specifications' => $obj->getSpecifications(),
            'packaging' => $obj->getPackaging(),
            'brand_name' => $obj->getBrand_name(),
            'model_num' => $obj->getModel_num(),
            'payment_type' => $obj->getPayment_type(),
            'manufacturers' => $obj->getManufacturers(),
            'place_of_origin' => $obj->getPlace_of_origin(),
            'related_items' => $obj->getRelated_items(),
            'colour' => $obj->getColour(),
            'min_bidding_price' => $obj->getMin_bidding_price(),
            'materials' => $obj->getMaterials(),
            'shipping' => $obj->getShipping(),
            'addtional_value' => $obj->getAddtional_value(),
            'validity' => $obj->getValidity(),
            'displayble' => $obj->getDisplayable(),
            'active' => $obj->getActive()
        );
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);
            try {
                $data['selling_order'] = $obj->getSelling_order();
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $last_id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage()
                );
            }
        } else {
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));
                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }
        return $result;
    }

    public function fetchAll ($pageNumber, $approve = null, $search_params = null , $tableColumns)
    {

        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }

    public function delete ($id)
    {
        $result = null;
        try {
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->delete($where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }

    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );
        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
            return $result;
        } else {
            throw new Exception('Invalid Product or Status Selected ' . $id);
        }
    }

    public function displayable ($id, $displayble)
    {
        $result = null;

        try
        {
            $data = array('displayble' => $displayble);

            $where = array('id = ?' => $id);

            $this->getDbTable()->update($data, $where);

            $result = array('status' => 'ok' ,'id' => $id);

        }
        catch (Exception $e)
        {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }
        return $result;
    }


    public function featured ($id, $featured)
    {
        $result = null;

        try {
            $data = array(
                    'featured' => $featured
            );

            $where = array(
                    'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                    'status' => 'ok',
                    'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
            );
        }
        return $result;
    }
    public function getExistingLeads ($user_id){

        $result = null;
        try
        {
           $result = $this->getDbTable()->getExistingLeads($user_id);

        }
        catch (Exception $e)
        {
            $result = null;
        }
        return $result;
    }

}