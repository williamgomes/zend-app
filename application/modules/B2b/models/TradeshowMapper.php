<?php

class B2b_Model_TradeshowMapper
{

    protected $_dbTable;

    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {

            $dbTable = new $dbTable();
        }

        if (! $dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable ()

    {
        if (null === $this->_dbTable) {

            $this->setDbTable('B2b_Model_DbTable_Tradeshow');
        }

        return $this->_dbTable;
    }

    public function save (B2b_Model_Tradeshow $obj)
    {
        $id = $obj->getId();

        $data = array(
            'id' => $obj->getId(),
            'user_id' => $obj->getUser_id(),
            'name' => $obj->getName(),
            'title' => $obj->getTitle(),
            'seo_title' => $obj->getSeo_title(),
            'organizer' => $obj->getOrganizer(),
            'start_date' => $obj->getStart_date(),
            'end_date' => $obj->getEnd_date(),
            'timepicker' => $obj->getTimepicker(),
            'country' => $obj->getCountry(),
            'state' => $obj->getState(),
            'postal_code' => $obj->getPostal_code(),
            'city' => $obj->getCity(),
            'street' => $obj->getStreet(),
            'description' => $obj->getDescription(),
            'b2b_images' => $obj->getB2b_images(),
            'added_on' => $obj->getAdded_on(),
            'venue' => $obj->getVenue(),
            'address' => $obj->getAddress(),
            'exhibitors' => $obj->getExhibitors(),
            'attendees' => $obj->getAttendees(),
            'floor' => $obj->getFloor(),
            'size' => $obj->getSize(),
            'phone' => $obj->getPhone(),
            'phone_part2' => $obj->getPhone_part2(),
            'phone_part3' => $obj->getPhone_part3(),
            'fax' => $obj->getFax(),
            'fax_part2' => $obj->getFax_part2(),
            'fax_part3' => $obj->getFax_part3(),
            'website' => $obj->getWebsite(),
            'focus' => $obj->getFocus(),
            'product_service' => $obj->getProduct_service(),
            'theme' => $obj->getTheme(),
            'homepage' => $obj->getHomepage(),
            'logo' => $obj->getLogo(),
            'summary' => $obj->getSummary(),
            'attendee_info' => $obj->getAttendee_info(),
            'exhibitor_info' => $obj->getExhibitor_info(),
            'displayable' => $obj->getStatus(),
            'active' => $obj->getActive()
        );

        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);
            try
            {
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $last_id
                );
            }
            catch (Exception $e)
            {
                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage()
                );
            }
        }

        else

        {

            // Start the Update process

            try {

                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));

                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            }

            catch (Exception $e)

            {

                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }

        return $result;
    }


    public function fetchAll ($pageNumber, $approve = null, $search_params = null, $tableColumns = array('userChecking' => true))
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }



    public function delete ($id)
    {
        $result = null;

        try {

            $where = array(
                'id = ?' => $id
            );

            $this->getDbTable()->delete($where);

            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {

            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }

        return $result;
    }

    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );
        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
            return $result;
        } else {
            throw new Exception('Oops : Invalid Product or Status Selected ' . $id);
        }
    }

    public function getShowsByTitle ($title)
    {
        $result = null;
        try {
            if ($title) {
                $result = $this->getDbTable()->getShowByTitle($title, '1');
            }
        } catch (Exception $e) {
            $result = null;
        }
        return $result;
    }

    public function getTradeShows ($pageNumber, $approve, $search_params = null, $userChecking = true){

        $paginator = null;
        try {
            $resultSet = $this->getDbTable()->getAllShows($approve, $search_params, $userChecking);
            $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
            $paginator = Zend_Paginator::factory($resultSet);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);
            return $paginator;

        } catch (Exception $e) {
            return $e->getMessage() ;
        }
        return $paginator;
    }

    public function displayable ($id, $displayable)
    {
        $result = null;
        try {
            $data = array(
                    'displayable' => $displayable
            );
            $where = array(
                    'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                    'status' => 'ok',
                    'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
            );
        }
        return $result;
    }
}