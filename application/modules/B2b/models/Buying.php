<?php
class B2b_Model_Buying
{
    protected $_id;
    protected $_user_id;
    protected $_product_grp_id;
    protected $_group_id;
    protected $_buying_order;
    protected $_category;
    protected $_name;
    protected $_seo_title;
    protected $_keyword;
    protected $_keyword_1;
    protected $_keyword_2;
    protected $_keyword_3;
    protected $_description;
    protected $_displayble;
    protected $_quantity;
    protected $_qty_per_unit;
    protected $_price_currency_locale ;
    protected $_price_currency;
    protected $_price;
    protected $_price_per_unit;
    protected $_buying_capacity;
    protected $_max_buying_unit;
    protected $_subject;
    protected $_validity;
    protected $_specifications;
    protected $_packaging;
    protected $_delivery;
    protected $_delivery_leadtime;
    protected $_primary_file_field;
    protected $_others_images;
    protected $_shipping;
    protected $_shipping_price;
    protected $_payment_type;
    protected $_company_name;
    protected $_email;
    protected $_country;
    protected $_address;
    protected $_preference;
    protected $_website;
    protected $_business_type;
    protected $_active;


    /**
	 * @return the $_price_currency_locale
	 */
	public function getPrice_currency_locale() {
		return $this->_price_currency_locale;
	}

	/**
	 * @param field_type $_price_currency_locale
	 */
	public function setPrice_currency_locale($_price_currency_locale) {
		$this->_price_currency_locale = $_price_currency_locale;
	}

	public function __construct (array $options = null)

    {
        if (is_array($options)) {

            $this->setOptions($options);
        }
    }
    public function __set ($name, $value)

    {
        $method = 'set' . $name;

        if (('mapper' == $name) || ! method_exists($this, $method)) {

            throw new Exception('Invalid Auth property' . $method);
        }

        $this->$method($value);
    }
    public function __get ($name)

    {
        $method = 'get' . $name;

        if (('mapper' == $name) || ! method_exists($this, $method)) {

            throw new Exception('Invalid Auth property' );
        }

        return $this->$method();
    }
    public function setOptions (array $options)

    {
        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {

            $method = 'set' . ucfirst($key);

            if (in_array($method, $methods)) {

                $this->$method($value);
            }
        }

        return $this;
    }
    public function save ()
    {
        $mapper = new B2b_Model_BuyingMapper();
        $return = $mapper->save($this);
        return $return;
    }
    /**
     *
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }
    /**
     *
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }
    /**
     *
     * @return the $_user_id
     */
    public function getUser_id ()
    {
        return $this->_user_id;
    }
    /**
     *
     * @param field_type $_user_id
     */
    public function setUser_id ($_user_id)
    {
        $auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$globalIdentity = $auth->getIdentity ();
		  $this->_user_id = ($globalIdentity->allow_to_change_ownership != '1' || empty($_user_id)) ? $globalIdentity->user_id : $_user_id;
		} else {
			$this->_user_id = $_user_id;
		}
		return $this; 
    }
    /**
     * @return the $_product_grp_id
     */
    public function getProduct_grp_id ()
    {
        return $this->_product_grp_id;
    }

	/**
     * @param field_type $_product_grp_id
     */
    public function setProduct_grp_id ($_product_grp_id)
    {
        $this->_product_grp_id = $_product_grp_id;
    }

	/**
     * @return the $_group_id
     */
    public function getGroup_id ()
    {
        return $this->_group_id;
    }

	/**
     * @param field_type $_group_id
     */
    public function setGroup_id ($_group_id)
    {
        $this->_group_id = $_group_id;
    }

	/**
     * @return the $_seo_title
     */
    public function getSeo_title ()
    {
        return $this->_seo_title;
    }

	/**
     * @param field_type $_seo_title
     */
    public function setSeo_title ($text, $id = null)
    {        
		$pattern = Eicra_File_Constants::TITLE_PATTERN;
		$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
		$text = preg_replace ( $pattern, $replacement, trim ( $text ) );
		
		// DB Connection		
		$buying_db = new B2b_Model_DbTable_Buying();

		if (empty ( $id )) 
		{
			$select = $buying_db->select ()->from ( array ( 'buy' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_buying_leads' ), array ( 'buy.id' ) )
											->where ( 'buy.seo_title = ?', $text );
		} 
		else 
		{
			$select = $buying_db->select ()->from ( array ( 'buy' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_buying_leads' ), array ( 'buy.id' ) )
											->where ( 'buy.seo_title = ?', $text )->where ( 'buy.id != ?', $id );
		}
		
		$rs = $select->query ()->fetchAll ();
		if ($rs) 
		{
			foreach ( $rs as $row ) 
			{
				$ids = ( int ) $row ['id'];
			}
		}
		if (empty ( $ids )) 
		{
			$this->_seo_title = $text;
		} 
		else 
		{
			$select = $buying_db->select ()->from ( array ( 'buy' => Zend_Registry::get ( 'dbPrefix' ) . 'b2b_buying_leads' ), array ( 'buy.id' ) )
											->order ( array ( 'buy.id DESC' ) )
											->limit ( 1 );
			
			$rs = $select->query ()->fetchAll ();
			if ($rs) 
			{
				foreach ( $rs as $row ) 
				{
					$last_id = ( int ) $row ['id'];
				}
			}
			
			if (empty ( $id )) 
			{
				$this->_seo_title = $text . '-' . ($last_id + 1);
			} 
			else 
			{
				$this->_seo_title = $text . '-' . $id;
			}
		}
		return $this;
    }

	/**
     * @return the $_buying_order
     */
    public function getBuying_order ()
    {
        if(empty($this->_buying_order))
		{
			$this->setBuying_order('');
		}
		return $this->_buying_order;
    }

	/**
     * @param field_type $_buying_order
     */
    public function setBuying_order ($_buying_order)
    {
        if(!empty($_buying_order))
        {
            $this->_buying_order = $_buying_order;
        }
        else
        {
            $group_id = $this->getGroup_id();
            $user_id = $this->getUser_id();
            $table_name		=	'b2b_buying_leads';
            $fields_arr		=	array('id','group_id','user_id','buying_order');
            if(empty($this->_id))
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, null);
            }
            else
            {
                $OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $this->_id);
            }
            $OrderObj->setHeighestOrder($group_id,$user_id);
            $last_order = $OrderObj->getHighOrder();

            $this->_buying_order = $last_order + 1;
        }
        return $this;
    }

	/**
     *
     * @return the $_category
     */
    public function getCategory ()
    {
        return $this->_category;
    }
    /**
     *
     * @param field_type $_category
     */
    public function setCategory ($_category)
    {
        $this->_category = $_category;
    }
    /**
     *
     * @return the $_name
     */
    public function getName ()
    {
        return $this->_name;
    }
    /**
     *
     * @param field_type $_name
     */
    public function setName ($_name)
    {
        $this->_name = $_name;
    }
    /**
     *
     * @return the $_keyword
     */
    public function getKeyword ()
    {
        return $this->_keyword;
    }
    /**
     *
     * @param field_type $_keyword
     */
    public function setKeyword ($_keyword)
    {
        $this->_keyword = $_keyword;
    }
    /**
     *
     * @return the $_keyword_1
     */
    public function getKeyword_1 ()
    {
        return $this->_keyword_1;
    }
    /**
     *
     * @param field_type $_keyword_1
     */
    public function setKeyword_1 ($_keyword_1)
    {
        $this->_keyword_1 = $_keyword_1;
    }
    /**
     *
     * @return the $_keyword_2
     */
    public function getKeyword_2 ()
    {
        return $this->_keyword_2;
    }
    /**
     *
     * @param field_type $_keyword_2
     */
    public function setKeyword_2 ($_keyword_2)
    {
        $this->_keyword_2 = $_keyword_2;
    }
    /**
     *
     * @return the $_keyword_3
     */
    public function getKeyword_3 ()
    {
        return $this->_keyword_3;
    }
    /**
     *
     * @param field_type $_keyword_3
     */
    public function setKeyword_3 ($_keyword_3)
    {
        $this->_keyword_3 = $_keyword_3;
    }
    /**
     *
     * @return the $_description
     */
    public function getDescription ()
    {
        return $this->_description;
    }
    /**
     *
     * @param field_type $_description
     */
    public function setDescription ($_description)
    {
        $this->_description = $_description;
    }
    /**
     *
     * @return the $_displayble
     */
    public function getDisplayble ()
    {
        return $this->_displayble;
    }
    /**
     *
     * @param field_type $_displayble
     */
    public function setDisplayble ($_displayble)
    {
        $this->_displayble = $_displayble;
    }
    /**
     *
     * @return the $_quantity
     */
    public function getQuantity ()
    {
        return $this->_quantity;
    }
    /**
     *
     * @param field_type $_quantity
     */
    public function setQuantity ($_quantity)
    {
        $this->_quantity = $_quantity;
    }
    /**
     *
     * @return the $_qty_per_unit
     */
    public function getQty_per_unit ()
    {
        return $this->_qty_per_unit;
    }
    /**
     *
     * @param field_type $_qty_per_unit
     */
    public function setQty_per_unit ($_qty_per_unit)
    {
        $this->_qty_per_unit = $_qty_per_unit;
    }
    /**
     *
     * @return the $_price_currency
     */
    public function getPrice_currency ()
    {
        return $this->_price_currency;
    }
    /**
     *
     * @param field_type $_price_currency
     */
    public function setPrice_currency ($_price_currency)
    {
        $this->_price_currency = $_price_currency;
    }
    /**
     *
     * @return the $_price
     */
    public function getPrice ()
    {
        return $this->_price;
    }
    /**
     *
     * @param field_type $_price
     */
    public function setPrice ($_price)
    {
        $this->_price = $_price;
    }
    /**
     *
     * @return the $_price_per_unit
     */
    public function getPrice_per_unit ()
    {
        return $this->_price_per_unit;
    }
    /**
     *
     * @param field_type $_price_per_unit
     */
    public function setPrice_per_unit ($_price_per_unit)
    {
        $this->_price_per_unit = $_price_per_unit;
    }
    /**
     *
     * @return the $_buying_capacity
     */
    public function getBuying_capacity ()
    {
        return $this->_buying_capacity;
    }
    /**
     *
     * @param field_type $_buying_capacity
     */
    public function setBuying_capacity ($_buying_capacity)
    {
        $this->_buying_capacity = $_buying_capacity;
    }
    /**
     *
     * @return the $_max_buying_unit
     */
    public function getMax_buying_unit ()
    {
        return $this->_max_buying_unit;
    }
    /**
     *
     * @param field_type $_max_buying_unit
     */
    public function setMax_buying_unit ($_max_buying_unit)
    {
        $this->_max_buying_unit = $_max_buying_unit;
    }
    /**
     *
     * @return the $_subject
     */
    public function getSubject ()
    {
        return $this->_subject;
    }
    /**
     *
     * @param field_type $_subject
     */
    public function setSubject ($_subject)
    {
        $this->_subject = $_subject;
    }
    /**
     *
     * @return the $_validity
     */
    public function getValidity ()
    {
        $locale = Eicra_Global_Variable::getSession()->sess_lang;
        $date_obj = new Zend_Date($this->_validity);
        $this->_validity = $date_obj->get('YYYY-MM-dd HH:mm:ss', $locale);
        return $this->_validity;
    }
    /**
     *
     * @param field_type $_validity
     */
    public function setValidity ($_validity)
    {
        
        
        $locale = Eicra_Global_Variable::getSession()->sess_lang;
		   
        if($_validity)
        {
                $data_arr = preg_split('/[- :]/',$_validity);
                if($data_arr[0])
                {
                    $date_obj = new Zend_Date($_validity, null, $locale);
                    //$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);
                    $this->_validity = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", $time);
                    
                }
        }
        else
        {
                $date_obj = new Zend_Date((time() + (10 * 365 * 24 * 60 *60)), null, $locale);
                $this->_validity = $this->_validity = $date_obj->get('YYYY-MM-dd HH:mm:ss'); //date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
        }
        
        
        
//        if ($_validity) {
//            $data_arr = preg_split('/[- :]/', $_validity);
//            if ($data_arr[0]) {
//                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
//                $this->_validity = date("Y-m-d H:i:s", $time);
//            }
//        } else {
//            $this->_validity = date("Y-m-d H:i:s", (time() + (10 * 365 * 24 * 60 *60)));
//        }
    }
    /**
     *
     * @return the $_specifications
     */
    public function getSpecifications ()
    {
        return $this->_specifications;
    }
    /**
     *
     * @param field_type $_specifications
     */
    public function setSpecifications ($_specifications)
    {
        $this->_specifications = $_specifications;
    }
    /**
     *
     * @return the $_packaging
     */
    public function getPackaging ()
    {
        return $this->_packaging;
    }
    /**
     *
     * @param field_type $_packaging
     */
    public function setPackaging ($_packaging)
    {
        $this->_packaging = $_packaging;
    }
    /**
     *
     * @return the $_delivery
     */
    public function getDelivery ()
    {
        return $this->_delivery;
    }
    /**
     *
     * @param field_type $_delivery
     */
    public function setDelivery ($_delivery)
    {
        $this->_delivery = $_delivery;
    }
    /**
     *
     * @return the $_delivery_leadtime
     */
    public function getDelivery_leadtime ()
    {
        return $this->_delivery_leadtime;
    }
    /**
     *
     * @param field_type $_delivery_leadtime
     */
    public function setDelivery_leadtime ($_delivery_leadtime)
    {
        $this->_delivery_leadtime = $_delivery_leadtime;
    }
    /**
     *
     * @return the $_primary_file_field
     */
    public function getPrimary_file_field ()
    {
        return $this->_primary_file_field;
    }
    /**
     *
     * @param field_type $_primary_file_field
     */
    public function setPrimary_file_field ($_primary_file_field)
    {
        $this->_primary_file_field = $_primary_file_field;
    }
    /**
     *
     * @return $_others_images
     */
    public function getOthers_images ()
    {
        return $this->_others_images;
    }
    /**
     *
     * @param field_type $_others_images
     */
    public function setOthers_images ($_others_images)
    {
        $this->_others_images = $_others_images;
    }
    /**
     *
     * @return the $_shipping
     */
    public function getShipping ()
    {
        return $this->_shipping;
    }
    /**
     *
     * @param field_type $_shipping
     */
    public function setShipping ($_shipping)
    {
        $this->_shipping = $_shipping;
    }
    /**
     *
     * @return the $_shipping_price
     */
    public function getShipping_price ()
    {
        return $this->_shipping_price;
    }
    /**
     *
     * @param field_type $_shipping_price
     */
    public function setShipping_price ($_shipping_price)
    {
        $this->_shipping_price = $_shipping_price;
    }
    /**
     *
     * @return the $_payment_type
     */
    public function getPayment_type ()
    {
        return $this->_payment_type;
    }
    /**
     *
     * @param field_type $_payment_type
     */
    public function setPayment_type ($_payment_type)
    {
        $this->_payment_type = $_payment_type;
    }
    /**
     *
     * @return the $_company_name
     */
    public function getCompany_name ()
    {
        return $this->_company_name;
    }
    /**
     *
     * @param field_type $_company_name
     */
    public function setCompany_name ($_company_name)
    {
        $this->_company_name = $_company_name;
    }
    /**
     *
     * @return the $_email
     */
    public function getEmail ()
    {
        return $this->_email;
    }
    /**
     *
     * @param field_type $_email
     */
    public function setEmail ($_email)
    {
        $this->_email = $_email;
    }
    /**
     *
     * @return the $_country
     */
    public function getCountry ()
    {
        return $this->_country;
    }
    /**
     *
     * @param field_type $_country
     */
    public function setCountry ($_country)
    {
        $this->_country = $_country;
    }
    /**
     *
     * @return the $_address
     */
    public function getAddress ()
    {
        return $this->_address;
    }
    /**
     *
     * @param field_type $_address
     */
    public function setAddress ($_address)
    {
        $this->_address = $_address;
    }
    /**
     *
     * @return the $_preference
     */
    public function getPreference ()
    {
        return $this->_preference;
    }
    /**
     *
     * @param field_type $_preference
     */
    public function setPreference ($_preference)
    {
        $this->_preference = $_preference;
    }
    /**
     *
     * @return the $_website
     */
    public function getWebsite ()
    {
        return $this->_website;
    }
    /**
     *
     * @param field_type $_website
     */
    public function setWebsite ($_website)
    {
        $this->_website = $_website;
    }
    /**
     *
     * @return the $_business_type
     */
    public function getBusiness_type ()
    {
        return $this->_business_type;
    }
    /**
     *
     * @param field_type $_business_type
     */
    public function setBusiness_type ($_business_type)
    {
        $this->_business_type = $_business_type;
    }
    /**
     *
     * @return the $_active
     */
    public function getActive ()
    {
        return $this->_active;
    }
    /**
     *
     * @param field_type $_active
     */
    public function setActive ($_active)
    {
        $this->_active = $_active;
    }
}