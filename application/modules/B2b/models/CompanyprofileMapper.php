<?php
class B2b_Model_CompanyprofileMapper
{
    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided : ' . serialize($dbTable));
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    public function getDbTable ()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('B2b_Model_DbTable_CompanyProfile');
        }
        return $this->_dbTable;
    }
    public function save (B2b_Model_Companyprofile $obj)
    {
        $id = null;
        $profileDB = $this->getDbTable()->getUserProfile($obj->getUser_id());
        $compliances = $obj->getCompliances();
        $bizType_id = $obj->getBusinesstype_id();
        $user_id = $obj->getUser_id();
        if (! empty($profileDB)) 
		{
            $id = $profileDB['id'];
			$obj->setSeo_title($obj->getCompany_name(), $id);
        }
		else
		{
			$obj->setSeo_title($obj->getCompany_name());
		}
        $data = array(
            'group_id' => $obj->getGroup_id(),
            'user_id' => $user_id,
            'businesstype_id' => $bizType_id,
            'plan_id' => $obj->getPlan_id(),
            'category_id' => $obj->getCategory_id(),
            'category_id' => $obj->getCategory_id(),
            'trade_type' => $obj->getTrade_type(),
            'company_name' => $obj->getCompany_name(),
            'registered_in' => $obj->getRegistered_in(),
            'street' => $obj->getStreet(),
            'city' => $obj->getCity(),
            'profile_image_primary' => $obj->getProfile_image_primary(),
            'profile_image' => $obj->getProfile_image(),
            'avatar_img' => $obj->getAvatar_img(),
            'banner_img' => $obj->getBanner_img(),
            'postal_code' => $obj->getPostal_code(),
            'company_category' => $obj->getCompany_category(),
            'selling_categories' => $obj->getSelling_categories(),
            'buying_categories' => $obj->getBuying_categories(),
            'QA_policy' => $obj->getQA_policy(),
            'store_name' => $obj->getStore_name(),
            'legal_owner' => $obj->getLegal_owner(),
            'office_size' => $obj->getOffice_size(),
            'establishing_year' => $obj->getEstablishing_year(),
            'number_of_employees' => $obj->getNumber_of_employees(),
            'annual_revenue' => $obj->getAnnual_revenue(),
            'main_products' => $obj->getMain_products(),
            'main_products_part1' => $obj->getMain_products_part1(),
            'main_products_part2' => $obj->getMain_products_part2(),
            'main_products_part3' => $obj->getMain_products_part3(),
            'other_products' => $obj->getOther_products(),
            'certification' => $obj->getCertification(),
            'main_markets' => $obj->getMain_markets(),
            'annual_sales' => $obj->getAnnual_sales(),
            'nearest_ports' => $obj->getNearest_ports(),
            'main_customers' => $obj->getMain_customers(),
            'avg_lead_time' => $obj->getAvg_lead_time(),
            'overseas_office' => $obj->getOverseas_office(),
            'delivery_terms' => $obj->getDelivery_terms(),
            'payment_currency' => $obj->getPayment_currency(),
            'payment_type' => $obj->getPayment_type(),
            'language' => $obj->getLanguage(),
            'factory_location' => $obj->getFactory_location(),
            'website' => $obj->getWebsite(),
            'phone' => $obj->getPhone(),
            'phone_part2' => $obj->getPhone_part2(),
            'phone_part3' => $obj->getPhone_part3(),
            'fax' => $obj->getFax(),
            'fax_part2' => $obj->getFax_part2(),
            'fax_part3' => $obj->getFax_part3(),
            'mobile' => $obj->getMobile(),
            'mobile_part2' => $obj->getMobile_part2(),
            'mobile_part3' => $obj->getMobile_part3(),
            'company_information' => $obj->getCompany_information(),
            'policy' => $obj->getPolicy(),
            'terms_condition' => $obj->getTerms_condition(),
            'added_on' => $obj->getAdded_on(),
            'certified' => $obj->getCertified(),
            'seo_title' => $obj->getSeo_title(),
            'factory_size' => $obj->getFactory_size(),
            'export_market' => $obj->getExport_market(),
            'compliances' => $compliances,
            'escrow' => $obj->getEscrow(),
            'export_percentage' => $obj->getExport_percentage(),
            'trade_shows' => $obj->getTrade_shows(),
            'production_capacity' => $obj->getProduction_capacity(),
            'country' => $obj->getCountry(),
            'payment_method' => $obj->getPayment_method(),
            'keywords' => $obj->getKeywords(),
            'meta_desc' => $obj->getMeta_desc(),
            'active' => $obj->getActive(),
            'displayble' => $obj->getDisplayble(),

        )
        ;
        $this->saveCompliancesMapper($compliances, $user_id);
        $this->saveBizTypeMapper($bizType_id, $user_id);
        if ($id == null || empty($id)) {
            unset($data['id']);
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $last_id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage() . serialize($profileDB)
                );
            }
        } else {
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));
                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            } catch (Exception $e) {
                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }
        return $result;
    }
    public function getDefaultProfile ($user_id)
    {
        $resultSet = $this->getDbTable()->getUserProfile($user_id);
        $compliancesTbl = new B2b_Model_DbTable_CompliancesMapper();
        $biztype = new B2b_Model_DbTable_BiztypeMapper();
        $businesstype = $biztype->getBiztype($user_id);
        $compliances = $compliancesTbl->getCompliances($user_id);
        $businesstype_id = array();
        $compliances_id = array();
        for ($i = 0; $i < count($businesstype); $i ++) {
            $businesstype_id[$i] = $businesstype[$i]['biztype_id'];
        }
        for ($i = 0; $i < count($compliances); $i ++) {
            $compliances_id[$i] = $compliances[$i]['compliance_id'];
        }
        if (is_array($resultSet) && (count($resultSet) > 1)) {
            $resultSet['businesstype_id'] = $businesstype_id;
            $resultSet['compliances'] = $compliances_id;
        }		
        return $resultSet;
    }
    public function fetchAll ($pageNumber, $approve = null, $search_params = null, $tableColumns = null)
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
    public function delete ($id)
    {
        $result = null;
        try {
			$company_info = $this->getDbTable()->getInfo ($id);
			
			$profile_image = explode(',',$company_info['profile_image']);
			$banner_img = explode(',', $company_info['banner_img']);
			$avatar_img = explode(',', $company_info['avatar_img']);
			
			$image_path = 'data/frontImages/b2b/profile_image';
			
			
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->delete($where);
			foreach($profile_image as $key=>$file)
			{
				if($file)
				{
					$dir = $image_path.DS.$file;
					$res = Eicra_File_Utility::deleteRescursiveDir($dir);
				}
			}
			
			foreach($banner_img as $key=>$file)
			{
				if($file)
				{
					$dir = $image_path.DS.$file;
					$res = Eicra_File_Utility::deleteRescursiveDir($dir);
				}
			}
			
			foreach($avatar_img as $key=>$file)
			{
				if($file)
				{
					$dir = $image_path.DS.$file;
					$res = Eicra_File_Utility::deleteRescursiveDir($dir);
				}
			}
			
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }


    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );
        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
            return $result;
        } else {
            throw new Exception('Oops : Invalid Product or Status Selected ' . $id);
        }
    }
    private function saveCompliancesMapper ($compliances, $user_id)
    {
        $compliancesMapper = new B2b_Model_DbTable_CompliancesMapper();
        if (! empty($compliances) && is_numeric($user_id)) {
            $list = explode(',', $compliances);
            $result = array();
            for ($i = 0; $i < count($list); $i ++) {
                $row = array();
                $compliances_id = $list[$i];
                if ($compliances_id) {
                    $row['user_id'] = $user_id;
                    $row['compliance_id'] = $compliances_id;
                    $result[$i] = $row;
                }
            }
            if (count($result) > 0) {
                $compliancesMapper->saveCompliances($result, $user_id);
            }
        } else {
            $compliancesMapper->deleteCompliances($user_id);
        }
    }
    private function saveBizTypeMapper ($bizType, $user_id)
    {
        $biztypes = new B2b_Model_DbTable_BiztypeMapper();
        if (! empty($bizType) && is_numeric($user_id)) {
            $list = explode(',', $bizType);
            $result = array();
            for ($i = 0; $i < count($list); $i ++) {
                $row = array();
                $biztype_id = $list[$i];
                if ($biztype_id) {
                    $row['user_id'] = $user_id;
                    $row['biztype_id'] = $biztype_id;
                    $result[$i] = $row;
                }
            }
            if (count($result) > 0) {
                $biztypes->savebiztype($result, $user_id);
            }
        }
        else {
            $biztypes->deleteBiztype($user_id);
        }
    }
}

?>