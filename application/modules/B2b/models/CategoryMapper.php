<?php
class B2b_Model_CategoryMapper
{
    protected $_dbTable;
    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }

        if (! $dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;

        return $this;
    }
    public function getDbTable ()
    {
        if (null === $this->_dbTable) {

            $this->setDbTable('B2b_Model_DbTable_Category');
        }
        return $this->_dbTable;
    }
    public function save (B2b_Model_Category $obj)
    {
        $id = $obj->getId();
        $data = array(
        'id' => $obj->getId(), 'group_id' => $obj->getGroup_id(),
                'parent_id' => $obj->getParent_id(),
                'name' => $obj->getName(),
                'description' => $obj->getDescription(),
                'active' => $obj->getActive(),
                'login' => $obj->getLogin(),
                'b2b_images' => $obj->getB2b_images(),
                'added_on' => $obj->getAdded_on(),
                'package_id' => $obj->getPackage_id(),
                'url_portion' => $obj->getUrl_portion(),
                'isComment' => $obj->getIscomment(),
                'isShowProfile' => $obj->getIsshowprofile(),
                'displayble' => $obj->getDisplayble(),
                'user_id' => $obj->getUser_id());

        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);
            $data['category_order'] = $obj->getCategory_order();
            try {
                $last_id = $this->getDbTable()->insert($data);
                $this->getDbTable()->update(array('category_order' => $last_id), array('id = ?' => $last_id));

                $result = array('status' => 'ok', 'id' => $last_id);
            }
            catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id,
                        'msg' => $e->getMessage());
            }
        }
        else {
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            }
            catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id,
                        'msg' => $e->getMessage());
            }
        }

        return $result;
    }
    public function fetchAll ($pageNumber, $approve = null, $search_params = null, $userChecking = true)
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $userChecking);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
    public function delete ($id)
    {
        $result = null;
        try 
		{
			//cat_info		
			$cat_search_params['filter']['filters'][] = array('field' => 'id','operator' => 'eq','value' => $id);	
			$cat_info = $this->getDbTable()->getListInfo(null, $cat_search_params, false);		
			$category_name = ($cat_info && $cat_info[0] && $cat_info[0]['name']) ? $cat_info[0]['name'] : null;
			$category_thumb = ($cat_info && $cat_info[0] && $cat_info[0]['b2b_images']) ? $cat_info[0]['b2b_images'] : null;
			
			//Get It's Child category_order
			$table_name = 'b2b_category';
			$fields_arr = array('id', 'group_id', 'parent_id', 'category_order');
			$OrderObj = new Administrator_Controller_Helper_GlobalOrders($table_name, $fields_arr, $id);
			$parent_id = $OrderObj->getCategory_id();
			$parent_id = 	($category_id) ? $category_id : 0;
			$search_params['filter']['filters'][] = array('field' => 'parent_id','operator' => 'eq','value' => $parent_id);
			$rs = $this->getDbTable()->getListInfo (null, $search_params, false);
			if($rs)
			{				
				foreach($rs as $row)
				{
					$sub_id = (int)$row['id'];
					$OrderObj->setNewOrder($sub_id); 
				}
			}
			
			//update parents of it's child
			$whereP = array('parent_id = ?' => $id);
			$this->getDbTable()->update(array('parent_id' => $parent_id),$whereP);
			
			//Update category_id of the buying, selling, product of this category to zero
			$whereA = array('category = ?' => $id);
			$buying_db = new B2b_Model_DbTable_Buying();
			$buying_db->update(array('category' => $parent_id),$whereA);
			
			$whereB = array('category = ?' => $id);
			$selling_db = new B2b_Model_DbTable_Selling();
			$selling_db->update(array('category' => $parent_id),$whereB);
			
			$whereC = array('category_id = ?' => $id);
			$product_db = new B2b_Model_DbTable_Products();
			$product_db->update(array('category_id' => $parent_id), $whereC);
			
			$whereD = array('category_id = ?' => $id);
			$company_db = new B2b_Model_DbTable_CompanyProfile();
			$company_db->update(array('category_id' => $parent_id), $whereD);
			
			// Remove from Category
           	$where = array('id = ?' => $id);
            $this->getDbTable()->delete($where);
			
			//REMOVE IMAGE
			
			if($cat_info && $cat_info[0] && $cat_info[0]['file_path_b2b_images'] && $category_thumb)
			{
				$dir = $cat_info[0]['file_path_b2b_images'].DS.$category_thumb;				
				if(file_exists($dir))
				{
					$res = Eicra_File_Utility::deleteRescursiveDir($dir);
				}
			}
			
            $result = array('status' => 'ok', 'id' => $id);
        } catch (Exception $e) {
            $result = array('status' => 'err', 'id' => $id,
                    'msg' => $e->getMessage());
        }

        return $result;
    }

    public function updateStatus($data , $id){
        $where = array('id = ?' => $id);
        if ((!empty($id) && is_numeric($id)) && !empty($data)  ){
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array('status' => 'ok' ,'id' => $id);
            return $result;
        }
        else {
            throw new Exception('Invalid Product or Status Selected ' . $id);
        }
    }

    public function updateOrder ($id, $order)
    {
        $result = null;
        try
        {
            $data = array('category_order' => $order);
            $where = array('id = ?' => $id);
            $this->getDbTable()->update($data, $where);
            $result = array('status' => 'ok' ,'id' => $id);
        }
        catch (Exception $e)
        {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }
        return $result;
    }


    public function showProfile ($id, $showProfile)
	{
	    $result = null;
	    try
	    {
	        $data = array('isshowprofile' => $showProfile);
	        $where = array('id = ?' => $id);
	        $this->getDbTable()->update($data, $where);
	        $result = array('status' => 'ok' ,'id' => $id);
	    }
	    catch (Exception $e)
	    {
	        $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
	    }
	    return $result;
	}

	public function displayable ($id, $displayble)
	{
	    $result = null;
	    try
	    {
	        $data = array('displayble' => $displayble);
	        $where = array('id = ?' => $id);
	        $this->getDbTable()->update($data, $where);
	        $result = array('status' => 'ok' ,'id' => $id);
	    }
	    catch (Exception $e)
	    {
	        $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
	    }
	    return $result;
	}
	public function featured ($id, $featured)
	{
	    $result = null;

	    try {
	        $data = array(
	                'featured' => $featured
	        );

	        $where = array(
	                'id = ?' => $id
	        );
	        $this->getDbTable()->update($data, $where);
	        $result = array(
	                'status' => 'ok',
	                'id' => $id
	        );
	    } catch (Exception $e) {
	        $result = array(
	                'status' => 'err',
	                'id' => $id,
	                'msg' => $e->getMessage()
	        );
	    }
	    return $result;
	}
}
