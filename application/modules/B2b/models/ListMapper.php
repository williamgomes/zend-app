<?php

class B2b_Model_B2bListMapper 

{

	protected $_dbTable;


    public function setDbTable($dbTable)

    {

        if (is_string($dbTable)) {

            $dbTable = new $dbTable();

        }

        if (!$dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');

        }

        $this->_dbTable = $dbTable;

        return $this;

    }

 

    public function getDbTable()

    {

        if (null === $this->_dbTable) {

            $this->setDbTable('B2b_Model_DbTable_B2b');

        }

        return $this->_dbTable;

    }

	

	

   	

	

	public function fetchAll($pageNumber,$group_id = null,$approve = null)

    {				

        $resultSet = $this->getDbTable()->getB2bInfoByGroup($group_id,$approve); 				     

		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;

		$paginator = Zend_Paginator::factory($resultSet);

		$paginator->setItemCountPerPage($viewPageNum);

    	$paginator->setCurrentPageNumber($pageNumber);

        return $paginator;

    }

}

?>