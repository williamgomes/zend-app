<?php
class B2b_Model_UserPreferencesMapper {

	public function setDbTable($dbTable)

	{

		if (is_string($dbTable)) {

			$dbTable = new $dbTable();

		}


		if (!$dbTable instanceof Zend_Db_Table_Abstract) {

			throw new Exception('Invalid table data gateway provided : ' . serialize($dbTable));

		}

		$this->_dbTable = $dbTable;

		return $this;

	}


	public function getDbTable()
	{
		if (null === $this->_dbTable) {

			$this->setDbTable('B2b_Model_DbTable_UserPreferences');
		}

		return $this->_dbTable;

	}


	public function save(B2b_Model_UserPreferences $obj)
	{


		$id =$obj->getId();
		$data = array(
				'id' 				    =>	$obj->getId (),
				'user_id' 				=>	$obj->getUser_id(),
				'facebook' 				=>	$obj->getFacebook(),
				'facebook_display' 		=>	$obj->getFacebook_display(),
				'twitter' 				=>	$obj->getTwitter(),
				'twitter_display' 		=>	$obj->getTwitter_display(),
				'googleplus' 			=>	$obj->getGoogleplus(),
				'googleplus_display' 	=>	$obj->getGoogleplus_display(),
				'youtube' 				=>	$obj->getYoutube(),
				'youtube_display' 		=>	$obj->getYoutube_display(),
				'flickr' 				=>	$obj->getFlickr(),
				'flickr_display' 		=>	$obj->getFlickr_display(),
				'paypal' 				=>	$obj->getPaypal(),
				'paypal_currency' 		=>	$obj->getPaypal_currency(),
				'enable_paypal' 		=>	$obj->getEnable_paypal(),
				'tocheckout' 			=>	$obj->getTocheckout(),
				'tocheckout_local' 		=>	$obj->getTocheckout_local(),
				'enable_2co' 			=>	$obj->getEnable_2co(),
				'moneybookers' 			=>	$obj->getMoneybookers(),
				'moneybookers_currency' =>	$obj->getMoneybookers_currency(),
				'enable_mb' 			=>	$obj->getEnable_mb(),
				'storeurl' 				=>	$obj->getStoreurl(),
				'inquiry' 				=>	$obj->getInquiry(),
				'newslettter' 			=>	$obj->getNewslettter(),
		        'b2b_images' 			=>	$obj->getB2b_images(),
				'other_info' 			=>	$obj->getOther_info(),
				'active' 				=>	$obj->getActive ()

		);



		if ( !is_numeric($id)  || empty($id)  )
		{
			unset($data['id']);
			try
			{
				$last_id = $this->getDbTable()->insert($data);
				$result = array('status' => 'ok' ,'id' => $id  );
			}
			catch (Exception $e)

			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());
			}

		}

		else
		{
			// Start the Update process
			try
			{
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id );
			}


			catch (Exception $e)

			{

				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());

			}

		}

		return $result;

	}

	public function delete ($id){

		$result = null;
		try {
			$where = array('id = ?' => $id);
			$this->getDbTable()->delete($where);
			$result = array('status' => 'ok' ,'id' => $id);
		} catch (Exception $e) {
			$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());

		}

		return $result;
	}
}
