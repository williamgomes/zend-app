<?php

class B2b_Model_UserPreferences {

    private $_id;
    private $_user_id;
    private $_facebook;
    private $_facebook_display;
    private $_twitter;
    private $_twitter_display;
    private $_googleplus;
    private $_googleplus_display;
    private $_youtube;
    private $_youtube_display;
    private $_flickr;
    private $_flickr_display;
    private $_paypal;
    private $_paypal_currency;
    private $_enable_paypal;
    private $_tocheckout;
    private $_tocheckout_local;
    private $_enable_2co;
    private $_moneybookers;
    private $_moneybookers_currency;
    private $_enable_mb;
    private $_storeurl;
    private $_inquiry;
    private $_newslettter;
    private $_b2b_images;
    private $_other_info;
    private $_active;

    public function __construct(array $options = null) {

        if (is_array($options)) {

            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {

        $method = 'set' . $name;

        if (('mapper' == $name) || !method_exists($this, $method)) {

            throw new Exception('Invalid Setter Property - ' . $method);
        }


        $this->method($value);
    }

    public function __get($name) {


        $method = 'get' . $name;


        if (('mapper' == $name) || !method_exists($this, $method)) {

            throw new Exception('Invalid Getter Property - ' . $method);
        }

        return $this->$method();
    }

    public function setOptions(array $options) {


        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {


            $method = 'set' . ucfirst($key);


            if (in_array($method, $methods)) {


                $this->$method($value);
            }
        }


        return $this;
    }

    public function save() {

        $mapper = new B2b_Model_UserPreferencesMapper();

        $return = $mapper->save($this);

        return $return;
    }

    /**
     * @return the $_id
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @return the $_user_id
     */
    public function getUser_id() {
        return $this->_user_id;
    }

    /**
     * @return the $_facebook
     */
    public function getFacebook() {
        return $this->_facebook;
    }

    /**
     * @return the $_facebook_display
     */
    public function getFacebook_display() {
        return $this->_facebook_display;
    }

    /**
     * @return the $_twitter
     */
    public function getTwitter() {
        return $this->_twitter;
    }

    /**
     * @return the $_twitter_display
     */
    public function getTwitter_display() {
        return $this->_twitter_display;
    }

    /**
     * @return the $_googleplus
     */
    public function getGoogleplus() {
        return $this->_googleplus;
    }

    /**
     * @return the $_googleplus_display
     */
    public function getGoogleplus_display() {
        return $this->_googleplus_display;
    }

    /**
     * @return the $_youtube
     */
    public function getYoutube() {
        return $this->_youtube;
    }

    /**
     * @return the $_youtube_display
     */
    public function getYoutube_display() {
        return $this->_youtube_display;
    }

    /**
     * @return the $_flickr
     */
    public function getFlickr() {
        return $this->_flickr;
    }

    /**
     * @return the $_flickr_display
     */
    public function getFlickr_display() {
        return $this->_flickr_display;
    }

    /**
     * @return the $_paypal
     */
    public function getPaypal() {
        return $this->_paypal;
    }

    /**
     * @return the $_paypal_currency
     */
    public function getPaypal_currency() {
        return $this->_paypal_currency;
    }

    /**
     * @return the $_enable_paypal
     */
    public function getEnable_paypal() {
        return $this->_enable_paypal;
    }

    /**
     * @return the $_tocheckout
     */
    public function getTocheckout() {
        return $this->_tocheckout;
    }

    /**
     * @return the $_tocheckout_local
     */
    public function getTocheckout_local() {
        return $this->_tocheckout_local;
    }

    /**
     * @return the $_enable_2co
     */
    public function getEnable_2co() {
        return $this->_enable_2co;
    }

    /**
     * @return the $_moneybookers
     */
    public function getMoneybookers() {
        return $this->_moneybookers;
    }

    /**
     * @return the $_moneybookers_currency
     */
    public function getMoneybookers_currency() {
        return $this->_moneybookers_currency;
    }

    /**
     * @return the $_enable_mb
     */
    public function getEnable_mb() {
        return $this->_enable_mb;
    }

    /**
     * @return the $_storeurl
     */
    public function getStoreurl() {
        return $this->_storeurl;
    }

    /**
     * @return the $_inquiry
     */
    public function getInquiry() {
        return $this->_inquiry;
    }

    /**
     * @return the $_newslettter
     */
    public function getNewslettter() {
        return $this->_newslettter;
    }

    /**
     * @return the $_other_info
     */
    public function getOther_info() {
        return $this->_other_info;
    }

    /**
     * @return the $_active
     */
    public function getActive() {
        return $this->_active;
    }

    /**
     * @param field_type $_id
     */
    public function setId($_id) {
        $this->_id = $_id;
    }

    /**
     * @param field_type $_user_id
     */
    public function setUser_id($_user_id) {
        $this->_user_id = $_user_id;
    }

    /**
     * @param field_type $_facebook
     */
    public function setFacebook($_facebook) {
        $this->_facebook = $_facebook;
    }

    /**
     * @param field_type $_facebook_display
     */
    public function setFacebook_display($_facebook_display) {
        $this->_facebook_display = $_facebook_display;
    }

    /**
     * @param field_type $_twitter
     */
    public function setTwitter($_twitter) {
        $this->_twitter = $_twitter;
    }

    /**
     * @param field_type $_twitter_display
     */
    public function setTwitter_display($_twitter_display) {
        $this->_twitter_display = $_twitter_display;
    }

    /**
     * @param field_type $_googleplus
     */
    public function setGoogleplus($_googleplus) {
        $this->_googleplus = $_googleplus;
    }

    /**
     * @param field_type $_googleplus_display
     */
    public function setGoogleplus_display($_googleplus_display) {
        $this->_googleplus_display = $_googleplus_display;
    }

    /**
     * @param field_type $_youtube
     */
    public function setYoutube($_youtube) {
        $this->_youtube = $_youtube;
    }

    /**
     * @param field_type $_youtube_display
     */
    public function setYoutube_display($_youtube_display) {
        $this->_youtube_display = $_youtube_display;
    }

    /**
     * @param field_type $_flickr
     */
    public function setFlickr($_flickr) {
        $this->_flickr = $_flickr;
    }

    /**
     * @param field_type $_flickr_display
     */
    public function setFlickr_display($_flickr_display) {
        $this->_flickr_display = $_flickr_display;
    }

    /**
     * @param field_type $_paypal
     */
    public function setPaypal($_paypal) {
        $this->_paypal = $_paypal;
    }

    /**
     * @param field_type $_paypal_currency
     */
    public function setPaypal_currency($_paypal_currency) {
        $this->_paypal_currency = $_paypal_currency;
    }

    /**
     * @param field_type $_enable_paypal
     */
    public function setEnable_paypal($_enable_paypal) {
        $this->_enable_paypal = $_enable_paypal;
    }

    /**
     * @param field_type $_tocheckout
     */
    public function setTocheckout($_tocheckout) {
        $this->_tocheckout = $_tocheckout;
    }

    /**
     * @param field_type $_tocheckout_local
     */
    public function setTocheckout_local($_tocheckout_local) {
        $this->_tocheckout_local = $_tocheckout_local;
    }

    /**
     * @param field_type $_enable_2co
     */
    public function setEnable_2co($_enable_2co) {
        $this->_enable_2co = $_enable_2co;
    }

    /**
     * @param field_type $_moneybookers
     */
    public function setMoneybookers($_moneybookers) {
        $this->_moneybookers = $_moneybookers;
    }

    /**
     * @param field_type $_moneybookers_currency
     */
    public function setMoneybookers_currency($_moneybookers_currency) {
        $this->_moneybookers_currency = $_moneybookers_currency;
    }

    /**
     * @param field_type $_enable_mb
     */
    public function setEnable_mb($_enable_mb) {
        $this->_enable_mb = $_enable_mb;
    }

    /**
     * @param field_type $_storeurl
     */
    public function setStoreurl($_storeurl) {
        $this->_storeurl = $_storeurl;
    }

    /**
     * @param field_type $_inquiry
     */
    public function setInquiry($_inquiry) {
        $this->_inquiry = $_inquiry;
    }

    /**
     * @param field_type $_newslettter
     */
    public function setNewslettter($_newslettter) {
        $this->_newslettter = $_newslettter;
    }

    /**
     * @return the $_b2b_images
     */
    public function getB2b_images() {
        return $this->_b2b_images;
    }

    /**
     * @param field_type $_b2b_images
     */
    public function setB2b_images($_b2b_images) {
        $this->_b2b_images = $_b2b_images;
    }

    /**
     * @param field_type $_other_info
     */
    public function setOther_info($_other_info) {
        $this->_other_info = $_other_info;
    }

    /**
     * @param field_type $_active
     */
    public function setActive($_active) {
        $this->_active = $_active;
    }

}
