<?php
class B2b_Model_InquiryMapper
{
    protected $_dbTable;
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('B2b_Model_DbTable_Inquiry');
        }
        return $this->_dbTable;
    }
    public function save(B2b_Model_Inquiry $obj)
    {
        $id = $obj->getId();
        $data = array(
                'sender_id' 		=> 	$obj->getSender_id(),
                'recipient_id' 		=> 	$obj->getRecipient_id(),
                'recipient_title' 	=> 	$obj->getRecipient_title(),
                'trade_type' 		=> 	$obj->getTrade_type(),
                'name' 				=> 	$obj->getName(),
                'company_name' 		=> 	$obj->getCompany_name(),
                'street' 			=> 	$obj->getStreet(),
                'state' 			=> 	$obj->getState(),
                'city' 				=> 	$obj->getCity(),
                'postal_code' 		=> 	$obj->getPostal_code(),
                'country' 		    => 	$obj->getCountry(),
                'authorization' 	=> 	$obj->getAuthorization(),
                'ip' 				=> 	$obj->getIp(),
                'reply_with' 		=> 	$obj->getReply_with(),
                'phone' 			=> 	$obj->getPhone(),
                'phone_part2' 		=> 	$obj->getPhone_part2(),
                'phone_part3' 		=> 	$obj->getPhone_part3(),
                'fax' 				=> 	$obj->getFax(),
                'fax_part2' 		=> 	$obj->getFax_part2(),
                'fax_part3' 		=> 	$obj->getFax_part3(),
                'website' 			=> 	$obj->getWebsite(),
                'email' 			=> 	$obj->getEmail(),
                'return_mail' 		=> 	$obj->getReturn_mail(),
                'subject' 			=> 	$obj->getSubject(),
                'message' 			=> 	$obj->getMessage(),
                'flag' 				=> 	$obj->getFlag(),
                'feedbacks'		    => 	$obj->getFeedbacks(),
                'read' 				=> 	$obj->getRead(),
                'displayble' 		=> 	$obj->getDisplayble(),
                'date' 			    => 	$obj->getAdded_on(),
                'status' 			=> 	$obj->getStatus(),
                'active' 			=> 	$obj->getActive(),
                'parent' 			=> 	$obj->getParent()

        );

        // Start the Update process
        try
        {
            $id =$this->getDbTable()->insert($data);
            $result = array('status' => 'ok' ,'id' => $id);
        }
        catch (Exception $e)
        {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }

        return $result;
    }

    public function fetchAll($pageNumber, $approve = null, $search_params = null, $tableColumns = array('userChecking' => true))
    {		
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns); 		     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }

    public function delete ($id){
        $result = null;
        try {
            $where = array('id = ?' => $id);
            $this->getDbTable()->delete($where);
            $result = array('status' => 'ok' ,'id' => $id);
        } catch (Exception $e) {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }
        return $result;
    }
    public function updateStatus($data , $id){
        $where = array('id = ?' => $id);
        if ((!empty($id) && is_numeric($id)) && !empty($data)  ){
            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array('status' => 'ok' ,'id' => $id);
            return $result;
        }
        else {
            throw new Exception('Invalid Status Selected ' . $id);
        }
    }

    public function read ($id, $read)
    {
        $result = null;
        try
        {
            $data = array('read' => $read);
            $where = array('id = ?' => $id);
            $this->getDbTable()->update($data, $where);
            $result = array('status' => 'ok' ,'id' => $id);
        }
        catch (Exception $e)
        {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }
        return $result;
    }

    public function flag ($id, $flag)
    {
        $result = null;
        try
        {
            $data = array('flag' => $flag);
            $where = array('id = ?' => $id);
            $this->getDbTable()->update($data, $where);
            $result = array('status' => 'ok' ,'id' => $id);
        }
        catch (Exception $e)
        {
            $result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
        }
        return $result;
    }

    public function trash ($id, $trash)
    {
        $result = null;

        try {
            $data = array(
                'active' => $trash
            );

            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }

}