<?php
/**
* This is the DbTable class for the b2b_business_type table.
*/
class B2b_Model_DbTable_BusinessType extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'b2b_business_type';
    protected $_cols = null;
	
	//Get Datas
	public function getBusinessTypeInfo($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row)
		{
           // throw new Exception("Count not find row $id");
		   $options = null;
        }
		else
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ;
    }
	
	//Get Datas
	public function getAllBusinessType()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'business_type'))
                       ->order('business_type ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }
	
	//Get Datas
	public function getBusinessTypeByGroup($group_id = null)
    {
		if($group_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('group_id =?',$group_id)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->order('id ASC');
		}
		$options = $this->fetchAll($select);
        return $options;
    }
		
	//Get Datas
	public function getOptions($group_id = null)
    {
		if($group_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'business_type'))
						   ->where('group_id =?',$group_id)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'business_type'))
						   ->order('id ASC');
		}
		$options = $this->getAdapter()->fetchAll($select);
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($group_id = null)
    {
		if($group_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'business_type'))
						   ->where('group_id =?',$group_id)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'business_type'))
						   ->order('id ASC');
		}
		$options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }
	
	public function isDuplicate($value, $group_id = null, $id = null)
    {
		if(empty($group_id))
		{
			if(empty($id))
			{
				$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => $this->_name,
							'field' => 'business_type'
						)
					);
			}
			else
			{
				$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'business_type',
						'exclude' => array('field' => 'id', 'value' => $id)
					)
				);
			}
		}
		else
		{
			if(empty($id))
			{
				$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' 	=> 		$this->_name,
							'field' 	=> 		'business_type',
							'exclude' 	=> 		'group_id ='. $group_id
						)
					);
			}
			else
			{
				$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' 	=> 		$this->_name,
						'field' 	=> 		'business_type',
						'exclude' 	=>  	'group_id =' . $group_id . ' AND id != ' . $id
					)
				);
			}
		}
		return $validator->isValid($value);
	}
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null, $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;	
		
		$b2b_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_business_type'] && is_array($tableColumns['b2b_business_type'])) ? $tableColumns['b2b_business_type'] : array( 'bbt.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'group_name' => 'bg.group_name', 'file_num_per_page' => 'bg.file_num_per_page', 'file_col_num' => 'bg.file_col_num', 'file_sort' => 'bg.file_sort', 'file_order' => 'bg.file_order', 'meta_title' => 'bg.meta_title', 'meta_keywords' => 'bg.meta_keywords', 'meta_desc' => 'bg.meta_desc', 'role_id' => 'bg.role_id');
        $b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ");
		$b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;
	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('bbt' => $this->_name), $b2b_business_type_arr );
						   
		if (($b2b_group_column_arr &&  is_array($b2b_group_column_arr) && count( $b2b_group_column_arr) > 0)) {
            $select ->joinLeft(array(
                'bg' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'bbt.group_id  = bg.id', $b2b_group_column_arr);
        }
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) 
		{
            $select->joinLeft(array(
                'up' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'up.user_id = bbt.entry_by', $user_profile_column_arr);
        }
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('bbt.entry_by = ?' , $user_id);
		}

        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['field'] == 'group')
					{
						$select->order(' group_name '.$sort_value_arr['dir']);
					}
					else
					{
						if($sort_value_arr['dir'] == 'exp')
						{
							$select->order(new Zend_Db_Expr($sort_value_arr['field']));
						}
						else
						{
							$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
						}
					}
				}
			}
			else
			{
				$select->order("bbt.id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{										
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("bbt.id ASC"); 
		}
		
		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'bbt.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			

				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'group' :
					$operatorFirstPart = " bg.group_name ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(bbt.entry_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(bbt.entry_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(bbt.entry_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}
?>