<?php

class B2b_Model_DbTable_Package extends Eicra_Abstract_DbTable
{

    protected $_name = 'membership_packages';

    public function getPackagesByPid ($product_id, $active = true)
    {
        try {
            $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('product_id =?', $product_id)
                ->order('price ASC');
            if ($active) {
                $select->where('active = ?', '1');
            }
            $options = $this->fetchAll($select)->toArray();
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getPackageById ($id)
    {
        try {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();
                $options = is_array($options) ? array_map('stripslashes',
                        $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getAllPackages ($active = true)
    {
        $select = $this->select()
            ->from($this->_name, array('*'))
            ->order('price ASC');
        if ($active) {
            $select->where('active = ?', '1');
            $select->where('displayable = ?', '1');
        }

        $options = $this->fetchAll($select)->toArray();
        return $options;
    }

    public function getInactiveLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array('*'))
            ->where('user_id =?', $user_id)
            ->where('active = ?', '0')
            ->order('id ASC');
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    public function getIDByUser ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array('id'))
            ->where('user_id =?', $user_id)
            ->order('id ASC');
        $options = $this->getAdapter()->fetchRow($select);
        return $options;
    }

    public function getPackagesPairs ()
    {
        $select = $this->select()
            ->from($this->_name, array('id', 'name'))
            ->where('active = ?', '1')
            ->order('id ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }

    // Get All Datas
    public function getListInfo ($approve = null, $search_params = null,
            $userChecking = true)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

		$b2b_dataLimit = ($search_params && is_array($search_params) && $search_params['dataLimit']) ? $search_params['dataLimit'] : null;
        $hasChild = (is_array($search_params) &&
                 array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('pkg' => $this->_name), array('pkg.*'));

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' &&
                 $userChecking == true) {
            $select->where('pkg.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("pkg.active = ?", $approve);
        }
        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order(
                            $sort_value_arr['field'] . ' ' .
                                     $sort_value_arr['dir']);
                }
            } else {
                $select->order('pkg.package_order ASC');
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
				$search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order('pkg.package_order ASC');
        }

        if ($hasChild === false) {
            $select->where("pkg.parent_id = ?", '0');
        }

		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}

        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
        return $options;
    }

    public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'pkg.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
                                                                       // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                        $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                                 ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                                 $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'id':
                $operatorFirstPart = "pkg.id";
                break;
            case 'name':
                $operatorFirstPart = "pkg.name";
                break;
            case 'price':
                $operatorFirstPart = "pkg.price";
                break;
            case 'selling':
                $operatorFirstPart = "pkg.selling";
                break;
            case 'products':
                $operatorFirstPart = "pkg.products";
                break;
            case 'buying':
                $operatorFirstPart = "pkg.buying ";
                break;
            case 'renewable':
                $operatorFirstPart = "pkg.renewable";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }

        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}