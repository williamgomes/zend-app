<?php


class B2b_Model_DbTable_UserPreferences extends Eicra_Abstract_DbTable
{
	protected $_name    =  'b2b_user_preferences';


	public function getPreferences($user_id)
    {
	      $select = $this->select()

	            ->from($this->_name, array('*'))

	            ->where('user_id =?', $user_id)

	            ->order('id ASC');

	      $row = $this->getAdapter()->fetchRow( $select);

         if (!$row)
         {
             return null;
         }

         else {
         	return   $row ;

         }

     } 
	
}