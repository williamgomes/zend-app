<?php

/**
* This is the DbTable class for the user_profile table.
*/
class B2b_Model_DbTable_MemberList extends Eicra_Abstract_DbTable
{

    /**
     * Table name
     */
    protected $_name = 'user_profile';

    protected $_cols = null;

    public function getListInfo ($approve = null, $search_params = null)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        if ($approve == null) {
            $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('m' => $this->_name),
                    array('m.*',
                            'full_name' => "CONCAT(m.title, ' ', m.firstName, ' ', m.lastName)",
                            '(SELECT COUNT(bbl.id) FROM ' .
                                     Zend_Registry::get('dbPrefix') .
                                     'b2b_buying_leads as bbl WHERE m.user_id = bbl.user_id) AS buying_product_num',

                            '(SELECT COUNT(bsl.id) FROM ' .
                                     Zend_Registry::get('dbPrefix') .
                                     'b2b_selling_leads as bsl WHERE m.user_id = bsl.user_id) AS selling_product_num')

                             )
                ->joinLeft(
                    array(
                            'r' => Zend_Registry::get('dbPrefix') . 'roles'),
                    'm.role_id = r.role_id')
                ->where('r.role_lock = 1 OR r.role_id = ' . $role_id);
            // ->orwhere('r.role_id = ? ', $role_id);
        } else {
            $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('m' => $this->_name),
                    array('m.*',
                            'full_name' => "CONCAT(m.title, ' ', m.firstName, ' ', m.lastName)",
                            '(SELECT COUNT(bbl.id) FROM ' .
                                     Zend_Registry::get('dbPrefix') .
                                     'b2b_buying_leads as bbl WHERE m.user_id = bbl.user_id) AS buying_product_num',

                            '(SELECT COUNT(bsl.id) FROM ' .
                                     Zend_Registry::get('dbPrefix') .
                                     'b2b_selling_leads as bsl WHERE m.user_id = bsl.user_id) AS selling_product_num')

                )
                ->joinLeft(
                    array(
                            'r' => Zend_Registry::get('dbPrefix') . 'roles'),
                    'm.role_id = r.role_id')
                ->where('r.role_lock = 1 OR r.role_id = ' . $role_id)
                ->where('m.status = ? ', $approve);
        }

        $select->joinLeft(
                array('bp' => Zend_Registry::get('dbPrefix') . 'b2b_products'),
                'm.user_id = bp.user_id', array('product_num' => 'COUNT(bp.id)'))->group(
                'm.user_id');
        
        $select->joinLeft(
                array('mp' => Zend_Registry::get('dbPrefix') . 'membership_packages'),
                'm.package_id = mp.id', array('renewable' => 'mp.renewable'));

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order(
                            $sort_value_arr['field'] . ' ' .
                                     $sort_value_arr['dir']);
                }
            } else {
                $select->order('product_num DESC');
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $where_arr[$i] = ' ' .
                                 $this->getOperatorString($filter_obj);
                        $i ++;
                    } else
                        if ($filter_obj['filters']) {
                            $where_sub_arr = array();
                            $sub = 0;
                            foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                                $where_sub_arr[$sub] = ' ' . $this->getOperatorString(
                                        $sub_filter_obj);
                                $sub ++;
                            }
                            $where_arr[$i] = ' (' . implode(
                                    strtoupper($filter_obj['logic']),
                                    $where_sub_arr) . ') ';
                            $i ++;
                        }
                }
                $where = implode(strtoupper($search_params['filter']['logic']),
                        $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order('product_num DESC');
        }
        
        $options = $this->fetchAll($select);

        if (! $options) {
            $options = null;
        }
        return $options;
    }

    private function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'm.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
                                                                       // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                        $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                                 ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                                 $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " .
                         $table_prefix . "firstName, ' ', " . $table_prefix .
                         "lastName) ";
                break;
            case 'product_num':
                $operatorFirstPart = ' ( SELECT COUNT(bps.id) FROM ' .
                         Zend_Registry::get('dbPrefix') .
                         'b2b_products AS bps WHERE bps.user_id = bp.user_id ) ';
                break;
            case 'buying_product_num':
                $operatorFirstPart = ' (SELECT COUNT(bbls.id) FROM ' .
                         Zend_Registry::get('dbPrefix') .
                         'b2b_buying_leads as bbls WHERE m.user_id = bbls.user_id) ';
                break;
            case 'selling_product_num':
                $operatorFirstPart = ' (SELECT COUNT(bbsl.id) FROM ' .
                        Zend_Registry::get('dbPrefix') .
                        'b2b_selling_leads as bbsl WHERE m.user_id = bbsl.user_id) ';
                break;
            case 'last_access':
                $data_arr = preg_split('/[- :]/', $operator_arr['value']);
                if ($data_arr[0]) {
                    $time = strtotime(
                            $data_arr[0] . ' ' . $data_arr[1] . ' ' .
                                     $data_arr[2] . ' ' . $data_arr[3] . ' ' .
                                     $data_arr[4] . ':' . $data_arr[5] . ':' .
                                     $data_arr[6]);
                    $operator_arr['value'] = date("Y-m-d H:i:s", $time);
                }
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    private function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(
                Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}

?>