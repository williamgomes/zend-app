<?php
class B2b_Model_DbTable_ProductGroup extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'b2b_product_group';
    
    public function getGroupInfo($id)
    {
    
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row)
        {
            $options = null;
        }
        else
        {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
            
        }
    
        return   $options ;
    
        
    }
    
    //Get Datas
    
    
    
    
    public function getAllGroups()
    
    {
        $select = $this->select()
        ->from($this->_name, array('*'))
        ->order('id ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        
        return $options;
     
    }
    
    public function getAllGroupsByUserId($user_id)
    {
        $select = $this->select()
        ->from($this->_name, array('*'))
        ->where('user_id =?',$user_id)
        ->order('id ASC');
    
        $options = $this->getAdapter()->fetchAll($select);
    
        return $options;
    }
}
?>