<?php

class B2b_Model_DbTable_Tradeshow extends Eicra_Abstract_DbTable
{

    protected $_name = 'b2b_tradeshow';
    protected $_cols = null;

    public function getExistingLeads ($user_id)
    {
        try {

            $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('user_id =?', $user_id)
                ->order('id ASC');
            $options = $this->getAdapter()->fetchAll($select);

            if ($options) {
                return $options;
            } else {

                return null;
            }
        }

        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getPackageById ($id)
    {
        try {

            $id = (int) $id;

            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();

                $options = is_array($options) ? array_map('stripslashes',
                        $options) : stripslashes($options);

                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    public function getAllLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->
        from($this->_name, array('*'))
            ->
        where('user_id =?', $user_id)
            ->
        order('id ASC');

        $options = $this->getAdapter()->fetchAll($select);

        return $options;
    }

    public function getShowByTitle ($title , $active)
    {
        $select = $this->select()
        ->distinct()
        ->setIntegrityCheck(false)

        ->from(array(
                'sst' => $this->_name
        ), array(
                '*'
        ))
        ->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = sst.user_id', array(
                'status' => 'usr.status',
                'state' => 'usr.state',
                'city' => 'usr.city'
        ))
        ->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'sst.country = tld.id', array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
        ))
        ->joinLeft(array(
                'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pid.id = usr.package_id', array(
                'package_name' => 'pid.name',
                'package_logo' => 'pid.b2b_images',
                'priority_flag' => 'pid.priority_flag'
        ))
        ->joinLeft(array(
                'cyp' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'usr.user_id  = cyp.user_id', array(

                'company_name' => 'cyp.company_name',
                'establishing_year' => 'cyp.establishing_year',
                'keywords' => 'cyp.keywords',
                'main_products' => 'cyp.main_products',
                'company_title' => 'cyp.seo_title',
                'trade_type' => 'cyp.trade_type',
        ))

        ->joinLeft(array(
            'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
        ), 'grp.id  = cyp.group_id', array(
            'images_path' => 'grp.file_path_b2b_images'
        ))

        ->order('id ASC');

        if (! empty($title)) {
            $select->where('sst.seo_title =?', $title);
        }

        if (! empty($active) && is_numeric($active)) {
            $select->where('sst.active = ?', $active);
            $select->where('sst.displayable = ?', $active);
            $select->where('cyp.active = ?', $active);
            $select->where('usr.status = ?', $active);

        }
        $options = $this->fetchAll($select)->toArray();
        return $options;
    }

    public function getAllShows ($approve, $search_params = null, $userChecking = true, $user_id = null)
    {
        $select = $this->select()
        ->distinct()
        ->setIntegrityCheck(false)

            ->from(array(
            'tds' => $this->_name
        ), array(
             'name','title','seo_title','name','address','description','start_date','timepicker','b2b_images',
                    'country','state','city','venue','displayable'
        ))
            ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = tds.user_id', array(
            'status' => 'usr.status',
            'usr_state' => 'usr.state',
        	'usr_city' => 'usr.city',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
            ->joinLeft(array(
            'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'tds.country = tld.id', array(
            'country_name' => 'tld.value',
            'country_code' => 'tld.code'
        ))

        ->joinLeft(array(
        		'usrtld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = usrtld.id', array(
        		'user_country_name' => 'usrtld.value',
        		'user_country_code' => 'usrtld.code'
        ))
            ->joinLeft(array(
            'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pid.id = usr.package_id', array(
            'package_name' => 'pid.name',
            'package_logo' => 'pid.b2b_images',
            'is_feedback' => 'pid.feedback',
            'is_voting' => 'pid.voting',
            'is_profile' => 'pid.profile',
            'is_telephone' => 'pid.telephone',
            'is_website' => 'pid.website',
            'priority' => 'pid.priority'
        ))
            ->joinLeft(array(
            'cyp' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'usr.user_id  = cyp.user_id', array(
            'company_name' => 'cyp.company_name',
            'establishing_year' => 'cyp.establishing_year',
            'company_title' => 'cyp.seo_title',
            'trade_type' => 'cyp.trade_type',
        ))
            ->joinLeft(array(
                    'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'grp.id  = cyp.group_id', array(
                    'images_path' => 'grp.file_path_b2b_images'
            ))
            ->order('tds.added_on ASC');

        if (! empty($user_id) && is_numeric($user_id)) {
            $select->where('tds.user_id =?', $user_id);
        }

        if (! empty($approve) && is_numeric($approve)) {
            $select->where('tds.active = ?', $approve);
            $select->where('cyp.active = ?', $approve);
            $select->where('usr.status = ?', $approve);
            $select->where('tds.displayable = ?', $approve);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i ++;
                    } else
                    if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                            $sub ++;
                        }
                        $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                        $i ++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        }
        $options = $this->fetchAll($select)->toArray();
        return $options;
    }

    public function getListInfo ($approve = null, $search_params = null, $tableColumns= null)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;

		$b2b_tradeshow_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_tradeshow'] && is_array($tableColumns['b2b_tradeshow'])) ? $tableColumns['b2b_tradeshow'] : array('ts.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)" );
        $tradeshow_countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['tradeshow_countries'] && is_array($tableColumns['tradeshow_countries'])) ? $tableColumns['tradeshow_countries'] : null;
        $user_countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_countries'] && is_array($tableColumns['user_countries'])) ? $tableColumns['user_countries'] : null;
        $membership_packages_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['membership_packages'] && is_array($tableColumns['membership_packages'])) ? $tableColumns['membership_packages'] : null;
		$company_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_company_profile'] && is_array($tableColumns['b2b_company_profile'])) ? $tableColumns['b2b_company_profile'] : null;
        $b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : null;
		$b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$setUniqueResult	=	($tableColumns && is_array($tableColumns) && is_bool($tableColumns['setUniqueResult'])) ? $tableColumns['setUniqueResult'] : false;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

        $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('ts' => $this->_name), $b2b_tradeshow_column_arr);

		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = ts.user_id', $user_profile_column_arr);
        }

        if (($tradeshow_countries_column_arr &&  is_array($tradeshow_countries_column_arr) && count($tradeshow_countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'ts.country = tld.id', $tradeshow_countries_column_arr );
        }

        if (($user_countries_column_arr &&  is_array($user_countries_column_arr) && count($user_countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'usrtld' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'usr.country = usrtld.id', $user_countries_column_arr );
        }

        if (($membership_packages_column_arr &&  is_array($membership_packages_column_arr) && count($membership_packages_column_arr) > 0)) {
            $select->joinLeft(array(

                'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
            ), 'pid.id = usr.package_id', $membership_packages_column_arr) ;
        }

        if (($company_profile_column_arr &&  is_array($company_profile_column_arr) && count($company_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'cyp' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
            ), 'usr.user_id  = cyp.user_id', $company_profile_column_arr) ;
        }

        if (($b2b_group_column_arr &&  is_array($b2b_group_column_arr) && count( $b2b_group_column_arr) > 0)) {
            $select ->joinLeft(array(
                'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'cyp.group_id  = grp.id', $b2b_group_column_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('ts.user_id = ?', $user_id);
        }

        if ($approve != null) {
            $select->where("ts.active = ?", $approve);
        }

        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order(
                            $sort_value_arr['field'] . ' ' .
                            $sort_value_arr['dir']);
                }
            } else {
                $select->order("ts.id ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("ts.id ASC");
        }
		
		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}
		
        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
        return $options;
    }

    public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ts.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
            // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                        $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                        ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                        $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(usr.title, ' ', usr.firstName, ' ',usr.lastName) ";
                break;
			case 'package_name':
					$operatorFirstPart = " pid.name ";
				break;			
			case 'user_country_id':
					$operatorFirstPart = " usr.country ";
				break;
			case 'user_country_name':
				    $operatorFirstPart = " usrtld.value ";
				    break;
			case 'tradeshow_country_name':
				    $operatorFirstPart = " tld.value ";
				    break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                        $operator_arr['value'] . '%" ';
                        break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                        $operator_arr['value'] . '%" ';
                        break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                        $operator_arr['value'] . '%" ';
                        break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                        $operator_arr['value'] . '" ';
                        break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                        $operator_arr['value'] . '" ';
                        break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                        $operator_arr['value'] . '" ';
                        break;
        }
        return $operatorString;
    }

    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}