<?php
/**
* This is the DbTable class for the b2b_group table.
*/
class B2b_Model_DbTable_Group extends Eicra_Abstract_DbTable{
    /** Table name */
    protected $_name    =  'b2b_group';
    protected $_cols = null;
	
	//Get Datas
	public function getAllGroupInfo()
    {
       $select = $this->select()
                       ->from($this->_name, array('*'))
                       ->order('id ASC');
		$options = $this->fetchAll($select)->toArray();
		if (!$options)
		{
           // throw new Exception("Count not find rows $id");
		   $options = null;
        }
        return $options;
    }
	//Get Datas
	public function getGroupInfo()
    {
       $select = $this->select()
                       ->from($this->_name, array('id', 'group_name'))
                       ->where('active =?','1')
                       ->order('id ASC');

		 $options = $this->getAdapter()->fetchPairs($select);
		if (!$options)
		{
			return null;
            //throw new Exception("Count not find rows $id");
        }
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        return $options;
    }

	//Get Datas
	public function getGroupName($group_id)
    {
        $row = $this->fetchRow('id = ' . $group_id);
        if (!$row)
		{
           // throw new Exception("Count not find row $group_id");
		   $group_name = null;
        }
		else
		{
			$group_name = $row->toArray();
			$group_name = is_array($group_name) ? array_map('stripslashes', $group_name) : stripslashes($group_name);
		}
        return $group_name;
    }

    public function getDefaultGroupId()
    {
            $select = $this->select()

        ->from($this->_name, array('id', 'group_name'))

        ->where('active =?','1')

        ->order('id ASC');
        $row = $this->fetchRow( $select );
        return $row->toArray();

    }

    public function getAllGroups()
    {
    	$select = $this->select()
    	->from($this->_name, array('id', 'group_name'))
    	->where('active =?','1')
    	->order('id ASC');

    	$options = $this->fetchAll($select)->toArray();
		if (!$options)
		{
           // throw new Exception("Count not find rows $id");
		   $options = null;
        }
        return $options;
    	if (!$options)
    	{
    		return null;
    		//throw new Exception("Count not find rows $id");
    	}
    	$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
    	return $options;
    }
	
	public function getListInfo ($approve = null, $search_params = null, $tableColumns = null)
    {

        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : array('grp.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : null;
        $b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

        $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array(
            'grp' => $this->_name
        ), $b2b_group_column_arr) ;


        if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = grp.entry_by', $user_profile_column_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking  === true) {
            $select->where('grp.entry_by = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("grp.active = ?", $approve);
        }
      

        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("grp.id ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("grp.id ASC");
        }
		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}

        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
		
        return $options;

    }
	
	public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'grp.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName) ";
                break;
           /* case 'product_group_name':
                $operatorFirstPart = " pgp.name ";
                break;
			case 'package_name':
					$operatorFirstPart = " pid.name ";
				break;
			case 'category_name':
				    $operatorFirstPart = " cat.name ";
				    break;
			case 'package_id':
					$operatorFirstPart = " usr.package_id ";
				break;
			case 'user_country_id':
					$operatorFirstPart = " usr.country ";
			break;
			case 'advtype_input':
					$operatorFirstPart = " typ.id ";
				break;
			case 'short_description':
			    $operatorFirstPart = " grp.short_description ";
			    break;

		    case 'detail_description':
		        $operatorFirstPart = " grp.detail_description ";
		        break;

			case 'user_country_name':
				    $operatorFirstPart = " tld.value ";
				    break;
			case 'category_displayble':
                $operatorFirstPart = " ( grp.category_id = '0' OR cat.displayble = '".$operator_arr['value']."'  ) AND 1 ";
				$operator_arr['value'] = "1";;
                break;
			case 'category_active':
                $operatorFirstPart = " cat.active ";
                break;
			case 'category_isShowProfile':
                $operatorFirstPart = " cat.isShowProfile ";
                break;
			case 'cpy_active':
				    $operatorFirstPart = " cpy.active ";
				    break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE grp.id  = vts.table_id) AS total_votes ";
                break;*/
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(inv.invoice_create_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(inv.invoice_create_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(inv.invoice_create_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
			case 'hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'deal_hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' <= "' . $operator_arr['value'] . '" AND TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' > "0" ';
                break;
			case 'deal_day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' <= "' . $operator_arr['value'] . '" AND DATEDIFF(' . $operator_arr['field'] . ', now())' . ' > "0" ';
                break;
			case 'deal_month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' > "0" ';
                break;
			case 'deal_year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' > "0" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}
?>