<?php
class B2b_Model_DbTable_Buying extends Eicra_Abstract_DbTable
{
    protected $_name = 'b2b_buying_leads';
    protected $_cols = null;

    public function getExistingLeads ($user_id = null, $order_field = 'name', $order_by = 'ASC')
    {
        try {

            $select = $this->select()
                ->
            from($this->_name, array(
                '*'
            ));
			
			if(!empty($user_id))
			{
                $select-> where('user_id =?', $user_id);
			}
            $select->order($order_field.' '.$order_by);

            $options = $this->getAdapter()->fetchAll($select);

            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {

            return null;
        }
    }
	
	public function getNumInActiveBuyingLeads($group_id = null) 
    {
		if(empty($group_id))
		{
			$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.active =?','0'); 
		}
		else
		{
       		$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.group_id =?',$group_id)
					   ->where('gp.active =?','0'); 
		}		

        $options = $this->fetchAll($select);		

        if (!$options) 
		{
            $num_of_item = 0;
        }
		else
		{
			foreach($options as $row)
			{
				$num_of_item = $row['num'];
			}
		}
        return $num_of_item; 
    }
	
    public function getLeadById ($id)
    {
        try {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();
                $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    public function getAllLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            '*'
        ))
            ->where('user_id =?', $user_id)
            ->order('id ASC');
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }
    public function getInactiveLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            '*'
        ))
            ->where('user_id =?', $user_id)
            ->where('active = ?', '0')
            ->order('id ASC');
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }



    // Get All Datas for a user.
    public function getNumOfLeads ($user_id)
    {
        if ($user_id) {
            $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
                    'buy' => $this->_name
            ), array(
                    'current_leads_num' => 'COUNT(buy.id)'
            ))
            ->joinLeft(array(
                    'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = buy.user_id', array(
                    'package_id' => 'usr.package_id',
                    'status' => 'usr.status',
                    'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
            ))
            ->joinLeft(array(
                    'pkg' => Zend_Registry::get('dbPrefix') . 'membership_packages'
            ), 'pkg.id = usr.package_id', array(
                    'package_name' => 'pkg.name',
                    'max_lead' => 'pkg.buying',
                    'active' => 'pkg.preapproval'
            ))

            ->joinLeft(array(
                    'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
            ), 'buy.user_id  = cpy.user_id', array(
                    'company_name' => 'cpy.company_name',
                    'company_status' => 'cpy.active',
                    'company_id' => 'cpy.id'
            ))

            ->where('buy.user_id = ?', $user_id);
            $options = $this->fetchRow($select)->toArray();
            if (count($options) > 0 ) {
                return $options;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }


    public function getDraftLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            '*'
        ))
            ->where('user_id =?', $user_id)
            ->where('displayble = ?', '0')
            ->order('id ASC');
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }
    public function getListInfo ($approve = null, $search_params = null, $tableColumns= null)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

		$b2b_buying_leads_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_buying_leads'] && is_array($tableColumns['b2b_buying_leads'])) ? $tableColumns['b2b_buying_leads'] : array('buy.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'usr.username', 'firstName' => 'usr.firstName', 'lastName' => 'usr.lastName', 'title' => 'usr.title', 'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)" );
        $company_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_company_profile'] && is_array($tableColumns['b2b_company_profile'])) ? $tableColumns['b2b_company_profile'] : null;
        $user_countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : null;
        $user_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_category'] && is_array($tableColumns['b2b_category'])) ? $tableColumns['b2b_category'] : null;
        $b2b_biztype_mapper_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_biztype_mapper'] && is_array($tableColumns['b2b_biztype_mapper'])) ? $tableColumns['b2b_biztype_mapper'] : null;
        $b2b_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_business_type'] && is_array($tableColumns['b2b_business_type'])) ? $tableColumns['b2b_business_type'] : null;
        $user_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_category'] && is_array($tableColumns['b2b_category'])) ? $tableColumns['b2b_category'] : null;
        $membership_packages_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['membership_packages'] && is_array($tableColumns['membership_packages'])) ? $tableColumns['membership_packages'] : null;
		$vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
		$b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : null;
		//$b2b_product_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_product_group'] && is_array($tableColumns['b2b_product_group'])) ? $tableColumns['b2b_product_group'] : array( 'product_group_name' => 'pgp.name' );
		$b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$setUniqueResult	=	($tableColumns && is_array($tableColumns) && is_bool($tableColumns['setUniqueResult'])) ? $tableColumns['setUniqueResult'] : false;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

		$select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array(
            'buy' => $this->_name
        ), $b2b_buying_leads_column_arr) ;


        if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = buy.user_id', $user_profile_column_arr);
        }

        if (($user_countries_column_arr &&  is_array($user_countries_column_arr) && count($user_countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'usr.country = tld.id', $user_countries_column_arr );
        }

        if (($company_profile_column_arr &&  is_array($company_profile_column_arr) && count($company_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
            ), 'buy.user_id  = cpy.user_id', $company_profile_column_arr) ;
        }

        if (($membership_packages_column_arr &&  is_array($membership_packages_column_arr) && count($membership_packages_column_arr) > 0)) {
            $select->joinLeft(array(

                'pkg' => Zend_Registry::get('dbPrefix') . 'membership_packages'
            ), 'pkg.id = usr.package_id', $membership_packages_column_arr) ;
        }

        if (($user_category_column_arr &&  is_array($user_category_column_arr) && count( $user_category_column_arr) > 0)) {
            $select ->joinLeft(array(
                'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
            ), 'buy.category  = cat.id', $user_category_column_arr);
        }

        if (($b2b_biztype_mapper_arr &&  is_array($b2b_biztype_mapper_arr) && count( $b2b_biztype_mapper_arr) > 0)) {
            $select->joinLeft(array(
                    'biz' => Zend_Registry::get('dbPrefix') . 'b2b_biztype_mapper'
            ), 'usr.user_id  = biz.user_id', $b2b_biztype_mapper_arr);
        }

        if (($b2b_business_type_arr &&  is_array($b2b_business_type_arr) && count( $b2b_business_type_arr) > 0)) {
            $select->joinLeft(array(
            'typ' => Zend_Registry::get('dbPrefix') . 'b2b_business_type'
            ), 'biz.biztype_id  = typ.id', $b2b_business_type_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking  === true) {
            $select->where('buy.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("buy.active = ?", $approve);
        }

        if (($b2b_group_column_arr &&  is_array($b2b_group_column_arr) && count( $b2b_group_column_arr) > 0)) {
            $select ->joinLeft(array(
                'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'buy.group_id  = grp.id', $b2b_group_column_arr);
        }

		if (($vote_column_arr &&  is_array($vote_column_arr) && count( $vote_column_arr) > 0)) {
            $select ->joinLeft(array(
                'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting'
            ), 'buy.id  = vt.table_id', $vote_column_arr);
        }

        if (($where &&  is_array($where) && count( $where) > 0)) {

            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("buy.buying_order ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {

                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params); //implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("buy.buying_order ASC");
        }

		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}

        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
		if($setUniqueResult === true)
		{
			if($options)
			{
			    $options = $options->toArray();
				$this->setUniqueResult($options, 'id', array(
						'business_type'
					), true);
			}
		}
        return $options;
    }

    public function getOfferById ($approve, $id)
    {
    	$select = $this->select()
    	->distinct()
    	->setIntegrityCheck(false)
    	->from(array(
    			'buy' => $this->_name
    	), array(
    			'buy.*'
    	))
    	->joinLeft(array(
    			'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
    	), 'usr.user_id = buy.user_id', array(
    			'status' => 'usr.status',
    			'state' => 'usr.state',
    			'city' => 'usr.city',
    			'user_country_id' => 'usr.country'
    	))
    	->joinLeft(array(
    			'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
    	), 'buy.category  = cat.id', array(
    			'category_name' => 'cat.name',
    	))
    	->joinLeft(array(
    			'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
    	), 'buy.user_id  = cpy.user_id', array(
    			'company_name' => 'cpy.company_name',
    			'buying_categories' => 'cpy.buying_categories',
    			'registered_in' => 'cpy.registered_in',
    			'logo' => 'cpy.profile_image_primary',
    			'company_title' => 'cpy.seo_title'
    	))
    	->joinLeft(array(
    			'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
    	), 'grp.id  = 1', array(
    			'group_id' => 'grp.id',
    			'default_img_path' => 'grp.file_path_b2b_images',
    			'images_path' => 'grp.file_path_profile_image',
    			'selling_img_path' => 'grp.file_path_selling_images',
    			'buying_img_path' => 'grp.file_path_buying_images',
    			'product_img_path' => 'grp.file_path_product_images',
    			'img_big_width' => 'grp.img_big_width',
    			'img_big_resize_func' => 'grp.img_big_resize_func',
    			'img_big_height' => 'grp.img_big_height',
    			'file_thumb_height' => 'grp.file_thumb_height',
    			'review_id' => 'grp.review_id',
    			'file_thumb_width' => 'grp.file_thumb_width'


    	)) ;

    	if ($approve) {
    		$select->where("buy.active = ?", $approve);
    		$select->where("buy.displayble = ?", $approve);
    	}
    	if ($id){
    		$select->where("buy.id = ?", $id);
    	}
    	else {
    		return null;
    	}
    	$options = $this->fetchAll($select)->toArray();

    	if (! $options || ! count($options) > 1) {
    		$options = null;
    	}
    	else {
    		return $options;
    	}
    }

    public function getOfferDetails ($approve = null, $title = null)
    {
        $select = $this->select()
        ->distinct()
        ->setIntegrityCheck(false)
        ->from(array(
            'buy' => $this->_name
        ), array(
            'buy.*'
        ))
        ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = buy.user_id', array(
            'status' => 'usr.status',
            'state' => 'usr.state',
            'city' => 'usr.city',
            'user_country_id' => 'usr.country'
        ))
        ->joinLeft(array(
            'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
        ), 'buy.category  = cat.id', array(
            'category_name' => 'cat.name',
        ))
        ->joinLeft(array(
            'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'buy.user_id  = cpy.user_id', array(
            'company_name' => 'cpy.company_name',
            'buying_categories' => 'cpy.buying_categories',
            'registered_in' => 'cpy.registered_in',
            'logo' => 'cpy.profile_image_primary',
            'company_title' => 'cpy.seo_title'
        ));

        if ($approve) {
            $select->where("usr.status = ?", $approve);
            $select->where("buy.active = ?", $approve);
            $select->where("buy.displayble = ?", $approve);
        }
        if ($title){
            $select->where("buy.seo_title = ?", $title);
        }
        else {
            return null;
        }
        $options = $this->fetchAll($select)->toArray();
        if (! $options || ! count($options) > 1) {
            $options = null;
        }
        else {
            return $options;
        }
    }

    private function setUniqueResult (&$multi_array, $uniq_field_name, $extra_field_array, $just = false)
    {
        if ($multi_array != null) {
            $uniq_array = array();
            foreach ($multi_array as $key => $array_value) {
                $uniq_array[$key] = $array_value[$uniq_field_name];
            }
            $uniq_array = array_unique($uniq_array);
            $uniq_result = array();
            $uniq_array_key = 0;
            foreach ($uniq_array as $key => $uniq_id) {
                $uniq_result[$uniq_array_key] = $this->getExtraField_Array($multi_array, $uniq_id, $uniq_field_name, $extra_field_array);
                $uniq_array_key ++;
            }
            if($just === true){ $this->setJustUnique($uniq_result, $extra_field_array); }
            $multi_array = $uniq_result;
        }
    }
    private function getExtraField_Array (&$multi_array, $uniq_id, $uniq_field_name, $extra_field_array)
    {
        $result_array = array();
        $result_key = 0;
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                $result_array = $array_value;
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    $result_array[$extra_field_value] = array();
                }
                break;
            }
        }
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    if ($array_value[$extra_field_value]) {
                        $result_array[$extra_field_value][$result_key] = stripslashes( $array_value[$extra_field_value]);
                    }
                }
                $result_key ++;
            }
        }
        return $result_array;
    }

    public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'buy.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'product_group_name':
                $operatorFirstPart = " pgp.name ";
                break;
			case 'package_name':
					$operatorFirstPart = " pkg.name ";
				break;
			case 'advtype_input':
					$operatorFirstPart = " typ.id ";
				break;
			case 'package_id':
					$operatorFirstPart = " usr.package_id ";
				break;
			case 'user_country_id':
					$operatorFirstPart = " usr.country ";
				break;
			case 'user_country_name':
				    $operatorFirstPart = " tld.value ";
				    break;
			case 'category_name':
				    $operatorFirstPart = " cat.name ";
				    break;
			case 'buying_id_in':
				$buying_id_in_arr = explode(',', $operator_arr['value']);
				$buying_id_in_arr = array_filter( $buying_id_in_arr );
				foreach($buying_id_in_arr as $key => $value)
				{
					$buying_id_in_arr[$key] = '"'.$value.'"';
				}
				$buying_id_in_string = implode(',', $buying_id_in_arr);
                $operatorFirstPart = " buy.id IN( ".$buying_id_in_string."  ) AND 1 ";
				$operator_arr['value'] = "1";
                break;
			case 'category_displayble':
                $operatorFirstPart = " ( buy.category = '0' OR cat.displayble = '".$operator_arr['value']."'  ) AND 1 ";
				$operator_arr['value'] = "1";;
                break;
			case 'category_active':
                $operatorFirstPart = " cat.active ";
                break;
			case 'category_isShowProfile':
                $operatorFirstPart = " cat.isShowProfile ";
                break;
            case 'cpy_active':
                $operatorFirstPart = " cpy.active ";
                break;
			case 'cpy_active':
				    $operatorFirstPart = " cpy.active ";
				    break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(buy.invoice_create_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(buy.invoice_create_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(buy.invoice_create_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
			case 'hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' >= "' . $operator_arr['value'] . '" ';
                break;
			case 'deal_hour':
                $operatorString = 'TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' <= "' . $operator_arr['value'] . '" AND TIMESTAMPDIFF(HOUR,now(),' . $operator_arr['field'] . ')' . ' > "0" ';
                break;
			case 'deal_day':
                $operatorString = 'DATEDIFF(' . $operator_arr['field'] . ', now())' . ' <= "' . $operator_arr['value'] . '" AND DATEDIFF(' . $operator_arr['field'] . ', now())' . ' > "0" ';
                break;
			case 'deal_month':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y%m"), date_format(now(), "%Y%m"))' . ' > "0" ';
                break;
			case 'deal_year':
                $operatorString = 'period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' <= "' . $operator_arr['value'] . '" AND period_diff(date_format(' . $operator_arr['field'] . ', "%Y"), date_format(now(), "%Y"))' . ' > "0" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }
    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

    private function setJustUnique(&$multi_array, $extra_field_array)
    {
        foreach ($multi_array as $key => $value_arr)
        {
            foreach ($value_arr as $value_arr_key => $value)
            {
                if(in_array($value_arr_key, $extra_field_array))
                {
                    $multi_array[$key][$value_arr_key] = array_unique ($multi_array[$key][$value_arr_key]);
                }
            }
        }
    }
}
