<?php

class B2b_Model_DbTable_SuccessStories extends Eicra_Abstract_DbTable
{

    protected $_name = 'b2b_success_stories';

    public function getExistingLeads ($user_id)
    {
        try {
            $select = $this->select()
                ->from($this->_name, array(
                '*'
            ))
                ->where('user_id =?', $user_id)
                ->order('id ASC');
            $options = $this->getAdapter()->fetchAll($select);

            if ($options) {
                return $options;
            } else {

                return null;
            }
        }

        catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    public function getStorybyuser ($user_id)
    {
        try {
            $select = $this->select()
            ->from($this->_name, array(
                    '*'
            ))
            ->where('user_id =?', $user_id)
            ->order('id ASC');
            $options = $this->getAdapter()->fetchRow($select);

            if ($options) {
                return $options;
            } else {

                return null;
            }
        }

        catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }


    public function getPackageById ($id)
    {
        try {

            $id = (int) $id;

            $row = $this->fetchRow('id = ' . $id);

            if ($row) {
                $options = $row->toArray();

                $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);

                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    public function getAllLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            '*'
        ))
            ->where('user_id =?', $user_id)
            ->order('id ASC');

        $options = $this->getAdapter()->fetchAll($select);

        return $options;
    }

    public function getInactiveLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            'id',
            'title',
            'seo_title',
            'name',
            'description',
            'image',
            'user_id'
        ))
            ->where('user_id =?', $user_id)
            ->where('active = ?', '0')
            ->order('id ASC');

        $options = $this->getAdapter()->fetchAll($select);

        return $options;
    }

    public function getAllStories ($approve = null, $search_params = null, $userChecking = true, $user_id = null) // ($user_id, $active)
    {
        $select = $this->select()
            ->distinct()
            ->setIntegrityCheck(false)
            ->from(array(
            'sst' => $this->_name
        ), array(
            'id',
            'title',
            'seo_title',
            'name',
            'description',
            'b2b_images',
            'user_id',
            'displayable'
        ))
            ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = sst.user_id', array(
            'status' => 'usr.status',
            'state' => 'usr.state',
            'city' => 'usr.city',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
            ->joinLeft(array(
            'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
            'country_name' => 'tld.value',
            'country_code' => 'tld.code'
        ))
            ->joinLeft(array(
            'pkg' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pkg.id = usr.package_id', array(
            'package_name' => 'pkg.name',
            'package_logo' => 'pkg.b2b_images',
            'is_feedback' => 'pkg.feedback',
            'is_profile' => 'pkg.profile',
            'priority_flag' => 'pkg.priority_flag'
        ))
            ->joinLeft(array(
            'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'usr.user_id  = cpy.user_id', array(

            'company_name' => 'cpy.company_name',
            'establishing_year' => 'cpy.establishing_year',
            'keywords' => 'cpy.keywords',
            'main_products' => 'cpy.main_products',
            'company_title' => 'cpy.seo_title',
            'trade_type' => 'cpy.trade_type'
        ))
            ->joinLeft(array(
            'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
        ), 'grp.id  = cpy.group_id', array(
            'images_path' => 'grp.file_path_b2b_images'
        ))
            ->order('id ASC');

        if (! empty($user_id) && is_numeric($user_id)) {
            $select->where('sst.user_id =?', $user_id);
        }

        if (! empty($approve) && is_numeric($approve)) {
            $select->where('sst.active = ?', $approve);
            $select->where('cpy.active = ?', $approve);
            $select->where('usr.status = ?', $approve);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i ++;
                    } else
                        if ($filter_obj['filters']) {
                            $where_sub_arr = array();
                            $sub = 0;
                            foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                                $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                                $sub ++;
                            }
                            $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                            $i ++;
                        }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        }

        $options = $this->fetchAll($select)->toArray();
        return $options;
    }

    public function getStoryByTitle ($text, $active)
    {
        $select = $this->select()
            ->distinct()
            ->setIntegrityCheck(false)
            ->from(array(
            'sst' => $this->_name
        ), array(
            'id',
            'title',
            'seo_title',
            'name',
            'description',
            'b2b_images',
            'user_id',
            'displayable'
        ))
            ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = sst.user_id', array(
            'status' => 'usr.status',
            'state' => 'usr.state',
            'city' => 'usr.city'
        ))
            ->joinLeft(array(
            'ct' => Zend_Registry::get('dbPrefix') . 'cities'
        ), 'usr.city = ct.city_id', array(
            'city_name' => 'ct.city'
        ))
            ->joinLeft(array(
            'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
            'country_name' => 'tld.value',
            'code' => 'tld.code'
        ))
            ->joinLeft(array(
            'pkg' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pkg.id = usr.package_id', array(
            'package_name' => 'pkg.name',
            'package_logo' => 'pkg.b2b_images',
            'priority_flag' => 'pkg.priority_flag'
        ))
            ->joinLeft(array(
            'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'usr.user_id  = cpy.user_id', array(

            'company_name' => 'cpy.company_name',
            'establishing_year' => 'cpy.establishing_year',
            'keywords' => 'cpy.keywords',
            'main_products' => 'cpy.main_products',
            'company_title' => 'cpy.seo_title',
            'trade_type' => 'cpy.trade_type'
        ))
            ->joinLeft(array(
            'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
        ), 'cpy.category_id  = cat.id', array(
            'category_name' => 'cat.name'
        ))
            ->joinLeft(array(
            'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
        ), 'grp.id  = cpy.group_id', array(
            'images_path' => 'grp.file_path_b2b_images'
        ))
            ->order('id ASC');

        if (! empty($text)) {
            $select->where('sst.seo_title =?', $text);
        }

        if (! empty($active) && is_numeric($active)) {
            $select->where('sst.active = ?', $active);
            $select->where('cpy.active = ?', $active);
            $select->where('usr.status = ?', $active);
        }
        $options = $this->fetchAll($select)->toArray();
        return $options;
    }

    // Get All Datas
    public function getListInfo ($approve = null, $search_params = null, $tableColumns = null)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;

        $b2b_success_stories_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_success_stories'] && is_array($tableColumns['b2b_success_stories'])) ? $tableColumns['b2b_success_stories'] : array(
            'sst.*'
        );
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array(
            'username' => 'usr.username',
            'firstName' => 'usr.firstName',
            'lastName' => 'usr.lastName',
            'title' => 'usr.title',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        );
        $company_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_company_profile'] && is_array($tableColumns['b2b_company_profile'])) ? $tableColumns['b2b_company_profile'] : array(

            'company_name' => 'cpy.company_name',
            'establishing_year' => 'cpy.establishing_year',
            'keywords' => 'cpy.keywords',
            'main_products' => 'cpy.main_products',
            'company_title' => 'cpy.seo_title',
            'trade_type' => 'cpy.trade_type'
        );
        $user_countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array(
            'country_name' => 'tld.value',
            'country_code' => 'tld.code'
        );
        $user_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_category'] && is_array($tableColumns['b2b_category'])) ? $tableColumns['b2b_category'] : null;
        $b2b_biztype_mapper_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_biztype_mapper'] && is_array($tableColumns['b2b_biztype_mapper'])) ? $tableColumns['b2b_biztype_mapper'] : null;
        $b2b_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_business_type'] && is_array($tableColumns['b2b_business_type'])) ? $tableColumns['b2b_business_type'] : null;
        $user_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_category'] && is_array($tableColumns['b2b_category'])) ? $tableColumns['b2b_category'] : null;
        $membership_packages_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['membership_packages'] && is_array($tableColumns['membership_packages'])) ? $tableColumns['membership_packages'] : array(
            'package_name' => 'pkg.name',
            'package_logo' => 'pkg.b2b_images',
            'priority_flag' => 'pkg.priority_flag'
        );
        $vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
        $b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : array(
            'images_path' => 'grp.file_path_b2b_images'
        );
        $b2b_product_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_product_group'] && is_array($tableColumns['b2b_product_group'])) ? $tableColumns['b2b_product_group'] : array(
            'product_group_name' => 'pgp.name'
        );
        $b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
        $userChecking = ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
        $setUniqueResult = ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['setUniqueResult'])) ? $tableColumns['setUniqueResult'] : false;
        $where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

        /*
         * $select = $this->select() ->setIntegrityCheck(false) ->from(array( 'sst' => $this->_name ), array( 'sst.*' )) ->joinLeft(array( 'up' => Zend_Registry::get('dbPrefix') . 'user_profile' ), 'sst.user_id = up.user_id', array( 'username' => 'up.username', 'full_name' => "CONCAT(up.title, ' ', up.firstName, ' ', up.lastName)" )); if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) { $select->where('sst.user_id = ?', $user_id); } if ($approve != null) { $select->where("sst.active = ?", $approve); }
         */

        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'sst' => $this->_name
        ), $b2b_success_stories_column_arr);

        if (($user_profile_column_arr && is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = sst.user_id', $user_profile_column_arr);
        }

        if (($user_countries_column_arr && is_array($user_countries_column_arr) && count($user_countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'usr.country = tld.id', $user_countries_column_arr);
        }

        if (($company_profile_column_arr && is_array($company_profile_column_arr) && count($company_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
            ), 'sst.user_id  = cpy.user_id', $company_profile_column_arr);
        }

        if (($membership_packages_column_arr && is_array($membership_packages_column_arr) && count($membership_packages_column_arr) > 0)) {
            $select->joinLeft(array(

                'pkg' => Zend_Registry::get('dbPrefix') . 'membership_packages'
            ), 'pkg.id = usr.package_id', $membership_packages_column_arr);
        }

        if (($user_category_column_arr && is_array($user_category_column_arr) && count($user_category_column_arr) > 0)) {
            $select->joinLeft(array(
                'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
            ), 'sst.category  = cat.id', $user_category_column_arr);
        }

        if (($b2b_biztype_mapper_arr && is_array($b2b_biztype_mapper_arr) && count($b2b_biztype_mapper_arr) > 0)) {
            $select->joinLeft(array(
                'biz' => Zend_Registry::get('dbPrefix') . 'b2b_biztype_mapper'
            ), 'usr.user_id  = biz.user_id', $b2b_biztype_mapper_arr);
        }

        if (($b2b_business_type_arr && is_array($b2b_business_type_arr) && count($b2b_business_type_arr) > 0)) {
            $select->joinLeft(array(
                'typ' => Zend_Registry::get('dbPrefix') . 'b2b_business_type'
            ), 'biz.biztype_id  = typ.id', $b2b_business_type_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking === true) {
            $select->where('sst.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("sst.active = ?", $approve);
        }

        if (($b2b_group_column_arr && is_array($b2b_group_column_arr) && count($b2b_group_column_arr) > 0)) {
            $select->joinLeft(array(
                'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'cpy.group_id  = grp.id', $b2b_group_column_arr);
        }

        if (($vote_column_arr && is_array($vote_column_arr) && count($vote_column_arr) > 0)) {
            $select->joinLeft(array(
                'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting'
            ), 'sst.id  = vt.table_id', $vote_column_arr);
        }

        if (($where && is_array($where) && count($where) > 0)) {

            foreach ($where as $filter => $param) {
                $select->where($filter, $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("sst.id ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("sst.id ASC");
        }

        if (! empty($b2b_dataLimit)) {
            $select->limit($b2b_dataLimit);
        }
		
        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
        return $options;
    }

    public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'sst.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
                                                                       // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName) ";
                break;
            case 'product_group_name':
                $operatorFirstPart = " pgp.name ";
                break;
            case 'package_name':
                $operatorFirstPart = " pkg.name ";
                break;
            case 'category_name':
                $operatorFirstPart = " cat.name ";
                break;
            case 'user_country_id':
                $operatorFirstPart = " usr.country ";
                break;
			case 'user_country_name':
				$operatorFirstPart = " tld.value ";
				break;
            case 'category_displayble':
                $operatorFirstPart = " ( sel.category = '0' OR cat.displayble = '" . $operator_arr['value'] . "'  ) AND 1 ";
                $operator_arr['value'] = "1";
                break;
            case 'category_active':
                $operatorFirstPart = " cat.active ";
                break;
            case 'category_isShowProfile':
                $operatorFirstPart = " cat.isShowProfile ";
                break;
            case 'cpy_active':
                $operatorFirstPart = " cpy.active ";
                break;
            case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM " . Zend_Registry::get('dbPrefix') . "vote_voting as vts WHERE pro.id  = vts.table_id) AS total_votes ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
}