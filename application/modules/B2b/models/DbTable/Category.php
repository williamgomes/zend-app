<?php

class B2b_Model_DbTable_Category extends Eicra_Abstract_DbTable {

    /**

     * Table name
     */
    protected $_name = 'b2b_category';
    private $categoryTree = array();
    private $tree = '';
    private $counter = 0;
    protected $_cols = null;

    public function getCategoryInfo() {
        $select = $this->select()
                ->from($this->_name, array('id', 'name'))
                ->where('active =?', '1')
                ->order('name ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        if (!$options) {
            return null;
        }
        $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes(
                        $options);
        return $options;
    }

    public function getCategoryInfoByGroup($group_id) {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('active =?', '1')
                ->where('group_id =?', $group_id)
                ->order('category_order ASC');
        $options = $this->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getCategoryInfoByUrl($url_portion) {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('url_portion =?', $url_portion)
                ->limit(1);
        $options = $this->fetchAll($select);
        if ($options) {
            $options = ($options[0]) ? $options[0] : $options;
        }
        return $options;
    }

    public function getCategoryName($id) {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('id =?', $id);
        $options = $this->getDefaultAdapter()->fetchRow($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    // Get All Datas
    public function getListInfo($approve = null, $search_params = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $b2b_dataLimit = ($search_params && is_array($search_params) && $search_params['dataLimit']) ? $search_params['dataLimit'] : null;
        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('nc' => $this->_name), array(
                    'nc.*',
                    '(SELECT COUNT(bcp.id) FROM ' . Zend_Registry::get('dbPrefix') . 'b2b_company_profile as bcp  LEFT JOIN ' . Zend_Registry::get('dbPrefix') . 'user_profile AS cusr ON cusr.user_id = bcp.user_id WHERE bcp.category_id = nc.id AND bcp.active = "1" AND bcp.displayble = "1" AND cusr.status = "1") AS company_num',
                    '(SELECT COUNT(bp.id) FROM ' . Zend_Registry::get('dbPrefix') . 'b2b_products as bp  LEFT JOIN ' . Zend_Registry::get('dbPrefix') . 'user_profile AS pusr ON pusr.user_id = bp.user_id WHERE bp.category_id = nc.id AND bp.active = "1" AND bp.displayble = "1" AND pusr.status = "1") AS product_num',
                    '(SELECT COUNT(bbl.id) FROM ' . Zend_Registry::get('dbPrefix') . 'b2b_buying_leads as bbl  LEFT JOIN ' . Zend_Registry::get('dbPrefix') . 'user_profile AS blusr ON blusr.user_id = bbl.user_id WHERE bbl.category = nc.id AND bbl.active = "1" AND bbl.displayble = "1" AND blusr.status = "1") AS buying_leads_num',
                    '(SELECT COUNT(bsl.id) FROM ' . Zend_Registry::get('dbPrefix') . 'b2b_selling_leads as bsl  LEFT JOIN ' . Zend_Registry::get('dbPrefix') . 'user_profile AS slusr ON slusr.user_id = bsl.user_id WHERE bsl.category = nc.id AND bsl.active = "1" AND bsl.displayble = "1" AND slusr.status = "1") AS selling_leads_num'))
                ->joinLeft(
                        array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'nc.user_id = up.user_id', array('username' => 'up.username'))
                ->joinLeft(
                array('gr' => Zend_Registry::get('dbPrefix') . 'b2b_group'), 'gr.id = nc.group_id', array('group_name' => 'gr.group_name', 'file_path_b2b_images' => 'gr.file_path_b2b_images'));
        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('nc.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("nc.active = ?", $approve);
        }
        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order(
                            $sort_value_arr['field'] . ' ' .
                            $sort_value_arr['dir']);
                }
            } else {
                $select->order("nc.group_id ASC")->order('nc.category_order ASC');
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params, $hasChild, 'parent_id');
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("nc.group_id ASC")->order('nc.category_order ASC');
        }

        if ($hasChild === false) {
            $select->where("nc.parent_id = ?", '0');
        }

        if (!empty($b2b_dataLimit)) {
            $select->limit($b2b_dataLimit);
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getCategoryById($id) {
        try {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();
                $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCategoryTree() {
        try {
            $select = $this->select()
                    ->from($this->_name, array('id', 'parent_id', 'name'))
                    ->where('active =?', '1')
                    ->order('id ASC');
            $options = $this->fetchAll($select)->toArray();
            if (!$options) {
                $options = null;
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getSubcategories($parentId) {
        if (!is_numeric($parentId)) {
            throw new Exception('Supplied Category ID is not an integer value');
        }
        try {
            $select = $this->select()
                    ->from($this->_name, array('id', 'parent_id', 'name'))
                    ->where('active =?', '1')
                    ->where('parent_id =?', $parentId)
                    ->order('id ASC');
            $options = $this->fetchAll($select)->toArray();
            if (!$options) {
                $options = null;
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getRootNodes() {
        try {
            $select = $this->select()
                    ->from($this->_name, array('id', 'name'))
                    ->where('active =?', '1')
                    ->where('parent_id =?', '0')
                    ->order('id ASC');
            $options = $this->fetchAll($select)->toArray();
            if (!$options) {
                $options = null;
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getTree() {
        $rowsetArray = $this->getCategoryTree();
        $arrayTree = array();
        // Edited Debug by Nazrul
        // =================================================
        foreach ($rowsetArray as $row) {
            $id = $row["id"];
            $parent_id = $row["parent_id"] === NULL ? "NULL" : $row["parent_id"];
            $this->data[$id] = $row;
            $this->index[$parent_id][] = $id;
        }
        $this->display_child_nodes(0, 0);
        echo '<br> <hr>';
        // =================================================
        $this->generateArray($arrayTree, $rowsetArray, '0');
        echo '<pre>';
        print_r($arrayTree);
        echo '</pre>';
        exit();
        /*
         * foreach ($rowsetArray as $row ) { $id = $row["id"]; $parent_id =
         * $row["parent_id"] === NULL ? "NULL" : $row["parent_id"];
         * $this->data[$id] = $row; $this->index[$parent_id][] = $id; }
         * $this->display_child_nodes(0,0); echo $this->tree; echo '<br />
         * <hr>'; return $this->categoryTree;
         */
    }

    private function generateArray(&$arrayTree, &$rowsetArray, $parent_id) {
        $length = count($arrayTree);
        foreach ($rowsetArray as $key => $array) {
            if ($array['parent_id'] == $parent_id) {
                $arrayTree[$length] = $this->getChild($array, $rowsetArray);
                $length ++;
            }
        }
    }

    private function getChild($mainArray, &$rowsetArray) {
        if ($this->hasChild($rowsetArray, $mainArray['id'])) {
            $cnt = 0;
            foreach ($rowsetArray as $key => $array) {
                if ($array['parent_id'] == $mainArray['id']) {
                    $mainArray['children'][$cnt] = $this->getChild($array, $rowsetArray);
                    $cnt ++;
                }
            }
            return $mainArray;
        } else {
            return $mainArray;
        }
    }

    private function hasChild(&$rowsetArray, $parent_id) {
        $has = false;
        foreach ($rowsetArray as $key => $array) {
            if ($array['parent_id'] == $parent_id) {
                $has = true;
                break;
            }
        }
        return $has;
    }

    private function display_child_nodes($parent_id, $level) {
        $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
        if (isset($this->index[$parent_id])) {
            foreach ($this->index[$parent_id] as $id) {
                echo str_repeat(" ==> ", $level) . $this->data[$id]["name"] .
                "<br>";
                $this->display_child_nodes($id, $level + 1);
            }
        }
    }

    public function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'nc.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
            // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                        $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                        ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                        $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " .
                        $table_prefix . "firstName, ' ', " . $table_prefix .
                        "lastName) ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                        $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                        $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                        $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                        $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                        $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                        $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                        $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(
                        Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

    public function checkSubCategory($parent, $checkApprove = false) {
        if (!empty($parent)) {
            if ($checkApprove) {
                $validator = new Zend_Validate_Db_RecordExists(
                        array(
                    'table' => $this->_name,
                    'field' => 'parent_id',
                    'exclude' => 'active = "1" AND  displayble = "1"'
                        )
                );
            } else {
                $validator = new Zend_Validate_Db_RecordExists(
                        array(
                    'table' => $this->_name,
                    'field' => 'parent_id'
                        )
                );
            }
            $rs = ($validator->isValid($parent)) ? true : false;
        }
        return $rs;
    }

}
