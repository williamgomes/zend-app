<?php

class B2b_Model_DbTable_Compliances extends Eicra_Abstract_DbTable
{

    protected $_name    =  'b2b_compliances';

    //Get Datas
    public function getCompliancesInfo($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row)
        {
            $options = null;
        }
        else
        {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
        return   $options ;
    }

    //Get Datas
    public function getAllCompliances()
    {
        $select = $this->select()
        ->from($this->_name, array('id', 'compliances'))
        ->order('id ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }



    //Get Datas
    public function getSelectOptions($group_id = null)
    {
        if($group_id)
        {
            $select = $this->select()
            ->from($this->_name, array('id', 'business_type'))
            ->where('group_id =?',$group_id)
            ->order('id ASC');
        }
        else
        {
            $select = $this->select()
            ->from($this->_name, array('id', 'business_type'))
            ->order('id ASC');
        }
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }


}
?>