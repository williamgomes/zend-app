<?php
/**
* This is the DbTable class for the b2b_group table.
*/
class B2b_Model_DbTable_Inquiry extends Eicra_Abstract_DbTable{
    /** Table name */
    protected $_name    =  'b2b_inquiries';
	//Get Datas
	public function getAllGroupInfo()
    {
       $select = $this->select()
                       ->from($this->_name, array('*'))
                       ->order('id ASC');
		$options = $this->fetchAll($select)->toArray();
		if (!$options)
		{
           // throw new Exception("Count not find rows $id");
		   $options = null;
        }
        return $options;
    }


    public function getListInfo ($approve = null, $search_params = null,  $tableColumns = null)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $recipient_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
		$hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;

        $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array(
            'iqy' => $this->_name
        ), array(
            'iqy.*'
        ))
		->joinLeft(array(
            'iqys' =>  $this->_name
        ), 'iqy.parent = iqys.id', array(
            'parent_subject_name' => 'iqys.subject'            
        ))
        ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = iqy.sender_id', array(
            'user_email' => 'usr.username',
            'user_phone' => 'usr.phone',
            'user_fax' => 'usr.fax',
            'user_mobile' => 'usr.mobile',
            'user_website' => 'usr.website',
            'address' => 'usr.address',
            'user_city' => 'usr.city',
            'user_company' => 'usr.companyName',
            'user_postcode' => 'usr.postalCode',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
        ->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
        ))
       ->joinLeft(array(
            'cpy' => Zend_Registry::get('dbPrefix') . 'b2b_company_profile'
        ), 'usr.user_id  = cpy.user_id', array(
            'cp_company_name' => 'cpy.company_name',
            'cp_street' => 'cpy.street',
            'cp_state' => 'cpy.country',
            'cp_city' => 'cpy.city',
            'profile_since' => 'cpy.added_on',
            'cp_website' => 'cpy.website',
            'company_title' => 'cpy.seo_title',
            'cp_phone' => 'cpy.phone',
            'cp_phone_part2' => 'cpy.phone_part2',
            'cp_phone_part3' => 'cpy.phone_part3',
            'cp_fax' => 'cpy.fax',
            'cp_fax_part2' => 'cpy.fax_part2',
            'cp_fax_part3' => 'cpy.fax_part3',
            'cp_mobile' => 'cpy.mobile',
            'cp_mobile_part2' => 'cpy.mobile_part2',
            'cp_mobile_part3' => 'cpy.mobile_part3',
            'establishing_year' => 'cpy.establishing_year',
        ));

        if ($recipient_id && $auth->getIdentity()->access_other_user_article != '1') {
            $select->where('iqy.recipient_id= ?', $recipient_id);
        }

        if ($approve != null) {
            $select->where("iqy.active = ?", $approve);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
                $select->order("iqy.date DESC");
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
						$hasChild = ($filter_obj['field'] == 'parent') ? true : $hasChild;	
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i ++;
                    } else
                    if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
							$hasChild = ($sub_filter_obj['field'] == 'parent') ? true : $hasChild;
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                            $sub ++;
                        }
                        $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                        $i ++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("iqy.product_grp_id ASC")->order("iqy.id ASC");
        }
		
		if($hasChild === false && $auth->getIdentity()->access_other_user_article == '1')
		{
			$select->where("iqy.parent = ?", '0');
		}        

        $options = $this->fetchAll($select);

        if (! $options) {
            $options = null;
        }
        return $options;
    }


    private function setUniqueResult (&$multi_array, $uniq_field_name, $extra_field_array, $just = false)
    {
        if ($multi_array != null) {
            $uniq_array = array();
            foreach ($multi_array as $key => $array_value) {
                $uniq_array[$key] = $array_value[$uniq_field_name];
            }
            $uniq_array = array_unique($uniq_array);
            $uniq_result = array();
            $uniq_array_key = 0;
            foreach ($uniq_array as $key => $uniq_id) {
                $uniq_result[$uniq_array_key] = $this->getExtraField_Array($multi_array, $uniq_id, $uniq_field_name, $extra_field_array);
                $uniq_array_key ++;
            }
            if($just === true){ $this->setJustUnique($uniq_result, $extra_field_array); }
            $multi_array = $uniq_result;
        }
    }

    private function getExtraField_Array (&$multi_array, $uniq_id, $uniq_field_name, $extra_field_array)
    {
        $result_array = array();
        $result_key = 0;
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                $result_array = $array_value;
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    $result_array[$extra_field_value] = array();
                }
                break;
            }
        }
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    if ($array_value[$extra_field_value]) {
                        $result_array[$extra_field_value][$result_key] = $array_value[$extra_field_value];
                    }
                }
                $result_key ++;
            }
        }
        return $result_array;
    }


    private function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'iqy.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'name':
                $operatorFirstPart = " iqy.name ";
				break;
            case 'subject':
                $operatorFirstPart = " iqy.subject ";
                break;
            case 'flag':
                $operatorFirstPart = " iqy.flag ";
                break;
            case 'read':
                $operatorFirstPart = " iqy.read ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(inv.invoice_create_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(inv.invoice_create_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(inv.invoice_create_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    private function setJustUnique(&$multi_array, $extra_field_array)
    {
        foreach ($multi_array as $key => $value_arr)
        {
            foreach ($value_arr as $value_arr_key => $value)
            {
                if(in_array($value_arr_key, $extra_field_array))
                {
                    $multi_array[$key][$value_arr_key] = array_unique ($multi_array[$key][$value_arr_key]);
                }
            }
        }
    }

    private function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}
?>