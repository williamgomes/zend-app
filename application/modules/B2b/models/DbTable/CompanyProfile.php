<?php

class B2b_Model_DbTable_CompanyProfile extends Eicra_Abstract_DbTable
{
    protected $_name = 'b2b_company_profile';
    protected $_cols = null;
    public function getUserProfile ($user_id)
    {
        $select = $this->select()
            ->from($this->_name, array(
            '*'
        ))
            ->where('user_id =?', $user_id)
            ->order('id ASC');
        $row = $this->getAdapter()->fetchRow($select);
        if (! $row) {
            return null;
        }
        return $row;
    }
	
	public function getInfo ($id)
    {
        try {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();
                $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
	
    public function hasCompany ($companyTitle)
    {
        $select = $this->select()
        ->from($this->_name, array(
            'id',
            'company_name',
            'user_id'
        ))
        ->where('seo_title =?', $companyTitle)
        ->order('id ASC');
        $row = $this->getAdapter()->fetchRow($select);
        if (! $row) {
            return null;
        }
        return $row;
    }
    public function isUserExists ($user_id)
    {
        $select = $this->select()
        ->from($this->_name, array(
                'id',
                'company_name',
                'seo_title'
        ))
        ->where('user_id =?', $user_id);
        $row = $this->getAdapter()->fetchRow($select);
        return $row;
    }
    public function getCompanyList ($approve = null, $search_params = null, $userChecking = true, $type = null)
    {
        $select = $this->select()
            ->distinct()
            ->setIntegrityCheck(false)
            ->from(array(
            'cpy' => $this->_name
        ), array(
            'id',
            'user_id',
            'company_name',
            'seo_title',
            'profile_image_primary',
            'profile_image',
            'establishing_year',
            'compliances',
            'company_information',
            'trade_type',
            'compliances',
            'businesstype_id',
            'seo_title',
            'company_information',
			'keywords',
            'added_on'
        ))
            ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = cpy.user_id', array(
            'status' => 'usr.status',
            'state' => 'usr.state',
            'city' => 'usr.city',
            'user_country_id' => 'usr.country'
        ))
            ->joinLeft(array(
            'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
            'country_name' => 'tld.value',
            'country_code' => 'tld.code'
        ))
            ->joinLeft(array(
            'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pid.id = usr.package_id', array(
            'package_name' => 'pid.name',
            'package_logo' => 'pid.b2b_images',
            'priority_flag' => 'pid.priority_flag',
            'is_profile' => 'pid.profile',
            'priority' => 'pid.priority'
        ))
            ->joinLeft(array(
            'ucm' => Zend_Registry::get('dbPrefix') . 'b2b_compliance_mapper'
        ), 'usr.user_id  = ucm.user_id', array())
            ->joinLeft(array(
            'cpl' => Zend_Registry::get('dbPrefix') . 'b2b_compliances'
        ), 'ucm.compliance_id  = cpl.id', array(
            'compliance_name' => 'cpl.compliances',
            'compliance_img' => 'cpl.image'
        ))
            ->joinLeft(array(
            'biz' => Zend_Registry::get('dbPrefix') . 'b2b_biztype_mapper'
        ), 'usr.user_id  = biz.user_id', array())
            ->
        joinLeft(array(
            'typ' => Zend_Registry::get('dbPrefix') . 'b2b_business_type'
        ), 'biz.biztype_id  = typ.id', array(
            'business_type' => 'typ.business_type'
        ))
            ->joinLeft(array(
            'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
        ), 'cpy.category_id  = cat.id', array(
            'category_name' => 'cat.name',
            'category_id' => 'cat.id',
            'parent_id' => 'cat.parent_id',
			'category_isShowProfile' => 'cat.isShowProfile'
        ))
            ->joinLeft(array(
            'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
        ), 'grp.id  = cpy.group_id', array(
            'images_path' => 'grp.file_path_profile_image',
            'review_id' => 'grp.review_id'
        ));
        if ($approve) {
            $select->where("usr.status = ?", $approve);
            $select->where("cpy.active = ?", $approve);
        }
        if (! empty($type) && is_numeric($type)) {
            $select->where("cpy.trade_type != ?", $type);
        }
        if ($search_params != null) {
            
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                }
            } else {
				$select->order("cpy.company_name ASC");
			}
            if ($search_params['filter'] && $search_params['filter']['filters']) {
				$search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
				$select->order("cpy.company_name ASC");
			}
        $options = $this->fetchAll($select)->toArray();
        if (!empty($options)){
            $this->setUniqueResult($options, 'user_id', array(
                'compliance_name',
                'compliance_img',
                'business_type'
            ));
        }
        return $options;
    }
	
	public function getNumInActiveCompanies($group_id = null) 
    {
		if(empty($group_id))
		{
			$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.active =?','0'); 
		}
		else
		{
       		$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.group_id =?',$group_id)
					   ->where('gp.active =?','0'); 
		}		

        $options = $this->fetchAll($select);		

        if (!$options) 
		{
            $num_of_item = 0;
        }
		else
		{
			foreach($options as $row)
			{
				$num_of_item = $row['num'];
			}
		}
        return $num_of_item; 
    }
	
    public function getCompanyDetails ($title , $active = null, $type = null)
    {
        $select = $this->select()
        ->distinct()
        ->setIntegrityCheck(false)
        ->from(array(
                'cpy' => $this->_name
        ), array(
                'trade_type' => 'cpy.trade_type',
                'company_name' => 'cpy.company_name',
                'banner_img' => 'cpy.banner_img',
                'avatar_img' => 'cpy.avatar_img',
                'registered_in' => 'cpy.registered_in',
                'company_street' => 'cpy.street',
                'company_city' => 'cpy.city',
                'profile_image_primary' => 'cpy.profile_image_primary',
                'company_postal_code' => 'cpy.postal_code',
                'company_category' => 'cpy.company_category',
                'selling_categories' => 'cpy.selling_categories',
                'buying_categories' => 'cpy.buying_categories',
                'QA_policy' => 'cpy.QA_policy',
                'store_name' => 'cpy.store_name',
                'profile_image' => 'cpy.profile_image',
                'legal_owner' => 'cpy.legal_owner',
                'establishing_year' => 'cpy.establishing_year',
                'number_of_employees' => 'cpy.number_of_employees',
                'annual_revenue' => 'cpy.annual_revenue',
                'main_products' => 'cpy.main_products',
                'main_products_part1' => 'cpy.main_products_part1',
                'main_products_part2' => 'cpy.main_products_part2',
                'main_products_part3' => 'cpy.main_products_part3',
                'certification' => 'cpy.certification',
                'export_market' => 'cpy.export_market',
                'annual_sales' => 'cpy.annual_sales',
                'nearest_ports' => 'cpy.nearest_ports',
                'main_customers' => 'cpy.main_customers',
                'main_markets' => 'cpy.main_markets',
                'overseas_office' => 'cpy.overseas_office',
				'office_size' => 'cpy.office_size',
                'avg_lead_time' => 'cpy.avg_lead_time',
                'other_products' => 'cpy.other_products',
                'delivery_terms' => 'cpy.delivery_terms',
                'payment_currency' => 'cpy.payment_currency',
                'payment_type' => 'cpy.payment_type',
                'language' => 'cpy.language',
                'factory_location' => 'cpy.factory_location',
                'website' => 'cpy.website',
                'phone' => 'cpy.phone',
                'phone_part2' => 'cpy.phone_part2',
                'phone_part3' => 'cpy.phone_part3',
                'fax' => 'cpy.fax',
                'fax_part2' => 'cpy.fax_part2',
                'fax_part3' => 'cpy.fax_part3',
                'mobile' => 'cpy.mobile',
                'mobile_part2' => 'cpy.mobile_part2',
                'mobile_part3' => 'cpy.mobile_part3',
                'company_information' => 'cpy.company_information',
                'policy' => 'cpy.policy',
                'terms_condition' => 'cpy.terms_condition',
                'added_on' => 'cpy.added_on',
                'certified' => 'cpy.certified',
                'seo_title' => 'cpy.seo_title',
                'factory_size' => 'cpy.factory_size',
                'escrow' => 'cpy.escrow',
                'export_percentage' => 'cpy.export_percentage',
                'trade_shows' => 'cpy.trade_shows',
                'production_capacity' => 'cpy.production_capacity',
                'payment_method' => 'cpy.payment_method',
                'keywords' => 'cpy.keywords',
                'meta_desc' => 'cpy.meta_desc',
                'company_active' => 'cpy.active'
        ))
        ->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = cpy.user_id', array(
            'country' => 'usr.country',
            'register_date' => 'usr.register_date',
            'state' => 'usr.state',
            'city' => 'usr.city',
            'registered_on' => 'usr.register_date',
            'user_zip' => 'usr.postalCode',
            'usr_address' => 'usr.address',
            'usr_phone' => 'usr.phone',
            'usr_mobile' => 'usr.mobile',
            'usr_fax' => 'usr.fax',
            'usr_website' => 'usr.website',
            'user_status' => 'usr.status',
			'user_email' => 'usr.username',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
        ->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
                'country_name' => 'tld.value',
                'country_code' => 'tld.code'
        ))
        ->joinLeft(array(
                'ctld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'cpy.country = ctld.id', array(
                'company_country_name' => 'ctld.value',
                'company_country_code' => 'ctld.code'
        ))
        ->joinLeft(array(
                'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
        ), 'pid.id = usr.package_id', array(
                'package_name' => 'pid.name',
                'package_logo' => 'pid.b2b_images',
                'priority_flag' => 'pid.priority_flag',
                'is_feedback' => 'pid.feedback',
                'is_certification' => 'pid.certification',
                'is_massage' => 'pid.massage',
                'is_voting' => 'pid.voting',
                'is_profile' => 'pid.profile',
                'is_telephone' => 'pid.telephone',
                'is_website' => 'pid.website',
				'eCommerce' => 'pid.eCommerce',
                'is_social' => 'pid.social_network',
            	'priority' => 'pid.priority'
        ))
        ->joinLeft(array(
                'ucm' => Zend_Registry::get('dbPrefix') . 'b2b_compliance_mapper'
        ), 'usr.user_id  = ucm.user_id', array())
        ->joinLeft(array(
                'cpl' => Zend_Registry::get('dbPrefix') . 'b2b_compliances'
        ), 'ucm.compliance_id  = cpl.id', array(
                'compliance_name' => 'cpl.compliances',
                'compliance_img' => 'cpl.image'
        ))
        ->joinLeft(array(
                'biz' => Zend_Registry::get('dbPrefix') . 'b2b_biztype_mapper'
        ), 'usr.user_id  = biz.user_id', array())
        ->
        joinLeft(array(
                'typ' => Zend_Registry::get('dbPrefix') . 'b2b_business_type'
        ), 'biz.biztype_id  = typ.id', array(
                'business_type' => 'typ.business_type'
        ))
        ->joinLeft(array(
                'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
        ), 'grp.id  = cpy.group_id', array(
                'group_id' => 'grp.id',
                'default_img_path' => 'grp.file_path_b2b_images',
                'images_path' => 'grp.file_path_profile_image',
                'selling_img_path' => 'grp.file_path_selling_images',
                'buying_img_path' => 'grp.file_path_buying_images',
                'product_img_path' => 'grp.file_path_product_images',
                'img_big_width' => 'grp.img_big_width',
                'img_big_resize_func' => 'grp.img_big_resize_func',
                'img_big_height' => 'grp.img_big_height',
                'file_thumb_height' => 'grp.file_thumb_height',
                'review_id' => 'grp.review_id',
                'file_thumb_width' => 'grp.file_thumb_width'
        ))
            ->joinLeft(array(
            'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
        ), 'cpy.category_id  = cat.id', array(
            'category_name' => 'cat.name',
            'category_id' => 'cat.id',
            'parent_id' => 'cat.parent_id',
			'category_isShowProfile' => 'cat.isShowProfile'
        ))
        /*
        ->joinLeft(array(
                'buy' => Zend_Registry::get('dbPrefix') . 'b2b_buying_leads'
        ), 'buy.user_id  = cpy.user_id', array(
                'buying_leads_title' => 'buy.seo_title',
                'buying_leads_name' => 'buy.name'
        ))
        ->joinLeft(array(
                'sel' => Zend_Registry::get('dbPrefix') . 'b2b_selling_leads'
        ), 'sel.user_id  = cpy.user_id', array(
                'selling_leads_title' => 'sel.seo_title',
                'selling_leads_name' => 'sel.name'
        ))
        ->joinLeft(array(
                'pro' => Zend_Registry::get('dbPrefix') . 'b2b_products'
        ), 'pro.user_id  = cpy.user_id', array(
                'products_title' => 'sel.seo_title',
                'products_name' => 'sel.name'
        ))*/
        ->joinLeft(array(
                'pfc' => Zend_Registry::get('dbPrefix') . 'b2b_user_preferences'
        ), 'pfc.user_id  = cpy.user_id', array(
                'facebook' => 'pfc.facebook',
                'facebook_display' => 'pfc.facebook_display',
                'twitter' => 'pfc.twitter',
                'twitter_display' => 'pfc.twitter_display',
                'googleplus' => 'pfc.googleplus',
                'googleplus_display' => 'pfc.googleplus_display',
                'youtube' => 'pfc.youtube',
                'youtube_display' => 'pfc.youtube_display',
                'flickr' => 'pfc.flickr',
                'flickr_display' => 'pfc.flickr_display',
                'paypal' => 'pfc.paypal',
                'paypal_currency' => 'pfc.paypal_currency',
                'enable_paypal' => 'pfc.enable_paypal',
                'tocheckout' => 'pfc.tocheckout',
                'tocheckout_local' => 'pfc.tocheckout_local',
                'enable_2co' => 'pfc.enable_2co',
                'moneybookers' => 'pfc.moneybookers',
                'moneybookers_currency' => 'pfc.moneybookers_currency',
                'enable_mb' => 'pfc.enable_mb',
                'enable_paypal' => 'pfc.enable_paypal',
                'preferance_profile_images' => 'pfc.profile_image',
                'storeurl' => 'pfc.storeurl',
                'enable_paypal' => 'pfc.enable_paypal',
                'preferance_images' => 'pfc.b2b_images',
                'preferance_info' => 'pfc.other_info'
        ));
        //->order("sel.id ASC");
        if ($active) {
            $select->where("usr.status = ?", '1');
            $select->where("cpy.active = ?", '1');
            $select->where("cpy.seo_title = ?", $title);
        }
        if (! empty($type) && is_numeric($type)) {
            $select->where("cpy.trade_type != ?", $type);
        }
        $options = $this->fetchAll($select)->toArray();
        if (empty($options)) {
            $options = null;
        } else {
            $this->setUniqueResult($options, 'id', array(
                    'compliance_name',
                    'compliance_img',
                    'business_type'
                    /* 'buying_leads_title',
                    'buying_leads_name',
                    'selling_leads_title',
                    'selling_leads_name',
                    'products_title',
                    'products_name' */
            ), true );
        }
        return $options;
    }
	private function setJustUnique(&$multi_array, $extra_field_array)
	{
		foreach ($multi_array as $key => $value_arr)
		{
			foreach ($value_arr as $value_arr_key => $value)
			{
				if(in_array($value_arr_key, $extra_field_array))
				{
					$multi_array[$key][$value_arr_key] = array_unique ($multi_array[$key][$value_arr_key]);
				}
			}
		}
	}
    private function setUniqueResult (&$multi_array, $uniq_field_name, $extra_field_array, $just = false)
    {
        if ($multi_array != null) {
            $uniq_array = array();
            foreach ($multi_array as $key => $array_value) {
                $uniq_array[$key] = $array_value[$uniq_field_name];
            }
            $uniq_array = array_unique($uniq_array);
            $uniq_result = array();
            $uniq_array_key = 0;
            foreach ($uniq_array as $key => $uniq_id) {
                $uniq_result[$uniq_array_key] = $this->getExtraField_Array($multi_array, $uniq_id, $uniq_field_name, $extra_field_array);
                $uniq_array_key ++;
            }
			if($just === true){ $this->setJustUnique($uniq_result, $extra_field_array); }
            $multi_array = $uniq_result;
        }
    }
    private function getExtraField_Array (&$multi_array, $uniq_id, $uniq_field_name, $extra_field_array)
    {
        $result_array = array();
        $result_key = 0;
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                $result_array = $array_value;
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    $result_array[$extra_field_value] = array();
                }
                break;
            }
        }
        foreach ($multi_array as $key => $array_value) {
            if ($uniq_id == $array_value[$uniq_field_name]) {
                foreach ($extra_field_array as $extra_field_array_key => $extra_field_value) {
                    if ($array_value[$extra_field_value]) {
                        $result_array[$extra_field_value][$result_key] = $array_value[$extra_field_value];
                    }
                }
                $result_key ++;
            }
        }
        return $result_array;
    }
    public function getCompanyByTitle ($title)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array(
            'cpy' => $this->_name
        ), array(
            'user_id',
            'company_name',
            'seo_title',
            'establishing_year',
            'compliances',
            'businesstype_id'
        ))
            ->joinLeft(array(
            'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'usr.user_id = cpy.user_id', array(
            'country' => 'usr.country',
            'register_date' => 'usr.register_date',
            'status' => 'usr.status',
            'state' => 'usr.state',
            'city' => 'usr.city',
            'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
            ->joinLeft(array(
            'tld' => Zend_Registry::get('dbPrefix') . 'countries'
        ), 'usr.country = tld.id', array(
            'name' => 'tld.value',
            'code' => 'tld.code'
        ));
        $options = $this->fetchAll($select)->toArray();
        if (empty($options)) {
            $options = null;
        }
        return $options;
    }
    
    
    // Get All Datas
    public function getListInfo ($approve = null, $search_params = null, $tableColumns= null)
    {
        
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;
		$user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array(
                        'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
                        'state' => 'usr.state', 'city' => 'usr.city',
                        'username' => 'usr.username',
                        'user_status' => 'usr.status');
        $company_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_company_profile'] && is_array($tableColumns['b2b_company_profile'])) ? $tableColumns['b2b_company_profile'] : array('cpy.*');
        $user_countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array('country' => 'tld.value', 'code' => 'tld.code');
        $user_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_category'] && is_array($tableColumns['b2b_category'])) ? $tableColumns['b2b_category'] : array(
					'category_name' => 'cat.name',
					'category_id' => 'cat.id',
					'parent_id' => 'cat.parent_id',
					'category_isShowProfile' => 'cat.isShowProfile'
				);
        $countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['company_countries'] && is_array($tableColumns['company_countries'])) ? $tableColumns['company_countries'] : null;
        $b2b_compliance_mapper_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_compliance_mapper'] && is_array($tableColumns['b2b_compliance_mapper'])) ? $tableColumns['b2b_compliance_mapper'] : array();
        $b2b_biztype_mapper_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_biztype_mapper'] && is_array($tableColumns['b2b_biztype_mapper'])) ? $tableColumns['b2b_biztype_mapper'] : array();
        $membership_packages_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['membership_packages'] && is_array($tableColumns['membership_packages'])) ? $tableColumns['membership_packages'] : array('package_name' => 'pid.name');
		$vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
		$b2b_compliance_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_compliances'] && is_array($tableColumns['b2b_compliances'])) ? $tableColumns['b2b_compliances'] :  null;
		$b2b_biztype_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_business_type'] && is_array($tableColumns['b2b_business_type'])) ? $tableColumns['b2b_business_type'] :  null;
		$b2b_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['b2b_group'] && is_array($tableColumns['b2b_group'])) ? $tableColumns['b2b_group'] : null;
		$b2b_dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$setUniqueResult	=	($tableColumns && is_array($tableColumns) && is_bool($tableColumns['setUniqueResult'])) ? $tableColumns['setUniqueResult'] : false;
		$where = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;
        $select = $this->select()
            ->distinct()
            ->setIntegrityCheck(false)
            ->from(array('cpy' => $this->_name),
                $company_profile_column_arr
                );
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
            ), 'usr.user_id = cpy.user_id', $user_profile_column_arr);
        }
		if (($countries_column_arr &&  is_array($countries_column_arr) && count($countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'cpycunt' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'cpy.country = cpycunt.id', $countries_column_arr );
        }
        if (($user_countries_column_arr &&  is_array($user_countries_column_arr) && count($user_countries_column_arr) > 0)) {
            $select->joinLeft(array(
                'tld' => Zend_Registry::get('dbPrefix') . 'countries'
            ), 'usr.country = tld.id', $user_countries_column_arr );
        }
        if (($membership_packages_column_arr &&  is_array($membership_packages_column_arr) && count($membership_packages_column_arr) > 0)) {
            $select->joinLeft(array(
                'pid' => Zend_Registry::get('dbPrefix') . 'membership_packages'
            ), 'pid.id = usr.package_id', $membership_packages_column_arr) ;
        }
        if (($user_category_column_arr &&  is_array($user_category_column_arr) && count( $user_category_column_arr) > 0)) {
            $select ->joinLeft(array(
                'cat' => Zend_Registry::get('dbPrefix') . 'b2b_category'
            ), 'cpy.category_id  = cat.id', $user_category_column_arr);
        }
        if (($b2b_compliance_mapper_column_arr &&  is_array($b2b_compliance_mapper_column_arr) && count( $b2b_compliance_mapper_column_arr) > 0)) {
            $select ->joinLeft(array(
                'ucm' => Zend_Registry::get('dbPrefix') . 'b2b_compliance_mapper'
            ), 'usr.user_id  = ucm.user_id', $b2b_compliance_mapper_column_arr);
        }
        if (($b2b_compliance_column_arr &&  is_array($b2b_compliance_column_arr) && count( $b2b_compliance_column_arr) > 0)) {
            $select ->joinLeft(array(
                'cpl' => Zend_Registry::get('dbPrefix') . 'b2b_compliances'
            ), 'ucm.compliance_id  = cpl.id', $b2b_compliance_column_arr);
        }
        if (($b2b_biztype_mapper_column_arr &&  is_array($b2b_biztype_mapper_column_arr) && count( $b2b_biztype_mapper_column_arr) > 0)) {
            $select ->joinLeft(array(
                'biz' => Zend_Registry::get('dbPrefix') . 'b2b_biztype_mapper'
            ), 'usr.user_id  = biz.user_id', $b2b_biztype_mapper_column_arr);
        }
        if (($b2b_biztype_column_arr &&  is_array($b2b_biztype_column_arr) && count( $b2b_biztype_column_arr) > 0)) {
            $select ->joinLeft(array(
                'typ' => Zend_Registry::get('dbPrefix') . 'b2b_business_type'
            ), 'biz.biztype_id  = typ.id', $b2b_biztype_column_arr);
        }
        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' &&
                 $userChecking == true) {
            $select->where('cpy.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("cpy.active = ?", $approve);
        }
		if (($b2b_group_column_arr &&  is_array($b2b_group_column_arr) && count( $b2b_group_column_arr) > 0)) {
            $select ->joinLeft(array(
                'grp' => Zend_Registry::get('dbPrefix') . 'b2b_group'
            ), 'grp.id  = cpy.group_id', $b2b_group_column_arr);
        }
		if (($vote_column_arr &&  is_array($vote_column_arr) && count( $vote_column_arr) > 0)) {
            $select ->joinLeft(array(
                'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting'
            ), 'cpy.id  = vt.table_id', $vote_column_arr);
        }
        if (($where &&  is_array($where) && count( $where) > 0)) {
            foreach ($where as $filter => $param){
                $select->where($filter , $param);
            }
        }
        
        
        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
                    	$select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
					}
                }
            } else {
                $select->order("cpy.company_name ASC");
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $whereCompany = $search_services->getSearchWhereClause($search_params);
                if (! empty($whereCompany)) {
                    $select->where($whereCompany);
                    
                }
            }
        } else {
            $select->order("cpy.company_name ASC");
        }
		if(!empty($b2b_dataLimit))
		{
			$select->limit($b2b_dataLimit);
		}
        $options = $this->fetchAll($select);
        
        if (! $options) {
            $options = null;
        }
        if($setUniqueResult === true)
        {
            if (!empty($options)){
                $options =  $options->toArray();
                $this->setUniqueResult($options, 'user_id', array(
                    'compliance_name',
                    'compliance_img',
                    'business_type'
                ));
            }
        }
        return $options;
        
    }
    public function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'cpy.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
                                                                       // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                        $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                                 ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                                 $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " .
                         $table_prefix . "firstName, ' ', " . $table_prefix .
                         "lastName) ";
                break;
			case 'package_name':
					$operatorFirstPart = " pid.name ";
				break;
			case 'package_price':
					$operatorFirstPart = " pid.price ";
				break;
			case 'package_id':
					$operatorFirstPart = " usr.package_id ";
				break;
			case 'advtype_input':
					$operatorFirstPart = " typ.id ";
				break;
			case 'user_country_id':
					$operatorFirstPart = " usr.country ";
				break;
			case 'company_id_in':
				$company_id_in_arr = explode(',', $operator_arr['value']);
				$company_id_in_arr = array_filter( $company_id_in_arr );
				foreach($company_id_in_arr as $key => $value)
				{
					$company_id_in_arr[$key] = '"'.$value.'"';
				}
				$company_id_in_string = implode(',', $company_id_in_arr);
                $operatorFirstPart = " cpy.id IN( ".$company_id_in_string."  ) AND 1 ";
				$operator_arr['value'] = "1";
                break;
			case 'category_displayble':
                $operatorFirstPart = " ( cpy.category_id = '0' OR cat.displayble = '".$operator_arr['value']."'  ) AND 1 ";
				$operator_arr['value'] = "1";
                break;
			case 'category_active':
                $operatorFirstPart = " cat.active ";
                break;
            case 'category_name':
                $operatorFirstPart = " cat.name ";
                break;
            case 'country':
                $operatorFirstPart = " tld.value ";
                break;
			case 'category_isShowProfile':
                $operatorFirstPart = " cat.isShowProfile ";
                break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE cpy.id  = vts.table_id) AS total_votes ";
                break;
			case 'added_day_num':
                $operatorFirstPart = " ( (DATEDIFF(now(), cpy.added_on)) > pid.renewable AND pid.renewable != '0' AND usr.package_id != '0' )  AND 1 ";
				$operator_arr['value'] = "1";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                         $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                         $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }
    public function isColumnExists ($column)
    {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
    public function isActive ($user_id)
    {
        $select = $this->select()
        ->from($this->_name, array(
                'id',
				'group_id',
				'company_name',
                'active'
        ))
        ->where('user_id =?', $user_id);
        $row = $this->getAdapter()->fetchRow($select);
        return $row;
    }
    public function isUserExits($user_id)
    {
    	$select = $this->select()
    	->setIntegrityCheck(false)
    	->from(array(
                'cp' => $this->_name
            ), array(
    			'company_id' => 'cp.id',
    			'active'  => 'cp.active'
    	))
    	->joinLeft(array(
    			'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
    	), 'usr.user_id = cp.user_id', array(
    			'package_id' => 'usr.package_id',
    			'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)",
    			'user_status' => 'usr.status',
    	))
    	->where('cp.user_id =?', $user_id);
    	$row = $this->getAdapter()->fetchRow($select);
    	return $row;
    }
    public function getUserByID ($id)
    {
        $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array(
                'cpy' => $this->_name
        ), array(
                'user_id',
                'company_name',
                'seo_title'
        ))
        ->joinLeft(array(
                'usr' => Zend_Registry::get('dbPrefix') . 'user_profile'
        ), 'cpy.user_id = usr.user_id', array(
                'username' => 'usr.username',
                'full_name' => "CONCAT(usr.title, ' ', usr.firstName, ' ', usr.lastName)"
        ))
        ->where('cpy.id =?', $id);
        $options = $this->fetchAll($select)->toArray();
        if (empty($options)) {
            $options = null;
        }
        return $options;
    }
}
?>