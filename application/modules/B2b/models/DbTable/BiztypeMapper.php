<?php

class B2b_Model_DbTable_BiztypeMapper extends Eicra_Abstract_DbTable {

    protected $_name = 'b2b_biztype_mapper';

    public function savebiztype($resultArray, $user_id) {

        if (is_array($resultArray) && is_numeric($user_id)) {
            $this->deleteBiztype($user_id);
            foreach ($resultArray as $row) {

                if ($row['biztype_id']) {
                    $this->insert($row);
                }
            }
        } else {

            throw new Exception('Oops... Invalid b2b_biztype_mapper data to update');
        }
    }

    public function deleteBiztype($user_id) {

        if ($user_id) {
            $where = $this->getAdapter()->quoteInto('user_id IN (?)', $user_id);
            $this->delete($where);
        } else {

            throw new Exception('Oops... Invalid  User ID provided ');
        }
    }

    public function getBiztype($user_id) {
        $select = $this->select()
                ->from($this->_name, array(
                    'biztype_id'
                ))
                ->where('user_id =?', $user_id)
                ->order('id ASC');
        $rows = $this->getAdapter()->fetchAll($select);

        if (!$rows) {
            return null;
        }
        return $rows;
    }

}
