<?php

class B2b_Model_Inquiry {

    private $_id;
    private $_recipient_id;
    private $_recipient_title;
    private $_sender_id;
    private $_trade_type;
    private $_name;
    private $_company_name;
    private $_authorization;
    private $_street;
    private $_state;
    private $_city;
    private $_postal_code;
    private $_country;
    private $_ip;
    private $_reply_with;
    private $_location;
    private $_phone;
    private $_phone_part2;
    private $_phone_part3;
    private $_fax;
    private $_fax_part2;
    private $_fax_part3;
    private $_website;
    private $_email;
    private $_return_mail;
    private $_subject;
    private $_message;
    private $_flag;
    private $_feedbacks;
    private $_read;
    private $_displayble;
    private $_added_on;
    private $_status;
    private $_active;
    private $_parent;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Setter Property - ' . $method);
        }
        $this->method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Getter Property - ' . $method);
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function save() {
        $mapper = new B2b_Model_InquiryMapper();
        $return = $mapper->save($this);
        return $return;
    }

    /**
     * @return the $_id
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param field_type $_id
     */
    public function setId($_id) {
        $this->_id = $_id;
    }

    /**
     * @return the $_id
     */
    public function getParent() {
        return $this->_parent;
    }

    /**
     * @param field_type $_id
     */
    public function setParent($_parent) {
        $this->_parent = $_parent;
    }

    /**
     * @return the $_sender_id
     */
    public function getSender_id() {
        return $this->_sender_id;
    }

    /**
     * @param field_type $_sender_id
     */
    public function setSender_id($_sender_id) {
        $this->_sender_id = $_sender_id;
    }

    /**
     * @return the $_recipient_id
     */
    public function getRecipient_id() {
        return $this->_recipient_id;
    }

    /**
     * @param field_type $_recipient_id
     */
    public function setRecipient_id($_recipient_id) {
        $this->_recipient_id = $_recipient_id;
    }

    /**
     * @return the $_recipient_title
     */
    public function getRecipient_title() {
        return $this->_recipient_title;
    }

    /**
     * @param field_type $_recipient_title
     */
    public function setRecipient_title($_recipient_title) {
        $this->_recipient_title = $_recipient_title;
    }

    /**
     * @return the $_trade_type
     */
    public function getTrade_type() {
        return $this->_trade_type;
    }

    /**
     * @param field_type $_trade_type
     */
    public function setTrade_type($_trade_type) {
        $this->_trade_type = $_trade_type;
    }

    /**
     * @return the $_name
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param field_type $_name
     */
    public function setName($_name) {
        $this->_name = $_name;
    }

    /**
     * @return the $_company_name
     */
    public function getCompany_name() {
        return $this->_company_name;
    }

    /**
     * @param field_type $_company_name
     */
    public function setCompany_name($_company_name) {
        $this->_company_name = $_company_name;
    }

    /**
     * @return the $_street
     */
    public function getStreet() {
        return $this->_street;
    }

    /**
     * @param field_type $_street
     */
    public function setStreet($_street) {
        $this->_street = $_street;
    }

    /**
     * @return the $_state
     */
    public function getState() {
        return $this->_state;
    }

    /**
     * @param field_type $_state
     */
    public function setState($_state) {
        $this->_state = $_state;
    }

    /**
     * @return the $_city
     */
    public function getCity() {
        return $this->_city;
    }

    /**
     * @param field_type $_city
     */
    public function setCity($_city) {
        $this->_city = $_city;
    }

    /**
     * @return the $_postal_code
     */
    public function getPostal_code() {
        return $this->_postal_code;
    }

    /**
     * @param field_type $_postal_code
     */
    public function setPostal_code($_postal_code) {
        $this->_postal_code = $_postal_code;
    }

    /**
     * @return the $_country
     */
    public function getCountry() {
        return $this->_country;
    }

    /**
     * @param field_type $_country
     */
    public function setCountry($_country) {
        $this->_country = $_country;
    }

    /**
     * @return the $_authorization
     */
    public function getAuthorization() {
        return $this->_authorization;
    }

    /**
     * @param field_type $_authorization
     */
    public function setAuthorization($_authorization) {
        $this->_authorization = $_authorization;
    }

    /**
     * @return the $_ip
     */
    public function getIp() {
        return $this->_ip;
    }

    /**
     * @param field_type $_ip
     */
    public function setIp($_ip) {
        $ip = null;

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {         // check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {         // to check ip is pass
        // from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->_ip = $ip;
    }

    /**
     * @return the $_reply_with
     */
    public function getReply_with() {
        return $this->_reply_with;
    }

    /**
     * @param field_type $_reply_with
     */
    public function setReply_with($_reply_with) {
        $this->_reply_with = $_reply_with;
    }

    /**
     * @return the $_location
     */
    public function getLocation() {
        return $this->_location;
    }

    /**
     * @param field_type $_location
     */
    public function setLocation($_location) {
        $this->_location = $_location;
    }

    /**
     * @return the $_phone
     */
    public function getPhone() {
        return $this->_phone;
    }

    /**
     * @param field_type $_phone
     */
    public function setPhone($_phone) {
        $this->_phone = $_phone;
    }

    /**
     * @return the $_phone_part2
     */
    public function getPhone_part2() {
        return $this->_phone_part2;
    }

    /**
     * @param field_type $_phone_part2
     */
    public function setPhone_part2($_phone_part2) {
        $this->_phone_part2 = $_phone_part2;
    }

    /**
     * @return the $_phone_part3
     */
    public function getPhone_part3() {
        return $this->_phone_part3;
    }

    /**
     * @param field_type $_phone_part3
     */
    public function setPhone_part3($_phone_part3) {
        $this->_phone_part3 = $_phone_part3;
    }

    /**
     * @return the $_fax
     */
    public function getFax() {
        return $this->_fax;
    }

    /**
     * @param field_type $_fax
     */
    public function setFax($_fax) {
        $this->_fax = $_fax;
    }

    /**
     * @return the $_fax_part2
     */
    public function getFax_part2() {
        return $this->_fax_part2;
    }

    /**
     * @param field_type $_fax_part2
     */
    public function setFax_part2($_fax_part2) {
        $this->_fax_part2 = $_fax_part2;
    }

    /**
     * @return the $_fax_part3
     */
    public function getFax_part3() {
        return $this->_fax_part3;
    }

    /**
     * @param field_type $_fax_part3
     */
    public function setFax_part3($_fax_part3) {
        $this->_fax_part3 = $_fax_part3;
    }

    /**
     * @return the $_website
     */
    public function getWebsite() {
        return $this->_website;
    }

    /**
     * @param field_type $_website
     */
    public function setWebsite($_website) {
        $this->_website = $_website;
    }

    /**
     * @return the $_email
     */
    public function getEmail() {
        return $this->_email;
    }

    /**
     * @param field_type $_email
     */
    public function setEmail($_email) {
        $this->_email = $_email;
    }

    /**
     * @return the $_return_mail
     */
    public function getReturn_mail() {
        return $this->_return_mail;
    }

    /**
     * @param field_type $_return_mail
     */
    public function setReturn_mail($_return_mail) {
        $this->_return_mail = $_return_mail;
    }

    /**
     * @return the $_subject
     */
    public function getSubject() {
        return $this->_subject;
    }

    /**
     * @param field_type $_subject
     */
    public function setSubject($_subject) {
        $this->_subject = $_subject;
    }

    /**
     * @return the $_message
     */
    public function getMessage() {
        return $this->_message;
    }

    /**
     * @param field_type $_message
     */
    public function setMessage($_message) {
        $this->_message = $_message;
    }

    /**
     * @return the $_flag
     */
    public function getFlag() {
        return $this->_flag;
    }

    /**
     * @param field_type $_flag
     */
    public function setFlag($_flag) {
        $this->_flag = $_flag;
    }

    /**
     * @return the $_feedbacks
     */
    public function getFeedbacks() {
        return $this->_feedbacks;
    }

    /**
     * @param field_type $_feedbacks
     */
    public function setFeedbacks($_feedbacks) {
        $this->_feedbacks = $_feedbacks;
    }

    /**
     * @return the $_read
     */
    public function getRead() {
        return $this->_read;
    }

    /**
     * @param field_type $_read
     */
    public function setRead($_read) {
        $this->_read = $_read;
    }

    /**
     * @return the $_displayble
     */
    public function getDisplayble() {
        return $this->_displayble;
    }

    /**
     * @param field_type $_displayble
     */
    public function setDisplayble($_displayble) {
        $this->_displayble = $_displayble;
    }

    /**
     * @return the $_added_on
     */
    public function getAdded_on() {
        return ($this->_added_on) ? $this->_added_on : date("Y-m-d h:i:s");
    }

    /**
     * @param field_type $_added_on
     */
    public function setAdded_on($_added_on) {
        $this->_added_on = $_added_on;
    }

    /**
     * @return the $_status
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * @param field_type $_status
     */
    public function setStatus($_status) {
        $this->_status = $_status;
    }

    /**
     * @return the $_active
     */
    public function getActive() {
        return $this->_active;
    }

    /**
     * @param field_type $_active
     */
    public function setActive($_active) {
        $this->_active = $_active;
    }

}
