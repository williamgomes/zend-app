<?php
class B2b_Model_CompliancesMapper
{
	protected $_dbTable;

	public function setDbTable($dbTable)
	{

		if (is_string($dbTable)) {

			$dbTable = new $dbTable();
		}

		if (!$dbTable instanceof Zend_Db_Table_Abstract) {

			throw new Exception('Invalid table data gateway provided');
		}

		$this->_dbTable = $dbTable;
		return $this;
	}


	public function getDbTable()
	{

		if (null === $this->_dbTable) {

			$this->setDbTable('B2b_Model_DbTable_Compliances');
		}

		return $this->_dbTable;
	}


	public function fetchAll ()
    {
        try {
            return $this->getDbTable()
                ->fetchAll()
                ->toArray();
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }


	public function delete ($id){

		$result = null;
		try {
			$where = array('id = ?' => $id);

			$this->getDbTable()->delete($where);

			$result = array('status' => 'ok' ,'id' => $id);

		} catch (Exception $e) {

			$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());

		}

		return $result;

	}




}