<?php
class B2b_Model_PackageMapper
{
    protected $_dbTable;
    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {

            $dbTable = new $dbTable();
        }

        if (! $dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    public function getDbTable ()

    {
        if (null === $this->_dbTable) {

            $this->setDbTable('B2b_Model_DbTable_Package');
        }

        return $this->_dbTable;
    }
    public function save (B2b_Model_Package $obj)
    {
        $id = $obj->getId();

        $data = array(

            'id' => $obj->getId(),
            'group_id' => $obj->getGroup_id(),
            'products' => $obj->getProducts(),
            'name' => $obj->getName(),
            'description' => $obj->getDescription(),
            'b2b_images' => $obj->getB2b_images(),
            'selling' => $obj->getSelling(),
            'buying' => $obj->getBuying(),
            'user_id' => $obj->getUser_id(),
            'price' => $obj->getPrice(),
            'strikethrough_price' => $obj->getStrikethrough_price(),
            'profile' => $obj->getProfile(),
            'telephone' => $obj->getTelephone(),
            'website' => $obj->getWebsite(),
            'renewable' => $obj->getRenewable(),
            'certification' => $obj->getCertification(),
            'priority' => $obj->getPriority(),
            'priority_flag' => $obj->getPriority_flag(),
            'feedback' => $obj->getFeedback(),
            'voting' => $obj->getVoting(),
            'spotlight' => $obj->getSpotlight(),
            'tradeshow' => $obj->getTradeshow(),
            'social_network' => $obj->getSocial_network(),
           // 'buyers_profile' => $obj->getBuyers_profile(),
            'massage' => $obj->getMassage(),
            'directory' => $obj->getDirectory(),
            'preapproval' => $obj->getPreapproval(),
            'rm_footer' => $obj->getRm_footer(),
            'rm_header' => $obj->getRm_header(),
            'gallery' => $obj->getGallery(),
        	'eCommerce'	=> $obj->getECommerce(),
            'active' => $obj->getActive(),
            'displayable' => $obj->getDisplayable()
        );

        if ((null === ($id = $obj->getId())) || empty($id))
        {

            unset($data['id']);
            $data['package_order'] = $obj->getPackage_order();
            try
            {
                $last_id = $this->getDbTable()->insert($data);
                $result = array(
                    'status' => 'ok',
                    'id' => $last_id
                );
            }
            catch (Exception $e)
            {

                $result = array(
                    'status' => 'err',
                    'id' => $last_id,
                    'msg' => $e->getMessage()
                );
            }
        }
        else
        {

            // Start the Update process

            try {

                $this->getDbTable()->update($data, array(
                    'id = ?' => $id
                ));

                $result = array(
                    'status' => 'ok',
                    'id' => $id
                );
            }
            catch (Exception $e)
            {

                $result = array(
                    'status' => 'err',
                    'id' => $id,
                    'msg' => $e->getMessage()
                );
            }
        }

        return $result;
    }
    public function fetchAll ($pageNumber, $approve = null, $search_params = null)
    {
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
    public function delete ($id)
    {
        $result = null;

        try {

            $where = array(
                'id = ?' => $id
            );

            $this->getDbTable()->delete($where);

            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {

            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }

        return $result;
    }
    public function updateStatus ($data, $id)
    {
        $where = array(
            'id = ?' => $id
        );

        if ((! empty($id) && is_numeric($id)) && ! empty($data)) {

            $resultSet = $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id,
                'records' => $data['active']
            );
            return $result;
        } else {

            throw new Exception('Invalid Product or Status Selected ' . $id);
        }
    }
    public function getPackages ($active)
    {
        $packages = null;

        try {
            if ($active) {
                $packages = $this->getDbTable()->getAllPackages($active);
            } else {
                $packages = $this->getDbTable()->getAllPackages(false);
            }

            return $packages;
        }
        catch (Exception $e) {

            return null;
        }
    }

    public function getAllPackages ($active)
    {
        $packages = null;

        try {
            $packages = $this->getDbTable()->getAllPackages($active);
            return $packages;
        }
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

    private function getActivationStatus ()
    {
        $productTable = new B2b_Model_DbTable_Preferences();
        $value = $productTable->getSettingByKey('auto_publish_buylead');
        $status = $value['value'];
        if ($status) {
            return '1';
        } else {
            return '0';
        }
    }
    public function displayable ($id, $displayable)
    {
        $result = null;
        try {
            $data = array(
                'displayable' => $displayable
            );
            $where = array(
                'id = ?' => $id
            );
            $this->getDbTable()->update($data, $where);
            $result = array(
                'status' => 'ok',
                'id' => $id
            );
        } catch (Exception $e) {
            $result = array(
                'status' => 'err',
                'id' => $id,
                'msg' => $e->getMessage()
            );
        }
        return $result;
    }
}