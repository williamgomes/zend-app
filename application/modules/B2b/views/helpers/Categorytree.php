<?php
class B2b_View_Helper_Categorytree extends Zend_View_Helper_Abstract 
{

	public static function getTreeDataSource($parent,$view,$group_id,$selectCategory = null, $expanded = false)
	{
		$category_db = new B2b_Model_DbTable_Category();
		$category_info	=	$category_db->getCategoryInfoByGroup($group_id);
		if($category_info && !is_array($category_info)){  $category_info = $category_info->toArray();} 
		
		if($parent == '' || $parent == null)
		{
			$data_obj = array(
								array(
										'id'	=> '0',
										'text' => html_entity_decode($view->translator->translator("common_tree_root"), ENT_QUOTES, 'UTF-8'), 
										'imageUrl'=> "vendor/scripts/js/images/folder.png",
										'expanded' => (bool)$expanded,
										'hasChildren'	=> true								
								)
						);
			if(self::hasChild('0', $category_info))
			{
				$data_obj[0]['hasChildren'] =  true;
				$data_obj[0]['items'] = ($expanded === true) ? self::recursiveTree($parent, $view, $category_info, null, $expanded) : null  ;
			}
			else
			{
				$data_obj[0]['hasChildren'] =  false;
				$data_obj[0]['expanded'] =  false;
				$data_obj[0]['imageUrl'] =  'vendor/scripts/js/images/page_white_text.png';
			}
		}
		else
		{
				$data_obj = self::recursiveTree($parent, $view, $category_info, null, $expanded);  
		}
		return $data_obj;
	}
	
	private static function recursiveTree($parent, $view, $category_info, $selectCategory = null, $expanded = false)
	{
		if($category_info)
		{
			$arr_count = 0;
			$data_obj = array();			
			foreach($category_info as $key => $info)
			{
				if($info['parent_id'] == $parent)
				{					
					$data_obj[$arr_count] = array( 'id'	=> $info['id'], 'parent' => $info['parent_id'], 'text' => $view->escape($info['name']), 'imageUrl' => "vendor/scripts/js/images/page_white_text.png" );
					if(self::hasChild($info['id'], $category_info))
					{
						$data_obj[$arr_count]['expanded']	=	(bool)$expanded;
						$data_obj[$arr_count]['hasChildren']	=	true;
						$data_obj[$arr_count]['items'] = ($expanded == true) ? self::recursiveTree($data_obj[$arr_count]['id'], $view, $category_info, $selectCategory) : null;
						$data_obj[$arr_count]['imageUrl'] = "vendor/scripts/js/images/folder.png";
					}
					else
					{
						$data_obj[$arr_count]['imageUrl'] = "vendor/scripts/js/images/page_white_text.png";
						$data_obj[$arr_count]['hasChildren']	=	false;
					}
					$arr_count++;					
				}								
			}			
		}	
		return $data_obj;	
	}
	
	private static function hasChild($parent, $category_info)
	{
		$hasChild = false;
		foreach($category_info as $key => $info)
		{
			if($parent == $info['parent_id'])
			{
				$hasChild = true;
			}
		}
		return $hasChild;
	}
}