<?php

class B2b_View_Helper_Group extends Zend_View_Helper_Abstract 
{	
	public function getNumOfB2b($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'b2b_category'), array('COUNT(*) AS num_cat'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_cat'];
		}
		return $num_cat;
	}
	
	public function getNumOfSelling($group_id)
	{
		//DB Connection
		$selling_db = new B2b_Model_DbTable_Selling();	
		
		$select = $selling_db->select()
					   			->from(array('s' => Zend_Registry::get('dbPrefix').'b2b_selling_leads'), array('COUNT(s.id) AS num_selling_leads'))
					   ->where('s.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_selling_leads'];
		}
		return $num_cat;
	}
	
	public function getNumOfBuying($group_id)
	{
		//DB Connection
		$buying_db = new B2b_Model_DbTable_Buying();	
		
		$select = $buying_db->select()
					   			->from(array('b' => Zend_Registry::get('dbPrefix').'b2b_buying_leads'), array('COUNT(b.id) AS num_buying_leads'))
					   			->where('b.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_buying_leads'];
		}
		return $num_cat;
	}
	
	public function getNumOfCompany($group_id)
	{
		//DB Connection
		$company_db = new B2b_Model_DbTable_CompanyProfile();	
		
		$select = $company_db->select()
					   			->from(array('cp' => Zend_Registry::get('dbPrefix').'b2b_company_profile'), array('COUNT(cp.id) AS num_company_profile'))
					   			->where('cp.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_company_profile'];
		}
		return $num_cat;
	}
	
	public function getNumOfB2bType($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();  
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'b2b_business_type'), array('COUNT(*) AS num_type'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_type = $row['num_type'];
		}
		return $num_type;
	}
	
	public function getNumOfArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'cities'), array('COUNT(*) AS num_area'))
					   ->where('g.state_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_area = $row['num_area'];
		}
		return $num_area;
	}
	
	public function getNumOfProForArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.area_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfB2bForType($id, $field_name, $table_name = 'b2b_company_profile')
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').$table_name), array('COUNT(*) AS num_b2b'))
					   ->where('g.'.$field_name.' = ?', $id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_b2b'];
		}
		return $num_pro;
	}
	
	public function getNumOfB2bForRoomType($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('room_type_id'));
		
		$rs = $select->query()->fetchAll();
		$count = 0;
		foreach($rs as $row)
		{
			$num_pro_arr = explode(',',$row['room_type_id']);
			if(in_array($id,$num_pro_arr))
			{
				$count++;
			}
		}
		return $count;
	}
	
	public function getNumOfProduct($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT(*) AS num_product'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_product = $row['num_product'];
		}
		return $num_product;
	}
	
	public function getNumOfProductCountry($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT( DISTINCT g.country_id ) AS num_country'))
					   ->where('g.group_id = ?',$group_id);
					   
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_product = $row['num_country'];
		}
		return $num_product;
	}
	
	public function getPopularB2b($group_id, $limit = 20)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('vv' => Zend_Registry::get('dbPrefix').'vote_voting'), array('item_id' => 'DISTINCT(vv.table_id)', 'votes' => 'SUM(vv.vote_value)'))
					   ->where('hp.group_id = ?',$group_id)
					   ->where('vv.table_name = ?','hotels_page')
					   ->group('vv.table_id')
					   ->order('votes DESC')
					   ->joinLeft(array('hp' => Zend_Registry::get('dbPrefix').'hotels_page'), 'vv.table_id = hp.id')
					   ->limit($limit);
		
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			$options = $rs;
		}
		else
		{
			$options = null;
		}
		return $options;
	}
	
}
