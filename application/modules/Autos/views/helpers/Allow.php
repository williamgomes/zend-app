<?php
class Autos_View_Helper_Allow extends Zend_View_Helper_Abstract 
{
	public function allow($action = null, $controller = null, $module = null, $callback = null, $params = null) 
	{
		return Eicra_Service_RuleChecker::isAllowed($action, $controller, $module, $callback, $params);
	}
	
	public function allowRule($action = null, $controller = null, $module = null,$role_id = null, $callback = null, $params = null) 
	{
		return Eicra_Service_RuleChecker::isAllowedRule($action, $controller, $module,$role_id , $callback, $params);
	}	
}