<?php
class Gallery_View_Helper_CategoryPanel extends Zend_View_Helper_Abstract 
{
	public function checkChild($parent,$group_id)
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		$select = $conn->select()
						->from(array('g' => Zend_Registry::get('dbPrefix').'gallery_category'), array('g.category_name','g.category_title','g.category_order','g.parent'))						
						->where('g.parent = ?', $parent)
						->where('g.group_id = ?', $group_id);
		$rs = $select->query()->fetchAll();	
		if($rs)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function getSubCategory($parent,$view,$group_id,$selectCategory = null,$menu_name) 
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$selectSubCategory = $conn->select()
						->from(array('g' => Zend_Registry::get('dbPrefix').'gallery_category'), array('g.id','g.group_id','g.category_name','g.category_title','g.parent','g.category_order'))						
						->where('g.parent = ?', $parent)
						->where('g.group_id = ?', $group_id);
			$rs = $selectSubCategory->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $topic_datas)
				{
					if($parent == '0')
					{
						$class = ' style="margin-left:0px; padding-left:0px;" class="news_category"';
					}
					else
					{
						$class = ' style="margin:2px;padding-left:15px;" class="news_subcategory"';
					}
					$v .= '<ul  '.$class.' >';						
							if($selectCategory == $topic_datas['id'])
							{
								$v .= '<li style="background-color:#D7D7D7">';
							}
							else
							{
								$v .= '<li>';
							}
							if(self::checkChild($topic_datas['id'],$group_id))
							{
								$v .= '<a href="'.$view->url(array('module'=>'Gallery','controller'=>'frontend','action'=>'categories','menu_id'=> $view->escape($menu_name),'parent' => $view->escape($topic_datas['category_title'])),'Gallery Category/:menu_id/:parent',false).'" >'.$view->escape($topic_datas['category_name']).'</a>';
							}
							else
							{
								$v .= '<a href="'.$view->url(array('module'=>'Gallery','controller'=>'frontend','action'=>'viewproduct','menu_id'=> $view->escape($menu_name),'parent' => $view->escape($topic_datas['category_title']),'group_id' => $view->escape($topic_datas['group_id'])),'Gallery Product/:menu_id/:parent/:group_id',false).'" >'.$view->escape($topic_datas['category_name']).'</a>';
							}
							$v .= '</li>';
							if(self::checkChild($topic_datas['id'],$group_id))
							{
								$v .= self::getSubCategory($topic_datas['id'],$view,$group_id,$selectCategory,$menu_name);
							}
						 $v .= '</ul>';
					
										
				}
				return $v;
			}
	}	
}