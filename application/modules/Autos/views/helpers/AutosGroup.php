<?php

class Autos_View_Helper_AutosGroup extends Zend_View_Helper_Abstract 
{	
	public function getNumOfAutos($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_category'), array('COUNT(*) AS num_cat'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_cat'];
		}
		return $num_cat;
	}
	
	public function getNumOfAutosType($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_business_type'), array('COUNT(*) AS num_type'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_type = $row['num_type'];
		}
		return $num_type;
	}
	
	public function getNumOfArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'cities'), array('COUNT(*) AS num_area'))
					   ->where('g.state_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_area = $row['num_area'];
		}
		return $num_area;
	}
	
	public function getNumOfProForArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.area_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfAutosForType($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.autos_type = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfAutosForBrand($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.brand_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfProduct($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'autos_page'), array('COUNT(*) AS num_product'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_product = $row['num_product'];
		}
		return $num_product;
	}
	
}
