<?php

class Autos_Form_AreaForm extends Zend_Form {

    protected $_editor;

    public function __construct($options = null) {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(APPLICATION_PATH . '/modules/Autos/forms/source/' . $translator->getLangFile() . '.AreaForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/Autos/forms/source/' . $translator->getLangFile() . '.AreaForm.ini', 'area') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/Autos/forms/source/en_US.AreaForm.ini', 'area');
        parent::__construct($config->area);
    }

    public function init() {
        $this->createForm();
    }

    public function createForm() {
        $this->elementDecorator();
        $this->loadAreas($this->state_id);
    }

    //Element Decorator
    private function elementDecorator() {
        $this->setElementDecorators(array(
            'ViewHelper', 'FormElements',
        ));
    }

    public function loadAreas($element) {
        $translator = Zend_Registry::get('translator');
        $element->setRegisterInArrayValidator(false);
        $selected = '';
        $states = new Autos_Model_DbTable_State();
        $states_options = $states->getSelectOptions();
        $element->addMultiOption('', $translator->translator('autos_select_state'));
        foreach ($states_options as $key => $value) {
            if ($selected == $key) {
                $element->addMultiOption($key, $value);
                $element->setValue($selected);
            } else {
                $element->addMultiOption($key, $value);
            }
        }
    }

}
