<?php

class Autos_Form_AutosGroupForm  extends Zend_Form 
{
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.AutosGroupForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.AutosGroupForm.ini', 'autos_group') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/en_US.AutosGroupForm.ini', 'autos_group');
            parent::__construct($config->autos );
        }

        public function init()
        {
            $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
		 	$this->elementDecorator();
			$this->loadUserGroup ();
			$this->loadReviewGroup ();
			$this->loadDynamicForms ();
			$this->file_type->setRegisterInArrayValidator(false);
			$this->doSecurityFiltering();		
        }
		
		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}		
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array('ViewHelper','FormElements'));
		}
		
		private function loadDynamicForms ()
		{			
			$dynamicForm = new Members_Model_DbTable_Forms();			
        	$dynamicForm_options = $dynamicForm->getAllForms();
			if($dynamicForm_options)
			{
				$this->dynamic_form->addMultiOptions($dynamicForm_options);		
			}							 
		}
		
		private function loadReviewGroup ()
		{			
			$reviewGroup = new Review_Model_DbTable_Setting();			
        	$reviewGroup_options = $reviewGroup->getAllReviews();
			$this->review_id->addMultiOptions($reviewGroup_options);									 
		}
		
		private function loadUserGroup ()
		{			
			$userGroup = new Members_Model_DbTable_Role();			
                        $userGroup_options = $userGroup->getOptions();
			$this->role_id->addMultiOptions($userGroup_options);									 
		}	
		
}