<?php
class Autos_Form_SparePartsForm  extends Zend_Form {

		protected $_editor;
		protected $_options;
		protected $_form_info;
		protected $_tagsAllowed;
		protected $_allowAttribs;
		
		public function __construct($options = null) 
		{	
			$translator = Zend_Registry::get('translator');
			$this->_options = $options;				
            $config = (file_exists( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.SparePartsForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.SparePartsForm.ini', 'autos') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/en_US.SparePartsForm.ini', 'autos');
            parent::__construct($config->autos ); 
        }

        public function init()
        {	
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();				 	
				
			 //$this->loadCountries($this->country_id);	
			// $this->loadStates($this->state_id);	
			// $this->loadArea($this->area_id);
			 //$this->loadBusinessType($this->autos_type);
			 $this->loadAutosAgent($this->autos_agent);
			 $this->loadRelatedItems ();
			 //$this->setElementTrueValue($this->equipment_installed);
			 //$this->setElementTrueValue($this->equipment_options);
			 $this->dynamicElements(); 
			
			 if(!empty($this->_tagsAllowed[0]) && !empty($this->_form_info))
			 {
				$this->setElementFilters(array('StringTrim', new Zend_Filter_StripTags(array('allowTags' => $this->_tagsAllowed,'allowAttribs' => $this->_allowAttribs))));	
			 }	
			 $this->doSecurityFiltering();
        }

		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}
		
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}
		
		public function loadRelatedItems ()
		{	
			$this->autos_id->setRegisterInArrayValidator(false);			
			$relatedItems = new Autos_Model_DbTable_Autos();			
        	$relatedItems_options = $relatedItems->getRelatedItems();	
			if($relatedItems_options)
			{
				$this->autos_id->setMultiOptions($relatedItems_options);	
			}								 
		}
		
		//Dynamic Elements
		private function dynamicElements()
		{
			if(!empty($this->_options))
			{
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($this->_options['form_id']);
				$this->_form_info = $form_info;
				if(!empty($this->_form_info))
				{
					$this->_tagsAllowed = explode(',', $this->_form_info['support_tags']); 
					$this->_allowAttribs = explode(',', $this->_form_info['support_attribs']);
				}
				try
				{
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($this->_options['form_id']); 
					if($field_groups)
					{
						foreach($field_groups as $group)
						{
							$group_arr = array();
							$group_name = $group->field_group;
							$field_info = $field_db->getFieldsInfo($this->_options['form_id'],$group_name);	
							foreach($field_info as $fields)
							{						
								$group_arr[$fields->field_name] = $fields->field_name;
								$required = ($fields->required == '1' && $fields->display_admin == '1' && $fields->display_frontend == '1') ? true : false;
								$checked  = ($fields->field_default_value) ? true : false;
								$multiOptions = explode(',',$fields->field_option);
								$multiOptions_arr =	$this->getMultiOptionsValueSerialize($fields);
								
								switch($fields->field_type)
								{
									case 'text':
										if($fields->regexpr)
										{
											if(preg_match("/^[a-zA-Z0-9_]+$/",$fields->regexpr))
											{
												$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value,
																'validators' => array($fields->regexpr)
																));
											}
											else
											{			
												$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value));
												$new_element = $this->getElement($fields->field_name);
												$new_element->addValidator('regex', false, array($fields->regexpr));
											}
										}
										else
										{
											$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
										}
										break;
									case 'file':								
											$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required));
											$this->setFileDecorators($fields->field_name);
										break;
									case 'textarea':
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'cols' => $fields->field_width, 'rows' => $fields->field_height, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'info' => $fields->field_desc, 'frontend' => $fields->display_frontend, 'required'    => $required, 'value' => $fields->field_default_value));
										break;
									case 'hidden':
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
									break;
									case 'radio':
										$this->addElement($fields->field_type, $fields->field_name,
														array('label' 			=> 	$fields->field_label, 
															  'id' 				=> 	$fields->field_id,
															  'required'    	=> 	$required,
															  'rel' 			=> 	$fields->id,
															  'admin' 			=> 	$fields->display_admin, 
															  'frontend' 		=> 	$fields->display_frontend,
															  'info' 			=> 	$fields->field_desc,
															  'class' 			=> 	$fields->field_class, 
															  'title' 			=> 	$fields->field_title,
															  'value' 			=> 	$fields->field_default_value,
														  	  'separator' 		=> 	$fields->field_separator,
															  'multiOptions' 	=> 	$multiOptions_arr
															));
												$this->setElementDecorator($fields->field_name);
										break;							
									case 'checkbox':
										$this->addElement($fields->field_type, $fields->field_name,
														array('label' 			=> 	$fields->field_label, 
															  'id' 				=> 	$fields->field_id,
															  'required'    	=> 	$required,
															  'rel' 			=> 	$fields->id,
															  'admin' 			=> 	$fields->display_admin, 
															  'frontend' 		=> 	$fields->display_frontend,
															  'info' 			=> 	$fields->field_desc,
															  'class' 			=> 	$fields->field_class, 
															  'title' 			=> 	$fields->field_title,
															  'checkedValue' 	=> 	$multiOptions[0],
															  'uncheckedValue' 	=> 	$multiOptions[1],
															  'checked' 		=>  $checked
															));
													$this->setElementDecorator($fields->field_name);
										break;
									case 'multiCheckbox':								
									$dfault_value_arr = explode(',',$fields->field_default_value);
									$this->addElement($fields->field_type, $fields->field_name,
													array('label' 			=> 	$fields->field_label, 
														  'id' 				=> 	$fields->field_id,
														  'required'    	=> 	$required,
														  'rel' 			=> 	$fields->id,
														  'admin' 			=> 	$fields->display_admin, 
														  'frontend' 		=> 	$fields->display_frontend,
														  'info' 			=> 	$fields->field_desc,
														  'class' 			=> 	$fields->field_class, 
														  'title' 			=> 	$fields->field_title,
														  'separator' 		=> 	$fields->field_separator,
														  'multiOptions' 	=> 	$multiOptions_arr,
														  'value' 			=>  $dfault_value_arr
														));
										$this->setElementDecorator($fields->field_name);
										$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
									break;
									case 'select':
										$this->addElement($fields->field_type, $fields->field_name,
														array('label' 			=> $fields->field_label, 
															  'id' 				=> $fields->field_id,
															  'class' 			=> $fields->field_class, 
															  'title' 			=> $fields->field_title,
															  'size' 			=> $fields->field_width,
															  'rel' 			=> $fields->id,
															  'admin' 			=> $fields->display_admin, 
															  'frontend' 		=> $fields->display_frontend,
															  'info' 			=> 	$fields->field_desc,
															  'required'   		=> $required,
															  'value' 			=> $fields->field_default_value,
															  'multiOptions' 	=> $multiOptions_arr
															));
													$this->setElementDecorator($fields->field_name);
													  $this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
													  $this->loadCountriesDynamic ($fields->field_name);
										break;
									case 'multiselect':								
									$dfault_value_arr = explode(',',$fields->field_default_value);
									$this->addElement($fields->field_type, $fields->field_name,
													array('label' 			=> $fields->field_label, 
														  'id' 				=> $fields->field_id,
														  'class' 			=> $fields->field_class, 
														  'title' 			=> $fields->field_title,
														  'size' 			=> $fields->field_height,
														  'rel' 			=> $fields->id,
														  'admin' 			=> $fields->display_admin, 
														  'info' 			=> 	$fields->field_desc,
														  'frontend' 		=> $fields->display_frontend,
														  'required'   		=> $required,
														  'value' 			=> $dfault_value_arr,
														  'multiOptions' 	=> $multiOptions_arr
														));
										$this->setElementDecorator($fields->field_name);
										$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
									break;
								}
							}
							$this->addDisplayGroup($group_arr, $group_name,array('disableLoadDefaultDecorators' => true,'title' => $group_name, 'legend' => $group_name));								
						}		
					}		
				}
				catch(Exception $e)
				{
					throw new Exception($e->getMessage());				
				}
			}
		}
		
		private function setElementTrueValue($element)
		{
			$element->setRegisterInArrayValidator(false);			
			$options = $element->getMultiOptions();
			$element->clearMultiOptions();
			foreach($options as $key=>$value)
			{
				if($key == '_')
				{
					$element->addMultiOption('',$value);
				}
				else
				{
					$element->addMultiOption($value,$value);
				}
			}
			$element->setRegisterInArrayValidator(false);
		}
		
		//Each Element Decorator
		private function setElementDecorator($field_name)
		{
			$element = $this->getElement($field_name);
			$element->setDecorators(array(
					'ViewHelper','FormElements',										
					
				));
		} 
		
		private function setFileDecorators($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_File')
			{
				$element->setDecorators(array('file','file'));			
				$element->setMaxFileSize($this->_options['attach_file_max_size']);
			}			
		}
		
		public function getFormInfo ()
		{			
			return $this->_form_info;										 
		}
		
		public function loadCountriesDynamic ($element_name)
		{			
			$element = $this->getElement($element_name);
			$type = $element->getType();
			$element_name_arr = explode('_',$element_name);
			if(in_array(Eicra_File_Constants::COUNTRY, $element_name_arr) && $type == 'Zend_Form_Element_Select')
			{	
				$translator = Zend_Registry::get('translator');			
				$global_conf = Zend_Registry::get('global_conf');
				$countries = new Eicra_Model_DbTable_Country();
				$countries_options = $countries->getOptions();
				$selected = $global_conf['default_country'];
				$option = array('' => $translator->translator('common_select_country'));
				$element->setMultiOptions($option);
				foreach($countries_options as $key=>$value)
				{
					if($selected == $key)
					{
						$element->addMultiOption($value,$value);
						$element->setValue($value);
					}
					else
					{
						$element->addMultiOption($value,$value);
					}
				}
			}								 
		}
		
		//Get Multioptions
		private function getMultiOptionsValueSerialize($fields)
		{
			if($fields->field_type == 'select' || $fields->field_type == 'multiselect')
			{
				if(strpos($fields->field_option,';;') && strpos($fields->field_option,'::'))
				{
					$main_arr = explode(';;', $fields->field_option);
					if(!empty($main_arr[0]))
					{
						$multiOptions_arr = array();					
						foreach($main_arr as $main_arr_key => $main_arr_value)
						{
							$group_arr = explode('::', $main_arr_value);
							if(!empty($group_arr[0]) && !empty($group_arr[1]))
							{
								$value_arr = explode(',', $group_arr[1]);
								$options_arr = array();
								foreach($value_arr as $value_arr_key=>$value_arr_value)
								{
									$options_arr[$value_arr_value] = $value_arr_value;
								}							
								$multiOptions_arr[$group_arr[0]] = $options_arr;
							}
						}
					}
					else
					{
						$multiOptions = explode(',',$fields->field_option);
						$multiOptions_arr = array();
						foreach($multiOptions as $key=>$value)
						{
							$multiOptions_arr[$value] = $value;
						}
					}
				}
				else
				{
					$multiOptions = explode(',',$fields->field_option);
					$multiOptions_arr = array();
					foreach($multiOptions as $key=>$value)
					{
						$multiOptions_arr[$value] = $value;
					}
				}
			}
			else
			{
				$multiOptions = explode(',',$fields->field_option);
				$multiOptions_arr = array();
				foreach($multiOptions as $key=>$value)
				{
					$multiOptions_arr[$value] = $value;
				}
			}
			return $multiOptions_arr;
		} 
		
		public function loadAutosBrand ()
		{			
			$autosBrand = new Autos_Model_DbTable_Brand();			
        	$autosBrand_options = $autosBrand->getAllBrand();	
			$translator = Zend_Registry::get('translator');		
			$this->brand_id->addMultiOption('',$translator->translator('autos_brand_select'));
			$this->brand_id->addMultiOptions($autosBrand_options);									 
		}
		
		public function loadAutosGroup ()
		{			
			$autosGroup = new Autos_Model_DbTable_AutosGroup();			
        	$autosGroup_options = $autosGroup->getGroupInfo();	
			$translator = Zend_Registry::get('translator');		
			$this->group_id->addMultiOption('',$translator->translator('autos_group_add_edit_select'));
			$this->group_id->addMultiOptions($autosGroup_options);									 
		}
		
		public function loadAutosAgent ($element)
		{			
			$memberList = new Members_Model_DbTable_MemberList();			
        	$memberList_options = $memberList->getAllMembers();	
			$translator = Zend_Registry::get('translator');
			$element->setRegisterInArrayValidator(false);			
			$element->addMultiOption('',$translator->translator('autos_agent_select'));
			$element->addMultiOptions($memberList_options);	 								 
		}
		
		public function loadBusinessType ($element)
		{
			$translator = Zend_Registry::get('translator');			
			$element->setRegisterInArrayValidator(false);		
			$element->addMultiOption('',$translator->translator('autos_select_businessType'));											 
		}
		
		public function loadCountries($element)
		{
			$translator = Zend_Registry::get('translator');
			$element->setRegisterInArrayValidator(false);
			$global_conf = Zend_Registry::get('global_conf');
			$countries = new Eicra_Model_DbTable_Country();
        	$countries_options = $countries->getOptions();
			$selected = $global_conf['default_country'];
			$element->addMultiOption('0',$translator->translator('autos_select_country'));
			foreach($countries_options as $key=>$value)
			{
				if($selected == $key)
				{
					$element->addMultiOption($key,$value);
					$element->setValue($selected);
				}
				else
				{
					$element->addMultiOption($key,$value);
				}
			}						 
		}
		
		public function loadStates ($element)
		{
			$translator = Zend_Registry::get('translator');
			$element->setRegisterInArrayValidator(false);
			$global_conf = Zend_Registry::get('global_conf');
			$country_id = $global_conf['default_country'];
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);			
			$element->addMultiOption('0',$translator->translator('autos_select_state'));
			$element->addMultiOptions($states_options);									 
		}
		
		public function loadArea ($element)
		{	
			$translator = Zend_Registry::get('translator');				
			$element->setRegisterInArrayValidator(false);
			$element->addMultiOption('0',$translator->translator('autos_select_area'));											 
		} 
						
		
		
		public function setEditor($baseURL)
		{
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "none",
								theme : "advanced",
								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "insertimage,image,media,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,insertdate,inserttime,preview,print,|,ltr,rtl",
								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help",
								theme_advanced_buttons3 : "tablecontrols,hr,removeformat,visualaid,sub,sup,charmap,emotions,advhr",
								theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,|,styleprops,del,ins,attribs,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor,|,fullscreen",
								theme_advanced_buttons5 : "fontsizeselect,formatselect,fontselect,styleselect,|,code,iespell",
								theme_advanced_toolbar_location : "top",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								
								
								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type],style[type|title|disabled|media],script[type|src]",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
        						remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});
							function loadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceAddControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\';
								
							}
							function unloadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceRemoveControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\';
							}
						</script>';
		}
		
		public function getEditor()
		{
			echo $this->_editor;
		} 	
		  
}