<?php
class Autos_Form_BrandForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.BrandForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.BrandForm.ini', 'brand') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/en_US.BrandForm.ini', 'brand');
            parent::__construct($config->brand );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();		
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}		 	  
}