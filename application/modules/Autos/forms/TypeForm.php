<?php

class Autos_Form_TypeForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.TypeForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/'.$translator->getLangFile().'.TypeForm.ini', 'type') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Autos/forms/source/en_US.TypeForm.ini', 'type');
            parent::__construct($config->type );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->loadAutosGroup();	
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}
		
		public function loadAutosGroup ()
		{			
			$autosGroup = new Autos_Model_DbTable_AutosGroup();			
        	$autosGroup_options = $autosGroup->getGroupInfo();	
			$translator = Zend_Registry::get('translator');		
			$this->group_id->addMultiOption('',$translator->translator('autos_group_add_edit_select'));
			$this->group_id->addMultiOptions($autosGroup_options);									 
		}
		 	  
}