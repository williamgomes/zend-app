<?php

class Autos_BackendController extends Zend_Controller_Action {

    private $CategoryForm;
    private $uploadForm;
    private $_controllerCache;
    private $_auth_obj;
    private $translator;

    public function init() {
        /* Initialize action controller here */
        $this->CategoryForm = new Autos_Form_CategoryForm ();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');

        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = Zend_Registry::get('config')->eicra->params->domain . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);

            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    //CATEGORY LIST FUNCTION

    public function listAction() {
        /* $group_id = $this->getRequest()->getParam('group_id');
          // action body
          $pageNumber = $this->getRequest()->getParam('page');
          $getViewPageNum = $this->getRequest()->getParam('viewPageNum');
          $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

          if(empty($getViewPageNum) && empty($viewPageNumSes))
          {
          $viewPageNum = '30';
          }
          else if(!empty($getViewPageNum) && empty($viewPageNumSes))
          {
          $viewPageNum = $getViewPageNum;
          }
          else if(empty($getViewPageNum) && !empty($viewPageNumSes))
          {
          $viewPageNum = $viewPageNumSes;
          }
          else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
          {
          $viewPageNum = $getViewPageNum;
          }

          Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
          $uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
          if( ($datas = $this->_controllerCache->load($uniq_id)) === false )
          {
          $categories = new Autos_Model_CategoryListMapper();
          $datas =  $categories->fetchAll($pageNumber,$group_id);
          $this->_controllerCache->save($datas, $uniq_id);
          }
          $this->view->datas =  $datas;
          $this->view->group_id = $group_id; */

        // action body
        $group_id = $this->_request->getParam('group_id');
        $this->view->group_id = $group_id;

        $group_db = new Autos_Model_DbTable_AutosGroup();
        $group_info = ($group_id) ? $group_db->getGroupName($group_id) : null;
        $this->view->group_info = $group_info;

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');

                if ($group_id) {
                    $posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_request->getParam('group_id'));
                }
                $posted_data['hasChild'] = false;

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'group_id' => $group_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $list_mapper = new Autos_Model_CategoryListMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['autos_num_format'] = $this->view->numbers($entry_arr['autos_num']);
                            $entry_arr['publish_status_category_name'] = str_replace('_', '-', $entry_arr['category_name']);
                            $entry_arr['view_title_format'] = $this->translator->translator('autos_click_view_item', $this->view->escape($entry_arr['category_name']));
                            $entry_arr['cat_date_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['cat_date']));
                            $entry_arr['has_child'] = $list_mapper->getDbTable()->checkSubCategory($entry['id']);
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    //CATEGORY FUNCTIONS

    public function parentAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        $id = substr($this->_getParam('id'), 5);
        $parent_id = substr($this->_getParam('to'), 5);

        if ($parent_id != '' && !empty($id)) {
            //DB Connection
            $conn = Zend_Registry::get('msqli_connection');
            $conn->getConnection();

            // Update Menu Parent
            $where = array();
            $where[] = 'id = ' . $conn->quote($id);
            try {
                $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('parent' => $parent_id), $where);
                $result = array('status' => 'ok');
            } catch (Exception $e) {
                $result = array('status' => 'err', 'msg' => $e->getMessage());
            }
            if ($result['status'] == 'ok') {
                $OrdersObj = new Autos_Controller_Helper_CategoryOrders($id);
                $OrdersObj->setNewOrder($id);
                $json_arr = array('status' => 'ok', 'id' => $id, 'parent_id' => $parent_id);
            } else {
                $msg = $translator->translator('category_change_parent_err');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function autosgroupAction() {
        if ($this->_request->isPost()) {
            $group_id = $this->_request->getPost('grp_id');

            $autosGroup = Autos_View_Helper_Categorytree::getSubCategory('0', $this->view, $group_id, null);

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');

            if ($autosGroup) {
                $json_arr = array('status' => 'ok', 'autosGroup' => $autosGroup);
            } else {
                $msg = $translator->translator('autos_group_err');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function uploadfileAction() {
        $theme = Zend_Registry::get('jtheme');
        $this->view->theme = $theme;
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        $group_id = $this->_getParam('group_id', 0);
        $file_content = ($this->_getParam('file_content', 0)) ? $this->_getParam('file_content', 0) : null;

        //Put Group Information in the form
        $group_info = new Autos_Model_DbTable_AutosGroup();
        $option = $group_info->getGroupName($group_id);
        $this->uploadForm = new Autos_Form_UploadForm($option);

        $path = 'data/frontImages/autos/category_thumb';

        if ($this->_request->getPost()) {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->uploadForm->isValid($this->_request->getPost())) {
                Members_Controller_Helper_FileRename::fileRename($this->uploadForm->upload_file, $path);

                $Filename = $this->uploadForm->upload_file->getFileName();
                $ext = Eicra_File_Utility::GetExtension($Filename);
                $file_obj = new Autos_Controller_Helper_File($path, $Filename, $ext, $option);
                if ($file_obj->checkCategoryThumbExt()) {
                    if ($this->uploadForm->upload_file->receive()) {
                        $msg = $translator->translator('File_upload_success');
                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'img_date' => date("Y-m-d H:i:s", filemtime(APPLICATION_PATH . realpath('../' . $path . '/' . $Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null, false));
                    } else {
                        $validatorMsg = $this->uploadForm->upload_file->getMessages();
                        $vMsg = implode("\n", $validatorMsg);
                        $json_arr = array('status' => 'err', 'msg' => $vMsg, 'newName' => $this->uploadForm->upload_file->getFileName(null, false));
                    }
                } else {
                    $msg = $translator->translator('File_upload_ext_err', $ext);
                    $json_arr = array('status' => 'err', 'msg' => $msg, 'newName' => $this->uploadForm->upload_file->getFileName(null, false));
                }
            } else {
                $validatorMsg = $this->uploadForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'err', 'msg' => $vMsg);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->group_id = $group_id;
            $this->view->file_content = $file_content;
            $this->view->group_info = $option;
            $this->view->upload_path = $path;
            $this->view->uploadForm = $this->uploadForm;
            $this->render();
        }
    }

    public function deletefileAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $this->view->translator = $translator;

        if ($this->_request->isPost()) {
            $file_info = $this->_request->getPost('file_info');
            if (empty($file_info)) {
                $msg = $translator->translator("insert_selected_file_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            } else {
                $each_file_arr = explode('; ', $file_info);
                $deleted_file_name = '';
                foreach ($each_file_arr as $key => $each_file) {
                    $file_info_arr = explode(',', $each_file);
                    if ($file_info_arr[1]) {
                        $dir = $file_info_arr[0] . DS . $file_info_arr[1];
                        $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                    }

                    if ($res) {
                        $deleted_file_name .= $file_info_arr[1] . ', ';
                        $msg = $translator->translator("file_delete_success", $deleted_file_name);
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $translator->translator("file_delete_err", $file_info_arr[1]);
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                        break;
                    }
                }
            }
        } else {
            $msg = $translator->translator("file_delete_err");
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function addAction() {
        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            if ($this->CategoryForm->isValid($this->_request->getPost())) {
                $auth = Zend_Auth::getInstance();
                $stat = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article == '1') ? '1' : '0';
                $catagories = new Autos_Model_Catagories($this->CategoryForm->getValues());
                $catagories->setActive($stat);
                $catagories->setFeatured('0');


                $perm = new Autos_View_Helper_Allow();
                if ($perm->allow()) {
                    $result = $catagories->saveCategory();
                    if ($result['status'] == 'ok') {
                        $msg = $translator->translator("category_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $msg = $translator->translator("category_save_err");
                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                    }
                } else {
                    $Msg = $translator->translator("category_add_action_deny_desc");
                    $json_arr = array('status' => 'errP', 'msg' => $Msg);
                }
            } else {
                $validatorMsg = $this->CategoryForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->catForm = $this->CategoryForm;
            $this->render();
        }
    }

    public function editAction() {
        $id = $this->_getParam('id', 0);
        $translator = $this->translator;
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

        //DB Connection
        $conn = Zend_Registry::get('msqli_connection');
        $conn->getConnection();

        //Get Category Info
        $categoryData = new Autos_Model_DbTable_Category();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();


            if ($this->CategoryForm->isValid($this->_request->getPost())) {
                $id = $this->_request->getPost('id');
                $parent_id = $this->_request->getPost('parent');
                if (!empty($id)) {
                    if ($id != $parent_id) {
                        $categoryOptions = new Autos_Model_DbTable_Category();
                        $idString = $categoryOptions->getAllSubCategoryId($id);
                        $id_arr = explode(',', $idString);
                        if (!in_array($parent_id, $id_arr, true)) {
                            $categoryInfo = $categoryData->getCategoryInfo($id);
                            $catagories = new Autos_Model_Catagories($this->CategoryForm->getValues());
                            $catagories->setPrev_parent($categoryInfo['parent']);
                            $catagories->setPrev_group($categoryInfo['group_id']);
                            $catagories->setId($id);
                            $catagories->setCategory_title($this->CategoryForm->getValue('category_title'), $id);

                            $perm = new Autos_View_Helper_Allow();
                            if ($perm->allow()) {
                                $result = $catagories->saveCategory();
                                if ($result['status'] == 'ok') {
                                    $msg = $translator->translator("category_save_success");
                                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                                } else {
                                    $msg = $translator->translator("category_save_err");
                                    $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                                }
                            } else {
                                $Msg = $translator->translator("category_add_action_deny_desc");
                                $json_arr = array('status' => 'errP', 'msg' => $Msg);
                            }
                        } else {
                            $Msg = $translator->translator("category_child_not_parent");
                            $json_arr = array('status' => 'err', 'msg' => $Msg);
                        }
                    } else {
                        $msg = $translator->translator("category_same_parent_desc");
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                    }
                } else {
                    $msg = $translator->translator("category_save_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $validatorMsg = $this->CategoryForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $categoryInfo = $categoryData->getCategoryInfo($id);
            if ($categoryInfo) {
                $category_thumb = $categoryInfo['category_thumb'];
                $group_id = $categoryInfo['group_id'];
                $parent = $categoryInfo['parent'];
                if ($parent == '0') {
                    $selected = 'style="background-color:#D7D7D7"';
                    $parent_name = $translator->translator('autos_tree_root');
                } else {
                    $select = $conn->select()->from(array('gc' => Zend_Registry::get('dbPrefix') . 'autos_category'), array('gc.category_name'))->where('id = ?', $parent);
                    $rs = $select->query()->fetchAll();
                    foreach ($rs as $row) {
                        $parent_name = $row['category_name'];
                    }
                    $selected = '';
                }


                $this->view->categoryTree = '<table class="example" id="dnd-example">' .
                        '<tbody ><tr id="node-0">' .
                        '<td ' . $selected . '>' .
                        '<span class="folder">' . $translator->translator('autos_tree_root') . '</span>' .
                        '</td>' .
                        '</tr>' . Autos_View_Helper_Categorytree::getSubCategory('0', $this->view, $group_id, $parent) . '</tbody></table>';

                $this->CategoryForm->populate($categoryInfo);
                $this->view->id = $id;
                $this->view->parent_id = $parent;
                $this->view->parent_name = $parent_name;
                $this->view->category_thumb = $category_thumb;
                $this->view->catForm = $this->CategoryForm;
                $this->render();
            } else {
                $this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
            }
        }
    }

    public function categorytreeAction() {
        if ($this->_request->isPost()) {
            $group_id = $this->_request->getPost('grp_id');
            if ($this->_request->getPost('parent')) {
                $parent = $this->_request->getPost('parent');
            } else {
                $parent = null;
            }

            $categoryTree = Autos_View_Helper_Categorytree::getSubCategory('0', $this->view, $group_id, $parent);

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');

            if ($categoryTree) {
                $json_arr = array('status' => 'ok', 'categoryTree' => $categoryTree);
            } else {
                $msg = $translator->translator('cat_tree_err');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function deleteAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id = $this->_request->getPost('id');

            //cat_info
            $catObj = new Autos_Model_DbTable_Category();
            $cat_info = $catObj->getCategoryInfo($id);

            $category_name = $cat_info['category_name'];
            $category_thumb = $cat_info['category_thumb'];

            //DB Connection
            $conn = Zend_Registry::get('msqli_connection');
            $conn->getConnection();

            //Get It's Child category_order
            $OrdersObj = new Autos_Controller_Helper_CategoryOrders($id);
            $parent_id = $OrdersObj->getParent();
            $select = $conn->select()
                    ->from(array('gc' => Zend_Registry::get('dbPrefix') . 'autos_category'), array('gc.id'))
                    ->where('gc.parent = ?', $id);
            $rs = $select->query()->fetchAll();
            if ($rs) {
                foreach ($rs as $row) {
                    $sub_id = (int) $row['id'];
                    $OrdersObj->setNewOrder($sub_id);
                }
            }

            //update parents of it's child
            $whereP = array();
            $whereP[] = 'parent = ' . $conn->quote($id);
            $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('parent' => $parent_id), $whereP);

            //Update category_id of the product of this category to zero
            $whereA = array();
            $whereA[] = 'category_id = ' . $conn->quote($id);
            $conn->update(Zend_Registry::get('dbPrefix') . 'autos_page', array('category_id' => '0'), $whereA);

            // Remove from Category
            $where = array();
            $where[] = 'id = ' . $conn->quote($id);
            try {
                $conn->delete(Zend_Registry::get('dbPrefix') . 'autos_category', $where);
                $file_obj = new Autos_Controller_Helper_File('data/frontImages/autos/category_thumb/', $category_thumb);
                if ($file_obj->cleanFile()) {
                    $msg = $translator->translator('category_list_delete_success', $category_name);
                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                } else {
                    $msg = $translator->translator('category_list_file_delete_success', $category_name);
                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                }
            } catch (Exception $e) {
                $msg = $translator->translator('category_list_delete_err', $category_name);
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id_str = $this->_request->getPost('id_st');
            $perm = new Autos_View_Helper_Allow();
            if ($perm->allow('delete', 'backend', 'Autos')) {
                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    foreach ($id_arr as $id) {
                        //Get It's Child category_order
                        $OrdersObj = new Autos_Controller_Helper_CategoryOrders($id);
                        $parent_id = $OrdersObj->getParent();
                        $select = $conn->select()
                                ->from(array('gc' => Zend_Registry::get('dbPrefix') . 'autos_category'), array('gc.id'))
                                ->where('gc.parent = ?', $id);
                        $rs = $select->query()->fetchAll();
                        if ($rs) {
                            foreach ($rs as $row) {
                                $sub_id = (int) $row['id'];
                                $OrdersObj->setNewOrder($sub_id);
                            }
                        }

                        //update parents of it's child
                        $whereP = array();
                        $whereP[] = 'parent = ' . $conn->quote($id);
                        $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('parent' => $parent_id), $whereP);

                        //Update category_id of the product of this category to zero
                        $whereA = array();
                        $whereA[] = 'category_id = ' . $conn->quote($id);
                        $conn->update(Zend_Registry::get('dbPrefix') . 'autos_page', array('category_id' => '0'), $whereA);

                        //Get Category Thumbnil
                        $select = $conn->select()
                                ->from(array('gc' => Zend_Registry::get('dbPrefix') . 'autos_category'), array('gc.category_thumb'))
                                ->where('gc.id = ?', $id);
                        $rs = $select->query()->fetchAll();
                        if ($rs) {
                            foreach ($rs as $row) {
                                $category_thumb = $row['category_thumb'];
                            }
                        }

                        // Remove from Category
                        $where = array();
                        $where[] = 'id = ' . $conn->quote($id);
                        try {
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'autos_category', $where);
                            $file_obj = new Autos_Controller_Helper_File('data/frontImages/autos/category_thumb/', $category_thumb);
                            if ($file_obj->cleanFile()) {
                                $msg = $translator->translator('category_list_delete_success');
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator('category_list_file_delete_success');
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            }
                        } catch (Exception $e) {
                            $msg = $translator->translator('category_list_delete_err');
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('category_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id = $this->_request->getPost('id');
            $category_name = $this->_request->getPost('category_name');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            //DB Connection
            $conn = Zend_Registry::get('msqli_connection');
            $conn->getConnection();

            // Update Article status
            $where = array();
            $where[] = 'id = ' . $conn->quote($id);
            try {
                $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('active' => $active), $where);
                $return = true;
            } catch (Exception $e) {
                $return = false;
            }
            if ($return) {
                $json_arr = array('status' => 'ok', 'active' => $active);
            } else {
                $msg = $translator->translator('category_list_publish_err', $category_name);
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);

                //DB Connection
                $conn = Zend_Registry::get('msqli_connection');
                $conn->getConnection();

                foreach ($id_arr as $id) {
                    // Update Article status
                    $where = array();
                    $where[] = 'id = ' . $conn->quote($id);
                    try {
                        $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('active' => $active), $where);
                        $return = true;
                    } catch (Exception $e) {
                        $return = false;
                    }
                }

                if ($return) {
                    $json_arr = array('status' => 'ok');
                } else {
                    $msg = $translator->translator('category_list_publish_err');
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator("category_selected_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }

        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function featuredAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $category_name = $this->_request->getPost('category_name');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'featured':
                    $feature = '1';
                    break;
                case 'unfeatured':
                    $feature = '0';
                    break;
            }

            //DB Connection
            $conn = Zend_Registry::get('msqli_connection');
            $conn->getConnection();

            // Update Article status
            $where = array();
            $where[] = 'id = ' . $conn->quote($id);
            try {
                $conn->update(Zend_Registry::get('dbPrefix') . 'autos_category', array('featured' => $feature), $where);
                $return = true;
            } catch (Exception $e) {
                $return = false;
            }
            if ($return) {
                $json_arr = array('status' => 'ok', 'featured' => $feature);
            } else {
                $msg = $translator->translator('category_list_feature_err', $category_name);
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function upAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getPost('id');
        $category_order = $this->_request->getPost('category_order');

        $OrderObj = new Autos_Controller_Helper_CategoryOrders($id);
        $returnV = $OrderObj->decreaseOrder();

        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }

        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function downAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getPost('id');
        $category_order = $this->_request->getPost('category_order');

        $OrderObj = new Autos_Controller_Helper_CategoryOrders($id);
        $returnV = $OrderObj->increaseOrder();
        if ($returnV['status'] == 'err') {
            $json_arr = array('status' => 'err', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        } else {
            $json_arr = array('status' => 'ok', 'id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
        }

        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function orderallAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $id_str = $this->_request->getPost('id_arr');
            $category_order_str = $this->_request->getPost('category_order_arr');

            $id_arr = explode(',', $id_str);
            $category_order_arr = explode(',', $category_order_str);

            $order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
            $checkOrder = $order_numbers->checkOrderNumbers($category_order_arr);
            if ($checkOrder['status'] == 'err') {
                $json_arr = array('status' => 'err', 'msg' => $checkOrder['msg']);
            } else {
                //Save Category Order
                $i = 0;
                foreach ($id_arr as $id) {
                    $OrderObj = new Autos_Controller_Helper_CategoryOrders($id);
                    $OrderObj->saveOrder($category_order_arr[$i]);
                    $i++;
                }
                $msg = $translator->translator("category_zero_order_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg);
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

}
