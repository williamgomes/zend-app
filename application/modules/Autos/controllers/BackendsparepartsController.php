<?php
class Autos_BackendsparepartsController extends Zend_Controller_Action
{
	private $SparePartsForm;
	private $uploadForm;
	private $_controllerCache;	
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {				
        /* Initialize action controller here */
		$this->SparePartsForm =  new Autos_Form_SparePartsForm ();
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
    }
	
	public function preDispatch() 
	{
		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
				
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');

		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{	
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}	
	}
	
	//PROPERTY LIST FUNCTION

    public function listAction()
    {
		$approve = $this->getRequest()->getParam('approve');	
		$this->view->approve = $approve;
		$featured = $this->getRequest()->getParam('featured');
		$this->view->featured = $featured;
						
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$list_mapper	=	new Autos_Model_SparepartsListMapper();				
				
				if($featured)
				{
					$posted_data['filter']['filters'][] = array('field' => 'featured', 'operator' => 'eq', 'value' => $this->_request->getParam('featured'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'featured'	=> $featured, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr 											= 	(!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr 											= 	is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']								=  	$this->view->numbers($entry_arr['id']);
								$entry_arr['spare_parts_base_price_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_base_price']);
								$entry_arr['spare_parts_promotion_price_format'] 	= 	$this->view->numbers($entry_arr['spare_parts_promotion_price']);
								$entry_arr['spare_parts_order_qty_min_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_order_qty_min']);
								$entry_arr['publish_status_spare_parts_name'] 		= 	str_replace('_', '-', $entry_arr['spare_parts_name']);
								$entry_arr['entry_date_lang_format']				=  	$this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
								$entry_arr['entry_date_format']						=  	$this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
								$img_thumb_arr 										= 	explode(',',$entry_arr['spare_parts_image']);								
								$entry_arr['spare_parts_image_format'] 				= 	($this->view->escape($entry_arr['spare_parts_image_primary'])) ? 'data/frontImages/autos/spare_parts_image/'.$this->view->escape($entry_arr['spare_parts_image_primary']) :  'data/frontImages/autos/spare_parts_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']							=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;						
								$view_datas['data_result'][$key]					=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}									
    }	
	
	public function savedAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Autos_Model_SavedSparePartsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_spare_parts_name'] = str_replace('_', '-', $entry_arr['spare_parts_name']);
								$entry_arr['spare_parts_base_price_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_base_price']);
								$entry_arr['spare_parts_promotion_price_format'] 	= 	$this->view->numbers($entry_arr['spare_parts_promotion_price']);
								$entry_arr['entry_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
								$entry_arr['entry_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
															
								$img_thumb_arr = explode(',',$entry_arr['spare_parts_image']);								
								$entry_arr['spare_parts_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/spare_parts_image/'.$this->view->escape($entry_arr['autos_primary_image']) :  'data/frontImages/autos/spare_parts_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}						
    }	
	
	//PROPERTY FUNCTIONS	
		
	/*public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Eicra_Model_DbTable_City();			
        	$cities_options = $cities->getOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function autosgroupAction()
	{	
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;	
		
		if ($this->_request->isPost()) 
		{
			$group_id = $this->_request->getPost('grp_id');	
			$parant = $this->_request->getPost('id');
			$expanded = ($this->_request->getPost('expanded')) ? $this->_request->getPost('expanded') : false;	
						
			$group_db = new Autos_Model_DbTable_SparepartsGroup();
			$group_info = $group_db->getGroupName($group_id);
			
			//$autosGroup = Autos_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,null);
			
			$businessType = new Autos_Model_DbTable_BusinessType();	
			$businessType_options = $businessType->getOptions($group_id);
			
			
			$dynamic_field_arr = $this->getDynamicfield($group_id);
			$param_fields = array(
								'table_name' => 'autos_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$group_info['id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);
			$treeDataSource = Autos_View_Helper_Categorytree::getTreeDataSource($parant,$this->view,$group_id,null,$expanded);
			
			if($treeDataSource)
			{			
				$json_arr = array('status' => 'ok','autosGroup' => $autosGroup, 'TreeDataSource' => $treeDataSource,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
			}
			else
			{
				$msg = $translator->translator('autos_group_err');	
				$json_arr = array('status' => 'err','msg' => $msg,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
			}	
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	private function getDynamicfield($group_id)
	{
		try
		{
			$translator = Zend_Registry::get('translator');
			$group_info = new Autos_Model_DbTable_SparepartsGroup();
			$option = $group_info->getGroupName($group_id);
			if($option['dynamic_form'])
			{
				$option['form_id'] = $option['dynamic_form'];
				$this->SparePartsForm = new Autos_Form_SparePartsForm ($option);
				$groupsObj = $this->SparePartsForm->getDisplayGroups();				
				if(!empty($groupsObj))
				{
					$dynamic_field_obj = array();	
					$group_key	= 0;			
					foreach($groupsObj as $group)
					{
						$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_TITLE] = $translator->translator($group->getAttrib('title'));
						$elementsObj =  $group->getElements();
						if($elementsObj)
						{
							$element_key = 0;
							foreach($elementsObj as  $element)
							{
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['name']		=	$element->getName();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['value']		=	$element->getValue();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['label']		=	$element->getLabel();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['required']	=	($element->isRequired()) ? true : false;
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['type']		= 	strtolower(str_replace('Zend_Form_Element_', '', $element->getType()));
																
								$elements_attributes = $element->getAttribs();
								if($elements_attributes)
								{
									foreach($elements_attributes as $attributes_key => $attribute_value)
									{
										$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key][$attributes_key]		=	$attribute_value;
									}
								}
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['element']		=		$element->render();
								$element_key++;								
							}
						}	
						$group_key++;					
					}
				}
				$dynamic_field_arr = array('status' => 'ok', 'dynamic_fields' => $dynamic_field_obj, 'form_info' => $this->SparePartsForm->getFormInfo());
			}
			else
			{
				$dynamic_field_arr = array('status' => 'err', 'dynamic_fields' => null);
			}
		}
		catch(Exception $e)
		{
			$dynamic_field_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'dynamic_fields' => null);
		}
		return $dynamic_field_arr;
	}*/
	
	public function addAction()
	{			
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			//Checking Membership Package Option
			$packages = new Members_Controller_Helper_Packages();	
			if($packages->isPackage())
			{
				$module = $this->_request->getModuleName();
				$controller = $this->_request->getControllerName();
				$action = $this->_request->getActionName();
				if($packages->isPackageField($module,$controller,$action))
				{
					$package_no = $packages->getPackageFieldValue($module,$controller,$action);				
				}
				else
				{
					$package_no = 0;
				}
			}
			else
			{
				$package_no = 0;
			}		
			$autos_db = new Autos_Model_DbTable_Spareparts();
			$p_num = $autos_db->numOfSpareParts();
			if(empty($package_no) || ($p_num < $package_no))
			{					
				if($this->SparePartsForm->isValid($this->_request->getPost())) 
				{	
					try
					{
						
						$autos = new Autos_Model_Spareparts($this->SparePartsForm->getValues());
						$auth = $this->view->auth;
						$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';	
						$autos->setActive($stat);
						$autos->setFeatured('0');
						$autos->setSpare_parts_image_primary($this->_request->getPost('spare_parts_image_primary'));												
						$autos->setAutos_id($this->_request->getPost('autos_id'));									
						
						if($this->view->allow())
						{
							$result = $autos->saveSpareparts();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);								
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}						
						}
						else
						{
							$Msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $Msg);
						}
					}
					catch(Exception $e)
					{
						$json_arr = array('status' => 'err','msg' => $e->getMessage());
					}				
				}
				else
				{
					$validatorMsg = $this->SparePartsForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}	
			}
			else
			{
				$msg = $translator->translator("autos_add_limit_err",$package_no);
				$json_arr = array('status' => 'err','msg' => $msg);
			}					
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{						
			$this->view->SparePartsForm = $this->SparePartsForm;
			$group_db = new Autos_Model_DbTable_AutosGroup();
			$group_info = $group_db->getAllGroupInfo();
			if($group_info)
			{
				$this->view->assign('first_group_id', $group_info[0]['id']);
			}
			else
			{
				$this->view->assign('first_group_id', null);
			}
			
			if($this->_auth_obj->access_file_image_manager != '1')
			{
				$info = $group_info[0]->toArray();
				$this->view->group_info_json = Zend_Json_Encoder::encode($info);
				$info['group_id'] = $group_info[0]['id'];
				$this->userUploaderSettings($info);
			}
		}	
	}	
	
	public function editAction()
	{
		$id = $this->_getParam('id', 0);
		$translator = $this->translator;
		
		//Get Autos Info
		$autosData = new Autos_Model_DbTable_Spareparts();		
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$id = $this->_request->getPost('id');
						
			if($this->SparePartsForm->isValid($this->_request->getPost())) 
			{					
				if(!empty($id))
				{					
					$autos = new Autos_Model_Spareparts($this->SparePartsForm->getValues());					
					$autos->setId($id);
					$autos->setSpare_parts_image_primary($this->_request->getPost('spare_parts_image_primary'));									
					$autos->setAutos_id($this->_request->getPost('autos_id'));			
					$autos->setSpare_parts_title($this->SparePartsForm->spare_parts_title->getValue(), $autos->getId());							
					
					if($this->view->allow())
					{
						$result = $autos->saveSpareparts();
						if($result['status'] == 'ok')
						{
							$msg = $translator->translator("page_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);						
						}
						else
						{
							$msg = $translator->translator("page_save_err");
							$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
						}						
					}
					else
					{
						$msg =  $translator->translator("page_add_action_deny_desc");
						$json_arr = array('status' => 'errP','msg' => $msg);
					}					
				}
				else
				{
					$msg = $translator->translator("page_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			
			}
			else
			{
				$validatorMsg = $this->SparePartsForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);		
		}
		else
		{	
			//Get Autos Info 
			$autosInfo = $autosData->getInfo($id);	
			if($autosInfo)
			{				
				//ASSIGN PROPERTY AGENT
				$r = true;			
				if(is_numeric($autosInfo['autos_agent']))
				{ 
					if(is_int((int)$autosInfo['autos_agent']))
					{
						$r = false;
					}				
				}
				if($r){ $this->SparePartsForm->autos_agent->addMultiOption($autosInfo['autos_agent'],stripslashes($autosInfo['autos_agent'])); }
					
				$group_db = new Autos_Model_DbTable_AutosGroup();
				$group_info = $group_db->getAllGroupInfo();
				if($group_info)
				{
					$this->view->assign('first_group_id', $group_info[0]['id']);
					$autosInfo['group_id']	=	$group_info[0]['id'];
				}
				else
				{
					$this->view->assign('first_group_id', null);
					$autosInfo['group_id']	=	null;
				}					
								
				$this->view->autosInfo =  $autosInfo;						
				$this->SparePartsForm->populate($autosInfo);
				$this->SparePartsForm->autos_id->setIsArray(true); 				
				$autos_id_arr = explode(',',$autosInfo['autos_id']);
				$this->SparePartsForm->autos_id->setValue($autos_id_arr);						
				$this->view->id = $id;				
				$this->view->spare_parts_image_primary = $autosInfo['spare_parts_image_primary'];					
				$this->view->SparePartsForm = $this->SparePartsForm;				
				$this->userUploaderSettings($autosInfo);
				
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}	
	}	
	
	private function userUploaderSettings($info)
	{
		$group_db = new Autos_Model_DbTable_AutosGroup();
		$group_info = $group_db->getGroupName($info['group_id']);
		$param_fields = array(
								'table_name' => 'autos_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_spare_parts_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));	
		
		/**************************************************For Secondary**************************************************/	
			/*$secondary_requested_data	=	$requested_data;
			$secondary_requested_data['file_path_field'] = 'file_path_feature_interior_plan_image';			
			
			$this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));		
			$this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info));*/
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						
			if ($this->view->allow('delete'))
			{
				$id = $this->_request->getPost('id');			
				
				
				//page_info
				$proObj = new Autos_Model_DbTable_Spareparts();
				$page_info = $proObj->getInfo($id);
						
				
				$spare_parts_name = $page_info['spare_parts_name'];
				$spare_parts_image = explode(',',$page_info['spare_parts_image']);
						
				
				$spare_parts_image_path = 'data/frontImages/autos/spare_parts_image';
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Remove from Autos
				try
				{					
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					
					$conn->delete(Zend_Registry::get('dbPrefix').'autos_spare_parts', $where);
					
					foreach($spare_parts_image as $key=>$file)
					{
						if($file)
						{
							$dir = $spare_parts_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('page_list_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('page_list_file_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
					$json_arr = array('status' => 'ok');
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('page_list_delete_err',$file);			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deletesavedAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$perm = new Autos_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendspareparts','Autos'))
			{
				$id = $this->_request->getPost('id');	
									
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Remove from Autos
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'autos_spare_parts_saved', $where);
					$msg = 	$translator->translator('autos_list_saved_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('autos_list_saved_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg.' '.$e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('autos_list_saved_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');						
			if ($this->view->allow('delete'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
										
					foreach($id_arr as $id)
					{						
						//Get Category Thumbnil
						$select = $conn->select()
									->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_spare_parts'), array('gc.spare_parts_image'))
									->where('gc.id = ?',$id);
						$rs = $select->query()->fetchAll();
						if($rs)
						{				
							foreach($rs as $row)
							{
								$spare_parts_image = explode(',',$row['spare_parts_image']);
							}
						}
												
						
						$spare_parts_image_path = 'data/frontImages/autos/spare_parts_image';
												
						//Remove from Category						
						try
						{														
							$where = array();
							$where[] = 'id = '.$conn->quote($id);
							
							$conn->delete(Zend_Registry::get('dbPrefix').'autos_spare_parts', $where);
							foreach($spare_parts_image as $key=>$file)
							{
								if($file)
								{
									$dir = $spare_parts_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								}
								if($res)
								{
									$msg = 	$translator->translator('page_list_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('page_list_file_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
							$json_arr = array('status' => 'ok');
						}
						catch (Exception $e) 
						{
							$msg = 	$translator->translator('category_list_delete_err');			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deletesavedallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Autos_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendpro','Autos'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					foreach($id_arr as $id)
					{	
						// Remove from Category
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'autos_spare_parts_saved', $where);
							$msg = 	$translator->translator('page_list_delete_success');			
							$json_arr = array('status' => 'ok','msg' => $msg);	
						}
						catch (Exception $e) 
						{
							$msg = 	$translator->translator('category_list_delete_err');			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function isnot_int($autos_agent)
	{
		$r = true;			
		if(is_numeric($autos_agent))
		{ 
			if(is_int((int)$autos_agent))
			{
				$r = false;
			}				
		}
		return $r;
	}
	
	public function publishAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{	
				$id = $this->_request->getPost('id');
				$spare_parts_name = $this->_request->getPost('spare_parts_name');
				$paction = $this->_request->getPost('paction');
				$data = array();
				
				switch($paction)
				{
					case 'publish':
						$active = '1';
						break;
					case 'unpublish':
						$active = '0';
						break;
				}
				
				$data = array('active' => $active);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Update Autos status
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->update(Zend_Registry::get('dbPrefix').'autos_spare_parts',$data, $where);
					$return = true;
				}
				catch (Exception $e) 
				{
					$msg = $e->getMessage();
					$return = false;
				}
				if($return)
				{			
					$json_arr = array('status' => 'ok','active' => $active);
				}
				else
				{
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
		}
		else
		{
			$msg = $translator->translator('common_publish_perm');	
			$json_arr = array('status' => 'err','msg' => $msg);
		}
				
			
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function publishallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if($this->view->allow('publish'))
		{
			if ($this->_request->isPost()) 
			{	
				$id_str  = $this->_request->getPost('id_st');
				$paction = $this->_request->getPost('paction');
				
				switch($paction)
				{
					case 'publish':
						$active = '1';
						break;
					case 'unpublish':
						$active = '0';
						break;
				}
				
				if(!empty($id_str))
				{
					$id_arr = explode(',',$id_str);
					
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					foreach($id_arr as $id)
					{
						// Update Article status
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{	
							$conn->update(Zend_Registry::get('dbPrefix').'autos_spare_parts',array('active' => $active), $where);
							$return = true;
						}
						catch (Exception $e) 
						{
							$return = false;
						}
					}
				
					if($return)
					{			
						$json_arr = array('status' => 'ok');
					}
					else
					{
						$msg = $translator->translator('page_list_publish_err');	
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator("page_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
		}
		else
		{
			$msg = $translator->translator('common_publish_perm');	
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
			
	public function featuredAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$id = $this->_request->getPost('id');
				$spare_parts_name = $this->_request->getPost('spare_parts_name');
				$paction = $this->_request->getPost('paction');
				
				switch($paction)
				{
					case 'featured':
						$feature = '1';
						break;
					case 'unfeatured':
						$feature = '0';
						break;
				}
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Update Article status
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->update(Zend_Registry::get('dbPrefix').'autos_spare_parts',array('featured' => $feature), $where);
					$return = true;
				}
				catch (Exception $e) 
				{
					$return = false;
				}
				if($return)
				{			
					$json_arr = array('status' => 'ok','featured' => $feature);
				}
				else
				{
					$msg = $translator->translator('page_list_feature_err',$spare_parts_name);	
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
		}
		else
		{
			$msg = $translator->translator('common_perm_err');	
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function upAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		
		$id 	= $this->_request->getPost('id');
		$page_order 	= $this->_request->getPost('page_order');				
				
		$OrderObj = new Autos_Controller_Helper_AutosOrders($id);
		$returnV =	$OrderObj->decreaseOrder();
		
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function downAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
		
		$id = $this->_request->getPost('id');
		$page_order = $this->_request->getPost('page_order');
						
		$OrderObj = new Autos_Controller_Helper_AutosOrders($id);
		$returnV =	$OrderObj->increaseOrder();
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function orderallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_str = $this->_request->getPost('id_arr');
			$autos_order_str = $this->_request->getPost('autos_order_arr');			
			
			$id_arr = explode(',',$id_str);
			$autos_order_arr = explode(',',$autos_order_str);
			
			$order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
			$checkOrder = $order_numbers->checkOrderNumbers($autos_order_arr);
			if( $checkOrder['status'] == 'err')
			{
				$json_arr = array('status' => 'err','msg' => $checkOrder['msg']);
			}
			else
			{
				//Save Category Order
				$i = 0;
				foreach($id_arr as $id)
				{
					$OrderObj = new Autos_Controller_Helper_AutosOrders($id);
					$OrderObj->saveOrder($autos_order_arr[$i]);
					$i++;
				}
				$msg = $translator->translator("page_zero_order_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}			
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function searchAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Autos_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("autos_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Autos_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("autos_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}	
}

