<?php
class Autos_SearchController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_group_form_id_field = 'dynamic_form';
	private $_group_table = 'autos_group';
	private $_static_table = 'autos_page';
	private $_controllerCache;
	private $_translator;
	private	$_snopphing_cart;
	private $_ckLicense = true;
	
	public function init()
	{
		/* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');			
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
		
		/*Check Module License*/
		//$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
		if($this->_ckLicense == true)
		{

			$license = new Zend_Session_Namespace('License');
			if(!$license || !$license->license_data || !$license->license_data['modules'])
			{
				$curlObj = new Eicra_License_Version();
				$curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
				$license->license_data = $curlObj->getArrayResult();
			}
		}
		$this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$this->_snopphing_cart	=	($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;				
	}
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;		
		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = 1;
		}	

	}
	
	public function itemsAction()
	{
		$group_id = $this->_page_id;
		if ($this->_request->isPost()) 
		{			
			$postValue = $this->_request->getPost();
			$postValue['post_code_='] =	($postValue['post_code_='] == $this->_translator->translator('autos_block_simple_search_postcode_value')) ? '' : $postValue['post_code_='] ;		
			$group_id  = ($postValue['group_id_=']) ? $postValue['group_id_='] : $this->_page_id ;
			$search_db = new Settings_Model_DbTable_Search();
			$search_settings_info = $search_db->getInfo($group_id, $this->_group_table, $this->_group_form_id_field, $this->_static_table);
			
			if($search_settings_info)
			{
				$search_obj = new Settings_Controller_Helper_Search($group_id, $this->_group_table, $this->_group_form_id_field, $this->_static_table, $search_settings_info, $postValue);
				$view_datas = $search_obj->search();
				$group_datas = $search_obj->returnGroupInfo();
				$this->view->assign('group_datas', $group_datas);
				$this->view->assign('view_datas', $view_datas);
			}
			else
			{
				$this->view->assign('errorMessage', $this->_translator->translator('autos_search_settings_not_found'));
			}			
			$this->view->assign('postValue', $postValue);	
		}
	}	
	public function searchAction()
	{
		$global_conf = Zend_Registry::get('global_conf');
		$posted_data	=	$this->_request->getParams();	
		//print_r($posted_data);
		//exit;		
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		$preferences_db = new Autos_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		//echo $this->autos_truncate("Hello world",2,3,true);
		//exit();		
		if($this->_request->isPost())
		{
			Eicra_Global_Variable::getSession()->autos_search_info = $this->_request->getPost();
			if($posted_data && $posted_data['filter'] && $posted_data['filter']['filters'])
			{
				foreach($posted_data['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}			
		}
		
		if ($this->_request->isPost() && empty($posted_data['block_search'])) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();				

				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				

				$getViewPageNum = $this->_request->getParam('pageSize'); 
				$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Search-Autos-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_postValue = Zend_Json_Encoder::encode($postValue);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{	
					$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
					$vote = new Vote_Controller_Helper_ShowVoteButton('inline','autos_page', false );
					$review_helper = new Review_View_Helper_Review();					
					$today = date('Y-m-d');					
					$list_mapper = new Autos_Model_AutosListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
							$entry_arr 								= 	(!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr 								= 	is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']					=  	$this->view->numbers($entry_arr['id']);
							$entry_arr['autos_model_name_format'] 	= 	$this->autos_truncate( $entry_arr['autos_model_name'], 0, 4);
							$entry_arr['feature_mileage_format']	=  	$this->view->numbers($entry_arr['feature_mileage']);
							$entry_arr['feature_doors_format']		=  	$this->view->numbers($entry_arr['feature_doors']);
							$entry_arr['autos_price_format']		=  	$this->view->numbers($entry_arr['autos_price']);
							$entry_arr['autos_strickthrow_price_format']	=  	($entry_arr['autos_strickthrow_price']) ? $this->view->numbers($entry_arr['autos_strickthrow_price']) : null;

							$entry_arr['autos_desc_format'] 		= 	$this->view->escape(strip_tags( $entry_arr['autos_desc']));
							$entry_arr['autos_date_lang_format']	=  	$this->view->numbers(date('d/m/Y',strtotime($entry_arr['autos_date'])));	
							$entry_arr['autos_date_format']			=  	$this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['autos_date'])));
							$img_thumb_arr 							= 	explode(',',$entry_arr['autos_image']);	
							$autos_image_no 						= 	(empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
							$entry_arr['autos_image_format'] 		= 	($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/'.$this->view->escape($entry_arr['autos_primary_image']) :  'data/frontImages/autos/autos_image/'.$img_thumb_arr[0] ;
							$entry_arr['autos_image_no_format'] 	= 	$this->_translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));

							$entry_arr['review_no']					=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']			=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));

							$entry_arr['thumb_width'] 				= 	($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
							$entry_arr['thumb_height'] 				= 	($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';

							$list_stars = '';
							for($i = 1; $i < $maximum_stars_digit; $i++)
							{
								$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_autos_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_autos_img/star-inactive.png" />';

							}

							$entry_arr['original_price'] 			= 	$entry_arr['autos_price'];
							$entry_arr['price'] 					=	$entry_arr['original_price'];
							$entry_arr['primary_file_field_format']	=	$entry_arr['autos_image_format'] ;
							$entry_arr['name'] 						=	$entry_arr['autos_model_name'];

							$entry_arr['list_stars_format']			= 	$list_stars;
							$entry_arr['vote_format']				= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['autos_model_name']));								
							$entry_arr['table_name'] 				= 	'autos_page';
							$entry_arr['shopping_cart_enable']		= 	(($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') &&  ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
							$entry_arr['eCommerce'] 				= 	$preferences_data['eCommerce'];

							$view_datas['data_result'][$key]		=	$entry_arr;
							$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}		
	}
	
	private function autos_truncate($phrase,$start_words, $max_words, $char = false, $charset = 'UTF-8')
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				$dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				$phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
			}
		}
		else
		{
			$phrase_array = explode(' ',$phrase);
			if(count($phrase_array) > $max_words && $max_words > 0)
				$phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
		return $phrase;
	}	
	
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}
}

