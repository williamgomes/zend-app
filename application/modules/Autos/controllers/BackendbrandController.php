<?php

class Autos_BackendbrandController extends Zend_Controller_Action
{	
	private $autosBrandForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {	
        /* Initialize action controller here */		
		$this->autosBrandForm =  new Autos_Form_BrandForm ();
		$this->view->autosBrandForm =  $this->autosBrandForm;		

		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');

		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {
		// action body
		/*$pageNumber = $this->getRequest()->getParam('page');		
		$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 		
		$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
		
		if(empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = '30';
		}
		else if(!empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
		else if(empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $viewPageNumSes;
		}
		else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
					
		Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
		$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
		if( ($datas = $this->_controllerCache->load($uniq_id)) === false ) 
		{		
			$brand = new Autos_Model_BrandListMapper();			
			$datas =  $brand->fetchAll($pageNumber);
			$this->_controllerCache->save($datas, $uniq_id);
		}
		$this->view->datas = $datas;*/	
						
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'create' => $this->_request->getParam('create'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Autos_Model_BrandListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);								
								$entry_arr['publish_status_brand_name'] = str_replace('_', '-', $entry_arr['brand_name']);
								$entry_arr['entry_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
								$entry_arr['entry_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}	
	}
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function addAction()
	{		
		/*if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->autosBrandForm->isValid($this->_request->getPost())) 
			{	
				$brand = new Autos_Model_Brand($this->autosBrandForm->getValues());
								
				$perm = new Autos_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $brand->saveBrand();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];
						$brandInfo = new Autos_Model_DbTable_Brand();
						$datas = $brandInfo->getBrandInfo($last_id);
						$user_db = new Members_Model_DbTable_MemberList();
						$member_info = $user_db->getMemberInfo($datas['entry_by']) ;
						$datas['entry_by'] = $member_info['username'];
						$msg = $translator->translator("autos_brand_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg,'datas' => $datas);
					}
					else
					{
						$msg = $translator->translator("autos_brand_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("autos_brand_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->autosBrandForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errBrand)
				{					
					foreach($errBrand as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}*/	
		
				
		try
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->view->allow())
			{	
				if ($this->_request->isPost()) 
				{
					$posted_data	=	$this->_request->getParams();
					if($posted_data['models'])
					{	
						$brandInfo = new Autos_Model_DbTable_Brand(); 
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{ 							
							if($this->autosBrandForm->isValid($posted_data_value)) 
							{	
								$brand = new Autos_Model_Brand($this->autosBrandForm->getValues());					
								
								if(!$brandInfo->isDuplicate($this->autosBrandForm->getValue('brand_name')))
								{
									$save_result = $brand->saveBrand();
									if($save_result['status'] == 'ok')
									{
										if($save_result['id'])
										{
											$posted_data['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $save_result['id']);
										}
										$result_row = $brandInfo->getListInfo(null, $posted_data);
										$posted_data['models'][$posted_data_key]['id'] = $save_result['id'];
										foreach($result_row as $row_data)
										{
											$posted_data['models'][$posted_data_key]['full_name'] = $this->view->escape($row_data['full_name']);											
											$posted_data['models'][$posted_data_key]['id_format'] = $this->view->numbers($row_data['id']);
											$posted_data['models'][$posted_data_key]['entry_by'] = $row_data['entry_by'];
											$posted_data['models'][$posted_data_key]['entry_date'] = $row_data['entry_date'];
											$posted_data['models'][$posted_data_key]['publish_status_brand_name'] = str_replace('_', '-', $row_data['brand_name']);
											$posted_data['models'][$posted_data_key]['entry_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($row_data['entry_date'])));	
											$posted_data['models'][$posted_data_key]['entry_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($row_data['entry_date'])));
										}
										
										$status = 'ok';	
									}
									else
									{
										$status = 'err';
										$msg = $translator->translator("autos_brand_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
										break;
									}							
								}
								else
								{
									$status = 'err';	
									$msg = $translator->translator("autos_brand_save_duplicate_err");
									$json_arr = array('status' => 'err','msg' => $msg);
									break;
								}	
											
							}
							else
							{
								$status = 'err';
								$validatorMsg = $this->autosBrandForm->getMessages();
								$vMsg = array();
								$i = 0;
								foreach($validatorMsg as $key => $errType)
								{					
									foreach($errType as $errkey => $value)
									{
										$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
										$i++;
									}
								}
								$json_arr = array('status' => 'errV','msg' => $vMsg);
								break;
							}	
						}	
					
						if($status == 'ok')
						{
							$msg = $translator->translator("autos_brand_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
						}			
					}
					else
					{
						$msg = $translator->translator("autos_brand_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator("autos_brand_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$Msg =  $translator->translator("autos_brand_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}
		}			
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}	
		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	
	}
	
	public function editAction()
	{	
		/*if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');			
			$brand_name = $this->_request->getPost('brand_name');
			
			$brand = new Autos_Model_Brand();
			$brand->setId($id);		
			$brand->setBrand_name($brand_name);								
				
			$perm = new Autos_View_Helper_Allow();
			if($perm->allow())
			{
				$result = $brand->saveBrand();
				if($result['status'] == 'ok')
				{
					$msg = $translator->translator("autos_brand_save_success");
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				else
				{
					$msg = $translator->translator("autos_brand_save_err");
					$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
				}	
			}
			else
			{
				$Msg =  $translator->translator("autos_brand_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}*/	
		
		try
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->view->allow())
			{
				if ($this->_request->isPost()) 
				{	
					$posted_data	=	$this->_request->getParams();
					if($posted_data['models'])
					{	
						$brandInfo = new Autos_Model_DbTable_Brand();
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{	
							if($this->autosBrandForm->isValid($posted_data_value)) 
							{											
								$brand = new Autos_Model_Brand($this->autosBrandForm->getValues());
								$brand->setId($posted_data_value['id']);					
								if(!$brandInfo->isDuplicate($this->autosBrandForm->getValue('brand_name'), null, $brand->getId()))
								{							
									$save_result = $brand->saveBrand();
									if($save_result['status'] == 'ok')
									{
										$status = 'ok';	
									}
									else
									{
										$status = 'err';
										$msg = $translator->translator("autos_brand_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
										break;
									}
								}
								else
								{
									$status = 'err';
									$msg = $translator->translator("autos_brand_save_duplicate_err");
									$json_arr = array('status' => 'err','msg' => $msg);
									break;
								}								
							}
							else
							{
								$validatorMsg = $this->autosBrandForm->getMessages();
								$vMsg = array();
								$i = 0;
								foreach($validatorMsg as $key => $errType)
								{					
									foreach($errType as $errkey => $value)
									{
										$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
										$i++;
									}
								}
								$status = 'err';
								$json_arr = array('status' => 'errV','msg' => $vMsg);
								break;
							}
						}
						
						if($status == 'ok')
						{
							$msg = $translator->translator("autos_brand_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
						}
					}
					else
					{
						$msg = $translator->translator("autos_brand_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);	
					}
				}
				else
				{
					$msg = $translator->translator("autos_brand_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);	
				}
			}
			else
			{
				$Msg =  $translator->translator("autos_brand_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}			
					
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);		
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			$check_num_pro = new Autos_View_Helper_AutosGroup();
					
			if($check_num_pro->getNumOfAutosForBrand($id) == '0')
			{
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'autos_brand', $where);
					$msg = 	$translator->translator('autos_brand_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('autos_brand_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('autos_brand_for_page_delete_err');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('autos_brand_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
					
				$check_num_pro = new Autos_View_Helper_AutosGroup();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{			
					if($check_num_pro->getNumOfAutosForBrand($id) == '0')
					{
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'autos_brand', $where);
							$msg = 	$translator->translator('autos_brand_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e) 
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('autos_brand_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('autos_brand_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("autos_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('autos_brand_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
		
}

