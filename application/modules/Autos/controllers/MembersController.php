<?php

//Members Frontend controller
class Autos_MembersController extends Zend_Controller_Action {

    private $_page_id;
    private $_controllerCache;
    private $_auth_obj;
    private $translator;
    private $ProductForm;
    private $SparePartsForm;

    public function init() {
        $url = $this->view->url(array('module' => 'Members', 'controller' => 'frontend', 'action' => 'login'), 'Frontend-Login', true);
        Eicra_Global_Variable::checkSession($this->_response, $url);
        Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();

        /* Initialize Template */
        $template_obj = new Eicra_View_Helper_Template;
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath(APPLICATION_PATH . '/layouts/scripts/' . $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout(true));

        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();


        $this->view->menu_id = $this->_request->getParam('menu_id');
        $getAction = $this->_request->getActionName();


        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();

            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        $this->view->setEscape('stripslashes');
    }

    public function listAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_list_header_title'), $this->view->url()));

        $group_id = $this->_request->getParam('group');
        $this->view->group_id = $group_id;
        $approve = $this->getRequest()->getParam('approve');
        $this->view->approve = $approve;
        $category_id = $this->getRequest()->getParam('category');
        $this->view->category_id = $category_id;

        $group_db = new Autos_Model_DbTable_AutosGroup();
        $group_info = ($group_id) ? $group_db->getGroupName($group_id) : null;
        $this->view->group_info = $group_info;

        $group_list = $group_db->getAllGroupInfo();
        $this->view->group_list = $group_list;

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        $list_mapper = new Autos_Model_AutosListMapper();
        $this->view->assign('mileage_info', $list_mapper->getDbTable()->getMileage());

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                if ($group_id) {
                    $posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $group_id);
                    $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
                }
                if ($category_id) {
                    $posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $category_id);
                    $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
                }

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontendRoute = 'Members-Auto-List/*';
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group' => $group_id, 'category' => $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRoute, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['autos_price_format'] = $this->view->numbers($entry_arr['autos_price']);
                            $entry_arr['feature_mileage_format'] = $this->view->numbers($entry_arr['feature_mileage']);
                            $entry_arr['publish_status_autos_model_name'] = str_replace('_', '-', $entry_arr['autos_model_name']);
                            $entry_arr['autos_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['autos_date'])));
                            $entry_arr['autos_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['autos_date'])));
                            $img_thumb_arr = explode(',', $entry_arr['autos_image']);
                            $entry_arr['autos_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }

        $business_type_db = new Autos_Model_DbTable_BusinessType();
        $this->view->assign('type_info', $business_type_db->getOptions($group_id));

        $brand_db = new Autos_Model_DbTable_Brand();
        $this->view->assign('brand_info', $brand_db->getAllBrand());

        $cat_Info = new Autos_Model_DbTable_Category();
        $this->view->assign('cat_data', $cat_Info->getOptions($group_id));

        $country_db = new Autos_Model_DbTable_Country();
        $this->view->assign('country_data', $country_db->getCountryInfo());

        $mem_db = new Members_Model_DbTable_MemberList();
        $this->view->assign('mem_data', $mem_db->getAllMembers());
    }

    public function addAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_list_header_title'), 'Members-Auto-List'), array($this->translator->translator('autos_frontend_list_add_page_name'), $this->view->url()));
        $this->ProductForm = new Autos_Form_ProductForm ();
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            try {
                //Checking Membership Package Option
                $packages = new Members_Controller_Helper_Packages();
                if ($packages->isPackage()) {
                    $module = $this->_request->getModuleName();
                    $controller = 'backendpro';
                    $action = $this->_request->getActionName();
                    if ($packages->isPackageField($module, $controller, $action)) {
                        $package_no = $packages->getPackageFieldValue($module, $controller, $action);
                    } else {
                        $package_no = 0;
                    }
                } else {
                    $package_no = 0;
                }

                $autos_db = new Autos_Model_DbTable_Autos();
                $p_num = $autos_db->numOfAutos();
                if (empty($package_no) || ($p_num < $package_no)) {
                    $group_id = $this->_request->getPost('group_id');
                    if (!empty($group_id)) {
                        $group_info = new Autos_Model_DbTable_AutosGroup();
                        $option = $group_info->getGroupName($group_id);
                        if ($option['dynamic_form']) {
                            $option['form_id'] = $option['dynamic_form'];
                            $this->ProductForm = new Autos_Form_ProductForm($option);
                            $field_db = new Members_Model_DbTable_Fields();
                            $field_groups = $field_db->getGroupNames($option['form_id']);
                            foreach ($field_groups as $group) {
                                $displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
                                $elements = $displaGroup->getElements();
                                foreach ($elements as $element) {
                                    $group_arr[$element->getName()] = $element->getName();
                                    if ($element->getType() == 'Zend_Form_Element_File') {
                                        $element_name = $element->getName();
                                        $required = ($element->isRequired()) ? true : false;
                                        $element_info = $element;
                                        $this->ProductForm->removeElement($element_name);
                                        $this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(), 'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required' => $required));
                                    }
                                    $this->ProductForm->addDisplayGroup($group_arr, $group->field_group, array('disableLoadDefaultDecorators' => true, 'title' => $group->field_group, 'legend' => $group->field_group));
                                }
                            }
                        }
                    }
                    if ($this->_request->getPost('billing_user_places_order_email_enable') == 'yes') {
                        $this->ProductForm->billing_user_places_order_email_address->setRequired(true);
                    } else {
                        $this->ProductForm->billing_user_places_order_email_address->setRequired(false);
                    }
                    if ($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes') {
                        $this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
                    } else {
                        $this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
                    }
                    if ($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes') {
                        $this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
                    } else {
                        $this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
                    }
                    if ($this->ProductForm->isValid($this->_request->getPost())) {
                        $autos = new Autos_Model_Autos($this->ProductForm->getValues());
                        $auth = $this->view->auth;
                        $stat = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article == '1') ? '1' : '0';
                        $autos->setActive($stat);
                        $autos->setFeatured('0');
                        $autos->setAutos_primary_image($this->_request->getPost('autos_primary_image'));
                        $autos->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));
                        $autos->setRelated_items($this->_request->getPost('related_items'));

                        if ($this->view->allow()) {
                            $result = $autos->saveAutos();
                            if ($result['status'] == 'ok') {
                                $msg = $this->translator->translator("page_save_success");
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                                if ($option['dynamic_form']) {
                                    $result2 = $autos->saveDynamicForm($option, $this->ProductForm, $result, $this->_request);
                                    if ($result2['status'] == 'ok') {
                                        $msg = $this->translator->translator("page_save_success");
                                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                                    } else {
                                        $msg = $this->translator->translator("page_save_err");
                                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result2['msg']);
                                    }
                                }
                            } else {
                                $msg = $this->translator->translator("page_save_err");
                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                            }
                        } else {
                            $Msg = $this->translator->translator("page_add_action_deny_desc");
                            $json_arr = array('status' => 'errP', 'msg' => $Msg);
                        }
                    } else {
                        $validatorMsg = $this->ProductForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                    }
                } else {
                    $msg = $this->translator->translator("autos_add_limit_err", $package_no);
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->proForm = $this->ProductForm;
        }
    }

    public function editAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_list_header_title'), 'Members-Auto-List'), array($this->translator->translator('autos_frontend_list_edit_page_name'), $this->view->url()));

        $id = $this->_getParam('id', 0);
        $translator = $this->translator;

        //Get Autos Info
        $this->ProductForm = new Autos_Form_ProductForm ();
        $autosData = new Autos_Model_DbTable_Autos();

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            $id = $this->_request->getPost('id');
            if (!empty($id)) {
                $autosInfo = $autosData->getAutosInfo($id);
                $group_info = new Autos_Model_DbTable_AutosGroup();
                $option = $group_info->getGroupName($autosInfo['group_id']);
                if ($option['dynamic_form']) {
                    $option['form_id'] = $option['dynamic_form'];
                    $this->ProductForm = new Autos_Form_ProductForm($option);
                    $field_db = new Members_Model_DbTable_Fields();
                    $field_groups = $field_db->getGroupNames($option['form_id']);

                    if ($field_groups) {

                        foreach ($field_groups as $group) {

                            $displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
                            $elements = $displaGroup->getElements();
                            foreach ($elements as $element) {
                                $group_arr[$element->getName()] = $element->getName();
                                if ($element->getType() == 'Zend_Form_Element_File') {
                                    $element_name = $element->getName();
                                    $required = ($element->isRequired()) ? true : false;
                                    $element_info = $element;
                                    $this->ProductForm->removeElement($element_name);
                                    $this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(), 'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required' => $required));
                                }
                                $this->ProductForm->addDisplayGroup($group_arr, $group->field_group, array('disableLoadDefaultDecorators' => true, 'title' => $group->field_group));
                            }
                        }
                    }
                }
            }

            try {
                if ($this->_request->getPost('billing_user_places_order_email_enable') == 'yes') {
                    $this->ProductForm->billing_user_places_order_email_address->setRequired(true);
                } else {
                    $this->ProductForm->billing_user_places_order_email_address->setRequired(false);
                }
                if ($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes') {
                    $this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
                } else {
                    $this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
                }
                if ($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes') {
                    $this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
                } else {
                    $this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
                }
                if ($this->ProductForm->isValid($this->_request->getPost())) {
                    if (!empty($id)) {
                        $autos = new Autos_Model_Autos($this->ProductForm->getValues());
                        $autos->setPrev_category($autosInfo['category_id']);
                        $autos->setPrev_group($autosInfo['group_id']);
                        $autos->setId($id);
                        $autos->setAutos_primary_image($this->_request->getPost('autos_primary_image'));
                        $autos->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));
                        $autos->setAutos_model_title($this->ProductForm->getValue('autos_model_title'), $id);
                        $autos->setRelated_items($this->_request->getPost('related_items'));

                        $perm = new Autos_View_Helper_Allow();
                        if ($perm->allow()) {
                            $result = $autos->saveAutos();
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator("page_save_success");
                                $json_arr = array('status' => 'ok', 'msg' => $msg, 'obj' => $this->ProductForm->getValues());
                                if ($option['dynamic_form']) {
                                    $result2 = $autos->saveDynamicForm($option, $this->ProductForm, $result, $this->_request);
                                    if ($result2['status'] == 'ok') {
                                        $msg = $translator->translator("page_save_success");
                                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'obj' => $this->ProductForm->getValues());
                                    } else {
                                        $msg = $translator->translator("page_save_err1");
                                        $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result2['msg']);
                                    }
                                }
                            } else {
                                $msg = $translator->translator("page_save_err2");
                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                            }
                        } else {
                            $msg = $translator->translator("page_add_action_deny_desc");
                            $json_arr = array('status' => 'errP', 'msg' => $msg);
                        }
                    } else {
                        $msg = $translator->translator("page_save_err3");
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                    }
                } else {
                    $validatorMsg = $this->ProductForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            //Get Autos Info	
            $autosInfo = $autosData->getAutosInfo($id);
            if ($autosInfo) {
                //Put Group Information in the view
                $group_info = new Autos_Model_DbTable_AutosGroup();
                $option = $group_info->getGroupName($autosInfo['group_id']);

                //Dynamic Form
                if (!empty($option['dynamic_form'])) {
                    $option['form_id'] = $option['dynamic_form'];

                    $this->ProductForm = new Autos_Form_ProductForm($option);

                    $dynamic_valu_db = new Members_Model_DbTable_FieldsValue();
                    $autosInfo = $dynamic_valu_db->getFieldsValueInfo($autosInfo, $autosInfo['id']);
                }


                //Get Category Info
                $categoryData = new Autos_Model_DbTable_Category();
                $group_id = $autosInfo['group_id'];


                if ($autosInfo['category_id'] == '0') {
                    $selected = 'style="background-color:#D7D7D7"';
                    $category_name = $translator->translator("autos_tree_root");
                } else {
                    $categoryInfo = $categoryData->getCategoryInfo($autosInfo['category_id']);
                    $category_name = $categoryInfo['category_name'];
                    $selected = '';
                }


                //ASSIGN CATEGORY TREE
                $this->view->categoryTree = '<table class="example" id="dnd-example">' .
                        '<tbody ><tr id="node-0">' .
                        '<td ' . $selected . '>' .
                        '<span class="folder">' . $translator->translator("autos_tree_root") . '</span>' .
                        '</td>' .
                        '</tr>' . Autos_View_Helper_Categorytree::getSubCategory('0', $this->view, $group_id, $autosInfo['category_id']) . '</tbody></table>';

                //ASSIGN PROPERTY TYPE
                $r = true;
                if (is_numeric($autosInfo['autos_agent'])) {
                    if (is_int((int) $autosInfo['autos_agent'])) {
                        $r = false;
                    }
                }
                if ($r) {
                    $this->ProductForm->autos_agent->addMultiOption($autosInfo['autos_agent'], stripslashes($autosInfo['autos_agent']));
                }

                //ASSIGN PROPERTY TYPE
                $autosBusinessType = new Autos_Model_DbTable_BusinessType();
                $autosBusinessType_options = $autosBusinessType->getSelectOptions($autosInfo['group_id']);
                $this->ProductForm->autos_type->addMultiOptions($autosBusinessType_options);

                //ASSIGN STATE
                $states = new Eicra_Model_DbTable_State();
                $states_options = $states->getOptions($autosInfo['country_id']);
                $this->ProductForm->state_id->setMultiOptions($states_options);

                //ASSIGN AREA / CITY
                $areas = new Eicra_Model_DbTable_City();
                $areas_options = $areas->getOptions($autosInfo['state_id']);
                $this->ProductForm->area_id->setMultiOptions($areas_options);

                //ASSIGN INSTALLED EQUIPMENT & OPTIONS
                $this->ProductForm->equipment_installed->setIsArray(true);
                $equipment_installed_arr = explode(',', $autosInfo['equipment_installed']);
                $autosInfo['equipment_installed'] = $equipment_installed_arr;

                //ASSIGN OTHERS EQUIPMENT & OPTIONS
                $this->ProductForm->equipment_options->setIsArray(true);
                $equipment_options_arr = explode(',', $autosInfo['equipment_options']);
                $autosInfo['equipment_options'] = $equipment_options_arr;

                //ASSIGN EMPTY DATE
                $autosInfo['availabe_from'] = ($autosInfo['availabe_from'] == '0000-00-00') ? '' : $autosInfo['availabe_from'];
                $autosInfo['available_to'] = ($autosInfo['available_to'] == '0000-00-00') ? '' : $autosInfo['available_to'];
                $autosInfo['available_expire_date'] = ($autosInfo['available_expire_date'] == '0000-00-00') ? '' : $autosInfo['available_expire_date'];
                $autosInfo['available_activation_date'] = ($autosInfo['available_activation_date'] == '0000-00-00') ? '' : $autosInfo['available_activation_date'];
                $autosInfo['meter_electric_expire'] = ($autosInfo['meter_electric_expire'] == '0000-00-00') ? '' : $autosInfo['meter_electric_expire'];
                $autosInfo['meter_gas_expire'] = ($autosInfo['meter_gas_expire'] == '0000-00-00') ? '' : $autosInfo['meter_gas_expire'];
                $autosInfo['meter_energy_expire'] = ($autosInfo['meter_energy_expire'] == '0000-00-00') ? '' : $autosInfo['meter_energy_expire'];



                $this->view->option = $option;
                $this->view->autosInfo = $autosInfo;
                $this->ProductForm->populate($autosInfo);
                $this->ProductForm->related_items->setIsArray(true);
                $related_items_arr = explode(',', $autosInfo['related_items']);
                $this->ProductForm->related_items->setValue($related_items_arr);
                $this->view->id = $id;
                $this->view->entry_by = $autosInfo['entry_by'];
                $this->view->category_name = $category_name;
                $this->view->autos_primary_image = $autosInfo['autos_primary_image'];
                $this->view->feature_primary_interior_image = $autosInfo['feature_primary_interior_image'];
                $this->view->proForm = $this->ProductForm;
                $this->view->form_info = $this->ProductForm->getFormInfo();
                $this->dynamicUploaderSettings($this->view->form_info);
                $this->userUploaderSettings($autosInfo);
                $this->render();
            } else {
                $this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl() . $this->view->baseUrl() . '/Members-Auto-Add', array());
            }
        }
    }

    private function userUploaderSettings($info) {
        $group_db = new Autos_Model_DbTable_AutosGroup();
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'autos_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;

        /*         * ***************************************************For Primary************************************************ */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_autos_image';

        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json_Encoder::encode($this->view->primary_settings_info));

        /*         * ************************************************For Secondary************************************************* */
        $secondary_requested_data = $requested_data;
        $secondary_requested_data['file_path_field'] = 'file_path_spots_photo_gallery';

        $this->view->assign('secondary_settings_info', array_merge($secondary_requested_data, $settings_info));
        $this->view->assign('secondary_settings_json_info', Zend_Json_Encoder::encode($this->view->secondary_settings_info));
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json_Encoder::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

    public function favoriteAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_favorite_list_header_title'), $this->view->url()));

        // action body		
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Favorite-Auto-List/*', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_mapper = new Autos_Model_SavedAutosListMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['publish_status_autos_model_name'] = str_replace('_', '-', $entry_arr['autos_model_name']);
                            $entry_arr['autos_price_format'] = $this->view->numbers($entry_arr['autos_price']);
                            $entry_arr['autos_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['autos_date'])));
                            $entry_arr['autos_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['autos_date'])));
                            $entry_arr['feature_mileage_format'] = $this->view->numbers($entry_arr['feature_mileage']);
                            $img_thumb_arr = explode(',', $entry_arr['autos_image']);
                            $entry_arr['autos_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function listspareAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('autos_spare_part_list_page_title'), $this->view->url()));

        $approve = $this->getRequest()->getParam('approve');
        $this->view->approve = $approve;
        $featured = $this->getRequest()->getParam('featured');
        $this->view->featured = $featured;

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $list_mapper = new Autos_Model_SparepartsListMapper();

                if ($featured) {
                    $posted_data['filter']['filters'][] = array('field' => 'featured', 'operator' => 'eq', 'value' => $this->_request->getParam('featured'));
                    $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
                }

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'featured' => $featured, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Auto-Spare-Parts-List/*', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['spare_parts_base_price_format'] = $this->view->numbers($entry_arr['spare_parts_base_price']);
                            $entry_arr['spare_parts_promotion_price_format'] = $this->view->numbers($entry_arr['spare_parts_promotion_price']);
                            $entry_arr['spare_parts_order_qty_min_format'] = $this->view->numbers($entry_arr['spare_parts_order_qty_min']);
                            $entry_arr['publish_status_spare_parts_name'] = str_replace('_', '-', $entry_arr['spare_parts_name']);
                            $entry_arr['entry_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['entry_date'])));
                            $entry_arr['entry_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['entry_date'])));
                            $img_thumb_arr = explode(',', $entry_arr['spare_parts_image']);
                            $entry_arr['spare_parts_image_format'] = ($this->view->escape($entry_arr['spare_parts_image_primary'])) ? 'data/frontImages/autos/spare_parts_image/' . $this->view->escape($entry_arr['spare_parts_image_primary']) : 'data/frontImages/autos/spare_parts_image/' . $img_thumb_arr[0];
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function addspareAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_list_header_title'), 'Members-Auto-Spare-Parts-List/*'), array($this->translator->translator('autos_spare_parts_add_page_name'), $this->view->url()));
        $this->SparePartsForm = new Autos_Form_SparePartsForm ();

        if ($this->_request->isPost()) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = $this->translator;
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            //Checking Membership Package Option
            $packages = new Members_Controller_Helper_Packages();
            if ($packages->isPackage()) {
                $module = $this->_request->getModuleName();
                $controller = 'backendspareparts';
                $action = 'add';
                if ($packages->isPackageField($module, $controller, $action)) {
                    $package_no = $packages->getPackageFieldValue($module, $controller, $action);
                } else {
                    $package_no = 0;
                }
            } else {
                $package_no = 0;
            }
            $autos_db = new Autos_Model_DbTable_Spareparts();
            $p_num = $autos_db->numOfSpareParts();
            if (empty($package_no) || ($p_num < $package_no)) {
                if ($this->SparePartsForm->isValid($this->_request->getPost())) {
                    try {

                        $autos = new Autos_Model_Spareparts($this->SparePartsForm->getValues());
                        $auth = $this->view->auth;
                        $stat = ($auth->hasIdentity() && $auth->getIdentity()->auto_publish_article == '1') ? '1' : '0';
                        $autos->setActive($stat);
                        $autos->setFeatured('0');
                        $autos->setSpare_parts_image_primary($this->_request->getPost('spare_parts_image_primary'));
                        $autos->setAutos_id($this->_request->getPost('autos_id'));

                        if ($this->view->allow()) {
                            $result = $autos->saveSpareparts();
                            if ($result['status'] == 'ok') {
                                $msg = $translator->translator("page_save_success");
                                $json_arr = array('status' => 'ok', 'msg' => $msg);
                            } else {
                                $msg = $translator->translator("page_save_err");
                                $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                            }
                        } else {
                            $Msg = $translator->translator("page_add_action_deny_desc");
                            $json_arr = array('status' => 'errP', 'msg' => $Msg);
                        }
                    } catch (Exception $e) {
                        $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                    }
                } else {
                    $validatorMsg = $this->SparePartsForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } else {
                $msg = $translator->translator("autos_add_limit_err", $package_no);
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->SparePartsForm = $this->SparePartsForm;
            $group_db = new Autos_Model_DbTable_AutosGroup();
            $group_info = $group_db->getAllGroupInfo();
            if ($group_info) {
                $this->view->assign('first_group_id', $group_info[0]['id']);
            } else {
                $this->view->assign('first_group_id', null);
            }

            if ($this->_auth_obj->access_file_image_manager != '1') {
                $info = $group_info[0]->toArray();
                $this->view->group_info_json = Zend_Json_Encoder::encode($info);
                $info['group_id'] = $group_info[0]['id'];
                $this->userSpareUploaderSettings($info);
            }
        }
    }

    public function editspareAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_list_header_title'), 'Members-Auto-Spare-Parts-List/*'), array($this->translator->translator('autos_page_edit_page_name'), $this->view->url()));
        $this->SparePartsForm = new Autos_Form_SparePartsForm ();

        $id = $this->_getParam('id', 0);
        $translator = $this->translator;

        //Get Autos Info
        $autosData = new Autos_Model_DbTable_Spareparts();

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            $id = $this->_request->getPost('id');

            if ($this->SparePartsForm->isValid($this->_request->getPost())) {
                if (!empty($id)) {
                    $autos = new Autos_Model_Spareparts($this->SparePartsForm->getValues());
                    $autos->setId($id);
                    $autos->setSpare_parts_image_primary($this->_request->getPost('spare_parts_image_primary'));
                    $autos->setAutos_id($this->_request->getPost('autos_id'));
                    $autos->setSpare_parts_title($this->SparePartsForm->spare_parts_title->getValue(), $autos->getId());

                    if ($this->view->allow()) {
                        $result = $autos->saveSpareparts();
                        if ($result['status'] == 'ok') {
                            $msg = $translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $msg = $translator->translator("page_save_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg . " " . $result['msg']);
                        }
                    } else {
                        $msg = $translator->translator("page_add_action_deny_desc");
                        $json_arr = array('status' => 'errP', 'msg' => $msg);
                    }
                } else {
                    $msg = $translator->translator("page_save_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $validatorMsg = $this->SparePartsForm->getMessages();
                $vMsg = array();
                $i = 0;
                foreach ($validatorMsg as $key => $errType) {
                    foreach ($errType as $errkey => $value) {
                        $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                        $i++;
                    }
                }
                $json_arr = array('status' => 'errV', 'msg' => $vMsg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            //Get Autos Info 
            $autosInfo = $autosData->getInfo($id);
            if ($autosInfo) {
                //ASSIGN PROPERTY AGENT
                $r = true;
                if (is_numeric($autosInfo['autos_agent'])) {
                    if (is_int((int) $autosInfo['autos_agent'])) {
                        $r = false;
                    }
                }
                if ($r) {
                    $this->SparePartsForm->autos_agent->addMultiOption($autosInfo['autos_agent'], stripslashes($autosInfo['autos_agent']));
                }

                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getAllGroupInfo();
                if ($group_info) {
                    $this->view->assign('first_group_id', $group_info[0]['id']);
                    $autosInfo['group_id'] = $group_info[0]['id'];
                } else {
                    $this->view->assign('first_group_id', null);
                    $autosInfo['group_id'] = null;
                }

                $this->view->autosInfo = $autosInfo;
                $this->SparePartsForm->populate($autosInfo);
                $this->SparePartsForm->autos_id->setIsArray(true);
                $autos_id_arr = explode(',', $autosInfo['autos_id']);
                $this->SparePartsForm->autos_id->setValue($autos_id_arr);
                $this->view->id = $id;
                $this->view->spare_parts_image_primary = $autosInfo['spare_parts_image_primary'];
                $this->view->SparePartsForm = $this->SparePartsForm;
                $this->userSpareUploaderSettings($autosInfo);
            } else {
                $this->_helper->redirector('addspare', $this->view->getController, $this->view->getModule, array());
            }
        }
    }

    public function savedAction() {
        //BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_autos_spare_parts_favorite_list_header_title'), $this->view->url()));

        // action body		
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Favorite-Auto-Spare-Parts-List/*', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_mapper = new Autos_Model_SavedSparePartsListMapper();
                    $list_datas = $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['publish_status_spare_parts_name'] = str_replace('_', '-', $entry_arr['spare_parts_name']);
                            $entry_arr['spare_parts_base_price_format'] = $this->view->numbers($entry_arr['spare_parts_base_price']);
                            $entry_arr['spare_parts_promotion_price_format'] = $this->view->numbers($entry_arr['spare_parts_promotion_price']);
                            $entry_arr['entry_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['entry_date'])));
                            $entry_arr['entry_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['entry_date'])));

                            $img_thumb_arr = explode(',', $entry_arr['spare_parts_image']);
                            $entry_arr['spare_parts_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/spare_parts_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/spare_parts_image/' . $img_thumb_arr[0];
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function userSpareUploaderSettings($info) {
        $group_db = new Autos_Model_DbTable_AutosGroup();
        $group_info = $group_db->getGroupName($info['group_id']);
        $param_fields = array(
            'table_name' => 'autos_group',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['group_id'],
            'file_path_field' => '',
            'file_extension_field' => 'file_type',
            'file_max_size_field' => 'file_size_max'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $group_info;

        /*         * ***************************************************For Primary************************************************ */
        $primary_requested_data = $requested_data;
        $primary_requested_data['file_path_field'] = 'file_path_spare_parts_image';

        $this->view->assign('primary_settings_info', array_merge($primary_requested_data, $settings_info));
        $this->view->assign('primary_settings_json_info', Zend_Json_Encoder::encode($this->view->primary_settings_info));

        /*         * ************************************************For Secondary************************************************* */
        /* $secondary_requested_data	=	$requested_data;
          $secondary_requested_data['file_path_field'] = 'file_path_feature_interior_plan_image';

          $this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));
          $this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info)); */
    }

}
