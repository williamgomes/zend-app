<?php
class Autos_MytestController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_controllerCache;	
	private $_auth_obj;	
	private $currency;
	private $translator;
	private	$_snopphing_cart;
	private $_ckLicense = true;
	
    public function init()
    {


        /* Initialize action controller here */				
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');	
		
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
		
				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;	
			
	}
	
	public function sampleAction()
    {								
		
		$this->view->pageTitle = "Sample Page";
		$this->view->pageContent = "This is some content of the page.";
		$this->view->sampleArray = array("English" => 90, "Bangla" => 75, "Maths" => 95, "Sc. Studies" => 60, "Science" => 70, "Religion" => 80);

	}
	
	
		
	
}

