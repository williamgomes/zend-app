<?php
class Autos_FrontendsparepartsController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_controllerCache;
	private $_modules_license;
	private $_ckLicense = true;
	private	$_snopphing_cart;	
	private $_auth_obj;	
	private $currency;
	private $translator;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
			
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
		
		/*Check Module License*/
		//$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
		if($this->_ckLicense == true)
		{

			$license = new Zend_Session_Namespace('License');
			if(!$license || !$license->license_data || !$license->license_data['modules'])
			{
				$curlObj = new Eicra_License_Version();
				$curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
				$license->license_data = $curlObj->getArrayResult();
			}
		}
		$this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$this->_snopphing_cart	=	($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;			
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;		
		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}		
	}
	
	public function listAction()
    {
		$global_conf = Zend_Registry::get('global_conf');								
		$posted_data	=	$this->_request->getParams();		
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		$preferences_db = new Autos_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();	
		$this->view->assign('preferences_data', $preferences_data);					

		if ($this->_request->isPost() && empty($posted_data['block_search'])) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				if ($preferences_data && $preferences_data['autos_spare_parts_list_other_page_sortby'] && $preferences_data['autos_spare_parts_list_other_page_sortby'] != '_') 
				{
					$sort_arr 					= 	explode('-', $preferences_data['autos_spare_parts_list_other_page_sortby']);
					$posted_data['sort'][0]		= 	array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
					$thumb_width 				= 	($preferences_data['spare_parts_img_width'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToWidth' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'width="'.$preferences_data['spare_parts_img_width'].'"' : '';
					$thumb_height 				= 	($preferences_data['spare_parts_img_height'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToHeight' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'height="'.$preferences_data['spare_parts_img_height'].'"' : '';
				}
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
					
				$getViewPageNum = $this->_request->getParam('pageSize'); 
				$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Spare-Parts-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);					
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{	
					//$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
					$vote = new Vote_Controller_Helper_ShowVoteButton('inline','autos_spare_parts', false );
					$review_helper = new Review_View_Helper_Review();
								
					$list_mapper = new Autos_Model_SparepartsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr 											= 	(!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr 											= 	is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']								=  	$this->view->numbers($entry_arr['id']);
								$entry_arr['spare_parts_number_format'] 			= 	$this->view->numbers($entry_arr['spare_parts_number']);
								$entry_arr['spare_parts_manufacturer_number_format']= 	$this->view->numbers($entry_arr['spare_parts_manufacturer_number']);
								$entry_arr['spare_parts_name_format'] 				= 	$this->autos_truncate( $entry_arr['spare_parts_name'], 0, 6);
								$entry_arr['name'] 									= 	$entry_arr['spare_parts_name'];
								$entry_arr['spare_parts_base_price_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_base_price']);
								$entry_arr['spare_parts_promotion_price_format'] 	= 	$this->view->numbers($entry_arr['spare_parts_promotion_price']);
								$entry_arr['price']									=	(empty($entry_arr['spare_parts_promotion_price'])) ?	$entry_arr['spare_parts_base_price'] : $entry_arr['spare_parts_promotion_price'];
								$entry_arr['price_format']							=	$this->view->numbers(round($entry_arr['price'], 2));
								$entry_arr['spare_parts_order_qty_min_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_order_qty_min']);
								$entry_arr['publish_status_spare_parts_name'] 		= 	str_replace('_', '-', $entry_arr['spare_parts_name']);
								$entry_arr['entry_date_lang_format']				=  	$this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
								$entry_arr['entry_date_format']						=  	$this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
								$img_thumb_arr 										= 	explode(',',$entry_arr['spare_parts_image']);
								$autos_image_no 									= 	(empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);								
								$entry_arr['spare_parts_image_format'] 				= 	($this->view->escape($entry_arr['spare_parts_image_primary'])) ? 'data/frontImages/autos/spare_parts_image/'.$this->view->escape($entry_arr['spare_parts_image_primary']) :  'data/frontImages/autos/spare_parts_image/'.$img_thumb_arr[0] ;
								$entry_arr['spare_parts_image_no_format'] 			= 	$this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));
								$entry_arr['primary_file_field_format']				=	$entry_arr['spare_parts_image_format'] ;
								
								$entry_arr['thumb_width']							= 	$thumb_width;
								$entry_arr['thumb_height']							= 	$thumb_height;
								
								$entry_arr['vote_format']							= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['spare_parts_name']));								
								
								$entry_arr['review_id']								=	$preferences_data['spare_parts_review_id'];					
								$entry_arr['review_no']								=  	(!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
								$entry_arr['review_no_format']						=  	(!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
									
								$entry_arr['edit_enable']							=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;						
								$entry_arr['shopping_cart_enable']					= 	(($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') &&  ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_spare_parts'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
								$entry_arr['table_name'] 							= 	'autos_spare_parts';
								$entry_arr['eCommerce'] 							= 	$preferences_data['eCommerce'];
								
								$view_datas['data_result'][$key]					=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
				
	}
	
	public function classifiedsAction()
    {
		$global_conf = Zend_Registry::get('global_conf');								
		$posted_data	=	$this->_request->getParams();		
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		$preferences_db = new Autos_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();	
		$this->view->assign('preferences_data', $preferences_data);					

			
		try
		{	
			// action body
			if ($preferences_data && $preferences_data['autos_spare_parts_list_other_page_sortby'] && $preferences_data['autos_spare_parts_list_other_page_sortby'] != '_') 
			{
				$sort_arr 					= 	explode('-', $preferences_data['autos_spare_parts_list_other_page_sortby']);
				$posted_data['sort'][0]		= 	array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
				$thumb_width 				= 	($preferences_data['spare_parts_img_width'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToWidth' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'width="'.$preferences_data['spare_parts_img_width'].'"' : '';
				$thumb_height 				= 	($preferences_data['spare_parts_img_height'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToHeight' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'height="'.$preferences_data['spare_parts_img_height'].'"' : '';
			}
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $this->_request->getParam('pageSize'); 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Spare-Parts-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
			
			$list_mapper = new Autos_Model_SparepartsListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));	
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);					
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				//$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','autos_spare_parts', false );
				$review_helper = new Review_View_Helper_Review();
									
				$view_datas = array('data_result' => array(), 'total' => 0);			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr 											= 	(!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr 											= 	is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']								=  	$this->view->numbers($entry_arr['id']);
							$entry_arr['spare_parts_number_format'] 			= 	$this->view->numbers($entry_arr['spare_parts_number']);
							$entry_arr['spare_parts_manufacturer_number_format']= 	$this->view->numbers($entry_arr['spare_parts_manufacturer_number']);
							$entry_arr['spare_parts_name_format'] 				= 	$this->autos_truncate( $entry_arr['spare_parts_name'], 0, 6);
							$entry_arr['name'] 									= 	$entry_arr['spare_parts_name'];
							$entry_arr['spare_parts_base_price_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_base_price']);
							$entry_arr['spare_parts_promotion_price_format'] 	= 	$this->view->numbers($entry_arr['spare_parts_promotion_price']);
							$entry_arr['price']									=	(empty($entry_arr['spare_parts_promotion_price'])) ?	$entry_arr['spare_parts_base_price'] : $entry_arr['spare_parts_promotion_price'];
							$entry_arr['price_format']							=	$this->view->numbers(round($entry_arr['price'], 2));
							$entry_arr['spare_parts_order_qty_min_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_order_qty_min']);
							$entry_arr['publish_status_spare_parts_name'] 		= 	str_replace('_', '-', $entry_arr['spare_parts_name']);
							$entry_arr['entry_date_lang_format']				=  	$this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
							$entry_arr['entry_date_format']						=  	$this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
							$img_thumb_arr 										= 	explode(',',$entry_arr['spare_parts_image']);
							$autos_image_no 									= 	(empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);								
							$entry_arr['spare_parts_image_format'] 				= 	($this->view->escape($entry_arr['spare_parts_image_primary'])) ? 'data/frontImages/autos/spare_parts_image/'.$this->view->escape($entry_arr['spare_parts_image_primary']) :  'data/frontImages/autos/spare_parts_image/'.$img_thumb_arr[0] ;
							$entry_arr['spare_parts_image_no_format'] 			= 	$this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));
							$entry_arr['primary_file_field_format']				=	$entry_arr['spare_parts_image_format'] ;
							
							$entry_arr['thumb_width']							= 	$thumb_width;
							$entry_arr['thumb_height']							= 	$thumb_height;
							
							$entry_arr['vote_format']							= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['spare_parts_name']));								
							
							$entry_arr['review_id']								=	$preferences_data['spare_parts_review_id'];					
							$entry_arr['review_no']								=  	(!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']						=  	(!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
								
							$entry_arr['edit_enable']							=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;						
							$entry_arr['shopping_cart_enable']					= 	(($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') &&  ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_spare_parts'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
							$entry_arr['table_name'] 							= 	'autos_spare_parts';
							$entry_arr['eCommerce'] 							= 	$preferences_data['eCommerce'];
							
							$view_datas['data_result'][$key]					=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}				
			$view_datas['total']	=	$list_datas->getTotalItemCount();
			$view_datas['paginator']	=	$list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
		}		
		$this->view->assign('json_arr', $json_arr);								
	}
	
	public function autos_truncate($phrase,$start_words, $max_words)
	{
	   $phrase_array = explode(' ',$phrase);
	   if(count($phrase_array) > $max_words && $max_words > 0)
		  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
	   return $phrase;
	}
	
	public function printAction()
    {	
		$this->_helper->layout->disableLayout();	
		$this->view->module = $this->_request->getModuleName();
		$this->view->controller = $this->_request->getControllerName();
		$this->view->action = $this->_request->getActionName();			  
			
		if (!empty($this->_page_id)) 
		{
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details';
			if( ($autos_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$autos_db = new Autos_Model_DbTable_Autos();
				$autos_info = $autos_db->getAutosInfo($this->_page_id);
				$this->_controllerCache->save($autos_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Autos_Model_DbTable_AutosGroup();
				$group_info = $group_db->getGroupName($autos_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Autos_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$autos_info	=	$dynamic_value_db->getFieldsValueInfo($autos_info,$autos_info['id']);										
			}
			$propertyForm = new Autos_Form_ProductForm ($group_info);
			$propertyForm->populate($autos_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$autos_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
				
			$this->view->propertyForm = $propertyForm;
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $autos_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo; 
			$this->view->tab	= 0;
		}
		else if($this->_request->getParam('autos_model_title'))
		{
			$autos_model_title = $this->_request->getParam('autos_model_title');
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details_'.preg_replace('/[^a-zA-Z0-9_]/','_',$autos_model_title);
			if( ($autos_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$autos_db = new Autos_Model_DbTable_Autos();
				$autos_model_title_info = $autos_db->getTitleToId($autos_model_title);			
				$autos_id = $autos_model_title_info[0]['id'];
				$autos_info = $autos_db->getAutosInfo($autos_id);
				$this->_controllerCache->save($autos_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Autos_Model_DbTable_AutosGroup();
				$group_info = $group_db->getGroupName($autos_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Autos_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$autos_info	=	$dynamic_value_db->getFieldsValueInfo($autos_info,$autos_info['id']);										
			}
			$propertyForm = new Autos_Form_ProductForm ($group_info);
			$propertyForm->populate($autos_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$autos_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
				
			$this->view->propertyForm = $propertyForm;
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $autos_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo;
			$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
		}
	}
	
	public function detailsAction()
    {			
		$global_conf = Zend_Registry::get('global_conf');
		$posted_data	=	$this->_request->getParams();
		$spareparts_db = new Autos_Model_DbTable_Spareparts();
		$this->view->assign('spareparts_db', $spareparts_db);
		
		if(empty($this->_page_id))
		{
			$spare_parts_title = $this->_request->getParam('spare_parts_title');
			if($spare_parts_title)
			{
				$spareparts_title_info	=	$spareparts_db->getTitleToId($spare_parts_title);
				if($spareparts_title_info)
				{					
					$this->_page_id		=	$spareparts_title_info [0]['id'];
				}
			}
		}
		
		if(!empty($this->_page_id))
		{			
			$preferences_db = new Autos_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();	
			$this->view->assign('preferences_data', $preferences_data);	
			
			if ($preferences_data && $preferences_data['autos_spare_parts_list_other_page_sortby'] && $preferences_data['autos_spare_parts_list_other_page_sortby'] != '_') 
			{
				$sort_arr 					= 	explode('-', $preferences_data['autos_spare_parts_list_other_page_sortby']);
				$posted_data['sort'][0]		= 	array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
				$thumb_width 				= 	($preferences_data['spare_parts_img_width'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToWidth' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'width="'.$preferences_data['spare_parts_img_width'].'"' : '';
				$thumb_height 				= 	($preferences_data['spare_parts_img_height'] && ($preferences_data['spare_parts_img_resize_func'] == 'resizeToHeight' || $preferences_data['spare_parts_img_resize_func'] == 'resize' )) ? 'height="'.$preferences_data['spare_parts_img_height'].'"' : '';
			}
			
			$posted_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);	
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$review_helper = new Review_View_Helper_Review();
			$this->view->assign('review_helper', $review_helper);		
						
			$encode_params = Zend_Json_Encoder::encode($posted_data);					
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','autos_spare_parts', false );				
					
				$list_datas =  $spareparts_db->getListInfo('1', $posted_data, array('userChecking' => false));
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr 											= 	(!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr 											= 	is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']								=  	$this->view->numbers($entry_arr['id']);
							$entry_arr['spare_parts_number_format'] 			= 	$this->view->numbers($entry_arr['spare_parts_number']);
							$entry_arr['spare_parts_manufacturer_number_format']= 	$this->view->numbers($entry_arr['spare_parts_manufacturer_number']);
							$entry_arr['spare_parts_name_format'] 				= 	$this->autos_truncate( $entry_arr['spare_parts_name'], 0, 6);
							$entry_arr['name'] 									= 	$entry_arr['spare_parts_name'];
							$entry_arr['spare_parts_base_price_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_base_price']);
							$entry_arr['spare_parts_promotion_price_format'] 	= 	$this->view->numbers($entry_arr['spare_parts_promotion_price']);
							$entry_arr['price']									=	(empty($entry_arr['spare_parts_promotion_price'])) ?	$entry_arr['spare_parts_base_price'] : $entry_arr['spare_parts_promotion_price'];
							$entry_arr['price_format']							=	$this->view->numbers(round($entry_arr['price'], 2));
							$entry_arr['spare_parts_order_qty_min_format'] 		= 	$this->view->numbers($entry_arr['spare_parts_order_qty_min']);
							$entry_arr['publish_status_spare_parts_name'] 		= 	str_replace('_', '-', $entry_arr['spare_parts_name']);
							$entry_arr['entry_date_lang_format']				=  	$this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
							$entry_arr['entry_date_format']						=  	$this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
							$img_thumb_arr 										= 	explode(',',$entry_arr['spare_parts_image']);
							$autos_image_no 									= 	(empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);								
							$entry_arr['spare_parts_image_format'] 				= 	($this->view->escape($entry_arr['spare_parts_image_primary'])) ? 'data/frontImages/autos/spare_parts_image/'.$this->view->escape($entry_arr['spare_parts_image_primary']) :  'data/frontImages/autos/spare_parts_image/'.$img_thumb_arr[0] ;
							$entry_arr['spare_parts_image_no']					=	$autos_image_no;
							$entry_arr['spare_parts_image_no_format'] 			= 	$this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));
							$entry_arr['primary_file_field_format']				=	$entry_arr['spare_parts_image_format'] ;
							
							$entry_arr['thumb_width']							= 	$thumb_width;
							$entry_arr['thumb_height']							= 	$thumb_height;
							
							$entry_arr['vote_format']							= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['spare_parts_name']));								
							
							$entry_arr['review_id']								=	$preferences_data['spare_parts_review_id'];					
							$entry_arr['review_no']								=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']						=  (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
							$entry_arr['review_datas']							= 	$review_helper->getReviewList($entry_arr,$entry_arr['id']);
							$entry_arr['shopping_cart_enable']					= 	(($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') &&  ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_spare_parts'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
							$entry_arr['table_name'] 							= 	'autos_spare_parts';
							$entry_arr['eCommerce'] 							= 	$preferences_data['eCommerce'];
								
							$entry_arr['edit_enable']							=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['autos_agent']) ? true : false;						
							$view_datas['data_result'][$key]					=	$entry_arr;	
							$key++;					
					}					
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			if($view_datas['data_result'])
			{
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'][0], 'posted_data' => $posted_data);
			}
			else
			{
				$json_arr = array('status' => 'err', 'data_result' => null, 'posted_data' => $posted_data);
			}
		}
		else
		{
			$json_arr = array('status' => 'err', 'data_result' => null, 'posted_data' => $posted_data);
		}
		$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
		$this->view->assign('json_arr', $json_arr);
	}
	
	//For Ajax Save Autos Spare Parts
	public function saveAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$user_id = $globalIdentity->user_id;
				$spare_parts_id = $this->_request->getPost('id');
				try
				{
					$clause    = $this->_DBconn->quoteInto('user_id = ?', $user_id);
					$validator_saved_autos = new Zend_Validate_Db_RecordExists(
								array(
									'table' 	=> Zend_Registry::get('dbPrefix').'autos_spare_parts_saved',
									'field' 	=> 'spare_parts_id',
									'exclude' 	=> $clause
								)
							);
					if(!$validator_saved_autos->isValid($spare_parts_id))
					{
						//Update category_id of the product of this category to zero
						$data = array(					
							'user_id' 		=> 	$user_id,
							'spare_parts_id' 	=>	$spare_parts_id										
						);
						try
						{
							$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'autos_spare_parts_saved',$data);
							$msg = $translator->translator("autos_spare_recorded_success");
							$json_arr = array('status' => 'ok','msg' =>  $msg);
						}
						catch(Exception $e)
						{
							$msg = $translator->translator("autos_spare_recorded_err");
							$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage());
						}				
					}
					else
					{								
						$msg = $translator->translator("autos_spare_already_saved_err");
						$json_arr = array('status' => 'err','msg' =>  $msg);
					}				
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err','msg' =>  $e->getMessage());
				}				
			}
			else
			{
				$msg = $translator->translator("common_login_err");
				$json_arr = array('status' => 'err','msg' =>  $msg);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
		
	//For Ajax Search
	/*public function searchAction()
	{
		if($this->_request->getPost('search_type') != 'post')
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
		}
		
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			
			$search_obj = new Autos_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("autos_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			if($this->_request->getPost('search_type') == 'post')
			{
				$group_db =  new Autos_Model_DbTable_AutosGroup();
				$group_info = $group_db->getGroupName($this->_request->getParam('group_id'));
				$this->view->group_datas = $group_info;
				$this->view->post_datas = $this->_request->getPost();
				if($result['status'] == 'ok')
				{					 
					 $this->view->view_datas = $result['result_data'];
				}
				else
				{
					$this->view->view_datas = null;
				}
			}
			else
			{
				//Convert To JSON ARRAY	
				$res_value = Zend_Json_Encoder::encode($json_arr);	
				$this->_response->setBody($res_value);
			}
		}
	}*/
	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_field[$i]	= 'active';
			$postData_arr['active'] = 1;
			$search_obj = new Autos_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("autos_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
		
	/*public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Eicra_Model_DbTable_City();			
        	$cities_options = $cities->getOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}*/
		
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}		
}

