<?php

class Autos_Service_Invoice {

    protected $_invoice_obj;
    protected $_calendar_data;

    public function __construct($invoice_obj = null) {
        $this->_invoice_obj = $invoice_obj;
        $this->_translator = Zend_Registry::get('translator');
    }

    public function createItinerary() {
        if ($this->_invoice_obj['invoice_items']) {
            try {
                $autos_id_arr = array();
                foreach ($this->_invoice_obj['invoice_items'] as $key => $item_arr) {
                    if ($item_arr['object_value']) {
                        $obj_info = Zend_Json::decode($item_arr['object_value']);
                        if ($obj_info['item']['table_name'] == 'autos_page') {
                            $autos_id_arr[$key] = $obj_info['table_id'];
                        }
                    }
                }

                $autos_id_string = implode(',', $autos_id_arr);
                if (!empty($autos_id_string)) {
                    $autos_db = new Autos_Model_DbTable_Autos();
                    $search_params['filter']['filters'][0] = array('field' => 'autos_id_string', 'operator' => 'eq', 'value' => $autos_id_string);

                    $list_info = $autos_db->getListInfo(null, $search_params, false);
                    if ($list_info) {
                        $invoice_info = array();
                        $key_id = 0;
                        $key_id1 = 0;
                        foreach ($list_info as $list_info_key => $info_arr) {
                            if ($info_arr['billing_user_places_order_email_enable'] == 'yes' && !empty($info_arr['billing_user_places_order_email_address'])) {
                                $invoice_info['billing_user_places_order_email_address'][$key_id1] = $info_arr['billing_user_places_order_email_address'];
                                $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                                $key_id1++;
                            }
                            if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                                $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                                $invoice_info['item_details'][$key_id] = '<table><tr><td colspan="2" style="height:8px"></td></tr>
																					 <tr>
																						<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
																						' . stripslashes($info_arr['autos_model_name']) . '
																						</td>
																					</tr></table>';
                                $key_id++;
                            }
                        }

                        $email_class = new Invoice_View_Helper_Email();

                        //Email Template Array Start
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	 	= 			$invoice_info['billing_user_places_order_email_address'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_places_order_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_places_order_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_places_order_email_address'] );
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TO];
                        $email_data_arr[Eicra_File_Constants::PAY_TO] = $this->_invoice_obj[Eicra_File_Constants::PAY_TO];
                        $email_data_arr[Eicra_File_Constants::INVOICE_LOGO] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_LOGO];
                        $email_data_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_CREATE_DATE];
                        $email_data_arr[Eicra_File_Constants::INVOICE_DATE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_DATE];
                        $email_data_arr[Eicra_File_Constants::INVOICE_TABLE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TABLE];

                        $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_PAYMENT_STATUS];
                        $email_data_arr['title'] = $this->_invoice_obj['title'];
                        $email_data_arr['firstName'] = $this->_invoice_obj['firstName'];
                        $email_data_arr['lastName'] = $this->_invoice_obj['lastName'];
                        $email_data_arr[Eicra_File_Constants::INVOICE_ID] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
                        $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT];
                        //Email Template Array End
                        //Initialize Email Template
                        $template_id_field = 'default_template_id';
                        $settings_db = new Invoice_Model_DbTable_Setting();
                        $template_info = $settings_db->getInfoByModule($this->_invoice_obj[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                        $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("autos_invoice_template_id");

                        $email_class->sendCommonMail($email_data_arr);
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function deleteItinerary() {
        
    }

    public function paidItinerary() {
        try {
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $autos_db = new Autos_Model_DbTable_Autos();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            $autos_id_arr = array();
            foreach ($invoice_items_arr as $invoice_items_arr_key => $invoice_items) {
                $object_value = Zend_Json::decode($invoice_items['object_value']);

                if ($invoice_items['object_value']) {
                    if ($object_value['item']['table_name'] == 'autos_page') {
                        $autos_id_arr[$invoice_items_arr_key] = $object_value['table_id'];
                    }
                }
            }

            // Extra START
            $email_class = new Invoice_View_Helper_Email();
            $invoice_later = $email_class->generateInvoiceLater($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            if ($invoice_later['status'] == 'ok') {
                $email_data_arr = $invoice_later['invoice_arr'];

                $autos_id_string = implode(',', $autos_id_arr);
                if (!empty($autos_id_string)) {
                    $search_params['filter']['filters'][0] = array('field' => 'autos_id_string', 'operator' => 'eq', 'value' => $autos_id_string);

                    $list_info = $autos_db->getListInfo(null, $search_params, false);
                    if ($list_info) {
                        $invoice_info = array();
                        $key_id = 0;
                        $key_id1 = 0;
                        foreach ($list_info as $list_info_key => $info_arr) {
                            $full_name = stripslashes($info_arr['full_name']);
                            if ($info_arr['billing_user_pay_invoice_email_enable'] == 'yes' && !empty($info_arr['billing_user_pay_invoice_email_enable'])) {
                                $invoice_info['billing_user_pay_invoice_email_address'][$key_id1] = $info_arr['billing_user_pay_invoice_email_address'];
                                $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                                $key_id1++;
                            }
                            if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                                $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                                $key_id++;
                            }
                        }

                        //Email Template Array Start
                        $global_conf = Zend_Registry::get('global_conf');
                        $currency = new Zend_Currency($global_conf['default_locale']);
                        $currencySymbol = $currency->getSymbol();
                        $currencyShortName = $currency->getShortName();
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	= 			$invoice_info['billing_user_pay_invoice_email_address'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_pay_invoice_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_pay_invoice_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_pay_invoice_email_address'] );

                        $email_data_arr['title'] = '';
                        $email_data_arr['firstName'] = $full_name;
                        $email_data_arr['lastName'] = '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_SUBJECT] = '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoice_later['invoice_info']['total'] + $invoice_later['invoice_info']['service_charge'] + $invoice_later['invoice_info']['tax']), 2, '.', ',') . ' ' . $currencyShortName;
                        $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#006600">' . $this->_translator->translator("common_paid_language") . '</h1>';
                        //Email Template Array End
                        //Initialize Email Template
                        $template_id_field = 'paid_template_id';
                        $settings_db = new Invoice_Model_DbTable_Setting();
                        $template_info = $settings_db->getInfoByModule($invoice_later['invoice_info'][Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                        $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("autos_invoice_template_id");

                        $email_class->sendCommonMail($email_data_arr);
                    }
                }
            }
            // Extra END
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }

    public function unpaidItinerary() {
        
    }

    public function cancelItinerary() {
        try {
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $autos_db = new Autos_Model_DbTable_Autos();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            $autos_id_arr = array();
            foreach ($invoice_items_arr as $invoice_items_arr_key => $invoice_items) {
                $object_value = Zend_Json::decode($invoice_items['object_value']);
                if ($invoice_items['object_value']) {
                    if ($object_value['item']['table_name'] == 'autos_page') {
                        $autos_id_arr[$invoice_items_arr_key] = $object_value['table_id'];
                    }
                }
            }

            // Extra START
            $email_class = new Invoice_View_Helper_Email();
            $invoice_later = $email_class->generateInvoiceLater($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            if ($invoice_later['status'] == 'ok') {
                $email_data_arr = $invoice_later['invoice_arr'];

                $autos_id_string = implode(',', $autos_id_arr);
                if (!empty($autos_id_string)) {
                    $search_params['filter']['filters'][0] = array('field' => 'autos_id_string', 'operator' => 'eq', 'value' => $autos_id_string);

                    $list_info = $autos_db->getListInfo(null, $search_params, false);
                    if ($list_info) {
                        $invoice_info = array();
                        $key_id = 0;
                        $key_id1 = 0;
                        foreach ($list_info as $list_info_key => $info_arr) {
                            $full_name = stripslashes($info_arr['full_name']);
                            if ($info_arr['billing_user_cancel_order_email_enable'] == 'yes' && !empty($info_arr['billing_user_cancel_order_email_enable'])) {
                                $invoice_info['billing_user_cancel_order_email_address'][$key_id1] = $info_arr['billing_user_cancel_order_email_address'];
                                $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                                $key_id1++;
                            }
                            if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                                $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                                $key_id++;
                            }
                        }

                        //Email Template Array Start
                        $global_conf = Zend_Registry::get('global_conf');
                        $currency = new Zend_Currency($global_conf['default_locale']);
                        $currencySymbol = $currency->getSymbol();
                        $currencyShortName = $currency->getShortName();
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	= 			$invoice_info['billing_user_pay_invoice_email_address'];
                        //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_cancel_order_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_cancel_order_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_cancel_order_email_address'] );
                        $email_data_arr['title'] = '';
                        $email_data_arr['firstName'] = $full_name;
                        $email_data_arr['lastName'] = '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_SUBJECT] = '';
                        $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoice_later['invoice_info']['total'] + $invoice_later['invoice_info']['service_charge'] + $invoice_later['invoice_info']['tax']), 2, '.', ',') . ' ' . $currencyShortName;
                        $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#FF9900">' . $this->_translator->translator("common_cancelled_language") . '</h1>';
                        //Email Template Array End
                        //Initialize Email Template
                        $template_id_field = 'cancel_template_id';
                        $settings_db = new Invoice_Model_DbTable_Setting();
                        $template_info = $settings_db->getInfoByModule($invoice_later['invoice_info'][Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                        $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("autos_invoice_template_id");


                        $email_class->sendCommonMail($email_data_arr);
                    }
                }
            }
            // Extra END
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }

}

?>