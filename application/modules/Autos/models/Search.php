<?php
class Autos_Model_Search
{
	protected $_view;	
	protected $_search_field;
	protected $_data_arr;
	protected $translator;
	protected $_fieldType = array(
									'autos_model_name' 	=>	'LIKE',
									'post_code' 		=>	'LIKE',
									'feature_mileage' 	=>	'=',
									'autos_price' 	=>	'LIKE'
									);
 
    public function __construct($view = null,$search_field,$data_arr)
	{
		$this->_view = $view;	
		$this->translator =  Zend_Registry::get('translator');
		$this->_search_field	= $search_field;	
		$this->_data_arr = $data_arr;
	}	
	
	public function _truncate($phrase,$start_words, $max_words)
	{
	   $phrase_array = explode(' ',$phrase);
	   if(count($phrase_array) > $max_words && $max_words > 0)
		  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
	   return $phrase;
	}	
	
	public function _isnot_int($autos_agent)
	{
		$r = true;			
		if(is_numeric($autos_agent))
		{ 
			if(is_int((int)$autos_agent))
			{
				$r = false;
			}				
		}
		return $r;
	}
		
	public function doSearch($limit = null)
	{	
		$i =0 ;	
		foreach($this->_search_field as $field)
		{
			if($field != 'Submit' && $field != 'search_type' && $field != 'group_id' )
			{
				if($field == 'autos_agent')
				{
					if($this->_data_arr[$field] != '' && $this->_data_arr[$field] != 'any' )
					{
						$this->_fieldType[$field] = ($this->_isnot_int($this->_data_arr[$field] )) ? 'LIKE' : '=';
					}
				}
				switch($this->_fieldType[$field])
				{
					case 'LIKE':
						if($this->_data_arr[$field] != '' && $this->_data_arr[$field] != 'any' ){ $query_arr[$i] = $field." ".$this->_fieldType[$field]." '%".$this->_data_arr[$field]."%'"; $i++; }
						break;
					case '=':
						if($this->_data_arr[$field] != '' && $this->_data_arr[$field] != 'any' ){ $query_arr[$i] = $field." ".$this->_fieldType[$field]." '".$this->_data_arr[$field]."'"; $i++; }
						break;
					default:
						if($this->_data_arr[$field] != '' && $this->_data_arr[$field] != 'any' ){ $query_arr[$i] = $field." = '".$this->_data_arr[$field]."'"; $i++; }
						break;
				}
			}			
		}
		
		$auth = Zend_Auth::getInstance ();		
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		$query_arr[$i] = (($user_id && $auth->getIdentity()->access_other_user_article == '1') || (!$auth->hasIdentity ())) ? ' (1 = 1) ' : '( autos_agent = "'.$user_id.'" OR entry_by = "'.$user_id.'" )';
		
		$where = ($query_arr) ? implode(' AND ',$query_arr) : '';
		$order = 'group_id ASC';
		$limits = ($limit) ? ' LIMIT '.$limit : '';
		$search_db = new Autos_Model_DbTable_Search();
		$global_conf = Zend_Registry::get('global_conf');
		$currency = new Zend_Currency($global_conf['default_locale']);
		try 
		{
			$result_data = $search_db->getSearchInfo($where,$order.$limits);
			$mem_db = new Members_Model_DbTable_MemberList();
			$country_db = new Autos_Model_DbTable_Country();
			$area_db = new Autos_Model_DbTable_Area();
			$cat_Info = new Autos_Model_DbTable_Category();
			$business_type_db = new Autos_Model_DbTable_BusinessType();
			$state_db = new Autos_Model_DbTable_State();
			$brand_db	=	new Autos_Model_DbTable_Brand();
			if($result_data)
			{
				$i=0;
				foreach($result_data as $row)
				{
					$brand_info = $brand_db->getBrandInfo($this->_view->escape($result_data[$i]['brand_id']));
					$result_data[$i]['brand_name']	=	$this->_view->escape($brand_info['brand_name']);					
					
					$member_info = $mem_db->getMemberInfo($this->_view->escape($result_data[$i]['entry_by']));
					$result_data[$i]['autos_desc'] = $this->_truncate(stripslashes($row['autos_desc']),0,300);
					$result_data[$i]['autos_date_short'] = strftime('%Y-%m-%d',strtotime($row['autos_date']));
					$result_data[$i]['autos_model_name_short']	=	$this->_truncate($result_data[$i]['autos_model_name'],0,6);
					$result_data[$i]['autos_date_title'] = '<strong>'.$this->translator->translator('common_date').' : </strong>'.$this->_view->escape($row['autos_date']).'<br /><strong>'.$this->translator->translator('common_entry').' :</strong>'.$this->_view->escape($member_info['username']);
					if(empty($result_data[$i]['category_id']))
					{
						$result_data[$i]['category_name']	=	 $this->translator->translator('common_no_catagory');
					}
					else
					{											  					 
							$category_info = $cat_Info->getCategoryInfo($this->_view->escape($result_data[$i]['category_id']));
							$result_data[$i]['category_name']	= $this->_view->escape($category_info['category_name']);
					}			
					
					if(!empty($result_data[$i]['autos_agent']))
					{
						$owner_info = ($this->_isnot_int($result_data[$i]['autos_agent'])) ? $this->_view->escape($result_data[$i]['autos_agent']) : $mem_db->getMemberInfo($this->_view->escape($result_data[$i]['autos_agent']));
						$result_data[$i]['owner_name'] = (is_array($owner_info)) ? '<br /><strong>'.$this->translator->translator('autos_agent').' : </strong>'.$owner_info['title'].' '.$owner_info['firstName'].' '.$owner_info['lastName'] : '<br /><strong>'.$this->translator->translator('autos_agent').' : </strong>'.$owner_info;																 
					}
					$result_data[$i]['price'] = $result_data[$i]['autos_price'];
					$result_data[$i]['autos_price'] = ($result_data[$i]['autos_price'])? '&nbsp;<strong>'.$this->translator->translator('autos_price').' : </strong>'.$result_data[$i]['autos_price'].'&nbsp;'.$currency->getSymbol() : '';
					$result_data[$i]['autos_postcode'] = ($result_data[$i]['post_code'])? '<br /><strong>'.$this->translator->translator('autos_post_code').' : </strong>'.$result_data[$i]['post_code'] : '';
					$result_data[$i]['autos_mileage'] = ($result_data[$i]['feature_mileage'])? '<br /><strong>'.$this->translator->translator('autos_mileage').' : </strong>'.$result_data[$i]['feature_mileage'] : '';
					
					if($result_data[$i]['autos_type'])
					{					
						$business_type = $business_type_db->getBusinessTypeInfo($result_data[$i]['autos_type']);
						$result_data[$i]['business_type']	=	$this->_view->escape($business_type['business_type']);
					}
					else
					{
						$result_data[$i]['business_type']	=	'';
					}
					
					if($this->_view->escape($result_data[$i]['autos_primary_image']))
					{ 
						$thumb_image = 'data/frontImages/autos/autos_image/'.$this->_view->escape($result_data[$i]['autos_primary_image']); 
					}
					else
					{ 
						if($this->_view->escape($result_data[$i]['autos_image']))
						{
							$autos_image_val_arr = explode(',',$result_data[$i]['autos_image']);
							$thumb_image = 'data/frontImages/autos/autos_image/'.$this->_view->escape($autos_image_val_arr[0]); 
						}
						else
						{
							$thumb_image = 'data/frontImages/autos/design_images/no_image.jpg'; 
						}		
					}
					$result_data[$i]['thumb_image'] = $thumb_image;
					$group_db = new Autos_Model_DbTable_AutosGroup();
					$group_datas = $group_db->getGroupName($result_data[$i]['group_id']);
					if($group_datas['file_thumb_width'] && $group_datas['file_thumb_resize_func'] != 'resizeToHeight')
					{ 
						$width = 'width="'.$this->_view->escape($group_datas['file_thumb_width']).'"'; 
					}
					else
					{
						$width = '';
					} 
					$result_data[$i]['width'] = $width;
					if($group_datas['file_thumb_height'] && $group_datas['file_thumb_resize_func'] != 'resizeToWidth')
					{ 
						$height = 'height="'.$this->_view->escape($group_datas['file_thumb_height']).'"'; 
					} 
					else
					{
						$height = '';
					}
					$result_data[$i]['height'] = $height;
					
					if($result_data[$i]['area_id'])
					{
						try
						{
							$area_info = $area_db->getAreaInfo($result_data[$i]['area_id']);
							$result_data[$i]['autos_location']	=	($area_info['city']) ? '<br /><strong>'.$this->translator->translator('autos_location').' : </strong>'.$this->_view->escape($area_info['city']) : '';
							$result_data[$i]['city'] =	($area_info['city']) ? $this->_view->escape($area_info['city']) : '';
						}
						catch(Exception $e)
						{
							$result_data[$i]['autos_location']	= '<br /><strong>'.$this->translator->translator('autos_location').' : </strong>'.$e->getMessage();
							$result_data[$i]['city'] = '';
						}
					}
					else
					{
						$result_data[$i]['autos_location']	=	'';
						$result_data[$i]['city'] = '';
					}
					
					if($result_data[$i]['state_id'])
					{
						try
						{
							$state_info = $state_db->getStateInfo($result_data[$i]['state_id']);						
							$result_data[$i]['state_name'] =	($state_info['state_name']) ? $this->_view->escape($state_info['state_name']) : '';
						}
						catch(Exception $e)
						{
							$result_data[$i]['state_name'] = '';
						}
					}
					else
					{
						$result_data[$i]['state_name'] = '';
					}
					
					if($result_data[$i]['country_id'])
					{
						try
						{
							$countries = $country_db->getCountryName($result_data[$i]["country_id"]);
							$result_data[$i]['country_name'] =	($countries['value']) ? $this->_view->escape($countries['value']) : '';
						}
						catch(Exception $e)
						{
							$result_data[$i]['country_name'] = '';
						}
					}
					else
					{
						$result_data[$i]['country_name'] = '';
					}
					
					$i++;
				}			
				$result = array('status' => 'ok' ,'result_data' => $result_data,'where' =>$where);
			}
			else
			{
				$result = array('status' => 'err' ,'msg' => $this->translator->translator('common_data_found_err'),'where' =>$where);
			}
		}
		catch (Exception $e)
		{
			$result = array('status' => 'err' , 'msg' => $e->getMessage(),'where' =>$where);	
		}
		return 	$result;
	}
}
?>