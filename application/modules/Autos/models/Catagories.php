<?php
class Autos_Model_Catagories
{
	protected $_id;
	protected $_group_id;
	protected $_entry_by;
	protected $_category_name;
	protected $_category_title;
	protected $_category_thumb;
	protected $_category_desc;
	protected $_parent;
	protected $_category_order;
	protected $_featured;
	protected $_active;
	protected $_prev_parent;
	protected $_prev_group;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;
	
 
    public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}			
	}
		
		public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)){
				throw new Exception('Invalid Auth autos');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveCategory()
		{
			$mapper  = new Autos_Model_CategoryMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setGroup_id($text)
		{
			$this->_group_id = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setCategory_name($text)
		{
			$this->_category_name =  addslashes($text);
			return $this;
		}
		
		public function setCategory_title($text,$id = null)
		{
			$pattern = Eicra_File_Constants::TITLE_PATTERN;
			$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
			$text = preg_replace($pattern, $replacement, trim($text));
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			if(empty($id))
			{		
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'autos_category'), array('m.id'))
							->where('m.category_title = ?',$text);
			}
			else
			{
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'autos_category'), array('m.id'))
							->where('m.category_title = ?',$text)->where('m.id != ?',$id);
			}
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$ids = (int)$row['id'];
				}
			}
			if(empty($ids))
			{
				$this->_category_title = $text;
			}
			else
			{
				$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'autos_category'), array('m.id'))
						->order(array('m.id DESC'))->limit(1);
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_id = (int)$row['id'];
					}
				}
				if(empty($id))
				{
					$this->_category_title = $text.'-'.($last_id+1);
				}
				else
				{
					$this->_category_title = $text.'-'.$id;
				}
			}
			return $this;
		}		
		
		public function setCategory_thumb($text)
		{
			$this->_category_thumb = $text;
			return $this;
		}
		
		public function setPrev_parent($text)
		{
			$this->_prev_parent = $text;
			return $this;
		}
		
		public function setPrev_group($text)
		{
			$this->_prev_group = $text;
			return $this;
		}
		
		
		public function setCategory_order($text)
		{
			if(!empty($text))
			{
				$this->_category_order = $text;
			}
			else
			{
				$group_id = $this->getGroup_id();
				$parent = $this->getParent();
				if(empty($this->_id))
				{
					$OrderObj = new Autos_Controller_Helper_CategoryOrders();
				}
				else
				{
					$OrderObj = new Autos_Controller_Helper_CategoryOrders($this->_id);
				}
				$OrderObj->setHeighestOrder($group_id,$parent);				
				$last_order = $OrderObj->getHighOrder();
				
				$this->_category_order = $last_order + 1;
			}
			return $this;
		}	
		
		public function setParent($text)
		{
			$this->_parent = $text;
			return $this;
		}
		
		public function setCategory_desc($text)
		{
			$this->_category_desc =  addslashes($text);
			return $this;
		}
		
		public function setFeatured($text)
		{
			$this->_featured = $text;
			return $this;
		}
		
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		public function setMeta_title($text)
		{
			$this->_meta_title =  addslashes($text);
			return $this;
		}
		
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords =  addslashes($text);
			return $this;
		}
		
		public function setMeta_desc($text)
		{
			$this->_meta_desc =  addslashes($text);
			return $this;
		}
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getGroup_id()
		{         
			return $this->_group_id;
		}
		
		public function getEntry_by()
		{   
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}     
			return $this->_entry_by;
		}
		
		public function getCategory_name()
		{
			return $this->_category_name;
		}
		
		public function getCategory_title()
		{         
			return $this->_category_title;
		}
		
		public function getCategory_thumb()
		{         
			return $this->_category_thumb;
		}
		
		public function getPrev_parent()
		{
			return $this->_prev_parent;
		}
		
		public function getPrev_group()
		{
			return $this->_prev_group;
		}
		
		public function getCategory_order()
		{   
			if(empty($this->_category_order))
			{ 
				$this->setCategory_order(''); 
			}    
			return $this->_category_order;
		}
		
		public function getParent()
		{         
			return $this->_parent;
		}
		
		public function getCategory_desc()
		{         
			return $this->_category_desc;
		}
		
		public function getFeatured()
		{         
			return $this->_featured;
		}
		
		public function getActive()
		{         
			return $this->_active;
		}
		
		public function getMeta_title()
		{         
			return $this->_meta_title;
		}
		
		public function getMeta_keywords()
		{         
			return $this->_meta_keywords;
		}
		
		public function getMeta_desc()
		{         
			return $this->_meta_desc;
		}
}
?>