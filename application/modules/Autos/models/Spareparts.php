<?php
class Autos_Model_Spareparts
{
	protected $_id;
	protected $_spare_parts_name;
	protected $_spare_parts_title;
	protected $_spare_parts_number;
	protected $_spare_parts_manufacturer_number;
	protected $_autos_agent;
	protected $_spare_parts_image_primary;
	protected $_spare_parts_image;
	protected $_spare_parts_description;
	protected $_spare_parts_item_condition;
	protected $_entry_by;
	protected $_spare_parts_base_price;
	protected $_spare_parts_promotion_price;	
	protected $_featured;
	protected $_active;
	protected $_spare_parts_delivery;
	protected $_spare_parts_payment_method;
	protected $_spare_parts_returns_refund;
	protected $_spare_parts_order_qty_min;
	protected $_spare_parts_origin;
	protected $_spare_parts_mechine_type;
	protected $_spare_parts_brand_name;
	protected $_spare_parts_material;
	protected $_spare_parts_type;
	protected $_spare_parts_protection_guarantee;
	protected $_spare_parts_specifications;
	protected $_spare_parts_terms_condition;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;	
	protected $_autos_id;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveSpareparts()
		{
			$mapper  = new Autos_Model_SparepartsMapper();
			$return = $mapper->save($this);
			return $return;
		}		
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}		
				
		public function setSpare_parts_name($text)
		{
			$this->_spare_parts_name = $text;
			return $this;
		}
		
		public function setSpare_parts_title($text,$id = null)
		{
			$pattern = Eicra_File_Constants::TITLE_PATTERN;
			$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
			$text = preg_replace($pattern, $replacement, trim($text));
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			if(empty($id))
			{		
				$select = $conn->select()
							->from(array('asp' => Zend_Registry::get('dbPrefix').'autos_spare_parts'), array('asp.id'))
							->where('asp.spare_parts_title = ?',$text);
			}
			else
			{
				$select = $conn->select()
							->from(array('asp' => Zend_Registry::get('dbPrefix').'autos_spare_parts'), array('asp.id'))
							->where('asp.spare_parts_title = ?',$text)->where('asp.id != ?',$id);
			}
			
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$ids = (int)$row['id'];
				}
			}
			if(empty($ids))
			{
				$this->_spare_parts_title = $text;
			}
			else
			{
				$select = $conn->select()
						->from(array('asp' => Zend_Registry::get('dbPrefix').'autos_spare_parts'), array('asp.id'))
						->order(array('asp.id DESC'))->limit(1);
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_id = (int)$row['id'];
					}
				}
				if(empty($id))
				{
					$this->_spare_parts_title = $text.'-'.($last_id+1);
				}
				else
				{
					$this->_spare_parts_title = $text.'-'.$id;
				}
			}
			return $this;
		}			
		
		public function setSpare_parts_number($text)
		{
			$this->_spare_parts_number = $text;
			return $this;
		}			
		
		public function setSpare_parts_manufacturer_number($text)
		{
			$this->_spare_parts_manufacturer_number = $text;
			return $this;
		}	
		
		public function setSpare_parts_description($text)
		{
			$this->_spare_parts_description = $text;
			return $this;
		}		
		
		public function setSpare_parts_item_condition($text)
		{
			$this->_spare_parts_item_condition = $text;
			return $this;
		}
		
		public function setAutos_agent($text)
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_autos_agent = ($globalIdentity->allow_to_change_ownership != '1') ? $globalIdentity->user_id : $text;
			}
			else
			{
				$this->_autos_agent = $text;
			}			
			return $this;
		}
		
		
		public function setAutos_id($text)
		{
			if(is_array($text))
			{					
				$autos_id = implode(',',$text);
			}
			else
			{
				$autos_id = $text;
			}
			$this->_autos_id = $autos_id;
			return $this;
		}
		
		public function setSpare_parts_base_price($text)
		{
			if(empty($text))
			{
				$this->_spare_parts_base_price = 0;
			}
			else
			{
				$this->_spare_parts_base_price = $text;
			}
			return $this;
		}
		
		public function setSpare_parts_promotion_price($text)
		{
			if(empty($text))
			{
				$this->_spare_parts_promotion_price = 0;
			}
			else
			{
				$this->_spare_parts_promotion_price = $text;
			}
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setSpare_parts_image_primary($text)
		{
			$this->_spare_parts_image_primary = $text;
			return $this;
		}
		
		public function setSpare_parts_image($text)
		{
			$this->_spare_parts_image = $text;
			return $this;
		}
		
		public function setSpare_parts_delivery($text)
		{
			$this->_spare_parts_delivery = $text;
			return $this;
		}
		
		public function setSpare_parts_payment_method($text)
		{
			$this->_spare_parts_payment_method = $text;
			return $this;
		}
		
		public function setSpare_parts_returns_refund($text)
		{
			$this->_spare_parts_returns_refund = $text;
			return $this;
		}		
		
		
		public function setSpare_parts_order_qty_min($text)
		{
			$this->_spare_parts_order_qty_min = $text;
			return $this;
		}		
		
		public function setSpare_parts_origin($text)
		{
			$this->_spare_parts_origin =  $text;
			return $this;
		}
		
		public function setSpare_parts_mechine_type($text)
		{
			$this->_spare_parts_mechine_type = $text;
			return $this;
		}
		
		public function setSpare_parts_brand_name($text)
		{
			$this->_spare_parts_brand_name = $text;
			return $this;
		}
		
		public function setSpare_parts_material($text)
		{
			$this->_spare_parts_material = $text;
			return $this;
		}		
		
		
		public function setSpare_parts_type($text)
		{
			$this->_spare_parts_type = $text;
			return $this;
		}		
		
		public function setSpare_parts_protection_guarantee($text)
		{
			$this->_spare_parts_protection_guarantee =  $text;
			return $this;
		}		
		
		
		public function setSpare_parts_specifications($text)
		{
			$this->_spare_parts_specifications = $text;
			return $this;
		}		
		
		public function setSpare_parts_terms_condition($text)
		{
			$this->_spare_parts_terms_condition =  $text;
			return $this;
		}
		
		public function setFeatured($text)
		{
			$this->_featured = $text;
			return $this;
		}
		
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		public function setMeta_title($text)
		{
			$this->_meta_title =  $text;
			return $this;
		}
		
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords =  $text;
			return $this;
		}
		
		public function setMeta_desc($text)
		{
			$this->_meta_desc =  $text;
			return $this;
		}
		
				
		public function getId()
		{         
			return $this->_id;
		}		
				
		public function getSpare_parts_name()
		{
			return $this->_spare_parts_name;
		}
		
		public function getSpare_parts_title()
		{
			return $this->_spare_parts_title;
		}		
				
		public function getSpare_parts_number()
		{
			return $this->_spare_parts_number;
		}		
				
		public function getSpare_parts_manufacturer_number()
		{
			return $this->_spare_parts_manufacturer_number;
		}
		
		public function getSpare_parts_description()
		{
			return $this->_spare_parts_description;
		}
		
		public function getSpare_parts_item_condition()
		{
			return $this->_spare_parts_item_condition;
		}
		
		public function getAutos_agent()
		{
			$this->_autos_agent = (empty($this->_autos_agent)) ? $this->getEntry_by() : $this->_autos_agent;			
			return $this->_autos_agent;
		}
		
		public function getSpare_parts_base_price()
		{         
			return $this->_spare_parts_base_price;
		}
		
		public function getSpare_parts_promotion_price()
		{         
			return $this->_spare_parts_promotion_price;
		}
		
		
		public function getAutos_id()
		{
			return $this->_autos_id;
		}
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getSpare_parts_image_primary()
		{
			return $this->_spare_parts_image_primary;
		}
		
		public function getSpare_parts_image()
		{         
			return $this->_spare_parts_image;
		}
		
		
		
		
		public function getSpare_parts_delivery()
		{
			return $this->_spare_parts_delivery;
		}
		
		public function getSpare_parts_payment_method()
		{
			return $this->_spare_parts_payment_method;
		}
		
		public function getSpare_parts_returns_refund()
		{
			return $this->_spare_parts_returns_refund;
		}
		
		public function getSpare_parts_order_qty_min()
		{
			return $this->_spare_parts_order_qty_min;
		}
		
		public function getSpare_parts_origin()
		{
			return $this->_spare_parts_origin;
		}
		
		public function getSpare_parts_mechine_type()
		{
			return $this->_spare_parts_mechine_type;
		}
		
		public function getSpare_parts_brand_name()
		{
			return $this->_spare_parts_brand_name;
		}
		
		public function getSpare_parts_material()
		{
			return $this->_spare_parts_material;
		}
		
		public function getSpare_parts_type()
		{
			return $this->_spare_parts_type;
		}
		
		public function getSpare_parts_protection_guarantee()
		{
			return $this->_spare_parts_protection_guarantee;
		}
		
		public function getSpare_parts_specifications()
		{
			return $this->_spare_parts_specifications;
		}
		
		public function getSpare_parts_terms_condition()
		{
			return $this->_spare_parts_terms_condition;
		}		
		
		public function getFeatured()
		{         
			return $this->_featured;
		}
		
		public function getActive()
		{         
			return $this->_active;
		}		
		
		public function getMeta_title()
		{
			return $this->_meta_title;
		}
		
		public function getMeta_keywords()
		{
			return $this->_meta_keywords;
		}
		
		public function getMeta_desc()
		{
			return $this->_meta_desc;
		}		
}
?>