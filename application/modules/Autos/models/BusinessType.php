<?php
class Autos_Model_BusinessType
{
	protected $_id;
	protected $_group_id;
	protected $_business_type;	
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveBusinessType()
		{
			$mapper  = new Autos_Model_BusinessTypeMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setGroup_id($text)
		{
			$this->_group_id = $text;
			return $this;
		}
		
		public function setBusiness_type($text)
		{
			$this->_business_type = $text;
			return $this;
		}
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getGroup_id()
		{         
			return $this->_group_id;
		}
		
		public function getBusiness_type()
		{
			return $this->_business_type;
		}		
}
?>