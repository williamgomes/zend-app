<?php
class Autos_Model_Autos
{
	protected $_id;
	protected $_category_id;
	protected $_group_id;
	protected $_brand_id;
	protected $_autos_model_name;
	protected $_autos_model_title;
	protected $_autos_agent;
	protected $_autos_type;
	protected $_autos_primary_image;
	protected $_autos_image;
	protected $_autos_desc;
	protected $_entry_by;
	protected $_autos_price;
	protected $_autos_strickthrow_price;
	protected $_autos_order;
	protected $_stars;
	protected $_featured;
	protected $_active;
	protected $_prev_category;
	protected $_prev_group;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;
	protected $_country_id;
	protected $_state_id;
	protected $_area_id;
	protected $_showroom_no;
	protected $_showroom_name;
	protected $_showroom_phone;
	protected $_showroom_fax;
	protected $_showroom_email;
	protected $_post_code;
	protected $_showroom_address;
	protected $_feature_mileage;
	protected $_feature_kilometers;
	protected $_feature_at_car_id;
	protected $_feature_vin;
	protected $_feature_body_type;
	protected $_feature_bodystyle;
	protected $_feature_exterior_color;
	protected $_feature_interior_color;
	protected $_feature_cargo_area;
	protected $_feature_seats;
	protected $_feature_gauges;
	protected $_feature_lighting_elements;
	protected $_feature_engine_cylinder;
	protected $_feature_engine;
	protected $_feature_transmission;
	protected $_feature_wheelbase;
	protected $_feature_drivetrain;
	protected $_feature_drive_type;
	protected $_feature_fuel_type;
	protected $_feature_doors;
	protected $_feature_stock_number;
	protected $_feature_convenience;
	protected $_feature_entertainment;
	protected $_feature_primary_interior_image;
	protected $_feature_interior_plan_image;
	protected $_feature_google_map;
	protected $_feature_additional;
	protected $_brochure_title;
	protected $_brochure_desc;
	protected $_body_exterior;
	protected $_body_bonnet;
	protected $_body_bumper;
	protected $_body_doors;
	protected $_body_windows;
	protected $_body_radiator;
	protected $_body_roof_rack;
	protected $_body_spoiler;
	protected $_body_carpet;
	protected $_body_center_console;
	protected $_body_seats;
	protected $_body_speakers;
	protected $_body_entertainment;
	protected $_body_convenience;
	protected $_body_lighting;
	protected $_body_instrumentation;
	protected $_body_safety;
	protected $_body_suspension;
	protected $_body_short_desc;
	protected $_power_braking_system;
	protected $_power_adjusting_mechanism;
	protected $_power_engine_cooling_system;
	protected $_power_engine_oil_system;
	protected $_power_engine_components;
	protected $_power_engine_parts;
	protected $_power_engine_others;
	protected $_power_fuel_supply_system;
	protected $_power_chassis_frame;
	protected $_power_suspension;
	protected $_power_steering_system;
	protected $_power_transmission_system;
	protected $_power_camshaft;
	protected $_power_cylinder;
	protected $_power_wheels;
	protected $_power_tire_parts;
	protected $_power_connecting_rod;
	protected $_power_distributor;
	protected $_power_harmonic_balancer;
	protected $_power_piston;
	protected $_power_rocker_details;
	protected $_power_exhaust_system;
	protected $_power_gear;
	protected $_power_clutch_assembly;
	protected $_power_train_desc;
	protected $_elelectrical_audio_video_device;
	protected $_elelectrical_charging_system;
	protected $_elelectrical_window_system;
	protected $_elelectrical_alternator;	
	protected $_elelectrical_supply_system;
	protected $_elelectrical_battery;
	protected $_elelectrical_gauges_meters;
	protected $_elelectrical_ignition_system;
	protected $_elelectrical_signaling_system;
	protected $_elelectrical_sensors;
	protected $_elelectrical_starting_system;
	protected $_elelectrical_switches;
	protected $_elelectrical_wiring_harnesses;
	protected $_elelectrical_engine_control_unit;
	protected $_elelectrical_internet_facilities;
	protected $_elelectrical_temperature_control;
	protected $_elelectrical_system;
	protected $_elelectrical_short_desc;
	protected $_equipment_installed;
	protected $_equipment_options;
	protected $_misc_ac_system;
	protected $_misc_bearings;
	protected $_misc_hoses;
	protected $_misc_wiper_system;
	protected $_misc_other_parts;
	protected $_misc_lubrication_system;
	protected $_misc_seals;
	protected $_misc_safety_security;
	protected $_misc_disclaimer;
	protected $_availabe_from;
	protected $_available_to;
	protected $_available_expire_date;
	protected $_available_activation_date;
	protected $_meter_electric;
	protected $_meter_electric_expire;
	protected $_meter_gas;
	protected $_meter_gas_expire;
	protected $_meter_energy;
	protected $_meter_energy_expire;
	protected $_payment_desc;
	protected $_payment_terms_policy;
	protected $_misc_terms_condition;
	protected $_related_items;
	protected $_billing_user_places_order_email_enable;
	protected $_billing_user_places_order_email_address;
	protected $_billing_user_pay_invoice_email_enable;
	protected $_billing_user_pay_invoice_email_address;
	protected $_billing_user_cancel_order_email_enable;
	protected $_billing_user_cancel_order_email_address;
	protected $_billing_order_payment_email_enable;
	protected $_billing_item_desc;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveAutos()
		{
			$mapper  = new Autos_Model_AutosMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function saveDynamicForm($option,$dynamicForm,$result,$request)
		{
			$translator = Zend_Registry::get('translator');
			$field_db = new Members_Model_DbTable_Fields();
			$field_groups = $field_db->getGroupNames($option['form_id']); 
			foreach($field_groups as $group)
			{
				$group_name = $group->field_group;
				$displaGroup = $dynamicForm->getDisplayGroup($group_name);
				$elementsObj = $displaGroup->getElements();
				
				foreach($elementsObj as $element)
				{
					if(substr($element->getName(), -5) != '_prev')
					{
						$table_id = $result['id'];
						$form_id = $option['form_id'];
						$field_id	=	$element->getAttrib('rel');
						$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $request->getPost($element->getName()) : $element->getValue();
						if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
						{ 
							if(is_array($field_value)) { $field_value = ''; }
						}
						if($element->getType() == 'Zend_Form_Element_Multiselect') 
						{ 
							$field_value	=	$request->getPost($element->getName());
							if(is_array($field_value)) { $field_value = implode(',',$field_value); }
						}
						if(($element->getType() == 'Zend_Form_Element_File' && !empty($field_value)) || ($element->getType() != 'Zend_Form_Element_File'))
						{
							try
							{
								$DBconn = Zend_Registry::get('msqli_connection');
								$DBconn->getConnection();
								
								// Remove from Value
								$where = array();
								$where[0] = 'table_id = '.$DBconn->quote($table_id);
								$where[1] = 'form_id = '.$DBconn->quote($form_id);
								$where[2] = 'field_id = '.$DBconn->quote($field_id);
								$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
								
								//Add Value
								$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
								$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
								
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
							}
							catch(Exception $e)
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage());
								
							}
						}	
						else
						{
							$msg = $translator->translator("page_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}					
					}
					else
					{
						$msg = $translator->translator("page_save_success");
					    $json_arr = array('status' => 'ok','msg' => $msg);
					}
				}				
			}
			return $json_arr;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setGroup_id($text)
		{
			$this->_group_id = $text;
			return $this;
		}
		
		public function setCategory_id($text)
		{
			$this->_category_id = $text;
			return $this;
		}
		
		public function setBrand_id($text)
		{
			$this->_brand_id = $text;
			return $this;
		}
		
		public function setAutos_model_name($text)
		{
			$this->_autos_model_name = $text;
			return $this;
		}
		
		public function setAutos_model_title($text,$id = null)
		{
			$pattern = Eicra_File_Constants::TITLE_PATTERN;
			$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
			$text = preg_replace($pattern, $replacement, trim($text));
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			if(empty($id))
			{		
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'autos_page'), array('m.id'))
							->where('m.autos_model_title = ?',$text);
			}
			else
			{
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'autos_page'), array('m.id'))
							->where('m.autos_model_title = ?',$text)->where('m.id != ?',$id);
			}
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$ids = (int)$row['id'];
				}
			}
			if(empty($ids))
			{
				$this->_autos_model_title = $text;
			}
			else
			{
				$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'autos_page'), array('m.id'))
						->order(array('m.id DESC'))->limit(1);
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_id = (int)$row['id'];
					}
				}
				if(empty($id))
				{
					$this->_autos_model_title = $text.'-'.($last_id+1);
				}
				else
				{
					$this->_autos_model_title = $text.'-'.$id;
				}
			}
			return $this;
		}
		
		public function setAutos_agent($text)
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_autos_agent = ($globalIdentity->allow_to_change_ownership != '1') ? $globalIdentity->user_id : $text;
			}
			else
			{
				$this->_autos_agent = $text;
			}			
			return $this;
		}
		
		
		public function setRelated_items($text)
		{
			if(is_array($text))
			{					
				$related_items = implode(',',$text);
			}
			else
			{
				$related_items = $text;
			}
			$this->_related_items = $related_items;
			return $this;
		}
		
		public function setAutos_price($text)
		{
			if(empty($text))
			{
				$this->_autos_price = 0;
			}
			else
			{
				$this->_autos_price = $text;
			}
			return $this;
		}
		
		public function setAutos_strickthrow_price($text)
		{
			if(empty($text))
			{
				$this->_autos_strickthrow_price = 0;
			}
			else
			{
				$this->_autos_strickthrow_price = $text;
			}
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setAutos_primary_image($text)
		{
			$this->_autos_primary_image = $text;
			return $this;
		}
		
		public function setAutos_image($text)
		{
			$this->_autos_image = $text;
			return $this;
		}
		
		public function setAutos_type($text)
		{
			$this->_autos_type = $text;
			return $this;
		}
		
		public function setPrev_category($text)
		{
			$this->_prev_category = $text;
			return $this;
		}
		
		public function setPrev_group($text)
		{
			$this->_prev_group = $text;
			return $this;
		}
		
		
		public function setAutos_order($text)
		{
			if(!empty($text))
			{
				$this->_autos_order = $text;
			}
			else
			{
				$group_id = $this->getGroup_id();
				$category_id = $this->getCategory_id();
				if(empty($this->_id))
				{
					$OrderObj = new Autos_Controller_Helper_AutosOrders();
				}
				else
				{
					$OrderObj = new Autos_Controller_Helper_AutosOrders($this->_id);
				}
				$OrderObj->setHeighestOrder($group_id,$category_id);				
				$last_order = $OrderObj->getHighOrder();
				
				$this->_autos_order = $last_order + 1;
			}
			return $this;
		}
		
		public function setStars($text)
		{
			$this->_stars = $text;
			return $this;
		}		
		
		public function setAutos_desc($text)
		{
			$this->_autos_desc =  $text;
			return $this;
		}
		
		public function setFeatured($text)
		{
			$this->_featured = $text;
			return $this;
		}
		
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		public function setMeta_title($text)
		{
			$this->_meta_title =  $text;
			return $this;
		}
		
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords =  $text;
			return $this;
		}
		
		public function setMeta_desc($text)
		{
			$this->_meta_desc =  $text;
			return $this;
		}
		
		public function setCountry_id($text)
		{
			$this->_country_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setState_id($text)
		{
			$this->_state_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setArea_id($text)
		{
			$this->_area_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setShowroom_no($text)
		{
			$this->_showroom_no =   $text;
			return $this;
		}
		
		public function setShowroom_name($text)
		{
			$this->_showroom_name = $text;
			return $this;
		}
		
		public function setShowroom_phone($text)
		{
			$this->_showroom_phone = $text;
			return $this;
		}
		
		public function setShowroom_fax($text)
		{
			$this->_showroom_fax = $text;
			return $this;
		}
		
		public function setShowroom_email($text)
		{
			$this->_showroom_email = $text;
			return $this;
		}
		
		public function setPost_code($text)
		{
			$this->_post_code = str_replace(" ","",$text);
			return $this;
		}
		
		public function setShowroom_address($text)
		{
			$this->_showroom_address =  $text;
			return $this;
		}
		
		public function setFeature_mileage($text)
		{
			$this->_feature_mileage = ($text)? $text : '0';
			return $this;
		}
		
		public function setFeature_kilometers($text)
		{
			$this->_feature_kilometers =  $text;
			return $this;
		}
		
		public function setFeature_at_car_id ($text)
		{
			$this->_feature_at_car_id  =  $text;
			return $this;
		}
		
		public function setFeature_vin ($text)
		{
			$this->_feature_vin  =  $text;
			return $this;
		}
		
		public function setFeature_bodystyle($text)
		{
			$this->_feature_bodystyle =  $text;
			return $this;
		}
		
		public function setFeature_body_type($text)
		{
			$this->_feature_body_type =  $text;
			return $this;
		}
		
		public function setFeature_exterior_color($text)
		{
			$this->_feature_exterior_color =  $text;
			return $this;
		}
		
		public function setFeature_interior_color($text)
		{
			$this->_feature_interior_color =  $text;
			return $this;
		}
		
		public function setFeature_cargo_area($text)
		{
			$this->_feature_cargo_area =  $text;
			return $this;
		}
		
		public function setFeature_seats($text)
		{
			$this->_feature_seats =  $text;
			return $this;
		}
		
		public function setFeature_gauges($text)
		{
			$this->_feature_gauges =  $text;
			return $this;
		}
		
		public function setFeature_lighting_elements($text)
		{
			$this->_feature_lighting_elements =  $text;
			return $this;
		}
		
		public function setFeature_engine($text)
		{
			$this->_feature_engine =  $text;
			return $this;
		}
		
		public function setFeature_engine_cylinder($text)
		{
			$this->_feature_engine_cylinder =  $text;
			return $this;
		}
		
		public function setFeature_transmission($text)
		{
			$this->_feature_transmission = $text;
			return $this;
		}
		
		public function setFeature_wheelbase($text)
		{
			$this->_feature_wheelbase =  $text;
			return $this;
		}
		
		public function setFeature_drivetrain($text)
		{
			$this->_feature_drivetrain =  $text;
			return $this;
		}
		
		public function setFeature_drive_type($text)
		{
			$this->_feature_drive_type =  $text;
			return $this;
		}
		
		public function setFeature_fuel_type($text)
		{
			$this->_feature_fuel_type =  $text;
			return $this;
		}
		
		public function setFeature_doors($text)
		{
			$this->_feature_doors = ($text)? $text : '0';
			return $this;
		}
		
		public function setFeature_stock_number($text)
		{
			$this->_feature_stock_number =  $text;
			return $this;
		}
		
		public function setFeature_convenience($text)
		{
			$this->_feature_convenience =  $text;
			return $this;
		}
		
		public function setFeature_entertainment($text)
		{
			$this->_feature_entertainment =  $text;
			return $this;
		}
		
		public function setFeature_primary_interior_image($text)
		{
			$this->_feature_primary_interior_image = $text;
			return $this;
		}
		
		public function setFeature_interior_plan_image($text)
		{
			$this->_feature_interior_plan_image = $text;
			return $this;
		}
		
		public function setFeature_google_map($text)
		{
			$this->_feature_google_map =  ($text) ? addslashes($text) : null;
			return $this;
		}
		
		public function setFeature_additional($text)
		{
			$this->_feature_additional =  $text;
			return $this;
		}
		
		public function setBrochure_title($text)
		{
			$this->_brochure_title =  addslashes($text);
			return $this;
		}
		
		public function setBrochure_desc($text)
		{
			$this->_brochure_desc =  $text;
			return $this;
		}
		
		public function setBody_exterior($text)
		{
			$this->_body_exterior =  $text;
			return $this;
		}
		
		public function setBody_bonnet($text)
		{
			$this->_body_bonnet =  $text;
			return $this;
		}
		
		public function setBody_bumper($text)
		{
			$this->_body_bumper =  $text;
			return $this;
		}
		
		public function setBody_doors($text)
		{
			$this->_body_doors =  $text;
			return $this;
		}
		
		public function setBody_windows($text)
		{
			$this->_body_windows =  $text;
			return $this;
		}
		
		public function setBody_radiator($text)
		{
			$this->_body_radiator =  $text;
			return $this;
		}
		
		public function setBody_roof_rack($text)
		{
			$this->_body_roof_rack =  $text;
			return $this;
		}
		
		public function setBody_spoiler($text)
		{
			$this->_body_spoiler =  $text;
			return $this;
		}
		
		public function setBody_carpet($text)
		{
			$this->_body_carpet =  $text;
			return $this;
		}
		
		public function setBody_center_console($text)
		{
			$this->_body_center_console =  $text;
			return $this;
		}
		
		public function setBody_seats($text)
		{
			$this->_body_seats =  $text;
			return $this;
		}
		
		public function setBody_speakers($text)
		{
			$this->_body_speakers =  $text;
			return $this;
		}
		
		public function setBody_entertainment($text)
		{
			$this->_body_entertainment =  $text;
			return $this;
		}
		
		public function setBody_convenience($text)
		{
			$this->_body_convenience =  $text;
			return $this;
		}
		
		public function setBody_lighting($text)
		{
			$this->_body_lighting =  $text;
			return $this;
		}
		
		public function setBody_instrumentation($text)
		{
			$this->_body_instrumentation =  $text;
			return $this;
		}
		
		public function setBody_safety($text)
		{
			$this->_body_safety =  $text;
			return $this;
		}
		
		public function setBody_suspension($text)
		{
			$this->_body_suspension =  $text;
			return $this;
		}
		
		public function setBody_short_desc($text)
		{
			$this->_body_short_desc =  $text;
			return $this;
		}
		
		public function setPower_braking_system($text)
		{
			$this->_power_braking_system =  $text;
			return $this;
		}
		
		public function setPower_adjusting_mechanism($text)
		{
			$this->_power_adjusting_mechanism =  $text;
			return $this;
		}
		
		public function setPower_engine_cooling_system($text)
		{
			$this->_power_engine_cooling_system =  $text;
			return $this;
		}
		
		public function setPower_engine_oil_system($text)
		{
			$this->_power_engine_oil_system =  $text;
			return $this;
		}
		
		public function setPower_engine_components($text)
		{
			$this->_power_engine_components =  $text;
			return $this;
		}
		
		public function setPower_engine_parts($text)
		{
			$this->_power_engine_parts =  $text;
			return $this;
		}
		
		public function setPower_engine_others($text)
		{
			$this->_power_engine_others =  $text;
			return $this;
		}
		
		public function setPower_fuel_supply_system($text)
		{
			$this->_power_fuel_supply_system =  $text;
			return $this;
		}
		
		public function setPower_chassis_frame($text)
		{
			$this->_power_chassis_frame =  $text;
			return $this;
		}
		
		public function setPower_suspension($text)
		{
			$this->_power_suspension =  $text;
			return $this;
		}
		
		public function setPower_steering_system($text)
		{
			$this->_power_steering_system =  $text;
			return $this;
		}
		
		public function setPower_transmission_system($text)
		{
			$this->_power_transmission_system =  $text;
			return $this;
		}
		
		public function setPower_camshaft($text)
		{
			$this->_power_camshaft =  $text;
			return $this;
		}
		
		public function setPower_cylinder($text)
		{
			$this->_power_cylinder =  $text;
			return $this;
		}
		
		public function setPower_wheels($text)
		{
			$this->_power_wheels =  $text;
			return $this;
		}
		
		public function setPower_tire_parts($text)
		{
			$this->_power_tire_parts =  $text;
			return $this;
		}
		
		public function setPower_connecting_rod($text)
		{
			$this->_power_connecting_rod =  $text;
			return $this;
		}
		
		public function setPower_distributor($text)
		{
			$this->_power_distributor =  $text;
			return $this;
		}
		
		public function setPower_harmonic_balancer($text)
		{
			$this->_power_harmonic_balancer =  $text;
			return $this;
		}
		
		public function setPower_piston($text)
		{
			$this->_power_piston =  $text;
			return $this;
		}
		
		public function setPower_rocker_details($text)
		{
			$this->_power_rocker_details =  $text;
			return $this;
		}
		
		public function setPower_exhaust_system($text)
		{
			$this->_power_exhaust_system =  $text;
			return $this;
		}
		
		public function setPower_gear($text)
		{
			$this->_power_gear =  $text;
			return $this;
		}
		
		public function setPower_clutch_assembly($text)
		{
			$this->_power_clutch_assembly =  $text;
			return $this;
		}
		
		public function setPower_train_desc($text)
		{
			$this->_power_train_desc =  $text;
			return $this;
		}
		
		public function setElelectrical_audio_video_device($text)
		{
			$this->_elelectrical_audio_video_device =  $text;
			return $this;
		}
		
		public function setElelectrical_charging_system($text)
		{
			$this->_elelectrical_charging_system =  $text;
			return $this;
		}
		
		public function setElelectrical_window_system($text)
		{
			$this->_elelectrical_window_system =  $text;
			return $this;
		}
		
		public function setElelectrical_alternator($text)
		{
			$this->_elelectrical_alternator =  $text;
			return $this;
		}
		
		public function setElelectrical_supply_system($text)
		{
			$this->_elelectrical_supply_system =  $text;
			return $this;
		}
		
		public function setElelectrical_battery($text)
		{
			$this->_elelectrical_battery =  $text;
			return $this;
		}
		
		public function setElelectrical_gauges_meters($text)
		{
			$this->_elelectrical_gauges_meters =  $text;
			return $this;
		}
		
		public function setElelectrical_ignition_system($text)
		{
			$this->_elelectrical_ignition_system =  $text;
			return $this;
		}
		
		public function setElelectrical_signaling_system($text)
		{
			$this->_elelectrical_signaling_system =  $text;
			return $this;
		}
		
		public function setElelectrical_sensors($text)
		{
			$this->_elelectrical_sensors =  $text;
			return $this;
		}
		
		public function setElelectrical_starting_system($text)
		{
			$this->_elelectrical_starting_system =  $text;
			return $this;
		}
		
		public function setElelectrical_switches($text)
		{
			$this->_elelectrical_switches =  $text;
			return $this;
		}
		
		public function setElelectrical_wiring_harnesses($text)
		{
			$this->_elelectrical_wiring_harnesses =  $text;
			return $this;
		}
		
		public function setElelectrical_engine_control_unit($text)
		{
			$this->_elelectrical_engine_control_unit =  $text;
			return $this;
		}
		
		public function setElelectrical_internet_facilities($text)
		{
			$this->_elelectrical_internet_facilities =  $text;
			return $this;
		}
		
		public function setElelectrical_temperature_control($text)
		{
			$this->_elelectrical_temperature_control =  $text;
			return $this;
		}
		
		public function setElelectrical_system($text)
		{
			$this->_elelectrical_system =  $text;
			return $this;
		}
		
		public function setElelectrical_short_desc($text)
		{
			$this->_elelectrical_short_desc =  $text;
			return $this;
		}
		
		public function setEquipment_installed($text)
		{
			if(is_array($text))
			{					
				$equipment_installed = implode(',',$text);
			}
			else
			{
				$equipment_installed = $text;
			}
			$this->_equipment_installed =  $equipment_installed;
			return $this;
		}
		
		public function setEquipment_options($text)
		{
			if(is_array($text))
			{					
				$equipment_options = implode(',',$text);
			}
			else
			{
				$equipment_options = $text;
			}
			$this->_equipment_options =  $text;
			return $this;
		}
		
		public function setAvailabe_from($text)
		{
			$this->_availabe_from = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_to($text)
		{
			$this->_available_to = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_expire_date($text)
		{
			$this->_available_expire_date = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_activation_date($text)
		{
			$this->_available_activation_date = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_electric($text)
		{
			$this->_meter_electric =  addslashes($text);
			return $this;
		}
		
		public function setMeter_electric_expire($text)
		{
			$this->_meter_electric_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_gas($text)
		{
			$this->_meter_gas =  addslashes($text);
			return $this;
		}
		
		public function setMeter_gas_expire($text)
		{
			$this->_meter_gas_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_energy($text)
		{
			$this->_meter_energy =  addslashes($text);
			return $this;
		}
		
		public function setMeter_energy_expire($text)
		{
			$this->_meter_energy_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMisc_ac_system($text)
		{
			$this->_misc_ac_system =  $text;
			return $this;
		}
		
		public function setMisc_bearings($text)
		{
			$this->_misc_bearings =  $text;
			return $this;
		}
		
		public function setMisc_hoses($text)
		{
			$this->_misc_hoses =  $text;
			return $this;
		}
		
		public function setMisc_wiper_system($text)
		{
			$this->_misc_wiper_system =  $text;
			return $this;
		}
		
		public function setMisc_other_parts($text)
		{
			$this->_misc_other_parts =  $text;
			return $this;
		}
		
		public function setMisc_lubrication_system($text)
		{
			$this->_misc_lubrication_system =  $text;
			return $this;
		}
		
		public function setMisc_seals($text)
		{
			$this->_misc_seals =  $text;
			return $this;
		}
		
		public function setMisc_safety_security($text)
		{
			$this->_misc_safety_security =  $text;
			return $this;
		}
		
		public function setMisc_disclaimer($text)
		{
			$this->_misc_disclaimer =  $text;
			return $this;
		}
		
		public function setPayment_desc($text)
		{
			$this->_payment_desc =  $text;
			return $this;
		}
		
		public function setPayment_terms_policy($text)
		{
			$this->_payment_terms_policy =  $text;
			return $this;
		}
		
		public function setMisc_terms_condition($text)
		{
			$this->_misc_terms_condition =  $text;
			return $this;
		}		
		
		public function setBilling_user_places_order_email_enable($text)
		{
			$this->_billing_user_places_order_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_places_order_email_address($text)
		{
			$this->_billing_user_places_order_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_user_pay_invoice_email_enable($text)
		{
			$this->_billing_user_pay_invoice_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_pay_invoice_email_address($text)
		{
			$this->_billing_user_pay_invoice_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_user_cancel_order_email_enable($text)
		{
			$this->_billing_user_cancel_order_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_cancel_order_email_address($text)
		{
			$this->_billing_user_cancel_order_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_order_payment_email_enable($text)
		{
			$this->_billing_order_payment_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_item_desc($text)
		{
			$this->_billing_item_desc =  $text;
			return $this;
		}
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getGroup_id()
		{         
			return $this->_group_id;
		}
		
		public function getCategory_id()
		{
			return $this->_category_id;
		}
		
		public function getBrand_id()
		{
			return $this->_brand_id;
		}
		
		public function getAutos_model_name()
		{
			return $this->_autos_model_name;
		}
		
		public function getAutos_model_title()
		{
			return $this->_autos_model_title;
		}
		
		public function getAutos_agent()
		{
			return $this->_autos_agent;
		}
		
		public function getAutos_price()
		{         
			return $this->_autos_price;
		}

		public function getAutos_strickthrow_price()
		{
			return $this->_autos_strickthrow_price;
		}
		
		
		public function getRelated_items()
		{
			return $this->_related_items;
		}
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getAutos_primary_image()
		{
			return $this->_autos_primary_image;
		}
		
		public function getAutos_image()
		{         
			return $this->_autos_image;
		}
		
		
		public function getAutos_type()
		{         
			return $this->_autos_type = ($this->_autos_type)? $this->_autos_type : '0';
		}
		
		public function getPrev_category()
		{
			return $this->_prev_category;
		}
		
		public function getPrev_group()
		{
			return $this->_prev_group;
		}
		
		public function getAutos_order()
		{   
			if(empty($this->_autos_order))
			{ 
				$this->setAutos_order(''); 
			}    
			return $this->_autos_order;
		}
		
		public function getStars()
		{
			return $this->_stars;
		}
				
		public function getAutos_desc()
		{         
			return $this->_autos_desc;
		}
		
		public function getFeatured()
		{         
			return $this->_featured;
		}
		
		public function getActive()
		{         
			return $this->_active;
		}		
		
		public function getMeta_title()
		{
			return $this->_meta_title;
		}
		
		public function getMeta_keywords()
		{
			return $this->_meta_keywords;
		}
		
		public function getMeta_desc()
		{
			return $this->_meta_desc;
		}
		
		public function getCountry_id()
		{
			return $this->_country_id = ($this->_country_id)? $this->_country_id : '0';
		}
		
		public function getState_id()
		{
			return $this->_state_id = ($this->_state_id)? $this->_state_id : '0';
		}
		
		public function getArea_id()
		{
			return $this->_area_id = ($this->_area_id)? $this->_area_id : '0';
		}
		
		public function getShowroom_no()
		{
			return $this->_showroom_no;
		}
		
		public function getShowroom_name()
		{
			return $this->_showroom_name;
		}
		
		public function getShowroom_phone()
		{
			return $this->_showroom_phone;
		}
		
		public function getShowroom_fax()
		{
			return $this->_showroom_fax;
		}
		
		public function getShowroom_email()
		{
			return $this->_showroom_email;
		}
		
		public function getPost_code()
		{
			return $this->_post_code;
		}
		
		public function getShowroom_address()
		{
			return $this->_showroom_address;
		}
		
		public function getFeature_mileage()
		{
			return $this->_feature_mileage = ($this->_feature_mileage)? $this->_feature_mileage : '0';
		}
		
		public function getFeature_kilometers()
		{
			return $this->_feature_kilometers;
		}
		
		public function getFeature_at_car_id ()
		{
			return $this->_feature_at_car_id;
		}
		
		public function getFeature_vin ()
		{
			return $this->_feature_vin;
		}
		
		public function getFeature_body_type()
		{
			return $this->_feature_body_type;
		}
		
		public function getFeature_bodystyle()
		{
			return $this->_feature_bodystyle;
		}
		
		public function getFeature_exterior_color()
		{
			return $this->_feature_exterior_color;
		}
		
		public function getFeature_interior_color()
		{
			return $this->_feature_interior_color;
		}
		
		public function getFeature_cargo_area()
		{
			return $this->_feature_cargo_area;
		}
		
		public function getFeature_seats()
		{
			return $this->_feature_seats;
		}
		
		public function getFeature_gauges()
		{
			return $this->_feature_gauges;
		}
		
		public function getFeature_lighting_elements()
		{
			return $this->_feature_lighting_elements;
		}
		
		public function getFeature_engine()
		{
			return $this->_feature_engine;
		}
		
		public function getFeature_engine_cylinder()
		{
			return $this->_feature_engine_cylinder;
		}
		
		public function getFeature_transmission()
		{
			return $this->_feature_transmission;
		}
		
		public function getFeature_wheelbase()
		{
			return $this->_feature_wheelbase;
		}
		
		public function getFeature_drivetrain()
		{
			return $this->_feature_drivetrain;
		}
		
		public function getFeature_drive_type()
		{
			return $this->_feature_drive_type;
		}
		
		public function getFeature_fuel_type()
		{
			return $this->_feature_fuel_type;
		}
		
		public function getFeature_doors()
		{
			return $this->_feature_doors;
		}
		
		public function getFeature_stock_number()
		{
			return $this->_feature_stock_number;
		}
		
		public function getFeature_convenience()
		{
			return $this->_feature_convenience;
		}
		
		public function getFeature_entertainment()
		{
			return $this->_feature_entertainment;
		}
		
		public function getFeature_primary_interior_image()
		{
			return $this->_feature_primary_interior_image;
		}
		
		public function getFeature_interior_plan_image()
		{
			return $this->_feature_interior_plan_image;
		}
		
		public function getFeature_google_map()
		{
			return $this->_feature_google_map;
		}
		
		public function getFeature_additional()
		{
			return $this->_feature_additional;
		}
		
		public function getBrochure_title()
		{
			return $this->_brochure_title;
		}
		
		public function getBrochure_desc()
		{
			return $this->_brochure_desc;
		}
		
		public function getBody_exterior()
		{
			return $this->_body_exterior;
		}
		
		public function getBody_bonnet()
		{
			return $this->_body_bonnet;
		}
		
		public function getBody_bumper()
		{
			return $this->_body_bumper;
		}
		
		public function getBody_doors()
		{
			return $this->_body_doors;
		}
		
		public function getBody_windows()
		{
			return $this->_body_windows;
		}
		
		public function getBody_radiator()
		{
			return $this->_body_radiator;
		}
		
		public function getBody_roof_rack()
		{
			return $this->_body_roof_rack;
		}
		
		public function getBody_spoiler()
		{
			return $this->_body_spoiler;
		}
		
		public function getBody_carpet()
		{
			return $this->_body_carpet;
		}
		
		public function getBody_center_console()
		{
			return $this->_body_center_console;
		}
		
		public function getBody_seats()
		{
			return $this->_body_seats;
		}
		
		public function getBody_speakers()
		{
			return $this->_body_speakers;
		}
		
		public function getBody_entertainment()
		{
			return $this->_body_entertainment;
		}
		
		public function getBody_convenience()
		{
			return $this->_body_convenience;
		}
		
		public function getBody_lighting()
		{
			return $this->_body_lighting;
		}
		
		public function getBody_instrumentation()
		{
			return $this->_body_instrumentation;
		}
		
		public function getBody_safety()
		{
			return $this->_body_safety;
		}
		
		public function getBody_suspension()
		{
			return $this->_body_suspension;
		}
		
		public function getBody_short_desc()
		{
			return $this->_body_short_desc;
		}
		
		public function getPower_braking_system()
		{
			return $this->_power_braking_system;
		}
		
		public function getPower_adjusting_mechanism()
		{
			return $this->_power_adjusting_mechanism;
		}
		
		public function getPower_engine_cooling_system()
		{
			return $this->_power_engine_cooling_system;
		}
		
		public function getPower_engine_oil_system()
		{
			return $this->_power_engine_oil_system;
		}
		
		public function getPower_engine_components()
		{
			return $this->_power_engine_components;
		}
		
		public function getPower_engine_parts()
		{
			return $this->_power_engine_parts;
		}
		
		public function getPower_engine_others()
		{
			return $this->_power_engine_others;
		}
		
		public function getPower_fuel_supply_system()
		{
			return $this->_power_fuel_supply_system;
		}
		
		public function getPower_chassis_frame()
		{
			return $this->_power_chassis_frame;
		}
		
		public function getPower_suspension()
		{
			return $this->_power_suspension;
		}
		
		public function getPower_steering_system()
		{
			return $this->_power_steering_system;
		}
		
		public function getPower_transmission_system()
		{
			return $this->_power_transmission_system;
		}
		
		public function getPower_camshaft()
		{
			return $this->_power_camshaft;
		}	
		
		public function getPower_cylinder()
		{
			return $this->_power_cylinder;
		}	
		
		public function getPower_wheels()
		{
			return $this->_power_wheels;
		}	
		
		public function getPower_tire_parts()
		{
			return $this->_power_tire_parts;
		}		
		
		public function getPower_connecting_rod()
		{
			return $this->_power_connecting_rod;
		}		
		
		public function getPower_distributor()
		{
			return $this->_power_distributor;
		}		
		
		public function getPower_harmonic_balancer()
		{
			return $this->_power_harmonic_balancer;
		}		
		
		public function getPower_piston()
		{
			return $this->_power_piston;
		}		
		
		public function getPower_rocker_details()
		{
			return $this->_power_rocker_details;
		}		
		
		public function getPower_exhaust_system()
		{
			return $this->_power_exhaust_system;
		}	
		
		public function getPower_gear()
		{
			return $this->_power_gear;
		}	
		
		public function getPower_clutch_assembly()
		{
			return $this->_power_clutch_assembly;
		}	
		
		public function getPower_train_desc()
		{
			return $this->_power_train_desc;
		}	
		
		public function getElelectrical_audio_video_device()
		{
			return $this->_elelectrical_audio_video_device;
		}	
		
		public function getElelectrical_charging_system()
		{
			return $this->_elelectrical_charging_system;
		}	
		
		public function getElelectrical_window_system()
		{
			return $this->_elelectrical_window_system;
		}	
		
		public function getElelectrical_alternator()
		{
			return $this->_elelectrical_alternator;
		}	
		
		public function getElelectrical_supply_system()
		{
			return $this->_elelectrical_supply_system;
		}	
		
		public function getElelectrical_battery()
		{
			return $this->_elelectrical_battery;
		}	
		
		public function getElelectrical_gauges_meters()
		{
			return $this->_elelectrical_gauges_meters;
		}	
		
		public function getElelectrical_ignition_system()
		{
			return $this->_elelectrical_ignition_system;
		}	
		
		public function getElelectrical_signaling_system()
		{
			return $this->_elelectrical_signaling_system;
		}	
		
		public function getElelectrical_sensors()
		{
			return $this->_elelectrical_sensors;
		}	
		
		public function getElelectrical_starting_system()
		{
			return $this->_elelectrical_starting_system;
		}	
		
		public function getElelectrical_switches()
		{
			return $this->_elelectrical_switches;
		}	
		
		public function getElelectrical_wiring_harnesses()
		{
			return $this->_elelectrical_wiring_harnesses;
		}	
		
		public function getElelectrical_engine_control_unit()
		{
			return $this->_elelectrical_engine_control_unit;
		}	
		
		public function getElelectrical_internet_facilities()
		{
			return $this->_elelectrical_internet_facilities;
		}	
		
		public function getElelectrical_temperature_control()
		{
			return $this->_elelectrical_temperature_control;
		}	
		
		public function getElelectrical_system()
		{
			return $this->_elelectrical_system;
		}	
		
		public function getElelectrical_short_desc()
		{
			return $this->_elelectrical_short_desc;
		}	
		
		public function getEquipment_installed()
		{
			return $this->_equipment_installed;
		}	
		
		public function getEquipment_options()
		{
			return $this->_equipment_options;
		}
		
		public function getAvailabe_from()
		{
			return $this->_availabe_from = ($this->_availabe_from)? $this->_availabe_from : '0000-00-00';
		}
		
		public function getAvailable_to()
		{
			return $this->_available_to = ($this->_available_to)? $this->_available_to : '0000-00-00';
		}
		
		public function getAvailable_expire_date()
		{
			return $this->_available_expire_date = ($this->_available_expire_date)? $this->_available_expire_date : '0000-00-00';
		}
		
		public function getAvailable_activation_date()
		{
			return $this->_available_activation_date = ($this->_available_activation_date)? $this->_available_activation_date : '0000-00-00';
		}
		
		public function getMeter_electric()
		{
			return $this->_meter_electric;
		}
		
		public function getMeter_electric_expire()
		{
			return $this->_meter_electric_expire = ($this->_meter_electric_expire)? $this->_meter_electric_expire : '0000-00-00';
		}
		
		public function getMeter_gas()
		{
			return $this->_meter_gas;
		}
		
		public function getMeter_gas_expire()
		{
			return $this->_meter_gas_expire = ($this->_meter_gas_expire)? $this->_meter_gas_expire : '0000-00-00';
		}
		
		public function getMeter_energy()
		{
			return $this->_meter_energy;
		}
		
		public function getMeter_energy_expire()
		{
			return $this->_meter_energy_expire = ($this->_meter_energy_expire )? $this->_meter_energy_expire  : '0000-00-00';
		}
		
		public function getMisc_ac_system()
		{
			return $this->_misc_ac_system;
		}
		
		public function getMisc_bearings()
		{
			return $this->_misc_bearings;
		}
		
		public function getMisc_hoses()
		{
			return $this->_misc_hoses;
		}
		
		public function getMisc_wiper_system()
		{
			return $this->_misc_wiper_system;
		}
		
		public function getMisc_other_parts()
		{
			return $this->_misc_other_parts;
		}
		
		public function getMisc_lubrication_system()
		{
			return $this->_misc_lubrication_system;
		}
		
		public function getMisc_seals()
		{
			return $this->_misc_seals;
		}
		
		public function getMisc_safety_security()
		{
			return $this->_misc_safety_security;
		}
		
		public function getMisc_disclaimer()
		{
			return $this->_misc_disclaimer;
		}
		
		public function getPayment_desc()
		{
			return $this->_payment_desc;
		}
		
		public function getPayment_terms_policy()
		{
			return $this->_payment_terms_policy;
		}
		
		public function getMisc_terms_condition()
		{
			return $this->_misc_terms_condition;
		}
			
		
	public function getBilling_user_places_order_email_enable()
	{
		return $this->_billing_user_places_order_email_enable;
	}		
	
	public function getBilling_user_places_order_email_address()
	{
		return $this->_billing_user_places_order_email_address;
	}		
	
	public function getBilling_user_pay_invoice_email_enable()
	{
		return $this->_billing_user_pay_invoice_email_enable;
	}		
	
	public function getBilling_user_pay_invoice_email_address()
	{
		return $this->_billing_user_pay_invoice_email_address;
	}		
	
	public function getBilling_user_cancel_order_email_enable()
	{
		return $this->_billing_user_cancel_order_email_enable;
	}		
	
	public function getBilling_user_cancel_order_email_address()
	{
		return $this->_billing_user_cancel_order_email_address;
	}		
	
	public function getBilling_order_payment_email_enable()
	{
		return $this->_billing_order_payment_email_enable;
	}		
	
	public function getBilling_item_desc()
	{
		return $this->_billing_item_desc;
	}
}
?>