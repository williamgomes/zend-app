<?php
/**
* This is the DbTable class for the autos_group table.
*/
class Autos_Model_DbTable_AutosGroup extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_group';		
	protected $_cols	=	null;
		
	
	//Get Datas
	public function getAllGroupInfo() 
    {
       $select = $this->select()
                       ->from($this->_name, array('*'))
                       ->order('id ASC'); 
		 $options = $this->fetchAll($select);
		if (!$options) 
		{
           // throw new Exception("Count not find rows $id");
		   $options = null;
        }
        return $options;   
    }
	
	//Get Datas
	public function getGroupInfo() 
    {
       $select = $this->select()
                       ->from($this->_name, array('id', 'autos_name'))
                       ->order('autos_name ASC'); 
		 $options = $this->getAdapter()->fetchPairs($select);
		if (!$options) 
		{
            //throw new Exception("Count not find rows $id");
			$options = null;
        }
		else
		{
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;   
    }
	
	//Get Datas
	public function getGroupName($group_id) 
    {
		$group_id = (int)$group_id;
        $row = $this->fetchRow('id = ' . $group_id);
        if (!$row) 
		{
           // throw new Exception("Count not find row $group_id");
		   $group_name = null;
        }
		else
		{
			$group_name = $row->toArray();
			$group_name = is_array($group_name) ? array_map('stripslashes', $group_name) : stripslashes($group_name);
		}
        return $group_name;   
    }	

	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('ag' => $this->_name),array('ag.id', 'group_name' => 'ag.autos_name', 'ag.role_id', 'ag.group_type', 'ag.active', 'ag.entry_by', 'file_num_per_page' => 'ag.file_num_per_page', 'file_col_num' => 'ag.file_col_num', 'file_sort' => 'ag.file_sort', 'file_order' => 'ag.file_order', 'meta_title' => 'ag.meta_title', 'meta_keywords' => 'ag.meta_keywords', 'meta_desc' => 'ag.meta_desc', '(SELECT COUNT(tp.id) FROM '.Zend_Registry::get('dbPrefix').'autos_page as tp WHERE ag.id = tp.group_id) AS autos_num', '(SELECT COUNT(tbt.id) FROM '.Zend_Registry::get('dbPrefix').'autos_business_type as tbt WHERE ag.id = tbt.group_id) AS business_type_num'))
						->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'ag.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))
						->joinLeft(array('ac' => Zend_Registry::get('dbPrefix').'autos_category'), 'ag.id = ac.group_id', array( 'category_num' => 'COUNT(ac.id)'))
						->group('ag.id');
							
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('ag.entry_by = ?', $user_id);
		}
		
		if($approve != null)
		{
			$select->where("ag.active = ?", $approve);
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}
				}
			}
			else
			{
				$select->order("ag.id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{						
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("ag.id ASC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options; 
	}	
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ag.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'group_name' :
					$operatorFirstPart = " ag.autos_name ";
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(ac.cat_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(ac.cat_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(ac.cat_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}

?>