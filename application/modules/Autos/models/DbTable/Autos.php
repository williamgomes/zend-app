<?php

/**
 * This is the DbTable class for the autos_page table.
 */
class Autos_Model_DbTable_Autos extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'autos_page';
    protected $_cols = null;

    //Get Datas
    public function getAutosInfo($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            // throw new Exception("Count not find row=$id");
            $options = null;
        } else {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
        return $options;
    }

    public function getOptionsByBrand($brand_id = null) {
        if ($brand_id) {
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('ap' => $this->_name), array('DISTINCT(ap.category_id)'))
                    ->where('ap.brand_id =?', $brand_id)
                    ->joinLeft(array('ac' => Zend_Registry::get('dbPrefix') . 'autos_category'), 'ac.id = ap.category_id', array('category_id' => 'ac.id', 'category_name' => 'ac.category_name'))
                    ->order('ac.category_name ASC');
        } else {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->order('id ASC');
        }
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    public function getOptionsByCategory($category_id = null, $brand_id = null) {
        if ($category_id) {
            $select = $this->select()
                    ->from(array('ap' => $this->_name), array('id' => 'ap.id', 'autos_model_name' => 'ap.autos_model_name'))
                    ->where('ap.brand_id =?', $brand_id)
                    ->where('ap.category_id =?', $category_id)
                    ->order('ap.autos_model_name ASC');
        } else {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->order('id ASC');
            if (!empty($brand_id)) {
                $select->where('brand_id =?', $brand_id);
            }
        }
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    //Get Datas
    public function getTitleToId($autos_model_title) {
        $autos_model_title = addslashes($autos_model_title);
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.autos_model_title =? ', $autos_model_title)
                    ->limit(1);
            $options = $this->fetchAll($select);
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Price
    public function getAutosMatchPrice($current_id, $autos_price, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.active = ?', '1')
                    ->where('gp.autos_price < (' . $autos_price . ' + (' . $autos_price . '*20/100))')
                    ->where('gp.autos_price > (' . $autos_price . ' - (' . $autos_price . '*20/100))')
                    ->limit($limit);

            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Region
    public function getAutosMatchRegion($current_id, $state_id, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.active = ?', '1')
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.state_id = ?', $state_id)
                    ->limit($limit);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Agent
    public function getAutosMatchAgent($current_id, $autos_agent, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.active = ?', '1')
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.autos_agent = ?', $autos_agent)
                    ->limit($limit);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Next Autos
    public function getNextAutos($autos_id, $group_id, $type_id = null) {
        try {
            $select = ($type_id) ?
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id >? ', $autos_id)
                            ->where('gp.autos_type =? ', $type_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id ASC')
                            ->limit(1) :
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id > ? ', $autos_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id ASC')
                            ->limit(1);

            $row = $this->getAdapter()->fetchAll($select);
            if ($row) {
                $options = $row[0];
            } else {
                $options = '';
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Previous Autos
    public function getPrevAutos($autos_id, $group_id, $type_id = null) {
        try {
            $select = ($type_id) ?
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id <? ', $autos_id)
                            ->where('gp.autos_type =? ', $type_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id DESC')
                            ->limit(1) :
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id <? ', $autos_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id DESC')
                            ->limit(1);
            $row = $this->getAdapter()->fetchAll($select);
            if ($row) {
                $options = $row[0];
            } else {
                $options = '';
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get All Datas
    public function getAutosInfoByGroup($group_id = null, $approve = null) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $approve = ($approve == null) ? '1=1' : 'gp.active = "' . $approve . '"';

        if (empty($group_id)) {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.entry_by = ?', $user_id)
                        ->orwhere('gp.autos_agent = ?', $user_id)
                        ->where($approve)
                        ->order('gp.group_id ASC')
                        ->order("gp.category_id ASC")
                        ->order('gp.autos_order ASC');
            } else {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where($approve)
                        ->order('gp.group_id ASC')
                        ->order("gp.category_id ASC")
                        ->order('gp.autos_order ASC');
            }
        } else {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.entry_by = ?', $user_id)
                        ->orwhere('gp.autos_agent = ?', $user_id)
                        ->where('gp.group_id =?', $group_id)
                        ->where($approve)
                        ->order("gp.category_id ASC")
                        ->order('gp.autos_order ASC');
            } else {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.group_id =?', $group_id)
                        ->where($approve)
                        ->order("gp.category_id ASC")
                        ->order('gp.autos_order ASC');
            }
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            //throw new Exception("Count not find row Category");
            $options = null;
        }
        return $options;
    }

    public function getBorderItems($field = 'autos_price', $range = 'DESC', $active = '1') {
        try {
            $select = $this->select()
                    ->from(array('ap' => $this->_name), array($field => 'ap.' . $field))
                    ->where('ap.active =? ', $active)
                    ->order('ap.' . $field . ' ' . $range)
                    ->limit(1);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options[0][$field];
            } else {
                return '0';
            }
        } catch (Exception $e) {
            return '0';
        }
    }

    public function numOfAutos($condition = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $select = $this->select()->from(array('p' => $this->_name), array('COUNT(*) AS num_autos'));
        if ($auth->hasIdentity() && ($userChecking == true )) {
            $identity = $auth->getIdentity();
            $user_id = $identity->user_id;
            $select->where('p.autos_agent = ' . $user_id . ' OR p.entry_by = ' . $user_id);
        }

        if ($condition && is_array($condition)) {
            $select->where('p.' . $condition['field'] . ' ' . $condition['operator'] . ' ' . $condition['value']);
        }

        $options = $this->fetchAll($select);
        return ($options) ? $options[0]['num_autos'] : 0;
    }

    public function getNumInActiveAutos($group_id = null) {
        if (empty($group_id)) {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('COUNT(gp.id) as num'))
                    ->where('gp.active =?', '0');
        } else {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('COUNT(gp.id) as num'))
                    ->where('gp.group_id =?', $group_id)
                    ->where('gp.active =?', '0');
        }


        $options = $this->fetchAll($select);
        if (!$options) {
            $num_of_news = 0;
        } else {
            foreach ($options as $row) {
                $num_of_news = $row['num'];
            }
        }
        return $num_of_news;
    }

    //Get Related Items
    public function getRelatedItems() {
        $select = $this->select()
                ->from($this->_name, array('id', 'autos_model_name'))
                ->where('active = ?', '1')
                ->order('autos_model_name ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        if ($options) {
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        } else {
            //throw new Exception("Count not find rows $id"); 
            $options = null;
        }
        return $options;
    }

    //Get Mileage
    public function getMileage($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_mileage' => 'DISTINCT(feature_mileage)'))
                ->order('feature_mileage ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Fuel Type
    public function getFuelType($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_fuel_type' => 'DISTINCT(feature_fuel_type)'))
                ->order('feature_fuel_type ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Exterior Color
    public function getExteriorColor($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_exterior_color' => 'DISTINCT(feature_exterior_color)'))
                ->order('feature_exterior_color ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Doors
    public function getDoors($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_doors' => 'DISTINCT(feature_doors)'))
                ->order('feature_doors ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Doors
    public function getBodyStyle($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_bodystyle' => 'DISTINCT(feature_bodystyle)'))
                ->order('feature_bodystyle ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Doors
    public function getDriveType($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_drive_type' => 'DISTINCT(feature_drive_type)'))
                ->order('feature_drive_type ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Engine
    public function getEngineSize($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_engine' => 'DISTINCT(feature_engine)'))
                ->order('feature_engine ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Transmission
    public function getTransmission($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_transmission' => 'DISTINCT(feature_transmission)'))
                ->order('feature_transmission ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Price
    public function getPrice($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('autos_price' => 'DISTINCT(autos_price)'))
                ->order('autos_price ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get Models For Frontend
    public function getFrontendModels($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('active =?', '1')
                ->order('autos_model_name ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get All Datas
    public function getListInfo($approve = null, $search_params = null, $tableColumns = null) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $autos_page_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_page'] && is_array($tableColumns['autos_page'])) ? $tableColumns['autos_page'] : array('ap.*');
        $autos_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_business_type'] && is_array($tableColumns['autos_business_type'])) ? $tableColumns['autos_business_type'] : array('business_type' => 'abt.business_type');
        $autos_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_category'] && is_array($tableColumns['autos_category'])) ? $tableColumns['autos_category'] : array('category_name' => 'ac.category_name');
        $cities_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['cities'] && is_array($tableColumns['cities'])) ? $tableColumns['cities'] : array('city' => 'ct.city');
        $countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array('country_name' => 'cut.value');
        $states_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['states'] && is_array($tableColumns['states'])) ? $tableColumns['states'] : array('state_name' => 'st.state_name');
        $vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
        $autos_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_group'] && is_array($tableColumns['autos_group'])) ? $tableColumns['autos_group'] : array('group_name' => 'ag.autos_name', 'review_id' => 'ag.review_id', 'file_thumb_width' => 'ag.file_thumb_width', 'file_thumb_height' => 'ag.file_thumb_height', 'file_thumb_resize_func' => 'ag.file_thumb_resize_func');
        $autos_brand_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_brand'] && is_array($tableColumns['autos_brand'])) ? $tableColumns['autos_brand'] : array('brand_name' => 'ab.brand_name');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array('username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
        $owner_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_profile'] && is_array($tableColumns['owner_profile'])) ? $tableColumns['owner_profile'] : array('owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) ", 'owner_email' => 'ups.username', 'owner_id' => 'ups.user_id', 'owner_phone' => 'ups.phone', 'owner_postalCode' => 'ups.postalCode', 'owner_address' => 'ups.address', 'country' => 'ups.country');
        $owner_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_country'] && is_array($tableColumns['owner_country'])) ? $tableColumns['owner_country'] : null;
        $dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
        $userChecking = ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
        $whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('ap' => $this->_name), $autos_page_column_arr);

        if (($autos_group_column_arr && is_array($autos_group_column_arr) && count($autos_group_column_arr) > 0)) {
            $select->joinLeft(array('ag' => Zend_Registry::get('dbPrefix') . 'autos_group'), 'ap.group_id = ag.id', $autos_group_column_arr);
        }
        if (($autos_brand_column_arr && is_array($autos_brand_column_arr) && count($autos_brand_column_arr) > 0)) {
            $select->joinLeft(array('ab' => Zend_Registry::get('dbPrefix') . 'autos_brand'), 'ap.brand_id = ab.id', $autos_brand_column_arr);
        }

        if (($autos_business_type_arr && is_array($autos_business_type_arr) && count($autos_business_type_arr) > 0)) {
            $select->joinLeft(array('abt' => Zend_Registry::get('dbPrefix') . 'autos_business_type'), 'abt.id = ap.autos_type', $autos_business_type_arr);
        }

        if (($autos_category_column_arr && is_array($autos_category_column_arr) && count($autos_category_column_arr) > 0)) {
            $select->joinLeft(array('ac' => Zend_Registry::get('dbPrefix') . 'autos_category'), 'ac.id = ap.category_id', $autos_category_column_arr);
        }

        if (($cities_column_arr && is_array($cities_column_arr) && count($cities_column_arr) > 0)) {
            $select->joinLeft(array('ct' => Zend_Registry::get('dbPrefix') . 'cities'), 'ct.city_id = ap.area_id', $cities_column_arr);
        }

        if (($states_column_arr && is_array($states_column_arr) && count($states_column_arr) > 0)) {
            $select->joinLeft(array('st' => Zend_Registry::get('dbPrefix') . 'states'), 'st.state_id = ap.state_id', $states_column_arr);
        }

        if (($countries_column_arr && is_array($countries_column_arr) && count($countries_column_arr) > 0)) {
            $select->joinLeft(array('cut' => Zend_Registry::get('dbPrefix') . 'countries'), 'cut.id = ap.country_id', $countries_column_arr);
        }

        if (($user_profile_column_arr && is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'ap.entry_by = up.user_id', $user_profile_column_arr);
        }

        if (($owner_profile_column_arr && is_array($owner_profile_column_arr) && count($owner_profile_column_arr) > 0)) {
            $select->joinLeft(array('ups' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'ap.autos_agent = ups.user_id', $owner_profile_column_arr);
        }

        if (($owner_country_column_arr && is_array($owner_country_column_arr) && count($owner_country_column_arr) > 0)) {
            $select->joinLeft(array('ocut' => Zend_Registry::get('dbPrefix') . 'countries'), 'ocut.id = ups.country', $owner_country_column_arr);
        }

        if (($vote_column_arr && is_array($vote_column_arr) && count($vote_column_arr) > 0)) {
            $select->joinLeft(array('vt' => Zend_Registry::get('dbPrefix') . 'vote_voting'), 'ap.id  = vt.table_id', $vote_column_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('ap.entry_by = ' . $user_id . ' OR ap.autos_agent = ' . $user_id);
        }

        if ($approve != null) {
            $select->where("ap.active = ?", $approve);
        }

        if (($whereClause && is_array($whereClause) && count($whereClause) > 0)) {

            foreach ($whereClause as $filter => $param) {
                $select->where($filter, $param);
            }
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    if ($sort_value_arr['dir'] == 'exp') {
                        $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                    } else {
                        $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                    }
                }
            } else {
                $select->order("ap.category_id ASC")
                        ->order('ap.autos_order ASC');
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("ap.category_id ASC")
                    ->order('ap.autos_order ASC');
        }

        if (!empty($dataLimit)) {
            $select->limit($dataLimit);
        }
        file_put_contents('abc.txt', $select);
        $options = $this->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ap.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'autos_id_string' :
                $id_arr = explode(',', $operator_arr['value']);
                $id_arr = array_filter($id_arr);
                $ids = implode(',', $id_arr);
                $operatorFirstPart = " ap.id IN(" . $ids . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'stars_arr':
                $stars_arr = explode(',', $operator_arr['value']);
                $stars_arr = array_filter($stars_arr);
                $stars_string = implode(',', $stars_arr);
                $operatorFirstPart = " ap.stars IN(" . $stars_string . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'category_id_arr':
                $category_id_arr = explode(',', $operator_arr['value']);
                $category_id_arr = array_filter($category_id_arr);
                $category_id_string = implode(',', $category_id_arr);
                $operatorFirstPart = " ap.category_id IN(" . $category_id_string . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'group_id_arr':
                $group_id_arr = explode(',', $operator_arr['value']);
                $group_id_arr = array_filter($group_id_arr);
                $group_id_string = implode(',', $group_id_arr);
                $operatorFirstPart = " ap.group_id IN(" . $group_id_string . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'equipment_installed':
                $equipment_installed_arr = explode(',', $operator_arr['value']);
                $equipment_installed_arr = array_filter($equipment_installed_arr);
                foreach ($equipment_installed_arr as $key => $value) {
                    $operatorFirstPartArr[$key] = 'ap.equipment_installed LIKE "%' . $value . '%"';
                }
                $operatorFirstPart = " (" . implode(' AND ', $operatorFirstPartArr) . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'equipment_options':
                $equipment_options_arr = explode(',', $operator_arr['value']);
                $equipment_options_arr = array_filter($equipment_options_arr);
                foreach ($equipment_options_arr as $key => $value) {
                    $operatorFirstPartArr[$key] = 'ap.equipment_options LIKE "%' . $value . '%"';
                }
                $operatorFirstPart = " (" . implode(' AND ', $operatorFirstPartArr) . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'autos_price-gte' :
                $operator_arr['field'] = 'autos_price';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'autos_price-lte' :
                $operator_arr['field'] = 'autos_price';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_mileage-gte' :
                $operator_arr['field'] = 'feature_mileage';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_mileage-lte' :
                $operator_arr['field'] = 'feature_mileage';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_doors-lte' :
                $operator_arr['field'] = 'feature_doors';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_doors-gte' :
                $operator_arr['field'] = 'feature_doors';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'groups' :
                $operatorFirstPart = " ap.group_id IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'categories' :
                $operatorFirstPart = " ap.category_id IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'businesstype' :
                $operatorFirstPart = " ap.autos_type IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'city' :
                $operatorFirstPart = " ct.city ";
                break;
            case 'country_name' :
                $operatorFirstPart = " cut.value ";
                break;
            case 'autos_location':
                $operatorFirstPart = '(ap.showroom_address LIKE "%' . $operator_arr['value'] . '%" OR ct.city LIKE "' . $operator_arr['value'] . '%" OR cut.value LIKE "' . $operator_arr['value'] . '%") AND 1';
                $operator_arr['value'] = '1';
                break;
            case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM " . Zend_Registry::get('dbPrefix') . "vote_voting as vts WHERE ap.id  = vts.table_id) AS total_votes ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(ap.autos_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(ap.autos_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(ap.autos_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>