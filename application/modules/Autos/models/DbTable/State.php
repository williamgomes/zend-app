<?php
/**
* This is the DbTable class for the states table.
*/
class Autos_Model_DbTable_State extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'states';
	
	//Get Datas
	public function getStateInfo($state_id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('state_id = ' . $state_id);
        if (!$row) 
		{
           // throw new Exception("Count not find row $id");
		   $options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ; 
    }	
	
		
	//Get Datas
	public function getAllStateInfo($country_id = null)
    {
		if(empty($country_id))
		{
			$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('st' => $this->_name), array('st.state_id','st.country_id', 'st.state_name', 'country_name' => 'c.value'))
						   ->order('st.state_id ASC')
						   ->joinLeft(array('c' => Zend_Registry::get('dbPrefix').'countries'), 'c.id = st.country_id');			
		}
		else
		{
			$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('st' => $this->_name), array('st.state_id','st.country_id', 'st.state_name', 'country_name' => 'c.value'))
						   ->where('st.country_id = ?',$country_id)
						   ->order('st.state_id ASC')
						   ->joinLeft(array('c' => Zend_Registry::get('dbPrefix').'countries'), 'c.id = st.country_id');	
		}
		$options = $this->fetchAll($select);        
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($country_id = null)
    {
		if($country_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('state_id', 'state_name'))
						   ->where('country_id =?',$country_id)
						   ->order('state_name ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('state_id', 'state_name'))
						   ->order('state_name ASC');			
		} 
		$options = $this->getAdapter()->fetchPairs($select); 
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);        
        return $options;
    }	
}
?>