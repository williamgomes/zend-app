<?php

/**
 * This is the DbTable class for the autos_category table.
 */
class Autos_Model_DbTable_Category extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'autos_category';
    protected $_cols = null;

    //Get Datas
    public function getCategoryInfo($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if ($row) {
            // throw new Exception("Count not find row $id");
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        } else {
            $options = null;
        }

        return $options;
    }

    //Get Datas
    public function getCategoryInfoFromTitle($category_title) {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('category_title =?', $category_title)
                ->limit(1);
        $options = $this->fetchAll($select);
        if (!$options) {
            // throw new Exception("Count not find row Category");
            $options = null;
        }
        return $options[0];
    }

    //Get All Datas
    public function getAllParentCategoryInfo($group_id = null) {
        if (empty($group_id)) {
            $select = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id', 'gc.group_id', 'gc.category_name', 'gc.category_thumb', 'gc.category_order', 'gc.active', 'gc.featured', 'gc.cat_date', 'gc.entry_by'))
                    ->where("gc.parent = ?", '0')
                    ->order('gc.group_id ASC')
                    ->order('gc.category_order ASC');
        } else {
            $select = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id', 'gc.group_id', 'gc.category_name', 'gc.category_thumb', 'gc.category_order', 'gc.active', 'gc.featured', 'gc.cat_date', 'gc.entry_by'))
                    ->where("gc.parent = ?", '0')
                    ->where("gc.group_id = ?", $group_id)
                    ->order('gc.category_order ASC');
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            // throw new Exception("Count not find row Category");
            $options = null;
        }
        return $options;
    }

    //Get Datas
    public function getOptions($group_id = null) {
        if ($group_id) {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->where('group_id =?', $group_id)
                    ->order('id ASC');
        } else {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->order('id ASC');
        }
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    public function getCheckChild($parent) {
        if (!empty($parent)) {
            $validator = new Zend_Validate_Db_RecordExists(
                    array(
                'table' => $this->_name,
                'field' => 'parent'
                    )
            );
            $rs = ($validator->isValid($parent)) ? true : false;
        }
        return $rs;
    }

    public function getAllSubCategoryId($parent) {
        if (!empty($parent)) {
            $selectSub = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id'))
                    ->where('gc.parent = ?', $parent);
            $rs = $selectSub->query()->fetchAll();
            if ($rs) {
                foreach ($rs as $options) {
                    $id .= $options['id'] . ',';
                    if ($this->getCheckChild($options['id'])) {
                        $id .= $this->getAllSubCategoryId($options['id']);
                    }
                }
            }
        }
        return $id;
    }

    public function checkSubCategory($parent) {
        if (!empty($parent)) {
            $validator = new Zend_Validate_Db_RecordExists(
                    array(
                'table' => $this->_name,
                'field' => 'parent'
                    )
            );
            $rs = ($validator->isValid($parent)) ? true : false;
        }
        return $rs;
    }

    //Get All Datas
    public function getListInfo($approve = null, $search_params = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('ac' => $this->_name), array('ac.*'))
                ->joinLeft(array('ag' => Zend_Registry::get('dbPrefix') . 'autos_group'), 'ac.group_id = ag.id', array('group_name' => 'ag.autos_name', 'file_num_per_page' => 'ag.file_num_per_page', 'file_col_num' => 'ag.file_col_num', 'file_sort' => 'ag.file_sort', 'file_order' => 'ag.file_order', 'cat_sort' => 'ag.cat_sort', 'cat_order' => 'ag.cat_order', 'group_meta_title' => 'ag.meta_title', 'group_meta_keywords' => 'ag.meta_keywords', 'group_meta_desc' => 'ag.meta_desc', 'role_id' => 'ag.role_id'))
                ->joinLeft(array('acp' => $this->_name), 'ac.parent = acp.id', array('parent_name' => 'acp.category_name'))
                ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'ac.entry_by = up.user_id', array('username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))
                ->joinLeft(array('ap' => Zend_Registry::get('dbPrefix') . 'autos_page'), 'ac.id = ap.category_id', array('autos_num' => 'COUNT(ap.id)'))
                ->group('ac.id');

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('ac.entry_by = ?', $user_id);
        }

        if ($approve != null) {
            $select->where("ac.active = ?", $approve);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    if ($sort_value_arr['dir'] == 'exp') {
                        $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                    } else {
                        $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                    }
                }
            } else {
                $select->order("ac.group_id ASC")
                        ->order('ac.category_order ASC');
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $hasChild = ($filter_obj['field'] == 'parent') ? true : $hasChild;
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i++;
                    } else if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                            $hasChild = ($sub_filter_obj['field'] == 'parent') ? true : $hasChild;
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                            $sub++;
                        }
                        $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                        $i++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("ac.group_id ASC")
                    ->order('ac.category_order ASC');
        }

        if ($hasChild === false) {
            $select->where("ac.parent = ?", '0');
        }

        $options = $this->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    private function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ac.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);

                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'group_name' :
                $operatorFirstPart = " ag.autos_name ";
                break;
            case 'autos_num' :
                $operatorFirstPart = ' ( SELECT COUNT(aps.id) FROM ' . Zend_Registry::get('dbPrefix') . 'autos_page AS aps WHERE aps.category_id = ac.id ) ';
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(ac.cat_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(ac.cat_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(ac.cat_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    private function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>