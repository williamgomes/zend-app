<?php
/**
* This is the DbTable class for the cities table.
*/
class Autos_Model_DbTable_Area extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'cities';
	
	//Get Datas
	public function getAreaInfo($city_id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('city_id = ' . $city_id);
        if (!$row) 
		{
            //throw new Exception("Can't not find data of city_id = $city_id");
			$options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
		return   $options ; 
    }	
	
		
	//Get Datas
	public function getAllAreaInfo($state_id = null)
    {
		if(empty($state_id))
		{
			$select = $this->select()
							->setIntegrityCheck(false)
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->order('ct.city_id ASC')
						   ->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'st.state_id = ct.state_id', array('state_name' => 'st.state_name'));				
		}
		else
		{
			$select = $this->select()
							->setIntegrityCheck(false)
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->where('ct.state_id = ?',$state_id)
						   ->order('ct.city_id ASC')
						   ->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'st.state_id = ct.state_id', array('state_name' => 'st.state_name'));		
		}
		$options = $this->fetchAll($select);        
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($state_id = null)
    {
		if($state_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('city_id', 'city'))
						   ->where('state_id =?',$state_id)
						   ->order('city ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('city_id', 'city'))
						   ->order('city ASC');			
		} 
		$options = $this->getAdapter()->fetchPairs($select); 
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);       
        return $options;
    }
	
}

?>