<?php
class Autos_Model_SparepartsMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_Spareparts');
        }
        return $this->_dbTable;
    }
	
	public function save(Autos_Model_Spareparts $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				unset($data['id']);
				
				$data = array(					
					'spare_parts_name' 					=>	$obj->getSpare_parts_name(),
					'spare_parts_title' 				=>	$obj->getSpare_parts_title(),
					'spare_parts_number' 				=>	$obj->getSpare_parts_number(),
					'spare_parts_manufacturer_number' 	=>	$obj->getSpare_parts_manufacturer_number(),
					'autos_agent' 						=>	$obj->getAutos_agent(),
					'spare_parts_description' 			=>	$obj->getSpare_parts_description(),	
					'spare_parts_item_condition' 		=>	$obj->getSpare_parts_item_condition(),
					'spare_parts_base_price' 			=>	$obj->getSpare_parts_base_price(),	
					'spare_parts_promotion_price' 		=>	$obj->getSpare_parts_promotion_price(),					
					'autos_id'							=>	$obj->getAutos_id(),
					'spare_parts_image_primary'			=>	$obj->getSpare_parts_image_primary(),
					'spare_parts_image' 				=>	$obj->getSpare_parts_image(),
					'entry_by' 							=>	$obj->getEntry_by(),
					'spare_parts_delivery' 				=>	$obj->getSpare_parts_delivery(),					
					'spare_parts_payment_method'		=>	$obj->getSpare_parts_payment_method(),	
					'spare_parts_returns_refund' 		=>	$obj->getSpare_parts_returns_refund(),					
					'spare_parts_order_qty_min'			=>	$obj->getSpare_parts_order_qty_min(),
					'spare_parts_origin' 				=>	$obj->getSpare_parts_origin(),					
					'spare_parts_mechine_type'			=>	$obj->getSpare_parts_mechine_type(),
					'spare_parts_brand_name' 			=>	$obj->getSpare_parts_brand_name(),					
					'spare_parts_material'				=>	$obj->getSpare_parts_material(),
					'spare_parts_type' 					=>	$obj->getSpare_parts_type(),					
					'spare_parts_protection_guarantee'	=>	$obj->getSpare_parts_protection_guarantee(),
					'spare_parts_specifications' 		=>	$obj->getSpare_parts_specifications(),					
					'spare_parts_terms_condition'		=>	$obj->getSpare_parts_terms_condition(),
					'entry_date' 						=>	date("Y-m-d h:i:s"),				
					'featured' 							=>	$obj->getFeatured(),
					'active' 							=>	$obj->getActive(),
					'meta_title' 						=>	$obj->getMeta_title(),
					'meta_keywords' 					=>	$obj->getMeta_keywords(),
					'meta_desc' 						=>	$obj->getMeta_desc()							
				);
				try 
				{
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{
				/*if(($obj->getCategory_id() == $obj->getPrev_category()) && ($obj->getGroup_id() == $obj->getPrev_group()))
				{
					$data = array(					
						'spare_parts_name' 			=>	$obj->getSpare_parts_name(),
						'spare_parts_title' 		=>	$obj->getSpare_parts_title(),
						'autos_agent' 				=>	$obj->getAutos_agent(),
						//'autos_price' 				=>	$obj->getAutos_price(),					
						'autos_id'					=>	$obj->getAutos_id(),
						'spare_parts_image_primary'	=>	$obj->getSpare_parts_image_primary(),
						'spare_parts_image' 		=>	$obj->getSpare_parts_image(),						
						//'autos_desc' 				=>	$obj->getAutos_desc(),					
						//'autos_order'				=>	$obj->getAutos_order(),				
						'featured' 					=>	$obj->getFeatured(),
						'active' 					=>	$obj->getActive(),
						'meta_title' 				=>	$obj->getMeta_title(),
						'meta_keywords' 			=>	$obj->getMeta_keywords(),
						'meta_desc' 				=>	$obj->getMeta_desc()							
					);
				}
				else
				{*/
					$data = array(					
						'spare_parts_name' 					=>	$obj->getSpare_parts_name(),
						'spare_parts_title' 				=>	$obj->getSpare_parts_title(),
						'spare_parts_number' 				=>	$obj->getSpare_parts_number(),
						'spare_parts_manufacturer_number' 	=>	$obj->getSpare_parts_manufacturer_number(),
						'autos_agent' 						=>	$obj->getAutos_agent(),
						'spare_parts_description' 			=>	$obj->getSpare_parts_description(),	
						'spare_parts_item_condition' 		=>	$obj->getSpare_parts_item_condition(),
						'spare_parts_base_price' 			=>	$obj->getSpare_parts_base_price(),	
						'spare_parts_promotion_price' 		=>	$obj->getSpare_parts_promotion_price(),					
						'autos_id'							=>	$obj->getAutos_id(),
						'spare_parts_image_primary'			=>	$obj->getSpare_parts_image_primary(),
						'spare_parts_image' 				=>	$obj->getSpare_parts_image(),
						'spare_parts_delivery' 				=>	$obj->getSpare_parts_delivery(),					
						'spare_parts_payment_method'		=>	$obj->getSpare_parts_payment_method(),	
						'spare_parts_returns_refund' 		=>	$obj->getSpare_parts_returns_refund(),					
						'spare_parts_order_qty_min'			=>	$obj->getSpare_parts_order_qty_min(),
						'spare_parts_origin' 				=>	$obj->getSpare_parts_origin(),					
						'spare_parts_mechine_type'			=>	$obj->getSpare_parts_mechine_type(),
						'spare_parts_brand_name' 			=>	$obj->getSpare_parts_brand_name(),					
						'spare_parts_material'				=>	$obj->getSpare_parts_material(),
						'spare_parts_type' 					=>	$obj->getSpare_parts_type(),					
						'spare_parts_protection_guarantee'	=>	$obj->getSpare_parts_protection_guarantee(),
						'spare_parts_specifications' 		=>	$obj->getSpare_parts_specifications(),					
						'spare_parts_terms_condition'		=>	$obj->getSpare_parts_terms_condition(),							
						'meta_title' 						=>	$obj->getMeta_title(),
						'meta_keywords' 					=>	$obj->getMeta_keywords(),
						'meta_desc' 						=>	$obj->getMeta_desc()								
					);
				//}
				// Start the Update process
				try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>