<?php

class Administrator_View_Helper_Menu extends Zend_View_Helper_Abstract {

    public $view;
    protected $_translator;
    protected $_allow;
    protected $_modules_license;
    protected $_travel_array = array(4, 5, 6, 7, 8, 9, 10, 11, 12);
    protected $_menus = array(
        0 => array(
            'module' => 'Members,Administrator,Members,Members,Members,Members,Members,Userinfo,Userinfo,Userinfo',
            'controller' => 'index,role,index,index,index,form,password,backendpro,backendpro,backendpro',
            'action' => 'profile,list,add,list,list,updatepackages,change,list,assignmanager,addmanager',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_user',
            'sub_menu_const' => 'admin_header_menu_user_profile;;;;fa fa-tags,,admin_menu_new_group;;;;fa fa-tags,,admin_menu_new_user;;;/Members/index/group;fa fa-tags,,admin_menu_edit_user;;;;fa fa-tags,,admin_menu_search_user;;;;fa fa-tags,,admin_menu_package;;;/Members/form/values/form_id/9;fa fa-tags,,Member_menu_change_pass;;Members;;fa fa-tags,,Show User List;;Userinfo;;fa fa-tags,,Assign Manager;;Userinfo;;fa fa-tags,,Add Manager;;Userinfo;;fa fa-tags'
        ),
        1 => array(
            'module' => 'Settings,Members,Members,Administrator,Members',
            'controller' => 'global,form,form,index,index',
            'action' => 'update,add,list,approval,profile',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_site',
            'sub_menu_const' => 'admin_header_menu_global_settings;;;;fa fa-tags,,admin_menu_custom_form_create;;;;fa fa-tags,,admin_menu_custom_form;;;;fa fa-tags,,admin_menu_pending_approval;;;;fa fa-tags,,admin_menu_site_preview;;;/;fa fa-tags'
        ),
        2 => array(
            'module' => 'Articles,Articles,Articles,Articles,Articles',
            'controller' => 'backend,backend,backend,backend,backend',
            'action' => 'add,list,list,list,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_page',
            'sub_menu_const' => 'admin_menu_article_add;;;;fa fa-tags,,admin_menu_article_list_view;;;;fa fa-tags,,admin_menu_article_edit_delete;;;;fa fa-tags,,admin_menu_article_link_to_menu;;;/Articles/backend/list/link/1;fa fa-tags,,admin_menu_article_draft;;;/Articles/backend/list/draft/0;fa fa-tags'
        ),
        3 => array(
            'module' => 'Menu,Menu,Menu,Menu,Menu',
            'controller' => 'backend,backend,backend,backend,backend',
            'action' => 'addgrp,listgrp,add,list,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_name',
            'sub_menu_const' => 'admin_menu_group_add;;;/Menu/backend/listgrp/create/new;fa fa-tags,,admin_menu_group_manage;;;;fa fa-tags,,admin_menu_menu_add;;;;fa fa-tags,,admin_menu_edit_delete;;;;fa fa-tags,,admin_menu_list_draft;;;/Menu/backend/list/draft/0;fa fa-tags'
        ),
        4 => array(
            'module' => 'Flight,Flight,Flight,Flight,Flight',
            'controller' => 'api,airport,airport,airlines,airlines',
            'action' => 'list,add,list,add,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_flight,,Flight',
            'sub_menu_const' => 'flight_page_api_page_name;;Flight;;fa fa-tags,,flight_menu10;;Flight;;fa fa-tags,,flight_menu8;;Flight;;fa fa-tags,,flight_menu9;;Flight;;fa fa-tags,,flight_menu6;;Flight;;fa fa-tags'
        ),
        5 => array(
            'module' => 'Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos,Autos',
            'controller' => 'preferences,backendspareparts,backendgrp,backendgrp,backendgrp,backendbrand,backendbrand,backendpro,backendpro,backendpro,backendtype,backendtype,backend,backend',
            'action' => 'settings,list,listgrp,addgrp,listgrp,list,list,add,list,saved,add,list,add,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_autos,,Autos',
            'sub_menu_const' => 'admin_title_preferences;;Autos;;fa fa-tags,,autos_menu15;;Autos;;fa fa-tags,,admin_menu_autos_group;;Autos,,autos_menu9;;Autos,,autos_menu1;;Autos;;fa fa-tags,,autos_menu13;;Autos;/Autos/backendbrand/list/create/new,,autos_menu14;;Autos;;fa fa-tags,,autos_menu10;;Autos,,autos_menu3;;Autos,,autos_menu7;;Autos;;fa fa-tags,,autos_menu11;;Autos;/Autos/backendtype/list/create/new,,autos_menu4;;Autos;;fa fa-tags,,autos_menu12;;Autos,,autos_menu2;;Autos'
        ),
        6 => array(
            'module' => 'Property,Property,Property,Property,Property,Property,Property,Property,Property,Property,Property',
            'controller' => 'preferences,backendgrp,backendgrp,backendgrp,backendpro,backendpro,backendpro,backendtype,backendtype,backend,backend',
            'action' => 'settings,listgrp,addgrp,listgrp,add,list,saved,add,list,add,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_property',
            'sub_menu_const' => 'admin_title_preferences;;Property;;fa fa-tags,,admin_menu_property_group;;Property,,property_menu8;;Property,,property_menu9;;Property;;fa fa-tags,,property_menu10;;Property,,property_menu11;;Property,,property_menu7;;Property;;fa fa-tags,,property_menu12;;Property;/Property/backendtype/list/create/new,,property_menu13;;Property;;fa fa-tags,,property_menu14;;Property,,property_menu15;;Property'
        ),
        7 => array(
            'module' => 'Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels,Hotels',
            'controller' => 'preferences,api,backendgrp,backendgrp,backendgrp,backendpro,backendpro,backendpro,backendrooms,backendrooms,backendroom,backendroom,backendtype,backendtype,backend,backend,backend,backend',
            'action' => 'settings,list,listgrp,addgrp,listgrp,add,list,saved,add,list,add,list,add,list,add,list,add,add',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_hotels,,Hotels',
            'sub_menu_const' => 'admin_title_preferences;;Hotels;;fa fa-tags,,hotels_menu19;;Hotels;;fa fa-tags,,admin_menu_hotels_group;;Hotels,,hotels_menu10;;Hotels,,hotels_menu1;;Hotels;;fa fa-tags,,hotels_menu11;;Hotels,,hotels_menu3;;Hotels,,hotels_menu7;;Hotels;;fa fa-tags,,hotels_menu12;;Hotels,,hotels_menu8;;Hotels;;fa fa-tags,,hotels_menu17;;Hotels,,hotels_menu18;;Hotels;;fa fa-tags,,hotels_menu13;;Hotels;/Hotels/backendtype/list/create/new,,hotels_menu4;;Hotels;;fa fa-tags,,hotels_menu14;;Hotels,,hotels_menu2;;Hotels;;fa fa-tags,,hotels_menu9;;Hotels;/Vote/backendvote/list/group_id/hotels_page,,invoice_admin_menu;;Invoice;/Invoice/backend/list'
        ),
        8 => array(
            'module' => 'Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals,Vacationrentals',
            'controller' => 'preferences,api,backendgrp,backendgrp,backendgrp,backendpro,backendpro,backendpro,backendtype,backendtype,backend,backend,backend',
            'action' => 'settings,update,listgrp,addgrp,listgrp,add,list,saved,add,list,add,list,add',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_vacationrentals,,Vacationrentals',
            'sub_menu_const' => 'admin_title_preferences;;Vacationrentals;;fa fa-tags,,vacationrentals_menu15;;Vacationrentals;;fa fa-tags,,admin_menu_vacationrentals_group;;Vacationrentals,,vacationrentals_menu9;;Vacationrentals,,vacationrentals_menu1;;Vacationrentals;;fa fa-tags,,vacationrentals_menu10;;Vacationrentals,,vacationrentals_menu3;;Vacationrentals,,vacationrentals_menu7;;Vacationrentals;;fa fa-tags,,vacationrentals_menu11;;Vacationrentals;/Vacationrentals/backendtype/list/create/new,,vacationrentals_menu4;;Vacationrentals;;fa fa-tags,,vacationrentals_menu12;;Vacationrentals,,vacationrentals_menu2;;Vacationrentals;;fa fa-tags,,vacationrentals_menu8;;Vacationrentals;/Vote/backendvote/list/group_id/vacationrentals_page'
        ),
        9 => array(
            'module' => 'Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours,Tours',
            'controller' => 'preferences,api,backendgrp,backendgrp,backendgrp,backendpro,backendpro,backendpro,backendtype,backendtype,backend,backend,backend',
            'action' => 'settings,update,listgrp,addgrp,listgrp,add,list,saved,add,list,add,list,add',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_tours,,Tours',
            'sub_menu_const' => 'admin_title_preferences;;Tours;;fa fa-tags,,tours_menu15;;Tours;;fa fa-tags,,admin_menu_tours_group;;Tours,,tours_menu9;;Tours,,tours_menu1;;Tours;;fa fa-tags,,tours_menu10;;Tours,,tours_menu3;;Tours,,tours_menu7;;Tours;;fa fa-tags,,tours_menu11;;Tours;/Tours/backendtype/list/create/new,,tours_menu4;;Tours;;fa fa-tags,,tours_menu12;;Tours,,tours_menu2;;Tours;;fa fa-tags,,tours_menu8;;Tours;/Vote/backendvote/list/group_id/tours_page'
        ),
        10 => array(
            'module' => 'Tracker,Tracker,Tracker,Tracker,Tracker,Tracker',
            'controller' => 'backendairwaybill,backendairwaybill,backendtrack,backendtrack,backendnetwork,backendnetwork',
            'action' => 'list,add,add,list,list,add',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'tracker_menu,,Tracker',
            'sub_menu_const' => 'track_menu1;;Tracker,,track_menu2;;Tracker,,tracker_status_add_title;;Tracker;;fa fa-tags,,track_menu3;;Tracker,,tracker_network_list_page_name;;Tracker,,tracker_network_add_page_name;;Tracker'
        ),
        11 => array(
            'module' => 'Costcalculator',
            'controller' => 'backendpackage',
            'action' => 'list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_cost_calculator,,Costcalculator',
            'sub_menu_const' => 'admin_menu_cost_calculator_setting;;Costcalculator'
        ),
        12 => array(
            'module' => 'B2b,B2b,B2b,B2b,B2b,B2b,B2b,B2b,B2b,B2b,B2b',
            'controller' => 'backendpreferences,backendgrp,backendcategory,backendpackage,backendbuyleads,backendsellleads,products,companies,messageboards,Tradeshow,Successstories',
            'action' => 'preferences,listgrp,list,list,list,list,list,list,list,list,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'menu_b2b_main,,B2b',
            'sub_menu_const' => 'menu_b2b_sys_preferences;;B2b;;;,,menu_b2b_group;;B2b;;fa fa-tags,,b2b_title_category_management;;B2b;;;,,menu_b2b_package_management;;B2b;;fa fa-tags,,b2b_admin_buying_leads;;B2b,,b2b_admin_sellilng_leads;;B2b,,menu_b2b_admin_product;;B2b,,menu_b2b_admin_companies;;B2b,,menu_b2b_admin_messageboards;;B2b;;fa fa-tags,,menu_members_tradeshows;;B2b,,menu_b2b_admin_stories;;B2b,,'
        ),
        13 => array(
            'module' => 'Gallery,News,Projects,Newsletter,Poll,Review,Comment',
            'controller' => 'backendpro,backendpro,index,templates,backendpoll,backend,backendcomment',
            'action' => 'list,list,filepreview,send,list,list,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_module',
            'sub_menu_const' => 'admin_menu_gallery;;;;fa fa-tags,,admin_menu_news;;;;fa fa-tags,,admin_menu_project;;;;fa fa-tags,,admin_menu_newsletter;;;;fa fa-tags,,admin_menu_poll;;;;fa fa-tags,,admin_menu_review;;;;fa fa-tags,,admin_menu_comment;;;;fa fa-tags'
        ),
        14 => array(
            'module' => 'Paymentgateway,Paymentgateway,Invoice,Invoice,Settings',
            'controller' => 'backendgateway,currency,backend,backend,global',
            'action' => 'list,list,list,quotelist,price',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_billing',
            'sub_menu_const' => 'gateway_menu_title;;Paymentgateway;;fa fa-tags,,currency_menu_title;;Paymentgateway;;fa fa-tags,,invoice_admin_menu;;Invoice;;fa fa-tags,,invoice_menu2;;Invoice;;fa fa-tags,,admin_header_menu_global_price_settings;;Settings;;fa fa-tags'
        ),
        15 => array(
            'module' => 'Administrator,Administrator,Editors,Editors,Editors,Editors,Theme,Database,Newsletter,Geo,Geo,Geo',
            'controller' => 'privilege,role,backend,backend,backend,backend,template,backend,templates,country,state,city',
            'action' => 'list,list,template,js,css,language,list,list,list,list,list,list',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_setup',
            'sub_menu_const' => 'admin_menu_prev,,admin_menu_role;;;;fa fa-tags,,admin_menu_template,,admin_menu_js,,admin_menu_css,,admin_menu_language;;;;fa fa-tags,,admin_header_menu_manage_template;;;;fa fa-tags,,admin_menu_database;;;;fa fa-tags,,admin_menu_email_template;;Newsletter;;fa fa-tags,,geo_menu1;;Geo;;,,geo_menu2;;Geo;;,,geo_menu3;;Geo;;'
        ),
        16 => array(
            'module' => 'Settings,Database,Utility,Settings,Utility,Utility',
            'controller' => 'global,csv,backend,global,backend,backend',
            'action' => 'ip,list,php,cronsetup,cache,log',
            'menu_li_class' => 'icon-leaf',
            'main_menu_const' => 'admin_menu_utility',
            'sub_menu_const' => 'admin_menu_setting_ip;;Settings;;fa fa-tags,,admin_menu_export_import;;;;fa fa-tags,,admin_menu_php_info;;;;fa fa-tags,,admin_menu_cronsetup_info;;Property;;fa fa-tags,,admin_menu_cache_refresh;;;;fa fa-tags,,admin_menu_logs;;;;fa fa-tags'
        )
    );

    public function __construct($view) {
        $this->_translator = Zend_Registry::get('translator');
        $this->view = $view;
        $this->_allow = new Administrator_View_Helper_Allow();
        $this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
    }

    public function getMenu() {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        if ($role_id <= 101) {
            $this->_menus[17] = array(
                'module' => '',
                'controller' => '',
                'action' => '',
                'menu_li_class' => 'icon-info',
                'main_menu_const' => 'admin_menu_helps',
                'sub_menu_const' => ''
            );
        }
        foreach ($this->_menus as $all_menu_key => $each_menu) {

            $travel_menu = (in_array($all_menu_key, $this->_travel_array)) ? true : false;
            $this->createSessionRouteAction($auth, $this->_menus, $each_menu, $travel_menu);
        }
        $admin_top_menu_arr = $this->completeAction();
        return $admin_top_menu_arr;
    }

    private function completeAction() {
        $admin_top_menu_arr = (Eicra_Global_Variable::getSession()->admin_top_menu) ? Eicra_Global_Variable::getSession()->admin_top_menu : '';
        if (!empty($admin_top_menu_arr)) {
            $admin_top_menu_arr['complete'] = 'complete';
        }
        Eicra_Global_Variable::getSession()->admin_top_menu = $admin_top_menu_arr;
        return $admin_top_menu_arr;
    }

    private function createSessionRouteAction($auth, $postedData, $each_menu, $travel_menu) {
        try {
            if ($auth->hasIdentity()) {
                $travel_menu = $travel_menu;
                $modules = $each_menu['module'];
                $module_arr = explode(',', $modules);
                $controllers = $each_menu['controller'];
                $controller_arr = explode(',', $controllers);
                $actions = $each_menu['action'];
                $action_arr = explode(',', $actions);
                $main_menu_const = $each_menu['main_menu_const'];
                $main_menu_const_arr = explode(',', $main_menu_const);
                $sub_menu_const = $each_menu['sub_menu_const'];
                $sub_menu_const_arr = explode(',,', $sub_menu_const);
                $main_menu_li_class = $each_menu['menu_li_class'];

                $trans_module_var = (!empty($main_menu_const_arr[1])) ? $main_menu_const_arr[1] : '';
                $trans_module = (!empty($main_menu_const_arr[2])) ? $main_menu_const_arr[2] : '';
                $trans_module_link = (!empty($main_menu_const_arr[3])) ? $main_menu_const_arr[3] : '';

                $menu_code = '';

                $sub_menu_code = '';

                $menu_session_arr = (Eicra_Global_Variable::getSession()->admin_top_menu) ? Eicra_Global_Variable::getSession()->admin_top_menu : array();

                $sub_menu_session_arr = array();

                if ($main_menu_const_arr[0] == 'admin_menu_helps') {
                    $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
                    if ($auth->getIdentity()->access_other_user_profile == '1') {
                        $all_license = new Zend_Session_Namespace('License');
                        $license_datas = $all_license->license_data;

                        $community_forum = ($license_datas['community_forum']) ? $this->view->escape($license_datas['community_forum']) : 'javascript:void(0);';
                        $open_support_ticket = ($license_datas['open_support_ticket']) ? $this->view->escape($license_datas['open_support_ticket']) : 'javascript:void(0);';
                        $knowledgebase = ($license_datas['knowledgebase']) ? $this->view->escape($license_datas['knowledgebase']) : 'javascript:void(0);';
                        $user_guides = ($license_datas['user_guides']) ? $this->view->escape($license_datas['user_guides']) : 'javascript:void(0);';
                        $video_tutorial = ($license_datas['video_tutorial']) ? $this->view->escape($license_datas['video_tutorial']) : 'javascript:void(0);';
                        $blogs = ($license_datas['blogs']) ? $this->view->escape($license_datas['blogs']) : 'javascript:void(0);';
                        $extralink = ($license_datas['blogs']) ? $this->view->escape($license_datas['extralink']) : '';

                        $sub_menu_session_arr[0]['menu_name'] = $this->_translator->translator('admin_menu_community_forum');
                        $sub_menu_session_arr[0]['menu_link'] = $community_forum;
                        $sub_menu_session_arr[0]['menu_target'] = 'target="_blank"';
                        $sub_menu_session_arr[1]['menu_name'] = $this->_translator->translator('admin_menu_open_support_ticket');
                        $sub_menu_session_arr[1]['menu_link'] = $open_support_ticket;
                        $sub_menu_session_arr[1]['menu_target'] = 'target="_blank"';
                        $sub_menu_session_arr[2]['menu_name'] = $this->_translator->translator('admin_menu_knowledgebase');
                        $sub_menu_session_arr[2]['menu_link'] = $knowledgebase;
                        $sub_menu_session_arr[2]['menu_target'] = 'target="_blank"';
                        $sub_menu_session_arr[3]['menu_name'] = $this->_translator->translator('admin_menu_video_tutorials');
                        $sub_menu_session_arr[3]['menu_link'] = $video_tutorial;
                        $sub_menu_session_arr[3]['menu_target'] = 'target="_blank"';
                        $sub_menu_session_arr[4]['menu_name'] = $this->_translator->translator('admin_menu_user_guides');
                        $sub_menu_session_arr[4]['menu_link'] = $user_guides;
                        $sub_menu_session_arr[4]['menu_target'] = 'target="_blank"';
                        $sub_menu_session_arr[5]['menu_name'] = $this->_translator->translator('admin_menu_blogs');
                        $sub_menu_session_arr[5]['menu_link'] = $blogs;
                        $sub_menu_session_arr[5]['menu_target'] = 'target="_blank"';

                        if (!empty($extralink)) {
                            $extralink_arr = explode(',', $extralink);
                            $i = 6;
                            foreach ($extralink_arr as $extralink_arr_key => $extralink_info) {
                                $extralink_info_arr = explode(';;', $extralink_info);
                                $sub_menu_session_arr[$i]['menu_name'] = $extralink_info_arr[0];
                                $sub_menu_session_arr[$i]['menu_link'] = $extralink_info_arr[1];
                                $sub_menu_session_arr[$i]['menu_target'] = 'target="_blank"';
                                $i++;
                            }
                        }


                        $menu_session_arr[$main_menu_const_arr[0]]['menu_name'] = $this->_translator->translator($main_menu_const_arr[0], $trans_module_var, $trans_module);
                        $menu_session_arr[$main_menu_const_arr[0]]['menu_link'] = (empty($trans_module_link)) ? 'javascript:void(0);' : $trans_module_link;
                        $menu_session_arr[$main_menu_const_arr[0]]['menu_li_class'] = 'class="' . $main_menu_li_class . '"';
                        $menu_session_arr[$main_menu_const_arr[0]]['sub_menu'] = (!empty($sub_menu_session_arr[0])) ? $sub_menu_session_arr : '';
                    }
                } else {
                    $active_menu_count = 0;
                    foreach ($module_arr as $module_key => $module) {
                        if ($this->_allow->allow($action_arr[$module_key], $controller_arr[$module_key], $module) && $this->_modules_license->checkModulesLicense($module)) {
                            $sub_menu_arr = explode(';', $sub_menu_const_arr[$module_key]);
                            $trans_sub_module_var = (!empty($sub_menu_arr[1])) ? $sub_menu_arr[1] : '';
                            $trans_sub_module = (!empty($sub_menu_arr[2])) ? $sub_menu_arr[2] : '';

                            if ($module == 'News') {
                                $news_grp_db = new News_Model_DbTable_NewsGroup();
                                $news_grp_info = $news_grp_db->getAllGroupInfo();
                                if ($news_grp_info) {
                                    $group_helper = new News_View_Helper_NewsGroup();

                                    foreach ($news_grp_info as $news_grp_info_key => $entry) {
                                        $num_cat = $group_helper->getNumOfNews($this->view->escape($entry->id));
                                        $num_page = $group_helper->getNumOfPage($this->view->escape($entry->id));
                                        $link_url = $this->view->url(array('module' => 'News', 'controller' => 'Backendpro', 'action' => 'list', 'group_id' => $entry->id), 'adminrout', true);
                                        $li_class = ($news_grp_info_key == (count($news_grp_info) - 1)) ? 'fa fa-tags' : 'fa fa-tags';

                                        $sub_menu_link = (!empty($sub_menu_arr[3])) ? str_replace($this->view->baseUrl(), '', $sub_menu_arr[3]) : str_replace($this->view->baseUrl(), '', $link_url);
                                        $sub_menu_session_arr[$active_menu_count]['menu_name'] = $this->view->escape($entry->news_name) . ' ' . $this->_translator->translator('admin_menu_other');
                                        $sub_menu_session_arr[$active_menu_count]['menu_link'] = $sub_menu_link;
                                        $sub_menu_session_arr[$active_menu_count]['menu_li_class'] = 'class="' . $li_class . '"';

                                        $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '>';
                                        $sub_menu_code .= '<a href="' . $this->view->baseUrl() . $sub_menu_link . '">' . $sub_menu_session_arr[$active_menu_count]['menu_name'] . '</a>';
                                        $sub_menu_code .= '</li>';

                                        $active_menu_count++;
                                    }
                                }
                            } else if ($module == 'Gallery') {
                                $sub_menu_link = (!empty($sub_menu_arr[3])) ? str_replace($this->view->baseUrl(), '', $sub_menu_arr[3]) : str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key]), 'adminrout', true));
                                $sub_menu_session_arr[$active_menu_count]['menu_name'] = $this->_translator->translator($sub_menu_arr[0], $trans_sub_module_var, $trans_sub_module);
                                $sub_menu_session_arr[$active_menu_count]['menu_link'] = $sub_menu_link;
                                $sub_menu_session_arr[$active_menu_count]['menu_li_class'] = (!empty($sub_menu_arr[4])) ? 'class="' . $sub_menu_arr[4] . '"' : '';
                                $sub_menu_session_arr[$active_menu_count]['menu_target'] = ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : '';

                                $sub_menu_session_arr[$active_menu_count]['sub_menu'] = array(
                                    0 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu3-3', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => 'add'), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    1 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu3', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key]), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    2 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu1-1', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => 'backendgrp', 'action' => 'addgrp'), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    3 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu1', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => 'backendgrp', 'action' => 'listgrp'), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    4 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu2-2', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => 'backend', 'action' => 'add'), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    5 => array(
                                        'menu_name' => $this->_translator->translator('gallery_menu2', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => 'backend', 'action' => 'list'), 'adminrout', true)),
                                        'menu_li_class' => 'class="icon-leaf"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    )
                                );


                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '>';
                                $sub_menu_code .= '<a href="' . $this->view->baseUrl() . $sub_menu_link . '" class="parent">' . $sub_menu_session_arr[$active_menu_count]['menu_name'] . '</a>';
                                $sub_menu_code .= '<ul>';
                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][0]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][0]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][1]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][1]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '<li><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][2]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][2]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '</ul>';
                                $sub_menu_code .= '</li>';

                                $active_menu_count++;
                            } else if ($module == 'Invoice' && $action_arr[$module_key] == 'list') {
                                $sub_menu_link = (!empty($sub_menu_arr[3])) ? str_replace($this->view->baseUrl(), '', $sub_menu_arr[3]) : str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key]), 'adminrout', true));
                                $sub_menu_session_arr[$active_menu_count]['menu_name'] = $this->_translator->translator($sub_menu_arr[0], $trans_sub_module_var, $trans_sub_module);
                                $sub_menu_session_arr[$active_menu_count]['menu_link'] = $sub_menu_link;
                                $sub_menu_session_arr[$active_menu_count]['menu_li_class'] = (!empty($sub_menu_arr[4])) ? 'class="' . $sub_menu_arr[4] . '"' : '';
                                $sub_menu_session_arr[$active_menu_count]['menu_target'] = ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : '';

                                $sub_menu_session_arr[$active_menu_count]['sub_menu'] = array(
                                    0 => array(
                                        'menu_name' => $this->_translator->translator('invoice_menu4', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key], 'status' => 1), 'adminrout', true)),
                                        'menu_li_class' => 'class="fa fa-tags"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    1 => array(
                                        'menu_name' => $this->_translator->translator('invoice_menu5', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key], 'status' => 2), 'adminrout', true)),
                                        'menu_li_class' => 'class="fa fa-tags"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    ),
                                    2 => array(
                                        'menu_name' => $this->_translator->translator('invoice_menu6', '', $module),
                                        'menu_link' => str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key], 'status' => 0), 'adminrout', true)),
                                        'menu_li_class' => 'class="fa fa-tags"',
                                        'menu_target' => ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : ''
                                    )
                                );


                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '>';
                                $sub_menu_code .= '<a href="' . $this->view->baseUrl() . $sub_menu_link . '" class="parent">' . $sub_menu_session_arr[$active_menu_count]['menu_name'] . '</a>';
                                $sub_menu_code .= '<ul>';
                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][0]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][0]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][1]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][1]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '<li><a href="' . $this->view->baseUrl() . $sub_menu_session_arr[$active_menu_count]['sub_menu'][2]['menu_link'] . '" ' . $sub_menu_session_arr[$active_menu_count]['menu_target'] . '>' . $sub_menu_session_arr[$active_menu_count]['sub_menu'][2]['menu_name'] . '</a></li>';
                                $sub_menu_code .= '</ul>';
                                $sub_menu_code .= '</li>';

                                $active_menu_count++;
                            } else {
                                $sub_menu_link = (!empty($sub_menu_arr[3])) ? str_replace($this->view->baseUrl(), '', $sub_menu_arr[3]) : str_replace($this->view->baseUrl(), '', $this->view->url(array('module' => $module, 'controller' => $controller_arr[$module_key], 'action' => $action_arr[$module_key]), 'adminrout', true));
                                $sub_menu_session_arr[$active_menu_count]['menu_name'] = $this->_translator->translator($sub_menu_arr[0], $trans_sub_module_var, $trans_sub_module);
                                $sub_menu_session_arr[$active_menu_count]['menu_link'] = $sub_menu_link;
                                $sub_menu_session_arr[$active_menu_count]['menu_li_class'] = (!empty($sub_menu_arr[4])) ? 'class="' . $sub_menu_arr[4] . '"' : '';
                                $sub_menu_session_arr[$active_menu_count]['menu_target'] = ($sub_menu_arr[0] == 'admin_menu_site_preview') ? 'target="_blank"' : '';

                                $sub_menu_code .= '<li ' . $sub_menu_session_arr[$active_menu_count]['menu_li_class'] . '>';
                                $sub_menu_code .= '<a href="' . $this->view->baseUrl() . $sub_menu_link . '">' . $sub_menu_session_arr[$active_menu_count]['menu_name'] . '</a>';
                                $sub_menu_code .= '</li>';

                                $active_menu_count++;
                            }
                        }
                    }
                    if (!empty($sub_menu_code)) {
                        if ($travel_menu) {
                            $menu_session_arr['admin_menu_travels']['menu_name'] = $this->_translator->translator('admin_menu_travels');
                            $menu_session_arr['admin_menu_travels']['menu_link'] = $this->_translator->translator('javascript:void(0);');
                            $menu_session_arr['admin_menu_travels']['menu_li_class'] = 'class="icon-link"';
                            if (empty($menu_session_arr['admin_menu_travels']['sub_menu']) || empty($menu_session_arr['admin_menu_travels']['sub_menu'][0])) {
                                $menu_session_index = 0;
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_name'] = $this->_translator->translator($main_menu_const_arr[0], $trans_module_var, $trans_module);
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_link'] = (empty($trans_module_link)) ? 'javascript:void(0);' : str_replace($this->view->baseUrl(), '', $trans_module_link);
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_li_class'] = (!empty($main_menu_li_class)) ? 'class="' . $main_menu_li_class . '"' : '';
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['sub_menu'] = (!empty($sub_menu_session_arr[0])) ? $sub_menu_session_arr : '';
                            } else {
                                $menu_session_index = count($menu_session_arr['admin_menu_travels']['sub_menu']);
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_name'] = $this->_translator->translator($main_menu_const_arr[0], $trans_module_var, $trans_module);
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_link'] = (empty($trans_module_link)) ? 'javascript:void(0);' : str_replace($this->view->baseUrl(), '', $trans_module_link);
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['menu_li_class'] = (!empty($main_menu_li_class)) ? 'class="' . $main_menu_li_class . '"' : '';
                                $menu_session_arr['admin_menu_travels']['sub_menu'][$menu_session_index]['sub_menu'] = (!empty($sub_menu_session_arr[0])) ? $sub_menu_session_arr : '';
                            }
                        } else {
                            $menu_session_arr[$main_menu_const_arr[0]]['menu_name'] = $this->_translator->translator($main_menu_const_arr[0], $trans_module_var, $trans_module);
                            $menu_session_arr[$main_menu_const_arr[0]]['menu_link'] = (empty($trans_module_link)) ? 'javascript:void(0);' : str_replace($this->view->baseUrl(), '', $trans_module_link);
                            $menu_session_arr[$main_menu_const_arr[0]]['menu_li_class'] = 'class="' . $main_menu_li_class . '"';
                            $menu_session_arr[$main_menu_const_arr[0]]['sub_menu'] = (!empty($sub_menu_session_arr[0])) ? $sub_menu_session_arr : '';
                        }
                    }
                }

                Eicra_Global_Variable::getSession()->admin_top_menu = $menu_session_arr;
                $json_arr = array('status' => 'ok', 'menu_session_arr' => $menu_session_arr);
            } else {
                $json_arr = array('status' => 'err', 'msg' => '', 'menu_session_arr' => null);
            }
        } catch (Exception $e) {
            $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'menu_session_arr' => null);
        }
    }

}
