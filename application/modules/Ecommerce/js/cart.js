// models / data


var cart = kendo.observable({		
    contents: sessionData,
    cleared: false,	

    contentsCount: function() {
        return this.get("contents").length;
    },

    add: function(item) {		
        var found = false;	
		//alert(item.toSource());	
		item.set("item_id", item.id);	
        this.set("cleared", false);
			
        for (var i = 0; i < this.contents.length; i ++) {
            var current = this.contents[i];									
            if ((current.item.item_id == item.item_id) && (current.item.table_name == item.table_name)) {
                current.set("quantity", parseInt(current.get("quantity")) + 1);				
                found = true;
                break;
            }
        }
		
        if (!found) {
            this.contents.push({ item: item, quantity: 1 });
        }
				
		if(visible == true)
		{
			visible = true;	
			$("#slide-in-handle").trigger('click');
		}
    },

    remove: function(item) {		
        for (var i = 0; i < this.contents.length; i ++) {
            var current = this.contents[i];			
            //if (current.item.item_id == item.item.item_id) {
			if (current === item) {
                this.contents.splice(i, 1);
                break;
            }
        }
		assignToSession.sendToServer();	
    },

    empty: function() {
        var contents = this.get("contents");
        contents.splice(0, contents.length);		
    },

    clear: function() {
        var contents = this.get("contents");
        contents.splice(0, contents.length);
        this.set("cleared", true);
    },

    total: function() {
        var price = 0,
            contents = this.get("contents"),
            length = contents.length,
            i = 0;

        for (; i < length; i ++) {
            price += parseFloat(contents[i].item.price) * contents[i].quantity;
        }		
		assignToSession.sendToServer();
        return kendo.format("{0:c}", price);
    }
});

var assignToSession = kendo.observable({
	sendToServer: function() {		
		var cart_result = cart.contents.toJSON();					
		var assignSession = new kendo.data.DataSource({
									type: "json",
									schema: { 
									data:	function(response){  return response.cart_result; },
									model: {} 
									},
									transport: { 
												read: { 
															type: "POST",
															url:  cartUrl,
															dataType: "json",
															data: { cart_result : cart_result},
															complete: function(e) {
																	//alert(e.responseText);
																  var json_arr = $.parseJSON(e.responseText);
																  if(json_arr.status == 'err')
																  {																	  
																	  commonMsgDialog({messages:{common_msg_dialog_id: "cart-msg", common_ok: json_arr.common_ok }}, json_arr.msg);
																  }
															}
														}
											}
								});
		
		assignSession.read();	
	}
});

var layoutModel = kendo.observable({
    cart: cart
});

var cartPreviewModel = kendo.observable({
    
	cart: cart,
	order_link : (orderLink) ? orderLink : "B2b-Invoice-Confirm",
		
    cartContentsClass: function() {		
			
        return this.cart.contentsCount() > 0 ? "active" : "empty";
    },

    removeFromCart: function(e) {
        this.get("cart").remove(e.data);
    },

    emptyCart: function() {
		visible = false;
		$("#slide-in-handle").trigger('click');
        cart.empty();
    },

    itemPrice: function(cartItem) {				
        return kendo.format("{0:c}", parseFloat(cartItem.item.price));
    },

    totalPrice: function() {
        return this.get("cart").total();
    },

    proceed: function(e) {
        //this.get("cart").clear();
        //eicra.navigate("/");
		document.location.href = this.order_link;
    }
});

var indexModel = kendo.observable({    
    cart: cart,
    addToCart: function(e) {									
        cart.add(e.data);
    }
});

var detailModel = kendo.observable({
    imgUrl: function() {
        return window.contentPath + "/images/200/" + this.get("current").image
    },

    price: function() {
        return kendo.format("{0:c}", this.get("current").price);
    },

    addToCart: function(e) {
        cart.add(this.get("current"));
    },

    setCurrent: function(itemID) {
        this.set("current", items.get(itemID));
    },

    previousHref: function() {
        var id = this.get("current").id - 1;
        if (id === 0) {
           id = items.data().length - 1;
        }

        return "#/menu/" + id;
    },

    nextHref: function() {
        var id = this.get("current").id + 1;

        if (id === items.data().length) {
           id = 1;
        }

        return "#/menu/" + id;
    },

    kCal: function() {
        return kendo.toString(this.get("current").stats.energy /  4.184, "0.0000");
    }
});




// Views and layouts
var layout = new kendo.Layout("layout-template", { model: layoutModel });
//var index = new kendo.View("index-template", { model: indexModel });

var cartPreview = new kendo.Layout("cart-preview-template", { model: cartPreviewModel });
var checkout = new kendo.View("checkout-template", {model: cartPreviewModel });
var detail = new kendo.View("detail-template", { model: detailModel });


var slide = '', visible = true;
function sliding()
{
	if (visible) {
		  slide.reverse();
	  } else {
		 slide.play();		  
	  }
	  visible = !visible;	
}

var eicra = new kendo.Router({
    init: function() {       
	    layout.render("#application");
		kendo.culture(cart_culture);
		slide = kendo.fx($("#slide-in-share")).slideIn("left"),
        visible = true;		
		$("#slide-in-handle").click(function(e) {
			
				sliding();
				e.preventDefault();
			
		});
		$('#cart_window').hide();
    }
});

// Routing
eicra.route("/", function() {
    //layout.showIn("#content", index);
    layout.showIn("#pre-content", cartPreview);
	$('#cart_window').fadeOut();	
});

eicra.route("/checkout", function() {			
	visible = false;
	$("#slide-in-handle").trigger('click');
    //cartPreview.hide();	
	layout.showIn("#checkout-content", checkout);
	$('#cart_window').fadeIn();	
});

eicra.route("/menu/:id", function(itemID) {
    layout.showIn("#pre-content", cartPreview);

    items.fetch(function(e) {
        detailModel.setCurrent(itemID);
        //layout.showIn("#checkout-content", detail);
    });
});

$(function() {	
	//alert(sessionData.toSource());	
    eicra.start();
});

function commonCartButtonFunction(grid, button_id)
{
	
	try{ 
			var datas = grid.dataSource.data();
			$.each(datas, function(i, obj_arr){
				var item_array = { id :obj_arr.id, price: obj_arr.price, name: obj_arr.name, primary_file_field_format: obj_arr.primary_file_field_format, table_name: obj_arr.table_name };		
				var indexCartModel = kendo.observable({  			
					addToCart: function(e) {					
						$.each(item_array, function(key, value){
								e.data[key] = value;
							});						
						cart.add(e.data);
					}
				});
				
				kendo.bind($("#"+button_id+obj_arr.id), indexCartModel); 
			});			
		}catch(e){};
}