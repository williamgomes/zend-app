<?php
class Property_Form_PreferencesForm extends Zend_Form {
	protected $_editor;
	public function __construct($options = null)

	{
		$translator = Zend_Registry::get ( 'translator' );
		$config = (file_exists ( APPLICATION_PATH . '/modules/Property/forms/source/' . $translator->getLangFile () . '.PreferencesForm.ini' )) ? new Zend_Config_Ini ( APPLICATION_PATH . '/modules/Property/forms/source/' . $translator->getLangFile () . '.PreferencesForm.ini', 'preferences' ) : new Zend_Config_Ini ( APPLICATION_PATH . '/modules/Property/forms/source/en_US.PreferencesForm.ini', 'preferences' );
		parent::__construct ( $config->preferences );
	}
	public function init()

	{
		$this->createForm ();
	}
	public function createForm() {
		$this->elementDecorator ();
		$this->loadReviewGroup ();
		/* $this->loadEmailTemplate ( array (
				$this->cron_admin_email_template_id,
				$this->cron_buying_user_email_template_id,
				$this->cron_selling_user_email_template_id,
				$this->cron_product_user_email_template_id,
				$this->cron_company_user_email_template_id
		) ); */
	}

	// Element Decorator
	private function elementDecorator()

	{
		$this->setElementDecorators ( array (
				'ViewHelper',
				'FormElements'
		) );
	}
	public function loadEmailTemplate($element_array) {
		$templates = new Newsletter_Model_DbTable_Templates ();
		$templates_options = $templates->getTemplatesData ();
		if ($element_array) {
			foreach ( $element_array as $element ) {
				$element->setRegisterInArrayValidator ( false );
				$element->addMultiOptions ( $templates_options );
			}
		}
	}
	private function setDB()

	{
		$elements = $this->getElements ();

		$setting_db = new Settings_Model_DbTable_Setting ();

		$setting_db->setDatabaseForm ( $elements );
	}
	public function loadPriceMargine()

	{
		$price = new Settings_Model_DbTable_Price ();

		$price_options = $price->getSelectOptions ();

		$this->price_margine->setRegisterInArrayValidator ( false );

		foreach ( $price_options as $key => $value )

		{

			$this->price_margine->addMultiOption ( $key, $value );
		}
	}
	private function loadReviewGroup() {
		$reviewGroup = new Review_Model_DbTable_Setting ();
		$reviewGroup_options = $reviewGroup->getAllReviews ();
/* 		$this->product_review_id->addMultiOptions ( $reviewGroup_options );
		$this->selling_review_id->addMultiOptions ( $reviewGroup_options );
		$this->buying_review_id->addMultiOptions ( $reviewGroup_options ); */
	}
	public function loadCountries()

	{
		$global_conf = Zend_Registry::get ( 'global_conf' );

		$countries = new Eicra_Model_DbTable_Country ();

		$countries_options = $countries->getOptions ();

		$selected = $global_conf ['default_country'];

		foreach ( $countries_options as $key => $value )

		{

			if ($selected == $key)

			{

				$this->default_country->addMultiOption ( $key, $value );

				$this->default_country->setValue ( $selected );
			}

			else

			{

				$this->default_country->addMultiOption ( $key, $value );
			}
		}
	}
	public function loadLocales()

	{
		$global_conf = Zend_Registry::get ( 'global_conf' );

		$locals = new Zend_Locale ();

		$locales_options = $locals->getLocaleList ();

		$selected = $global_conf ['default_locale'];

		// $currency = new Zend_Currency();

		foreach ( $locales_options as $key => $value )

		{

			$locals->setLocale ( $key );

			$region = $locals->getRegion ();

			if (! empty ( $region ))

			{

				// $currency->setLocale($key);

				// $currency_view = ($currency->getName()) ? $currency->getSymbol().' ('.$currency->getShortName().')'.' ---> '.$currency->getName() : $currency->getSymbol().' ('.$currency->getShortName().')';

				if ($selected == $key)

				{

					$this->default_locale->addMultiOption ( $key, $key . ' ---> ' . $region );

					// $this->default_locale->addMultiOption($key,$key.' -- '.$currency_view);

					$this->default_locale->setValue ( $selected );
				}

				else

				{

					$this->default_locale->addMultiOption ( $key, $key . ' ---> ' . $region );

					// $this->default_locale->addMultiOption($key,$key.' -- '.$currency_view);
				}
			}
		}
	}
	public function setEditor($baseURL)

	{
		$translator = Zend_Registry::get ( 'translator' );

		$this->_editor = '<script type="text/javascript" src="' . $baseURL . '/js/tiny_mce/tiny_mce.js"></script>';

		$this->_editor .= '<script language="javascript" type="text/javascript"   src="' . $baseURL . '/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';

		$this->_editor .= '<script type="text/javascript">

								tinyMCE.init({

								// General options

								mode : "none",

								theme : "advanced",

								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

								skin : "o2k7",

								skin_variant : "silver",

								// Theme options



								theme_advanced_buttons1 : "insertimage,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect",

								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

								theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl",

								theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor,fontsizeselect,|,fullscreen",

								theme_advanced_toolbar_location : "top",

								theme_advanced_toolbar_align : "left",

								theme_advanced_statusbar_location : "bottom",





								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type],style[type|title|disabled|media],script[type|src],center",

								convert_fonts_to_spans :  false,

								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",

								theme_advanced_default_font : "[arial|30] ",

								theme_advanced_resizing : true,



								forced_root_block : false,

								force_br_newlines : true,

								force_p_newlines : false,

								relative_urls : false,



								relative_urls : true,

        						remove_script_host : true,



								document_base_url : "' . $baseURL . '/",



								// Example content CSS (should be your site CSS)

								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",



								// Drop lists for link/image/media/template dialogs

								template_external_list_url : "lists/template_list.js",

								external_link_list_url : "lists/link_list.js",

								external_image_list_url : "lists/image_list.js",

								media_external_list_url : "lists/media_list.js",



								// Replace values for the template plugin

								template_replace_values : {

									username : "Some User",

									staffid : "991234"

								},

								template_popup_width : "500",

								template_popup_height : "400",

								template_templates : [

									{

										title : "Newsletter Template",

										src : "' . $baseURL . '/newsletterTemplate/editor_details.html",

										description : "Adds Editors Name and Staff ID"

									}

								]



							});

							function loadTinyMCE(id)

							{

								tinyMCE.execCommand(\'mceAddControl\', false, id);

								document.getElementById(\'loaderLink_\'+id).innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';

		$this->_editor .= "\''+id+'\'";

		$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="' . $translator->translator ( 'common_editor_close' ) . '" alt="' . $translator->translator ( 'common_editor_close' ) . '" /></a>\';

							}

							function unloadTinyMCE(id)

							{

								tinyMCE.execCommand(\'mceRemoveControl\', false, id);

								document.getElementById(\'loaderLink_\'+id).innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';

		$this->_editor .= "\''+id+'\'";

		$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="' . $translator->translator ( 'common_editor_open' ) . '" alt="' . $translator->translator ( 'common_editor_open' ) . '" /></a>\';

							}

						</script>';
	}
	public function getEditor()

	{
		echo $this->_editor;
	}
	public function addDisplayGroups(array $groups_arr) {
		foreach ( $groups_arr as $groups ) {

			foreach ( $groups as $key => $spec ) {
				$name = null;
				if (! is_numeric ( $key )) {
					$name = $key;
				}

				if ($spec instanceof Zend_Form_DisplayGroup) {
					$this->_addDisplayGroupObject ( $spec );
				}

				if (! is_array ( $spec ) || empty ( $spec )) {
					continue;
				}

				$argc = count ( $spec );
				$options = array ();

				if (isset ( $spec ['elements'] )) {
					$elements = $spec ['elements'];
					if (isset ( $spec ['name'] )) {
						$name = $spec ['name'];
					}
					if (isset ( $spec ['options'] )) {
						$options = $spec ['options'];
					}
					$this->addDisplayGroup ( $elements, $name, $options );
				} else {
					switch ($argc) {
						case (1 <= $argc) :
							$elements = array_shift ( $spec );
							if (! is_array ( $elements ) && (null !== $name)) {
								$elements = array_merge ( ( array ) $elements, $spec );
								$this->addDisplayGroup ( $elements, $name );
								break;
							}
						case (2 <= $argc) :
							if (null !== $name) {
								$options = array_shift ( $spec );
								$this->addDisplayGroup ( $elements, $name, $options );
								break;
							}
							$name = array_shift ( $spec );
						case (3 <= $argc) :
							$options = array_shift ( $spec );
						default :
							$this->addDisplayGroup ( $elements, $name, $options );
					}
				}
			}
		}
		return $this;
	}
}