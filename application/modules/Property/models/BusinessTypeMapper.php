<?php
class Property_Model_BusinessTypeMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Property_Model_DbTable_BusinessType');
        }
        return $this->_dbTable;
    }
	
	public function save(Property_Model_BusinessType $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				unset($data['id']);
				
				$data = array(					
					'group_id' 					=> 	$obj->getGroup_id(),
					'business_type' 			=>	$obj->getBusiness_type(),
					'entry_by' 					=>	$obj->getEntry_by(),
					'entry_date' 				=>	date("Y-m-d h:i:s")										
				);
				try 
				{
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{				
				$data = array(					
					'group_id' 					=> 	$obj->getGroup_id(),
					'business_type' 			=>	$obj->getBusiness_type()						
				);				
				// Start the Update process
				try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>