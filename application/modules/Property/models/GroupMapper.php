<?php
class Property_Model_GroupMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Property_Model_DbTable_PropertyGroup');
        }
        return $this->_dbTable;
    }
	
	public function save(Property_Model_Groups $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				unset($data['id']);
				
				$data = array(					
					'property_name' 				=>	$obj->getProperty_name(),
					'entry_by' 						=> 	$obj->getEntry_by(),
					'role_id' 						=>	$obj->getRole_id(),
					'review_id' 					=>	$obj->getReview_id(),
					'group_type' 					=>	$obj->getGroup_type(),
					'calendar_on_off' 				=>	$obj->getCalendar_on_off(),
					'category_on_off' 				=>	$obj->getCategory_on_off(),
					'category_panel' 				=>	$obj->getCategory_panel(),
					'horizontal_panel' 				=>	$obj->getHorizontal_panel(),
					'light_box_on' 					=>	$obj->getLight_box_on(),
					'num_of_pic_show' 				=>	$obj->getNum_of_pic_show(),
					'dynamic_form' 					=>	$obj->getDynamic_form(),
					'meta_title' 					=>	$obj->getMeta_title(),
					'meta_keywords' 				=>	$obj->getMeta_keywords(),
					'meta_desc' 					=>	$obj->getMeta_desc(),
					'cat_num_per_page'				=>	$obj->getCat_num_per_page(),
					'cat_col_num' 					=>	$obj->getCat_col_num(),
					'cat_sort' 						=>	$obj->getCat_sort(),
					'cat_order' 					=>	$obj->getCat_order(),
					'latest_cat_on' 				=>	$obj->getLatest_cat_on(),
					'latest_cat_num_per_page' 		=>	$obj->getLatest_cat_num_per_page(),
					'latest_cat_sort' 				=>	$obj->getLatest_cat_sort(),
					'latest_cat_order' 				=>	$obj->getLatest_cat_order(),
					'fearured_cat_on' 				=>	$obj->getFearured_cat_on(),
					'fearured_cat_num_per_page' 	=>	$obj->getFearured_cat_num_per_page(),
					'fearured_cat_sort' 			=>	$obj->getFearured_cat_sort(),
					'fearured_cat_order' 			=>	$obj->getFearured_cat_order(),
					'file_num_per_page' 			=>	$obj->getFile_num_per_page(),
					'file_col_num' 					=>	$obj->getFile_col_num(),
					'file_sort' 					=>	$obj->getFile_sort(),
					'file_order' 					=>	$obj->getFile_order(),
					'latest_file_on' 				=>	$obj->getLatest_file_on(),
					'latest_file_num_per_page' 		=>	$obj->getLatest_file_num_per_page(),
					'latest_file_sort' 				=>	$obj->getLatest_file_sort(),
					'latest_file_order' 			=>	$obj->getLatest_file_order(),
					'featured_file_on' 				=>	$obj->getFeatured_file_on(),
					'featured_file_num_per_page' 	=>	$obj->getFeatured_file_num_per_page(),
					'featured_file_sort' 			=>	$obj->getFeatured_file_sort(),
					'featured_file_order' 			=>	$obj->getFeatured_file_order(),
					'file_type' 					=>	$obj->getFile_type(),
					'file_size_max' 				=>	$obj->getFile_size_max(),
					'file_cat_thumb_width' 			=>	$obj->getFile_cat_thumb_width(),
					'file_cat_thumb_height' 		=>	$obj->getFile_cat_thumb_height(),
					'file_cat_thumb_resize_func' 	=>	$obj->getFile_cat_thumb_resize_func(),
					'file_thumb_width' 				=>	$obj->getFile_thumb_width(),
					'file_thumb_height' 			=>	$obj->getFile_thumb_height(),
					'file_thumb_resize_func' 		=>	$obj->getFile_thumb_resize_func(),
					'img_big_width' 				=>	$obj->getImg_big_width(),
					'img_big_height' 				=>	$obj->getImg_big_height(),
					'img_big_resize_func' 			=>	$obj->getImg_big_resize_func(),					
					'active' 						=>	$obj->getActive()					
				);
				try 
				{
					$id_res = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $id_res);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $id_res, 'msg' => $e->getMessage());	
				}		
			} 
			else 
			{
				$data = array(					
					'property_name' 				=>	$obj->getProperty_name(),
					'role_id' 						=>	$obj->getRole_id(),
					'group_type' 					=>	$obj->getGroup_type(),
					'review_id' 					=>	$obj->getReview_id(),
					'calendar_on_off' 				=>	$obj->getCalendar_on_off(),
					'category_on_off' 				=>	$obj->getCategory_on_off(),
					'category_panel' 				=>	$obj->getCategory_panel(),
					'horizontal_panel' 				=>	$obj->getHorizontal_panel(),
					'light_box_on' 					=>	$obj->getLight_box_on(),
					'num_of_pic_show' 				=>	$obj->getNum_of_pic_show(),
					'dynamic_form' 					=>	$obj->getDynamic_form(),
					'meta_title' 					=>	$obj->getMeta_title(),
					'meta_keywords' 				=>	$obj->getMeta_keywords(),
					'meta_desc' 					=>	$obj->getMeta_desc(),
					'cat_num_per_page'				=>	$obj->getCat_num_per_page(),
					'cat_col_num' 					=>	$obj->getCat_col_num(),
					'cat_sort' 						=>	$obj->getCat_sort(),
					'cat_order' 					=>	$obj->getCat_order(),
					'latest_cat_on' 				=>	$obj->getLatest_cat_on(),
					'latest_cat_num_per_page' 		=>	$obj->getLatest_cat_num_per_page(),
					'latest_cat_sort' 				=>	$obj->getLatest_cat_sort(),
					'latest_cat_order' 				=>	$obj->getLatest_cat_order(),
					'fearured_cat_on' 				=>	$obj->getFearured_cat_on(),
					'fearured_cat_num_per_page' 	=>	$obj->getFearured_cat_num_per_page(),
					'fearured_cat_sort' 			=>	$obj->getFearured_cat_sort(),
					'fearured_cat_order' 			=>	$obj->getFearured_cat_order(),
					'file_num_per_page' 			=>	$obj->getFile_num_per_page(),
					'file_col_num' 					=>	$obj->getFile_col_num(),
					'file_sort' 					=>	$obj->getFile_sort(),
					'file_order' 					=>	$obj->getFile_order(),
					'latest_file_on' 				=>	$obj->getLatest_file_on(),
					'latest_file_num_per_page' 		=>	$obj->getLatest_file_num_per_page(),
					'latest_file_sort' 				=>	$obj->getLatest_file_sort(),
					'latest_file_order' 			=>	$obj->getLatest_file_order(),
					'featured_file_on' 				=>	$obj->getFeatured_file_on(),
					'featured_file_num_per_page' 	=>	$obj->getFeatured_file_num_per_page(),
					'featured_file_sort' 			=>	$obj->getFeatured_file_sort(),
					'featured_file_order' 			=>	$obj->getFeatured_file_order(),
					'file_type' 					=>	$obj->getFile_type(),
					'file_size_max' 				=>	$obj->getFile_size_max(),
					'file_cat_thumb_width' 			=>	$obj->getFile_cat_thumb_width(),
					'file_cat_thumb_height' 		=>	$obj->getFile_cat_thumb_height(),
					'file_cat_thumb_resize_func' 	=>	$obj->getFile_cat_thumb_resize_func(),
					'file_thumb_width' 				=>	$obj->getFile_thumb_width(),
					'file_thumb_height' 			=>	$obj->getFile_thumb_height(),
					'file_thumb_resize_func' 		=>	$obj->getFile_thumb_resize_func(),
					'img_big_width' 				=>	$obj->getImg_big_width(),
					'img_big_height' 				=>	$obj->getImg_big_height(),
					'img_big_resize_func' 			=>	$obj->getImg_big_resize_func()					
				);
				// Start the Update process
				try 
				{		
				 	$this->getDbTable()->update($data, array('id = ?' => $id));	
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}			
			}
			return $result;
		}

}
?>