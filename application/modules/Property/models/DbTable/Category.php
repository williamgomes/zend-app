<?php

/**
 * This is the DbTable class for the property_category table.
 */
class Property_Model_DbTable_Category extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'property_category';
    protected $_cols = null;

    //Get Datas
    public function getCategoryInfo($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if ($row) {
            // throw new Exception("Count not find row $id");        
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        } else {
            $options = null;
        }
        return $options;
    }

    //Get Datas
    public function getCategoryInfoFromTitle($category_title) {
        $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('category_title =?', $category_title)
                ->limit(1);
        $options = $this->fetchAll($select);
        if ($options) {
            $options = ($options[0]) ? $options[0] : $options;
        }
        return $options;
    }

    //Get All Datas
    public function getAllParentCategoryInfo($group_id = null) {
        if (empty($group_id)) {
            $select = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id', 'gc.group_id', 'gc.category_name', 'gc.category_thumb', 'gc.category_order', 'gc.active', 'gc.featured', 'gc.cat_date', 'gc.entry_by'))
                    ->where("gc.parent = ?", '0')
                    ->order('gc.group_id ASC')
                    ->order('gc.category_order ASC');
        } else {
            $select = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id', 'gc.group_id', 'gc.category_name', 'gc.category_thumb', 'gc.category_order', 'gc.active', 'gc.featured', 'gc.cat_date', 'gc.entry_by'))
                    ->where("gc.parent = ?", '0')
                    ->where("gc.group_id = ?", $group_id)
                    ->order('gc.category_order ASC');
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            // throw new Exception("Count not find row Category");
            $options = null;
        }
        return $options;
    }

    //Get Datas
    public function getOptions($group_id = null, $order_field = 'category_order', $order_sort = 'ASC', $active = null) {
        if ($group_id) {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->where('group_id =?', $group_id)
                    ->order($order_field . ' ' . $order_sort);
        } else {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->order($order_field . ' ' . $order_sort);
        }
        if ($active != null) {
            $select->where('active =?', $active);
        }
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    public function getCheckChild($parent) {
        if (!empty($parent)) {
            $validator = new Zend_Validate_Db_RecordExists(
                    array(
                'table' => $this->_name,
                'field' => 'parent'
                    )
            );
            $rs = ($validator->isValid($parent)) ? true : false;
        }
        return $rs;
    }

    public function getAllSubCategoryId($parent) {
        if (!empty($parent)) {
            $selectSub = $this->select()
                    ->from(array('gc' => $this->_name), array('gc.id'))
                    ->where('gc.parent = ?', $parent);
            $rs = $selectSub->query()->fetchAll();
            if ($rs) {
                foreach ($rs as $options) {
                    $id .= $options['id'] . ',';
                    if ($this->getCheckChild($options['id'])) {
                        $id .= $this->getAllSubCategoryId($options['id']);
                    }
                }
            }
        }
        return $id;
    }

    public function checkSubCategory($parent) {
        if (!empty($parent)) {
            $validator = new Zend_Validate_Db_RecordExists(
                    array(
                'table' => $this->_name,
                'field' => 'parent'
                    )
            );
            $rs = ($validator->isValid($parent)) ? true : false;
        }
        return $rs;
    }
    
    
    public function getAllSubCategory($parent) {
        $result = '';
        if (!empty($parent)) {
            try{
                $select = $this->select()
                    ->from(array('cat' => $this->_name), array('id' => 'cat.id', 'category_name' => 'cat.category_name'))
                    ->where('cat.parent = ?', $parent)
                    ->order('cat.category_order ASC');;
                $result = $select->query()->fetchAll();
            } catch (Exception $e){
                throw new $e->getMessage();
            }
        }
        
        return $result;
    }

    //Get All Datas
    public function getListInfo($approve = null, $search_params = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('pc' => $this->_name), array('pc.*'))
                ->joinLeft(array('pg' => Zend_Registry::get('dbPrefix') . 'property_group'), 'pc.group_id = pg.id', array('group_name' => 'pg.property_name', 'file_num_per_page' => 'pg.file_num_per_page', 'file_col_num' => 'pg.file_col_num', 'file_sort' => 'pg.file_sort', 'file_order' => 'pg.file_order', 'cat_sort' => 'pg.cat_sort', 'cat_order' => 'pg.cat_order', 'group_meta_title' => 'pg.meta_title', 'group_meta_keywords' => 'pg.meta_keywords', 'group_meta_desc' => 'pg.meta_desc', 'role_id' => 'pg.role_id'))
                ->joinLeft(array('pcp' => $this->_name), 'pc.parent = pcp.id', array('parent_name' => 'pcp.category_name'))
                ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'pc.entry_by = up.user_id', array('username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))
                ->joinLeft(array('pp' => Zend_Registry::get('dbPrefix') . 'property_page'), 'pc.id = pp.category_id', array('property_num' => 'COUNT(pp.id)'))
                ->group('pc.id');

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('pc.entry_by = ?', $user_id);
        }

        if ($approve != null) {
            $select->where("pc.active = ?", $approve);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    if ($sort_value_arr['dir'] == 'exp') {
                        $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                    } else {
                        $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                    }
                }
            } else {
                $select->order("pc.group_id ASC")
                        ->order('pc.category_order ASC');
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $hasChild = ($filter_obj['field'] == 'parent') ? true : $hasChild;
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i++;
                    } else if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                            $hasChild = ($sub_filter_obj['field'] == 'parent') ? true : $hasChild;
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                            $sub++;
                        }
                        $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                        $i++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("pc.group_id ASC")
                    ->order('pc.category_order ASC');
        }

        if ($hasChild === false) {
            $select->where("pc.parent = ?", '0');
        }

        $options = $this->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    private function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'pc.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'group_name' :
                $operatorFirstPart = " pg.property_name ";
                break;
            case 'property_num' :
                $operatorFirstPart = ' ( SELECT COUNT(pps.id) FROM ' . Zend_Registry::get('dbPrefix') . 'property_page AS pps WHERE pps.category_id = pc.id ) ';
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(pc.cat_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(pc.cat_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(pc.cat_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    private function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>