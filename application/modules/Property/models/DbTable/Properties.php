<?php

/**
 * This is the DbTable class for the guestbook table.
 */
class Property_Model_DbTable_Properties extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'property_page';
    protected $_cols = null;

    //Get Datas
    public function getPropertiesInfo($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            // throw new Exception("Count not find row=$id");
            $options = null;
        } else {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
        return $options;
    }

    public function updateAvailableInformation($id, $data) {
        $this->update(array('available_information' => $data), array('id = ?' => $id));
    }

    public function updatePrice($id, $data) {
        $this->update(array('property_price' => $data), array('id = ?' => $id));
    }

    //Get Datas
    public function getTitleToId($property_title) {
        $property_title = addslashes($property_title);
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.property_title =? ', $property_title)
                    ->limit(1);
            $options = $this->fetchAll($select);
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Price
    public function getPropertyMatchPrice($current_id, $property_price, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.active = ?', '1')
                    ->where('gp.property_price < (' . $property_price . ' + (' . $property_price . '*80/100))')
                    ->where('gp.property_price > (' . $property_price . ' - (' . $property_price . '*80/100))')
                    ->limit($limit);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Region
    public function getPropertyMatchRegion($current_id, $state_id, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.active = ?', '1')
                    ->where('gp.state_id = ?', $state_id)
                    ->limit($limit);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Datas By Agent
    public function getPropertyMatchAgent($current_id, $property_owner, $limit) {
        try {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('*'))
                    ->where('gp.id != ?', $current_id)
                    ->where('gp.active = ?', '1')
                    ->where('gp.property_owner = ?', $property_owner)
                    ->limit($limit);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Next Property
    public function getNextProperty($property_id, $group_id, $type_id = null) {
        try {
            $select = ($type_id) ?
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id >? ', $property_id)
                            ->where('gp.property_type =? ', $type_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id ASC')
                            ->limit(1) :
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id >? ', $property_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id ASC')
                            ->limit(1);
            $row = $this->getAdapter()->fetchAll($select);
            if ($row) {
                $options = $row[0];
            } else {
                $options = '';
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get Previous Property
    public function getPrevProperty($property_id, $group_id, $type_id = null) {
        try {
            $select = ($type_id) ?
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id <? ', $property_id)
                            ->where('gp.property_type =? ', $type_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id DESC')
                            ->limit(1) :
                    $this->select()
                            ->from(array('gp' => $this->_name), array('*'))
                            ->where('gp.id <? ', $property_id)
                            ->where('gp.group_id =? ', $group_id)
                            ->order('gp.id DESC')
                            ->limit(1);
            $row = $this->getAdapter()->fetchAll($select);
            if ($row) {
                $options = $row[0];
            } else {
                $options = '';
            }
            return $options;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    //Get All Datas
    public function getPropertiesInfoByGroup($group_id = null, $approve = null) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $approve = ($approve == null) ? '1=1' : 'gp.active = "' . $approve . '"';

        if (empty($group_id)) {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.entry_by = ?', $user_id)
                        ->orwhere('gp.property_owner = ? ', $user_id)
                        ->where($approve)
                        ->order('gp.group_id ASC')
                        ->order("gp.category_id ASC")
                        ->order('gp.property_order ASC');
            } else {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where($approve)
                        ->order('gp.group_id ASC')
                        ->order("gp.category_id ASC")
                        ->order('gp.property_order ASC');
            }
        } else {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.entry_by = ?', $user_id)
                        ->orwhere('gp.property_owner = ? ', $user_id)
                        ->where('gp.group_id =?', $group_id)
                        ->where($approve)
                        ->order("gp.category_id ASC")
                        ->order('gp.property_order ASC');
            } else {
                $select = $this->select()
                        ->from(array('gp' => $this->_name), array('*'))
                        ->where('gp.group_id =?', $group_id)
                        ->where($approve)
                        ->order("gp.category_id ASC")
                        ->order('gp.property_order ASC');
            }
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            //throw new Exception("Count not find row Category");
            $options = null;
        }
        return $options;
    }

    public function getBorderItems($field = 'property_price', $range = 'DESC', $active = '1') {
        try {
            $select = $this->select()
                    ->from(array('pp' => $this->_name), array($field => 'pp.' . $field))
                    ->where('pp.active =? ', $active)
                    ->order('pp.' . $field . ' ' . $range)
                    ->limit(1);
            $options = $this->fetchAll($select);
            if ($options) {
                return $options[0][$field];
            } else {
                return '0';
            }
        } catch (Exception $e) {
            return '0';
        }
    }

    public function numOfProperty($condition = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $select = $this->select()
                ->from(array('p' => $this->_name), array('COUNT(p.id) AS num_property'));
        if ($auth->hasIdentity() && ($userChecking == true )) {
            $identity = $auth->getIdentity();
            $user_id = $identity->user_id;
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                /* $select = $this->select()
                  ->from(array('p' => $this->_name),array('COUNT(*) AS num_property'))
                  ->where('p.entry_by =?',$user_id)
                  ->orwhere('p.property_owner =?',$user_id); */
                $select->where('p.entry_by = ' . $user_id . ' OR p.property_owner = ' . $user_id);
            }
        }

        if ($condition && is_array($condition)) {
            $select->where('p.' . $condition['field'] . ' ' . $condition['operator'] . ' ' . $condition['value']);
        }

        $options = $this->fetchAll($select);
        return ($options) ? $options[0]['num_property'] : 0;
    }

    public function getNumInActiveProperty($group_id = null) {
        if (empty($group_id)) {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('COUNT(gp.id) as num'))
                    ->where('gp.active =?', '0');
        } else {
            $select = $this->select()
                    ->from(array('gp' => $this->_name), array('COUNT(gp.id) as num'))
                    ->where('gp.group_id =?', $group_id)
                    ->where('gp.active =?', '0');
        }


        $options = $this->fetchAll($select);
        if (!$options) {
            $num_of_news = 0;
        } else {
            foreach ($options as $row) {
                $num_of_news = $row['num'];
            }
        }
        return $num_of_news;
    }

    //Get Related Items
    public function getRelatedItems() {
        $select = $this->select()
                ->from($this->_name, array('id', 'property_name'))
                ->where('active = ?', '1')
                ->order('property_name ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        if ($options) {
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        } else {
            //throw new Exception("Count not find rows $id"); 
            $options = null;
        }
        return $options;
    }

    //Get Price
    public function getPrice($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('property_price' => 'DISTINCT(property_price)'))
                ->order('property_price ' . $order);
        $options = $this->getAdapter()->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getRoomNumber($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_room_no' => 'DISTINCT(feature_room_no)'))
                ->order('feature_room_no ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getPropertyType($order = 'ASC') {
        $select = $this->select()
                ->from(array('p' => $this->_name), array('property_type' => 'DISTINCT(p.property_type)'))
                ->order('p.property_type ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getStars() {
        $select = $this->select()
                ->from($this->_name, array('stars' => 'DISTINCT(stars)'))
                ->order('stars ASC');
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getNumberOfBedsRooms($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_bedroom' => 'DISTINCT(feature_bedroom)'))
                ->order('feature_bedroom ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getNumberOfBathRooms($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('feature_bathroom' => 'DISTINCT(feature_bathroom)'))
                ->order('feature_bathroom ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getPropertySize($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('property_size' => 'DISTINCT(property_size)'))
                ->order('property_size ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getPropertyBuildYear($order = 'ASC') {
        $select = $this->select()
                ->from($this->_name, array('property_build_year' => 'DISTINCT(property_build_year)'))
                ->order('property_build_year ' . $order);
        $options = $this->getAdapter()->fetchAll($select);

        if (!$options) {
            $options = null;
        }
        return $options;
    }

    //Get All Datas
    public function getListInfo($approve = null, $search_params = null, $tableColumns = null) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $property_page_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['property_page'] && is_array($tableColumns['property_page'])) ? $tableColumns['property_page'] : array('pp.*');
        $property_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['property_business_type'] && is_array($tableColumns['property_business_type'])) ? $tableColumns['property_business_type'] : array('business_type' => 'pbt.business_type');
        $property_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['property_category'] && is_array($tableColumns['property_category'])) ? $tableColumns['property_category'] : array('category_name' => 'pc.category_name');
        $cities_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['cities'] && is_array($tableColumns['cities'])) ? $tableColumns['cities'] : array('city' => 'ct.city');
        $countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array('country_name' => 'cut.value');
        $states_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['states'] && is_array($tableColumns['states'])) ? $tableColumns['states'] : array('state_name' => 'st.state_name');
        $vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
        $property_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['property_group'] && is_array($tableColumns['property_group'])) ? $tableColumns['property_group'] : array('group_name' => 'pg.property_name', 'review_id' => 'pg.review_id', 'file_thumb_width' => 'pg.file_thumb_width', 'file_thumb_height' => 'pg.file_thumb_height', 'file_thumb_resize_func' => 'pg.file_thumb_resize_func');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array('username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
        $owner_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_profile'] && is_array($tableColumns['owner_profile'])) ? $tableColumns['owner_profile'] : array('owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) ", 'owner_email' => 'ups.username', 'owner_id' => 'ups.user_id', 'owner_phone' => 'ups.phone', 'owner_postalCode' => 'ups.postalCode', 'owner_address' => 'ups.address', 'country' => 'ups.country');
        $owner_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_country'] && is_array($tableColumns['owner_country'])) ? $tableColumns['owner_country'] : null;
        $dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
        $userChecking = ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
        $whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('pp' => $this->_name), $property_page_column_arr);

        if (($property_group_column_arr && is_array($property_group_column_arr) && count($property_group_column_arr) > 0)) {
            $select->joinLeft(array('pg' => Zend_Registry::get('dbPrefix') . 'property_group'), 'pp.group_id = pg.id', $property_group_column_arr);
        }

        if (($property_business_type_arr && is_array($property_business_type_arr) && count($property_business_type_arr) > 0)) {
            $select->joinLeft(array('pbt' => Zend_Registry::get('dbPrefix') . 'property_business_type'), 'pbt.id = pp.property_type', $property_business_type_arr);
        }

        if (($property_category_column_arr && is_array($property_category_column_arr) && count($property_category_column_arr) > 0)) {
            $select->joinLeft(array('pc' => Zend_Registry::get('dbPrefix') . 'property_category'), 'pc.id = pp.category_id', $property_category_column_arr);
        }

        if (($cities_column_arr && is_array($cities_column_arr) && count($cities_column_arr) > 0)) {
            $select->joinLeft(array('ct' => Zend_Registry::get('dbPrefix') . 'cities'), 'ct.city_id = pp.area_id', $cities_column_arr);
        }

        if (($states_column_arr && is_array($states_column_arr) && count($states_column_arr) > 0)) {
            $select->joinLeft(array('st' => Zend_Registry::get('dbPrefix') . 'states'), 'st.state_id = pp.state_id', $states_column_arr);
        }

        if (($countries_column_arr && is_array($countries_column_arr) && count($countries_column_arr) > 0)) {
            $select->joinLeft(array('cut' => Zend_Registry::get('dbPrefix') . 'countries'), 'cut.id = pp.country_id', $countries_column_arr);
        }

        if (($user_profile_column_arr && is_array($user_profile_column_arr) && count($user_profile_column_arr) > 0)) {
            $select->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'pp.entry_by = up.user_id', $user_profile_column_arr);
        }

        if (($owner_profile_column_arr && is_array($owner_profile_column_arr) && count($owner_profile_column_arr) > 0)) {
            $select->joinLeft(array('ups' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'pp.property_owner = ups.user_id', $owner_profile_column_arr);
        }

        if (($owner_country_column_arr && is_array($owner_country_column_arr) && count($owner_country_column_arr) > 0)) {
            $select->joinLeft(array('ocut' => Zend_Registry::get('dbPrefix') . 'countries'), 'ocut.id = ups.country', $owner_country_column_arr);
        }

        if (($vote_column_arr && is_array($vote_column_arr) && count($vote_column_arr) > 0)) {
            $select->joinLeft(array('vt' => Zend_Registry::get('dbPrefix') . 'vote_voting'), 'pp.id  = vt.table_id', $vote_column_arr);
        }

        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('pp.entry_by = ' . $user_id . ' OR pp.property_owner = ' . $user_id);
        }

        if ($approve != null) {
            $select->where("pp.active = ?", $approve);
        }
        
        file_put_contents('getValue.txt', $search_params['filter']['filters'][0]['value']);
//        print_r($search_params['filter']['filters'][0]['field']);
//        echo "<pre>";
//        exit();
        
        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    if ($sort_value_arr['dir'] == 'exp') {
                        $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                    } else {
                        $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                    }
                }
            } else {
                $select->order("pp.category_id ASC")
                        ->order('pp.property_order ASC');
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("pp.category_id ASC")
                    ->order('pp.property_order ASC');
        }

        if (!empty($dataLimit)) {
            $select->limit($dataLimit);
        }
        file_put_contents("PropertyQuery.txt", $select);
        $options = $this->fetchAll($select);

        if (!$options) {
            $options = null;
        }

        return $options;
    }

    public function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'pp.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'property_id_string' :
                $id_arr = explode(',', $operator_arr['value']);
                $id_arr = array_filter($id_arr);
                $ids = implode(',', $id_arr);
                $operatorFirstPart = " pp.id IN(" . $ids . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'stars_arr':
                $stars_arr = explode(',', $operator_arr['value']);
                $stars_arr = array_filter($stars_arr);
                $stars_string = implode(',', $stars_arr);
                $operatorFirstPart = " pp.stars IN(" . $stars_string . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'category_id_arr':
                $category_id_arr = explode(',', $operator_arr['value']);
                $category_id_arr = array_filter($category_id_arr);
                $category_id_string = implode(',', $category_id_arr);
                $operatorFirstPart = " pp.category_id IN(" . $category_id_string . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'feature_amenities':
                $feature_amenities_arr = explode(',', $operator_arr['value']);
                $feature_amenities_arr = array_filter($feature_amenities_arr);
                foreach ($feature_amenities_arr as $key => $value) {
                    $operatorFirstPartArr[$key] = 'pp.feature_amenities LIKE "%' . $value . '%"';
                }
                $operatorFirstPart = " (" . implode(' AND ', $operatorFirstPartArr) . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'feature_other_amenities':
                $feature_other_amenities_arr = explode(',', $operator_arr['value']);
                $feature_other_amenities_arr = array_filter($feature_other_amenities_arr);
                foreach ($feature_other_amenities_arr as $key => $value) {
                    $operatorFirstPartArr[$key] = 'pp.feature_other_amenities LIKE "%' . $value . '%"';
                }
                $operatorFirstPart = " (" . implode(' AND ', $operatorFirstPartArr) . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'property_price-gte' :
                $operator_arr['field'] = 'property_price';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_price-lte' :
                $operator_arr['field'] = 'property_price';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_room_no-gte' :
                $operator_arr['field'] = 'feature_room_no';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_room_no-lte' :
                $operator_arr['field'] = 'feature_room_no';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_bedroom-lte' :
                $operator_arr['field'] = 'feature_bedroom';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_bedroom-gte' :
                $operator_arr['field'] = 'feature_bedroom';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_bathroom-lte' :
                $operator_arr['field'] = 'feature_bathroom';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'feature_bathroom-gte' :
                $operator_arr['field'] = 'feature_bathroom';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_size-lte' :
                $operator_arr['field'] = 'property_size';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_size-gte' :
                $operator_arr['field'] = 'property_size';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_build_year-lte' :
                $operator_arr['field'] = 'property_build_year';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_build_year-gte' :
                $operator_arr['field'] = 'property_build_year';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_lot-lte' :
                $operator_arr['field'] = 'property_lot';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_lot-gte' :
                $operator_arr['field'] = 'property_lot';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_estate_surface-lte' :
                $operator_arr['field'] = 'property_estate_surface';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_estate_surface-gte' :
                $operator_arr['field'] = 'property_estate_surface';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_land_surface-lte' :
                $operator_arr['field'] = 'property_land_surface';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'property_land_surface-gte' :
                $operator_arr['field'] = 'property_land_surface';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_hospital-lte' :
                $operator_arr['field'] = 'other_distance_hospital';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_hospital-gte' :
                $operator_arr['field'] = 'other_distance_hospital';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_airport-lte' :
                $operator_arr['field'] = 'other_distance_airport';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_airport-gte' :
                $operator_arr['field'] = 'other_distance_airport';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_railway-lte' :
                $operator_arr['field'] = 'other_distance_railway';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_railway-gte' :
                $operator_arr['field'] = 'other_distance_railway';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_school-lte' :
                $operator_arr['field'] = 'other_distance_school';
                $operator_arr['operator'] = 'lte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'other_distance_school-gte' :
                $operator_arr['field'] = 'other_distance_school';
                $operator_arr['operator'] = 'gte';
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            case 'groups' :
                $operatorFirstPart = " pp.group_id IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'categories' :
                $operatorFirstPart = " pp.category_id IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'businesstype' :
                $operatorFirstPart = " pp.property_type IN(" . $operator_arr['value'] . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM " . Zend_Registry::get('dbPrefix') . "vote_voting as vts WHERE pp.id  = vts.table_id) AS total_votes ";
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(pp.property_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(pp.property_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(pp.property_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>