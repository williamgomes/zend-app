<?php
/**
* This is the DbTable class for the countries table.
*/
class Property_Model_DbTable_Country extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'countries';
		
	
	//Get Datas
	public function getCountryInfo() 
    {
       $select = $this->select()
                       ->from($this->_name, array('id', 'value'))
                       ->order('value ASC'); 
		 $options = $this->getAdapter()->fetchPairs($select);
		if (!$options) 
		{
            //throw new Exception("Count not find rows $id");
			$options = null;
        }
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        return $options;   
    }
	
	//Get Datas
	public function getCountryName($country_id) 
    {
        $row = $this->fetchRow('id = ' . $country_id);
        if ($row) 
		{
			$country_name = $row->toArray();
			$country_name = is_array($country_name) ? array_map('stripslashes', $country_name) : stripslashes($country_name);
		}
		else
		{
           // throw new Exception("Count not find row $group_id");
		   $country_name = null;
        }		
        return $country_name;   
    }
	
}

?>