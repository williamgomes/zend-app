<?php
/**
* This is the DbTable class for the property_group table.
*/
class Property_Model_DbTable_PropertyGroup extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'property_group';	
	protected $_cols	=	null;	
		
	
	//Get Datas
	public function getAllGroupInfo() 
    {
       $select = $this->select()
                       ->from($this->_name, array('*'))
                       ->order('id ASC'); 
		 $options = $this->fetchAll($select);
		if (!$options) 
		{
           // throw new Exception("Count not find rows $id");
		   $options = null;
        }
        return $options;   
    }
	
	//Get Datas
	public function getGroupInfo() 
    {
       $select = $this->select()
                       ->from($this->_name, array('id', 'property_name'))
                       ->order('property_name ASC'); 
		 $options = $this->getAdapter()->fetchPairs($select);
		if (!$options) 
		{
            //throw new Exception("Count not find rows $id");
        }
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        return $options;   
    }
	
	//Get Datas
	public function getGroupName($group_id) 
    {
		$group_id = (int)$group_id;
        $row = $this->fetchRow('id = ' . $group_id);
        if (!$row) 
		{
           // throw new Exception("Count not find row $group_id");
		   $group_name = null;
        }
		else
		{
			$group_name = $row->toArray();
			$group_name = is_array($group_name) ? array_map('stripslashes', $group_name) : stripslashes($group_name);
		}
        return $group_name;   
    }
	
	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('pg' => $this->_name),array('pg.id', 'group_name' => 'pg.property_name', 'pg.role_id', 'pg.group_type', 'pg.active', 'pg.entry_by', 'file_num_per_page' => 'pg.file_num_per_page', 'file_col_num' => 'pg.file_col_num', 'file_sort' => 'pg.file_sort', 'file_order' => 'pg.file_order', 'meta_title' => 'pg.meta_title', 'meta_keywords' => 'pg.meta_keywords', 'meta_desc' => 'pg.meta_desc', '(SELECT COUNT(pp.id) FROM '.Zend_Registry::get('dbPrefix').'property_page as pp WHERE pg.id = pp.group_id) AS property_num', '(SELECT COUNT(pbt.id) FROM '.Zend_Registry::get('dbPrefix').'property_business_type as pbt WHERE pg.id = pbt.group_id) AS business_type_num'))
						->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'pg.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))
						->joinLeft(array('pc' => Zend_Registry::get('dbPrefix').'property_category'), 'pg.id = pc.group_id', array( 'category_num' => 'COUNT(pc.id)'))
						->group('pg.id');
							
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('pg.entry_by = ?', $user_id);
		}
		
		if($approve != null)
		{
			$select->where("pg.active = ?", $approve);
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("pg.id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{						
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("pg.id ASC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options; 
	}	
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'pg.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'group_name' :
					$operatorFirstPart = " pg.property_name ";
				break;
			default:
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(pc.cat_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(pc.cat_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(pc.cat_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}

?>