<?php
class Property_Model_Properties
{
	protected $_id;
	protected $_category_id;
	protected $_group_id;
	protected $_property_name;
	protected $_property_title;
	protected $_property_owner;
	protected $_property_type;
	protected $_property_code;
	protected $_property_primary_image;
	protected $_property_image;
	protected $_property_desc;
	protected $_entry_by;
	protected $_property_price;
	protected $_property_order;
	protected $_featured;
	protected $_active;
	protected $_prev_category;
	protected $_prev_group;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;
	protected $_country_id;
	protected $_state_id;
	protected $_area_id;
	protected $_house_location;
	protected $_house_no;
	protected $_house_name;
	protected $_house_phone;
	protected $_house_fax;
	protected $_house_email;
	protected $_post_code;
	protected $_property_address;
	protected $_feature_room_no;
	protected $_feature_reception;
	protected $_feature_dinning_space;
	protected $_feature_kitchen;
	protected $_feature_toilets;
	protected $_feature_bathroom;
	protected $_feature_garden;
	protected $_feature_conservatory;
	protected $_feature_garage;
	protected $_feature_parking;
	protected $_feature_glazing;
	protected $_feature_floor;
	protected $_feature_primary_floor_image;
	protected $_feature_floor_plan_image;
	protected $_feature_google_map;
	protected $_feature_additional;
	protected $_brochure_title;
	protected $_brochure_desc;
	protected $_availabe_from;
	protected $_available_to;
	protected $_available_expire_date;
	protected $_available_activation_date;
	protected $_meter_electric;
	protected $_meter_electric_expire;
	protected $_meter_gas;
	protected $_meter_gas_expire;
	protected $_meter_energy;
	protected $_meter_energy_expire;
	protected $_payment_desc;
	protected $_related_items;
	protected $_available_information;
	
	protected $_property_est_payment;
	protected $_property_size;
	protected $_property_lot;
	protected $_property_build_year;
	protected $_property_estate_surface;
	protected $_property_land_surface;
	protected $_property_tipology;
	protected $_stars;
	
	protected $_feature_highlights;
	protected $_feature_bedroom;
	protected $_feature_balcony;
	protected $_feature_courtyard;
	protected $_feature_furnishing;
	protected $_feature_amenities;
	protected $_feature_other_amenities;
	
	protected $_available_added_on;	
	
	protected $_payment_quote_id;
	protected $_payment_cycle;
	protected $_payment_loan;
	protected $_payment_down;
	protected $_payment_loan_product;
	protected $_payment_interest_rate;	
	protected $_payment_rate_lock;
	protected $_payment_due_in;
	protected $_payment_apr;
	protected $_payment_loan_discount;
	protected $_payment_appraisal_fee;
	protected $_payment_processing_fee;
	protected $_payment_underwriting_fee;	
	protected $_payment_payment_penalty;
	
	protected $_payment_monthly_interest;
	protected $_payment_monthly_taxes;
	protected $_payment_monthly_homeowners;	
	protected $_payment_monthly_mortgage;
	
	protected $_other_nearby_school;
	protected $_other_neighborhood;
	protected $_other_landmark;
	protected $_other_distance_hospital;
	protected $_other_distance_airport;
	protected $_other_distance_railway;	
	protected $_other_distance_school;
	
	protected $_billing_user_places_order_email_enable;
	protected $_billing_user_places_order_email_address;
	protected $_billing_user_pay_invoice_email_enable;
	protected $_billing_user_pay_invoice_email_address;
	protected $_billing_user_cancel_order_email_enable;
	protected $_billing_user_cancel_order_email_address;
	protected $_billing_order_payment_email_enable;
	protected $_billing_item_desc;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveProperties()
		{
			$mapper  = new Property_Model_PropertiesMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function saveDynamicForm($option,$dynamicForm,$result,$request)
		{
			$translator = Zend_Registry::get('translator');
			$field_db = new Members_Model_DbTable_Fields();
			$field_groups = $field_db->getGroupNames($option['form_id']); 
			foreach($field_groups as $group)
			{
				$group_name = $group->field_group;
				$displaGroup = $dynamicForm->getDisplayGroup($group_name);
				$elementsObj = $displaGroup->getElements();
				
				foreach($elementsObj as $element)
				{
					if(substr($element->getName(), -5) != '_prev')
					{
						$table_id = $result['id'];
						$form_id = $option['form_id'];
						$field_id	=	$element->getAttrib('rel');
						$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $request->getPost($element->getName()) : $element->getValue();
						if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
						{ 
							if(is_array($field_value)) { $field_value = ''; }
						}
						if($element->getType() == 'Zend_Form_Element_Multiselect') 
						{ 
							$field_value	=	$request->getPost($element->getName());
							if(is_array($field_value)) { $field_value = implode(',',$field_value); }
						}
						if(($element->getType() == 'Zend_Form_Element_File' && !empty($field_value)) || ($element->getType() != 'Zend_Form_Element_File'))
						{
							try
							{
								$DBconn = Zend_Registry::get('msqli_connection');
								$DBconn->getConnection();
								
								// Remove from Value
								$where = array();
								$where[0] = 'table_id = '.$DBconn->quote($table_id);
								$where[1] = 'form_id = '.$DBconn->quote($form_id);
								$where[2] = 'field_id = '.$DBconn->quote($field_id);
								$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
								
								//Add Value
								$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
								$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
								
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
							}
							catch(Exception $e)
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage());
								
							}
						}	
						else
						{
							$msg = $translator->translator("page_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}					
					}
					else
					{
						$msg = $translator->translator("page_save_success");
					    $json_arr = array('status' => 'ok','msg' => $msg);
					}
				}				
			}
			return $json_arr;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setGroup_id($text)
		{
			$this->_group_id = $text;
			return $this;
		}
		
		public function setCategory_id($text)
		{
			$this->_category_id = $text;
			return $this;
		}
		
		public function setProperty_name($text)
		{
			$this->_property_name = $text;
			return $this;
		}
		
		public function setProperty_title($text,$id = null)
		{
			$pattern = Eicra_File_Constants::TITLE_PATTERN;
			$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
			$text = preg_replace($pattern, $replacement, trim($text));
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			if(empty($id))
			{		
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'property_page'), array('m.id'))
							->where('m.property_title = ?',$text);
			}
			else
			{
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'property_page'), array('m.id'))
							->where('m.property_title = ?',$text)->where('m.id != ?',$id);
			}
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$ids = (int)$row['id'];
				}
			}
			if(empty($ids))
			{
				$this->_property_title = $text;
			}
			else
			{
				$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'property_page'), array('m.id'))
						->order(array('m.id DESC'))->limit(1);
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_id = (int)$row['id'];
					}
				}
				if(empty($id))
				{
					$this->_property_title = $text.'-'.($last_id+1);
				}
				else
				{
					$this->_property_title = $text.'-'.$id;
				}
			}
			return $this;
		}
		
		public function setProperty_owner($text)
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_property_owner = ($globalIdentity->allow_to_change_ownership != '1') ? $globalIdentity->user_id : $text;
			}
			else
			{
				$this->_property_owner = $text;
			}
			
			return $this;
		}
		
		
		public function setRelated_items($text)
		{
			if(is_array($text))
			{					
				$related_items = implode(',',$text);
			}
			else
			{
				$related_items = $text;
			}
			$this->_related_items = $related_items;
			return $this;
		}
		
		public function setProperty_price($text)
		{
			if(empty($text))
			{
				$this->_property_price = 0;
			}
			else
			{
				$this->_property_price = $text;
			}
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setProperty_primary_image($text)
		{
			$this->_property_primary_image = $text;
			return $this;
		}
		
		public function setProperty_image($text)
		{
			$this->_property_image = $text;
			return $this;
		}
		
		public function setProperty_type($text)
		{
			$this->_property_type = $text;
			return $this;
		}
		
		public function setProperty_code($text)
		{
			$this->_property_code = $text;
			return $this;
		}
		
		public function setPrev_category($text)
		{
			$this->_prev_category = $text;
			return $this;
		}
		
		public function setPrev_group($text)
		{
			$this->_prev_group = $text;
			return $this;
		}
		
		
		public function setProperty_order($text)
		{
			if(!empty($text))
			{
				$this->_property_order = $text;
			}
			else
			{
				$group_id = $this->getGroup_id();
				$category_id = $this->getCategory_id();
				if(empty($this->_id))
				{
					$OrderObj = new Property_Controller_Helper_PropertiesOrders();
				}
				else
				{
					$OrderObj = new Property_Controller_Helper_PropertiesOrders($this->_id);
				}
				$OrderObj->setHeighestOrder($group_id,$category_id);				
				$last_order = $OrderObj->getHighOrder();
				
				$this->_property_order = $last_order + 1;
			}
			return $this;
		}		
		
		public function setProperty_desc($text)
		{
			$this->_property_desc =  $text;
			return $this;
		}
		
		public function setFeatured($text)
		{
			$this->_featured = $text;
			return $this;
		}
		
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		public function setMeta_title($text)
		{
			$this->_meta_title =  $text;
			return $this;
		}
		
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords =  $text;
			return $this;
		}
		
		public function setMeta_desc($text)
		{
			$this->_meta_desc =  $text;
			return $this;
		}
		
		public function setCountry_id($text)
		{
			$this->_country_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setState_id($text)
		{
			$this->_state_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setArea_id($text)
		{
			$this->_area_id = ($text)? $text : '0';
			return $this;
		}
		
		public function setHouse_location($text)
		{
			$this->_house_location = $text;
			return $this;
		}
		
		public function setHouse_no($text)
		{
			$this->_house_no =   $text;
			return $this;
		}
		
		public function setHouse_name($text)
		{
			$this->_house_name =  $text;
			return $this;
		}
		
		public function setHouse_phone($text)
		{
			$this->_house_phone = $text;
			return $this;
		}
		
		public function setHouse_fax($text)
		{
			$this->_house_fax = $text;
			return $this;
		}
		
		public function setHouse_email($text)
		{
			$this->_house_email = $text;
			return $this;
		}
		
		public function setPost_code($text)
		{
			$this->_post_code = str_replace(" ","",$text);
			return $this;
		}
		
		public function setProperty_address($text)
		{
			$this->_property_address =  $text;
			return $this;
		}
		
		public function setFeature_room_no($text)
		{
			$this->_feature_room_no = ($text)? $text : '0';
			return $this;
		}
		
		public function setFeature_reception($text)
		{
			$this->_feature_reception =  $text;
			return $this;
		}
		
		public function setFeature_dinning_space($text)
		{
			$this->_feature_dinning_space =  $text;
			return $this;
		}
		
		public function setFeature_kitchen($text)
		{
			$this->_feature_kitchen =  $text;
			return $this;
		}
		
		public function setFeature_toilets($text)
		{
			$this->_feature_toilets =  $text;
			return $this;
		}
		
		public function setFeature_bathroom($text)
		{
			$this->_feature_bathroom =  $text;
			return $this;
		}
		
		public function setFeature_garden($text)
		{
			$this->_feature_garden =  $text;
			return $this;
		}
		
		public function setFeature_conservatory($text)
		{
			$this->_feature_conservatory =  $text;
			return $this;
		}
		
		public function setFeature_garage($text)
		{
			$this->_feature_garage =  $text;
			return $this;
		}
		
		public function setFeature_parking($text)
		{
			$this->_feature_parking =  $text;
			return $this;
		}
		
		public function setFeature_glazing($text)
		{
			$this->_feature_glazing =  $text;
			return $this;
		}
		
		public function setFeature_floor($text)
		{
			$this->_feature_floor =  $text;
			return $this;
		}
		
		public function setFeature_primary_floor_image($text)
		{
			$this->_feature_primary_floor_image = $text;
			return $this;
		}
		
		public function setFeature_floor_plan_image($text)
		{
			$this->_feature_floor_plan_image = $text;
			return $this;
		}
		
		public function setFeature_google_map($text)
		{
			$this->_feature_google_map =  ($text) ? $text : null;
			return $this;
		}
		
		public function setFeature_additional($text)
		{
			$this->_feature_additional =  $text;
			return $this;
		}
		
		public function setBrochure_title($text)
		{
			$this->_brochure_title =  $text;
			return $this;
		}
		
		public function setBrochure_desc($text)
		{
			$this->_brochure_desc =  $text;
			return $this;
		}
		
		public function setAvailabe_from($text)
		{
			$this->_availabe_from = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_to($text)
		{
			$this->_available_to = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_expire_date($text)
		{
			$this->_available_expire_date = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setAvailable_activation_date($text)
		{
			$this->_available_activation_date = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_electric($text)
		{
			$this->_meter_electric =  $text;
			return $this;
		}
		
		public function setMeter_electric_expire($text)
		{
			$this->_meter_electric_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_gas($text)
		{
			$this->_meter_gas =  $text;
			return $this;
		}
		
		public function setMeter_gas_expire($text)
		{
			$this->_meter_gas_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setMeter_energy($text)
		{
			$this->_meter_energy =  $text;
			return $this;
		}
		
		public function setMeter_energy_expire($text)
		{
			$this->_meter_energy_expire = ($text)? $text : '0000-00-00';
			return $this;
		}
		
		public function setPayment_desc($text)
		{
			$this->_payment_desc =  $text;
			return $this;
		}
		
		public function setAvailable_information($id, $price)
		{
			if(!empty($id))
			{
				$property_db = new Property_Model_DbTable_Properties();
				$property_info = $property_db->getPropertiesInfo($id);
				if(!empty($property_info['available_information']))
				{
					$available_information_arr = explode(',',$property_info['available_information']);
					$price_arr = explode(';;',$available_information_arr[0]);
					$price_prev = $price_arr[2];
					$this->_available_information = str_replace($price_prev, $price, $property_info['available_information']);
				}
				else
				{
					$this->_available_information = '';
				}
			}
			else
			{
				$this->_available_information = '';
			}
			return $this;
		}
		
		public function setProperty_est_payment($text)
		{
			$this->_property_est_payment =  $text;
			return $this;
		}
		
		public function setProperty_size($text)
		{
			$this->_property_size =  $text;
			return $this;
		}
		
		public function setProperty_lot($text)
		{
			$this->_property_lot =  $text;
			return $this;
		}
		
		public function setProperty_build_year($text)
		{
			$this->_property_build_year =  $text;
			return $this;
		}
		
		public function setProperty_estate_surface($text)
		{
			$this->_property_estate_surface =  $text;
			return $this;
		}
		
		public function setProperty_land_surface($text)
		{
			$this->_property_land_surface =  $text;
			return $this;
		}
		
		public function setProperty_tipology($text)
		{
			$this->_property_tipology =  $text;
			return $this;
		}
		
		public function setStars($text)
		{
			$this->_stars =  $text;
			return $this;
		}
		
		public function setFeature_highlights($text)
		{
			$this->_feature_highlights =  $text;
			return $this;
		}
		
		public function setFeature_bedroom($text)
		{
			$this->_feature_bedroom =  $text;
			return $this;
		}
		
		public function setFeature_balcony($text)
		{
			$this->_feature_balcony =  $text;
			return $this;
		}
		
		public function setFeature_courtyard($text)
		{
			$this->_feature_courtyard =  $text;
			return $this;
		}
		
		public function setFeature_furnishing($text)
		{
			$this->_feature_furnishing =  $text;
			return $this;
		}
		
		public function setFeature_amenities($text)
		{
			if(is_array($text))
			{					
				$feature_amenities = implode(',',$text);
			}
			else
			{
				$feature_amenities = $text;
			}
			$this->_feature_amenities =  $feature_amenities;
			return $this;
		}
		
		public function setFeature_other_amenities($text)
		{
			if(is_array($text))
			{					
				$feature_other_amenities = implode(',',$text);
			}
			else
			{
				$feature_other_amenities = $text;
			}
			$this->_feature_other_amenities =  $feature_other_amenities;
			return $this;
		}
		
		public function setAvailable_added_on($text)
		{
			$this->_available_added_on =  $text;
			return $this;
		}
		
		public function setPayment_quote_id($text)
		{
			$this->_payment_quote_id =  $text;
			return $this;
		}
		
		public function setPayment_cycle($text)
		{
			$this->_payment_cycle =  $text;
			return $this;
		}
		
		public function setPayment_loan($text)
		{
			$this->_payment_loan =  $text;
			return $this;
		}
		
		public function setPayment_down($text)
		{
			$this->_payment_down =  $text;
			return $this;
		}
		
		public function setPayment_loan_product($text)
		{
			$this->_payment_loan_product =  $text;
			return $this;
		}
		
		public function setPayment_interest_rate($text)
		{
			$this->_payment_interest_rate =  $text;
			return $this;
		}
		
		public function setPayment_rate_lock($text)
		{
			$this->_payment_rate_lock =  $text;
			return $this;
		}
		
		public function setPayment_due_in($text)
		{
			$this->_payment_due_in =  $text;
			return $this;
		}
		
		public function setPayment_apr($text)
		{
			$this->_payment_apr =  $text;
			return $this;
		}
		
		public function setPayment_loan_discount($text)
		{
			$this->_payment_loan_discount =  $text;
			return $this;
		}
		
		public function setPayment_appraisal_fee($text)
		{
			$this->_payment_appraisal_fee =  $text;
			return $this;
		}
		
		public function setPayment_processing_fee($text)
		{
			$this->_payment_processing_fee =  $text;
			return $this;
		}
		
		public function setPayment_underwriting_fee($text)
		{
			$this->_payment_underwriting_fee =  $text;
			return $this;
		}
		
		public function setPayment_payment_penalty($text)
		{
			$this->_payment_payment_penalty =  $text;
			return $this;
		}
		
		public function setPayment_monthly_interest($text)
		{
			$this->_payment_monthly_interest =  $text;
			return $this;
		}
		
		public function setPayment_monthly_taxes($text)
		{
			$this->_payment_monthly_taxes =  $text;
			return $this;
		}
		
		public function setPayment_monthly_homeowners($text)
		{
			$this->_payment_monthly_homeowners =  $text;
			return $this;
		}
		
		public function setPayment_monthly_mortgage($text)
		{
			$this->_payment_monthly_mortgage =  $text;
			return $this;
		}
		
		public function setOther_nearby_school($text)
		{
			$this->_other_nearby_school =  $text;
			return $this;
		}
		
		public function setOther_neighborhood($text)
		{
			$this->_other_neighborhood =  $text;
			return $this;
		}
		
		public function setOther_landmark($text)
		{
			$this->_other_landmark =  $text;
			return $this;
		}
		
		public function setOther_distance_hospital($text)
		{
			$this->_other_distance_hospital =  $text;
			return $this;
		}
		
		public function setOther_distance_airport($text)
		{
			$this->_other_distance_airport =  $text;
			return $this;
		}
		
		public function setOther_distance_railway($text)
		{
			$this->_other_distance_railway =  $text;
			return $this;
		}
		
		public function setOther_distance_school($text)
		{
			$this->_other_distance_school =  $text;
			return $this;
		}		
		
		public function setBilling_user_places_order_email_enable($text)
		{
			$this->_billing_user_places_order_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_places_order_email_address($text)
		{
			$this->_billing_user_places_order_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_user_pay_invoice_email_enable($text)
		{
			$this->_billing_user_pay_invoice_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_pay_invoice_email_address($text)
		{
			$this->_billing_user_pay_invoice_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_user_cancel_order_email_enable($text)
		{
			$this->_billing_user_cancel_order_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_user_cancel_order_email_address($text)
		{
			$this->_billing_user_cancel_order_email_address =  $text;
			return $this;
		}		
		
		public function setBilling_order_payment_email_enable($text)
		{
			$this->_billing_order_payment_email_enable =  $text;
			return $this;
		}		
		
		public function setBilling_item_desc($text)
		{
			$this->_billing_item_desc =  $text;
			return $this;
		}
		
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getGroup_id()
		{         
			return $this->_group_id;
		}
		
		public function getCategory_id()
		{
			return $this->_category_id;
		}
		
		public function getProperty_name()
		{
			return $this->_property_name;
		}
		
		public function getProperty_title()
		{
			return $this->_property_title;
		}
		
		public function getProperty_owner()
		{
			return $this->_property_owner;
		}
		
		public function getProperty_price()
		{         
			return $this->_property_price;
		}
		
		
		public function getRelated_items()
		{
			return $this->_related_items;
		}
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getProperty_primary_image()
		{
			return $this->_property_primary_image;
		}
		
		public function getProperty_image()
		{         
			return $this->_property_image;
		}
		
		
		public function getProperty_type()
		{         
			return $this->_property_type = ($this->_property_type)? $this->_property_type : '0';
		}
		
		public function getProperty_code()
		{
			return $this->_property_code;
		}
		
		public function getPrev_category()
		{
			return $this->_prev_category;
		}
		
		public function getPrev_group()
		{
			return $this->_prev_group;
		}
		
		public function getProperty_order()
		{   
			if(empty($this->_property_order))
			{ 
				$this->setProperty_order(''); 
			}    
			return $this->_property_order;
		}
				
		public function getProperty_desc()
		{         
			return $this->_property_desc;
		}
		
		public function getFeatured()
		{         
			return $this->_featured;
		}
		
		public function getActive()
		{         
			return $this->_active;
		}		
		
		public function getMeta_title()
		{
			return $this->_meta_title;
		}
		
		public function getMeta_keywords()
		{
			return $this->_meta_keywords;
		}
		
		public function getMeta_desc()
		{
			return $this->_meta_desc;
		}
		
		public function getCountry_id()
		{
			return $this->_country_id = ($this->_country_id)? $this->_country_id : '0';
		}
		
		public function getState_id()
		{
			return $this->_state_id = ($this->_state_id)? $this->_state_id : '0';
		}
		
		public function getArea_id()
		{
			return $this->_area_id = ($this->_area_id)? $this->_area_id : '0';
		}
		
		public function getHouse_location()
		{
			return  $this->_house_location;
		}
		
		public function getHouse_no()
		{
			return $this->_house_no;
		}
		
		public function getHouse_name()
		{
			return $this->_house_name;
		}
		
		public function getHouse_phone()
		{
			return $this->_house_phone;
		}
		
		public function getHouse_fax()
		{
			return $this->_house_fax;
		}
		
		public function getHouse_email()
		{
			return $this->_house_email;
		}
		
		public function getPost_code()
		{
			return $this->_post_code;
		}
		
		public function getProperty_address()
		{
			return $this->_property_address;
		}
		
		public function getFeature_room_no()
		{
			return $this->_feature_room_no = ($this->_feature_room_no)? $this->_feature_room_no : '0';
		}
		
		public function getFeature_reception()
		{
			return $this->_feature_reception;
		}
		
		public function getFeature_dinning_space()
		{
			return $this->_feature_dinning_space;
		}
		
		public function getFeature_kitchen()
		{
			return $this->_feature_kitchen;
		}
		
		public function getFeature_toilets()
		{
			return $this->_feature_toilets;
		}
		
		public function getFeature_bathroom()
		{
			return $this->_feature_bathroom;
		}
		
		public function getFeature_garden()
		{
			return $this->_feature_garden;
		}
		
		public function getFeature_conservatory()
		{
			return $this->_feature_conservatory;
		}
		
		public function getFeature_garage()
		{
			return $this->_feature_garage;
		}
		
		public function getFeature_parking()
		{
			return $this->_feature_parking;
		}
		
		public function getFeature_glazing()
		{
			return $this->_feature_glazing;
		}
		
		public function getFeature_floor()
		{
			return $this->_feature_floor;
		}
		
		public function getFeature_primary_floor_image()
		{
			return $this->_feature_primary_floor_image;
		}
		
		public function getFeature_floor_plan_image()
		{
			return $this->_feature_floor_plan_image;
		}
		
		public function getFeature_google_map()
		{
			return $this->_feature_google_map;
		}
		
		public function getFeature_additional()
		{
			return $this->_feature_additional;
		}
		
		public function getBrochure_title()
		{
			return $this->_brochure_title;
		}
		
		public function getBrochure_desc()
		{
			return $this->_brochure_desc;
		}
		
		public function getAvailabe_from()
		{
			return $this->_availabe_from = ($this->_availabe_from)? $this->_availabe_from : '0000-00-00';
		}
		
		public function getAvailable_to()
		{
			return $this->_available_to = ($this->_available_to)? $this->_available_to : '0000-00-00';
		}
		
		public function getAvailable_expire_date()
		{
			return $this->_available_expire_date = ($this->_available_expire_date)? $this->_available_expire_date : '0000-00-00';
		}
		
		public function getAvailable_activation_date()
		{
			return $this->_available_activation_date = ($this->_available_activation_date)? $this->_available_activation_date : '0000-00-00';
		}
		
		public function getMeter_electric()
		{
			return $this->_meter_electric;
		}
		
		public function getMeter_electric_expire()
		{
			return $this->_meter_electric_expire = ($this->_meter_electric_expire)? $this->_meter_electric_expire : '0000-00-00';
		}
		
		public function getMeter_gas()
		{
			return $this->_meter_gas;
		}
		
		public function getMeter_gas_expire()
		{
			return $this->_meter_gas_expire = ($this->_meter_gas_expire)? $this->_meter_gas_expire : '0000-00-00';
		}
		
		public function getMeter_energy()
		{
			return $this->_meter_energy;
		}
		
		public function getMeter_energy_expire()
		{
			return $this->_meter_energy_expire = ($this->_meter_energy_expire )? $this->_meter_energy_expire  : '0000-00-00';
		}
		
		public function getPayment_desc()
		{
			return $this->_payment_desc;
		}
		
		public function getAvailable_information()
		{
			return $this->_available_information;
		}
		
		public function getProperty_est_payment()
		{         
			return $this->_property_est_payment;
		}
		
		public function getProperty_size()
		{         
			return $this->_property_size;
		}
		
		public function getProperty_lot()
		{         
			return $this->_property_lot;
		}
		
		public function getProperty_build_year()
		{         
			return $this->_property_build_year;
		}
		
		public function getProperty_estate_surface()
		{         
			return $this->_property_estate_surface;
		}
		
		public function getProperty_land_surface()
		{         
			return $this->_property_land_surface;
		}
		
		public function getProperty_tipology()
		{         
			return $this->_property_tipology;
		}
		
		public function getStars()
		{         
			return $this->_stars;
		}
		
		public function getFeature_highlights()
		{         
			return $this->_feature_highlights;
		}
		
		public function getFeature_bedroom()
		{         
			return $this->_feature_bedroom;
		}
		
		public function getFeature_balcony()
		{         
			return $this->_feature_balcony;
		}
		
		public function getFeature_courtyard()
		{         
			return $this->_feature_courtyard;
		}
		
		public function getFeature_furnishing()
		{         
			return $this->_feature_furnishing;
		}
		
		public function getFeature_amenities()
		{         
			return $this->_feature_amenities;
		}
		
		public function getFeature_other_amenities()
		{         
			return $this->_feature_other_amenities;
		}
		
		public function getAvailable_added_on()
		{         
			return $this->_available_added_on;
		}
		
		public function getPayment_quote_id()
		{         
			return $this->_payment_quote_id;
		}
		
		public function getPayment_cycle()
		{         
			return $this->_payment_cycle;
		}
		
		public function getPayment_loan()
		{         
			return $this->_payment_loan;
		}
		
		public function getPayment_down()
		{         
			return $this->_payment_down;
		}
		
		public function getPayment_loan_product()
		{         
			return $this->_payment_loan_product;
		}
		
		public function getPayment_interest_rate()
		{         
			return $this->_payment_interest_rate;
		}
		
		public function getPayment_rate_lock()
		{         
			return $this->_payment_rate_lock;
		}
		
		public function getPayment_due_in()
		{         
			return $this->_payment_due_in;
		}
		
		public function getPayment_apr()
		{         
			return $this->_payment_apr;
		}
		
		public function getPayment_loan_discount()
		{         
			return $this->_payment_loan_discount;
		}
		
		public function getPayment_appraisal_fee()
		{         
			return $this->_payment_appraisal_fee;
		}
		
		public function getPayment_processing_fee()
		{         
			return $this->_payment_processing_fee;
		}
		
		public function getPayment_underwriting_fee()
		{         
			return $this->_payment_underwriting_fee;
		}
		
		public function getPayment_payment_penalty()
		{         
			return $this->_payment_payment_penalty;
		}
		
		public function getPayment_monthly_interest()
		{         
			return $this->_payment_monthly_interest;
		}
		
		public function getPayment_monthly_taxes()
		{         
			return $this->_payment_monthly_taxes;
		}
		
		public function getPayment_monthly_homeowners()
		{         
			return $this->_payment_monthly_homeowners;
		}
		
		public function getPayment_monthly_mortgage()
		{         
			return $this->_payment_monthly_mortgage;
		}
		
		public function getOther_nearby_school()
		{         
			return $this->_other_nearby_school;
		}
		
		public function getOther_neighborhood()
		{         
			return $this->_other_neighborhood;
		}
		
		public function getOther_landmark()
		{         
			return $this->_other_landmark;
		}
		
		public function getOther_distance_hospital()
		{         
			return $this->_other_distance_hospital;
		}
		
		public function getOther_distance_airport()
		{         
			return $this->_other_distance_airport;
		}
		
		public function getOther_distance_railway()
		{         
			return $this->_other_distance_railway;
		}
		
		public function getOther_distance_school()
		{         
			return $this->_other_distance_school;
		}		
		
	public function getBilling_user_places_order_email_enable()
	{
		return $this->_billing_user_places_order_email_enable;
	}		
	
	public function getBilling_user_places_order_email_address()
	{
		return $this->_billing_user_places_order_email_address;
	}		
	
	public function getBilling_user_pay_invoice_email_enable()
	{
		return $this->_billing_user_pay_invoice_email_enable;
	}		
	
	public function getBilling_user_pay_invoice_email_address()
	{
		return $this->_billing_user_pay_invoice_email_address;
	}		
	
	public function getBilling_user_cancel_order_email_enable()
	{
		return $this->_billing_user_cancel_order_email_enable;
	}		
	
	public function getBilling_user_cancel_order_email_address()
	{
		return $this->_billing_user_cancel_order_email_address;
	}		
	
	public function getBilling_order_payment_email_enable()
	{
		return $this->_billing_order_payment_email_enable;
	}		
	
	public function getBilling_item_desc()
	{
		return $this->_billing_item_desc;
	}
		
}
?>