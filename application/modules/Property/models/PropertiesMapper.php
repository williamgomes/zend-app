<?php
class Property_Model_PropertiesMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Property_Model_DbTable_Properties');
        }
        return $this->_dbTable;
    }
	
	public function save(Property_Model_Properties $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				try 
				{
					unset($data['id']);
					
					$data = array(					
						'group_id' 					=> 	$obj->getGroup_id(),
						'category_id' 				=>	$obj->getCategory_id(),
						'property_name' 			=>	$obj->getProperty_name(),
						'property_title' 			=>	$obj->getProperty_title(),
						'property_owner' 			=>	$obj->getProperty_owner(),
						'property_code' 			=>	$obj->getProperty_code(),
						'property_type'	 			=>	$obj->getProperty_type(),
						'property_price' 			=>	$obj->getProperty_price(),
						'related_items'				=>	$obj->getRelated_items(),
						'property_primary_image'	=>	$obj->getProperty_primary_image(),
						'property_image' 			=>	$obj->getProperty_image(),
						'entry_by' 					=>	$obj->getEntry_by(),
						'property_desc' 			=>	$obj->getProperty_desc(),					
						'property_order'			=>	$obj->getProperty_order(),	
						'property_date' 			=>	date("Y-m-d h:i:s"),				
						'featured' 					=>	$obj->getFeatured(),
						'active' 					=>	$obj->getActive(),
						'meta_title' 				=>	$obj->getMeta_title(),
						'meta_keywords' 			=>	$obj->getMeta_keywords(),
						'meta_desc' 				=>	$obj->getMeta_desc(),
						'country_id' 				=>	$obj->getCountry_id(),					
						'state_id' 					=>	$obj->getState_id(),
						'area_id' 					=>	$obj->getArea_id(),
						'house_location' 			=>	$obj->getHouse_location(),
						'house_no' 					=>	$obj->getHouse_no(),
						'house_name' 				=>	$obj->getHouse_name(),
						'house_phone' 				=>	$obj->getHouse_phone(),
						'house_fax' 				=>	$obj->getHouse_fax(),
						'house_email' 				=>	$obj->getHouse_email(),
						'post_code' 				=>	$obj->getPost_code(),
						'property_address' 			=>	$obj->getProperty_address(),
						'feature_room_no' 			=>	$obj->getFeature_room_no(),
						'feature_reception' 		=>	$obj->getFeature_reception(),
						'feature_dinning_space' 	=>	$obj->getFeature_dinning_space(),
						'feature_kitchen' 			=>	$obj->getFeature_kitchen(),
						'feature_toilets' 			=>	$obj->getFeature_toilets(),
						'feature_bathroom' 			=>	$obj->getFeature_bathroom(),
						'feature_garden' 			=>	$obj->getFeature_garden(),
						'feature_conservatory' 		=>	$obj->getFeature_conservatory(),
						'feature_garage' 			=>	$obj->getFeature_garage(),
						'feature_parking' 			=>	$obj->getFeature_parking(),
						'feature_glazing' 			=>	$obj->getFeature_glazing(),
						'feature_floor' 			=>	$obj->getFeature_floor(),
						'feature_primary_floor_image'=>	$obj->getFeature_primary_floor_image(),
						'feature_floor_plan_image' 	=>	$obj->getFeature_floor_plan_image(),
						'feature_google_map' 		=>	$obj->getFeature_google_map(),
						'feature_additional' 		=>	$obj->getFeature_additional(),
						'brochure_title' 			=>	$obj->getBrochure_title(),
						'brochure_desc' 			=>	$obj->getBrochure_desc(),
						'availabe_from' 			=>	$obj->getAvailabe_from(),
						'available_to' 				=>	$obj->getAvailable_to(),
						'available_expire_date' 	=>	$obj->getAvailable_expire_date(),
						'available_activation_date' =>	$obj->getAvailable_activation_date(),
						'meter_electric' 			=>	$obj->getMeter_electric(),
						'meter_electric_expire' 	=>	$obj->getMeter_electric_expire(),
						'meter_gas' 				=>	$obj->getMeter_gas(),
						'meter_gas_expire' 			=>	$obj->getMeter_gas_expire(),
						'meter_energy' 				=>	$obj->getMeter_energy(),
						'meter_energy_expire' 		=>	$obj->getMeter_energy_expire(),
						'payment_desc' 				=>	$obj->getPayment_desc(),
						'available_information'		=>	$obj->getAvailable_information(),
						'property_est_payment'		=>	$obj->getProperty_est_payment(),
						'property_size'				=>	$obj->getProperty_size(),
						'property_lot'				=>	$obj->getProperty_lot(),
						'property_build_year'		=>	$obj->getProperty_build_year(),
						'property_estate_surface'	=>	$obj->getProperty_estate_surface(),
						'property_land_surface'		=>	$obj->getProperty_land_surface(),
						'property_tipology'			=>	$obj->getProperty_tipology(),
						'stars'						=>	$obj->getStars(),
						'feature_highlights'		=>	$obj->getFeature_highlights(),
						'feature_bedroom'			=>	$obj->getFeature_bedroom(),
						'feature_balcony'			=>	$obj->getFeature_balcony(),
						'feature_courtyard'			=>	$obj->getFeature_courtyard(),
						'feature_furnishing'		=>	$obj->getFeature_furnishing(),
						'feature_amenities'			=>	$obj->getFeature_amenities(),
						'feature_other_amenities'	=>	$obj->getFeature_other_amenities(),
						'available_added_on'		=>	$obj->getAvailable_added_on(),
						'payment_quote_id'			=>	$obj->getPayment_quote_id(),
						'payment_cycle'				=>	$obj->getPayment_cycle(),
						'payment_loan'				=>	$obj->getPayment_loan(),
						'payment_down'				=>	$obj->getPayment_down(),
						'payment_loan_product'		=>	$obj->getPayment_loan_product(),
						'payment_interest_rate'		=>	$obj->getPayment_interest_rate(),
						'payment_rate_lock'			=>	$obj->getPayment_rate_lock(),
						'payment_due_in'			=>	$obj->getPayment_due_in(),
						'payment_apr'				=>	$obj->getPayment_apr(),
						'payment_loan_discount'		=>	$obj->getPayment_loan_discount(),
						'payment_appraisal_fee'		=>	$obj->getPayment_appraisal_fee(),
						'payment_processing_fee'	=>	$obj->getPayment_processing_fee(),
						'payment_underwriting_fee'	=>	$obj->getPayment_underwriting_fee(),
						'payment_payment_penalty'	=>	$obj->getPayment_payment_penalty(),
						'payment_monthly_interest'	=>	$obj->getPayment_monthly_interest(),
						'payment_monthly_taxes'		=>	$obj->getPayment_monthly_taxes(),
						'payment_monthly_homeowners'=>	$obj->getPayment_monthly_homeowners(),
						'payment_monthly_mortgage'	=>	$obj->getPayment_monthly_mortgage(),
						'other_nearby_school'		=>	$obj->getOther_nearby_school(),
						'other_neighborhood'		=>	$obj->getOther_neighborhood(),
						'other_landmark'			=>	$obj->getOther_landmark(),
						'other_distance_hospital'	=>	$obj->getOther_distance_hospital(),
						'other_distance_airport'	=>	$obj->getOther_distance_airport(),
						'other_distance_railway'	=>	$obj->getOther_distance_railway(),
						'other_distance_school'		=>	$obj->getOther_distance_school(),
						'activation_never'			=>	'0',
						'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
						'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
						'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
						'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
						'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
						'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
						'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
						'billing_item_desc' 						=>	$obj->getBilling_item_desc()	
					);
				
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{
				try 
				{	
					if(($obj->getCategory_id() == $obj->getPrev_category()) && ($obj->getGroup_id() == $obj->getPrev_group()))
					{
						$data = array(					
							'group_id' 					=> 	$obj->getGroup_id(),
							'category_id' 				=>	$obj->getCategory_id(),
							'property_name' 			=>	$obj->getProperty_name(),
							'property_title' 			=>	$obj->getProperty_title(),
							'property_owner' 			=>	$obj->getProperty_owner(),
							'property_code' 			=>	$obj->getProperty_code(),
							'property_price' 			=>	$obj->getProperty_price(),
							'related_items'				=>	$obj->getRelated_items(),
							'property_type'	 			=>	$obj->getProperty_type(),
							'property_primary_image'	=>	$obj->getProperty_primary_image(),
							'property_image' 			=>	$obj->getProperty_image(),
							'property_desc' 			=>	$obj->getProperty_desc(),
							'meta_title' 				=>	$obj->getMeta_title(),
							'meta_keywords' 			=>	$obj->getMeta_keywords(),
							'meta_desc' 				=>	$obj->getMeta_desc(),
							'country_id' 				=>	$obj->getCountry_id(),					
							'state_id' 					=>	$obj->getState_id(),
							'area_id' 					=>	$obj->getArea_id(),
							'house_location' 			=>	$obj->getHouse_location(),
							'house_no' 					=>	$obj->getHouse_no(),
							'house_name' 				=>	$obj->getHouse_name(),
							'house_phone' 				=>	$obj->getHouse_phone(),
							'house_fax' 				=>	$obj->getHouse_fax(),
							'house_email' 				=>	$obj->getHouse_email(),
							'post_code' 				=>	$obj->getPost_code(),
							'property_address' 			=>	$obj->getProperty_address(),
							'feature_room_no' 			=>	$obj->getFeature_room_no(),
							'feature_reception' 		=>	$obj->getFeature_reception(),
							'feature_dinning_space' 	=>	$obj->getFeature_dinning_space(),
							'feature_kitchen' 			=>	$obj->getFeature_kitchen(),
							'feature_toilets' 			=>	$obj->getFeature_toilets(),
							'feature_bathroom' 			=>	$obj->getFeature_bathroom(),
							'feature_garden' 			=>	$obj->getFeature_garden(),
							'feature_conservatory' 		=>	$obj->getFeature_conservatory(),
							'feature_garage' 			=>	$obj->getFeature_garage(),
							'feature_parking' 			=>	$obj->getFeature_parking(),
							'feature_glazing' 			=>	$obj->getFeature_glazing(),
							'feature_floor' 			=>	$obj->getFeature_floor(),
							'feature_primary_floor_image'=>	$obj->getFeature_primary_floor_image(),
							'feature_floor_plan_image' 	=>	$obj->getFeature_floor_plan_image(),
							'feature_google_map' 		=>	$obj->getFeature_google_map(),
							'feature_additional' 		=>	$obj->getFeature_additional(),
							'brochure_title' 			=>	$obj->getBrochure_title(),
							'brochure_desc' 			=>	$obj->getBrochure_desc(),
							'availabe_from' 			=>	$obj->getAvailabe_from(),
							'available_to' 				=>	$obj->getAvailable_to(),
							'available_expire_date' 	=>	$obj->getAvailable_expire_date(),
							'available_activation_date' =>	$obj->getAvailable_activation_date(),
							'meter_electric' 			=>	$obj->getMeter_electric(),
							'meter_electric_expire' 	=>	$obj->getMeter_electric_expire(),
							'meter_gas' 				=>	$obj->getMeter_gas(),
							'meter_gas_expire' 			=>	$obj->getMeter_gas_expire(),
							'meter_energy' 				=>	$obj->getMeter_energy(),
							'meter_energy_expire' 		=>	$obj->getMeter_energy_expire(),
							'available_information'		=>	$obj->getAvailable_information(),
							'payment_desc' 				=>	$obj->getPayment_desc(),
							'property_est_payment'		=>	$obj->getProperty_est_payment(),
							'property_size'				=>	$obj->getProperty_size(),
							'property_lot'				=>	$obj->getProperty_lot(),
							'property_build_year'		=>	$obj->getProperty_build_year(),
							'property_estate_surface'	=>	$obj->getProperty_estate_surface(),
							'property_land_surface'		=>	$obj->getProperty_land_surface(),
							'property_tipology'			=>	$obj->getProperty_tipology(),
							'stars'						=>	$obj->getStars(),
							'feature_highlights'		=>	$obj->getFeature_highlights(),
							'feature_bedroom'			=>	$obj->getFeature_bedroom(),
							'feature_balcony'			=>	$obj->getFeature_balcony(),
							'feature_courtyard'			=>	$obj->getFeature_courtyard(),
							'feature_furnishing'		=>	$obj->getFeature_furnishing(),
							'feature_amenities'			=>	$obj->getFeature_amenities(),
							'feature_other_amenities'	=>	$obj->getFeature_other_amenities(),
							'available_added_on'		=>	$obj->getAvailable_added_on(),
							'payment_quote_id'			=>	$obj->getPayment_quote_id(),
							'payment_cycle'				=>	$obj->getPayment_cycle(),
							'payment_loan'				=>	$obj->getPayment_loan(),
							'payment_down'				=>	$obj->getPayment_down(),
							'payment_loan_product'		=>	$obj->getPayment_loan_product(),
							'payment_interest_rate'		=>	$obj->getPayment_interest_rate(),
							'payment_rate_lock'			=>	$obj->getPayment_rate_lock(),
							'payment_due_in'			=>	$obj->getPayment_due_in(),
							'payment_apr'				=>	$obj->getPayment_apr(),
							'payment_loan_discount'		=>	$obj->getPayment_loan_discount(),
							'payment_appraisal_fee'		=>	$obj->getPayment_appraisal_fee(),
							'payment_processing_fee'	=>	$obj->getPayment_processing_fee(),
							'payment_underwriting_fee'	=>	$obj->getPayment_underwriting_fee(),
							'payment_payment_penalty'	=>	$obj->getPayment_payment_penalty(),
							'payment_monthly_interest'	=>	$obj->getPayment_monthly_interest(),
							'payment_monthly_taxes'		=>	$obj->getPayment_monthly_taxes(),
							'payment_monthly_homeowners'=>	$obj->getPayment_monthly_homeowners(),
							'payment_monthly_mortgage'	=>	$obj->getPayment_monthly_mortgage(),
							'other_nearby_school'		=>	$obj->getOther_nearby_school(),
							'other_neighborhood'		=>	$obj->getOther_neighborhood(),
							'other_landmark'			=>	$obj->getOther_landmark(),
							'other_distance_hospital'	=>	$obj->getOther_distance_hospital(),
							'other_distance_airport'	=>	$obj->getOther_distance_airport(),
							'other_distance_railway'	=>	$obj->getOther_distance_railway(),
							'other_distance_school'		=>	$obj->getOther_distance_school(),
							'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
							'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
							'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
							'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
							'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
							'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
							'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
							'billing_item_desc' 						=>	$obj->getBilling_item_desc()							
						);
					}
					else
					{
						$data = array(					
							'group_id' 					=> 	$obj->getGroup_id(),
							'category_id' 				=>	$obj->getCategory_id(),
							'property_title' 			=>	$obj->getProperty_title(),
							'property_name' 			=>	$obj->getProperty_name(),
							'property_owner' 			=>	$obj->getProperty_owner(),
							'property_code' 			=>	$obj->getProperty_code(),
							'property_price' 			=>	$obj->getProperty_price(),
							'related_items'				=>	$obj->getRelated_items(),
							'property_type'	 			=>	$obj->getProperty_type(),
							'property_primary_image'	=>	$obj->getProperty_primary_image(),
							'property_image' 			=>	$obj->getProperty_image(),
							'property_desc' 			=>	$obj->getProperty_desc(),
							'property_order'	    	=>	$obj->getProperty_order(),
							'meta_title' 				=>	$obj->getMeta_title(),
							'meta_keywords' 			=>	$obj->getMeta_keywords(),
							'meta_desc' 				=>	$obj->getMeta_desc(),
							'country_id' 				=>	$obj->getCountry_id(),					
							'state_id' 					=>	$obj->getState_id(),
							'area_id' 					=>	$obj->getArea_id(),
							'house_location' 			=>	$obj->getHouse_location(),
							'house_no' 					=>	$obj->getHouse_no(),
							'house_name' 				=>	$obj->getHouse_name(),
							'house_phone' 				=>	$obj->getHouse_phone(),
							'house_fax' 				=>	$obj->getHouse_fax(),
							'house_email' 				=>	$obj->getHouse_email(),
							'post_code' 				=>	$obj->getPost_code(),
							'property_address' 			=>	$obj->getProperty_address(),
							'feature_room_no' 			=>	$obj->getFeature_room_no(),
							'feature_reception' 		=>	$obj->getFeature_reception(),
							'feature_dinning_space' 	=>	$obj->getFeature_dinning_space(),
							'feature_kitchen' 			=>	$obj->getFeature_kitchen(),
							'feature_toilets' 			=>	$obj->getFeature_toilets(),
							'feature_bathroom' 			=>	$obj->getFeature_bathroom(),
							'feature_garden' 			=>	$obj->getFeature_garden(),
							'feature_conservatory' 		=>	$obj->getFeature_conservatory(),
							'feature_garage' 			=>	$obj->getFeature_garage(),
							'feature_parking' 			=>	$obj->getFeature_parking(),
							'feature_glazing' 			=>	$obj->getFeature_glazing(),
							'feature_floor' 			=>	$obj->getFeature_floor(),
							'feature_primary_floor_image'=>	$obj->getFeature_primary_floor_image(),
							'feature_floor_plan_image' 	=>	$obj->getFeature_floor_plan_image(),
							'feature_google_map' 		=>	$obj->getFeature_google_map(),
							'feature_additional' 		=>	$obj->getFeature_additional(),
							'brochure_title' 			=>	$obj->getBrochure_title(),
							'brochure_desc' 			=>	$obj->getBrochure_desc(),
							'availabe_from' 			=>	$obj->getAvailabe_from(),
							'available_to' 				=>	$obj->getAvailable_to(),
							'available_expire_date' 	=>	$obj->getAvailable_expire_date(),
							'available_activation_date' =>	$obj->getAvailable_activation_date(),
							'meter_electric' 			=>	$obj->getMeter_electric(),
							'meter_electric_expire' 	=>	$obj->getMeter_electric_expire(),
							'meter_gas' 				=>	$obj->getMeter_gas(),
							'meter_gas_expire' 			=>	$obj->getMeter_gas_expire(),
							'meter_energy' 				=>	$obj->getMeter_energy(),
							'meter_energy_expire' 		=>	$obj->getMeter_energy_expire(),
							'available_information'		=>	$obj->getAvailable_information(),
							'payment_desc' 				=>	$obj->getPayment_desc(),
							'property_est_payment'		=>	$obj->getProperty_est_payment(),
							'property_size'				=>	$obj->getProperty_size(),
							'property_lot'				=>	$obj->getProperty_lot(),
							'property_build_year'		=>	$obj->getProperty_build_year(),
							'property_estate_surface'	=>	$obj->getProperty_estate_surface(),
							'property_land_surface'		=>	$obj->getProperty_land_surface(),
							'property_tipology'			=>	$obj->getProperty_tipology(),
							'stars'						=>	$obj->getStars(),
							'feature_highlights'		=>	$obj->getFeature_highlights(),
							'feature_bedroom'			=>	$obj->getFeature_bedroom(),
							'feature_balcony'			=>	$obj->getFeature_balcony(),
							'feature_courtyard'			=>	$obj->getFeature_courtyard(),
							'feature_furnishing'		=>	$obj->getFeature_furnishing(),
							'feature_amenities'			=>	$obj->getFeature_amenities(),
							'feature_other_amenities'	=>	$obj->getFeature_other_amenities(),
							'available_added_on'		=>	$obj->getAvailable_added_on(),
							'payment_quote_id'			=>	$obj->getPayment_quote_id(),
							'payment_cycle'				=>	$obj->getPayment_cycle(),
							'payment_loan'				=>	$obj->getPayment_loan(),
							'payment_down'				=>	$obj->getPayment_down(),
							'payment_loan_product'		=>	$obj->getPayment_loan_product(),
							'payment_interest_rate'		=>	$obj->getPayment_interest_rate(),
							'payment_rate_lock'			=>	$obj->getPayment_rate_lock(),
							'payment_due_in'			=>	$obj->getPayment_due_in(),
							'payment_apr'				=>	$obj->getPayment_apr(),
							'payment_loan_discount'		=>	$obj->getPayment_loan_discount(),
							'payment_appraisal_fee'		=>	$obj->getPayment_appraisal_fee(),
							'payment_processing_fee'	=>	$obj->getPayment_processing_fee(),
							'payment_underwriting_fee'	=>	$obj->getPayment_underwriting_fee(),
							'payment_payment_penalty'	=>	$obj->getPayment_payment_penalty(),
							'payment_monthly_interest'	=>	$obj->getPayment_monthly_interest(),
							'payment_monthly_taxes'		=>	$obj->getPayment_monthly_taxes(),
							'payment_monthly_homeowners'=>	$obj->getPayment_monthly_homeowners(),
							'payment_monthly_mortgage'	=>	$obj->getPayment_monthly_mortgage(),
							'other_nearby_school'		=>	$obj->getOther_nearby_school(),
							'other_neighborhood'		=>	$obj->getOther_neighborhood(),
							'other_landmark'			=>	$obj->getOther_landmark(),
							'other_distance_hospital'	=>	$obj->getOther_distance_hospital(),
							'other_distance_airport'	=>	$obj->getOther_distance_airport(),
							'other_distance_railway'	=>	$obj->getOther_distance_railway(),
							'other_distance_school'		=>	$obj->getOther_distance_school(),
							'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
							'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
							'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
							'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
							'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
							'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
							'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
							'billing_item_desc' 						=>	$obj->getBilling_item_desc()						
						);
					}
					// Start the Update process
				
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>