<?php
class Property_View_Helper_SubCategory extends Zend_View_Helper_Abstract 
{
	public static function checkChild($parent,$group_id)
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		$select = $conn->select()
						->from(array('gc' => Zend_Registry::get('dbPrefix').'property_category'), array('gc.category_name'))						
						->where('gc.parent = ?', $parent)
						->where('gc.group_id = ?', $group_id);
		$rs = $select->query()->fetchAll();	
		if($rs)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function getSubCategory($parent,$view,$group_Info,$seperator,&$j) 
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		$translator = Zend_Registry::get('translator');
		
		$selectSub = $conn->select()
						->from(array('gc' => Zend_Registry::get('dbPrefix').'property_category'), array('gc.id','gc.group_id','gc.category_name','gc.category_order','gc.active','gc.cat_date','gc.featured','gc.category_thumb','gc.entry_by'))						
						->where('gc.parent = ?', $parent)
						->order('gc.category_order ASC');
			$rs = $selectSub->query()->fetchAll();			
			if($rs)
			{
				$mem_db = new Members_Model_DbTable_MemberList();
				foreach($rs as $topic_datas)
				{
					$j++;
					if($j % 2 == 0)
					{
						$onmouseout = "this.bgColor='#FFFFFF';";
						$background = '#FFFFFF';
					}
					else
					{
						$onmouseout = "this.bgColor='#F7F7F7';";
						$background = '#F7F7F7';
					}
					
					$rows .= '<tr bgcolor="'.$background.'" onmouseover="this.bgColor=\'#FCF5DD\';" onmouseout="'.$onmouseout.'">'.
								'<td class="center"><input type="checkbox" name="checkbtn" id="checkbtn" value="'.$view->escape($topic_datas['id']).'" class="check_btn" /></td>'.
								'<td class="center">'.
									$view->escape($topic_datas['id']).
								'<td>';
									$member_info = $mem_db->getMemberInfo($view->escape($topic_datas['entry_by']));
									$property_name = $group_Info->getGroupName($view->escape($topic_datas['group_id']));
									$link_url = $view->url(array('module' => 'Property', 'controller' => 'Backendpro', 'action' => 'list', 'group_id' => $view->escape($topic_datas['group_id'])),'adminrout',true);
									$rows .= '<div class="gallery-label">'.$view->escape($property_name['property_name']).'</div><div class="gallery-browse"><a href="'.$link_url.'"><img src="application/modules/Administrator/layouts/scripts/images/tools/browse_item.gif" border="0" class="myTooltip" alt="'.$view->translator->translator('hotels_click_view_product',$view->escape($property_name['property_name'])).'" title="'.$view->translator->translator('property_click_view_product',$view->escape($property_name['property_name'])).'" /></a></div>';
					$rows .=  	'</td>'.
								'<td><span class="thumb">';
								if($view->escape($topic_datas['category_thumb']))
								{
									$rows .= '<img src="data/frontImages/property/category_thumb/'.$view->escape($topic_datas['category_thumb']).'" width="30" height="20" /></span>';
								}
					$rows .=	'</td>'.
					            '<td>'.$seperator.$view->escape($topic_datas['category_name']).'</td>'.
								'<td>'.
									'<input type="text" size="1" class="category_order_text" name="category_order" id="category_order" value="'.$view->escape($topic_datas['category_order']).'" />'.
									'<a href="javascript: void(0);" rel="'.$view->escape($topic_datas['id']).'" class="up_btn"><img src="application/modules/Administrator/layouts/scripts/images/tools/up-arrow.gif"  border="0" /></a>&nbsp;<a href="javascript: void(0);" rel="'.$view->escape($topic_datas['id']).'" class="down_btn"><img src="application/modules/Administrator/layouts/scripts/images/tools/down-arrow.gif" border="0" /></a>'.
								'</td>'.
								'<td class="center" title="<strong>'.$translator->translator('common_date').' : </strong>'.$view->escape($topic_datas['cat_date']).'<br /><strong>'.$translator->translator('common_entry').' : </strong> '.$view->escape($member_info['username']).'">'.
								strftime('%Y-%m-%d',strtotime($view->escape($topic_datas['cat_date']))).
								'</td>'.
								'<td class="center">';								
									  if($view->escape($topic_datas['active']) == '1'){ $publish = 'unpublish'; }else{ $publish = 'publish'; } 
									  $rows .= '<a href="javascript:void(0);" class="publish_btn" rel="'.$view->escape($topic_datas['id']).'_'.$view->escape($topic_datas['category_name']).'_'.$publish.'">';
									  if($view->escape($topic_datas['active']) == '1')
									  { 
										  $rows .= '<img src="application/modules/Administrator/layouts/scripts/images/tools/published.gif" border="0" title="'.$translator->translator('common_unpublish_title').'" alt="'.$translator->translator('common_unpublish_title').'" />'; 
									  }
									  else
									  { 													
										  $rows .= '<img src="application/modules/Administrator/layouts/scripts/images/tools/unpublished.gif" border="0" title="'.$translator->translator('common_publish_title').'" alt="'.$translator->translator('common_publish_title').'" />';
									  } 
									  $rows .= '</a>';									
					$rows .=	'</td>'.
							  	'<td class="tools-list">'.
									'<ul>'.
										'<li>';
											if ($view->allow('edit','backend','Property'))
											{
												$rows .= '<a href="'.$view->url( array('module'=> 'Property', 'controller' => 'backend', 'action'     => 'edit', 'id' => $view->escape($topic_datas['id']), 'group_id' => $view->escape($topic_datas['group_id']) ), 'adminrout',    true).'"><img src="application/modules/Administrator/layouts/scripts/images/tools/edit.png" title="'.$translator->translator('common_edit_title').'" alt="'.$translator->translator('common_edit_title').'" border="0" /></a>';
											}
											$rows .= '</li><li>';
					 						if ($view->allow('delete','backend','Property'))
											{
												$rows .= '<a href="javascript:void(0);" class="delete_btn"  rel="'.$view->escape($topic_datas['id']).'_'.$view->escape($topic_datas['category_name']).'_'.$view->escape($topic_datas['category_thumb']).'"><img src="application/modules/Administrator/layouts/scripts/images/tools/delete.png" title="'.$translator->translator('common_delete_title').'" alt="'.$translator->translator('common_delete_title').'" border="0" /></a>';
											}
											$rows .= '</li>';
											
											$rows .= '<li>';
											if ($view->allow('featured','backend','Property'))
											{
												if($view->escape($topic_datas['featured']) == '1'){ $featured = 'unfeatured'; }else{ $featured = 'featured'; } 
												$rows .= '<a href="javascript:void(0);" class="featured_btn" rel="'.$view->escape($topic_datas['id']).'_'.$view->escape($topic_datas['category_name']).'_'.$featured.'">';
												if($view->escape($topic_datas['featured']) == '1')
												{ 
													$rows .= '<img src="application/modules/Administrator/layouts/scripts/images/tools/featured.gif" border="0" title="'.$translator->translator('common_unfeature_title').'" alt="'.$translator->translator('common_unfeature_title').'" />'; 
												}
												else
												{ 													
													$rows .= '<img src="application/modules/Administrator/layouts/scripts/images/tools/unfeatured.gif" border="0" title="'.$translator->translator('common_feature_title').'" alt="'.$translator->translator('common_feature_title').'" />';
												} 
												$rows .= '</a>';
											}
											$rows .= '</li>';
					$rows .= 			'</ul>'.
								  '</td>'.
							    '</tr>';
					
					if(self::checkChild($topic_datas['id'],$topic_datas['group_id']))
					{
						$rows .= self::getSubCategory($topic_datas['id'],$view,$group_Info,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seperator,$j);
					}					
				}
				return $rows;
			}
	}	
}