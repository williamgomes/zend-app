<?php
class Property_BackendproController extends Zend_Controller_Action
{
	private $ProductForm;
	private $uploadForm;
	private $_controllerCache;	
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {				
        /* Initialize action controller here */
		$this->ProductForm =  new Property_Form_ProductForm ();	
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
    }
	
	public function preDispatch() 
	{
		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');

		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{	
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}	
	}
	
	//PROPERTY LIST FUNCTION

    public function listAction()
    {
		$group_id =	$this->_request->getParam('group_id');
		$this->view->group_id = $group_id;
		$approve = $this->getRequest()->getParam('approve');	
		$this->view->approve = $approve;
		$category_id = $this->getRequest()->getParam('category_id');
		$this->view->category_id = $category_id;
		
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$group_info	=	($group_id) ? $group_db->getGroupName($group_id) : null;
		$this->view->group_info = $group_info;
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				if($group_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_request->getParam('group_id'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}
				if($category_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $this->_request->getParam('category_id'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'group_id'	=> $group_id, 'category_id'	=> $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Property_Model_PropertiesListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['property_price_format'] = $this->view->numbers(number_format($entry_arr['property_price']));
								$entry_arr['feature_room_no_format'] = $this->view->numbers($entry_arr['feature_room_no']);
								$entry_arr['publish_status_property_name'] = str_replace('_', '-', $entry_arr['property_name']);
								$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
								$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
								$img_thumb_arr = explode(',',$entry_arr['property_image']);								
								$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['property_owner']) ? true : false;						
								$entry_arr['property_desc']	=	'';
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Property_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Property_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Property_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());
	}	
	
	public function savedAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Property_Model_SavedPropertiesListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_property_name'] = str_replace('_', '-', $entry_arr['property_name']);
								$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
								$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
								$entry_arr['property_price_format']	= $this->view->numbers($entry_arr['property_price']);
								$entry_arr['feature_room_no_format']	= $this->view->numbers($entry_arr['feature_room_no']);
								$img_thumb_arr = explode(',',$entry_arr['property_image']);								
								$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['property_owner']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
    }	
	
	//PROPERTY FUNCTIONS	
	
	
	/*public function uploadfileAction()
	{
		$theme = Zend_Registry::get('jtheme');
		$this->view->theme = $theme;
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		$group_id = $this->_getParam('group_id', 0);
		$rel = $this->_getParam('rel', 0);
		$file_content = ($this->_getParam('file_content', 0)) ? $this->_getParam('file_content', 0) : null ;
				
		//Put Group Information in the form
		$group_info = new Property_Model_DbTable_PropertyGroup();
		$option = $group_info->getGroupName($group_id);		
		$this->uploadForm =  new Property_Form_UploadForm($option);
		
		switch($rel)
		{
			case 'general_plan':
					$path = 'data/frontImages/property/property_image';
				break;
			case 'floor_plan':
					$path = 'data/frontImages/property/floor_plan_image';
				break;
		}
		
		
		if ($this->_request->getPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			
			if($this->uploadForm->isValid($this->_request->getPost())) 
			{				
				Members_Controller_Helper_FileRename::fileRename($this->uploadForm->upload_file, $path);
				$Filename = $this->uploadForm->upload_file->getFileName();
				$ext = Eicra_File_Utility::GetExtension($Filename);
				$file_obj = new Property_Controller_Helper_File($path,$Filename,$ext,$option);
									
				if($file_obj->checkCategoryThumbExt())
				{				
					if($this->uploadForm->upload_file->receive())
					{
						$msg = $translator->translator('File_upload_success');
						$json_arr = array('status' => 'ok','msg' => $msg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));						
					}
					else
					{
						$validatorMsg = $this->uploadForm->upload_file->getMessages();
						$vMsg = implode("\n", $validatorMsg);	
						$json_arr = array('status' => 'err','msg' => $vMsg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
					}
				}
				else
				{
					$msg = $translator->translator('File_upload_ext_err',$ext);
					$json_arr = array('status' => 'err','msg' => $msg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
				}
			}
			else
			{
				$validatorMsg = $this->uploadForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'err','msg' => $vMsg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))));	
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);						
		}	
		else
		{	
			$this->view->rel		=	$rel;
			$this->view->file_content	=	$file_content;
			$this->view->group_id	=	$group_id;
			$this->view->group_info	=	$option;
			$this->view->upload_path	=	$path;
			$this->view->uploadForm = $this->uploadForm;			
			$this->render();
		}		
	}
	
	public function deletefileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		$this->view->translator	= $translator;
		
		if ($this->_request->isPost()) 
		{
			$file_info = $this->_request->getPost('file_info');
			if(empty($file_info))
			{
				$msg = $translator->translator("insert_selected_file_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			else
			{
				$each_file_arr = explode('; ',$file_info);
				$deleted_file_name = '';
				foreach($each_file_arr as $key => $each_file)
				{
					$file_info_arr = explode(',',$each_file);
					if($file_info_arr[1])
					{				
						$dir = $file_info_arr[0].DS.$file_info_arr[1];
						$res = Eicra_File_Utility::deleteRescursiveDir($dir);
					}
					
					if($res)
					{
						$deleted_file_name .= $file_info_arr[1].', ';
						$msg = $translator->translator("file_delete_success",$deleted_file_name);
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("file_delete_err",$file_info_arr[1]);
						$json_arr = array('status' => 'err','msg' => $msg);
						break;
					}
				}
			}
		}
		else
		{
			$msg = $translator->translator("file_delete_err");
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}	*/
	
	public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Eicra_Model_DbTable_City();			
        	$cities_options = $cities->getOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function propertygroupAction()
	{		
		if ($this->_request->isPost()) 
		{
			try
			{
				$this->_helper->viewRenderer->setNoRender();
				$this->_helper->layout->disableLayout();
				$translator = $this->translator;
				
				$group_id = $this->_request->getPost('grp_id');			
				$parant = $this->_request->getPost('id');
				$expanded = ($this->_request->getPost('expanded')) ? $this->_request->getPost('expanded') : false;
				
				$group_db = new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($group_id);
				
				//$propertyGroup = Property_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,null);
				$businessType = new Property_Model_DbTable_BusinessType();	
				$businessType_options = $businessType->getOptions($group_id);
								
				$dynamic_field_arr = $this->getDynamicfield($group_id);
				$param_fields = array(
									'table_name' => 'property_group', 
									'primary_id_field'	=>	'id', 
									'primary_id_field_value'	=>	$group_info['id'],
									'file_path_field'	=>	'', 
									'file_extension_field'	=>	'file_type', 
									'file_max_size_field'	=>	'file_size_max'
							);
				$treeDataSource = Property_View_Helper_Categorytree::getTreeDataSource($parant,$this->view,$group_id,null,$expanded);
				
				if($treeDataSource)
				{			
					$json_arr = array('status' => 'ok','propertyGroup' => $propertyGroup, 'TreeDataSource' => $treeDataSource,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
				}
				else
				{
					$msg = $translator->translator('property_group_err');	
					$json_arr = array('status' => 'err','msg' => $msg,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
				}	
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	private function getDynamicfield($group_id)
	{
		try
		{
			$translator = Zend_Registry::get('translator');
			$group_info = new Property_Model_DbTable_PropertyGroup();
			$option = $group_info->getGroupName($group_id);
			if($option['dynamic_form'])
			{
				$option['form_id'] = $option['dynamic_form'];
				$this->ProductForm = new Property_Form_ProductForm ($option);
				$groupsObj = $this->ProductForm->getDisplayGroups();				
				if(!empty($groupsObj))
				{
					$dynamic_field_obj = array();	
					$group_key	= 0;			
					foreach($groupsObj as $group)
					{
						$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_TITLE] = $translator->translator($group->getAttrib('title'));
						$elementsObj =  $group->getElements();
						if($elementsObj)
						{
							$element_key = 0;
							foreach($elementsObj as  $element)
							{
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['name']		=	$element->getName();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['value']		=	$element->getValue();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['label']		=	$element->getLabel();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['required']	=	($element->isRequired()) ? true : false;
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['type']		= 	strtolower(str_replace('Zend_Form_Element_', '', $element->getType()));
																
								$elements_attributes = $element->getAttribs();
								if($elements_attributes)
								{
									foreach($elements_attributes as $attributes_key => $attribute_value)
									{
										$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key][$attributes_key]		=	$attribute_value;
									}
								}
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['element']		=		$element->render();
								$element_key++;								
							}
						}	
						$group_key++;					
					}
				}
				$dynamic_field_arr = array('status' => 'ok', 'dynamic_fields' => $dynamic_field_obj, 'form_info' => $this->ProductForm->getFormInfo());
			}
			else
			{
				$dynamic_field_arr = array('status' => 'err', 'dynamic_fields' => null);
			}
		}
		catch(Exception $e)
		{
			$dynamic_field_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'dynamic_fields' => null);
		}
		return $dynamic_field_arr;
	}
	
	public function addAction()
	{			
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->view->translator	= $translator;
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			try
			{
				//Checking Membership Package Option
				$packages = new Members_Controller_Helper_Packages();	
				if($packages->isPackage())
				{
					$module = $this->_request->getModuleName();
					$controller = $this->_request->getControllerName();
					$action = $this->_request->getActionName();
					if($packages->isPackageField($module,$controller,$action))
					{
						$package_no = $packages->getPackageFieldValue($module,$controller,$action);				
					}
					else
					{
						$package_no = 0;
					}
				}
				else
				{
					$package_no = 0;
				}		
				$property_db = new Property_Model_DbTable_Properties();
				$p_num = $property_db->numOfProperty();
				if(empty($package_no) || ($p_num < $package_no))
				{
					$group_id = $this->_request->getPost('group_id');
					if(!empty($group_id))
					{						
						$group_info = new Property_Model_DbTable_PropertyGroup();
						$option = $group_info->getGroupName($group_id);
						if($option['dynamic_form'])
						{
							$option['form_id'] = $option['dynamic_form'];
							$this->ProductForm = new Property_Form_ProductForm ($option);
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($option['form_id']); 
							foreach($field_groups as $group)
							{						
								$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
								$elements = $displaGroup->getElements();
								foreach($elements as $element)
								{
									$group_arr[$element->getName()] = $element->getName();
									if($element->getType() == 'Zend_Form_Element_File')
									{
										$element_name = $element->getName();
										$required = ($element->isRequired()) ? true : false;
										$element_info = $element;
										$this->ProductForm->removeElement($element_name);
										$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
									}
									$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group, 'legend' => $group->field_group));
								}
							}				
						}
					}
					
					if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
					}
					
					if($this->ProductForm->isValid($this->_request->getPost())) 
					{	
						$properties = new Property_Model_Properties($this->ProductForm->getValues());
						$auth = Zend_Auth::getInstance ();
						$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';	
						$properties->setActive($stat);
						$properties->setFeatured('0');	
						$properties->setProperty_primary_image($this->_request->getPost('property_primary_image'));
						$properties->setFeature_primary_floor_image($this->_request->getPost('feature_primary_floor_image'));
						$properties->setRelated_items($this->_request->getPost('related_items'));	
						$properties->setFeature_amenities($this->_request->getPost('feature_amenities'));
						$properties->setFeature_other_amenities($this->_request->getPost('feature_other_amenities'));										
						
						$perm = new Property_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $properties->saveProperties();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{
									$result2 = $properties->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}						
						}
						else
						{
							$Msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $Msg);
						}
					
					}
					else
					{
						$validatorMsg = $this->ProductForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg);
					}	
				}
				else
				{
					$msg = $translator->translator("property_add_limit_err",$package_no);
					$json_arr = array('status' => 'err','msg' => $msg);
				}					
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{				
			$this->view->proForm = $this->ProductForm;			
			$this->render();
		}	
	}	
	
	public function editAction()
	{
		$id = $this->_getParam('id', 0);
		$translator = Zend_Registry::get('translator');
		
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		//Get Properties Info
		$propertiesData = new Property_Model_DbTable_Properties();		
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$id = $this->_request->getPost('id');
			if(!empty($id))
			{
				$propertiesInfo = $propertiesData->getPropertiesInfo($id);
				$group_info = new Property_Model_DbTable_PropertyGroup();
				$option = $group_info->getGroupName($propertiesInfo['group_id']);
				if($option['dynamic_form'])
				{
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Property_Form_ProductForm ($option);
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($option['form_id']); 
					foreach($field_groups as $group)
					{
						$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
						$elements = $displaGroup->getElements();
						foreach($elements as $element)
						{
							$group_arr[$element->getName()] = $element->getName();
							if($element->getType() == 'Zend_Form_Element_File')
							{
								$element_name = $element->getName();
								$required = ($element->isRequired()) ? true : false;
								$element_info = $element;
								$this->ProductForm->removeElement($element_name);
								$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
							}
							$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group));
						}
					}
				}
			}					
			
			
			try
			{
				if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
				}
				
				if($this->ProductForm->isValid($this->_request->getPost())) 
				{					
					if(!empty($id))
					{					
						$properties = new Property_Model_Properties($this->ProductForm->getValues());
						$properties->setPrev_category($propertiesInfo['category_id']);
						$properties->setPrev_group($propertiesInfo['group_id']);
						$properties->setId($id);
						$properties->setProperty_primary_image($this->_request->getPost('property_primary_image'));
						$properties->setFeature_primary_floor_image($this->_request->getPost('feature_primary_floor_image'));
						$properties->setProperty_title($this->ProductForm->getValue('property_title'),$id);		
						$properties->setRelated_items($this->_request->getPost('related_items'));	
						$properties->setAvailable_information($id, $this->_request->getPost('property_price'));		
						$properties->setFeature_amenities($this->_request->getPost('feature_amenities'));
						$properties->setFeature_other_amenities($this->_request->getPost('feature_other_amenities'));					
						
						$perm = new Property_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $properties->saveProperties();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{
									$result2 = $properties->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}						
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}						
						}
						else
						{
							$msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $msg);
						}					
					}
					else
					{
						$msg = $translator->translator("page_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				
				}
				else
				{
					$validatorMsg = $this->ProductForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);		
		}
		else
		{	
			//Get Properties Info 
			$propertiesInfo = $propertiesData->getPropertiesInfo($id);	
			if($propertiesInfo)
			{
				//Put Group Information in the view
				$group_info = new Property_Model_DbTable_PropertyGroup();
				$option = $group_info->getGroupName($propertiesInfo['group_id']);
				
				//Dynamic Form
				if(!empty($option['dynamic_form']))
				{   
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Property_Form_ProductForm ($option);	
						$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
					$propertiesInfo	=	$dynamic_valu_db->getFieldsValueInfo($propertiesInfo,$propertiesInfo['id']);					
				}
				
						
				//Get Category Info
				$categoryData = new Property_Model_DbTable_Category();	
				$group_id = $propertiesInfo['group_id'];
				
				
				if($propertiesInfo['category_id'] == '0')
				{
					$selected = 'style="background-color:#D7D7D7"';
					$category_name = $translator->translator("property_tree_root");
				}
				else
				{
					$categoryInfo = $categoryData->getCategoryInfo($propertiesInfo['category_id']);
					$category_name = $categoryInfo['category_name'];	
					$selected = '';
				}		
				
				
				//ASSIGN CATEGORY TREE
				$this->view->categoryTree = '<table class="example" id="dnd-example">'.
											'<tbody ><tr id="node-0">'.
											 '<td '.$selected.'>'.
												'<span class="folder">'.$translator->translator("property_tree_root").'</span>'.
											'</td>'.
										'</tr>'.Property_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,$propertiesInfo['category_id']).'</tbody></table>';
				
				//ASSIGN PROPERTY TYPE
				$r = true;			
				if(is_numeric($propertiesInfo['property_owner']))
				{ 
					if(is_int((int)$propertiesInfo['property_owner']))
					{
						$r = false;
					}				
				}
				if($r){ $this->ProductForm->property_owner->addMultiOption($propertiesInfo['property_owner'],stripslashes($propertiesInfo['property_owner'])); }
				
				//ASSIGN PROPERTY TYPE
				$propertyBusinessType = new Property_Model_DbTable_BusinessType();			
				$propertyBusinessType_options = $propertyBusinessType->getSelectOptions($propertiesInfo['group_id']);
				$this->ProductForm->property_type->addMultiOptions($propertyBusinessType_options);
				
				//ASSIGN STATE
				$states = new Eicra_Model_DbTable_State();			
				$states_options = $states->getOptions($propertiesInfo['country_id']);			
				$this->ProductForm->state_id->addMultiOptions($states_options);
				
				//ASSIGN AREA / CITY
				$areas = new Eicra_Model_DbTable_City();			
				$areas_options = $areas->getOptions($propertiesInfo['state_id']);			
				$this->ProductForm->area_id->addMultiOptions($areas_options);
				
				//ASSIGN FEATURE AMENITIES LIST
				$this->ProductForm->feature_amenities->setIsArray(true); 
				$feature_amenities_arr = explode(',',$propertiesInfo['feature_amenities']);	
				$propertiesInfo['feature_amenities'] = 	$feature_amenities_arr;	
				
				//ASSIGN FEATURE OTHER AMENITIES LIST
				$this->ProductForm->feature_other_amenities->setIsArray(true); 
				$other_feature_arr = explode(',',$propertiesInfo['feature_other_amenities']);	
				$propertiesInfo['feature_other_amenities'] = 	$other_feature_arr;
				
				//ASSIGN EMPTY DATE
				$propertiesInfo['availabe_from'] 			= ($propertiesInfo['availabe_from'] 			== '0000-00-00')? '' : $propertiesInfo['availabe_from'];
				$propertiesInfo['available_to'] 			= ($propertiesInfo['available_to'] 				== '0000-00-00')? '' : $propertiesInfo['available_to'];
				$propertiesInfo['available_expire_date'] 	= ($propertiesInfo['available_expire_date'] 	== '0000-00-00')? '' : $propertiesInfo['available_expire_date'];
				$propertiesInfo['available_activation_date']= ($propertiesInfo['available_activation_date'] == '0000-00-00')? '' : $propertiesInfo['available_activation_date'];
				$propertiesInfo['meter_electric_expire'] 	= ($propertiesInfo['meter_electric_expire'] 	== '0000-00-00')? '' : $propertiesInfo['meter_electric_expire'];
				$propertiesInfo['meter_gas_expire'] 		= ($propertiesInfo['meter_gas_expire'] 			== '0000-00-00')? '' : $propertiesInfo['meter_gas_expire'];
				$propertiesInfo['meter_energy_expire'] 		= ($propertiesInfo['meter_energy_expire'] 		== '0000-00-00')? '' : $propertiesInfo['meter_energy_expire'];
				$propertiesInfo['available_added_on'] 		= ($propertiesInfo['available_added_on'] 		== '0000-00-00')? '' : $propertiesInfo['available_added_on'];
								
						
				$this->view->option =  $option;		
				$this->view->propertiesInfo = $propertiesInfo;				
				$this->ProductForm->populate($propertiesInfo);
				$this->ProductForm->related_items->setIsArray(true); 				
				$related_items_arr = explode(',',$propertiesInfo['related_items']);
				$this->ProductForm->related_items->setValue($related_items_arr);						
				$this->view->id = $id;
				$this->view->entry_by = $propertiesInfo['entry_by'];
				$this->view->category_name = $category_name;
				$this->view->property_primary_image = $propertiesInfo['property_primary_image'];
				$this->view->feature_primary_floor_image = $propertiesInfo['feature_primary_floor_image'];			
				$this->view->proForm = $this->ProductForm;	
				$this->view->form_info = $this->ProductForm->getFormInfo ();			
				$this->dynamicUploaderSettings($this->view->form_info);	
				$this->userUploaderSettings($propertiesInfo);
				$this->render();
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}	
	}	
	
	private function userUploaderSettings($info)
	{
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$group_info = $group_db->getGroupName($info['group_id']);
		$param_fields = array(
								'table_name' => 'property_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_property_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));	
		
		/**************************************************For Secondary**************************************************/	
			$secondary_requested_data	=	$requested_data;
			$secondary_requested_data['file_path_field'] = 'file_path_feature_floor_plan_image';			
			
			$this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));		
			$this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info));
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$perm = new Property_View_Helper_Allow();			
			if ($perm->allow('delete','backendpro','Property'))
			{
				$id = $this->_request->getPost('id');			
				$group_id = $this->_request->getPost('group_id');
				
				//page_info
				$proObj = new Property_Model_DbTable_Properties();
				$page_info = $proObj->getPropertiesInfo($id);
				
				//Put Group Information in the view
				$group_db = new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($page_info['group_id']);
				
				$property_name = $page_info['property_name'];
				$property_image = explode(',',$page_info['property_image']);
				$feature_floor_plan_image = explode(',',$page_info['feature_floor_plan_image']);			
				
				$property_image_path = 'data/frontImages/property/property_image';
					
				$floor_plan_image_path = 'data/frontImages/property/floor_plan_image';
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Remove from Properties
				try
				{
					if(!empty($group_info['dynamic_form']))
					{
						//Delete Files From Folder
						$form_db = new Members_Model_DbTable_Forms(); 
						$fields_db	= new Members_Model_DbTable_Fields(); 
						$field_value_db = new Members_Model_DbTable_FieldsValue();
						
						$form_info = $form_db->getFormsInfo($group_info['dynamic_form']); 						
						$fields_infos = $fields_db->getFieldsInfo($group_info['dynamic_form']);		
						
						
						foreach($fields_infos as $fields)
						{
							if(trim($fields->field_type)	==	'file')
							{
								$field_value_arr = $field_value_db->getFieldsValue($id,$group_info['dynamic_form'],$fields->id);
								$field_value_arr = $field_value_arr->toArray();								
								if(!empty($field_value_arr[0]['field_value']))
								{	
									$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
									foreach($element_value_arr as $key => $e_value)
									{
										if(!empty($e_value))
										{	
											$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
											$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
										}
									}
								}								
							}
						}
						
						$whereD = array();
						$whereD[] = 'form_id = '.$conn->quote($group_info['dynamic_form']);
						$whereD[] = 'table_id = '.$conn->quote($id);
						$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereD);
					}
					
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					
					$conn->delete(Zend_Registry::get('dbPrefix').'property_page', $where);
					
					foreach($property_image as $key=>$file)
					{
						if($file)
						{
							$dir = $property_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('page_list_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('page_list_file_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
					
					foreach($feature_floor_plan_image as $key=>$file)
					{
						if($file)
						{
							$dir = $floor_plan_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('page_list_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('page_list_file_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('page_list_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deletesavedAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$perm = new Property_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendpro','Property'))
			{
				$id = $this->_request->getPost('id');	
									
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Remove from Properties
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'property_saved', $where);
					$msg = 	$translator->translator('property_list_saved_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('property_list_saved_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg.' '.$e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('property_list_saved_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Property_View_Helper_Allow();			
			if ($perm->allow('delete','backendpro','Property'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					//Delete Files From Folder
					$form_db = new Members_Model_DbTable_Forms(); 
					$fields_db	= new Members_Model_DbTable_Fields(); 
					$field_value_db = new Members_Model_DbTable_FieldsValue();
					
					foreach($id_arr as $id)
					{						
						//Get Category Thumbnil
						$select = $conn->select()
									->from(array('gc' => Zend_Registry::get('dbPrefix').'property_page'), array('gc.property_image','gc.feature_floor_plan_image','gc.group_id'))
									->where('gc.id = ?',$id);
						$rs = $select->query()->fetchAll();
						if($rs)
						{				
							foreach($rs as $row)
							{
								$property_image = explode(',',$row['property_image']);
								$feature_floor_plan_image = explode(',',$row['feature_floor_plan_image']);
								$group_id	=	$row['group_id'];
							}
						}
												
						
						$property_image_path = 'data/frontImages/property/property_image';
							
						$floor_plan_image_path = 'data/frontImages/property/floor_plan_image';
						
						//Put Group Information in the view
						$group_db = new Property_Model_DbTable_PropertyGroup();
						$group_info = $group_db->getGroupName($group_id);	
						
						//Remove from Category						
						try
						{
							if(!empty($group_info['dynamic_form']))
							{
								$form_info = $form_db->getFormsInfo($group_info['dynamic_form']); 						
								$fields_infos = $fields_db->getFieldsInfo($group_info['dynamic_form']);		
								
								
								foreach($fields_infos as $fields)
								{
									if(trim($fields->field_type)	==	'file')
									{
										$field_value_arr = $field_value_db->getFieldsValue($id,$group_info['dynamic_form'],$fields->id);
										$field_value_arr = $field_value_arr->toArray();	
										if(!empty($field_value_arr[0]['field_value']))
										{	
											$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
											foreach($element_value_arr as $key => $e_value)
											{
												if(!empty($e_value))
												{	
													$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
													$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
												}
											}
										}
									}
								}
								
								$whereD = array();
								$whereD[] = 'form_id = '.$conn->quote($group_info['dynamic_form']);
								$whereD[] = 'table_id = '.$conn->quote($id);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereD);
							}
							
							$where = array();
							$where[] = 'id = '.$conn->quote($id);
							
							$conn->delete(Zend_Registry::get('dbPrefix').'property_page', $where);
							foreach($property_image as $key=>$file)
							{
								if($file)
								{
									$dir = $property_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								}
								if($res)
								{
									$msg = 	$translator->translator('page_list_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('page_list_file_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}					
							
							foreach($feature_floor_plan_image as $key=>$file)
							{
								if($file)
								{
									$dir = $floor_plan_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
								}
								if($res)
								{
									$msg = 	$translator->translator('page_list_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('page_list_file_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
						}
						catch (Exception $e) 
						{
							$msg = 	$translator->translator('category_list_delete_err');			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deletesavedallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Property_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendpro','Property'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					foreach($id_arr as $id)
					{	
						// Remove from Category
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'property_saved', $where);
							$msg = 	$translator->translator('page_list_delete_success');			
							$json_arr = array('status' => 'ok','msg' => $msg);	
						}
						catch (Exception $e) 
						{
							$msg = 	$translator->translator('category_list_delete_err');			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function isnot_int($property_owner)
	{
		$r = true;			
		if(is_numeric($property_owner))
		{ 
			if(is_int((int)$property_owner))
			{
				$r = false;
			}				
		}
		return $r;
	}
	
	public function publishAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id = $this->_request->getPost('id');
			$property_name = $this->_request->getPost('property_name');
			$paction = $this->_request->getPost('paction');
			$data = array();
			
			$property_db = new Property_Model_DbTable_Properties();
			$property_info = $property_db->getPropertiesInfo($id);
			
			if(!$this->isnot_int($property_info['property_owner']))
			{
				$mem_db	=	new	Members_Model_DbTable_MemberList();
				$role_db	=	new	Administrator_Model_DbTable_Roles();
				$mem_info	=	$mem_db->getMemberInfo($property_info['property_owner']);
				$role_info  =	$role_db->getInfo($mem_info['role_id']);
				$to_mail	=	($role_info['allow_to_send_email'] == '1') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					$letter_type = 'activation';
					$data = array('active' => $active,'activation_never' => '0', 'property_name' => $property_info['property_name']);
					$datas = array('available_activation_date' => $property_info['available_activation_date'], 'available_expire_date' => $property_info['available_expire_date'], 'property_name' => $property_info['property_name']);
					break;
				case 'unpublish':
					$active = '0';
					$letter_type = 'deactivation';
					$activation_never = $this->_request->getPost('activation_never');
					$available_activation_date = $this->_request->getPost('available_activation_date');
					$available_expire_date = $this->_request->getPost('available_expire_date');
					$data = array('active' => $active, 'available_activation_date' => $available_activation_date, 'available_expire_date' => $available_expire_date, 'activation_never' => $activation_never, 'property_name' => $property_info['property_name']);
					$available_activation_date = ($activation_never == '1') ? $translator->translator('common_never') : $available_activation_date;
					$datas = array('available_activation_date' => $available_activation_date, 'available_expire_date' => $available_expire_date, 'property_name' => $property_info['property_name']);
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Properties status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'property_page',$data, $where);
				$email_obj = new Property_Controller_Helper_Emails();
				$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('page_list_publish_err',$property_name);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function publishallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
				
		if ($this->_request->isPost()) 
		{	
			$id_str  = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{	
						$conn->update(Zend_Registry::get('dbPrefix').'property_page',array('active' => $active,'activation_never' => '0'), $where);
						$return = true;
					}
					catch (Exception $e) 
					{
						$return = false;
					}
				}
			
				if($return)
				{			
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('page_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = $translator->translator("page_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
			
	public function featuredAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$property_name = $this->_request->getPost('property_name');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'featured':
					$feature = '1';
					break;
				case 'unfeatured':
					$feature = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
			 	$conn->update(Zend_Registry::get('dbPrefix').'property_page',array('featured' => $feature), $where);
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','featured' => $feature);
			}
			else
			{
				$msg = $translator->translator('page_list_feature_err',$property_name);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function upAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				
		$id 	= $this->_request->getPost('id');
		$page_order 	= $this->_request->getPost('page_order');				
				
		$OrderObj = new Property_Controller_Helper_PropertiesOrders($id);
		$returnV =	$OrderObj->decreaseOrder();
		
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function downAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
		
		$id = $this->_request->getPost('id');
		$page_order = $this->_request->getPost('page_order');
						
		$OrderObj = new Property_Controller_Helper_PropertiesOrders($id);
		$returnV =	$OrderObj->increaseOrder();
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function orderallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_str = $this->_request->getPost('id_arr');
			$property_order_str = $this->_request->getPost('property_order_arr');			
			
			$id_arr = explode(',',$id_str);
			$property_order_arr = explode(',',$property_order_str);
			
			$order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
			$checkOrder = $order_numbers->checkOrderNumbers($property_order_arr);
			if( $checkOrder['status'] == 'err')
			{
				$json_arr = array('status' => 'err','msg' => $checkOrder['msg']);
			}
			else
			{
				//Save Category Order
				$i = 0;
				foreach($id_arr as $id)
				{
					$OrderObj = new Property_Controller_Helper_PropertiesOrders($id);
					$OrderObj->saveOrder($property_order_arr[$i]);
					$i++;
				}
				$msg = $translator->translator("page_zero_order_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}			
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function searchAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Property_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("property_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Property_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("property_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function loadAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			$property_db = new Property_Model_DbTable_Properties();
			$property_info = $property_db->getPropertiesInfo($id);
			$res_value = $property_info['available_information'];			
		}
		$this->_response->setBody($res_value);
	}
	
	public function updateAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			if ($this->_request->isPost()) 
			{
				$data = $this->_request->getPost('dop_booking_calendar');
				$data_arr = explode(',',$data);
				$price_arr = explode(';;',$data_arr[0]);
				$price	 = $price_arr[2];
				
				$property_db = new Property_Model_DbTable_Properties();
				$property_db->updateAvailableInformation($id, $data);
				$property_db->updatePrice($id, $price);	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			}
		}
	}		
}

