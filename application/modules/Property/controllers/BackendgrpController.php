<?php

class Property_BackendgrpController extends Zend_Controller_Action
{	
	private $propertyGroupForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {		
        /* Initialize action controller here */		
		$this->propertyGroupForm =  new Property_Form_PropertyGroupForm ();
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
    }
	
	public function preDispatch() 
	{
		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);		
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listgrpAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');	
				$request_from	=	$this->_request->getParam('request_from');			
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				$backup 		= 	Eicra_Global_Variable::getSession()->viewPageNum;			
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Property_Model_PropertyGroupListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['group']=  array('id' => $entry_arr['id'], 'group_name' => $entry_arr['group_name']);
								$entry_arr['category_num_format']=  $this->view->numbers($entry_arr['category_num']);
								$entry_arr['property_num_format']=  $this->view->numbers($entry_arr['property_num']);
								$entry_arr['business_type_num_format']=  $this->view->numbers($entry_arr['business_type_num']);
								$entry_arr['publish_status_group_name'] = str_replace('_', '-', $entry_arr['group_name']);	
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				if($request_from == 'widget') {    Eicra_Global_Variable::getSession()->viewPageNum =  $backup;    }				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}				
    }
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function addgrpAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->propertyGroupForm->isValid($this->_request->getPost())) 
			{	
				$propertyGroup = new Property_Model_Groups($this->propertyGroupForm->getValues());
				$propertyGroup->setActive('1');
				$propertyGroup->setFile_type($this->_request->getPost('file_type'));
				
				$perm = new Property_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $propertyGroup->savePropertyGroup();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("property_group_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("property_group_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("property_group_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->propertyGroupForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{			
			$this->view->propertyGroupForm = $this->propertyGroupForm;			
			$this->render();
		}	
	}
	
	public function editgrpAction()
	{	
		$id = $this->_getParam('id', 0);	
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->propertyGroupForm->isValid($this->_request->getPost())) 
			{	
				$id = $this->_request->getPost('id');
				$propertyGroup = new Property_Model_Groups($this->propertyGroupForm->getValues());
				$propertyGroup->setId($id);	
				$propertyGroup->setFile_type($this->_request->getPost('file_type'));							
				
				$perm = new Property_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $propertyGroup->savePropertyGroup();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("property_group_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("property_group_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("property_group_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}
			
			}
			else
			{
				$validatorMsg = $this->propertyGroupForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			$Group = new Property_Model_DbTable_PropertyGroup();
			$GroupData = $Group->getGroupName($id);
			if($GroupData)
			{
				$this->view->id = $id;	
				$this->propertyGroupForm->populate($GroupData);
				
				$this->propertyGroupForm->file_type->setIsArray(true); 				
				$file_type_arr = explode(',',$GroupData['file_type']);
				$this->propertyGroupForm->file_type->setValue($file_type_arr);
							
				$this->view->propertyGroupForm = $this->propertyGroupForm;			
				$this->render();
			}
			else
			{
				$this->_helper->redirector('addgrp', $this->view->getController, $this->view->getModule, array());
			}
		}	
	}
	
	public function publishgrpAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$property_name = $this->_request->getPost('property_name');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'property_group',array('active' => $active), $where);
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
				$e_err = $e->getMessage();
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('property_list_publish_err',$property_name);	
				$json_arr = array('status' => 'err','msg' => $msg." ".$e_err);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
			
	}
	
	public function publishallgrpAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_str = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{
						$conn->update(Zend_Registry::get('dbPrefix').'property_group',array('active' => $active), $where);
						$return = true;
					}
					catch (Exception $e) 
					{
						$return = false;
						$e_err = $e->getMessage();
					}
				}
			
				if($return)
				{			
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('property_group_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg." ".$e_err);
				}
			}
			else
			{
				$msg = $translator->translator("property_group_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
			
	}
	
	public function deletegrpAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id = $this->_request->getPost('id');
			$property_name = $this->_request->getPost('property_name');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			$check_num_cat = new Property_View_Helper_PropertyGroup();
					
			if($check_num_cat->getNumOfProperty($id) == '0' && $check_num_cat->getNumOfProduct($id) == '0' && $check_num_cat->getNumOfPropertyType($id) == '0')
			{
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'property_group', $where);
					$msg = 	$translator->translator('property_group_delete_success',$property_name);			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('property_list_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('property_group_delete_err',$property_name);			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('property_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallgrpAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
					
				$check_num_cat = new Property_View_Helper_PropertyGroup();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{			
					if($check_num_cat->getNumOfProperty($id) == '0' && $check_num_cat->getNumOfProduct($id) == '0' && $check_num_cat->getNumOfPropertyType($id) == '0')
					{
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'property_group', $where);
							$msg = 	$translator->translator('property_group_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e) 
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('property_group_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('property_group_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("property_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('property_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function cronsetupAction()
	{
		
	}
		
}

