<?php
//Members Frontend controller
class Property_RegionlistController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	
    public function init()
    {
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');		
		$this->view->assign('translator',Zend_Registry::get('translator'));	
		$this->view->setEscape('stripslashes');	
			
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);		
	}
	
	public function preDispatch() 
	{	
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');	
				
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;			
				
	}
	
	public function viewcityAction()
	{
		$pageNumber = $this->getRequest()->getParam('page');
		$city = $this->getRequest()->getParam('city');
		$city_id = $this->getRequest()->getParam('city_id');
		$this->view->city =  $city;			
		$selectGP = $this->_DBconn->select()
									->from(array('hp' => Zend_Registry::get('dbPrefix').'property_page'), array('*'))
									->where('hp.active = ?', '1')
									->where('hp.area_id = ?', $city_id)
									->order('hp.id DESC');
									
		$news_info =  $selectGP->query()->fetchAll();	
		$this->view->user_name =  $user_name;						
		if(count($news_info) > 0)
		{
			$groupObj = new Property_Model_DbTable_PropertyGroup();
			$groupInfo = $groupObj->getGroupName('1');
			
			$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 			
			
			if($groupInfo)
			{
				$viewPageNum = $groupInfo['file_num_per_page'];
			}
			else
			{
				$viewPageNum = '10';
			}			
			
			$paginator = Zend_Paginator::factory($news_info);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->view_datas =  $paginator;
			$this->view->group_datas = $groupInfo;
		}
		else
		{
			$this->view->msg = 'NO data found';
		}
	}	
	
	public function viewstateAction()
	{
		$pageNumber = $this->getRequest()->getParam('page');
		$state = $this->getRequest()->getParam('state');
		$state_id = $this->getRequest()->getParam('state_id');
		$this->view->state =  $state;			
		$selectGP = $this->_DBconn->select()
									->from(array('hp' => Zend_Registry::get('dbPrefix').'property_page'), array('*'))
									->where('hp.active = ?', '1')
									->where('hp.state_id = ?', $state_id)
									->order('hp.id DESC');
									
		$news_info =  $selectGP->query()->fetchAll();	
		$this->view->user_name =  $user_name;						
		if(count($news_info) > 0)
		{
			$groupObj = new Property_Model_DbTable_PropertyGroup();
			$groupInfo = $groupObj->getGroupName('1');
			
			$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 	
						
			if($groupInfo)
			{
				$viewPageNum = $groupInfo['file_num_per_page'];
			}
			else
			{
				$viewPageNum = '10';
			}			
			
			$paginator = Zend_Paginator::factory($news_info);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->view_datas =  $paginator;
			$this->view->group_datas = $groupInfo;
		}
		else
		{
			$this->view->msg = 'NO data found';
		}
	}	
}