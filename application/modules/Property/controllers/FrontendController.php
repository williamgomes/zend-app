<?php
class Property_FrontendController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_controllerCache;
	private $_translator;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;			
		
		if($this->_request->getParam('menu_id'))
		{
			$menu_db = new Menu_Model_DbTable_Menu();
			$this->view->menu_info = $menu_db->getMenuByTitle($this->_request->getParam('menu_id'));
							
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}		
	}
	
	public function groupAction()
    {			
		$posted_data	=	$this->_request->getParams();		
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('group_id') : $this->_page_id;
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$group_db = new Property_Model_DbTable_PropertyGroup();
			$group_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';	
			$group_info = $group_db->getListInfo('1', $group_search_params, false);			
			
			if($group_info)
			{
				$group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
				$this->view->assign('group_info', $group_info[0] );
				$posted_data['sort'][] = array('field' => $group_info[0]['file_sort'],  'dir' => $group_info[0]['file_order']);				
			}
		}
		
		if($group_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
						
					$getViewPageNum = $this->_request->getParam('pageSize'); 
					$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'All-Property-List/*';
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);					
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
						$property_type_arr = explode(',', $this->_translator->translator('property_front_page_rent_id'));
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
						$review_helper = new Review_View_Helper_Review();
									
						$list_mapper = new Property_Model_PropertiesListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 25, true);
									$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
									$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
									$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
									$img_thumb_arr = explode(',',$entry_arr['property_image']);	
									$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
									$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
									
									$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));
														
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$list_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
										
									}
									$entry_arr['list_stars_format']		= 	$list_stars;
									$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
											
									$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;														
									
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}					
		}					
	}
	
	public function viewAction()
    {	
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('type') : $this->_page_id;
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'property_type', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$business_type_db = new Property_Model_DbTable_BusinessType();
			
			$business_type_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$business_type_search_params['filter']['logic'] = ($business_type_search_params['filter']['logic']) ? $business_type_search_params['filter']['logic'] : 'and';	
			$business_type_info = $business_type_db->getListInfo(null, $business_type_search_params, false);
			
			if($business_type_info)
			{
				$business_type_info = (!is_array($business_type_info)) ? $business_type_info->toArray() : $business_type_info;
				$this->view->assign('business_type_info', $business_type_info[0] );
				$posted_data['sort'][] = array('field' => $business_type_info[0]['file_sort'],  'dir' => $business_type_info[0]['file_order']);
			}
		}
		
		if($business_type_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
						
					$getViewPageNum = $this->_request->getParam('pageSize'); 
					$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Property-List-Type/*';
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'type' => $this->_request->getParam('type'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);					
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
						$property_type_arr = explode(',', $this->_translator->translator('property_front_page_rent_id'));
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
						$review_helper = new Review_View_Helper_Review();
									
						$list_mapper = new Property_Model_PropertiesListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 25, true);
									$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
									$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
									$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
									$img_thumb_arr = explode(',',$entry_arr['property_image']);	
									$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
									$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
										
									$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));
														
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$list_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
										
									}
									$entry_arr['list_stars_format']		= 	$list_stars;
									$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
									$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;
																									
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}					
		}				
	}
	
	public function categoryAction()
    {
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$this->_page_id = ($this->_page_id) ? $this->_page_id : $this->_request->getParam('category_id');
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$category_db = new Property_Model_DbTable_Category();
			
			$category_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$category_search_params['filter']['logic'] = ($category_search_params['filter']['logic']) ? $category_search_params['filter']['logic'] : 'and';	
			$category_info = $category_db->getListInfo('1', $category_search_params, false);
			
			if($category_info)
			{
				$category_info = (!is_array($category_info)) ? $category_info->toArray() : $category_info;
				$this->view->assign('category_info', $category_info[0] );
				$posted_data['sort'][] = array('field' => $category_info[0]['cat_sort'],  'dir' => $category_info[0]['cat_order']);
			}
		}
		
		if($category_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
						
					$getViewPageNum = $this->_request->getParam('pageSize'); 
					$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Property-Category-List/*';
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'category_id' => $this->_request->getParam('category_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);					
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
						$property_type_arr = explode(',', $this->_translator->translator('property_front_page_rent_id'));
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
						$review_helper = new Review_View_Helper_Review();
									
						$list_mapper = new Property_Model_PropertiesListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 25, true);
									$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
									$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
									$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
									$img_thumb_arr = explode(',',$entry_arr['property_image']);	
									$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
									$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
									
									$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));	
														
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$list_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
										
									}
									$entry_arr['list_stars_format']		= 	$list_stars;
									$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
									$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;
																									
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}					
		}
	}
	
	public function detailsAction()
    {		
		$this->view->module = $this->_request->getModuleName();
		$this->view->controller = $this->_request->getControllerName();
		$this->view->action = $this->_request->getActionName();	 
		$this->view->ProductForm   = new Property_Form_ProductForm();
			
		if (!empty($this->_page_id)) 
		{
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details';
			if( ($property_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$property_db = new Property_Model_DbTable_Properties();
				//$property_info = $property_db->getPropertiesInfo($this->_page_id);				
				
				$search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
				$property_info_obj = $property_db->getListInfo(null, $search_params,  array('property_page' => array( 'pp.*' ), 'userChecking' => false)); 
				if($property_info_obj)
				{
					$property_info_arr = ($property_info_obj) ? $property_info_obj->toArray() : null;
					$property_info = $property_info_arr[0];
					$property_info = (is_array($property_info)) ? array_map('stripslashes', $property_info) : stripslashes($property_info);
				}
				$this->_controllerCache->save($property_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($property_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Property_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($property_info['property_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$property_info	=	$dynamic_value_db->getFieldsValueInfo($property_info,$property_info['id']);										
			}
			$propertyForm = new Property_Form_ProductForm ($group_info);
			$propertyForm->populate($property_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$property_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
				
			$this->view->propertyForm = $propertyForm;
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $property_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo; 
			$this->view->tab	= 0;
		}
		else if($this->_request->getParam('property_title'))
		{
			$property_title = $this->_request->getParam('property_title');
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details_'.preg_replace('/[^a-zA-Z0-9_]/','_',$property_title);
			if( ($property_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$property_db = new Property_Model_DbTable_Properties();
				$search_params['filter']['filters'][0] = array('field' => 'property_title', 'operator' => 'eq', 'value' => $property_title);
				$property_info_obj = $property_db->getListInfo(null, $search_params,  array('property_page' => array( 'pp.*' ), 'userChecking' => false)); 
				if($property_info_obj)
				{
					$property_info_arr = ($property_info_obj) ? $property_info_obj->toArray() : null;
					$property_info = $property_info_arr[0];
					$property_info = (is_array($property_info)) ? array_map('stripslashes', $property_info) : stripslashes($property_info);
				}
				$this->_controllerCache->save($property_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($property_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Property_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($property_info['property_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$property_info	=	$dynamic_value_db->getFieldsValueInfo($property_info,$property_info['id']);										
			}
			$propertyForm = new Property_Form_ProductForm ($group_info);
			$propertyForm->populate($property_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$property_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
				
			$this->view->propertyForm = $propertyForm;			
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $property_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo;
			$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
		}
	}
	
	public function printAction()
    {	
		$this->_helper->layout->disableLayout();	
		$this->view->module = $this->_request->getModuleName();
		$this->view->controller = $this->_request->getControllerName();
		$this->view->action = $this->_request->getActionName();	    
			
		if (!empty($this->_page_id)) 
		{
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_print';
			if( ($property_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$property_db = new Property_Model_DbTable_Properties();
				$property_info = $property_db->getPropertiesInfo($this->_page_id);
				$this->_controllerCache->save($property_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($property_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Property_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($property_info['property_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}			
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$property_info	=	$dynamic_value_db->getFieldsValueInfo($property_info,$property_info['id']);										
			}
			$propertyForm = new Property_Form_ProductForm ($group_info);
			$propertyForm->populate($property_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$property_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $property_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo; 
			$this->view->tab	= 0;
			$this->view->propertyForm = $propertyForm;
			$this->view->currency = $this->getCurrency();			
			$this->view->property_db = new Property_Model_DbTable_Properties();
		}
		else if($this->_request->getParam('property_title'))
		{
			$property_title = $this->_request->getParam('property_title');
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details_'.preg_replace('/[^a-zA-Z0-9_]/','_',$property_title);
			if( ($property_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$property_db = new Property_Model_DbTable_Properties();
				$property_title_info = $property_db->getTitleToId($property_title);			
				$property_id = $property_title_info[0]['id'];
				$property_info = $property_db->getPropertiesInfo($property_id);
				$this->_controllerCache->save($property_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($property_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Property_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($property_info['property_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			$this->view->view_datas = $property_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo;
			$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
						
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$property_info	=	$dynamic_value_db->getFieldsValueInfo($property_info,$property_info['id']);										
			}
			$propertyForm = new Property_Form_ProductForm ($group_info);
			$propertyForm->populate($property_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$property_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}	
			
			$this->view->group_datas = $group_info;
			$this->view->propertyForm = $propertyForm;
			$this->view->currency = $this->getCurrency();			
			$this->view->property_db = new Property_Model_DbTable_Properties();
		}
	}
	
	//For Ajax Save Property
	public function saveAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$user_id = $globalIdentity->user_id;
				$property_id = $this->_request->getPost('id');
				try
				{
					$clause    = $this->_DBconn->quoteInto('user_id = ?', $user_id);
					$validator_saved_property = new Zend_Validate_Db_RecordExists(
								array(
									'table' 	=> Zend_Registry::get('dbPrefix').'property_saved',
									'field' 	=> 'property_id',
									'exclude' 	=> $clause
								)
							);
					if(!$validator_saved_property->isValid($property_id))
					{
						//Update category_id of the product of this category to zero
						$data = array(					
							'user_id' 		=> 	$user_id,
							'property_id' 	=>	$property_id										
						);
						try
						{
							$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'property_saved',$data);
							$msg = $translator->translator("property_recorded_success");
							$json_arr = array('status' => 'ok','msg' =>  $msg);
						}
						catch(Exception $e)
						{
							$msg = $translator->translator("property_recorded_err");
							$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage());
						}				
					}
					else
					{								
						$msg = $translator->translator("property_already_saved_err");
						$json_arr = array('status' => 'err','msg' =>  $msg);
					}				
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err','msg' =>  $e->getMessage());
				}				
			}
			else
			{
				$msg = $translator->translator("common_login_err");
				$json_arr = array('status' => 'err','msg' =>  $msg);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
		
	//For Ajax Search
	public function searchAction()
	{
		if($this->_request->getPost('search_type') != 'post')
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
		}
		
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			
			$search_obj = new Property_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("property_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			if($this->_request->getPost('search_type') == 'post')
			{
				$group_db =  new Property_Model_DbTable_PropertyGroup();
				$group_info = $group_db->getGroupName($this->_request->getParam('group_id'));
				$this->view->group_datas = $group_info;
				$this->view->post_datas = $this->_request->getPost();
				if($result['status'] == 'ok')
				{					 
					 $this->view->view_datas = $result['result_data'];
				}
				else
				{
					$this->view->view_datas = null;
				}
			}
			else
			{
				//Convert To JSON ARRAY	
				$res_value = Zend_Json_Encoder::encode($json_arr);	
				$this->_response->setBody($res_value);
			}
		}
	}
	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_field[$i]	= 'active';
			$postData_arr['active'] = 1;
			$search_obj = new Property_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("property_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Eicra_Model_DbTable_City();			
        	$cities_options = $cities->getOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function cityAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			try
			{
				$city = $this->_request->getPost('city');
				$limit = $this->_request->getPost('limit');
				$order = $this->_request->getPost('order');
				$order_sort = $this->_request->getPost('order_sort');
				
				$cities = new Property_Model_DbTable_Area();			
				$cities_options = $cities->searchArea($city, $limit, $order, $order_sort);
				if($cities_options)
				{
					$cities = array();
					$i = 0;
					foreach($cities_options as $key=>$value)
					{
						$cities[$i] = array('city_id' => $value['city_id'],'city' => $this->view->escape($value['city']), 'city_value' => $this->view->escape($value['city']).'~('.$value['city_id'].')');
						$i++;	
					}
					$json_arr = array('status' => 'ok','msg' => '','search_data' => $cities );				
				}
				else
				{
					$msg = $translator->translator("common_area_found_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}	
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}		
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}
	
	public function loadAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			$property_db = new Property_Model_DbTable_Properties();
			$property_info = $property_db->getPropertiesInfo($id);
			$res_value = $property_info['available_information'];			
		}
		$this->_response->setBody($res_value);
	}
	
	public function itinerarylistAction()
	{		
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Frontend-Login';
		Eicra_Global_Variable::getSession()->returnLink = $this->view->url();	
		Eicra_Global_Variable::checkSession($this->_response,$url);
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$this->view->currency = $this->getCurrency();
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$status = $this->getRequest()->getParam('status');	
				
				$posted_data['filter']['filters'][] = array('field' => 'module_name', 'operator' => 'eq', 'value' => $this->view->getModule);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';			
									
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Property-Itinerary-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'status' => $status, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Invoice_Model_InvoicesListMapper();
					$invoice_items_db = new Invoice_Model_DbTable_InvoiceItems();
					$property_db = new Property_Model_DbTable_Properties();				
					$list_datas =  $list_mapper->fetchAll($pageNumber, $status, $posted_data);
					
					$view_datas = array('data_result' => array(), 'total' => 0);	
							
					if($list_datas)
					{						
						$key = 0;	
						$status_class = new Invoice_Controller_Helper_Status($this->_translator);			
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ?  $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_invoice'] = str_replace('_', '-', $entry_arr['id']);	
								unset($entry_arr['invoice_desc']); 
								$entry_arr['invoice_create_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['invoice_create_date']));
								$entry_arr['invoice_status_format']	=	$status_class->getStatus($entry_arr['status']);
								$entry_arr['invoice_now_paid_status_format']	=	$status_class->getNowPaidStatus();
								$entry_arr['invoice_now_unpaid_status_format']	=	$status_class->getNowUnPaidStatus();							
								$entry_arr['total_amount_format'] = $this->view->numbers(number_format($this->view->escape($entry_arr['total_amount']), 2, '.', ""));
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;						
								
								//Invoice Items Start
								$invoice_items_info = $invoice_items_db->getInvoiceItems($entry_arr['id']);
								if($invoice_items_info)
								{
									foreach($invoice_items_info as $invoice_items_key => $invoice_items)
									{
										$object_value_decoded = Zend_Json_Decoder::decode($invoice_items['object_value']);
										$entry_arr['invoice_items'][$invoice_items_key]['item_id'] 												= 	$invoice_items['id'];
										$entry_arr['invoice_items'][$invoice_items_key][Eicra_File_Constants::VACATION_CHECK_IN] 				= 	$object_value_decoded[Eicra_File_Constants::VACATION_CHECK_IN];
										$entry_arr['invoice_items'][$invoice_items_key][Eicra_File_Constants::VACATION_CHECK_OUT] 				= 	$object_value_decoded[Eicra_File_Constants::VACATION_CHECK_OUT];
										$entry_arr['invoice_items'][$invoice_items_key]['property_id'] 											= 	$object_value_decoded['property_id'];
										$entry_arr['invoice_items'][$invoice_items_key]['property_info']										=	$property_db->getPropertiesInfo($object_value_decoded['property_id']);	
										$entry_arr['invoice_items'][$invoice_items_key]['total_night']											=	$this->getNights($entry_arr['invoice_items'][$invoice_items_key][Eicra_File_Constants::VACATION_CHECK_IN], $entry_arr['invoice_items'][$invoice_items_key][Eicra_File_Constants::VACATION_CHECK_OUT]);				
									}
								}
								//Invoice Items End
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);				
		}
	}	
	
	public function ownersAction()
	{
		if ($this->_request->isPost()) 
		{
			$role_id = $this->_request->getPost('role_id');
			$city	 = $this->_request->getPost('city');
			$city_arr = explode('~', $city);
			$city_arr[1] = (!empty($city_arr[1])) ? str_replace('(', '',str_replace(')', '', $city_arr[1])) : '';
			
			if(empty($city) || (strtolower($city ) == 'any'))
			{
				$select = $this->_DBconn->select()
									->from(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), array('up.user_id','title' => 'up.title', 'firstName' => 'up.firstName', 'lastName' => 'up.lastName', 'companyName' => 'up.companyName', 'address' => 'up.address', 'phone' => 'up.phone', 'mobile' => 'up.mobile', 'fax' => 'up.fax'))
									->where('up.role_id = ?', $role_id)
									->where('up.status = ?', '1')
									->group('up.user_id')
									->joinLeft(array('pp' => Zend_Registry::get('dbPrefix').'property_page'), 'pp.property_owner = up.user_id', array('property_num' => 'COUNT(pp.id)'))
									->joinLeft(array('rol' => Zend_Registry::get('dbPrefix').'roles'), 'up.role_id = rol.role_id', array('role_name' => 'rol.role_name', 'role_title' => 'rol.role_title', 'form_id' => 'rol.form_id'))
									->joinLeft(array('frm' => Zend_Registry::get('dbPrefix').'forms'), 'rol.form_id = frm.id', array('attach_file_path' => 'frm.attach_file_path'));
			}
			else
			{
				if(!empty($city_arr[1]))
				{
					$select = $this->_DBconn->select()
											->from(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), array('up.user_id','title' => 'up.title', 'firstName' => 'up.firstName', 'lastName' => 'up.lastName', 'companyName' => 'up.companyName', 'address' => 'up.address', 'phone' => 'up.phone', 'mobile' => 'up.mobile', 'fax' => 'up.fax'))
											->where('up.role_id = ?', $role_id)
											->where('up.status = ?', '1')
											->group('up.user_id')
											->joinLeft(array('pp' => Zend_Registry::get('dbPrefix').'property_page'), 'pp.property_owner = up.user_id', array('property_num' => 'COUNT(pp.id)'))
											->joinLeft(array('rol' => Zend_Registry::get('dbPrefix').'roles'), 'up.role_id = rol.role_id', array('role_name' => 'rol.role_name', 'role_title' => 'rol.role_title', 'form_id' => 'rol.form_id'))
											->joinLeft(array('frm' => Zend_Registry::get('dbPrefix').'forms'), 'rol.form_id = frm.id', array('attach_file_path' => 'frm.attach_file_path'))
											->where('pp.area_id = ?', $city_arr[1]);
				}
				else
				{
					$select = $this->_DBconn->select()
										->from(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), array('up.user_id','title' => 'up.title', 'firstName' => 'up.firstName', 'lastName' => 'up.lastName', 'companyName' => 'up.companyName', 'address' => 'up.address', 'phone' => 'up.phone', 'mobile' => 'up.mobile', 'fax' => 'up.fax'))
										->where('up.role_id = ?', $role_id)
										->where('up.status = ?', '1')
										->group('up.user_id')
										->joinLeft(array('pp' => Zend_Registry::get('dbPrefix').'property_page'), 'pp.property_owner = up.user_id', array('property_num' => 'COUNT(pp.id)'))
										->joinLeft(array('rol' => Zend_Registry::get('dbPrefix').'roles'), 'up.role_id = rol.role_id', array('role_name' => 'rol.role_name', 'role_title' => 'rol.role_title', 'form_id' => 'rol.form_id'))
										->joinLeft(array('frm' => Zend_Registry::get('dbPrefix').'forms'), 'rol.form_id = frm.id', array('attach_file_path' => 'frm.attach_file_path'))
										->where('pp.house_location LIKE "'.$city_arr[0].'%"');								
				}
			}			
			$view_datas =  $select->query()->fetchAll();
			$this->view->assign('view_datas', $view_datas);	
			$this->view->assign('role_id', $role_id);	
			$this->view->assign('city', $city);							
		}
	}
	
	public function agentsAction()
    {
		$this->_page_id = ($this->_page_id) ? $this->_page_id : $this->_request->getParam('property_owner');							
		if (!empty($this->_page_id)) 
		{			
			$viewPageNum = $this->_translator->translator('property_front_page_agent_property_list_per_page'); 
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$select = $this->_DBconn->select()
									->from(array('pp' => Zend_Registry::get('dbPrefix').'property_page'), array('*'))
									->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'pp.property_owner = up.user_id', array('username' => 'up.username','title' => 'up.title', 'firstName' => 'up.firstName', 'lastName' => 'up.lastName', 'companyName' => 'up.companyName', 'address' => 'up.address', 'phone' => 'up.phone', 'mobile' => 'up.mobile', 'fax' => 'up.fax'))
									->joinLeft(array('pg' => Zend_Registry::get('dbPrefix').'property_group'), 'pp.group_id = pg.id', array('group_name' => 'pg.property_name', 'review_id' => 'pg.review_id', 'file_num_per_page' => 'pg.file_num_per_page', 'file_col_num' => 'pg.file_col_num', 'file_sort' => 'pg.file_sort', 'file_order' => 'pg.file_order', 'file_thumb_width' => 'pg.file_thumb_width', 'file_thumb_height' => 'pg.file_thumb_height', 'file_thumb_resize_func' => 'pg.file_thumb_resize_func'))
									->joinLeft(array('pbt' => Zend_Registry::get('dbPrefix').'property_business_type'), 'pp.property_type = pbt.id', array('business_type' => 'pbt.business_type'))
									->joinLeft(array('pc' => Zend_Registry::get('dbPrefix').'property_category'), 'pp.category_id = pc.id', array('category_name' => 'pc.category_name', 'category_title' => 'pc.category_title'))
									->joinLeft(array('cu' => Zend_Registry::get('dbPrefix').'countries'), 'pp.country_id = cu.id', array('value' => 'cu.value'))
									->joinLeft(array('ct' => Zend_Registry::get('dbPrefix').'cities'), 'pp.area_id = ct.city_id', array('city' => 'ct.city'))
									->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'pp.state_id = st.state_id', array('state_name' => 'st.state_name'))
									->where('pp.property_owner = ?', $this->_page_id)
									->orwhere('pp.entry_by = ?', $this->_page_id)
									->where('pp.active = ?', '1');
			
				$view_datas =  $select->query()->fetchAll();
				$this->_controllerCache->save($view_datas, $uniq_id);
			}			
			
			$pageNumber = $this->_request->getParam('page');				
			$paginator = Zend_Paginator::factory($view_datas);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);		
			
			$this->view->view_datas = $paginator;		
		}						
	}
	
	private function property_truncate($phrase,$start_words, $max_words, $char = false, $charset = 'UTF-8')
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				  $dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				  $phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
				}
		}
		else
		{
		   $phrase_array = explode(' ',$phrase);
		   if(count($phrase_array) > $max_words && $max_words > 0)
			  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
	   return $phrase;
	}
	
	public function categoriesAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->_translator;
		if ($this->_request->isPost())
		{
			$group_id = $this->_request->getPost('id');
			$category_db = new Property_Model_DbTable_Category();
        	$category_options = $category_db->getOptions($group_id);
			if($category_options)
			{
				$categories = array();				
				foreach($category_options as $key => $options)
				{
					$categories[$key] = array('id' => $options['id'],'category_name' => $this->view->escape($options['category_name']));
				}
				$json_arr = array('status' => 'ok','msg' => '','categories' => $categories);
			}
			else
			{
				$msg = $translator->translator("common_category_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);
			$this->_response->setBody($res_value);
		}
	}
	
	private function getNights($sStartDate, $sEndDate)
	{		
		$sStartDate =strtotime($sStartDate);
		$sEndDate = strtotime($sEndDate);
		
		$datediff = $sEndDate - $sStartDate;
		$aDays	= floor($datediff/(60*60*24));
		
		return $aDays;
	}
}
