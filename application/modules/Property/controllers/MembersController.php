<?php
//Members Frontend controller
class Property_MembersController extends Zend_Controller_Action
{
	private $_page_id;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	private $ProductForm;
	
    public function init()
    {	
		$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
		Eicra_Global_Variable::checkSession($this->_response,$url);
		Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();
		
		/* Initialize Template */
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout(true));
		$this->translator =	Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->translator);	
	
        /* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();			
		
		
		$this->view->menu_id = $this->_request->getParam('menu_id');
		$getAction = $this->_request->getActionName();
		
			
		if($this->_request->getParam('menu_id'))
		{			
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
			
    }
	
	public function preDispatch() 
	{	
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;
		$this->view->setEscape('stripslashes');					
	}
	
	
	public function listAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_property_list_header_title'), $this->view->url()));
		
		$group_id =	$this->_request->getParam('group');
		$this->view->group_id = $group_id;
		$approve = $this->getRequest()->getParam('approve');	
		$this->view->approve = $approve;
		$category_id = $this->getRequest()->getParam('category');
		$this->view->category_id = $category_id;
		
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$group_info	=	($group_id) ? $group_db->getGroupName($group_id) : null;
		$this->view->group_info = $group_info;
		
		$group_list	=	$group_db->getAllGroupInfo();
		$this->view->group_list = $group_list;
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				if($group_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $group_id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}
				if($category_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $category_id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
					
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontendRoute = 'Members-Property-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group' => $group_id, 'category' => $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRoute,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{		
					$list_mapper = new Property_Model_PropertiesListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);								
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['property_price_format'] = $this->view->numbers($entry_arr['property_price']);
								$entry_arr['feature_room_no_format'] = $this->view->numbers($entry_arr['feature_room_no']);
								$entry_arr['property_size_format'] = $this->view->numbers($entry_arr['property_size']);
								$entry_arr['publish_status_property_name'] = str_replace('_', '-', $entry_arr['property_name']);
								$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
								$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
								$img_thumb_arr = explode(',',$entry_arr['property_image']);								
								$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['property_owner']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Property_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Property_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Property_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());
	}
	
	public function addAction()
	{		
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_property_list_header_title'), 'Members-Property-List'), array($this->translator->translator('property_frontend_list_add_page_name'), $this->view->url()));
		$this->ProductForm = new Property_Form_ProductForm ();
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();			
			
			try
			{
				//Checking Membership Package Option
				$packages = new Members_Controller_Helper_Packages();	
				if($packages->isPackage())
				{
					$module = $this->_request->getModuleName();
					$controller = 'backendpro';
					$action = $this->_request->getActionName();
					if($packages->isPackageField($module,$controller,$action))
					{
						$package_no = $packages->getPackageFieldValue($module,$controller,$action);				
					}
					else
					{
						$package_no = 0;
					}
				}
				else
				{
					$package_no = 0;
				}	
				
				$property_db = new Property_Model_DbTable_Properties();
				$p_num = $property_db->numOfProperty();
				if(empty($package_no) || ($p_num < $package_no))
				{
					$group_id = $this->_request->getPost('group_id');
					if(!empty($group_id))
					{						
						$group_info = new Property_Model_DbTable_PropertyGroup();
						$option = $group_info->getGroupName($group_id);
						if($option['dynamic_form'])
						{
							$option['form_id'] = $option['dynamic_form'];
							$this->ProductForm = new Property_Form_ProductForm ($option);
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($option['form_id']); 
							foreach($field_groups as $group)
							{						
								$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
								$elements = $displaGroup->getElements();
								foreach($elements as $element)
								{
									$group_arr[$element->getName()] = $element->getName();
									if($element->getType() == 'Zend_Form_Element_File')
									{
										$element_name = $element->getName();
										$required = ($element->isRequired()) ? true : false;
										$element_info = $element;
										$this->ProductForm->removeElement($element_name);
										$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
									}
									$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group, 'legend' => $group->field_group));
								}
							}				
						}
					}
					if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
					}
					if($this->ProductForm->isValid($this->_request->getPost())) 
					{												
						$property = new Property_Model_Properties($this->ProductForm->getValues());
						$auth = $this->view->auth;
						$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';	
						$property->setActive($stat); 
						$property->setFeatured('0');	
						$property->setProperty_primary_image($this->_request->getPost('property_primary_image'));
						$property->setFeature_primary_floor_image($this->_request->getPost('feature_primary_floor_image'));						
						$property->setRelated_items($this->_request->getPost('related_items'));										
						
						if($this->view->allow())
						{
							$result = $property->saveProperties();
							if($result['status'] == 'ok')
							{
								$msg = $this->translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{									
									$result2 = $property->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $this->translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $this->translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}
							}
							else
							{
								$msg = $this->translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}												
						}
						else
						{
							$Msg =  $this->translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $Msg);
						}									
					}
					else
					{
						$validatorMsg = $this->ProductForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg);
					}	
				}
				else
				{
					$msg = $this->translator->translator("property_add_limit_err",$package_no);
					$json_arr = array('status' => 'err','msg' => $msg);
				}					
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}		
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{				
			$this->view->proForm = $this->ProductForm;
		}	
	}
	
	public function editAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_property_list_header_title'), 'Members-Property-List'), array($this->translator->translator('property_frontend_list_edit_page_name'), $this->view->url()));
		
		$id = $this->_getParam('id', 0);
		$translator = $this->translator;
				
		//Get Property Info
		$this->ProductForm = new Property_Form_ProductForm ();	
		$propertyData = new Property_Model_DbTable_Properties();

		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();

			$id = $this->_request->getPost('id');
			if(!empty($id))
			{
				$propertyInfo = $propertyData->getPropertiesInfo($id);
				$group_info = new Property_Model_DbTable_PropertyGroup();
				$option = $group_info->getGroupName($propertyInfo['group_id']);
				if($option['dynamic_form'])
				{
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Property_Form_ProductForm ($option);
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($option['form_id']);
							
					if ($field_groups)
					{

						foreach($field_groups as $group)
							{
								
								$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
								$elements = $displaGroup->getElements();
								foreach($elements as $element)
								{
									$group_arr[$element->getName()] = $element->getName();
									if($element->getType() == 'Zend_Form_Element_File')
									{
										$element_name = $element->getName();
										$required = ($element->isRequired()) ? true : false;
										$element_info = $element;
										$this->ProductForm->removeElement($element_name);
										$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
									}
									$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group));
								}
							}
					}
				}
			}

			try
			{
				if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
				}
				if($this->ProductForm->isValid($this->_request->getPost()))
				{
					if(!empty($id))
					{
						$property = new Property_Model_Properties($this->ProductForm->getValues());
						$property->setPrev_category($propertyInfo['category_id']);
						$property->setPrev_group($propertyInfo['group_id']);
						$property->setId($id);
						$property->setProperty_primary_image($this->_request->getPost('property_primary_image'));
						$property->setFeature_primary_floor_image($this->_request->getPost('feature_primary_floor_image'));
						$property->setProperty_title($this->ProductForm->getValue('property_title'),$id);
						$property->setRelated_items($this->_request->getPost('related_items'));
											
						$perm = new Property_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $property->saveProperties();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg, 'obj' => $this->ProductForm->getValues());
								if($option['dynamic_form'])
								{
									$result2 = $property->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg, 'obj' => $this->ProductForm->getValues());
									}
									else
									{
										$msg = $translator->translator("page_save_err1");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}
								}
							}
							else
							{
								$msg = $translator->translator("page_save_err2");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}
						}
						else
						{
							$msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator("page_save_err3");
						$json_arr = array('status' => 'err','msg' => $msg);
					}

				}
				else
				{
					$validatorMsg = $this->ProductForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);
			$this->_response->setBody($res_value);
		}
		else
		{	
			//Get Property Info	
			$propertyInfo = $propertyData->getPropertiesInfo($id);
			if($propertyInfo)
			{
				//Put Group Information in the view
				$group_info = new Property_Model_DbTable_PropertyGroup();
				$option = $group_info->getGroupName($propertyInfo['group_id']);
	
				//Dynamic Form
				if(!empty($option['dynamic_form']))
				{
					$option['form_id'] = $option['dynamic_form'];
					
					$this->ProductForm = new Property_Form_ProductForm ($option);
					
						$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();
					$propertyInfo	=	$dynamic_valu_db->getFieldsValueInfo($propertyInfo,$propertyInfo['id']);
				}
	
	
				//Get Category Info
				$categoryData = new Property_Model_DbTable_Category();
				$group_id = $propertyInfo['group_id'];
	
	
				if($propertyInfo['category_id'] == '0')
				{
					$selected = 'style="background-color:#D7D7D7"';
					$category_name = $translator->translator("property_tree_root");
				}
				else
				{
					$categoryInfo = $categoryData->getCategoryInfo($propertyInfo['category_id']);
					$category_name = $categoryInfo['category_name'];
					$selected = '';
				}
	
	
				//ASSIGN CATEGORY TREE
				$this->view->categoryTree = '<table class="example" id="dnd-example">'.
											'<tbody ><tr id="node-0">'.
											 '<td '.$selected.'>'.
												'<span class="folder">'.$translator->translator("property_tree_root").'</span>'.
											'</td>'.
										'</tr>'.Property_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,$propertyInfo['category_id']).'</tbody></table>';
	
				//ASSIGN PROPERTY TYPE
				$r = true;
				if(is_numeric($propertyInfo['property_owner']))
				{
					if(is_int((int)$propertyInfo['property_owner']))
					{
						$r = false;
					}
				}
				if($r){ $this->ProductForm->property_owner->addMultiOption($propertyInfo['property_owner'],stripslashes($propertyInfo['property_owner'])); }
	
				//ASSIGN PROPERTY TYPE
				$propertyBusinessType = new Property_Model_DbTable_BusinessType();
				$propertyBusinessType_options = $propertyBusinessType->getSelectOptions($propertyInfo['group_id']);
				$this->ProductForm->property_type->addMultiOptions($propertyBusinessType_options);
	
				//ASSIGN STATE
				$states = new Eicra_Model_DbTable_State();
				$states_options = $states->getOptions($propertyInfo['country_id']);
				$this->ProductForm->state_id->addMultiOptions($states_options);
	
				//ASSIGN AREA / CITY
				$areas = new Eicra_Model_DbTable_City();
				$areas_options = $areas->getOptions($propertyInfo['state_id']);
				$this->ProductForm->area_id->addMultiOptions($areas_options);
	
				//ASSIGN EMPTY DATE
				$propertyInfo['availabe_from'] 				= ($propertyInfo['availabe_from'] 				== '0000-00-00')? '' : $propertyInfo['availabe_from'];
				$propertyInfo['available_to'] 				= ($propertyInfo['available_to'] 				== '0000-00-00')? '' : $propertyInfo['available_to'];
				$propertyInfo['available_expire_date'] 		= ($propertyInfo['available_expire_date'] 		== '0000-00-00')? '' : $propertyInfo['available_expire_date'];
				$propertyInfo['available_activation_date']	= ($propertyInfo['available_activation_date'] 	== '0000-00-00')? '' : $propertyInfo['available_activation_date'];
				$propertyInfo['meter_electric_expire'] 		= ($propertyInfo['meter_electric_expire'] 		== '0000-00-00')? '' : $propertyInfo['meter_electric_expire'];
				$propertyInfo['meter_gas_expire'] 			= ($propertyInfo['meter_gas_expire'] 			== '0000-00-00')? '' : $propertyInfo['meter_gas_expire'];
				$propertyInfo['meter_energy_expire'] 		= ($propertyInfo['meter_energy_expire'] 		== '0000-00-00')? '' : $propertyInfo['meter_energy_expire'];
				$propertyInfo['available_added_on'] 		= ($propertyInfo['available_added_on'] 			== '0000-00-00')? '' : $propertyInfo['available_added_on'];
	
	
				//ASSIGN FACILITIES LIST
				$this->ProductForm->feature_amenities->setIsArray(true); 
				$feature_amenities_arr = explode(',',$propertyInfo['feature_amenities']);	
				$propertyInfo['feature_amenities'] = 	$feature_amenities_arr;	
				
				//ASSIGN OTHER FACILITIES LIST
				$this->ProductForm->feature_other_amenities->setIsArray(true); 
				$other_feature_arr = explode(',',$propertyInfo['feature_other_amenities']);	
				$propertyInfo['feature_other_amenities'] = 	$other_feature_arr;
									
				
				$this->view->option =  $option;
				$this->view->propertyInfo = $propertyInfo;
				$this->ProductForm->populate($propertyInfo);
				$this->ProductForm->related_items->setIsArray(true);
				$related_items_arr = explode(',',$propertyInfo['related_items']);
				$this->ProductForm->related_items->setValue($related_items_arr);
				$this->view->id = $id;
				$this->view->entry_by = $propertyInfo['entry_by'];
				$this->view->category_name = $category_name;
				$this->view->property_primary_image = $propertyInfo['property_primary_image'];
				$this->view->feature_primary_floor_image = $propertyInfo['feature_primary_floor_image'];
				$this->view->proForm = $this->ProductForm;
				$this->view->form_info = $this->ProductForm->getFormInfo ();
				$this->dynamicUploaderSettings($this->view->form_info);	
				$this->userUploaderSettings($propertyInfo);
				$this->render();
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Property-Add', array());
			} 
		}	
			
    }
	
	private function userUploaderSettings($info)
	{
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$group_info = $group_db->getGroupName($info['group_id']);
		$param_fields = array(
								'table_name' => 'property_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_property_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));	
		
		/**************************************************For Secondary**************************************************/	
			$secondary_requested_data	=	$requested_data;
			$secondary_requested_data['file_path_field'] = 'file_path_feature_others_exclusive_images';			
			
			$this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));		
			$this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info));
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);		
	}
	
	public function favoriteAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_property_favorite_list_header_title'), $this->view->url()));
		
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Favorite-Property-List/*',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Property_Model_SavedPropertiesListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_property_name'] = str_replace('_', '-', $entry_arr['property_name']);
								$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
								$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
								$entry_arr['property_price_format']	= $this->view->numbers($entry_arr['property_price']);
								$entry_arr['feature_room_no_format']	= $this->view->numbers($entry_arr['feature_room_no']);
								$entry_arr['property_size_format'] = $this->view->numbers($entry_arr['property_size']);
								$img_thumb_arr = explode(',',$entry_arr['property_image']);								
								$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['property_owner']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
    }			
}