<?php
class Property_BookController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;	
	private $_translator;
	private $_controllerCache;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');	
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
				
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();						
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');

		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;			
	
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = 1;
		}	
			
	}
	
	public function bookingAction()
	{
		$property_title = $this->_request->getParam('property_title');
		$property_db = new Property_Model_DbTable_Properties();
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$view_datas = $property_db->getTitleToId($property_title);
		$group_datas = $group_db->getGroupName($view_datas[0]['group_id']);
		
		$post_search_info = Eicra_Global_Variable::getSession()->hotel_search_info;	
		if ($this->_request->isPost()) 
		{
			$postValues = $this->_request->getPost();
			$post_search_info = $postValues ;
			Eicra_Global_Variable::getSession()->hotel_search_info = $post_search_info;
			$this->view->assign('postValues', $postValues);				
		}
		if($post_search_info)
		{
			$check_in_date = ($post_search_info[Eicra_File_Constants::PROPERTY_CHECK_IN]) ? $post_search_info[Eicra_File_Constants::PROPERTY_CHECK_IN] : '';
			$check_out_date = ($post_search_info[Eicra_File_Constants::PROPERTY_CHECK_OUT]) ? $post_search_info[Eicra_File_Constants::PROPERTY_CHECK_OUT] : '';
		}
		else
		{
			$check_in_date = '';
			$check_out_date = '';
		}			
		$this->view->assign('post_search_info', $post_search_info);		
		$this->view->assign('view_datas', $view_datas);	
		$this->view->assign('group_datas', $group_datas);	
		$this->view->assign('property_title', $property_title);
		$this->view->assign('property_Form', new Property_Form_ProductForm());	
		$this->view->assign('currency', $this->getCurrency ());		
	}	
	
	
	public function invoiceAction()
	{		
		Eicra_Global_Variable::getSession()->postValues  =	($this->_request->isPost() && $this->_request->getPost()) ? $this->_request->getPost() : Eicra_Global_Variable::getSession()->postValues;
		Eicra_Global_Variable::getSession()->returnLink = $this->view->url();	
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Frontend-Login';		
		
		if($this->_request->isPost())
		{
			$postValues = $this->_request->getPost();	
		}
		else if(Eicra_Global_Variable::getSession()->postValues)
		{
			$postValues = Eicra_Global_Variable::getSession()->postValues;		
		}
		
		if(Eicra_Global_Variable::getSession()->cart_result)
		{
			$cart_result = Eicra_Global_Variable::getSession()->cart_result;
			$next_count = count($cart_result);
			if($postValues != '' && $this->_request->isPost())
			{
				$cart_result[$next_count] = $postValues;
			}
		}
		else
		{
			if($postValues != '' && $this->_request->isPost())
			{
				$cart_result[0] = $postValues;
			}
			else
			{
				$cart_result = null;
			}
		}
		Eicra_Global_Variable::getSession()->cart_result = $cart_result;
		
		if(!empty($cart_result))
		{			
			$invoice_arr = $this->generateInvoice($cart_result);
			if($invoice_arr['status'] == 'ok')
			{
				Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
			}
		}		
		$this->view->assign('invoice_arr', $invoice_arr);
	}
	
	public function confirmAction()
	{	
		$cart_result = Eicra_Global_Variable::getSession()->cart_result;
		if(!empty($cart_result))
		{
			$invoice_arr = $this->generateInvoice($cart_result);
			if($invoice_arr['status'] == 'ok')
			{
				Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
			}
		}
		$this->view->assign('invoice_arr', $invoice_arr);		
		$this->assignLogin();
		$this->view->logindetails = new Members_Form_LoginForm();	
		$this->dynamicUploaderSettings($this->view->form_info);		
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	private function assignLogin()
	{
		$template_id_field	=	'role_id';
		$settings_db = new Invoice_Model_DbTable_Setting();
		$template_info = $settings_db->getInfoByModule('Property');
		if($template_info)
		{			
			$selected_role_id = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator('property_invoice_register_role_id');									
		}
		else
		{					
			$selected_role_id =  $this->_translator->translator('property_invoice_register_role_id');
		}
		
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
		{
			$registrationForm =  new Members_Form_UserForm ($role_info);
			if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
			$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';
			
			$this->view->selected_role_id	=	$selected_role_id;					
			$this->view->registrationForm = $registrationForm;
		}
		else
		{
			throw new Exception("Register page not found");
		}		
	}
	
	public function removeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		if($this->_request->isPost())
		{
			try
			{
				$cart_id = $this->_request->getPost('cart_id');
				$cart_result = Eicra_Global_Variable::getSession()->cart_result;				
				unset($cart_result[$cart_id]);				
				Eicra_Global_Variable::getSession()->cart_result = $cart_result;
				if(!empty($cart_result))
				{			
					$invoice_arr = $this->generateInvoice($cart_result);
					if($invoice_arr['status'] == 'ok')
					{
						Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
					}
				}	
				$json_arr = array('status' => 'ok');
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
	}
	
	
	private function generateInvoice($cart_result)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$country_db = new Eicra_Model_DbTable_Country();
		$payment_db = new Paymentgateway_Model_DbTable_Gateway();
		$payment_info = $payment_db->getDefaultGateway();
		$auth = Zend_Auth::getInstance ();
		
		$global_conf = Zend_Registry::get('global_conf');
		$currency = new Zend_Currency($global_conf['default_locale']);
		$currencySymbol = $currency->getSymbol(); 
		$currencyShortName = $currency->getShortName();
		$marchent	=	new Invoice_Controller_Helper_Marchent($global_conf);
		$preferences_db = new Property_Model_DbTable_Preferences();
		$preferences_info = $preferences_db->getOptions();
		
		$property_db		=	new Property_Model_DbTable_Properties();
		
		if ($auth->hasIdentity ())
		{
			
			$globalIdentity = $auth->getIdentity();
			$user_id = $globalIdentity->user_id;			
			foreach($globalIdentity as $globalIdentity_key => $globalIdentity_value)
			{
				$mem_info[$globalIdentity_key] = $globalIdentity_value;
				$invoice_arr[$globalIdentity_key] = $globalIdentity_value;
			}
			if($mem_info['country'])
			{
				$country_info = $country_db->getInfo($mem_info['country']);
				$mem_info['country_name'] = stripslashes($country_info['value']);
			}			
			
			//invoice_arr assigning started
			$invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']).' '.stripslashes($mem_info['firstName']).' '.stripslashes($mem_info['lastName']).'<br />'.stripslashes($mem_info['mobile']).'<br />'.stripslashes($mem_info['city']).', '.stripslashes($mem_info['state']).', '.stripslashes($mem_info['postalCode']).'<br />'.stripslashes($country_info['value']).'<BR />'.stripslashes($mem_info['username']);
			$invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
			$invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
			$invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]	= date("Y-m-d h:i:s");
			$invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y",strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
			$invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
			$invoice_arr['site_name']	=	stripslashes($global_conf['site_name']);
			$invoice_arr['site_url']	=	stripslashes($global_conf['site_url']);
			$this->view->mem_info = $mem_info;
		}
			
		//invoice_items_arr assigning started
		$total_amount = 0;
		$total_tax = 0;
		$invoice_items_arr = array();
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
											<tbody>
											<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>'.$this->_translator->translator("property_invoice_desc_title").'</strong></td>
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>'.$this->_translator->translator("property_invoice_amount_title").'</strong></td>		
											</tr>';
		$marchent_email_id_arr = array();
		$item_arr = 0;
		foreach($cart_result as $cart_result_key => $dataInfo)
		{		
			$search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $dataInfo['property_id']);
			$list_info = $property_db->getListInfo(null, $search_params, array('owner_country' => array( 'owner_country_name' => 'ocut.value'), 'userChecking' => false)) ;
			if($list_info)
			{	
				foreach($list_info as $info_arr)
				{		
					$property_info = (is_array($info_arr)) ? $info_arr : $info_arr->toArray();
				}
			}	
			$dataInfo[Eicra_File_Constants::MAIN_TABLE_ID] = $dataInfo['property_id'];
			$marchent_email_id_arr[$cart_result_key] = $property_info['username'];
			$total_night = (int)$this->getNights($dataInfo[Eicra_File_Constants::PROPERTY_CHECK_IN], $dataInfo[Eicra_File_Constants::PROPERTY_CHECK_OUT]);
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] =   Zend_Json_Encoder::encode($dataInfo);
			$item_details = '<tr><td colspan="2" style="height:8px"></td></tr><tr>
								<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
								<strong>'.stripslashes($property_info['property_name']).'</strong>
								</td>
							</tr>';
			$invoice_items_arr[$item_arr]['inv']['property_name']	=	stripslashes($property_info['property_name']);
			$item_total = 0;
										
						$sub_total = $this->view->price($property_info['property_price']) * $total_night;
						
						$item_details .= '<tr><td colspan="2" style="height:2px"></td></tr><tr>
								<td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6">'.
									'<table width="100%">
										<tr>
											<td><img src="'.$this->view->serverUrl().$this->view->baseUrl().'/data/frontImages/property/property_image/'.$property_info['property_primary_image'].'" height="60" title="'.stripslashes($property_info['property_name']).'" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;"  /><span style="color:#F60; font-weight:bold; font-style:italic; text-decoration:underline;">';
											if(!empty($property_info['feature_room_no'])){ $item_details .= (int)stripslashes($property_info['feature_room_no']).' '. $this->_translator->translator('property_front_page_room'); } 
											if(!empty($property_info['feature_bathroom'])){ $item_details .= ' , ' . (int)stripslashes($property_info['feature_bathroom']).' '.$this->_translator->translator('property_front_page_bathroom' ); } 
											if(!empty($property_info['feature_bedroom'])){ $item_details .=  ' , ' . (int)stripslashes($property_info['feature_bedroom']).' '.$this->_translator->translator('property_front_page_bedroom'); }
											$item_details .= '</span><br />'
											.'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("property_invoice_check_in").' :</strong></span> '.$dataInfo[Eicra_File_Constants::PROPERTY_CHECK_IN].
											'<br  />'.
											'<span style="color:#06C; line-height:20px; font-weight:bold;"><strong>'.$this->_translator->translator("property_invoice_check_out").' :</strong></span> '.$dataInfo[Eicra_File_Constants::PROPERTY_CHECK_OUT].
											'<br  />'.
											'</td>'.
										'</tr>'.
									'</table>'.
								'</td>'.
								'<td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6">'.
									'<table border="0" width="100%">'.
										'<tr>'.
											'<td><span style="font-weight:bold;">'.$this->_translator->translator("property_invoice_total_night").' :</span> : '.$total_night.'</td>'.
										'</tr>'.												
										'<tr>'.
											'<td><span style="font-weight:bold;">'.$this->_translator->translator("property_invoice_room_cost").' </span> : '.$currencySymbol.' '.number_format($sub_total, 2, '.', ',').' '.$currencyShortName.'</td>'.
										'</tr>';
										if(Settings_Service_Price::is_exists('3', $global_conf, $sub_total))
										{												
											$booking_fee = Settings_Service_Price::getMargine('3');
											$booking_fee_amount	=	$this->view->price($sub_total, null, '3');
											$sub_total = $sub_total + $booking_fee_amount;
											$booking_fee_show	=	(preg_match("/%/i", $booking_fee)) ? $currencySymbol.' '.$booking_fee_amount.' '.$booking_fee : $booking_fee;
											$item_details .= '<tr>'.
												'<td><span style="font-weight:bold;">'.$this->_translator->translator("property_invoice_booking_fee").'</span> : '.$booking_fee_show.'</td>'.
											'</tr>';
											$item_details .= '<tr>'.
												'<td><span style="font-weight:bold;">'.$this->_translator->translator("property_invoice_total_title").'</span> : '.$currencySymbol.' '.number_format($sub_total, 2, '.', ',').' '.$currencyShortName.'</td>'.
											'</tr>';
										}
					$item_details .= '</table>'.
								'</td>
							</tr>';	
							$item_total += 	$sub_total;	
			if($property_info && $property_info['billing_item_desc'])
			{
				$item_details .= '<tr><td colspan="2" style="padding:26px 0px 15px 10px;"><strong>'.$this->_translator->translator("property_front_page_property_desc").'</strong>'.stripslashes($property_info['billing_item_desc']).'</td></tr>';
			}
			$invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::INVOICE_BOOK_FEE_SHOW] = $booking_fee_show;
			$invoice_items_arr[$item_arr]['inv']['property_price'] = $property_info['property_price'];
			$invoice_items_arr[$item_arr]['inv']['sub_total'] = $sub_total;
			$invoice_items_arr[$item_arr]['inv']['property_primary_image']	=	stripslashes($property_info['property_primary_image']);		
			$invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::PROPERTY_CHECK_IN]	=	$dataInfo[Eicra_File_Constants::PROPERTY_CHECK_IN];	
			$invoice_items_arr[$item_arr]['inv'][Eicra_File_Constants::PROPERTY_CHECK_OUT]	=	$dataInfo[Eicra_File_Constants::PROPERTY_CHECK_OUT];	
			$invoice_items_arr[$item_arr]['inv']['feature_room_no']	=	$property_info['feature_room_no'];
			$invoice_items_arr[$item_arr]['inv']['feature_bathroom']	=	$property_info['feature_bathroom'];
			$invoice_items_arr[$item_arr]['inv']['feature_bedroom']	=	$property_info['feature_bedroom'];	
			$invoice_items_arr[$item_arr]['inv']['session_key'] = $cart_result_key;
			$invoice_items_arr[$item_arr]['inv']['total_night'] = $total_night;			
				
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $item_total;
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
			$total_tax = 0;							
			$total_amount += $item_total;
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
			$item_arr++;
		}	
				
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
					<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
					<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("property_invoice_total_title").'&nbsp;</div>
					</td>
					<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format(($total_amount+$total_tax), 2, '.', ',').' '.$currencyShortName.'</strong></td>
				</tr>';
				
		$services_charge = 0;
		if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
		{
			$services_charge = $this->view->price($total_amount, null,'4');
			$services_charge_margine = Settings_Service_Price::getMargine('4');
			$services_charge_margine_show	=	(preg_match("/%/i", $services_charge_margine)) ? $currencySymbol.' '.$services_charge.' '.$services_charge_margine : $services_charge_margine;
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("property_invoice_service_charge").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$services_charge_margine_show.'</strong></td>'.
			'</tr>';
		}
		$now_payable = 0;
		$deposit_charge = 0;
		if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount))
		{	
			$deposit_charge		= $this->view->price($total_amount, null, '5');
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("property_invoice_deposit_charge", Settings_Service_Price::getMargine('5')).'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($deposit_charge, 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
			
			$now_payable	=	$services_charge	+	$deposit_charge;
			
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">'.$this->_translator->translator("property_invoice_deposit_payable").'</td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format($now_payable, 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
		}
		if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
		{	
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
				'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("property_invoice_grand_total").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.number_format(($total_amount+$total_tax+$services_charge), 2, '.', ',').' '.$currencyShortName.'</strong></td>'.
			'</tr>';
		}
		if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount) && !empty($now_payable))
		{
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">'.
				'<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right; font-weight:bold;" height="40" colspan="2">'.$this->_translator->translator("property_invoice_later_payable", $currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge) - $now_payable), 2, '.', ',').' '.$currencyShortName).'</td>'.
			'</tr>';
		}
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
														</table>';
				
				
		/*********ASSIGN PAYMENT INFO AND PAY TO START*******************/	
		$property_info['item'] = $preferences_info;
		if($marchent->isMarchentPaymentEnable(array('item_count' => $item_arr, 'dataInfo' => $property_info)))
		{
			$invoice_arr[Eicra_File_Constants::PAY_TO] = '<strong>'.stripslashes($property_info['owner_name']).'</strong>';
			$invoice_arr[Eicra_File_Constants::PAY_TO] .=  ($property_info['package_name']) ? '<br /><strong>'.$property_info['package_name'].'</strong>' : '';
			//if($dataInfo['item']['is_profile'] == 'YES')
			//{
				//if($dataInfo['item']['is_telephone'] == 'YES')
				//{
					$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($property_info['owner_phone']) ? '<br /><strong>'.$this->_translator->translator('property_front_page_label_phone').'</strong> '.stripslashes($property_info['owner_phone']) : '<br />';
				//}					
				$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($property_info['owner_address']) ? '<br />'.stripslashes($property_info['owner_address']) : '';
				$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($property_info['owner_country_name']) ? '<br />'.stripslashes($property_info['owner_country_name']) : '';
				$invoice_arr[Eicra_File_Constants::PAY_TO] .= '<br />'.stripslashes($property_info['owner_email']);
			//}			
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $property_info['property_owner'];
		}
		/*********ASSIGN PAYMENT INFO AND PAY TO END*******************/
		$invoice_arr[Eicra_File_Constants::MARCHENT_EMAIL_ID] = ($marchent_email_id_arr[0]) ? implode(',', $marchent_email_id_arr) : '';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
		$invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
		$invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
		$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
		$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;	
		$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;	
		$invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT]	=	$currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge)), 2, '.', ',').' '.$currencyShortName;
				
		//Initialize Invoice Action
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] 			= 'Property';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] 	= 'createItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] 	= '';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] 	= 'deleteItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] 	= '';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] 	= 'paidItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] 	= 'unpaidItinerary';
		$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] 	= 'cancelItinerary';
		
		//Initialize Email Template
		$template_id_field	=	'default_template_id';
		$settings_db = new Invoice_Model_DbTable_Setting();
		$template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);		
		$invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("property_invoice_template_id");	
		
		$templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id'] );
		$invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
		$invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
		$invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;
	
		$return = array('status' => 'ok','invoice_arr' => $invoice_arr);		
		
		return 	$return;
	}
	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?','invoice_template')
								->limit(1);
		}
		else
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.id = ?',$letter_id)
								->limit(1);
		}
							
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$templates_arr = $row ;
			}
		}
		else
		{
			$templates_arr['templates_desc'] = '';
		}			
		foreach($datas as $key=>$value)
		{
			$templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_desc']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_desc']);
			$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_title']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_title']);
		}
		return $templates_arr;
	}
	
	public function availabilityAction()
	{
		if($this->_request->isPost())
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			try
			{
				$property_id = $this->_request->getPost('property_id');
				$check_in_date = $this->_request->getPost('check_in');
				$check_out_date = $this->_request->getPost('check_out');
				
				$property_db = new Property_Model_DbTable_Properties();
				$property_info = $property_db->getPropertiesInfo($property_id);
				
				if($property_info)
				{
					if($this->isAvailable($property_info['available_information'], $check_in_date, $check_out_date))
					{
						$msg = $this->_translator->translator('property_check_availability_success');
						$json_arr = array('status' => 'ok','msg' => $msg, 'check_status' => '1');
					}
					else
					{
						$msg = $this->_translator->translator('property_check_availability_err');
						$json_arr = array('status' => 'ok','msg' => $msg, 'check_status' => '0');
					}
				}
				else
				{
					$msg = $this->_translator->translator('property_check_availability_err');
					$json_arr = array('status' => 'ok','msg' => $msg, 'check_status' => '0');
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
	}
	
	private function isAvailable($available_information, $check_in_date, $check_out_date)
	{	
		if(!empty($available_information) && !empty($check_in_date) && !empty($check_out_date))
		{
			$return = true;
			$available_date_arr = $this->getAvailableDate($available_information);
			$request_date_arr = $this->getRequestDate($check_in_date, $check_out_date);
			foreach($request_date_arr as $key => $request_date)
			{
				if(!in_array($request_date, $available_date_arr))
				{
					$return = false;
					break;
				}
			}
		}
		else
		{
			$return = false;
		}
		return $return;
	}
	
	private function getAvailableDate($available_information)
	{
		$available_information_arr = explode(',', $available_information);
		$available_date_arr = array();
		$i = 0;
		foreach($available_information_arr as $$available_information_arr_key => $available_information_data)
		{
			$available_information_data_arr = explode(';;', $available_information_data);
			if($available_information_data_arr[1] == '1')
			{
				$available_date_arr[$i] = $available_information_data_arr[0];
				$i++;
			}
		}
		return $available_date_arr;
	}
	
	private function getRequestDate($check_in_date, $check_out_date)
	{
		$date_array = array();		
		for($i = $check_in_date, $j = 0; $i <= $check_out_date; $i = date("Y-m-d", strtotime("+1 day", strtotime($i))), $j++)
		{
			$date_array[$j] = $i;
		}
		return $date_array;
	}
	
	private function getNights($sStartDate, $sEndDate)
	{		
		$sStartDate =strtotime($sStartDate);
		$sEndDate = strtotime($sEndDate);
		
		$datediff = $sEndDate - $sStartDate;
		$aDays	= floor($datediff/(60*60*24));
		
		return $aDays;
	}
	
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}
	
	
	public function registerAction()
    {
		
		$selected_role_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $this->_request->getParam('role_id');	
		
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		
		$registrationForm =  new Members_Form_UserForm ($role_info);
		if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
		$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';	
			
        if ($this->_request->isPost()) 
		{			
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				$element_name = $element->getName();
				if($element->getType() == 'Zend_Form_Element_File')
				{					
					$registrationForm->removeElement($element_name);
				}
				$element_name_arr = explode('_',$element_name);
				if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
				{
					$package_msg = 'ok';
				}
			}
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			try
			{	
				if($role_info['role_lock'] == '1' && $role_info != null)
				{			
									
					if ($registrationForm->isValid($this->_request->getPost())) 
					{
						$registrationForm =  new Members_Form_UserForm ($role_info);
						$registrationForm->populate($this->_request->getPost());
						$username = $this->_request->getPost('username');
						$user_check = new Members_Controller_Helper_Registers(); 
									
						if($user_check->getUsernameAvailable($username))
						{
							if($this->_request->getPost('password') == $this->_request->getPost('confirmPassword'))
							{
								$fromValues = $registrationForm;
								$members = new Members_Model_Members($this->_request->getPost());
								$perm = new Members_View_Helper_Allow();
								$members->setRole_id($selected_role_id);							
																															
								if($role_info)
								{
									if($role_info['auto_approve'] == '1')
									{							
										$members->setStatus(1);
									}
									else
									{
										$members->setStatus(0);
									}
								}
								else
								{
									$members->setStatus($translator->translator("set_frontend_registration_auto_publish", '', 'Members'));
								}	
														
								$members->setLoginurl($loginUrl);
										
								$result = $members->saveRegister();
								
								if($result['status'] == 'ok')
								{
									$msg = $translator->translator("member_registered_successfull", '', 'Members');
									$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
									if(!empty($role_info) && !empty($role_info['form_id']))
									{
										$field_db = new Members_Model_DbTable_Fields();
										$packageObj = new Members_Controller_Helper_Packages();
										$field_groups = $field_db->getGroupNames($form_info['id']); 
										
										//Add Data To Database
										foreach($field_groups as $group)
										{
											$group_name = $group->field_group;
											$displaGroup = $registrationForm->getDisplayGroup($group_name);
											$elementsObj = $displaGroup->getElements();
											foreach($elementsObj as $element)
											{
												$table_id = $result['id'];
												$form_id = $form_info['id'];
												$field_id	=	$element->getAttrib('rel');
												$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
												if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
												{ 
													if(is_array($field_value)) { $field_value = ''; }
												}
												if($element->getType() == 'Zend_Form_Element_Multiselect') 
												{ 
													$field_value	=	$this->_request->getPost($element->getName());
													if(is_array($field_value)) { $field_value = implode(',',$field_value); }
												}
												try
												{
													$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
													$DBconn = Zend_Registry::get('msqli_connection');
													$DBconn->getConnection();
													$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
													if($package_msg == 'ok')
													{
														$e_name = $element->getName();
														$element_name_arr = explode('_',$e_name);
														if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
														{
															$package_info = $packageObj->getFormAllDatas($field_value);
														}
													}
													$msg = $translator->translator("member_registered_successfull",'' ,'Members');
													$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
												catch(Exception $e)
												{
													$msg = $translator->translator("member_registered_err",'' ,'Members');
													$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
											}
										}
										
										//Email Send
										$register_helper = new Members_Controller_Helper_Registers();
										$field_info = $field_db->getFieldsInfo($role_info['form_id']);
										$allDatas = $members->getAllDatas();
										
										if($field_info)
										{
											$atta_count = 0;
											$attach_file_arr = array();
											foreach($field_info as $element)
											{
												if($element->field_type == 'file')
												{
													$attach_file_arr[$atta_count] = $allDatas[$element->field_name];
													$atta_count++;
												}
											}
										}
										else
										{
											$attach_file_arr = null;
										}
										
										$allDatas['status'] = ($data['status'] == '1') ? 'active' : 'inactive';
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();
										try 
										{
											if(!empty($allDatas['member_package']))
											{	
												if($registrationForm->member_package)
												{
													$allDatas['member_package'] = $registrationForm->member_package->getMultiOption($allDatas['member_package']);
												}
												else
												{
													$allDatas['member_package'] = '';
												}
											}
											else
											{							
												$allDatas['member_package'] = '';
											}
											$register_helper->sendMail($allDatas,$form_info,$attach_file_arr);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
										//Delete Attached Files
										if($form_info['attach_file_delete'] == '1')
										{
											if($attach_file_arr != null)
											{
												foreach($attach_file_arr as $key=>$value)
												{
													if(!empty($value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}								
												}
											}
										}
										if(!empty($package_msg) && !empty($package_info))
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
										else
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
									}
									else
									{
										$register_helper = new Members_Controller_Helper_Registers();
										$allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
										$allDatas['username'] = $members->getUsername();
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();
										$allDatas['title'] = $members->getTitle();
										$allDatas['firstName'] = $members->getFirstName();
										$allDatas['lastName'] = $members->getLastName();
										$allDatas['member_package'] = '';
										try 
										{
											$register_helper->sendMail($allDatas,null,null);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
									}
									
									//Authentication Start
									
									// Get our authentication adapter and check credentials						
									$adapter = $this->getAuthAdapter(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))) ));				
									$auth = Zend_Auth::getInstance();
									$result = $auth->authenticate($adapter);
									if (!$result->isValid()) 
									{	
										$pending_error = $this->checkPendingError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password')))) );	
											
										// Invalid credentials	
										if($pending_error)
										{
											$msg = $translator->translator("member_credentials_pending_err", '', 'Members');
										}
										else if($this->checkFrontendLoginError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))))) )
										{
											$msg = $translator->translator("member_credentials_frontend_login_err", '', 'Members');
										}
										else
										{								
											$msg = $translator->translator("member_credentials_err", '', 'Members');
										}
										$datas['failedCause'] = $msg;										
										$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));					
									}
									else
									{
										
										Eicra_Global_Variable::getSession()->username= $result->getIdentity();
										$users= new Administrator_Model_DbTable_Users();
														
										$data= array ('last_access' => date('Y-m-d H:i:s'));
										$where= $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
										if (!$users->update($data,$where)) 
										{
										  $msg = $translator->translator("member_last_access_err", '', 'Members');
										  $json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
										}
										else
										{
											$userInfo = $adapter->getResultRowObject(null, array('password'));
											$authStorage = $auth->getStorage();
											$authStorage->write($userInfo);
												
											$msg = $translator->translator("member_registered_successfull",'' ,'Members').' '.$translator->translator("member_loggin_successfull",Eicra_Global_Variable::getSession()->username, 'Members').$translator->translator("member_loggin_redirecting", '', 'Members');
											$json_arr = array('status' => 'ok','msg' => $msg, 'role_id' => $userInfo->role_id, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}			
									 }
								}
								else
								{
									$msg = $translator->translator("member_registered_err",'' ,'Members');
									$json_arr = array('status' => 'err','msg' => $msg.' '.$result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
								}				
							}
							else
							{
								$msg = $translator->translator("password_not_match",'' ,'Members');
								$json_arr = array('status' => 'errP','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
							}
						}
						else
						{
							$msg = $translator->translator("member_availability",$username, 'Members');
							$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						}
					}
					else
					{
						$validatorMsg = $registrationForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}			
				}
				else
				{
					$msg = $translator->translator("member_registered_not_permitted",$role_info['role_name'],'Members');
					$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			catch(Exception $e)
			{
				$msg = $e->getMessage();
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
			{
				$this->view->selected_role_id	=	$selected_role_id;					
				$this->view->registrationForm = $registrationForm;
			}
			else
			{
				throw new Exception("Register page not found");
			}
		}			
    }
	
	protected function getAuthAdapter($values)
	{
		$tbl = Zend_Registry::get('dbPrefix').'user_profile';
		$dbAdapter  = Zend_Registry::get('msqli_connection');		
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);			
		$authAdapter = new Zend_Auth_Adapter_DbTable(
                               $dbAdapter,
								array( 'mt' => $tbl),
								'mt.username',
								'mt.password',
                        	'MD5(CONCAT(mt.salt,?))'
                             );
		$authSelect = $authAdapter->getDbSelect();
		$authSelect->joinLeft(array('r' => Zend_Registry::get('dbPrefix').'roles'), 'mt.role_id = r.role_id', array('*'));
		$authSelect->where('r.allow_login_from_frontend = ?','1');
		$authAdapter->setIdentity($values['username']);	
		$authAdapter->setCredential($values['password']);
		
		return $authAdapter;
	}
	
	private function checkPendingError($datas)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => Zend_Registry::get('dbPrefix').'user_profile',
						'field' => 'status',
						'exclude' => 'username = "'.$datas['username'].'" AND  password = MD5(CONCAT(salt,"'.$datas['password'].'"))',
					)
				);
		return $validator->isValid(0);
	}
	
	private function checkFrontendLoginError($datas)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$mem_info = $mem_db->getMemberInfoByUsername($datas['username']);
		if($mem_info && $mem_info['role_id'])
		{
			$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => Zend_Registry::get('dbPrefix').'roles',
							'field' => 'allow_login_from_frontend',
							'exclude' => 'role_id = "'.$mem_info['role_id'].'" ',
						)
					);
			return $validator->isValid(0);
		}
		else
		{
			return false;
		}
	}
}
