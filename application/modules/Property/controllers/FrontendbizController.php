<?php
class Property_FrontendbizController extends Zend_Controller_Action
{
    private $_page_id;
    private $_translator;
    private $_controllerCache;
	private $_modules_license;
	private $_auth_obj;	
	private $_ckLicense;
	private	$_snopphing_cart;
	private	$_category;
	private $currency;
    public function init ()
    {
		//$this->view->setScriptPath( '/scripts/mob_scripts');
		
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
			
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
		
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
		
    }
    public function preDispatch ()
    {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
        $this->view->assign('front_template', $front_template);
		
        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
			$page_id_arr = array_filter( $page_id_arr );	
            $this->_page_id = ($page_id_arr && !empty($page_id_arr[0])) ? implode(',', $page_id_arr) : null;
        } else {
            $this->_page_id = null;
        }
		
		if($this->_request->getParam('category'))
		{
			try
			{
				$category_url = $this->_request->getParam('category');	
				$category_db = new Property_Model_DbTable_Category();
				$category_info = $category_db->getCategoryInfoFromTitle($category_url);
				if($category_info)
				{
					$this->_category = $category_info['id'];
				}
				else
				{
					$this->_category = 0;
				}
			}
			catch(Exception $e)
			{
				$this->_category = 0;
			}
		}
		else
		{
			$this->_category = 0;
		}
    }
    private function breadcrumbArray ($breadcrumb_arr, $category_db, $parent_id, $route = 'All-B2b-Category-list/*')
    {
        $parent_info = $category_db->getCategoryName($parent_id);
        if ($parent_info) {
            if (! empty($parent_info['parent_id'])) {
                $breadcrumb_arr = $this->breadcrumbArray($breadcrumb_arr,
                        $category_db, $parent_info['parent_id'], $route);
            }
            $breadcrumb_arr[] = array($this->view->escape($parent_info['name']),
                    $this->view->url(
                            array('module' => $this->view->getModule,
                                    'controller' => $this->view->getController,
                                    'action' => $this->view->getAction,
                                    'category' => $parent_info['url_portion']), $route, true));
        }
        return $breadcrumb_arr;
    }	
	
	private function property_truncate($phrase,$start_words, $max_words, $char = false, $charset = 'UTF-8')
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				  $dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				  $phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
				}
		}
		else
		{
		   $phrase_array = explode(' ',$phrase);
		   if(count($phrase_array) > $max_words && $max_words > 0)
			  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
	   return $phrase;
	}
	
	public function groupAction()
    {
		try 
		{
			// BreadCrumb
			/*Eicra_Global_Variable::getSession()->breadcrumb = array(
					array(
							$this->_translator->translator('b2b_front_product_list',
									'', 'B2b'), $this->view->url()),
					array(
							$this->_translator->translator(
									'b2b_front_product_list_active', '', 'B2b'),
							$this->view->url()));*/
			$preferences_db = new Property_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('group_id') : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'groups', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			// action body
			if ($preferences_data && $preferences_data['property_list_by_group_other_page_sortby'] && $preferences_data['property_list_by_group_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['property_list_by_group_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['property_list_by_group_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Classified-Property-Group-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
			$list_mapper = new Property_Model_PropertiesListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('property_group' =>  array( 'group_name' => 'pg.property_name', 'review_id' => 'pg.review_id', 'file_thumb_width' => 'pg.file_thumb_width', 'file_thumb_height' => 'pg.file_thumb_height', 'file_thumb_resize_func' => 'pg.file_thumb_resize_func', 'group_meta_title' => 'pg.meta_title', 'group_meta_keywords' => 'pg.meta_keywords', 'group_meta_desc' => 'pg.meta_desc'), 'userChecking' => false));
										
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));							
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
				$property_type_arr = explode(',', $this->_translator->translator('property_front_page_rent_id'));
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
				$review_helper = new Review_View_Helper_Review();							
				$view_datas = array('data_result' => array(), 'total' => 0);			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
							$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 15, true);
							$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
							$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
							$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
							$img_thumb_arr = explode(',',$entry_arr['property_image']);	
							$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
							$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
							$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
							
							$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));
												
							$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
							
							$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
							$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
							$list_stars = '';
							for($i = 1; $i < $maximum_stars_digit; $i++)
							{
								$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
								
							}
							$entry_arr['list_stars_format']		= 	$list_stars;
							$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
									
							$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;														
																						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}	
			$view_datas['total']	=	$list_datas->getTotalItemCount();
			$view_datas['paginator']	=	$list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}
	
	public function categoryAction()
	{
		try 
		{			
			$preferences_db = new Property_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? (($this->_category) ? $this->_category : $this->_request->getParam('category_id')) : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'categories', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			//HEADER CATEGORY START
			 $parent_id = ($this->_category) ? $this->_category : ((count($this->view->page_id_arr) > 1) ? 0 : $this->view->page_id_arr[0]);
			 $this->view->category_db = new Property_Model_DbTable_Category();
			
			 $search_params['filter']['filters'][0] = array( 'field' => 'parent', 'operator' => 'eq', 'value' => $parent_id);
			 $search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';
			
			 $this->view->category_info = $this->view->category_db->getListInfo('1', $search_params, false);
			 	 
			//HEADER CATEGORY END
			
			// action body
			if ($preferences_data && $preferences_data['property_list_by_category_other_page_sortby'] && $preferences_data['property_list_by_category_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['property_list_by_category_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['property_list_by_category_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Classified-Property-Category-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'category_id' => $this->_request->getParam('category_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
			$list_mapper = new Property_Model_PropertiesListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('property_category' =>  array( 'category_name' => 'pc.category_name', 'category_meta_title' => 'pc.meta_title', 'category_meta_keywords' => 'pc.meta_keywords', 'category_meta_desc' => 'pc.meta_desc'), 'userChecking' => false));
						
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
						$property_type_arr = explode(',', $this->_translator->translator('property_front_page_rent_id'));
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
				$review_helper = new Review_View_Helper_Review();
								
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{					
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
							$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 15, true);
							$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
							$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
							$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
							$img_thumb_arr = explode(',',$entry_arr['property_image']);	
							$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
							$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
							$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
							
							$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));	
												
							$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
							
							$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
							$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
							$list_stars = '';
							for($i = 1; $i < $maximum_stars_digit; $i++)
							{
								$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
								
							}
							$entry_arr['list_stars_format']		= 	$list_stars;
							$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
							$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;
																								
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			
			$view_datas['total'] = $list_datas->getTotalItemCount();	
			$view_datas['paginator'] =   $list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}	
	
	public function businesstypeAction()
	{
		try 
		{			
			$preferences_db = new Property_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('type_id') : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'businesstype', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			// action body
			if ($preferences_data && $preferences_data['property_list_by_business_type_other_page_sortby'] && $preferences_data['property_list_by_business_type_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['property_list_by_business_type_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['property_list_by_business_type_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Classified-Property-Business-Type-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'type_id' => $this->_request->getParam('type_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
			$list_mapper = new Property_Model_PropertiesListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('property_group' =>  array( 'group_name' => 'pg.property_name', 'review_id' => 'pg.review_id', 'file_thumb_width' => 'pg.file_thumb_width', 'file_thumb_height' => 'pg.file_thumb_height', 'file_thumb_resize_func' => 'pg.file_thumb_resize_func', 'group_meta_title' => 'pg.meta_title', 'group_meta_keywords' => 'pg.meta_keywords', 'group_meta_desc' => 'pg.meta_desc'), 'userChecking' => false));
						
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','property_page', false );
				$review_helper = new Review_View_Helper_Review();				
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
							$entry_arr['property_name_format'] = $this->property_truncate( $entry_arr['property_name'], 0, 15, true);
							$entry_arr['property_desc_format'] = $this->view->escape(strip_tags( $entry_arr['property_desc']));
							$entry_arr['property_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['property_date'])));	
							$entry_arr['property_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['property_date'])));
							$img_thumb_arr = explode(',',$entry_arr['property_image']);	
							$property_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
							$entry_arr['property_image_format'] = ($this->view->escape($entry_arr['property_primary_image'])) ? 'data/frontImages/property/property_image/'.$this->view->escape($entry_arr['property_primary_image']) :  'data/frontImages/property/property_image/'.$img_thumb_arr[0] ;
							$entry_arr['property_image_no_format'] = $this->_translator->translator('property_front_page_property_photo_no', $this->view->numbers($property_image_no));
								
							$entry_arr['property_price_format'] = $this->view->numbers(number_format($this->view->price($this->view->escape($entry_arr['property_price'])), 2, '.', ""));
												
							$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
							
							$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
							$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
							$list_stars = '';
							for($i = 1; $i < $maximum_stars_digit; $i++)
							{
								$list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_property_img/star-inactive.png" />';
								
							}
							$entry_arr['list_stars_format']		= 	$list_stars;
							$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['property_name']));								
							$entry_arr['book_calendar_enable']	=	(!empty($property_type_arr[0]) && in_array($entry_arr['property_type'], $property_type_arr)) ? true : false;
																									
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			
			$view_datas['total'] = $list_datas->getTotalItemCount();	
			$view_datas['paginator'] =   $list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}
	
	private function getCurrency ()
	{
		
		if (empty($this->currency))
		{
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}		
		else 
		{
			return $this->currency;
		}
		
	}
   
}