<?php

class Property_BackendtypeController extends Zend_Controller_Action
{	
	private $propertyTypeForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {	
        /* Initialize action controller here */		
		$this->propertyTypeForm =  new Property_Form_TypeForm ();
		$this->view->propertyTypeForm =  $this->propertyTypeForm;
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {
		// action body
		$group_id =	$this->_request->getParam('group_id');
		$this->view->group_id = $group_id;		
		
		$group_db = new Property_Model_DbTable_PropertyGroup();
		$group_info	=	($group_id) ? $group_db->getGroupName($group_id) : null;
		$this->view->group_info = $group_info;
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				if($group_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_request->getParam('group_id'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id'	=> $group_id, 'create' => $this->_request->getParam('create'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Property_Model_BusinessTypeListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['group']=  array('id' => $entry_arr['group_id'], 'group_name' => $entry_arr['group_name']);
								$entry_arr['publish_status_business_type'] = str_replace('_', '-', $entry_arr['business_type']);
								$entry_arr['entry_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['entry_date'])));	
								$entry_arr['entry_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['entry_date'])));
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['property_owner']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function addAction()
	{		
		/*if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->propertyTypeForm->isValid($this->_request->getPost())) 
			{	
				$businessType = new Property_Model_BusinessType($this->propertyTypeForm->getValues());
								
				$perm = new Property_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $businessType->saveBusinessType();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];
						$businessTypeInfo = new Property_Model_DbTable_BusinessType();
						$datas = $businessTypeInfo->getBusinessTypeInfo($last_id);
						
						$msg = $translator->translator("property_type_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg,'datas' => $datas);
					}
					else
					{
						$msg = $translator->translator("property_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("property_type_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->propertyTypeForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}*/	
		
		
		try
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->view->allow())
			{	
				if ($this->_request->isPost()) 
				{
					$posted_data	=	$this->_request->getParams();
					if($posted_data['models'])
					{	
						$businessTypeInfo = new Property_Model_DbTable_BusinessType(); 
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{ 
							$posted_data_value['group_id'] = ($posted_data_value['group'] && $posted_data_value['group']['id']) ? $posted_data_value['group']['id'] : $posted_data_value['group_id'];
							if($this->propertyTypeForm->isValid($posted_data_value)) 
							{	
								$businessType = new Property_Model_BusinessType($this->propertyTypeForm->getValues());					
								
								if(!$businessTypeInfo->isDuplicate($this->propertyTypeForm->getValue('business_type'), $this->propertyTypeForm->getValue('group_id')))
								{
									$save_result = $businessType->saveBusinessType();
									if($save_result['status'] == 'ok')
									{
										if($save_result['id'])
										{
											$posted_data['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $save_result['id']);
										}
										$result_row = $businessTypeInfo->getListInfo(null, $posted_data);
										$posted_data['models'][$posted_data_key]['id'] = $save_result['id'];
										foreach($result_row as $row_data)
										{
											$posted_data['models'][$posted_data_key]['group']=  array('id' => $row_data['group_id'],'group_name' => $this->view->escape($row_data['group_name']));
											$posted_data['models'][$posted_data_key]['full_name'] = $this->view->escape($row_data['full_name']);
											$posted_data['models'][$posted_data_key]['group_name'] = $this->view->escape($row_data['group_name']);
											$posted_data['models'][$posted_data_key]['id_format'] = $this->view->numbers($row_data['id']);
											$posted_data['models'][$posted_data_key]['entry_by'] = $row_data['entry_by'];
											$posted_data['models'][$posted_data_key]['entry_date'] = $row_data['entry_date'];
											$posted_data['models'][$posted_data_key]['publish_status_business_type'] = str_replace('_', '-', $row_data['business_type']);
											$posted_data['models'][$posted_data_key]['entry_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($row_data['entry_date'])));	
											$posted_data['models'][$posted_data_key]['entry_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($row_data['entry_date'])));
										}
										
										$status = 'ok';	
									}
									else
									{
										$status = 'err';
										$msg = $translator->translator("property_type_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
										break;
									}							
								}
								else
								{
									$status = 'err';	
									$msg = $translator->translator("property_type_save_duplicate_err");
									$json_arr = array('status' => 'err','msg' => $msg);
									break;
								}	
											
							}
							else
							{
								$status = 'err';
								$validatorMsg = $this->propertyTypeForm->getMessages();
								$vMsg = array();
								$i = 0;
								foreach($validatorMsg as $key => $errType)
								{					
									foreach($errType as $errkey => $value)
									{
										$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
										$i++;
									}
								}
								$json_arr = array('status' => 'errV','msg' => $vMsg);
								break;
							}	
						}	
					
						if($status == 'ok')
						{
							$msg = $translator->translator("property_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
						}			
					}
					else
					{
						$msg = $translator->translator("property_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator("property_type_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$Msg =  $translator->translator("property_type_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}
		}			
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}	
		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}
	
	public function editAction()
	{	
		/*if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			$group_id = $this->_request->getPost('group_id');
			$business_type = $this->_request->getPost('business_type');
			
			$businessType = new Property_Model_BusinessType();
			$businessType->setId($id);	
			$businessType->setGroup_id($group_id);	
			$businessType->setBusiness_type($business_type);								
				
			$perm = new Property_View_Helper_Allow();
			if($perm->allow())
			{
				$result = $businessType->saveBusinessType();
				if($result['status'] == 'ok')
				{
					$msg = $translator->translator("property_type_save_success");
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				else
				{
					$msg = $translator->translator("property_type_save_err");
					$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
				}	
			}
			else
			{
				$Msg =  $translator->translator("property_type_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}*/		
		try
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->view->allow())
			{
				if ($this->_request->isPost()) 
				{	
					$posted_data	=	$this->_request->getParams();
					if($posted_data['models'])
					{	
						$businessTypeInfo = new Property_Model_DbTable_BusinessType();
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{	
							$posted_data_value['group_id'] = ($posted_data_value['group'] && $posted_data_value['group']['id']) ? $posted_data_value['group']['id'] : $posted_data_value['group_id'];
							if($this->propertyTypeForm->isValid($posted_data_value)) 
							{											
								$businessType = new Property_Model_BusinessType($this->propertyTypeForm->getValues());
								$businessType->setId($posted_data_value['id']);					
								if(!$businessTypeInfo->isDuplicate($this->propertyTypeForm->getValue('business_type'), $this->propertyTypeForm->getValue('group_id'), $businessType->getId()))
								{							
									$save_result = $businessType->saveBusinessType();
									if($save_result['status'] == 'ok')
									{
										$status = 'ok';	
									}
									else
									{
										$status = 'err';
										$msg = $translator->translator("property_type_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
										break;
									}
								}
								else
								{
									$status = 'err';
									$msg = $translator->translator("property_type_save_duplicate_err");
									$json_arr = array('status' => 'err','msg' => $msg);
									break;
								}								
							}
							else
							{
								$validatorMsg = $this->propertyTypeForm->getMessages();
								$vMsg = array();
								$i = 0;
								foreach($validatorMsg as $key => $errType)
								{					
									foreach($errType as $errkey => $value)
									{
										$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
										$i++;
									}
								}
								$status = 'err';
								$json_arr = array('status' => 'errV','msg' => $vMsg);
								break;
							}
						}
						
						if($status == 'ok')
						{
							$msg = $translator->translator("property_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
						}
					}
					else
					{
						$msg = $translator->translator("property_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);	
					}
				}
				else
				{
					$msg = $translator->translator("property_type_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);	
				}
			}
			else
			{
				$Msg =  $translator->translator("property_type_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}			
					
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);		
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			$check_num_pro = new Property_View_Helper_PropertyGroup();
					
			if($check_num_pro->getNumOfPropertyForType($id) == '0')
			{
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'property_business_type', $where);
					$msg = 	$translator->translator('property_type_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('property_type_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('property_type_for_page_delete_err');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('property_type_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
					
				$check_num_pro = new Property_View_Helper_PropertyGroup();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{			
					if($check_num_pro->getNumOfPropertyForType($id) == '0')
					{
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'property_business_type', $where);
							$msg = 	$translator->translator('property_type_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e) 
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('property_type_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('property_type_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("property_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('property_type_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
		
}

