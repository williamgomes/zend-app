<?php
	class Members_Model_GeneralMapper
	{
		protected $_dbTable;
 
		public function setDbTable($dbTable)
		{
			if (is_string($dbTable)) {
				$dbTable = new $dbTable();
			}
			if (!$dbTable instanceof Zend_Db_Table_Abstract) {
				throw new Exception('Invalid table data gateway provided');
			}
			$this->_dbTable = $dbTable;
			return $this;
		}
	 
		public function getDbTable()
		{
			if (null === $this->_dbTable) {
				$this->setDbTable('Members_Model_DbTable_General');
			}
			return $this->_dbTable;
		}
		
		public function save(Members_Model_General $register)
		{
				 
			if ((null === ($id = $register->getId())) || empty($id)) 
			{				
				try
				{	
					unset($data['id']);
				
					$data = array(
						'form_id'  	=> 	$register->getForm_id(),
						'active'  	=> 	$register->getActive(),
						'user_id'  	=> 	$register->getUser_id(),
						'g_date'  	=> 	date("Y-m-d h:i:s")
					);
					$last_id = $this->getDbTable()->insert($data);				
					$return = array('status' => 'ok' ,'id' => $last_id);
				}
				catch(Exception $e)
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}			
			} 
			else 
			{
				$data = array(
						'form_id'   => 	$register->getForm_id()				
				);			
				
				// Start the Update process
       			try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));				
					$return = array('status' => 'ok' ,'id' => $id);
				} 
				catch (Exception $e) 
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}
			}
			return $return;
		}
	}
?>