<?php
	class Members_Model_Members
	{
		protected $_user_id;
		protected $_role_id;
		protected $_title;
		protected $_firstName;
		protected $_lastName;
		protected $_username;
		protected $_password;
		protected $_companyName;
		protected $_postalCode;
		protected $_address;
		protected $_phone;
		protected $_mobile;
		protected $_fax;
		protected $_website;
		protected $_state;
		protected $_country;
		protected $_city;
		protected $_status;
		protected $_salt;
		protected $_real_pass;
		protected $_loginurl;
		protected $_allDatas;
		protected $_package_id;
		
		
	
		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->_allDatas = $options;
				$this->setOptions($options);
			}
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveRegister()
		{			
			$mapper  = new Members_Model_MembersMapper();
			$return = $mapper->save($this);			
			return $return;
		}
		
		public function setUser_id($text)
		{
			$this->_user_id = $text;
			return $this;
		}	
		
		public function setTitle($text)
		{
			$this->_title = $text;
			return $this;
		}	
		public function setRole_id($text)
		{
			$this->_role_id = $text;
			return $this;
		}	
		public function setFirstName($text)
		{
			$this->_firstName = $text;
			return $this;
		}	
		public function setLastName($text)
		{
			$this->_lastName = $text;
			return $this;
		}	
		public function setUsername($text)
		{
			$this->_username = $text;
			return $this;
		}	
		public function setPassword($text)
		{
			$this->setSalt();
			$this->_real_pass = $text;
			$slat = $this->getSalt();
			$pass = md5($slat.$text);
			$this->_password = $pass;
			return $this;
		}	
		public function setCompanyName($text)
		{
			$this->_companyName = $text;
			return $this;
		}	
		public function setPostalCode($text)
		{
			$this->_postalCode = $text;
			return $this;
		}	
		public function setAddress($text)
		{
			$this->_address = $text;
			return $this;
		}	
		public function setPhone($text)
		{
			$this->_phone = $text;
			return $this;
		}	
		public function setMobile($text)
		{
			$this->_mobile = $text;
			return $this;
		}	
		public function setFax($text)
		{
			$this->_fax = $text;
			return $this;
		}	
		public function setWebsite($text)
		{
			$this->_website = $text;
			return $this;
		}	
		public function setState($text)
		{
			$this->_state = $text;
			return $this;
		}	
		public function setCountry($text)
		{
			$this->_country = $text;
			return $this;
		}	
		public function setCity($text)
		{
			$this->_city = $text;
			return $this;
		}	
		public function setStatus($text)
		{
			$this->_status = $text;
			return $this;
		}
		public function setSalt()
		{
			$register = new Members_Controller_Helper_Registers();
			$this->_salt = $register->createSalt() ;
			return $this;
		}
		public function setLoginurl($text)
		{
			$this->_loginurl = $text;
			return $this;
		}
		public function setPackage_id($text)
		{
			$this->_package_id = (!empty($text)) ? $text : 0;			
			return $this;
		}
		
		
		public function getUser_id()
		{         
			return $this->_user_id;
		}	
		
		public function getTitle()
		{        
			return $this->_title;
		}	
		public function getRole_id()
		{
			return $this->_role_id;
		}	
		public function getFirstName()
		{
			return $this->_firstName;
		}	
		public function getLastName()
		{
			return $this->_lastName;
		}	
		public function getUsername()
		{
			return $this->_username;
		}	
		public function getPassword()
		{
			return $this->_password;
		}	
		public function getCompanyName()
		{
			return $this->_companyName;
		}	
		public function getPostalCode()
		{
			return $this->_postalCode;
		}	
		public function getAddress()
		{
			return $this->_address;
		}	
		public function getPhone()
		{
			return $this->_phone;
		}	
		public function getMobile()
		{
			return $this->_mobile;
		}	
		public function getFax()
		{
			return $this->_fax;
		}	
		public function getWebsite()
		{
			return $this->_website;
		}	
		public function getState()
		{
			return $this->_state;
		}	
		public function getCountry()
		{
			return $this->_country;
		}	
		public function getCity()
		{
			return $this->_city;
		}
		public function getSalt()
		{
			return $this->_salt;
		}	
		public function getStatus()
		{
			return $this->_status;
		}		
		public function getReal_pass()
		{
			return $this->_real_pass;
		}
		public function getLoginurl()
		{
			return $this->_loginurl;
		}
		public function getAllDatas()
		{
			return $this->_allDatas;
		}
		public function getPackage_id()
		{
			return ($this->_package_id) ? $this->_package_id : 0;
		}
	}
?>