<?php
class Members_Model_MemberList 
{
	protected $_surname;
    protected $_title;
    protected $_forename;
	protected $_status;
	protected $_username;
   	
	
	public function __construct(array $options = null)
    {
        if (is_array($options)) 
		{
            $this->setOptions($options);
        }
    }
	
	public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }
	
	public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }	
	
	public function setSurname($text)
    {
        $this->_surname = $text;
        return $this;
    }
	
	public function setTitle($text)
    {
        $this->_title = $text;
        return $this;
    }
	
	public function setForename($text)
    {
        $this->_forename = $text;
        return $this;
    }
	
	public function setUsername($text)
    {
        $this->_Username = $text;
        return $this;
    }
	
	public function setStatus($text)
    {
        $this->_status = $text;
        return $this;
    }
	
	public function getSurname()
    {
        return $this->_surname;
    }
	
	public function getTitle()
    {
        return $this->_title;
    }
	
	public function getForename()
    {
        return $this->_forename;
    }
	
	public function getStatus()
    {
        return $this->_status;
    }
	
	public function getUsername()
    {
        return $this->_username;
    }	
}
?>