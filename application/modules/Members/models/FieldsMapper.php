<?php
	class Members_Model_FieldsMapper
	{
		protected $_dbTable;
 
		public function setDbTable($dbTable)
		{
			if (is_string($dbTable)) {
				$dbTable = new $dbTable();
			}
			if (!$dbTable instanceof Zend_Db_Table_Abstract) {
				throw new Exception('Invalid table data gateway provided');
			}
			$this->_dbTable = $dbTable;
			return $this;
		}
	 
		public function getDbTable()
		{
			if (null === $this->_dbTable) {
				$this->setDbTable('Members_Model_DbTable_Fields');
			}
			return $this->_dbTable;
		}
		
		public function save(Members_Model_Fields $register)
		{
				 
			if ((null === ($id = $register->getId())) || empty($id)) 
			{				
				try
				{	
					unset($data['id']);
				
					$data = array(
						'form_id'   				=> 	$register->getForm_id(),
						'field_name' 				=> 	$register->getField_name(),
						'field_group' 				=> 	$register->getField_group(),
						'group_order' 				=> 	$register->getGroup_order(),
						'field_id' 					=>	$register->getField_id(),
						'field_class' 				=>	$register->getField_class(),
						'field_type' 				=>	$register->getField_type(),
						'field_width' 				=>	$register->getField_width(),
						'field_height' 				=>	$register->getField_height(),
						'field_separator'			=>	$register->getField_separator(),
						'field_label' 				=>	$register->getField_label(),
						'field_title' 				=>	$register->getField_title(),
						'field_desc' 				=>	$register->getField_desc(),
						'field_option' 				=>	$register->getField_option(),
						'field_default_value'		=>	$register->getField_default_value(),
						'regexpr' 					=>	$register->getRegexpr(),
						'display_admin' 			=>	$register->getDisplay_admin(),
						'display_frontend' 			=>	$register->getDisplay_frontend(),
						'required'					=>	$register->getRequired(),
						'field_order'				=>	$register->getField_order()
					);
					$last_id = $this->getDbTable()->insert($data);				
					$return = array('status' => 'ok' ,'id' => $last_id);
				}
				catch(Exception $e)
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}			
			} 
			else 
			{
				$data = array(
						'form_id'   				=> 	$register->getForm_id(),
						'field_name' 				=> 	$register->getField_name(),
						'field_group' 				=> 	$register->getField_group(),
						'group_order' 				=> 	$register->getGroup_order(),
						'field_id' 					=>	$register->getField_id(),
						'field_class' 				=>	$register->getField_class(),
						'field_type' 				=>	$register->getField_type(),
						'field_width' 				=>	$register->getField_width(),
						'field_height' 				=>	$register->getField_height(),
						'field_separator'			=>	$register->getField_separator(),
						'field_label' 				=>	$register->getField_label(),
						'field_title' 				=>	$register->getField_title(),
						'field_desc' 				=>	$register->getField_desc(),
						'field_option' 				=>	$register->getField_option(),
						'field_default_value'		=>	$register->getField_default_value(),
						'regexpr' 					=>	$register->getRegexpr(),
						'display_admin' 			=>	$register->getDisplay_admin(),
						'display_frontend' 			=>	$register->getDisplay_frontend(),
						'required'					=>	$register->getRequired(),
						'field_order'				=>	$register->getField_order()					
				);			
				
				// Start the Update process
       			try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));				
					$return = array('status' => 'ok' ,'id' => $id);
				} 
				catch (Exception $e) 
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}
			}
			return $return;
		}
	}
?>