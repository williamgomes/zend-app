<?php
	class Members_Model_General
	{
		protected $_id;
		protected $_form_id;
		protected $_user_id;
		protected $_active;	
		
	
		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveGeneral()
		{			
			try
			{
				$mapper  = new Members_Model_GeneralMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = $e->getMessage();
			}
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}		
			
		public function setForm_id($text)
		{
			$this->_form_id = $text;
			return $this;
		}
			
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		public function setUser_id($text = null)
		{
			if(empty($text))
			{
				$auth = Zend_Auth::getInstance ();
				if ($auth->hasIdentity ())
				{
					$globalIdentity = $auth->getIdentity ();
					$this->_user_id = $globalIdentity->user_id;
				}
				else
				{
					$this->_user_id = '0';
				}
			}
			else
			{
				$this->_user_id = $text;
			}
			return $this;
		}	
				
		
		public function getId()
		{         
			return $this->_id;
		}	
			
		public function getForm_id()
		{
			return $this->_form_id;
		}	
			
		public function getActive()
		{
			return $this->_active;
		}
		
		public function getUser_id()
		{     
			if(empty($this->_user_id))
			{
				$this->setUser_id(); 
			}      
			return $this->_user_id;
		}		
	}
?>