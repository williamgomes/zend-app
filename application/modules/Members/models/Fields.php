<?php
	class Members_Model_Fields
	{
		protected $_id;
		protected $_form_id;
		protected $_field_name;
		protected $_field_group;
		protected $_group_order;
		protected $_field_id;
		protected $_field_class;
		protected $_field_type;
		protected $_field_width;
		protected $_field_height;
		protected $_field_separator;
		protected $_field_label;
		protected $_field_title;
		protected $_field_desc;
		protected $_field_option;
		protected $_field_default_value;
		protected $_regexpr;
		protected $_display_admin;
		protected $_display_frontend;
		protected $_required;
		protected $_field_order;	
		
	
		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveFields()
		{			
			try
			{
				$mapper  = new Members_Model_FieldsMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = $e->getMessage();
			}
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}		
			
		public function setForm_id($text)
		{
			$this->_form_id = $text;
			return $this;
		}	
		public function setField_name($text)
		{
			$this->_field_name = str_replace('-','_',str_replace(' ','_',trim($text)));
			return $this;
		}	
		public function setField_group($text)
		{
			$this->_field_group = addslashes(trim($text));
			return $this;
		}
		public function setGroup_order($text)
		{
			$this->_group_order = addslashes(trim($text));
			return $this;
		}
		public function setField_id($text)
		{
			$this->_field_id = str_replace('-','_',str_replace(' ','_',trim($text)));
			return $this;
		}	
		public function setField_class($text)
		{
			$this->_field_class = trim($text);
			return $this;
		}	
			
		public function setField_type($text)
		{
			$this->_field_type = trim($text);
			return $this;
		}	
			
		public function setField_width($text)
		{
			$this->_field_width = addslashes(trim($text));
			return $this;
		}	
			
		public function setField_height($text)
		{
			$this->_field_height = addslashes(trim($text));
			return $this;
		}
			
		public function setField_separator($text)
		{
			$this->_field_separator = trim($text);
			return $this;
		}	
		
		public function setField_label($text)
		{
			$this->_field_label = str_replace('"', "'",trim($text));
			return $this;
		}	
		
		public function setField_title($text)
		{
			$this->_field_title = trim($text);
			return $this;
		}
			
		public function setField_desc($text)
		{
			$this->_field_desc = trim($text);
			return $this;
		}	
		
		public function setField_option($text)
		{
			$this->_field_option = trim($text);
			return $this;
		}	
		
		public function setField_default_value($text)
		{
			$this->_field_default_value = trim($text);
			return $this;
		}
			
		public function setRegexpr($text)
		{
			$this->_regexpr = trim($text);
			return $this;
		}	
		
		public function setDisplay_admin($text)
		{
			$this->_display_admin = addslashes(trim($text));
			return $this;
		}
			
		public function setDisplay_frontend($text)
		{
			$this->_display_frontend = addslashes(trim($text));
			return $this;
		}	
		
		public function setRequired($text)
		{
			$this->_required = $text;
			return $this;
		}
					
		public function setField_order($text)
		{
			$this->_field_order = addslashes(trim($text));
			return $this;
		}
		
		
		
		public function getId()
		{         
			return $this->_id;
		}	
			
		public function getForm_id()
		{
			return $this->_form_id;
		}	
		public function getField_group()
		{
			return $this->_field_group;
		}
		public function getField_name()
		{
			return $this->_field_name;
		}
		public function getGroup_order()
		{
			return $this->_group_order;
		}	
		public function getField_id()
		{
			if($this->_field_type == 'file')
			{
				$this->_field_id = $this->_field_name;
			}
			return $this->_field_id;
		}
			
		public function getField_class()
		{
			return $this->_field_class;
		}
			
		public function getField_type()
		{
			return $this->_field_type;
		}
			
		public function getField_width()
		{
			return $this->_field_width;
		}
			
		public function getField_height()
		{
			return $this->_field_height;
		}
			
		public function getField_separator()
		{
			return $this->_field_separator;
		}
			
		public function getField_label()
		{
			return $this->_field_label;
		}
			
		public function getField_title()
		{
			return $this->_field_title;
		}
			
		public function getField_desc()
		{
			return $this->_field_desc;
		}	
		
		public function getField_option()
		{
			return $this->_field_option;
		}	
		
		public function getField_default_value()
		{
			return $this->_field_default_value;
		}
			
		public function getRegexpr()
		{
			return $this->_regexpr;
		}	
		
		public function getDisplay_admin()
		{
			return $this->_display_admin;
		}
			
		public function getDisplay_frontend()
		{
			return $this->_display_frontend;
		}
		
		public function getRequired()
		{
			return $this->_required;
		}
		
		public function getField_order()
		{
			return $this->_field_order;
		}	
	}
?>