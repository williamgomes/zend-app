<?php
	class Members_Model_MembersMapper
	{
		protected $_dbTable;
 
		public function setDbTable($dbTable)
		{
			if (is_string($dbTable)) {
				$dbTable = new $dbTable();
			}
			if (!$dbTable instanceof Zend_Db_Table_Abstract) {
				throw new Exception('Invalid table data gateway provided');
			}
			$this->_dbTable = $dbTable;
			return $this;
		}
	 
		public function getDbTable()
		{
			if (null === $this->_dbTable) {
				$this->setDbTable('Members_Model_DbTable_MemberList');
			}
			return $this->_dbTable;
		}
		
		public function save(Members_Model_Members $register)
		{
			$auth = Zend_Auth::getInstance ();
			$change_package = ($auth->hasIdentity ()) ? $auth->getIdentity()->allow_change_user_packages : false; 
			if ((null === ($user_id = $register->getUser_id())) || empty($user_id)) 
			{
				unset($data['user_id']);
				
				$data = array(
					'role_id'   	=> 	$register->getRole_id(),
					'title' 		=> 	$register->getTitle(),
					'firstName' 	=>	$register->getFirstName(),
					'lastName' 		=>	$register->getLastName(),
					'username' 		=>	$register->getUsername(),
					'password' 		=>	$register->getPassword(),
					'companyName' 	=>	$register->getCompanyName(),
					'postalCode' 	=>	$register->getPostalCode(),
					'address' 		=>	$register->getAddress(),
					'phone' 		=>	$register->getPhone(),
					'mobile' 		=>	$register->getMobile(),
					'fax' 			=>	$register->getFax(),
					'website'		=>	$register->getWebsite(),
					'state'			=>	$register->getState(),
					'country' 		=>	$register->getCountry(),
					'city' 			=>	$register->getCity(),
					'status' 		=>	$register->getStatus(),
					'salt' 			=>	$register->getSalt(),
					'package_id'	=>	$register->getPackage_id(),
					'register_date' =>	date("Y-m-d H:i:s")
				);
				
				
							
				try
				{
					$id = $this->getDbTable()->insert($data);					
					
					$return = array('status' => 'ok','msg' => $msg, 'id' => $id); 
				}
				catch (Exception $e)
				{
					$msg = $e->getMessage();
					$return = array('status' => 'err','msg' => $msg); 
				}			
			} 
			else 
			{				
					$data = array(
						'role_id'   	=> 	$register->getRole_id(),
						'title' 		=> 	$register->getTitle(),
						'firstName' 	=>	$register->getFirstName(),
						'lastName' 		=>	$register->getLastName(),
						'username' 		=>	$register->getUsername(),					
						'companyName' 	=>	$register->getCompanyName(),
						'postalCode' 	=>	$register->getPostalCode(),
						'address' 		=>	$register->getAddress(),
						'phone' 		=>	$register->getPhone(),
						'mobile' 		=>	$register->getMobile(),
						'fax' 			=>	$register->getFax(),
						'website'		=>	$register->getWebsite(),
						'state'			=>	$register->getState(),
						'country' 		=>	$register->getCountry(),
						'city' 			=>	$register->getCity()					
					);
					if($change_package)
					{
						$data['package_id']	=	$register->getPackage_id();
					}
				
				// Start the Update process
       			try 
				{	
					$this->getDbTable()->update($data, array('user_id = ?' => $user_id));				
					$return = array('status' => 'ok','msg' => $msg, 'id' => $user_id); 
				} 
				catch (Exception $e) 
				{
					$msg = $e->getMessage();
					$return = array('status' => 'err','msg' => $msg); 
				}
			}
			return $return;
		}
	}
?>