<?php
	class Members_Model_Forms
	{
		protected $_id;
		protected $_form_name;
		protected $_entry_by;
		protected $_login_set;
		protected $_db_set;
		protected $_email_set;
		protected $_email_template_set;
		protected $_email_to;
		protected $_email_cc;
		protected $_email_bcc;
		protected $_support_tags;
		protected $_support_attribs;
		protected $_editor_fields;
		protected $_attach_file_path;
		protected $_attach_file_delete;
		protected $_attach_file_type;
		protected $_attach_file_max_size;
		protected $_form_type;
		protected $_group_no;
		protected $_front_desc;
		protected $_meta_title;
		protected $_meta_keywords;
		protected $_meta_desc;
		protected $_label_class;
		protected $_label_style;
		protected $_star_class;
		protected $_star_style;
		protected $_active;
		protected $_captcha_enable;	
		protected $_captcha_field_name;	
		protected $_captcha_word_length;	
		protected $_captcha_width;	
		protected $_captcha_height;	
		protected $_captcha_dot_noise_level;	
		protected $_captcha_line_noise_level;	
		protected $_captcha_gc_freq;	
		protected $_captcha_font_color_r;	
		protected $_captcha_font_color_g;	
		protected $_captcha_font_color_b;	
		protected $_captcha_background_color_r;	
		protected $_captcha_background_color_g;	
		protected $_captcha_background_color_b;	
		protected $_captcha_font;	
		protected $_captcha_font_size;	
		protected $_captcha_img_dir;	
		protected $_captcha_img_url;	
		protected $_captcha_timeout;	
		
	
		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveForms()
		{			
			try
			{
				$mapper  = new Members_Model_FormsMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = $e->getMessage();
			}
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}	
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}	
		public function setForm_name($text)
		{
			$this->_form_name = $text;
			return $this;
		}
		public function setLogin_set($text)
		{
			$this->_login_set = $text;
			return $this;
		}		
		public function setDb_set($text)
		{
			$this->_db_set = $text;
			return $this;
		}
		public function setEmail_set($text)
		{
			$this->_email_set = $text;
			return $this;
		}	
		public function setAttach_file_path($text)
		{
			$this->_attach_file_path = $text;
			return $this;
		}		
		public function setAttach_file_delete($text)
		{
			$this->_attach_file_delete = $text;
			return $this;
		}
		public function setAttach_file_type($text)
		{
			$this->_attach_file_type = $text;
			return $this;
		}
		public function setAttach_file_max_size($text)
		{
			$this->_attach_file_max_size = $text;
			return $this;
		}
		public function setEmail_template_set($text)
		{
			$this->_email_template_set = $text;
			return $this;
		}
		public function setEmail_to($text)
		{
			$global_conf = Zend_Registry::get('global_conf');
			$this->_email_to = ($global_conf['global_email'] == $text ) ? null : $text;
			return $this;
		}	
		public function setEmail_cc($text)
		{
			$this->_email_cc = $text;
			return $this;
		}
		public function setEmail_bcc($text)
		{
			$this->_email_bcc = $text;
			return $this;
		}
		public function setSupport_tags($text)
		{
			$this->_support_tags = $text;
			return $this;
		}
		public function setSupport_attribs($text)
		{
			$this->_support_attribs = $text;
			return $this;
		}
		public function setEditor_fields($text)
		{
			$this->_editor_fields = $text;
			return $this;
		}
		public function setForm_type($text)
		{
			$this->_form_type = $text;
			return $this;
		}
		public function setFront_desc($text)
		{
			$this->_front_desc = trim($text);
			return $this;
		}
		public function setGroup_no($text)
		{
			$this->_group_no = trim($text);
			return $this;
		}	
			
		public function setMeta_title($text)
		{
			$this->_meta_title = $text;
			return $this;
		}	
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords = $text;
			return $this;
		}	
		public function setMeta_desc($text)
		{
			$this->_meta_desc = $text;
			return $this;
		}	
		public function setLabel_class($text)
		{
			$this->_label_class = $text;
			return $this;
		}	
		public function setLabel_style($text)
		{
			$this->_label_style = $text;
			return $this;
		}	
		public function setStar_class($text)
		{
			$this->_star_class = $text;
			return $this;
		}	
		public function setStar_style($text)
		{
			$this->_star_style = $text;
			return $this;
		}	
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}	
		public function setCaptcha_enable($text)
		{
			$this->_captcha_enable = $text;
			return $this;
		}	
		public function setCaptcha_field_name($text)
		{
			$this->_captcha_field_name = $text;
			return $this;
		}	
		public function setCaptcha_word_length($text)
		{
			$this->_captcha_word_length = $text;
			return $this;
		}	
		public function setCaptcha_width($text)
		{
			$this->_captcha_width = $text;
			return $this;
		}	
		public function setCaptcha_height($text)
		{
			$this->_captcha_height = $text;
			return $this;
		}	
		public function setCaptcha_dot_noise_level($text)
		{
			$this->_captcha_dot_noise_level = $text;
			return $this;
		}	
		public function setCaptcha_line_noise_level($text)
		{
			$this->_captcha_line_noise_level = $text;
			return $this;
		}	
		public function setCaptcha_gc_freq($text)
		{
			$this->_captcha_gc_freq = $text;
			return $this;
		}	
		public function setCaptcha_font_color_r($text)
		{
			$this->_captcha_font_color_r = $text;
			return $this;
		}	
		public function setCaptcha_font_color_g($text)
		{
			$this->_captcha_font_color_g = $text;
			return $this;
		}	
		public function setCaptcha_font_color_b($text)
		{
			$this->_captcha_font_color_b = $text;
			return $this;
		}	
		public function setCaptcha_background_color_r($text)
		{
			$this->_captcha_background_color_r = $text;
			return $this;
		}	
		public function setCaptcha_background_color_g($text)
		{
			$this->_captcha_background_color_g = $text;
			return $this;
		}	
		public function setCaptcha_background_color_b($text)
		{
			$this->_captcha_background_color_b = $text;
			return $this;
		}	
		public function setCaptcha_font($text)
		{
			$this->_captcha_font = $text;
			return $this;
		}	
		public function setCaptcha_font_size($text)
		{
			$this->_captcha_font_size = $text;
			return $this;
		}	
		public function setCaptcha_img_dir($text)
		{
			$this->_captcha_img_dir = $text;
			return $this;
		}	
		public function setCaptcha_img_url($text)
		{
			$this->_captcha_img_url = $text;
			return $this;
		}	
		public function setCaptcha_timeout($text)
		{
			$this->_captcha_timeout = $text;
			return $this;
		}	
		
		
		
		public function getId()
		{         
			return $this->_id;
		}	
		
		public function getEntry_by()
		{        
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}     
			return $this->_entry_by;
		}	
		public function getForm_name()
		{
			return $this->_form_name;
		}
		public function getLogin_set()
		{
			return $this->_login_set;
		}	
		public function getDb_set()
		{
			return $this->_db_set;
		}	
		public function getEmail_set()
		{
			return $this->_email_set;
		}
		public function getEmail_to()
		{
			return $this->_email_to;
		}	
		public function getEmail_cc()
		{
			return $this->_email_cc;
		}
		public function getEmail_bcc()
		{
			return $this->_email_bcc;
		}
		public function getSupport_tags()
		{
			return $this->_support_tags;
		}
		public function getSupport_attribs()
		{
			return $this->_support_attribs;
		}
		public function getEditor_fields()
		{
			return $this->_editor_fields;
		}
		public function getEmail_template_set()
		{
			return $this->_email_template_set;
		}
		public function getAttach_file_path()
		{
			return $this->_attach_file_path;
		}	
		public function getAttach_file_delete()
		{
			return $this->_attach_file_delete;
		}	
		public function getAttach_file_type()
		{
			return $this->_attach_file_type;
		}	
		public function getAttach_file_max_size()
		{
			return $this->_attach_file_max_size;
		}		
		public function getForm_type()
		{
			return $this->_form_type;
		}
		public function getGroup_no()
		{
			return $this->_group_no;
		}
		public function getFront_desc()
		{
			return $this->_front_desc;
		}	
		public function getMeta_title()
		{
			return $this->_meta_title;
		}	
		public function getMeta_keywords()
		{
			return $this->_meta_keywords;
		}	
		public function getMeta_desc()
		{
			return $this->_meta_desc;
		}	
		public function getLabel_class()
		{
			return $this->_label_class;
		}	
		public function getLabel_style()
		{
			return $this->_label_style;
		}	
		public function getStar_class()
		{
			return $this->_star_class;
		}	
		public function getStar_style()
		{
			return $this->_star_style;
		}	
		public function getActive()
		{
			return $this->_active;
		}	
		public function getCaptcha_enable()
		{
			return $this->_captcha_enable;
		}	
		public function getCaptcha_field_name()
		{
			return $this->_captcha_field_name;
		}	
		public function getCaptcha_word_length()
		{
			return $this->_captcha_word_length;
		}	
		public function getCaptcha_width()
		{
			return $this->_captcha_width;
		}	
		public function getCaptcha_height()
		{
			return $this->_captcha_height;
		}	
		public function getCaptcha_dot_noise_level()
		{
			return $this->_captcha_dot_noise_level;
		}	
		public function getCaptcha_line_noise_level()
		{
			return $this->_captcha_line_noise_level;
		}	
		public function getCaptcha_gc_freq()
		{
			return $this->_captcha_gc_freq;
		}	
		public function getCaptcha_font_color_r()
		{
			return $this->_captcha_font_color_r;
		}	
		public function getCaptcha_font_color_g()
		{
			return $this->_captcha_font_color_g;
		}	
		public function getCaptcha_font_color_b()
		{
			return $this->_captcha_font_color_b;
		}	
		public function getCaptcha_background_color_r()
		{
			return $this->_captcha_background_color_r;
		}	
		public function getCaptcha_background_color_g()
		{
			return $this->_captcha_background_color_g;
		}	
		public function getCaptcha_background_color_b()
		{
			return $this->_captcha_background_color_b;
		}	
		public function getCaptcha_font()
		{
			return $this->_captcha_font;
		}	
		public function getCaptcha_font_size()
		{
			return $this->_captcha_font_size;
		}	
		public function getCaptcha_img_dir()
		{
			return $this->_captcha_img_dir;
		}	
		public function getCaptcha_img_url()
		{
			return $this->_captcha_img_url;
		}	
		public function getCaptcha_timeout()
		{
			return $this->_captcha_timeout;
		}	
	}
?>