<?php
class Members_Model_FormValueListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Members_Model_DbTable_General');
        }
        return $this->_dbTable;
    }
   	
	
	public function fetchAll($pageNumber,$form_id = null)
    {				
        $resultSet = $this->getDbTable()->getAllValues($form_id); 	      
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);		
        return $paginator;
    }	
	
	public function fetchAllPublished($pageNumber,$form_id = null)
    {				
        $resultSet = $this->getDbTable()->getAllPublishedValues($form_id, '1'); 	      
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);		
        return $paginator;
    }
	
	public function fetchAllPublishedOrder($pageNumber,$form_id = null,$published,$order,$sort)
    {				
        $resultSet = $this->getDbTable()->getAllPublishedValuesOrder($form_id, $published,$order,$sort); 	      
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);		
        return $paginator;
    }
}
?>