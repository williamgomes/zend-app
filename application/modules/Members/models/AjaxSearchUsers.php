<?php
/****************Will Be Deprecated in version 1.9.3 Start**********************/

/*class Members_Model_AjaxSearchUsers 
{
	protected $_user_id;
    protected $_status;
   	protected $_mapper;
	protected $_lastName;
    protected $_title;
    protected $_firstName;
	protected $_username;
	
	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
	
	public function __set($name, $value)
    {
        $method = 'set'.$name;
        if (('mapper' == $name) || !method_exists($this, $method)) 
		{
            throw new Exception('Invalid Profile property');
        }
        $this->$method($value);
    }
	
	public function saveDatas()
    {
		$this->getMapper()->save($this);
	}
	
	public function delDatas()
    {
		$this->getMapper()->delete($this);
	}
	
	public function findDatas($values)
    {
		return $this->getMapper()->find($values,$this);
	}
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method))
		{
            throw new Exception('Invalid Profile property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
		{
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
			{
                $this->$method($value);
            }
        }
        return $this;
    }
	
	public function setMapper(Members_Model_AjaxSearchUsersMapper $mapper)
    {
       
       	if($mapper != null)
		{       
        	$this->_mapper = $mapper;
		}
		else
		{
			$this->_mapper = new Members_Model_AjaxSearchUsersMapper();
		}
    }
	
	public function getMapper()
    {
		if($this->_mapper != null)
		{       
        	return $this->_mapper;
		}
		else
		{
			$this->_mapper = new Members_Model_AjaxSearchUsersMapper();
			return $this->_mapper;
		}
    }
	
	public function setUser_id($text)
    {
        $this->_user_id = $text;
        return $this;
    }
	
	public function setStatus($text)
    {
        $this->_status = $text;
        return $this;
    }
	
	public function setLastName($text)
    {
        $this->_lastName = $text;
        return $this;
    }
	
	public function setTitle($text)
    {
        $this->_title = $text;
        return $this;
    }
	
	public function setFirstName($text)
    {
        $this->_firstName = $text;
        return $this;
    }
	
	public function setUsername($text)
    {
        $this->_username = $text;
        return $this;
    }
	
	
	
	public function getUser_id()
    {
        return $this->_user_id;
    }
	
	public function getLastName()
    {
        return $this->_lastName;
    }
	
	public function getTitle()
    {
        return $this->_title;
    }
	
	public function getFirstName()
    {
        return $this->_firstName;
    }
	
	public function getStatus()
    {
        return $this->_status;
    }
	
	public function getUsername()
    {
        return $this->_username;
    }		
}*/

/****************Will Be Deprecated in version 1.9.3 End**********************/
?>