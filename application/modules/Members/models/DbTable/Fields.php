<?php
/**
* This is the DbTable class for the forms_fields table.
*/
class Members_Model_DbTable_Fields extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'forms_fields';		
	protected $_cols	=	null;	
	
	//Get Datas
	public function getInfo($field_id) 
    {
        $field_id = (int)$field_id;
        $row = $this->fetchRow('id = ' . $field_id);
        if (!$row) 
		{
			$options = null;
           // throw new Exception("Count not find row $user_id");
        }
		else
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return  $options;   
    }
	
	//Get Datas
	public function getFieldsInfo($form_id,$group_name = null) 
    {
		if(empty($group_name))
		{
			 $select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('form_id = ?',$form_id)
						   ->order('group_order ASC')
						   ->order('field_order ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('form_id = ?',$form_id)
						   ->where('field_group = ?',$group_name)
						   ->order('group_order ASC')
						   ->order('field_order ASC');

		}
        $options = $this->fetchAll($select);
        return $options;  
    }
	
	//Get Datas By Field Name
	public function getFieldIdByFieldName($form_id = null,$field_name) 
    {
		if(empty($form_id))
		{
			$select = $this->select()
					   ->from($this->_name, array('*'))
					   ->where('field_name = ?',$field_name);
		}
		else
		{
		 $select = $this->select()
					   ->from($this->_name, array('*'))
					   ->where('form_id = ?',$form_id)
					   ->where('field_name = ?',$field_name)
					   ->limit(1);
		}
        $options = $this->fetchAll($select);
        return $options;  
    }	
	
	//Get Group Name
	public function getGroupNames($form_id) 
    {
		if(!empty($form_id))
		{
			try
			{
				 $select = $this->select()
							   ->from($this->_name, array('field_group' => 'DISTINCT(field_group)'))
							   ->where('form_id = ?',$form_id)
							   ->order('group_order ASC');
				$options = $this->fetchAll($select);
				if(!$options)
				{
					$options = null;
				}
			}
			catch(Exception $e)
			{
				$options = null;
			}	
		}
		else
		{
			$options = null;
		}	
        return $options;  
    }
	
	public function fieldValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'form_id',
						'exclude' => 'group_order = 1 and  field_order = 1',
					)
				);
		return $validator;
	}
	
	public function fieldNameValidator($form_id = null)
	{
		if(empty($form_id))
		{
			$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => $this->_name,
							'field' => 'field_name'
						)
					);
		}
		else
		{
			$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => $this->_name,
							'field' => 'field_name',
							'exclude' => 'form_id = '.$form_id,
						)
					);
		}
		return $validator;
	}
	
	public function getPriceFieldInfoByFormId($form_id) 
    {
         $select = $this->select()
                       ->from($this->_name, array('id','field_name'))
					   ->where('form_id = ?',$form_id)
					   ->where('group_order = ?','1')
					   ->where('field_order = ?','2')
                       ->order('field_order ASC')
					   ->limit(1);
        $options = $this->fetchAll($select);
        return $options;  
    }
	
	public function getFieldInfoByFormId($form_id) 
    {
         $select = $this->select()
                       ->from($this->_name, array('id','field_name'))
					   ->where('form_id = ?',$form_id)
					   ->where('group_order = ?','1')
					   ->where('field_order = ?','1')
                       ->order('field_order ASC')
					   ->limit(1);
        $options = $this->fetchAll($select);
        return $options;  
    }
	
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null)
	{	
		$forms_fields_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['forms_fields'] && is_array($tableColumns['forms_fields'])) ? $tableColumns['forms_fields'] : array('fg.*');
        $dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('ff' => $this->_name), $forms_fields_column_arr);
			   
			
		
		if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("ff.id ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("ff.id ASC"); 	 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{		
			$options = null;	
		}
        return $options; 
	}	
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ff.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
}

?>