<?php
/**
* This is the DbTable class for the forms_general table.
*/
class Members_Model_DbTable_General extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'forms_general';			
	protected $_cols	=	null;
	
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
			$options = null;
           // throw new Exception("Count not find row $id");
        }
		else
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return  $options;   
    }
	
	//Get Datas
	public function getFieldsInfo($form_id,$group_name = null) 
    {
		if(empty($group_name))
		{
			 $select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('form_id = ?',$form_id)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('form_id = ?',$form_id)
						   ->where('field_group = ?',$group_name)
						   ->order('id ASC');

		}
        $options = $this->fetchAll($select);
        return $options;  
    }
	
	//Check Value Available
	public function tableValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'form_id',
						'exclude' => array('field' => 'active', 'value' => '0')
					)
				);
		return $validator;
	}
	
	//Check Table Id Available
	public function tableIdValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'id',
						'exclude' => array('field' => 'active', 'value' => '0')
					)
				);
		return $validator;
	}	
	
	//Get All Values
	public function getAllValues($form_id = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
       	if(!empty($form_id))
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('gf' => $this->_name),array('gf.*'))
							   ->where("gf.form_id = ?",$form_id)
							   ->where("gf.user_id = ?",$user_id)
							   ->order('gf.id ASC');
			}
			else
			{
				$select = $this->select()
							   ->from(array('gf' => $this->_name),array('gf.*'))
							   ->where("gf.form_id = ?",$form_id)
							   ->order('gf.id ASC'); 
			}	 
		}
		else
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('gf' => $this->_name),array('gf.*'))
							   ->where("gf.user_id = ?",$user_id)
							   ->order('gf.id ASC');
			}
			else
			{
			
			$select = $this->select()
						   ->from(array('gf' => $this->_name),array('gf.*'))
						   ->order('gf.id ASC'); 
			}	 		
		}
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
			 $options = null;
          	//throw new Exception("Count not find row Forms");
        }
				
        return $options;    
    }
	
	//Get All Values
	public function getAllPublishedValues($form_id = null,$publish = '1') 
    {
       	if(!empty($form_id))
		{
			$select = $this->select()
						   ->from(array('gf' => $this->_name),array('gf.*'))
						   ->where("gf.form_id = ?",$form_id)
						   ->where("gf.active = ?",$publish)
						   ->order('gf.id ASC'); 	 
		}
		else
		{
			$select = $this->select()
						   ->from(array('gf' => $this->_name),array('gf.*'))
						   ->where("gf.active = ?",$publish)
						   ->order('gf.id ASC'); 	 		
		}
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
			 $options = null;
          	//throw new Exception("Count not find row Forms");
        }
				
        return $options;    
    }
	
	//Get All Values
	public function getAllPublishedValuesOrder($form_id = null,$publish = '1',$order,$sort) 
    {
       	if(!empty($form_id))
		{
			$select = $this->select()
						   ->from(array('gf' => $this->_name),array('gf.*'))
						   ->where("gf.form_id = ?",$form_id)
						   ->where("gf.active = ?",$publish)
						   ->order('gf.'.$order.' '.$sort); 	 
		}
		else
		{
			$select = $this->select()
						   ->from(array('gf' => $this->_name),array('gf.*'))
						   ->where("gf.active = ?",$publish)
						   ->order('gf.'.$order.' '.$sort); 	 		
		}
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
			 $options = null;
          	//throw new Exception("Count not find row Forms");
        }
				
        return $options;    
    }
	
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null)
	{
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;	
		
		$general_page_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['forms_general'] && is_array($tableColumns['forms_general'])) ? $tableColumns['forms_general'] : array('fg.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$user_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_country'] && is_array($tableColumns['user_country'])) ? $tableColumns['user_country'] : null;
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('fg' => $this->_name), $general_page_column_arr);
						   
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'fg.user_id = up.user_id', $user_profile_column_arr);				   
		}
		
		if (($user_country_column_arr &&  is_array($user_country_column_arr) && count( $user_country_column_arr) > 0)) 
		{
			$select->joinLeft(array('ocut' =>  Zend_Registry::get('dbPrefix').'countries'), 'ocut.id = up.country', $user_country_column_arr);
		}

		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('fg.user_id = ' . $user_id );
		}
		
		if($approve != null)
		{
			$select->where("fg.active = ?", $approve);
		}
		
		
		if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("fg.id ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("fg.id ASC"); 	 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if($options)
		{
			$fields_db = new Members_Model_DbTable_Fields();
			$field_search_params['filter']['logic'] = ($field_search_params['filter']['logic']) ? $field_search_params['filter']['logic'] : 'or';
			foreach($options as $options_arr)
			{
				$field_search_params['filter']['filters'][] = array('field' => 'form_id', 'operator' => 'eq', 'value' => $options_arr['form_id']);				
			}
			$fields_info = $fields_db->getListInfo(null, $field_search_params,  array('forms_fields' => array('id' => 'ff.id', 'field_form_id' => 'ff.form_id', 'field_name' => 'ff.field_name')));
			
			$fields_value_db = new Members_Model_DbTable_FieldsValue();
			$fields_value_info = $fields_value_db->getListInfo(null, $field_search_params,  array('forms_fields_values' => array('id' => 'ffv.id', 'table_id' => 'ffv.table_id', 'form_id' => 'ffv.form_id', 'field_id' => 'ffv.field_id', 'field_value' => 'ffv.field_value')));
			
			$options = $options->toArray();
			foreach($options as $options_arr_key => $options_arr)
			{
				if($fields_info)
				{
					foreach($fields_info as $fields_info_arr_key => $fields_info_arr)
					{
						if($options_arr['form_id'] == $fields_info_arr['field_form_id'])
						{
							if($fields_value_info)
							{
								foreach($fields_value_info as $fields_value_info_key => $fields_value_info_arr)
								{
									if($fields_info_arr['id']  == $fields_value_info_arr['field_id'] && $options_arr['form_id']  == $fields_value_info_arr['form_id'] && $options_arr['id']  == $fields_value_info_arr['table_id'])
									{
										$options[$options_arr_key][$fields_info_arr['field_name']] = $fields_value_info_arr['field_value'];
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			$options = null;	
		}
        return $options; 
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'fg.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(fg.g_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(fg.g_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(fg.g_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	} 	
}

?>