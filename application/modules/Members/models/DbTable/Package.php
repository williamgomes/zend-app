<?php
class Members_Model_DbTable_Package extends Eicra_Abstract_DbTable
{
    protected $_name = 'membership_packages';
    public function getPackagesByPid ($product_id , $active = true)
    {
        try {
            $select = $this->select()
                ->from($this->_name, array('*'))
                ->where('id =?', $product_id)
                ->order('price ASC');
            if ($active) {
                $select->where('active = ?', '1') ;
            }
            $options = $this->fetchAll($select)->toArray();
            if ($options) {
                return $options;
            } else {
                return null;
            }
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    public function getPackageById ($id)
    {
        try {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if ($row) {
                $options = $row->toArray();
                $options = is_array($options) ? array_map('stripslashes',
                        $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    public function getAllPackages ($active = true)
    {
        $select = $this->select()
            ->
        from($this->_name, array('*'))
            ->
        order('price ASC');
        if ($active){
            $select->where('active = ?', '1');
        }

        $options = $this->fetchAll($select)->toArray();
        return $options;
    }
    public function getInactiveLeadsByUserId ($user_id)
    {
        $select = $this->select()
            ->
        from($this->_name, array('*'))
            ->where('user_id =?', $user_id)
            ->where('active = ?', '0')
            ->order('id ASC');
        $options = $this->getAdapter()->fetchAll($select);
        return $options;
    }

    public function getIDByUser($user_id)
    {
        $select = $this->select()
        ->
        from($this->_name, array('id'))
        ->where('user_id =?', $user_id)
        ->order('id ASC');
        $options = $this->getAdapter()->fetchRow($select);
        return $options;
    }




    public function getPackagesPairs()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'name'))
					   ->where('active = ?','1')
                       ->order('id ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }

    // Get All Datas
    public function getListInfo ($approve = null, $search_params = null, $userChecking = true)
    {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $hasChild = (is_array($search_params) && array_key_exists('hasChild', $search_params)) ? $search_params['hasChild'] : true;
        $select = $this->select()
        ->setIntegrityCheck(false)
        ->from(array('nc' => $this->_name), array('nc.*'))
        ->joinLeft(
            array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'),
            'nc.user_id = up.user_id', array('username' => 'up.username'))
            ->joinLeft(
                array('gr' => Zend_Registry::get('dbPrefix') . 'b2b_group'),
                'gr.id = nc.group_id', array('group_name' => 'gr.group_name'));
        if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('nc.user_id = ?', $user_id);
        }
        if ($approve != null) {
            $select->where("nc.active = ?", $approve);
        }
        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    $select->order(
                        $sort_value_arr['field'] . ' ' .
                        $sort_value_arr['dir']);
                }
            } else {
                $select->order("nc.group_id ASC")->order('nc.package_order ASC');
            }
            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {

                        $hasChild = ($filter_obj['field'] == 'parent_id') ? true : $hasChild;
                        $where_arr[$i] = ' ' .
                            $this->getOperatorString($filter_obj);
                        $i ++;
                    } else
                    if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString(
                                $sub_filter_obj);
                            $sub ++;
                        }
                        $where_arr[$i] = ' (' . implode(
                            strtoupper($filter_obj['logic']),
                            $where_sub_arr) . ') ';
                        $i ++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']),
                    $where_arr);
                if (! empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("nc.group_id ASC")->order('nc.package_order ASC');
        }

        if($hasChild === false)
        {
            $select->where("nc.parent_id = ?", '0');
        }

        $options = $this->fetchAll($select);
        if (! $options) {
            $options = null;
        }
        return $options;
    }
    private function getOperatorString ($operator_arr)
    {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'nc.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); // explode('GMT',
            // $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime(
                    $data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] .
                    ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' .
                    $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }
        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name':
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " .
                    $table_prefix . "firstName, ' ', " . $table_prefix .
                    "lastName) ";
                    break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' .
                    $operator_arr['value'] . '%" ';
                    break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                    $operator_arr['value'] . '%" ';
                    break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' .
                    $operator_arr['value'] . '%" ';
                    break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'eqy':
                $operatorString = 'YEAR(m.register_date)' . ' = "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'eqm':
                $operatorString = 'date_format(m.register_date, "%M")' . ' = "' .
                    $operator_arr['value'] . '" ';
                    break;
            case 'eqd':
                $operatorString = 'date_format(m.register_date, "%d")' . ' = "' .
                    $operator_arr['value'] . '" ';
                    break;
            default:
                $operatorString = $operatorFirstPart . ' = "' .
                    $operator_arr['value'] . '" ';
                    break;
        }
        return $operatorString;
    }
}