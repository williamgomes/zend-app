<?php
/**
* This is the DbTable class for the forms table.
*/
class Members_Model_DbTable_Forms extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'forms';		
	protected $_cols	=	null;
	
	//Get Datas
	public function getFormsInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
			$options = null;
            //throw new Exception("Count not find row id");
        }
		else
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return  $options;   
    }
	
	public function getAllForms()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'form_name'))
                       ->order('form_name ASC');
        $options = $this->getAdapter()->fetchPairs($select);
		if(!$options){ $options = null; }
        return $options;
    }	
	
	public function getFormList()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'form_name'))
                       ->order('id ASC');
        $options = $this->fetchAll($select);
        return $options;
    }
	
	public function getFormId($form_name)
    {
        $select = $this->select()
                       ->from($this->_name, array('*'))
                       ->where('form_name = ?',$form_name);
        $options = $this->fetchAll($select);
        return $options;
    }
	
	public function formValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'form_name'
					)
				);
		return $validator;
	}	
	
	public function getDataListInfo($approve = null, $search_params = null,  $tableColumns = null)
	{
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;	
		
		$forms_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['forms'] && is_array($tableColumns['forms'])) ? $tableColumns['forms'] : array('f.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$user_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_country'] && is_array($tableColumns['user_country'])) ? $tableColumns['user_country'] : null;
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		$table_id_arr    =  ($tableColumns['table_id_arr'] && $tableColumns['table_id_arr'][0]) ? $tableColumns['table_id_arr'] : array();
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('f' => $this->_name), $forms_column_arr);
						   
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'f.entry_by = up.user_id', $user_profile_column_arr);				   
		}
		
		if (($user_country_column_arr &&  is_array($user_country_column_arr) && count( $user_country_column_arr) > 0)) 
		{
			$select->joinLeft(array('ocut' =>  Zend_Registry::get('dbPrefix').'countries'), 'ocut.id = up.country', $user_country_column_arr);
		}

		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('f.entry_by = ' . $user_id );
		}
		
		if($approve != null)
		{
			$select->where("f.active = ?", $approve);
		}
		
		
		if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("f.id ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("f.id ASC"); 	 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if($options)
		{
			$fields_db = new Members_Model_DbTable_Fields();
			$field_search_params['filter']['logic'] = ($field_search_params['filter']['logic']) ? $field_search_params['filter']['logic'] : 'or';
			foreach($options as $options_arr)
			{
				$field_search_params['filter']['filters'][] = array('field' => 'form_id', 'operator' => 'eq', 'value' => $options_arr['id']);				
			}
			$fields_info = $fields_db->getListInfo(null, $field_search_params,  array('forms_fields' => array('id' => 'ff.id', 'field_form_id' => 'ff.form_id', 'field_name' => 'ff.field_name')));
			
			$fields_value_db = new Members_Model_DbTable_FieldsValue();
			$fields_value_info = $fields_value_db->getListInfo(null, $field_search_params,  array('forms_fields_values' => array('id' => 'ffv.id', 'table_id' => 'ffv.table_id', 'form_id' => 'ffv.form_id', 'field_id' => 'ffv.field_id', 'field_value' => 'ffv.field_value')));
			
			$options = $options->toArray();
			foreach($options as $options_arr_key => $options_arr)
			{
				if($fields_info)
				{
					foreach($fields_info as $fields_info_arr_key => $fields_info_arr)
					{
						if($options_arr['id'] == $fields_info_arr['field_form_id'])
						{
							if($fields_value_info)
							{
								foreach($fields_value_info as $fields_value_info_key => $fields_value_info_arr)
								{
									if($fields_info_arr['id']  == $fields_value_info_arr['field_id'] && $options_arr['id']  == $fields_value_info_arr['form_id'] &&  in_array($fields_value_info_arr['table_id'], $table_id_arr ))
									{
										$options[$options_arr_key][$fields_info_arr['field_name']] = $fields_value_info_arr['field_value'];										
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			$options = null;	
		}
        return $options; 
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'f.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(f.g_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(f.g_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(f.g_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
}

?>