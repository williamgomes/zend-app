<?php
/**
* This is the DbTable class for the user_profile table.
*/
class Members_Model_DbTable_AjaxSearchUsers extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    = 'user_profile';
	protected $_primary = '';
	
	public function setPrimaryKey($values)
	{
		$this->_primary  = $values['search_by'];
	}
}

?>