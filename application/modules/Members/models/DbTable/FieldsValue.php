<?php
/**
* This is the DbTable class for the forms_fields_values table.
*/
class Members_Model_DbTable_FieldsValue extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'forms_fields_values';		
	protected $_cols	=	null;
	
	//Get Datas
	public function getFieldsValueInfo($members_info,$user_id) 
    {
		if(!empty($user_id))
		{
			if(empty($members_info['form_id']))
			{
				$select = $this->select()
							   ->from($this->_name, array('*'))
							   ->where('table_id = ?',$user_id);	
			}
			else
			{
				$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('table_id = ?',$user_id)
						   ->where('form_id = ?',$members_info['form_id']);
			}	
		}
        $options = $this->fetchAll($select);
		if($options)
		{
			$field_db = new Members_Model_DbTable_Fields();
			foreach($options as $entry)
			{
				$field_info = $field_db->getInfo($entry['field_id']);
				$field_name	= stripslashes($field_info['field_name']);
				$members_info[$field_name] = ($field_info['field_type'] == 'multiCheckbox' || $field_info['field_type'] == 'multiselect') ? explode(',',$entry['field_value']) : $entry['field_value'];
			}
		}
        return $members_info;  
    }
	
	//Get Field Values
	public function getFieldsValue($table_id,$form_id,$field_id) 
    {
		try
		{
			$select = $this->select()
							   ->from($this->_name, array('*'))
							   ->where('table_id = ?',$table_id)
							   ->where('form_id = ?',$form_id)
							   ->where('field_id = ?',$field_id);	
			$options = $this->fetchAll($select);
			
			if(!$options)
			{
				$options = null;
			}		
			 	
		}
		catch(Exception $e)
		{
			$options = null;
		}		
		return $options;		
	}
	
	public function getFieldsValue1($table_id,$form_id) 
    {
		$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('table_id = ?',$table_id)
						   ->where('form_id = ?',$form_id)->order('field_id ASC');	
		$options = $this->fetchAll($select);		
		return $options; 			
	}
	
	//Get Field Values
	public function getFieldsValue2($form_id,$field_id) 
    {
		$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where('form_id = ?',$form_id)
						   ->where('field_id = ?',$field_id);	
		$options = $this->fetchAll($select);		
		return $options; 			
	}
	
	public function fieldValueValidator($form_id,$field_id)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'table_id',
						'exclude' => 'form_id = '.$form_id.' AND field_id = '.$field_id 
					)
				);
		return $validator;
	}
	
	public function valueValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'form_id' 
					)
				);
		return $validator;
	}
	
	public function updateFieldValue($table_id,$form_id,$field_id,$result)
	{
		try
		{
			$data = array('field_value' => $result);			
			$where[]	=	$this->getAdapter()->quoteInto('table_id	=	?',$table_id);
			$where[]	=	$this->getAdapter()->quoteInto('form_id		=	?',$form_id);
			$where[]	=	$this->getAdapter()->quoteInto('field_id	=	?',$field_id);
			$this->update($data,$where);
			$json_arr = array('status' =>'ok');
		}
		catch(Exception $e)
		{
			$json_arr = array('status' =>'err','msg' => $e->getMesssage());
		}
		return $json_arr;
	}
	
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null)
	{	
		$forms_fields_values_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['forms_fields_values'] && is_array($tableColumns['forms_fields_values'])) ? $tableColumns['forms_fields_values'] : array('fg.*');
        $dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('ffv' => $this->_name), $forms_fields_values_column_arr);
			   
			
		
		if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("ffv.id ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("ffv.id ASC"); 	 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{		
			$options = null;	
		}
        return $options; 
	}	
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ffv.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}			
}

?>