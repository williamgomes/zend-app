<?php
/**
* This is the DbTable class for the roles table.
*/
class Members_Model_DbTable_Role extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'roles';	
	
	public function getOptions()
    {
     
	    $select = $this->select()
                       ->from($this->_name, array('role_id', 'role_name'))
					   ->where('allow_register_to_this_role = ?','1')
                       ->order('role_name ASC');
	   
	   
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }
	
	public function getAllOptions()
    {
     
	    $select = $this->select()
                       ->from($this->_name, array('role_id', 'role_name'))
                       ->order('role_name ASC');
	   
	   
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }
	
	//Get Datas
	public function getRoleInfo($role_id) 
    {
        $role_id = (int)$role_id;
        $row = $this->fetchRow('role_id = ' . $role_id);
        if (!$row) 
		{
             return NULL;
        }
		else
		{
        	return $row->toArray();  
		}  
    }
	
	public function getRoleNum($form_id)
    {
        $select = $this->select()
                       ->from($this->_name, array( 'num_role' => 'COUNT(*)'))
                       ->where('form_id = ?',$form_id);
		$rs = $this->fetchAll($select);
		foreach($rs as $row)
		{
			$num_role = $row['num_role'];
		}
        return $num_role;
    }
	
	public function formValidator()
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'role_id',
						'exclude' => array('field' => 'form_id', 'value' => '0')
					)
				);
		return $validator;
	}
}

?>