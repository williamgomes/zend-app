<?php
	class Members_Model_FormsMapper
	{
		protected $_dbTable;
 
		public function setDbTable($dbTable)
		{
			if (is_string($dbTable)) {
				$dbTable = new $dbTable();
			}
			if (!$dbTable instanceof Zend_Db_Table_Abstract) {
				throw new Exception('Invalid table data gateway provided');
			}
			$this->_dbTable = $dbTable;
			return $this;
		}
	 
		public function getDbTable()
		{
			if (null === $this->_dbTable) {
				$this->setDbTable('Members_Model_DbTable_Forms');
			}
			return $this->_dbTable;
		}
		
		public function save(Members_Model_Forms $register)
		{
				 
			if ((null === ($id = $register->getId())) || empty($id)) 
			{				
				try
				{	
					unset($data['id']);
				
					$data = array(
						'form_name'   					=> 	$register->getForm_name(),
						'entry_by' 						=> 	$register->getEntry_by(),
						'login_set' 					=> 	$register->getLogin_set(),
						'db_set' 						=>	$register->getDb_set(),
						'email_set' 					=>	$register->getEmail_set(),
						'email_template_set' 			=>	$register->getEmail_template_set(),
						'email_to' 						=>	$register->getEmail_to(),
						'email_cc' 						=>	$register->getEmail_cc(),
						'email_bcc' 					=>	$register->getEmail_bcc(),
						'support_tags' 					=>	$register->getSupport_tags(),
						'support_attribs'				=>	$register->getSupport_attribs(),
						'editor_fields' 				=>	$register->getEditor_fields(),
						'attach_file_path' 				=>	$register->getAttach_file_path(),
						'attach_file_delete'			=>	$register->getAttach_file_delete(),
						'attach_file_type' 				=>	$register->getAttach_file_type(),
						'attach_file_max_size'			=>	$register->getAttach_file_max_size(),
						'form_type' 					=>	$register->getForm_type(),
						'group_no' 						=>	$register->getGroup_no(),
						'front_desc' 					=>	$register->getFront_desc(),
						'meta_title' 					=>	$register->getMeta_title(),
						'meta_keywords' 				=>	$register->getMeta_keywords(),
						'meta_desc' 					=>	$register->getMeta_desc(),
						'label_class' 					=>	$register->getLabel_class(),
						'label_style' 					=>	$register->getLabel_style(),
						'star_class' 					=>	$register->getStar_class(),
						'star_style' 					=>	$register->getStar_style(),
						'active'						=>	$register->getActive(),
						'captcha_enable'				=>	$register->getCaptcha_enable(),
						'captcha_field_name'			=>	$register->getCaptcha_field_name(),
						'captcha_word_length'			=>	$register->getCaptcha_word_length(),
						'captcha_width'					=>	$register->getCaptcha_width(),
						'captcha_height'				=>	$register->getCaptcha_height(),
						'captcha_dot_noise_level'		=>	$register->getCaptcha_dot_noise_level(),
						'captcha_line_noise_level'		=>	$register->getCaptcha_line_noise_level(),
						'captcha_gc_freq'				=>	$register->getCaptcha_gc_freq(),
						'captcha_font_color_r'			=>	$register->getCaptcha_font_color_r(),
						'captcha_font_color_g'			=>	$register->getCaptcha_font_color_g(),
						'captcha_font_color_b'			=>	$register->getCaptcha_font_color_b(),
						'captcha_background_color_r'	=>	$register->getCaptcha_background_color_r(),
						'captcha_background_color_g'	=>	$register->getCaptcha_background_color_g(),
						'captcha_background_color_b'	=>	$register->getCaptcha_background_color_b(),
						'captcha_font'					=>	$register->getCaptcha_font(),
						'captcha_font_size'				=>	$register->getCaptcha_font_size(),
						'captcha_img_dir'				=>	$register->getCaptcha_img_dir(),
						'captcha_img_url'				=>	$register->getCaptcha_img_url(),
						'captcha_timeout'				=>	$register->getCaptcha_timeout()
					);
					$last_id = $this->getDbTable()->insert($data);				
					$return = array('status' => 'ok' ,'id' => $last_id);
				}
				catch(Exception $e)
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}			
			} 
			else 
			{
				try 
				{
					$data = array(
						'form_name'   				=> 	$register->getForm_name(),
						'db_set' 					=>	$register->getDb_set(),
						'login_set' 				=> 	$register->getLogin_set(),
						'email_set' 				=>	$register->getEmail_set(),
						'email_template_set' 		=>	$register->getEmail_template_set(),
						'email_to' 					=>	$register->getEmail_to(),
						'email_cc' 					=>	$register->getEmail_cc(),
						'email_bcc' 				=>	$register->getEmail_bcc(),
						'support_tags' 				=>	$register->getSupport_tags(),
						'support_attribs'			=>	$register->getSupport_attribs(),
						'editor_fields' 			=>	$register->getEditor_fields(),
						'attach_file_path' 			=>	$register->getAttach_file_path(),
						'attach_file_delete'		=>	$register->getAttach_file_delete(),
						'attach_file_type' 			=>	$register->getAttach_file_type(),
						'attach_file_max_size'		=>	$register->getAttach_file_max_size(),
						'form_type' 				=>	$register->getForm_type(),
						'group_no' 					=>	$register->getGroup_no(),
						'front_desc' 				=>	$register->getFront_desc(),
						'meta_title' 				=>	$register->getMeta_title(),
						'meta_keywords' 			=>	$register->getMeta_keywords(),
						'meta_desc' 				=>	$register->getMeta_desc(),
						'label_class' 				=>	$register->getLabel_class(),
						'label_style' 				=>	$register->getLabel_style(),
						'star_class' 				=>	$register->getStar_class(),
						'star_style' 				=>	$register->getStar_style(),
						'captcha_enable'				=>	$register->getCaptcha_enable(),
						'captcha_field_name'			=>	$register->getCaptcha_field_name(),
						'captcha_word_length'			=>	$register->getCaptcha_word_length(),
						'captcha_width'					=>	$register->getCaptcha_width(),
						'captcha_height'				=>	$register->getCaptcha_height(),
						'captcha_dot_noise_level'		=>	$register->getCaptcha_dot_noise_level(),
						'captcha_line_noise_level'		=>	$register->getCaptcha_line_noise_level(),
						'captcha_gc_freq'				=>	$register->getCaptcha_gc_freq(),
						'captcha_font_color_r'			=>	$register->getCaptcha_font_color_r(),
						'captcha_font_color_g'			=>	$register->getCaptcha_font_color_g(),
						'captcha_font_color_b'			=>	$register->getCaptcha_font_color_b(),
						'captcha_background_color_r'	=>	$register->getCaptcha_background_color_r(),
						'captcha_background_color_g'	=>	$register->getCaptcha_background_color_g(),
						'captcha_background_color_b'	=>	$register->getCaptcha_background_color_b(),
						'captcha_font'					=>	$register->getCaptcha_font(),
						'captcha_font_size'				=>	$register->getCaptcha_font_size(),
						'captcha_img_dir'				=>	$register->getCaptcha_img_dir(),
						'captcha_img_url'				=>	$register->getCaptcha_img_url(),
						'captcha_timeout'				=>	$register->getCaptcha_timeout()					
					);			
					
					// Start the Update process
       				
					$this->getDbTable()->update($data, array('id = ?' => $id));				
					$return = array('status' => 'ok' ,'id' => $id);
				} 
				catch (Exception $e) 
				{
					$return = array('status' => 'err' ,'msg' => $e->getMessage());
				}
			}
			return $return;
		}
	}
?>