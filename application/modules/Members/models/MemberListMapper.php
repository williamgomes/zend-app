<?php
class Members_Model_MemberListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Members_Model_DbTable_MemberList');
        }
        return $this->_dbTable;
    }
   	
	
	public function fetchAll($pageNumber, $approve = null, $search_params = null)
    {
        $resultSet = $this->getDbTable()->getMemberList($approve, $search_params);       
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }	
	
	public function fetchAllPackage($pageNumber)
    {
        $resultSet = $this->getDbTable()->fetchAll(); 
		$role_db = new Members_Model_DbTable_Role();
		$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
		$result_arr = array();
		$i = 0;
		foreach($resultSet as $results)
		{	
			$role_info = $role_db->getRoleInfo($results['role_id']);
			
			if(!empty($role_info['form_id']))
			{
				$results_arr = $results->toArray();
				$results_arr['form_id'] = $role_info['form_id'];
				$result_arr[$i]	=	$dynamic_valu_db->getFieldsValueInfo($results_arr,$results['user_id']);			
				$i++;
			}
		}     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($result_arr);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
}
?>