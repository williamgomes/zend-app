<?php
/****************Will Be Deprecated in version 1.9.3 Start**********************/

/*class Members_Model_AjaxSearchUsersMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Members_Model_DbTable_AjaxSearchUsers');
        }
        return $this->_dbTable;
    }
   	
	
	
	public function find($values, Members_Model_AjaxSearchUsers $datas)

    {		
		// Loading the Zend_Db_Adapter 
		
		$DbAdapter = $this->getDbTable()->getAdapter();
		
		if(!empty($values['searchKey']))
		{
			// Making the user given string into Secured SQL friendly String.  
			$libApi = new Eicra_Lib_Api();
			$searchKey = $libApi->ToDBString($values['searchKey'],'',false);
			
			// Building the Where Cluse based on Like Operator 
			
			switch($values['search_by'])
			{
				case "firstName":
					$where = " m.firstName LIKE '%".$searchKey."%' ";
				break;
				case "lastName":
					$where = " m.lastName LIKE '%".$searchKey."%' ";
				break;
				case "user_id":
					$where = " m.user_id = '".$searchKey."' ";
				break;
				case "username":
					$where = " m.username LIKE '".$searchKey."%' ";
				break;
			}
		}
		
		if(!empty($values['search_year']))
		{
			if(!empty($where))
			{
				$where .= " AND YEAR(m.register_date) = '".$values['search_year']."' ";
			}
			else
			{
				$where .= " YEAR(m.register_date) = '".$values['search_year']."' ";
			}
		}
		
		if(!empty($values['search_month']))
		{
			if(!empty($where))
			{
				$where .= " AND date_format(m.register_date,'%M') = '".$values['search_month']."' ";
			}
			else
			{
				$where .= " date_format(m.register_date,'%M') = '".$values['search_month']."' ";
			}
		}
		
		if(!empty($values['search_day']))
		{
			if(!empty($where))
			{
				$where .= " AND date_format(m.register_date,'%d') = '".$values['search_day']."' ";
			}
			else
			{
				$where .= " date_format(m.register_date,'%d') = '".$values['search_day']."' ";
			}
		}
		
		
		if(!empty($values['role_id']))
		{
			if(!empty($where))
			{
				$where .= " AND m.role_id = '".$values['role_id']."' ";
			}
			else
			{
				$where .= " m.role_id = '".$values['role_id']."' ";
			}
		}
				
		// Getting BD Prefix 
        $DbPrefix =  Zend_Registry::get('dbPrefix'); 
		
		// Start using the Zend_DB_Select Object 
		$dbSelect = new Zend_Db_Select($DbAdapter);
		
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		// Making the SQL Query string. 
		if(!empty($where))
		{
			$selectQuery = $DbAdapter->select()
										->from( array('m' => $DbPrefix.'user_profile'), array('*') )
										->where($where)										
										->joinLeft(array('r' => Zend_Registry::get('dbPrefix').'roles'), 'm.role_id = r.role_id')
										->where('r.role_lock =? ', '1');
		}
		else
		{
			$selectQuery = $DbAdapter->select()
										->from( array('m' => $DbPrefix.'user_profile'), array('*') )										
										->joinLeft(array('r' => Zend_Registry::get('dbPrefix').'roles'), 'm.role_id = r.role_id')
										->where('r.role_lock =? ', '1')
							   			->orwhere('r.role_id = ? ', $role_id);
		}
		
		// Making an Statement object through DbAdapter. 
		$stmt = $DbAdapter->query($selectQuery);		
		$result =  $stmt->fetchAll();
		
        if (0 == count($result)) 
		{
            return;
        }
		else
		{
			foreach ($result as $row) 
			{				
				$content[] = array( 'user_id' => $row['user_id'],'role_id' => $row['role_id'], 'firstName' => $row['firstName'],'lastName' => $row['lastName'],'username' => $row['username'],'companyName' => $row['companyName'],'register_date' => $row['register_date'],'status' => $row['status'],'last_access' => $row['last_access']);
			}				
			
		}		
		return $content;
    }	
}*/

/****************Will Be Deprecated in version 1.9.3 End**********************/
?>