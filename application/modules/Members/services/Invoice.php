<?php
class Members_Service_Invoice
{
	protected $_invoice_obj;
	protected $_calendar_data;
	protected $_translator;
	
	public function __construct($invoice_obj = null)
	{	
		$this->_invoice_obj = $invoice_obj;
		$this->_translator = Zend_Registry::get('translator');
	}
	
	
	
	public function createItinerary()
	{		
	}
	
	public function deleteItinerary()
	{
		try
		{
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	}
	
	public function paidItinerary()
	{
		try
		{	
			$invoice_db = new Invoice_Model_DbTable_Invoices();		
			$mem_db = new Members_Model_DbTable_MemberList();				
			$invoice_info	= $invoice_db->getInfo($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);		
			$mem_info	=	($invoice_info) ? $mem_db->getMemberInfo($invoice_info['user_id']) : null;
			
			if($invoice_info && $invoice_info['user_id'])
			{
				$mem_db->update(array('status' => '1'), array('user_id = ?' => $invoice_info['user_id']));
			}
			
			
			// Extra START
			//$email_class = new Invoice_View_Helper_Email();
			//$invoice_later = $email_class->generateInvoiceLater($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);				
			// Extra END
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	}
	
	public function unpaidItinerary()
	{
		try
		{	
			$invoice_db = new Invoice_Model_DbTable_Invoices();		
			$mem_db = new Members_Model_DbTable_MemberList();				
			$invoice_info	= $invoice_db->getInfo($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);		
			$mem_info	=	($invoice_info) ? $mem_db->getMemberInfo($invoice_info['user_id']) : null;
			
			if($invoice_info && $invoice_info['user_id'])
			{
				$mem_db->update(array('status' => '0'), array('user_id = ?' => $invoice_info['user_id']));
			}
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	}
	
	public function cancelItinerary()
	{
		try
		{	
			$invoice_db = new Invoice_Model_DbTable_Invoices();		
			$mem_db = new Members_Model_DbTable_MemberList();				
			$invoice_info	= $invoice_db->getInfo($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);		
			$mem_info	=	($invoice_info) ? $mem_db->getMemberInfo($invoice_info['user_id']) : null;
			
			if($invoice_info && $invoice_info['user_id'])
			{
				$mem_db->update(array('status' => '0'), array('user_id = ?' => $invoice_info['user_id']));
			}
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	}
}
?>