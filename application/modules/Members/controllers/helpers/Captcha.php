<?php
//Members Captcha Helpers Class
class Members_Controller_Helper_Captcha
{
	public static function getCaptchaElement($viewForm)
	{
		try
		{
			$captcha = array();
			$elements = $viewForm->getElements();
			foreach($elements as $element)
			{
				if($element->getType()  == 'Zend_Form_Element_Captcha')
				{	
					$element->setDecorators(array(	));	
					$captcha = array('status' => 'ok',  'captcha_field' => $element->render() , 'id' => $element->getCaptcha()->getId(), 'ImgUrl' => $element->getCaptcha()->getImgUrl(), 'Suffix' => $element->getCaptcha()->getSuffix(), 'captcha_name' => $element->getName(), 'DotNoiseLevel' => $element->getCaptcha()->getDotNoiseLevel(), 'LineNoiseLevel' => $element->getCaptcha()->getLineNoiseLevel(), 'GcFreq' => $element->getCaptcha()->getGcFreq());				
					break;
				}
			}
		}
		catch(Exception $e)
		{
			$captcha = array('status' => 'err', 'msg' => $e->getMessage());	
		}
		return $captcha;
	}
}
?>