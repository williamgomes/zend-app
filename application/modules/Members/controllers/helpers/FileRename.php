<?php
class Members_Controller_Helper_FileRename
{		
	public static function fileRename(&$fileObj, $path) //Reference file object &$fileObj
	{
		try
		{
			$fileObj->setDestination($path);	
			$global_conf = Zend_Registry::get('global_conf');
			if($global_conf['image_renaming_enable'] == '1')
			{		
				$fileInfo 				=		pathinfo($fileObj->getFileName());			
				$newFileName			=		time().'_'.str_replace(' ', '_', $fileInfo['filename']).'.'.$fileInfo['extension'];			
				$fileObj->addFilter('Rename', $newFileName);
			}			
		}
		catch(Exception $e)
		{
			file_put_contents('t.txt',$e->getMessage());
		}
    }
}
?>