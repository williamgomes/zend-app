<?php
//Members Registers Helpers Class
class Members_Controller_Helper_Registers
{
	public function getUsernameAvailable($username,$user_id = null) 
	{
		//DB Connection
		$member_db = new Members_Model_DbTable_MemberList();
		$validator	=	$member_db->memberValidator($user_id);		
        if ($validator->isValid($username)) 
		{
            $return = false;
        }
		else
		{
			$return = true;
		}
		
        return $return; 
	}
	
	public function createSalt ($charno = 8) 
	{
	
	  for ($i = 0; $i < $charno; $i++) 
	  {    
		$num = rand(33, 126); //This is roughly the ascii readable character range
		$salt .= chr($num); //Convert the number to a real character        
	  }    
	  return $salt;    
	}
	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?','register')
								->limit(1);
		}
		else
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.id = ?',$letter_id)
								->limit(1);
		}
							
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$templates_arr = $row ;
			}
		}
		else
		{
			$templates_arr['templates_desc'] = '';
			$templates_arr['templates_title'] = '';
		}			
		foreach($datas as $key=>$value)
		{
			$pattern = '/%'.$key.'%/i';
			if(function_exists('get_magic_quotes_gpc'))
			{
				$templates_arr['templates_desc'] = (!empty($value)  && !is_array($value)) ? preg_replace($pattern, $value, stripslashes($templates_arr['templates_desc'])) : preg_replace($pattern, '&nbsp;', stripslashes($templates_arr['templates_desc']));
				$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? preg_replace($pattern, $value, stripslashes($templates_arr['templates_title'])) : preg_replace($pattern, '&nbsp;', stripslashes($templates_arr['templates_title']));
			}
			else
			{
				$templates_arr['templates_desc'] = (!empty($value)  && !is_array($value)) ? preg_replace($pattern, $value, $templates_arr['templates_desc']) : preg_replace($pattern, '&nbsp;', $templates_arr['templates_desc']);
				$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? preg_replace($pattern, $value, $templates_arr['templates_title']) : preg_replace($pattern, '&nbsp;', $templates_arr['templates_title']);
			}			
		}
		return $templates_arr;
	}
	
	public function attachFiles($mailing,$form_info = null,$attach_file_arr = null)
	{
	  if($attach_file_arr != null)
	  {
		foreach($attach_file_arr as $key => $values)
		{
			if(!empty($values))
			{
				$value_arr = explode(',', $values);
				foreach($value_arr as $value_arr_key => $value)
				{
					$fileType = Eicra_File_Utility::GetExtension($value);				
					$myFile = file_get_contents(BASE_PATH.DS.$form_info['attach_file_path'].DS.$value);
					$at = new Zend_Mime_Part($myFile);
					$at->type        = 'application/'.$fileType;
					$at->disposition = Zend_Mime::DISPOSITION_INLINE;
					$at->encoding    = Zend_Mime::ENCODING_BASE64;
					$at->filename    = $value;			 
					$mailing->addAttachment($at);
				}
			}
		}
	  }
	}
	
	public function sendMail($datas,$form_info = null,$attach_file_arr = null)
	{
		$letter_id = ($form_info) ? $form_info['email_template_set'] : null;
		$email_cc = ($form_info && !empty($form_info['email_cc'])) ? explode(',',$form_info['email_cc']) : null ;
		$email_bcc = ($form_info && !empty($form_info['email_bcc'])) ? explode(',',$form_info['email_bcc']) : null ;
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  $subject = "New Registration From ".$global_conf['site_name'];
		  $datas['site_title'] = $global_conf['site_title'];
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  $templates_arr =  self::getLetterBody($datas,$letter_id);
		  $newsletter = $templates_arr['templates_desc'];
		  if($templates_arr['templates_title'])
		  {
			 $subject =  $templates_arr['templates_title'];
		  }
		 
		  $email_name_admin = ($form_info && !empty($form_info['email_to'])) ? $form_info['email_to'] : $global_conf['global_email'];		 
		  $email_name = $datas['username'];
		  
		  $mailing = new Zend_Mail('UTF-8');
		  $mailing->setMimeBoundary('1.0');
		  self::attachFiles($mailing,$form_info,$attach_file_arr);
		  $mailing->setFrom($email_name, $global_conf['site_name']);
		  $mailing->addTo($email_name_admin, $global_conf['site_name']);
		  if($email_cc)
		  {
			  if(is_array($email_cc))
			  {
				  foreach($email_cc as $email_cc_key => $email_cc_value)
				  {
		  			$mailing->addCc($email_cc_value, '');
				  }
			  }
			  else
			  {
				  $mailing->addCc($email_cc, '');
			  }
		  }
		  if($email_bcc)
		  {
			  if(is_array($email_bcc))
			  {
				  foreach($email_bcc as $email_bcc_key => $email_bcc_value)
				  {
		  			$mailing->addBcc($email_bcc_value);
				  }
			  }
			  else
			  {
				  $mailing->addBcc($email_bcc);
			  }
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);
			$return = true;
		  }
		  catch (Exception $e) 
		  {
		  	try
		  	{
				$mailing->send();
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
		  }      	  		  
		  	 
		  $mailing_user = new Zend_Mail('UTF-8');
		  $mailing_user->setMimeBoundary('1.0');
		  self::attachFiles($mailing_user,$form_info,$attach_file_arr);
		  $mailing_user->setFrom($global_conf['global_email'], $global_conf['site_name']);
		  $mailing_user->addTo($email_name, $global_conf['site_name']);
		  $mailing_user->setSubject($subject);
		  $mailing_user->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing_user->send($transport);
			$return = true;
		  }
		  catch (Exception $e) 
		  {
		  	try
		  	{
				$mailing_user->send();
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
		  }		 
		
		return $return;
	}
	
	public function sendGeneralMail($datas,$form_info = null,$attach_file_arr = null)
	{
		$letter_id = ($form_info) ? $form_info['email_template_set'] : null;
		$email_cc = ($form_info && !empty($form_info['email_cc'])) ? explode(',',$form_info['email_cc']) : null ;
		$email_bcc = ($form_info && !empty($form_info['email_bcc'])) ? explode(',',$form_info['email_bcc']) : null ;
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  $datas['site_title'] = $global_conf['site_title'];
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  $templates_arr =  self::getLetterBody($datas,$letter_id);
		  $newsletter = stripslashes($templates_arr['templates_desc']);
		  $subject = stripslashes($templates_arr['templates_title']);
		 
		  $email_name_admin = ($form_info && !empty($form_info['email_to'])) ? $form_info['email_to'] : $global_conf['global_email'];				  
		  $email_name = $email_name_admin;
		  $mailing = new Zend_Mail('UTF-8');
		  $mailing->setMimeBoundary('1.0');
		  self::attachFiles($mailing,$form_info,$attach_file_arr);
		  $mailing->setFrom(($form_info['set_from_email']) ? $form_info['set_from_email'] : $email_name, $global_conf['site_name']);
		  $mailing->addTo($email_name_admin, $global_conf['site_name']);
		  if($email_cc)
		  {
			  if(is_array($email_cc))
			  {
				  foreach($email_cc as $email_cc_key => $email_cc_value)
				  {
		  			$mailing->addCc($email_cc_value, '');
				  }
			  }
			  else
			  {
				  $mailing->addCc($email_cc, '');
			  }
		  }
		  if($email_bcc)
		  {
			  if(is_array($email_bcc))
			  {
				  foreach($email_bcc as $email_bcc_key => $email_bcc_value)
				  {
		  			$mailing->addBcc($email_bcc_value);
				  }
			  }
			  else
			  {
				  $mailing->addBcc($email_bcc);
			  }
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);
			$return = true;
		  }
		  catch (Exception $e) 
		  {
		  	try
		  	{
				$mailing->send();
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
		  } 		
		return $return;
	}
	
	public function sendEmailToFriend($datas,$form_info = null,$attach_file_arr = null,$to_mail = null, $secondary_email = null)
	{
		$letter_id = ($form_info) ? $form_info['email_template_set'] : null;
		$email_cc = ($form_info && !empty($form_info['email_cc'])) ? explode(',',$form_info['email_cc']) : null ;
		$email_bcc = ($form_info && !empty($form_info['email_bcc'])) ? explode(',',$form_info['email_bcc']) : null ;
		if(!empty($secondary_email))
		{
			$email_bcc_index = count($email_bcc);
			$email_bcc[$email_bcc_index] = $secondary_email;
		} 
		
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  $datas['site_title'] = $global_conf['site_title'];
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  $templates_arr =  self::getLetterBody($datas,$letter_id);
		  $newsletter = stripslashes($templates_arr['templates_desc']);
		  $subject = stripslashes($templates_arr['templates_title']);
		  if($datas[Eicra_File_Constants::FROM_MAIL])
		  {
		  	$email_name_admin = $datas[Eicra_File_Constants::FROM_MAIL];
		  }
		  else
		  {
		  	$email_name_admin = ($form_info && !empty($form_info['email_to'])) ? $form_info['email_to'] : $global_conf['global_email'];	
		  }		  			  
		  $email_name = ($to_mail) ? $to_mail : $email_name_admin;
		  $mailing = new Zend_Mail('utf-8');
		  $mailing->setMimeBoundary('1.0');
		  self::attachFiles($mailing,$form_info,$attach_file_arr);
		  $mailing->setFrom($email_name_admin, $global_conf['site_name']);
		  $mailing->addTo($email_name, $global_conf['site_name']);
		  
		  if($email_cc)
		  {
			  if(is_array($email_cc))
			  {
				  foreach($email_cc as $email_cc_key => $email_cc_value)
				  {
		  			$mailing->addCc($email_cc_value, '');
				  }
			  }
			  else
			  {
				  $mailing->addCc($email_cc, '');
			  }
		  }
		  if($email_bcc)
		  {
			  if(is_array($email_bcc))
			  {
				  foreach($email_bcc as $email_bcc_key => $email_bcc_value)
				  {
		  			$mailing->addBcc($email_bcc_value);
				  }
			  }
			  else
			  {
				  $mailing->addBcc($email_bcc);
			  }
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);			
			$return = array('status' => 'ok');
		  }
		  catch (Exception $e) 
		  {
		  	try
		  	{
				$mailing->send();
				$return = array('status' => 'ok');
			}
			catch (Exception $e) 
			{
				$return = array('status' => 'err','msg' => $e->getMessage());
			}
		  } 		
		return $return;
	}
	
	
	public function sendInvoiceEmail($datas,$email_template_set = null,$attach_file_arr = null,$to_mail = null, $secondary_email_cc = null, $secondary_email_bcc = null)
	{
		$letter_id = ($email_template_set) ? $email_template_set : null;
		$email_cc = (!empty($secondary_email_cc)) ? explode(',',$secondary_email_cc) : null ;
		$email_bcc = (!empty($secondary_email_bcc)) ? explode(',',$secondary_email_bcc) : null ;
		
		
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  $datas['site_title'] = $global_conf['site_title'];
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  $templates_arr =  self::getLetterBody($datas,$letter_id);
		  $newsletter = stripslashes($templates_arr['templates_desc']);
		  $subject = ($templates_arr['templates_title']) ? stripslashes($templates_arr['templates_title']) : stripslashes($datas[Eicra_File_Constants::INVOICE_SUBJECT]);
		  if($datas[Eicra_File_Constants::FROM_MAIL])
		  {
		  	$email_name_admin = $datas[Eicra_File_Constants::FROM_MAIL];
		  }
		  else
		  {
		  	$email_name_admin =  $global_conf['global_email'];	
		  }		  			  
		  $email_name = ($to_mail) ? $to_mail : $email_name_admin;
		  $mailing = new Zend_Mail('utf-8');
		  $mailing->setMimeBoundary('1.0');
		  self::attachFiles($mailing,$form_info,$attach_file_arr);
		  $mailing->setFrom($email_name_admin, $global_conf['site_name']);
		  $mailing->addTo($email_name, $global_conf['site_name']);
		  
		  if($email_cc)
		  {
			  if(is_array($email_cc))
			  {
				  foreach($email_cc as $email_cc_key => $email_cc_value)
				  {
		  			$mailing->addCc($email_cc_value, '');
				  }
			  }
			  else
			  {
				  $mailing->addCc($email_cc, '');
			  }
		  }
		  if($email_bcc)
		  {
			  if(is_array($email_bcc))
			  {
				  foreach($email_bcc as $email_bcc_key => $email_bcc_value)
				  {
		  			$mailing->addBcc($email_bcc_value);
				  }
			  }
			  else
			  {
				  $mailing->addBcc($email_bcc);
			  }
		  }
		  $mailing->setSubject($subject);
		  $mailing->setBodyHtml($newsletter, 'utf-8');
		  
		  try
		  {
		  	$mailing->send($transport);			
			$return = array('status' => 'ok', 'newsletter' => $newsletter);
		  }
		  catch (Exception $e) 
		  {
		  	try
		  	{
				$mailing->send();
				$return = array('status' => 'ok', 'newsletter' => $newsletter);
			}
			catch (Exception $e) 
			{
				$return = array('status' => 'err','msg' => $e->getMessage(), 'newsletter' => $newsletter);
			}
		  } 		
		return $return;
	}
}
?>