<?php
//Members Registers Helpers Class
class Members_Controller_Helper_Packages
{
	public function checkFormAvailable($table_name) 
	{
		//DB Connection
		$form_db = new Members_Model_DbTable_Forms();
		$validator	=	$form_db->formValidator();		
        $return = ($validator->isValid($table_name)) ? true : false;		
        return $return; 
	}
	
	public function checkPackageForm($name)
	{
		$form_name_array = explode('_',$name);
		if(in_array('package',$form_name_array,true))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function checkFieldAvailable($form_id) 
	{
		//DB Connection
		$field_db = new Members_Model_DbTable_Fields();
		$validator	=	$field_db->fieldValidator();		
        $return = ($validator->isValid($form_id)) ? true : false;		
        return $return; 
	}
	
	public function checkFieldNameAvailable($field_name,$form_id) 
	{
		//DB Connection
		$field_db = new Members_Model_DbTable_Fields();
		$validator	=	$field_db->fieldNameValidator($form_id);		
        $return = ($validator->isValid($field_name)) ? true : false;		
        return $return; 
	}
	
	public function checkTableAvailable($form_id) 
	{
		//DB Connection
		$table_db = new Members_Model_DbTable_General();
		$validator	=	$table_db->tableValidator();		
        $return = ($validator->isValid($form_id)) ? true : false;		
        return $return; 
	}
	
	public function checkTableIdAvailable($id) 
	{
		//DB Connection
		$table_db = new Members_Model_DbTable_General();
		$validator	=	$table_db->tableIdValidator();		
        $return = ($validator->isValid($id)) ? true : false;		
        return $return; 
	}
	
	public function getFormDatas($name)
	{
		$form_db = new Members_Model_DbTable_Forms();
		$field_db = new Members_Model_DbTable_Fields();
		$general_db = new Members_Model_DbTable_General();
		$value_db = new Members_Model_DbTable_FieldsValue();
		$global_conf = Zend_Registry::get('global_conf');
		$currency = new Zend_Currency($global_conf['default_locale']);
		$c_symbol = $currency->getSymbol();
		if(self::checkPackageForm($name))
		{
			$form_name = str_replace('_',' ',$name);
			if(self::checkFormAvailable($form_name))
			{
				$get_id = $form_db->getFormId($form_name);
				$form_id = $get_id[0]['id'];				
				if(self::checkFieldAvailable($form_id))
				{
					$field_info	=	$field_db->getFieldInfoByFormId($form_id);
					$field_id = $field_info[0]['id'];
					$peice_field_info	=	$field_db->getPriceFieldInfoByFormId($form_id);
					$price_field_id = $peice_field_info[0]['id'];			
					if(self::checkTableAvailable($form_id))
					{
						$general_table_info = $general_db->getAllPublishedValues($form_id,'1');
						$i = 0;
						foreach($general_table_info as $general_info)
						{
							$table_id = $general_info['id'];
							$value_data = $value_db->getFieldsValue($table_id,$form_id,$field_id);
							$price_value_data = $value_db->getFieldsValue($table_id,$form_id,$price_field_id);
							$data[$i] = array('id' => $table_id, 'name' => stripslashes($value_data[0]['field_value']), 'price' => stripslashes($price_value_data[0]['field_value']),'currency' => $c_symbol);
							$i++;
						}
					}
					else
					{
						$data = null;
					}
				}
				else
				{
					$data = null;
				}
			}
			else
			{
				$data = null;
			}
		}
		else
		{
			$data = null;
		}
		return $data;
	}
	
	public function getFormAllDatas($table_id)
	{
		$form_db = new Members_Model_DbTable_Forms();
		$field_db = new Members_Model_DbTable_Fields();
		$general_db = new Members_Model_DbTable_General();
		$value_db = new Members_Model_DbTable_FieldsValue();
			
		$table_info = $general_db->getInfo($table_id);
		if($table_info)
		{	
			$form_id = 	$table_info['form_id'];
			$datas = $value_db->getFieldsValue1($table_id,$form_id);
			$data_arr = array();			
			$i = 0;
			foreach($datas as $row)
			{
				$data_arr[$i] = array('id' =>  $row['id'],'table_id' =>  $row['table_id'],'form_id' =>  $row['form_id'],'field_id' =>  $row['field_id'],'field_value' =>  stripslashes($row['field_value']));
				$data = stripslashes($row['field_value']);
				$i++;
			}
			$data = $data_arr;
		}
		else
		{
			$data = null;
		}				
					
		return $data;
	}
	
	public function checkFormAssignedToRole($role_id) 
	{
		//DB Connection
		$role_db = new Members_Model_DbTable_Role();
		$validator	=	$role_db->formValidator();		
        $return = ($validator->isValid($role_id)) ? true : false;		
        return $return; 
	}
	
	public function checkFieldValueAvailable($user_id,$form_id,$field_id) 
	{
		//DB Connection
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$validator	=	$field_value_db->fieldValueValidator($form_id,$field_id);		
        $return = ($validator->isValid($user_id)) ? true : false;		
        return $return; 
	}
	
	public function checkFieldValueFormIdAvailable($form_id) 
	{
		//DB Connection
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$validator	=	$field_value_db->valueValidator();		
        $return = ($validator->isValid($form_id)) ? true : false;		
        return $return; 
	}
	
	public function isPackage()
	{
		$auth = Zend_Auth::getInstance ();
		if($auth->hasIdentity ())
		{
			$identity = $auth->getIdentity ();
			$role_id = $identity->role_id;
			$user_id = $identity->user_id;
			
			$role_db = new Members_Model_DbTable_Role();
			$field_db = new Members_Model_DbTable_Fields();
			$field_value_db = new Members_Model_DbTable_FieldsValue();
			$general_db = new Members_Model_DbTable_General();
			if(self::checkFormAssignedToRole($role_id))
			{
				$role_info = $role_db->getRoleInfo($role_id);
				$form_id =	$role_info['form_id'];
				if(self::checkFieldAvailable($form_id))
				{
					$fields_info = $field_db->getFieldsInfo($form_id,null) ;
					$field_name = null;
					foreach($fields_info as $fields)
					{
						if(self::checkPackageForm($fields['field_name']))
						{
							$field_name	= $fields['field_name'];
							$field_id	=	$fields['id'];
							break;
						}
					}
					if(!empty($field_name))
					{						
						if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
						{
							$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
							$general_table_id = $values_info[0]['field_value'];
							if(self::checkTableIdAvailable($general_table_id))
							{
								$general_table_info = $general_db->getInfo($general_table_id);
								$g_form_id = $general_table_info['form_id'];
								if(self::checkFieldAvailable($g_form_id))
								{
									if(self::checkFieldValueFormIdAvailable($g_form_id))
									{
										return true;
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}	
	}
	
	public function isPackageField($module,$controller,$action)
	{
		$auth = Zend_Auth::getInstance ();
		if($auth->hasIdentity ())
		{
			$identity = $auth->getIdentity ();
			$role_id = $identity->role_id;
			$user_id = $identity->user_id;
			
			$role_db = new Members_Model_DbTable_Role();
			$field_db = new Members_Model_DbTable_Fields();
			$field_value_db = new Members_Model_DbTable_FieldsValue();
			$general_db = new Members_Model_DbTable_General();
			if(self::checkFormAssignedToRole($role_id))
			{
				$role_info = $role_db->getRoleInfo($role_id);
				$form_id =	$role_info['form_id'];
				if(self::checkFieldAvailable($form_id))
				{
					$fields_info = $field_db->getFieldsInfo($form_id,null) ;
					$field_name = null;
					foreach($fields_info as $fields)
					{
						if(self::checkPackageForm($fields['field_name']))
						{
							$field_name	= $fields['field_name'];
							$field_id	=	$fields['id'];
							break;
						}
					}
					if(!empty($field_name))
					{						
						if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
						{
							$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
							$general_table_id = $values_info[0]['field_value'];
							if(self::checkTableIdAvailable($general_table_id))
							{
								$general_table_info = $general_db->getInfo($general_table_id);
								$g_form_id = $general_table_info['form_id'];
								if(self::checkFieldAvailable($g_form_id))
								{
									if(self::checkFieldValueFormIdAvailable($g_form_id))
									{
										$package_field_name = $module.'_'.$controller.'_'.$action;
										if(self::checkFieldNameAvailable($package_field_name,$g_form_id))
										{
										return true;
										}
										else
										{
											return false;
										}
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}	
	}	
	
	public function getPackageFieldValue($module,$controller,$action)
	{
		$auth = Zend_Auth::getInstance ();
		if($auth->hasIdentity ())
		{
			$identity = $auth->getIdentity ();
			$role_id = $identity->role_id;
			$user_id = $identity->user_id;
			
			$role_db = new Members_Model_DbTable_Role();
			$field_db = new Members_Model_DbTable_Fields();
			$field_value_db = new Members_Model_DbTable_FieldsValue();
			$general_db = new Members_Model_DbTable_General();
			if(self::checkFormAssignedToRole($role_id))
			{
				$role_info = $role_db->getRoleInfo($role_id);
				$form_id =	$role_info['form_id'];
				if(self::checkFieldAvailable($form_id))
				{
					$fields_info = $field_db->getFieldsInfo($form_id,null) ;
					$field_name = null;
					foreach($fields_info as $fields)
					{
						if(self::checkPackageForm($fields['field_name']))
						{
							$field_name	= $fields['field_name'];
							$field_id	=	$fields['id'];
							break;
						}
					}
					if(!empty($field_name))
					{						
						if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
						{
							$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
							$general_table_id = $values_info[0]['field_value'];
							if(self::checkTableIdAvailable($general_table_id))
							{
								$general_table_info = $general_db->getInfo($general_table_id);
								$g_form_id = $general_table_info['form_id'];
								if(self::checkFieldAvailable($g_form_id))
								{
									if(self::checkFieldValueFormIdAvailable($g_form_id))
									{
										$package_field_name = $module.'_'.$controller.'_'.$action;
										if(self::checkFieldNameAvailable($package_field_name,$g_form_id))
										{
											$package_field_info = $field_db->getFieldIdByFieldName($g_form_id,$package_field_name);
											$package_field_id = $package_field_info[0]['id'];
											$package_infos = $field_value_db->getFieldsValue($general_table_id,$g_form_id,$package_field_id);
											return $package_infos[0]['field_value'];
										}
										else
										{
											return false;
										}
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}	
	}
	
	public function getPackageFieldCronValue($module,$controller,$action)
	{
		$field_db = new Members_Model_DbTable_Fields();
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$general_db = new Members_Model_DbTable_General();
								
		$package_field_name = $module.'_'.$controller.'_'.$action;
		if(self::checkFieldNameAvailable($package_field_name,null))
		{
			$package_field_info = $field_db->getFieldIdByFieldName(null,$package_field_name);
			$i=0;
			foreach($package_field_info as $field_info)
			{
				$package_field_id = $field_info['id'];
				$g_form_id		  =	$field_info['form_id'];				
				$package_infos[$i] = $field_value_db->getFieldsValue2($g_form_id,$package_field_id);
				$i++;
			}
			return $package_infos;
		}
		else
		{
			return false;
		}		
	}
	
	
	//Frontend
	public function isPackageFrontend($user_id,$role_id)
	{		
		$role_db = new Members_Model_DbTable_Role();
		$field_db = new Members_Model_DbTable_Fields();
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$general_db = new Members_Model_DbTable_General();
		if(self::checkFormAssignedToRole($role_id))
		{
			$role_info = $role_db->getRoleInfo($role_id);
			$form_id =	$role_info['form_id'];
			if(self::checkFieldAvailable($form_id))
			{
				$fields_info = $field_db->getFieldsInfo($form_id,null) ;
				$field_name = null;
				foreach($fields_info as $fields)
				{
					if(self::checkPackageForm($fields['field_name']))
					{
						$field_name	= $fields['field_name'];
						$field_id	=	$fields['id'];
						break;
					}
				}
				if(!empty($field_name))
				{						
					if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
					{
						$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
						$general_table_id = $values_info[0]['field_value'];
						if(self::checkTableIdAvailable($general_table_id))
						{
							$general_table_info = $general_db->getInfo($general_table_id);
							$g_form_id = $general_table_info['form_id'];
							if(self::checkFieldAvailable($g_form_id))
							{
								if(self::checkFieldValueFormIdAvailable($g_form_id))
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}			
	}
	
	
	public function isPackageFrontendField($module,$controller,$action,$user_id,$role_id)
	{	
		$role_db = new Members_Model_DbTable_Role();
		$field_db = new Members_Model_DbTable_Fields();
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$general_db = new Members_Model_DbTable_General();
		if(self::checkFormAssignedToRole($role_id))
		{
			$role_info = $role_db->getRoleInfo($role_id);
			$form_id =	$role_info['form_id'];
			if(self::checkFieldAvailable($form_id))
			{
				$fields_info = $field_db->getFieldsInfo($form_id,null) ;
				$field_name = null;
				foreach($fields_info as $fields)
				{
					if(self::checkPackageForm($fields['field_name']))
					{
						$field_name	= $fields['field_name'];
						$field_id	=	$fields['id'];
						break;
					}
				}
				if(!empty($field_name))
				{						
					if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
					{
						$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
						$general_table_id = $values_info[0]['field_value'];
						if(self::checkTableIdAvailable($general_table_id))
						{
							$general_table_info = $general_db->getInfo($general_table_id);
							$g_form_id = $general_table_info['form_id'];
							if(self::checkFieldAvailable($g_form_id))
							{
								if(self::checkFieldValueFormIdAvailable($g_form_id))
								{
									$package_field_name = $module.'_'.$controller.'_'.$action;
									if(self::checkFieldNameAvailable($package_field_name,$g_form_id))
									{
									return true;
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function getPackageFrontendFieldValue($module,$controller,$action,$user_id,$role_id)
	{	
		try
		{			
			$role_db = new Members_Model_DbTable_Role();
			$field_db = new Members_Model_DbTable_Fields();
			$field_value_db = new Members_Model_DbTable_FieldsValue();
			$general_db = new Members_Model_DbTable_General();
			if(self::checkFormAssignedToRole($role_id))
			{
				$role_info = $role_db->getRoleInfo($role_id);
				$form_id =	$role_info['form_id'];
				if(self::checkFieldAvailable($form_id))
				{
					$fields_info = $field_db->getFieldsInfo($form_id,null) ;
					$field_name = null;
					foreach($fields_info as $fields)
					{
						if(self::checkPackageForm($fields['field_name']))
						{
							$field_name	= $fields['field_name'];
							$field_id	=	$fields['id'];
							break;
						}
					}
					if(!empty($field_name))
					{						
						if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
						{
							$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
							$general_table_id = $values_info[0]['field_value'];
							if(self::checkTableIdAvailable($general_table_id))
							{
								$general_table_info = $general_db->getInfo($general_table_id);
								$g_form_id = $general_table_info['form_id'];
								if(self::checkFieldAvailable($g_form_id))
								{
									if(self::checkFieldValueFormIdAvailable($g_form_id))
									{
										$package_field_name = $module.'_'.$controller.'_'.$action;
										if(self::checkFieldNameAvailable($package_field_name,$g_form_id))
										{
											$package_field_info = $field_db->getFieldIdByFieldName($g_form_id,$package_field_name);
											$package_field_id = $package_field_info[0]['id'];
											$package_infos = $field_value_db->getFieldsValue($general_table_id,$g_form_id,$package_field_id);
											return $package_infos[0]['field_value'];
										}
										else
										{
											return false;
										}
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}	
		}
		catch(Exception $e)
		{
			return false;
		}	
	}	
	
	public function getPackageFrontendFieldDetailsValue($module,$controller,$action,$user_id,$role_id)
	{				
		$role_db = new Members_Model_DbTable_Role();
		$field_db = new Members_Model_DbTable_Fields();
		$field_value_db = new Members_Model_DbTable_FieldsValue();
		$general_db = new Members_Model_DbTable_General();
		if(self::checkFormAssignedToRole($role_id))
		{
			$role_info = $role_db->getRoleInfo($role_id);
			$form_id =	$role_info['form_id'];
			if(self::checkFieldAvailable($form_id))
			{
				$fields_info = $field_db->getFieldsInfo($form_id,null) ;
				$field_name = null;
				foreach($fields_info as $fields)
				{
					if(self::checkPackageForm($fields['field_name']))
					{
						$field_name	= $fields['field_name'];
						$field_id	=	$fields['id'];
						break;
					}
				}
				if(!empty($field_name))
				{						
					if(self::checkFieldValueAvailable($user_id,$form_id,$field_id))
					{						
						$values_info = $field_value_db->getFieldsValue($user_id,$form_id,$field_id);
						$general_table_id = $values_info[0]['field_value'];
						if(self::checkTableIdAvailable($general_table_id))
						{
							$general_table_info = $general_db->getInfo($general_table_id);
							$g_form_id = $general_table_info['form_id'];
							if(self::checkFieldAvailable($g_form_id))
							{
								if(self::checkFieldValueFormIdAvailable($g_form_id))
								{
									$package_field_name = $module.'_'.$controller.'_'.$action;
									if(self::checkFieldNameAvailable($package_field_name,$g_form_id))
									{
										$package_field_info = $field_db->getFieldIdByFieldName($g_form_id,$package_field_name);
										$package_field_id = $package_field_info[0]['id'];																			
										$package_infos = $field_value_db->getFieldsValue($general_table_id,$g_form_id,$package_field_id);
										
										if($package_infos)
										{											
											if(!empty($package_infos[0]))
											{																																		
												return $package_infos[0];
											}
											else
											{												
												return false;
											}
										}
										else
										{
											return false;
										}
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}		
	}
}
?>