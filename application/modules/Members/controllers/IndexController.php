<?php
//Members Backend Controller
class Members_IndexController extends Zend_Controller_Action
{
	private $config ;
	private $_controllerCache;
	private $_auth_obj;
	
    public function init()
    {	
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		$this->view->setEscape('stripslashes');	
				
        /* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
	}

    public function listAction()
    {
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve 		= $this->_request->getParam('approve');					
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;	
							
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{		
					$list_mapper = new Members_Model_MemberListMapper();			
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{	
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_username'] = str_replace('_', '-', $entry_arr['username']);	
								$entry_arr['last_access_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['last_access']));
								$entry_arr['register_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['register_date']));
								$entry_arr['edit_enable']	= (($this->_auth_obj->access_other_user_profile == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) && ($entry_arr['role_lock'] == '1' || $this->_auth_obj->role_id == $entry_arr['role_id']) ) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		//Get Role List
		$roles = new Members_Model_DbTable_Role();
		$this->view->roleList = $roles->fetchAll();
		
    }
	
	public function groupAction()
    {
		
		$groupForm =  new Members_Form_GroupForm ();		
		$this->view->groupForm = $groupForm;	
	}
	
	public function addAction()
    {
		$translator = Zend_Registry::get('translator');
		$selected_role_id =  ($this->_request->getPost('role_id')) ?  $this->_request->getPost('role_id') :$this->_request->getParam('role_id');
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$role_info['attach_file_max_size'] = $form_info['attach_file_max_size'];
				$role_info['common_captcha_show_backend']	=	$translator->translator('common_captcha_show_backend');
				$role_info['display_type']	=	'display_admin' ;
				$this->view->form_info = $form_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}
		
		$registrationForm =  new Members_Form_UserForm ($role_info);
		$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
				
        if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				if($element->getType() == 'Zend_Form_Element_File')
				{
					$element_name = $element->getName();
					$registrationForm->removeElement($element_name);
				}
			}
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();			
							
			if ($registrationForm->isValid($this->_request->getPost())) 
			{
				$registrationForm =  new Members_Form_UserForm ($role_info);
				$registrationForm->populate($this->_request->getPost());
				$username = $this->_request->getPost('username');
				$user_check = new Members_Controller_Helper_Registers(); 
							
				if($user_check->getUsernameAvailable($username))
				{
					if($this->_request->getPost('password') == $this->_request->getPost('confirmPassword'))
					{
						$fromValues = $registrationForm;
						$members = new Members_Model_Members($this->_request->getPost());
						
						$perm = new Members_View_Helper_Allow();
						if(!$perm->allow('add','role','Administrator'))
						{
							$roles_id = ($selected_role_id) ? $selected_role_id : 102;
							$members->setRole_id($roles_id);
							$members->setStatus(0);
						}
						else
						{
							$members->setStatus(1);
						}
						$members->setLoginurl($loginUrl);
						
						$result = $members->saveRegister();
						
						if($result['status'] == 'ok')
						{							
							$msg = $translator->translator("member_registered_successfull");
							$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
							if(!empty($role_info) && !empty($role_info['form_id']))
							{
								$field_db = new Members_Model_DbTable_Fields();
								$field_groups = $field_db->getGroupNames($form_info['id']); 
								
								//Add Data To Database
								foreach($field_groups as $group)
								{
									$group_name = $group->field_group;
									$displaGroup = $registrationForm->getDisplayGroup($group_name);
									$elementsObj = $displaGroup->getElements();
									foreach($elementsObj as $element)
									{
										if(!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select'))
										{
											$table_id = $result['id'];
											$form_id = $form_info['id'];
											$field_id	=	$element->getAttrib('rel');
											$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
											if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
											{ 
												if(is_array($field_value)) { $field_value = ''; }
											}
											if($element->getType() == 'Zend_Form_Element_Multiselect') 
											{ 
												$field_value	=	$this->_request->getPost($element->getName());
												if(is_array($field_value)) { $field_value = implode(',',$field_value); }
											}
											try
											{
												$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
												$DBconn = Zend_Registry::get('msqli_connection');
												$DBconn->getConnection();
												$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
												$msg = $translator->translator("member_registered_successfull");
												$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											catch(Exception $e)
											{
												$msg = $translator->translator("member_registered_err");
												$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
										}
									}
								}
								
								//Email Send
								$register_helper = new Members_Controller_Helper_Registers();
								$field_info = $field_db->getFieldsInfo($role_info['form_id']);
								$allDatas = $members->getAllDatas();
								
								if($field_info)
								{
									$atta_count = 0;
									$attach_file_arr = array();
									foreach($field_info as $element)
									{
										if($element->field_type == 'file')
										{
											$attach_file_arr[$atta_count] = $allDatas[$element->field_name];
											$atta_count++;
										}
									}
								}
								else
								{
									$attach_file_arr = null;
								}
								
								$allDatas['status'] = ($data['status'] == '1') ? 'active' : 'inactive';
								$allDatas['password'] = $members->getReal_pass();
								$allDatas['loginurl'] = $members->getLoginurl();
								
								try 
								{
									if(!empty($allDatas['member_package']))
									{	
										if($registrationForm->member_package)
										{
											$allDatas['member_package'] = $registrationForm->member_package->getMultiOption($allDatas['member_package']);
										}
										else
										{
											$allDatas['member_package'] = '';
										}
									}
									else
									{							
										$allDatas['member_package'] = '';
									}
									$register_helper->sendMail($allDatas,$form_info,$attach_file_arr);
								}
								catch (Exception $e) 
								{
									$mail_msg = $e->getMessage();
								}
								//Delete Attached Files
								if($form_info['attach_file_delete'] == '1')
								{
									if($attach_file_arr != null)
									{
										foreach($attach_file_arr as $key=>$value)
										{
											if(!empty($value))
											{	
												$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$value;
												$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
											}								
										}
									}
								}
								$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));								
							}
						}
						else
						{
							$msg = $translator->translator("member_registered_err");
							$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						}				
					}
					else
					{
						$msg = $translator->translator("password_not_match");
						$json_arr = array('status' => 'errP','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}
				}
				else
				{
					$msg = $translator->translator("member_availability",$username);
					$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			else
			{
				$validatorMsg = $registrationForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			$this->view->selected_role_id	=	$selected_role_id;	
			if(!empty($selected_role_id) && $registrationForm->role_id){ $registrationForm->role_id->setValue($selected_role_id); }		
			$this->view->registrationForm = $registrationForm;
		}
		$this->dynamicUploaderSettings($this->view->form_info);			
    }
	
	public function editAction()
    {
		$user_id = ($this->_request->getPost('user_id'))? $this->_request->getPost('user_id') : $this->_getParam('user_id', 0);
		$translator = Zend_Registry::get('translator');	
		if(!empty($user_id))
		{
			$memberData = new Members_Model_DbTable_MemberList();
			$members_info = $memberData->getMemberInfo($user_id);
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);				
				$role_info['attach_file_max_size'] = $form_info['attach_file_max_size'];
				$role_info['common_captcha_show_backend']	=	$translator->translator('common_captcha_show_backend');
				$this->view->form_info = $form_info;
				$role_info['display_type']	=	'display_admin' ;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}
		$registrationForm =  new Members_Form_UserForm ($role_info);
		$registrationForm->removeElement('password');
		$registrationForm->removeElement('confirmPassword');
		
        if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				$element_name = $element->getName();
				$group_arr[$element_name] = $element_name;
				if($element->getType() == 'Zend_Form_Element_File')
				{	
					$registrationForm->removeElement($element_name);
				}					
			}				
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			
			$username = $this->_request->getPost('username');
			$user_check = new Members_Controller_Helper_Registers(); 
			
			if($user_check->getUsernameAvailable($username,$user_id))
			{		
				if ($registrationForm->isValid($this->_request->getPost())) 
				{					
					$registrationForm =  new Members_Form_UserForm ($role_info);
					$registrationForm->removeElement('password');
					$registrationForm->removeElement('confirmPassword');
					$registrationForm->populate($this->_request->getPost());
					
					$fromValues = $registrationForm;
					$members = new Members_Model_Members($this->_request->getPost());
					$roles_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $members_info['role_id'];
					$members->setUser_id($user_id);	
					$members->setRole_id($roles_id);			
					$result = $members->saveRegister();
						
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("member_save_successfull");
						$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						if(!empty($role_info) && !empty($role_info['form_id']))
						{
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($form_info['id']); 
							foreach($field_groups as $group)
							{
								$group_name = $group->field_group;
								$displaGroup = $registrationForm->getDisplayGroup($group_name);
								$elementsObj = $displaGroup->getElements();
								foreach($elementsObj as $element)
								{								
									if(!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select'))
									{
										
										if(substr($element->getName(), -5) != '_prev')
										{
											$table_id = $result['id'];
											$form_id = $form_info['id'];
											$field_id	=	$element->getAttrib('rel');
											$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
											if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
											{ 
												if(is_array($field_value)) { $field_value = ''; }
											}
											if($element->getType() == 'Zend_Form_Element_Multiselect') 
											{ 
												$field_value	=	$this->_request->getPost($element->getName());
												if(is_array($field_value)) { $field_value = implode(',',$field_value); }
											}
											
											try
											{
												$DBconn = Zend_Registry::get('msqli_connection');
												$DBconn->getConnection();
												
												// Remove from Value
												$where = array();
												$where[0] = 'table_id = '.$DBconn->quote($table_id);
												$where[1] = 'form_id = '.$DBconn->quote($form_id);
												$where[2] = 'field_id = '.$DBconn->quote($field_id);
												$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
												
												//Add Value
												$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
												$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
												if($element->getType() == 'Zend_Form_Element_File' && $this->_request->getPost($element->getName()) != $this->_request->getPost($element->getName().'_prev'))
												{
													$element_value	= $this->_request->getPost($element->getName().'_prev');
													if(!empty($element_value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$element_value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}
												}
												$msg = $translator->translator("member_registered_successfull");
												$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											catch(Exception $e)
											{
												$msg = $translator->translator("member_registered_err");
												$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											
										}
									}
								}
							}
						}
					}
					else
					{
						$msg = $translator->translator("member_save_err");
						$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}
					$res_value = Zend_Json_Encoder::encode($json_arr);			
					$this->_response->setBody($res_value);
				}
				else
				{
					$validatorMsg = $registrationForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			else
			{
				$msg = $translator->translator("member_availability",$username);
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			if($members_info)			
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);
				$members_info['form_id'] = 	$role_info['form_id'];
				$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
				$members_info	=	$dynamic_valu_db->getFieldsValueInfo($members_info,$user_id);
				$registrationForm->populate($members_info);	
				$this->view->members_info = 	$members_info;
				$this->view->role_info = 	$role_info;
				$this->view->user_id = 	$user_id;	
				$this->view->registrationForm = $registrationForm;
				$this->dynamicUploaderSettings($this->view->form_info);	
			}
			else
			{
				$this->_helper->redirector('group', $this->view->getController, $this->view->getModule, array());
			}
		}	
    }
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	public function profileAction()
    {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ())
		{
			$globalIdentity = $auth->getIdentity ();	
		}
		$user_id = $globalIdentity->user_id;
		$translator = Zend_Registry::get('translator');
		if(!empty($user_id))
		{
			$memberData = new Members_Model_DbTable_MemberList();
			$members_info = $memberData->getMemberInfo($user_id);
		
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);					
				$form_db = new Members_Model_DbTable_Forms();				
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$role_info['common_captcha_show_backend']	=	$translator->translator('common_captcha_show_backend');
				$this->view->form_info = $form_info;	
				$role_info['display_type']	=	'display_admin' ;			
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}
		$registrationForm =  new Members_Form_UserForm ($role_info);
		$registrationForm->removeElement('password');
		$registrationForm->removeElement('confirmPassword');
		
        if ($this->_request->isPost() && $this->_request->getPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				if($element->getType() == 'Zend_Form_Element_File')
				{
					$element_name = $element->getName();
					$registrationForm->removeElement($element_name);
				}
			}
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$username = $this->_request->getPost('username');
			$user_check = new Members_Controller_Helper_Registers(); 
			$roles_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $members_info['role_id'];
			
			if($user_check->getUsernameAvailable($username,$user_id))
			{		
				if ($registrationForm->isValid($this->_request->getPost())) 
				{
					$registrationForm =  new Members_Form_UserForm ($role_info);
					$registrationForm->removeElement('password');
					$registrationForm->removeElement('confirmPassword');
					$registrationForm->populate($this->_request->getPost());
					
					$fromValues = $registrationForm;
					$data = new Members_Model_Members($this->_request->getPost());
					$data->setUser_id($user_id);
					$data->setRole_id($roles_id);				
					$result = $data->saveRegister();
						
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("member_save_successfull");
						$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						$globalIdentity->username = $data->getUsername();
						$globalIdentity->title = $data->getTitle();
						$globalIdentity->firstName = $data->getFirstName();						
						$globalIdentity->lastName = $data->getLastName();
						$globalIdentity->role_id = $data->getRole_id();
						$updated_role_info = $role_db->getRoleInfo($globalIdentity->role_id);
						$globalIdentity->role_name = stripslashes($updated_role_info['role_name']);
						Eicra_Global_Variable::getSession()->admin_top_menu  = '';
						Eicra_Global_Variable::getSession()->acl_obj = '';
						if(!empty($role_info) && !empty($role_info['form_id']))
						{
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($form_info['id']); 
							foreach($field_groups as $group)
							{
								$group_name = $group->field_group;
								$displaGroup = $registrationForm->getDisplayGroup($group_name);
								$elementsObj = $displaGroup->getElements();
								foreach($elementsObj as $element)
								{
									if(!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select'))
									{
										if(substr($element->getName(), -5) != '_prev')
										{
											$table_id = $result['id'];
											$form_id = $form_info['id'];
											$field_id	=	$element->getAttrib('rel');
											$field_value = ($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
											if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
											{ 
												if(is_array($field_value)) { $field_value = ''; }
											}
											if($element->getType() == 'Zend_Form_Element_Multiselect') 
											{ 
												$field_value	=	$this->_request->getPost($element->getName());
												if(is_array($field_value)) { $field_value = implode(',',$field_value); }
											}
											
											try
											{
												$DBconn = Zend_Registry::get('msqli_connection');
												$DBconn->getConnection();
												
												// Remove from Value
												$where = array();
												$where[0] = 'table_id = '.$DBconn->quote($table_id);
												$where[1] = 'form_id = '.$DBconn->quote($form_id);
												$where[2] = 'field_id = '.$DBconn->quote($field_id);
												$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
												
												//Add Value
												$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
												$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
												if($element->getType() == 'Zend_Form_Element_File' && $this->_request->getPost($element->getName()) != $this->_request->getPost($element->getName().'_prev'))
												{
													$element_value	= $this->_request->getPost($element->getName().'_prev');
													if(!empty($element_value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$element_value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}
												}
												$msg = $translator->translator("member_save_successfull");
												$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											catch(Exception $e)
											{
												$msg = $translator->translator("member_save_err");
												$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											
										}
									}
								}
							}
						}
					}
					else
					{
						$msg = $translator->translator("member_save_err");
						$json_arr = array('status' => 'err','msg' => $msg.' '.$result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}
					$res_value = Zend_Json_Encoder::encode($json_arr);			
					$this->_response->setBody($res_value);
				}
				else
				{
					$validatorMsg = $registrationForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			else
			{
				$msg = $translator->translator("member_availability",$username);
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
			$members_info	=	$dynamic_valu_db->getFieldsValueInfo($members_info,$user_id);
			$registrationForm->populate($members_info);	
			$this->view->members_info = 	$members_info;
			$this->view->user_id = 	$user_id;	
			$this->view->registrationForm = $registrationForm;
			$this->dynamicUploaderSettings($this->view->form_info);	
		}	
    }
	
	public function ajaxsearchusersAction()
	{
		/****************Will Be Deprecated in version 1.9.3 Start  But Not This Function**********************/
		
		/*$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		// Some login when controller is loaded		
		$request = $this->getRequest();
		$values = $request->getParams();
		$data = new Members_Model_AjaxSearchUsers();		
		$resultSet = $data->findDatas($values);
				
		
		$members_role = new Members_Model_DbTable_Role();
				
		if (0 == count($resultSet)) 
		{
			$msg = $translator->translator("member_search_not_found");
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		else
		{	
			$pageNumber = $this->getRequest()->getParam('page');
			$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			
			$f = 0;
			foreach($paginator as $rawData)
			{
				$roleInfo = $members_role->getRoleInfo($rawData['role_id']);
				$result[$f]['role_lock'] = $roleInfo['role_lock'];
				foreach($rawData as $key =>$value)
				{
					$result[$f][$key] = stripslashes($value);
				}									
				$f++;
			}			
			$page_object = $paginator->getPages();						
			$json_arr = array('status' => 'ok','json_data' => $result,'page_object' => $page_object);		
		}
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);*/
				
		/****************Will Be Deprecated in version 1.9.3 End**********************/
	}
	
	public function ajaxactivedeactiveusersAction()
	{				
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$user_id = $this->_request->getPost('user_id');
			$username = $this->_request->getPost('username');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Product status
			$where = array();
			$where[] = 'user_id = '.$conn->quote($user_id);
			$return = $conn->update(Zend_Registry::get('dbPrefix').'user_profile',array('status' => $active), $where);
		
			if($return)
			{			
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('member_list_publish_err',$username);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function publishallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id_str  = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
				
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'user_id = '.$conn->quote($id);
					try
					{	
						$conn->update(Zend_Registry::get('dbPrefix').'user_profile',array('status' => $active), $where);
						$return = true;
					}
					catch (Exception $e) 
					{
						$return = false;
						$err = $e->getMessage();
					}
				}
			
				if($return)
				{			
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('page_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg." ".$err);
				}
			}
			else
			{
				$msg = $translator->translator("page_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function ajaxdeleteusersAction()
	{				
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$user_id = $this->_request->getPost('user_id');			
			$username = $this->_request->getPost('username');
					
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			//GET MEMBER INFORMATION
			$member = new Members_Model_DbTable_MemberList();
			$member_info = $member->getMemberInfo($user_id);
			
			//CHECK LOCK INFORMATION
			$members_role = new Members_Model_DbTable_Role(); 
			$roleInfo = $members_role->getRoleInfo($member_info['role_id']);
			
			if($roleInfo['role_lock'] == '1')
			{
				// Remove from Value
				$whereF = array();
				$whereF[0] = 'table_id = '.$conn->quote($user_id);
				$whereF[1] = 'form_id = '.$conn->quote($roleInfo['form_id']);
				
				// Remove from Product
				$where = array();
				$where[] = 'user_id = '.$conn->quote($user_id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'user_profile', $where);
					$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$whereF);
					$msg = 	$translator->translator('member_list_delete_success',$username);			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('member_list_delete_err',$username);			
					$json_arr = array('status' => 'err','msg' => $msg." ".$e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('member_list_delete_lock_err');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('member_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			$id_str = $this->_request->getPost('id_st');
			$perm = new Members_View_Helper_Allow();			
			if ($perm->allow('ajaxdeleteusers','index','Members'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					$non_del_arr = array();
					$k = 0;
					
					foreach($id_arr as $id)
					{
						//GET MEMBER INFORMATION
						$member = new Members_Model_DbTable_MemberList();
						$member_info = $member->getMemberInfo($id);
						
						//CHECK LOCK INFORMATION
						$members_role = new Members_Model_DbTable_Role(); 
						$roleInfo = $members_role->getRoleInfo($member_info['role_id']);
						
						if($roleInfo['role_lock'] == '1')
						{			
							// Remove from Value
							$whereF = array();
							$whereF[0] = 'table_id = '.$conn->quote($id);	
							$whereF[1] = 'form_id = '.$conn->quote($roleInfo['form_id']);
										
							//Remove from Category
							$where = array();
							$where[] = 'user_id = '.$conn->quote($id);
							try
							{
								$conn->delete(Zend_Registry::get('dbPrefix').'user_profile', $where);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$whereF);
								$msg = 	$translator->translator('member_list_delete_success',$member_info['username']);			
								$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
							catch (Exception $e) 
							{
								$non_del_arr[$k] = $id;
								$k++;
								$msg = 	$translator->translator('member_list_delete_err',$member_info['username']);			
								$json_arr = array('status' => 'ok','msg' => $msg." ".$e->getMessage(), 'non_del_arr' => $non_del_arr);
							}
						}
						else
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('member_list_delete_err');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
				}
				else
				{
					$msg = $translator->translator("member_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('member_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function preDispatch() 
	{	
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);	
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;	
	}
}