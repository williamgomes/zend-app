	<?php 
	
	class Members_ListController extends Zend_Controller_Action
	{
		public function init()
		{		
			/* Initialize action controller here */
			$getModule = $this->_request->getModuleName();
			$this->view->assign('getModule', $getModule);
			$getAction = $this->_request->getActionName();
			$this->view->assign('getAction', $getAction);
			$getController = $this->_request->getControllerName();
			$this->view->assign('getController', $getController);									
		}

		public function preDispatch() 
		{	
			$template_obj = new Eicra_View_Helper_Template;	
			$template_obj->setFrontendTemplate();	
			$front_template = Zend_Registry::get('front_template');		
			$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
			$this->view->front_template = $front_template;
			
			//DB Connection
			$this->_DBconn = Zend_Registry::get('msqli_connection');
			$this->_DBconn->getConnection();
			
			$translator = Zend_Registry::get('translator');
			$this->view->assign('translator', $translator);	
			$this->view->setEscape('stripslashes');
			
			if($this->_request->getParam('menu_id'))
			{
				$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
				$page_id_arr = $viewHelper->_getContentId();				
				$this->_page_id = implode(',',$page_id_arr);
			}
			

			
		}

		public function viewAction()
		{		
			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);
			

			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);			
			$resultSet = $list_info;   
			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);

			$this->view->assign('list_info', $paginator);


		}

		public function tailorAction()
		{			
			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'tailor'){

					unset($list_info[$key]);		
				}
			}		
			$resultSet = $list_info;   
			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);

		}

		public function mastercutterAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'master cutter'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function patternmakerAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'pattern maker'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function fashiondesignerAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'fashion designer'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   
			
			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function handembroidererAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'hand embroiderer'){
					unset($list_info[$key]);
				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function machineembroidererAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'machine embroiderer'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function salesAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'sales'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}

		public function shopmanagerAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'shop manager'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}
		public function qualitycontrollerAction()
		{

			$search_params['sort'][] = array('field' => 'id', 'dir' => 'DESC');

			$search_params['filter']['filters'][]  = array('field' => 'form_id', 'operator' => 'eq', 'value' =>$this->_page_id);



			$tableColumns['dataLimit'] =  10;

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params, $tableColumns);	

			foreach ($list_info as $key => $array) {

				if(strtolower(trim($array['position'])) != 'quality controller'){

					unset($list_info[$key]);

				}
			}		
			$resultSet = $list_info;   

			$pageNumber = $this->_request->getParam('page');    
			$viewPageNum = (Eicra_Global_Variable::getSession()->viewPageNum) ? Eicra_Global_Variable::getSession()->viewPageNum : 30;
			$paginator = Zend_Paginator::factory($resultSet);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);
			$this->view->assign('list_info', $paginator);


		}


		public function detailsAction()
		{	
			$id= $this->_request->getParam('id');
			$search_params['filter']['filters'][]  = array('field' => 'id', 'operator' => 'eq', 'value' => $id );

			$general_db = new Members_Model_DbTable_General();
			$list_info = $general_db->getListInfo(null, $search_params);

			$this->view->assign('list_info',$list_info);

		}


	}


	?>