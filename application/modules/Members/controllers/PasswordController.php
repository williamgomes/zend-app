<?php
//Members Password Controller
class Members_PasswordController extends Zend_Controller_Action 
{
	private $passForm;
	private $_controllerCache;
	public function init()
	{		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
	
		if($this->_request->getParam('user_id') || $this->_request->getPost('user_id'))
		{
			$user = (Zend_Auth::getInstance()->hasIdentity()) ? Zend_Auth::getInstance()->getIdentity() : null;
		}
		else
		{
			$user = null;
		}
		$this->passForm = new Members_Form_ChangePassForm($user);
		
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);	
	}
	
	public function preDispatch()
    {    
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login'; 
		Eicra_Global_Variable::checkSession($this->_response,$url);
    }
	
	public function changeAction() 
	{
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->passForm->isValid($this->_request->getPost())) 
			{
				//Get DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				$oldpass = trim($this->_request->getPost('oldpass'));
				$password = str_replace(' ', '', trim($this->_request->getPost('password')));
				$confirmPassword = str_replace(' ', '', trim($this->_request->getPost('confirmPassword')));
				$user = Zend_Auth::getInstance()->getIdentity();
				if($this->_request->getPost('user_id'))
				{
					$user_id = $this->_request->getPost('user_id');
					if($user->allow_reset_user_password == '1')
					{
						$ckOldpass = true;
					}
					else
					{
						$ckOldpass = $this->checkOldPass($oldpass,$user_id,$conn);
					}
				}
				else
				{	
					$user_id = $user->user_id;
					$ckOldpass = $this->checkOldPass($oldpass,$user_id,$conn);
				}				
				
				if($ckOldpass)
				{
					if($password == $confirmPassword)
					{
												
						//Get Salt
						$selectSalt = $conn->select()
							->from(array('u' => Zend_Registry::get('dbPrefix').'user_profile'), array('salt'))
							->where('u.user_id = ?', $user_id)
							->limit(1);
						
						$selectRes = $selectSalt->query()->fetchAll();
						$salt = $selectRes[0]['salt'];
						$newPass = $this->genPassword($password,$salt);
						
						
						$sql = 'UPDATE '.Zend_Registry::get('dbPrefix').'user_profile SET password = "'.$newPass.'" WHERE user_id = '.$conn->quote($user_id).' ';
						
						if($conn->query($sql))
						{
							$msg = $translator->translator('password_change_success');
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $translator->translator('password_change_err');
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator('password_not_match');
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator('old_password_check_err');
					$json_arr = array('status' => 'err','msg' => $msg);
				}
				
			}
			else
			{
				$validatorMsg = $this->passForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}
			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$user_id = $this->_request->getParam('user_id');
			$this->view->user_id = $user_id;
			$this->view->passForm = $this->passForm;
		}		
	}
	
	public function genPassword($newPass,$salt)
	{			
		$pass = md5($salt.$newPass);
		return $pass;
	}
	
	public function checkOldPass($oldpass,$user_id,$conn)
	{			
		//Get Salt
		$selectSalt = $conn->select()
			->from(array('u' => Zend_Registry::get('dbPrefix').'user_profile'), array('salt','password'))
			->where('u.user_id = ?', $user_id)
			->limit(1);
		
		$selectRes = $selectSalt->query()->fetchAll();
		$salt = $selectRes[0]['salt'];
		$encodePass = md5($salt.$oldpass);
		
		
		
		$password = $selectRes[0]['password'];
		
		if($encodePass == $password)
		{
			return true;
		}
		else
		{
			return false;
		}
	}	
}
