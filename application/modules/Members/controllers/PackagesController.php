<?php
//Members Packages controller
class Members_PackagesController extends Zend_Controller_Action
{	
	private $_controllerCache;
	private $_translator;
	private $_auth_obj;
	
    public function init()
    {	
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		$auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;	 
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
			
		$this->view->menu_id = $this->_request->getParam('menu_id');
		
		if($this->_request->getParam('menu_id'))
		{
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			foreach($page_id_arr as $key=>$value)
			{	$this->_page_id = $value;
				break;			
			}	
		}
		else
		{
			$this->_page_id = null;
		}				
    }
	
	public function preDispatch() 
	{	
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);			
		$this->view->setEscape('stripslashes');	
		
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;			
	}
	
	public function upgradeAction()
	{
		Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
        Eicra_Global_Variable::checkSession($this->_response, $url);
				
		// BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array( array( $this->_translator->translator('members_frontend_breadcrumb_dashboard_title'), 'Members-Dashboard'), array( $this->_translator->translator( 'menu_members_upgrade_membership'), $this->view->url()));
		
		$this->view->package_id = $this->_request->getParam('package_id');
		$this->view->form_id = $this->_request->getParam('form_id');		
		
		
		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			
			if($this->_request->getPost('package_id'))
			{
				$package_id = $this->_request->getPost('package_id');
				$package_id_arr = explode(',', $package_id);
				$invoice_arr = $this->generatePackageInvoice($package_id_arr);
				if($invoice_arr['status'] == 'ok')
				{
					Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];	
					$json_arr = array('status' => 'ok','msg' => $this->_translator->translator('members_front_membership_pkg_info_submit_success'));								
				}
				else
				{
					$json_arr = array('status' => 'err','msg' => $this->_translator->translator('invoice_no_invoice_found', '', 'Invoice'));
				}
			}
			else
			{
				$vMsg[0] = array('key' => 'package_id', 'errKey' => 'package_id', 'value' => $this->_translator->translator('member_selected_err'));	
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}
			
			// Convert To JSON ARRAY
			$res_value = Zend_Json::encode($json_arr);
			$this->_response->setBody($res_value);
		}
		else
		{
			if (!empty($this->view->form_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'form_id', 'operator' => 'eq', 'value' => $this->view->form_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			}
			
			$general_db = new Members_Model_DbTable_General();
			$general_info = $general_db->getListInfo(null, $posted_data, array('userChecking' => false));
			$this->view->packages_info = $general_info;
			
			$owner_form_id  = $this->_auth_obj->form_id;
			if($owner_form_id)
			{
				$form_db = new Members_Model_DbTable_Forms();
				$owner_param_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $owner_form_id);
				$owner_param_data['filter']['logic'] = ($owner_param_data['filter']['logic']) ? $owner_param_data['filter']['logic'] : 'and';
				$owner_form_info_arr  = $form_db->getDataListInfo(null, $owner_param_data,  array('userChecking' => false, 'table_id_arr' => array( $this->_auth_obj->user_id) ) );
				
				$owner_package_id = ($owner_form_info_arr && $owner_form_info_arr[0] && $owner_form_info_arr[0]['member_package']) ? $owner_form_info_arr[0]['member_package'] : 0;
				if(!empty($owner_package_id) && !empty($this->view->form_id))
				{			
					$posted_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $owner_package_id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';		
					
					$owner_package_info = $general_db->getListInfo(null, $posted_data, array('userChecking' => false));
					
					if($owner_package_info)
					{				
						$this->view->owner_packages_info = $owner_package_info[0];
					}
				}
			}
		}		
	}
	
	private function generatePackageInvoice($upgrade_info_arr)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$country_db = new Eicra_Model_DbTable_Country();
		$payment_db = new Paymentgateway_Model_DbTable_Gateway();
		$payment_info = $payment_db->getDefaultGateway();
		$auth = $this->view->auth;		
		
		$global_conf = Zend_Registry::get('global_conf');
		$currency = new Zend_Currency($global_conf['default_locale']);
		$currencySymbol = $currency->getSymbol(); 
		$currencyShortName = $currency->getShortName();
		$marchent	=	new Invoice_Controller_Helper_Marchent($global_conf);
									
		$package_db = new Members_Model_DbTable_General();
		
			
		if ($auth->hasIdentity ())
		{
			$globalIdentity = $auth->getIdentity();	
			$user_id = $globalIdentity->user_id;
			foreach($globalIdentity as $globalIdentity_key => $globalIdentity_value)
			{
				$mem_info[$globalIdentity_key] = $globalIdentity_value;
				$invoice_arr[$globalIdentity_key] = $globalIdentity_value;
			}
			if($mem_info['country'])
			{
				$country_info = $country_db->getInfo($mem_info['country']);
				$mem_info['country_name'] = stripslashes($country_info['value']);
			}
			//invoice_arr assigning started
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $global_conf['payment_user_id'];
			$invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']).' '.stripslashes($mem_info['firstName']).' '.stripslashes($mem_info['lastName']).'<br />'.stripslashes($mem_info['mobile']).'<br />'.stripslashes($mem_info['city']).', '.stripslashes($mem_info['state']).', '.stripslashes($mem_info['postalCode']).'<br />'.stripslashes($country_info['value']).'<BR />'.stripslashes($mem_info['username']);
			$invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
			$invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
			$invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]	= date("Y-m-d h:i:s");
			$invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y",strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
			$invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
			$invoice_arr['site_name']	=	stripslashes($global_conf['site_name']);
			$invoice_arr['site_url']	=	stripslashes($global_conf['site_url']);
			$this->view->mem_info = $mem_info;
		}
		
		/**************Owner Info************************/
		$owner_form_id  = $this->_auth_obj->form_id;
		if($owner_form_id)
		{
			$form_db = new Members_Model_DbTable_Forms();
			$owner_param_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $owner_form_id);
			$owner_param_data['filter']['logic'] = ($owner_param_data['filter']['logic']) ? $owner_param_data['filter']['logic'] : 'and';
			$owner_form_info_arr  = $form_db->getDataListInfo(null, $owner_param_data,  array('userChecking' => false, 'table_id_arr' => array( $this->_auth_obj->user_id) ) );
			
			$owner_package_id = ($owner_form_info_arr && $owner_form_info_arr[0] && $owner_form_info_arr[0]['member_package']) ? $owner_form_info_arr[0]['member_package'] : 0;
			if(!empty($owner_package_id) && !empty($upgrade_info_arr[0]))
			{	
				$owner_posted_data['filter']['filters'][] = array('field' => 'form_id', 'operator' => 'eq', 'value' => $upgrade_info_arr[0]);
				$owner_posted_data['filter']['logic'] = ($owner_posted_data['filter']['logic']) ? $owner_posted_data['filter']['logic'] : 'and';
				
				$owner_posted_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $owner_package_id);
				$owner_posted_data['filter']['logic'] = ($owner_posted_data['filter']['logic']) ? $owner_posted_data['filter']['logic'] : 'and';		
				
				$owner_package_info = $package_db->getListInfo(null, $owner_posted_data, array('userChecking' => false));
				
				if($owner_package_info)
				{				
					$owner_package_info = $owner_package_info[0];
				}
			}
		}
		
		$posted_data['filter']['filters'][] = array('field' => 'form_id', 'operator' => 'eq', 'value' => $upgrade_info_arr[0]);
		$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
		
		$posted_data['filter']['filters'][] = array('field' => 'id', 'operator' => 'eq', 'value' => $upgrade_info_arr[1]);
		$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';		
		$package_info_arr = $package_db->getListInfo(null, $posted_data, array('userChecking' => false));
		$package_info = $package_info_arr[0];
		
		$paid_price = ($owner_package_info && $owner_package_info['package_price']) ? ($package_info['package_price'] - $owner_package_info['package_price']) : $package_info['package_price'];	
		$sub_total = $paid_price;
		//invoice_items_arr assigning started
		$total_amount = 0;
		$total_tax = 0;
		$invoice_items_arr = array();
		$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
											<tbody>
											<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>'.$this->_translator->translator("members_invoice_desc_title").'</strong></td>
												<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>'.$this->_translator->translator("members_invoice_amount_title").'</strong></td>
											</tr>';
			
			//ITEM START
			$item_arr = 0;
			$item_details = '<tr><td colspan="2" style="height:8px"></td></tr>
									 <tr>
										<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
										'.stripslashes($package_info['package_name']).'
										</td>
									</tr>';
			$invoice_items_arr[$item_arr]['inv']['package_name']	=	stripslashes($package_info['package_name']);
			$item_details .= '<tr><td colspan="2" style="height:2px"></td></tr><tr>
												<td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6;">'.
													'<table width="100%">
														<tr>
															<td>'
															//.'<img src="'.$this->view->serverUrl().$this->view->baseUrl().'/data/frontImages/b2b/b2b_images/'.$package_info['b2b_images'].'" height="60" title="'.stripslashes($package_info['name']).'" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;"/>'
															.'<span style="color:#F60; font-weight:bold; font-style:italic; text-decoration:underline;">'.stripslashes($package_info['package_name']).'</span>'.
															'<br />'
															.'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("members_advanced_search_price_label").' :</span> '.$currencySymbol.' '.$this->view->numbers(number_format($package_info['package_price'], 2, '.', ',')).' '.$currencyShortName.
															//'<br  />'.
															//'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("b2b_invoice_short_description").' :</span> '.stripslashes($package_info['description']).
															'<br  />'.
															'</td>'.														
														'</tr>'.														
													'</table>'.
												'</td>'.
												'<td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6; line-height: 22px;"valign="top">'.
													'<table border="0" width="100%">';												
														
										$item_details .='<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("member_front_membership_pkg_upgrade_cost_title").'</span> : '.$currencySymbol.' '.$this->view->numbers(number_format($sub_total, 2, '.', ',')).' '.$currencyShortName.'</td>'.
														'</tr>';
																				
									$item_details .= '</table>'.
												'</td>
											</tr>';	
											$item_total += 	$sub_total;	
											
											$invoice_items_arr[$item_arr]['inv']['package_info'] = $package_info;
											$invoice_items_arr[$item_arr]['inv']['sub_total'] = $sub_total;
											$invoice_items_arr[$item_arr]['inv']['package_id']	=	stripslashes($package_info['id']);
											//$invoice_items_arr[$item_arr]['inv']['package_image']	=	stripslashes($package_info['b2b_images']);
											$invoice_items_arr[$item_arr]['inv']['previous_package_id']	=	stripslashes($owner_package_id);
											$invoice_items_arr[$item_arr]['inv']['previous_package_info']	=	$owner_package_info;										
			
			$total_amount += $sub_total;
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $sub_total;
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;	
			$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] =   Zend_Json_Encoder::encode($invoice_items_arr[$item_arr]['inv']);		
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
			$item_arr++;
			//ITEM END
			
			
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
						<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
						<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("member_invoice_total_title").'</div>
						</td>
						<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>'.$currencySymbol.' '.$this->view->numbers(number_format(($total_amount+$total_tax), 2, '.', ',')).' '.$currencyShortName.'</strong></td>
					</tr>';	
			
					
						
			$services_charge = 0;
			
			$now_payable = 0;
			$deposit_charge = 0;
			
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
																</table>';
			
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
			$invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
			$invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
			$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
			$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;	
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
			$invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT]	=	$currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge)), 2, '.', ',').' '.$currencyShortName;
			
			
			//Initialize Invoice Action
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] 			= 'Members';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] 	= 'upgradePackage';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] 	= '';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] 	= 'deletePackage';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] 	= '';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] 	= 'paidPackage';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] 	= 'unpaidPackage';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] 	= 'cancelPackage';
			
			//Initialize Email Template
			$template_id_field	=	'default_template_id';
			$settings_db = new Invoice_Model_DbTable_Setting();
			$template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);			
			$invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("members_invoice_template_id");									
			
			$register_helper = new Members_Controller_Helper_Registers();					
			$templates_arr = $register_helper->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
			$invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
			$invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
			$invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;
			
			$return = array('status' => 'ok','invoice_arr' => $invoice_arr);
		
		return 	$return;
	}
	
	public function listAction()
	{
		$form_db = new Members_Model_DbTable_Forms();
		$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();
		$general_db =	new Members_Model_DbTable_General();
		
		//$form_id_info =  $form_db->getFormId($this->_translator->translator('package_form_name'));	
		$form_id_info[0]['id'] = ($this->_page_id) ? $this->_page_id : '9';
		if($form_id_info)
		{	
			$form_info = $form_db->getFormsInfo($form_id_info[0]['id']);
			$this->view->assign('form_info' , $form_info);		
			$general_info = $general_db->getAllPublishedValues($form_id_info[0]['id'],'1');
			if($general_info)
			{
				$packages_info = array();				
				$i = 0;
				foreach($general_info as $data)
				{	
					$packages_info[$i]['form_id'] = $form_id_info[0]['id'];
					$packages_info[$i]['g_id']	=		$data->id;								
					$packages_info[$i]	=	$dynamic_valu_db->getFieldsValueInfo($packages_info[$i],$data->id);
					$i++;
				}
			}
			else
			{
				$packages_info = null;
			}
		}
		else
		{
			$packages_info = null;
		}
		$this->view->assign('packages_info' , $packages_info);	
	}	
}