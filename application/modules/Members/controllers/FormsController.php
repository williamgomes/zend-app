<?php
//Dynamic Frontend Form controller class
class Members_FormsController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_auth_obj;	
	
    public function init()
    {
        /* Initialize action controller here */				
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);	
		$this->view->setEscape('stripslashes');	
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
			
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));	
		$this->view->front_template = $front_template;	
		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
	}
	
	public function deletedynamicimageAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		$this->view->translator	= $translator;
		
		if ($this->_request->isPost()) 
		{
			$file_name = $this->_request->getPost('file_name');
			$file_path = $this->_request->getPost('file_path');
			
			if(empty($file_name))
			{
				$msg = $translator->translator("insert_selected_file_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			else
			{
				if($file_name)
				{				
					$dir = $file_path.DS.$file_name;
					$res = Eicra_File_Utility::deleteRescursiveDir($dir);
				}
				
				if($res)
				{
					$msg = $translator->translator("file_delete_success",$file_name);
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				else
				{
					$msg = $translator->translator("file_delete_err",$file_name);
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
		}
		else
		{
			$msg = $translator->translator("file_delete_err");
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}	
	
	
	public function generalAction()
    {	
		$form_id = ($this->_page_id) ? $this->_page_id	: $this->_request->getParam('form_id');	
		if($form_id)
		{								
			$form_db = new Members_Model_DbTable_Forms();
			$form_info = $form_db->getFormsInfo($form_id);	
			$generalForm =  new Members_Form_GeneralForm ($form_info);
			
			if($form_info['login_set'] == '1')
			{
				$getAction = $this->_request->getActionName();
				if($getAction != 'uploadfile')
				{			
					$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
					Eicra_Global_Variable::checkSession($this->_response,$url);
				}
			}
			
			if ($this->_request->isPost()) 
			{	
				$this->_helper->viewRenderer->setNoRender();
				$this->_helper->layout->disableLayout();
				$translator = Zend_Registry::get('translator');				
				
				$elements = $generalForm->getElements();
				foreach($elements as $element)
				{
					if($element->getType() == 'Zend_Form_Element_File')
					{
						$element_name = $element->getName();
						$generalForm->removeElement($element_name);
					}
				}
				
				if ($generalForm->isValid($this->_request->getPost())) 
				{
					$generalForm =  new Members_Form_GeneralForm ($form_info);
					$generalForm->populate($this->_request->getPost());
					$fromValues = $generalForm;	
					if($form_info['db_set'] == '1')
					{
						$general = new Members_Model_General(array('form_id' => $form_info['id'],'active' => '2', 'display_type'	=>	'display_frontend'));
						$result = $general->saveGeneral();
					}
					else
					{
						$result['status'] = 'ok';
						$result['id'] = 0;
					}
					
					if($result['status'] == 'ok')
					{				
						$field_db = new Members_Model_DbTable_Fields();
						$field_groups = $field_db->getGroupNames($form_info['id']); 
						
						//Add Data To Database						
						$attach_file_arr = array();
						foreach($field_groups as $group)
						{
							$group_name = $group->field_group;
							$displaGroup = $generalForm->getDisplayGroup($group_name);
							$elementsObj = $displaGroup->getElements();
							foreach($elementsObj as $element)
							{
								$table_id = $result['id'];
								$form_id = $form_info['id'];
								$field_id	=	$element->getAttrib('rel');
								$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
								if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
								{ 
									if(is_array($field_value)) { $field_value = ''; }
								}
								if($element->getType() == 'Zend_Form_Element_Multiselect') 
								{ 
									$field_value	=	$this->_request->getPost($element->getName());
									if(is_array($field_value)) { $field_value = implode(',',$field_value); }
								}
								if($element->getType() == 'Zend_Form_Element_File') 
								{ 
									$post_data = $this->_request->getPost($element->getName()); 
									$post_data_arr = explode(',',$post_data);									
									$attach_file_arr =  array_merge((array)$attach_file_arr, (array)$post_data_arr);
								}
								try
								{
									$msg = $translator->translator("member_form_submit_successfull");
									$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
									
									if($form_info['db_set'] == '1')
									{
										$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
										$this->_DBconn = Zend_Registry::get('msqli_connection');
										$this->_DBconn->getConnection();
										$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
										
										$msg = $translator->translator("member_form_submit_successfull");
										$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
									}								
									
								}
								catch(Exception $e)
								{
									$msg = $translator->translator("member_form_submit_err");
									$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
								}
							}						
						}
						
						//Send Data To Email
						if($form_info['email_set'] == '1')
						{
							$register_helper = new Members_Controller_Helper_Registers();	
							$allDatas = $fromValues->getValues();	
							try 
							{								
								$from_mail_field = Eicra_File_Constants::FROM_MAIL;
								$to_mail_field = Eicra_File_Constants::TO_MAIL;
								$allDatas[$from_mail_field] = trim($this->_request->getPost($from_mail_field));
								$to_mail	=	trim($this->_request->getPost($to_mail_field));
								
								if($to_mail)
								{
									$form_info['set_from_email'] = $to_mail;
									$send_result = $register_helper->sendEmailToFriend($allDatas,$form_info,$attach_file_arr,$to_mail);
								}
								$register_helper->sendGeneralMail($allDatas,$form_info,$attach_file_arr);
								$msg = $translator->translator("member_form_submit_successfull");
								$json_arr = array('status' => 'ok','msg' => $msg, 'attach_file_arr' => $attach_file_arr, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
							}
							catch (Exception $e) 
							{
								$msg = $e->getMessage();
								$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
							}
						}
						
						//Delete Attached Files
						if($form_info['attach_file_delete'] == '1')
						{
							$elements = $generalForm->getElements();
							foreach($elements as $element)
							{
								if($element->getType() == 'Zend_Form_Element_File')
								{
									$element_name = $element->getName();
									$element_value	= $this->_request->getPost($element_name);
									if(!empty($element_value))
									{
										$element_value_arr = explode(',',$element_value);	
										foreach($element_value_arr as $key => $e_value)
										{
											if(!empty($e_value))
											{
												$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
												$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
											}
										}
									}
								}
							}
						}
					}
					else
					{
						$json_arr = array('status' => 'err','msg' => $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
					}												
				}
				else
				{
					$validatorMsg = $generalForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
				}			
				$res_value = Zend_Json_Encoder::encode($json_arr);			
				$this->_response->setBody($res_value);
			}
			
			$this->view->form_info	= $form_info;
			$this->view->generalForm = $generalForm;
			$this->dynamicUploaderSettings($this->view->form_info);	
		}
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	public function uploadfileAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			if($this->_request->getParam('form_id'))
			{
				try
				{
					$form_db = new Members_Model_DbTable_Forms();
					$form_info = $form_db->getFormsInfo($this->_request->getParam('form_id'));
					
					$path = $form_info['attach_file_path'];					
					$generalForm =  new Members_Form_GeneralForm ($form_info);				
					
					$field_name	=	$this->_request->getParam('field_name');
					$upload_field = $generalForm->getElement($field_name);
						
					Members_Controller_Helper_FileRename::fileRename($upload_field, $path);
					
					$Filename = $upload_field->getFileName();
					$ext = Eicra_File_Utility::GetExtension($Filename);
					$option['file_type']	=	$form_info['attach_file_type'];
					$file_obj = new Members_Controller_Helper_File($path,$Filename,$ext,$option);
					
					if($file_obj->checkThumbExt())
					{
						if($upload_field->receive())
						{													
							$msg = $translator->translator('File_upload_success',$upload_field->getFileName(null,false));		
							$json_arr = array('status' => 'ok','msg' => $msg, 'newName' => $upload_field->getFileName(null,false));		
						}
						else
						{
							$validatorMsg = $upload_field->getMessages();
							$vMsg = implode("\n", $validatorMsg);	
							$json_arr = array('status' => 'err','msg' => $vMsg, 'newName' => $upload_field->getFileName(null,false));				
						}
					}
					else
					{
						$msg = $translator->translator('File_upload_ext_err',$ext, 'Settings');
						$json_arr = array('status' => 'err','msg' => $msg, 'newName' => $upload_field->getFileName(null,false));
					}							
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err','msg' => $e->getMessage());	
				}				
			}
			else
			{
				$json_arr = array('status' => 'err','msg' => 'parameter not found');	
			}					
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);				
		$this->_response->setBody($res_value);	
	}		
}