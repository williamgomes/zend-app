<?php

//Members Frontend controller
class Members_FrontendController extends Zend_Controller_Action {

    private $_DBconn;
    private $loginForm;
    private $ckLicense = true;
    private $curlObj;
    private $_page_id;
    private $_controllerCache;
    private $_translator;

    public function init() {
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);



        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');

        /* Initialize action controller here */
        $this->loginForm = new Members_Form_LoginForm();

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        //DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();

        $this->view->menu_id = $this->_request->getParam('menu_id');
        $getAction = $this->_request->getActionName();

        if ($getAction != 'register') {
            if ($this->ckLicense == true) {
                $this->curlObj = new Eicra_License_Version();
                $this->curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
                $license = new Zend_Session_Namespace('License');
                $license->license_data = $this->curlObj->getArrayResult();
            }
        } else if ($getAction == 'register') {
            if ($this->_request->getParam('menu_id')) {
                $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
                $page_id_arr = $viewHelper->_getContentId();

                $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
            } else {
                $this->_page_id = null;
            }
        }
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;
    }

    public function tokenAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->loginForm->token->getSession()->hash = $this->loginForm->token->getHash();
        $json_arr = array('status' => 'ok', 'token' => $this->loginForm->token->getHash(), 'session' => $this->loginForm->token->getSession()->hash);
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function loginAction() {
        $this->view->logindetails = $this->loginForm;
    }

    public function authAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        $mem_db = new Members_Model_DbTable_MemberList();
        $loginHelper = new Administrator_Controller_Helper_LoginHelper();
        $datas['username'] = $this->getRequest()->getPost('username');
        $datas['password'] = str_replace(' ', '', trim($this->getRequest()->getPost('password')));
        $datas['adminurl'] = $this->view->serverUrl() . $this->view->baseUrl();

        if ($this->loginForm->isValid($this->getRequest()->getPost())) {
            $this->loginForm->password->setValue($datas['password']);

            $ckLicense = $this->ckLicense;

            if ($ckLicense == true) {
                $valid = ($this->curlObj->isLicensed() && $ckLicense == true) ? true : true;
            } else {
                $valid = true;
            }

            if ($valid == false) {
                $json_arr = array('status' => 'err', 'msg' => $this->curlObj->getError(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
                $datas['failedCause'] = $this->curlObj->getError();
                if ($mem_db->isAdministrator($datas['username'])) {
                    $loginHelper->sendMail($datas);
                }
            } else {
                // Get our authentication adapter and check credentials						
                $adapter = $this->getAuthAdapter($this->loginForm->getValues());
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($adapter);

                if (!$result->isValid()) {
                    $pending_error = $this->checkPendingError($datas);

                    // Invalid credentials	
                    if ($pending_error) {
                        $msg = $translator->translator("member_credentials_pending_err");
                    } else if ($this->checkFrontendLoginError($datas)) {
                        $msg = $translator->translator("member_credentials_frontend_login_err");
                    } else {
                        if ($mem_db->isAdministrator($datas['username'])) {
                            $msg = $translator->translator("member_credentials_err");
                        } else {
                            $msg = $translator->translator("member_login_err");
                        }
                    }
                    $datas['failedCause'] = $msg;
                    if ($mem_db->isAdministrator($datas['username'])) {
                        $loginHelper->sendMail($datas);
                    }
                    $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
                } else {
                    if ($loginHelper->checkIP($adapter)) {
                        Eicra_Global_Variable::getSession()->username = $result->getIdentity();
                        $users = new Administrator_Model_DbTable_Users();

                        $data = array('last_access' => date('Y-m-d H:i:s'));
                        $where = $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
                        if (!$users->update($data, $where)) {
                            $msg = $translator->translator("member_last_access_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
                        } else {
                            $userInfo = $adapter->getResultRowObject(null, array('password'));
                            $authStorage = $auth->getStorage();
                            $authStorage->write($userInfo);

                            $msg = $translator->translator("member_loggin_successfull", Eicra_Global_Variable::getSession()->username);
                            $json_arr = array('status' => 'ok', 'msg' => $msg, 'role_id' => $userInfo->role_id, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
                        }
                    } else {
                        $msg = $translator->translator('login_incorrect_for_ip_block', $loginHelper->returnIP());
                        $auth->clearIdentity();
                        $datas['failedCause'] = $msg;
                        if ($mem_db->isAdministrator($datas['username'])) {
                            $loginHelper->sendMail($datas);
                        }
                        $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
                    }
                }
            }
        } else {
            $validatorMsg = $this->loginForm->getMessages();
            $vMsg = array();
            $i = 0;
            foreach ($validatorMsg as $key => $errType) {
                foreach ($errType as $errkey => $value) {
                    $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                    $Msg .= $value . "&nbsp;";
                    $i++;
                }
            }
            $datas['failedCause'] = $Msg;
            if ($mem_db->isAdministrator($datas['username'])) {
                $loginHelper->sendMail($datas);
            }
            $json_arr = array('status' => 'errV', 'msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
        }
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function fbauthAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        $username = $this->_request->getPost('username');
        $user_check = new Members_Controller_Helper_Registers();
        $authdata['username'] = $username;
        $authdata['password'] = $this->_request->getPost('password');
        if (!$username) {
            $json_arr = array('status' => 'err', 'msg' => $translator->translator("members_fb_data_fetch_error"));
        } else if ($user_check->getUsernameAvailable($username)) {
            // New user. Complete registration, thne just allow to access as normal login 	
            $countrObj = new Geo_Model_DbTable_Country();
            $search_params['filter']['filters'][] = array('field' => 'value', 'operator' => 'startswith', 'value' => trim($this->_request->getPost('country')));
            $country_info = $countrObj->getListInfo(null, $search_params);
            $country_info_array = $country_info->toArray();
            $retrived_data_from_fb = $this->_request->getPost();
            $retrived_data_from_fb['country'] = $country_info_array[0]['id'];
            $members = new Members_Model_Members($retrived_data_from_fb);
            $members->saveRegister();
            $adapter = $this->getAuthAdapter($authdata);
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter); // 'result' always true because this function will tigger only fb credential succeeded

            Eicra_Global_Variable::getSession()->username = $result->getIdentity();
            $users = new Administrator_Model_DbTable_Users();

            $data = array('last_access' => date('Y-m-d H:i:s'));
            $where = $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
            $users->update($data, $where);
            $userInfo = $adapter->getResultRowObject(null, array('password', 'salt'));
            $authStorage = $auth->getStorage();
            $authStorage->write($userInfo);

            if ($userInfo->allow_to_send_email == '1') {
                $register_helper = new Members_Controller_Helper_Registers();
                $allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
                $allDatas['username'] = $members->getUsername();
                $allDatas['password'] = $members->getReal_pass();
                $allDatas['loginurl'] = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
                $allDatas['title'] = $members->getTitle();
                $allDatas['firstName'] = $members->getFirstName();
                $allDatas['lastName'] = $members->getLastName();
                $allDatas['member_package'] = '';

                try {
                    $register_helper->sendMail($allDatas, null, null);
                } catch (Exception $e) {
                    $mail_msg = $e->getMessage();
                }
            }

            $msg = $translator->translator("member_loggin_successfull", Eicra_Global_Variable::getSession()->username);
            $json_arr = array('status' => 'ok', 'msg' => $msg);
        } else {
            // user already registered, just allow to access as normal login 			
            $adapter = $this->getFbAuthAdapter($authdata);
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter); // 'result' always true because this function will tigger only fb credential succeeded

            Eicra_Global_Variable::getSession()->username = $result->getIdentity();
            $users = new Administrator_Model_DbTable_Users();

            $data = array('last_access' => date('Y-m-d H:i:s'));
            $where = $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
            $users->update($data, $where);
            $userInfo = $adapter->getResultRowObject(null, array('password', 'salt'));
            $authStorage = $auth->getStorage();
            $authStorage->write($userInfo);

            $msg = $translator->translator("member_loggin_successfull", Eicra_Global_Variable::getSession()->username);
            $json_arr = array('status' => 'ok', 'msg' => $msg);
        }
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    private function checkPendingError($datas) {
        $validator = new Zend_Validate_Db_RecordExists(
                array(
            'table' => Zend_Registry::get('dbPrefix') . 'user_profile',
            'field' => 'status',
            'exclude' => 'username = "' . $datas['username'] . '" AND  password = MD5(CONCAT(salt,"' . $datas['password'] . '"))',
                )
        );
        return $validator->isValid(0);
    }

    private function checkFrontendLoginError($datas) {
        $mem_db = new Members_Model_DbTable_MemberList();
        $mem_info = $mem_db->getMemberInfoByUsername($datas['username']);
        if ($mem_info && $mem_info['role_id']) {
            $validator = new Zend_Validate_Db_RecordExists(
                    array(
                'table' => Zend_Registry::get('dbPrefix') . 'roles',
                'field' => 'allow_login_from_frontend',
                'exclude' => 'role_id = "' . $mem_info['role_id'] . '" ',
                    )
            );
            return $validator->isValid(0);
        } else {
            return false;
        }
    }

    public function logoutAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        try {

            if (Zend_Auth::getInstance()->hasIdentity()) {
                $username = Eicra_Global_Variable::getSession()->username;
                Zend_Auth::getInstance()->clearIdentity();

                if (!empty(Eicra_Global_Variable::getSession()->username)) {
                    Eicra_Global_Variable::clearSession();
                }
                $msg = $translator->translator("member_logout_successfull", $username);
                $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
            } else {
                $msg = $translator->translator("member_logout_already");
                $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
            }
        } catch (Exception $e) {

            $json_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($this->loginForm));
        }

        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    protected function getAuthAdapter($values) {
        $tbl = Zend_Registry::get('config')->eicra->params->tblprefix . 'user_profile';
        $dbAdapter = Zend_Registry::get('msqli_connection');
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter = new Zend_Auth_Adapter_DbTable(
                $dbAdapter, array('mt' => $tbl), 'mt.username', 'mt.password', 'MD5(CONCAT(mt.salt,?)) AND mt.status=1'
        );
        $authSelect = $authAdapter->getDbSelect();
        $authSelect->joinLeft(array('r' => Zend_Registry::get('dbPrefix') . 'roles'), 'mt.role_id = r.role_id', array('*'));
        $authSelect->where('r.allow_login_from_frontend = ?', '1');
        $authAdapter->setIdentity($values['username']);
        $authAdapter->setCredential($values['password']);

        return $authAdapter;
    }

    protected function getFbAuthAdapter($values) {
        $tbl = Zend_Registry::get('config')->eicra->params->tblprefix . 'user_profile';
        $dbAdapter = Zend_Registry::get('msqli_connection');
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter = new Zend_Auth_Adapter_DbTable(
                $dbAdapter, array('mt' => $tbl), 'mt.username', 'mt.username', 'mt.status=1'
        );
        $authSelect = $authAdapter->getDbSelect();
        $authSelect->joinLeft(array('r' => Zend_Registry::get('dbPrefix') . 'roles'), 'mt.role_id = r.role_id', array('*'));
        $authSelect->where('r.allow_login_from_frontend = ?', '1');
        $authAdapter->setIdentity($values['username']);
        $authAdapter->setCredential($values['username']);

        return $authAdapter;
    }

    public function typeAction() {

        $groupForm = new Members_Form_GroupForm ();
        $this->view->groupForm = $groupForm;
    }

    public function registerAction() {
        // BreadCrumb
        Eicra_Global_Variable::getSession()->breadcrumb = array(
            array(
                $this->_translator->translator(
                        'common_registration_title'),
                $this->view->url()));

        $global_conf = Zend_Registry::get('global_conf');
        if ($this->_page_id) {
            $selected_role_id = $this->_page_id;
        } else {
            $selected_role_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $this->_request->getParam('role_id');
        }
        if (!empty($selected_role_id)) {
            try {
                $role_db = new Members_Model_DbTable_Role();
                $role_info = $role_db->getRoleInfo($selected_role_id);
                $form_db = new Members_Model_DbTable_Forms();
                $form_info = $form_db->getFormsInfo($role_info['form_id']);
                $role_info['display_type'] = 'display_frontend';
                $this->view->form_info = $form_info;
                $this->view->role_info = $role_info;
            } catch (Exception $e) {
                $role_info = null;
            }
        } else {
            $role_info = null;
        }

        $registrationForm = new Members_Form_UserForm($role_info);
        if ($registrationForm->role_id) {
            $registrationForm->removeElement('role_id');
        }
        $loginUrl = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $elements = $registrationForm->getElements();
            foreach ($elements as $element) {
                $element_name = $element->getName();
                if ($element->getType() == 'Zend_Form_Element_File') {
                    $registrationForm->removeElement($element_name);
                }
                $element_name_arr = explode('_', $element_name);
                if ($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE, $element_name_arr, true)) {
                    $package_msg = 'ok';
                }
            }

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $translator = Zend_Registry::get('translator');
            try {
                if ($role_info['role_lock'] == '1' && $role_info != null) {

                    if ($registrationForm->isValid($this->_request->getPost())) {
                        $registrationForm = new Members_Form_UserForm($role_info);
                        $registrationForm->populate($this->_request->getPost());
                        $username = $this->_request->getPost('username');
                        $user_check = new Members_Controller_Helper_Registers();

                        if ($user_check->getUsernameAvailable($username)) {
                            if ($this->_request->getPost('password') == $this->_request->getPost('confirmPassword')) {
                                $fromValues = $registrationForm;
                                $members = new Members_Model_Members($this->_request->getPost());
                                $perm = new Members_View_Helper_Allow();
                                $members->setRole_id($selected_role_id);
                                if ($role_info) {
                                    if ($role_info['auto_approve'] == '1') {
                                        $members->setStatus(1);
                                    } else {
                                        $members->setStatus(0);
                                    }
                                } else {
                                    $members->setStatus($translator->translator("set_frontend_registration_auto_publish"));
                                }
                                $members->setLoginurl($loginUrl);

                                $result = $members->saveRegister();

                                if ($result['status'] == 'ok') {
                                    $member_id = $result['id'];
                                    $msg = $translator->translator("member_registered_successfull");
                                    $json_arr = array('status' => 'ok', 'msg' => $msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                    if (!empty($role_info) && !empty($role_info['form_id'])) {
                                        $field_db = new Members_Model_DbTable_Fields();
                                        $packageObj = new Members_Controller_Helper_Packages();
                                        $field_groups = $field_db->getGroupNames($form_info['id']);

                                        //Add Data To Database
                                        foreach ($field_groups as $group) {
                                            $group_name = $group->field_group;
                                            $displaGroup = $registrationForm->getDisplayGroup($group_name);
                                            $elementsObj = $displaGroup->getElements();
                                            foreach ($elementsObj as $element) {
                                                if (!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select')) {
                                                    $table_id = $result['id'];
                                                    $form_id = $form_info['id'];
                                                    $field_id = $element->getAttrib('rel');
                                                    $field_value = ($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
                                                    if ($element->getType() == 'Zend_Form_Element_MultiCheckbox') {
                                                        if (is_array($field_value)) {
                                                            $field_value = '';
                                                        }
                                                    }
                                                    if ($element->getType() == 'Zend_Form_Element_Multiselect') {
                                                        $field_value = $this->_request->getPost($element->getName());
                                                        if (is_array($field_value)) {
                                                            $field_value = implode(',', $field_value);
                                                        }
                                                    }
                                                    try {
                                                        $data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
                                                        $DBconn = Zend_Registry::get('msqli_connection');
                                                        $DBconn->getConnection();
                                                        $DBconn->insert(Zend_Registry::get('dbPrefix') . 'forms_fields_values', $data);
                                                        if ($package_msg == 'ok') {
                                                            $e_name = $element->getName();
                                                            $element_name_arr = explode('_', $e_name);

                                                            if ($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE, $element_name_arr, true)) {
                                                                $package_info = $packageObj->getFormAllDatas($field_value);
                                                            }
                                                        }
                                                        $msg = $translator->translator("member_registered_successfull");
                                                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                                    } catch (Exception $e) {
                                                        $msg = $translator->translator("member_registered_err");
                                                        $json_arr = array('status' => 'err', 'msg' => $msg . ' ' . $e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                                    }
                                                }
                                            }
                                        }

                                        //Email Send
                                        $register_helper = new Members_Controller_Helper_Registers();
                                        $field_info = $field_db->getFieldsInfo($role_info['form_id']);
                                        $allDatas = $members->getAllDatas();

                                        if ($field_info) {
                                            $atta_count = 0;
                                            $attach_file_arr = array();
                                            foreach ($field_info as $element) {
                                                if ($element->field_type == 'file') {
                                                    $attach_file_arr[$atta_count] = $allDatas[$element->field_name];
                                                    $atta_count++;
                                                }
                                            }
                                        } else {
                                            $attach_file_arr = null;
                                        }

                                        $allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
                                        $allDatas['password'] = $members->getReal_pass();
                                        $allDatas['loginurl'] = $members->getLoginurl();
                                        try {
                                            if (!empty($allDatas['member_package'])) {
                                                if ($registrationForm->member_package) {
                                                    $allDatas['member_package'] = $registrationForm->member_package->getMultiOption($allDatas['member_package']);
                                                } else {
                                                    $allDatas['member_package'] = '';
                                                }
                                            } else {
                                                $allDatas['member_package'] = '';
                                            }
                                            if ($role_info['allow_to_send_email'] == '1') {
                                                $register_helper->sendMail($allDatas, $form_info, $attach_file_arr);
                                            }
                                        } catch (Exception $e) {
                                            $mail_msg = $e->getMessage();
                                        }
                                        //Delete Attached Files
                                        if ($form_info['attach_file_delete'] == '1') {
                                            if ($attach_file_arr != null) {
                                                foreach ($attach_file_arr as $key => $value) {
                                                    if (!empty($value)) {
                                                        $element_value_arr = explode(',', $value);
                                                        foreach ($element_value_arr as $element_value_arr_key => $e_value) {
                                                            if (!empty($e_value)) {
                                                                $dir = BASE_PATH . DS . $form_info['attach_file_path'] . DS . $e_value;
                                                                $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (!empty($package_msg) && !empty($package_info)) {
                                            $gateway_db = new Paymentgateway_Model_DbTable_Gateway();
                                            $gateway_info = $gateway_db->getAllActiveGateway('ASC');
                                            $gateway_info = ($gateway_info && ($global_conf['payment_system'] == 'On')) ? $gateway_info->toArray() : '';
                                            $members->setUser_id($member_id);
                                            $invoice_arr = $this->generateInvoiceOld($members, $role_info, $package_info);
                                            if ($invoice_arr['status'] == 'ok') {
                                                $pacakge_price = round($invoice_arr['invoice_arr']['pacakge_price']);
                                                if (!empty($pacakge_price)) {
                                                    Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];
                                                    $json_arr = array('status' => 'ok', 'msg' => $msg . ' ' . $mail_msg, 'package_msg' => 'invoice', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                                }
                                            } else {
                                                $json_arr = array('status' => 'ok', 'msg' => $msg . ' ' . $mail_msg, 'package_msg' => $package_msg, 'gateway_info' => $gateway_info, 'member_id' => $member_id, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                            }
                                        } else {
                                            $json_arr = array('status' => 'ok', 'msg' => $msg . ' ' . $mail_msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                        }
                                        $package_id = $members->getPackage_id();
                                        if (!empty($package_id)) {
                                            $members->setUser_id($member_id);
                                            $invoice_arr = $this->generateInvoice($members, $role_info);
                                            if ($invoice_arr['status'] == 'ok') {
                                                $pacakge_price = round($invoice_arr['invoice_arr']['pacakge_price']);
                                                if (!empty($pacakge_price)) {
                                                    Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];
                                                    $json_arr = array('status' => 'ok', 'msg' => $msg . ' ' . $mail_msg, 'package_msg' => 'invoice', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                                }
                                            }
                                        }
                                    } else {
                                        if ($role_info['allow_to_send_email'] == '1') {
                                            $register_helper = new Members_Controller_Helper_Registers();
                                            $allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
                                            $allDatas['username'] = $members->getUsername();
                                            $allDatas['password'] = $members->getReal_pass();
                                            $allDatas['loginurl'] = $members->getLoginurl();
                                            $allDatas['title'] = $members->getTitle();
                                            $allDatas['firstName'] = $members->getFirstName();
                                            $allDatas['lastName'] = $members->getLastName();
                                            $allDatas['member_package'] = '';

                                            try {
                                                $register_helper->sendMail($allDatas, null, null);
                                            } catch (Exception $e) {
                                                $mail_msg = $e->getMessage();
                                            }
                                        }
                                    }
                                } else {
                                    $msg = $translator->translator("member_registered_err");
                                    $json_arr = array('status' => 'err', 'msg' => $msg . ' ' . $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                                }
                            } else {
                                $msg = $translator->translator("password_not_match");
                                $json_arr = array('status' => 'errP', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                            }
                        } else {
                            $msg = $translator->translator("member_availability", $username);
                            $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                        }
                    } else {
                        $validatorMsg = $registrationForm->getMessages();
                        $vMsg = array();
                        $i = 0;
                        foreach ($validatorMsg as $key => $errType) {
                            foreach ($errType as $errkey => $value) {
                                $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                                $i++;
                            }
                        }
                        $json_arr = array('status' => 'errV', 'msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                    }
                } else {
                    $msg = $translator->translator("member_registered_not_permitted", $role_info['role_name']);
                    $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
                }
            } catch (Exception $e) {
                $msg = $e->getMessage();
                $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if ($role_info['allow_register_to_this_role'] == '1' && $role_info != null) {
                $this->view->selected_role_id = $selected_role_id;
                $this->view->registrationForm = $registrationForm;
            } else {
                throw new Exception("Register page not found");
            }
        }
        $this->dynamicUploaderSettings($this->view->form_info);
    }

    public function generateInvoiceOld($members_info, $role_info, $packageInfo) {
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $currencySymbol = $currency->getSymbol();
        $currencyShortName = $currency->getShortName();
        $payment_db = new Paymentgateway_Model_DbTable_Gateway();
        $payment_info = $payment_db->getDefaultGateway();
        $country_id = $members_info->getCountry();
        if (!empty($country_id)) {
            $country_db = new Eicra_Model_DbTable_Country();
            $country_info = $country_db->getInfo($country_id);
        }
        if ($packageInfo) {
            $field_db = new Members_Model_DbTable_Fields();
            $field_info = $field_db->getFieldsInfo($packageInfo[0]['form_id']);
            if ($field_info) {
                foreach ($field_info as $field_info_key => $field_info_arr) {
                    $fieldInfoArr[$field_info_arr['id']] = $field_info_arr['field_name'];
                }
                foreach ($packageInfo as $packageInfoKey => $packageInfoArr) {
                    $package_info[$fieldInfoArr[$packageInfoArr['field_id']]] = $packageInfoArr['field_value'];
                }
            }
        }
        //invoice_arr assigning started
        $invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($members_info->getTitle()) . ' ' . stripslashes($members_info->getFirstName()) . ' ' . stripslashes($members_info->getLastName()) . '<br />' . stripslashes($members_info->getMobile()) . '<br />' . stripslashes($members_info->getCity()) . ', ' . stripslashes($members_info->getState()) . ', ' . stripslashes($members_info->getPostalCode()) . '<br />' . stripslashes($country_info['value']) . '<BR />' . stripslashes($members_info->getUsername());
        $invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
        $invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
        $invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = date("Y-m-d h:i:s");
        $invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y", strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
        $invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $members_info->getUsername();
        $invoice_arr[Eicra_File_Constants::INVOICE_USER_ID] = $members_info->getUser_id();

        $invoice_arr['site_name'] = stripslashes($global_conf['site_name']);
        $invoice_arr['site_url'] = stripslashes($global_conf['site_url']);
        $invoice_arr['title'] = $members_info->getTitle();
        $invoice_arr['firstName'] = $members_info->getFirstName();
        $invoice_arr['lastName'] = $members_info->getLastName();
        $invoice_arr['status'] = ($members_info->getStatus() == '1') ? 'active' : 'inactive';
        ;
        $invoice_arr['password'] = $members_info->getReal_pass();
        $invoice_arr['loginurl'] = $members_info->getLoginurl();


        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
		<tbody>
			<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
				<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_desc_title", '', 'Invoice') . '</strong></td>
				<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_amount_title", '', 'Invoice') . '</strong></td>
			</tr>';
        //Price Management
        $total_amount = ($package_info && $package_info['package_price']) ? $package_info['package_price'] : 0;
        $invoice_arr['pacakge_price'] = $total_amount;
        $invoice_arr[Eicra_File_Constants::INVOICE_MEMBER_PACKAGE] = $package_info[Eicra_File_Constants::INVOICE_MEMBER_PACKAGE];

        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr><td colspan="2" style="height:1px"></td></tr>
			<tr>
				<td id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
					' . $this->view->escape($invoice_arr[Eicra_File_Constants::INVOICE_MEMBER_PACKAGE]) . '
				</td>
				<td style="font-size:17px; margin:10px auto 2px auto; padding:10px 10px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0; text-align:right;">
					' . $currencySymbol . ' ' . $invoice_arr['pacakge_price'] . ' ' . $currencyShortName . '
				</td>
			</tr>';


        $total_tax = 0;
        $services_charge = 0;
        if (Settings_Service_Price::is_exists('4', $global_conf, $total_amount)) {
            $services_charge = $this->view->price($total_amount, $global_conf, '4');
            $services_charge_margine = Settings_Service_Price::getMargine('4');
            $services_charge_margine_show = (preg_match("/%/i", $services_charge_margine)) ? $currencySymbol . ' ' . $services_charge . ' ' . $services_charge_margine : $services_charge_margine;
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_service_charge") . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $services_charge_margine_show . '</strong></td>' .
                    '</tr>';
        }
        $now_payable = 0;
        $deposit_charge = 0;
        if (Settings_Service_Price::is_exists('5', $global_conf, $total_amount)) {
            $deposit_charge = $this->view->price($total_amount, null, '5');
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_deposit_charge", Settings_Service_Price::getMargine('5')) . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($deposit_charge, 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';

            $now_payable = $services_charge + $deposit_charge;

            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_deposit_payable") . '</td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($now_payable, 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';
        }
        if (Settings_Service_Price::is_exists('4', $global_conf, $total_amount)) {
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_grand_total") . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format(($total_amount + $total_tax + $services_charge), 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';
        }
        if (Settings_Service_Price::is_exists('5', $global_conf, $total_amount) && !empty($now_payable)) {
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">' .
                    '<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right; font-weight:bold;" height="40" colspan="2">' . $this->_translator->translator("members_invoice_later_payable", $currencySymbol . ' ' . number_format((($total_amount + $total_tax + $services_charge) - $now_payable), 2, '.', ',') . ' ' . $currencyShortName) . '</td>' .
                    '</tr>';
        }
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
		</table>';

        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;
        $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
        $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format((($total_amount + $total_tax + $services_charge)), 2, '.', ',') . ' ' . $currencyShortName;

        //Initialize Invoice Action
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] = 'Members';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] = 'createItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] = 'deleteItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] = 'paidItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] = 'unpaidItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] = 'cancelItinerary';

        //Initialize Email Template
        $template_id_field = 'default_template_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
        $invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("members_invoice_template_id");

        $templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
        $invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
        $invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
        $invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;

        $return = array('status' => 'ok', 'invoice_arr' => $invoice_arr);

        return $return;
    }

    public function generateInvoice($members_info, $role_info) {
        $global_conf = Zend_Registry::get('global_conf');
        $currency = new Zend_Currency($global_conf['default_locale']);
        $currencySymbol = $currency->getSymbol();
        $currencyShortName = $currency->getShortName();
        $payment_db = new Paymentgateway_Model_DbTable_Gateway();
        $payment_info = $payment_db->getDefaultGateway();
        $country_id = $members_info->getCountry();
        if (!empty($country_id)) {
            $country_db = new Eicra_Model_DbTable_Country();
            $country_info = $country_db->getInfo($country_id);
        }
        $package_id = $members_info->getPackage_id();
        if (!empty($package_id)) {
            $package_db = new Members_Model_DbTable_Package();
            $package_info = $package_db->getPackageById($package_id);
        }

        //invoice_arr assigning started
        $invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($members_info->getTitle()) . ' ' . stripslashes($members_info->getFirstName()) . ' ' . stripslashes($members_info->getLastName()) . '<br />' . stripslashes($members_info->getMobile()) . '<br />' . stripslashes($members_info->getCity()) . ', ' . stripslashes($members_info->getState()) . ', ' . stripslashes($members_info->getPostalCode()) . '<br />' . stripslashes($country_info['value']) . '<BR />' . stripslashes($members_info->getUsername());
        $invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
        $invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
        $invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = date("Y-m-d h:i:s");
        $invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y", strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
        $invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $members_info->getUsername();
        $invoice_arr[Eicra_File_Constants::INVOICE_USER_ID] = $members_info->getUser_id();

        $invoice_arr['site_name'] = stripslashes($global_conf['site_name']);
        $invoice_arr['site_url'] = stripslashes($global_conf['site_url']);
        $invoice_arr['title'] = $members_info->getTitle();
        $invoice_arr['firstName'] = $members_info->getFirstName();
        $invoice_arr['lastName'] = $members_info->getLastName();
        $invoice_arr['status'] = ($members_info->getStatus() == '1') ? 'active' : 'inactive';
        ;
        $invoice_arr['password'] = $members_info->getReal_pass();
        $invoice_arr['loginurl'] = $members_info->getLoginurl();


        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
		<tbody>
			<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
				<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_desc_title", '', 'Invoice') . '</strong></td>
				<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>' . $this->_translator->translator("invoice_amount_title", '', 'Invoice') . '</strong></td>
			</tr>';
        //Price Management
        $total_amount = ($package_info && $package_info['price']) ? $package_info['price'] : 0;
        $invoice_arr['pacakge_price'] = $total_amount;
        $invoice_arr[Eicra_File_Constants::INVOICE_MEMBER_PACKAGE] = $package_info['description'];

        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr><td colspan="2" style="height:1px"></td></tr>
			<tr>
				<td id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
					' . $this->view->escape($invoice_arr[Eicra_File_Constants::INVOICE_MEMBER_PACKAGE]) . '
				</td>
				<td style="font-size:17px; margin:10px auto 2px auto; padding:10px 10px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0; text-align:right;">
					' . $currencySymbol . ' ' . $invoice_arr['pacakge_price'] . ' ' . $currencyShortName . '
				</td>
			</tr>';


        $total_tax = 0;
        $services_charge = 0;
        if (Settings_Service_Price::is_exists('4', $global_conf, $total_amount)) {
            $services_charge = $this->view->price($total_amount, $global_conf, '4');
            $services_charge_margine = Settings_Service_Price::getMargine('4');
            $services_charge_margine_show = (preg_match("/%/i", $services_charge_margine)) ? $currencySymbol . ' ' . $services_charge . ' ' . $services_charge_margine : $services_charge_margine;
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_service_charge") . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $services_charge_margine_show . '</strong></td>' .
                    '</tr>';
        }
        $now_payable = 0;
        $deposit_charge = 0;
        if (Settings_Service_Price::is_exists('5', $global_conf, $total_amount)) {
            $deposit_charge = $this->view->price($total_amount, null, '5');
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_deposit_charge", Settings_Service_Price::getMargine('5')) . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($deposit_charge, 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';

            $now_payable = $services_charge + $deposit_charge;

            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_deposit_payable") . '</td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format($now_payable, 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';
        }
        if (Settings_Service_Price::is_exists('4', $global_conf, $total_amount)) {
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">' .
                    '<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("members_invoice_grand_total") . '</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>' . $currencySymbol . ' ' . number_format(($total_amount + $total_tax + $services_charge), 2, '.', ',') . ' ' . $currencyShortName . '</strong></td>' .
                    '</tr>';
        }
        if (Settings_Service_Price::is_exists('5', $global_conf, $total_amount) && !empty($now_payable)) {
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">' .
                    '<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right; font-weight:bold;" height="40" colspan="2">' . $this->_translator->translator("members_invoice_later_payable", $currencySymbol . ' ' . number_format((($total_amount + $total_tax + $services_charge) - $now_payable), 2, '.', ',') . ' ' . $currencyShortName) . '</td>' .
                    '</tr>';
        }
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
		</table>';

        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
        $invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;
        $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
        $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format((($total_amount + $total_tax + $services_charge)), 2, '.', ',') . ' ' . $currencyShortName;

        //Initialize Invoice Action
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] = 'Members';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] = 'createItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] = 'deleteItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] = '';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] = 'paidItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] = 'unpaidItinerary';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] = 'cancelItinerary';

        //Initialize Email Template
        $template_id_field = 'default_template_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
        $invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("members_invoice_template_id");

        $templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
        $invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
        $invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
        $invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;

        $return = array('status' => 'ok', 'invoice_arr' => $invoice_arr);

        return $return;
    }

    public function getLetterBody($datas, $letter_id = null) {
        //DB Connection
        $conn = Zend_Registry::get('msqli_connection');
        $conn->getConnection();
        if (empty($letter_id)) {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.templates_page = ?', 'invoice_template')
                    ->limit(1);
        } else {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.id = ?', $letter_id)
                    ->limit(1);
        }

        $rs = $select->query()->fetchAll();
        if ($rs) {
            foreach ($rs as $row) {
                $templates_arr = $row;
            }
        } else {
            $templates_arr['templates_desc'] = '';
        }
        foreach ($datas as $key => $value) {
            $templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? str_replace('%' . $key . '%', $value, $templates_arr['templates_desc']) : str_replace('%' . $key . '%', '&nbsp;', $templates_arr['templates_desc']);
            $templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? str_replace('%' . $key . '%', $value, $templates_arr['templates_title']) : str_replace('%' . $key . '%', '&nbsp;', $templates_arr['templates_title']);
        }
        return $templates_arr;
    }

    public function frontpayAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->_request->isPost()) {
            try {
                $payment_id = $this->_request->getPost('payment_id');
                $member_id = $this->_request->getPost('member_id');

                $payment_db = new Paymentgateway_Model_DbTable_Gateway();
                $payment_info = $payment_db->getInfo($payment_id);


                $mem_db = new Members_Model_DbTable_MemberList();
                $mem_info = $mem_db->getMemberInfo($member_id);

                $dynamic_valu_db = new Members_Model_DbTable_FieldsValue();
                $mem_info = $dynamic_valu_db->getFieldsValueInfo($mem_info, $mem_info['user_id']);

                $packageObj = new Members_Controller_Helper_Packages();
                foreach ($mem_info as $key => $value) {
                    $field_name_arr = explode('_', $key);
                    if (in_array('package', $field_name_arr)) {
                        $package_info = $packageObj->getFormAllDatas($value);
                    }
                }

                $data_helper = new Invoice_View_Helper_Data();

                //Set Data variables
                $payble = $package_info[1]['field_value'];
                $member_id = (int) $member_id;
                $client_email = $mem_info['username'];
                $quantity = 1;
                $input_type = ($payment_id == '3' || $payment_id == '6' || $payment_id == '7' || $payment_id == '9') ? 'html' : 'link';

                $payment_data = $data_helper->data($payble, $member_id, $client_email, $quantity, $payment_info, $this->view);
                $json_arr = array('status' => 'ok', 'payment_data' => $payment_data, 'payment_info' => $payment_info, 'input_type' => $input_type);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json_Encoder::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }

}
