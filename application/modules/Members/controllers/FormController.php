<?php
//Dynamic Backend Form controller class
class Members_FormController extends Zend_Controller_Action
{
	private $CustomForm;
	private $_controllerCache;	
	
    public function init()
    {
        /* Initialize action controller here */		
		$this->CustomForm =  new Members_Form_CustomForm ();	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');		
		
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);
		$this->view->setEscape('stripslashes');
			
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{	
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
		}	
	}
	
	//FORM LIST FUNCTION
    public function listAction()
    {
		// action body
		$pageNumber = $this->getRequest()->getParam('page');		
		$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 		
		$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
		
		if(empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = '30';
		}
		else if(!empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
		else if(empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $viewPageNumSes;
		}
		else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
					
		Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;		
		$forms = new Members_Model_FormListMapper();			
        $this->view->datas =  $forms->fetchAll($pageNumber);				
    }
	
	//FORM VALUE LIST FUNCTION
    public function valuesAction()
    {
		$form_id = $this->getRequest()->getParam('form_id');
		if($form_id)
		{
			// action body
			$pageNumber = $this->getRequest()->getParam('page');		
			$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 		
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			if(empty($getViewPageNum) && empty($viewPageNumSes))
			{
				$viewPageNum = '30';
			}
			else if(!empty($getViewPageNum) && empty($viewPageNumSes))
			{
				$viewPageNum = $getViewPageNum;
			}
			else if(empty($getViewPageNum) && !empty($viewPageNumSes))
			{
				$viewPageNum = $viewPageNumSes;
			}
			else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
			{
				$viewPageNum = $getViewPageNum;
			}
						
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;		
			$forms = new Members_Model_FormValueListMapper();			
			$this->view->datas =  $forms->fetchAll($pageNumber,$form_id);
			$this->view->form_id = $form_id;	
		}			
    }
	
	public function updatepackagesAction()
    {
        // action body
		$pageNumber = $this->getRequest()->getParam('page');		
		$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 		
		$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
		
		if(empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = '30';
		}
		else if(!empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
		else if(empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $viewPageNumSes;
		}
		else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
			
		Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;	
		$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
		if( ($member_datas = $this->_controllerCache->load($uniq_id)) === false ) 
		{	
			$member = new Members_Model_MemberListMapper();			
			$member_datas =  $member->fetchAllPackage($pageNumber);
			$this->_controllerCache->save($member_datas , $uniq_id);
		}
		 $this->view->member_datas	= $member_datas;
		//Get Role List
		$roles = new Members_Model_DbTable_Role();
		$this->view->roleList = $roles->fetchAll();		
    }
	
	public function doupdateAction()
	{		
		if ($this->_request->isPost()) 
		{
			try
			{
				$this->_helper->viewRenderer->setNoRender();
				$this->_helper->layout->disableLayout();
				$translator = Zend_Registry::get('translator');
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				
				$members_role = new Members_Model_DbTable_Role();
				$mem_db = new Members_Model_DbTable_MemberList();  
				$field_db = new Members_Model_DbTable_Fields();
				$field_value_db = new Members_Model_DbTable_FieldsValue();
				
				$user_id = $this->_request->getPost('user_id');
				$package_id	=	$this->_request->getPost('package_id');
				$field_name	=	$this->_request->getPost('field_name');
				
				$mem_info = $mem_db->getMemberInfo($user_id);
				$role_info = $members_role->getRoleInfo($mem_info['role_id']);
				$form_id	=	$role_info['form_id'];
				$field_info	=	$field_db->getFieldIdByFieldName($form_id,$field_name);
				$field_id	=	($field_info && $field_info[0]) ? $field_info[0]['id'] : null;	
				if(!empty($field_id))
				{
					$result	=	$field_value_db->updateFieldValue($user_id,$form_id,$field_id,$package_id);	
					
					if($result['status']	==	'ok')
					{
						$msg = $translator->translator("members_package_update_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$json_arr = array('status' => 'err','msg' => $result['msg']);
					}
				}
				else
				{
					$json_arr = array('status' => 'err','msg' => 'This user is not under the package system');
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage().' This user may not under the package system');
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);	
		}
	}
	
	//FORM LIST FUNCTIONS	
	public function addAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->CustomForm->isValid($this->_request->getPost())) 
			{	
				$forms = new Members_Model_Forms($this->CustomForm->getValues());
				$forms->setActive('1');
				
				$perm = new Members_View_Helper_Allow();
				if($perm->allow())
				{
					try
					{
						$result = $forms->saveForms();
						if($result['status'] == 'ok')
						{
							$form_id = $result['id'];
							$field_group_arr = $this->_request->getPost('field_group');
							$group_order_arr = $this->_request->getPost('group_order');
							$field_name_arr = $this->_request->getPost('field_name');
							$field_id_arr =$this->_request->getPost('field_id');
							$field_class_arr = $this->_request->getPost('field_class');
							$field_type_arr = $this->_request->getPost('field_type');
							$field_width_arr = $this->_request->getPost('field_width');
							$field_height_arr = $this->_request->getPost('field_height');
							$field_separator_arr = $this->_request->getPost('field_separator');
							$field_label_arr = $this->_request->getPost('field_label');
							$field_title_arr = $this->_request->getPost('field_title');
							$field_desc_arr = $this->_request->getPost('field_desc');
							$field_option_arr =$this->_request->getPost('field_option');
							$field_default_value_arr = $this->_request->getPost('field_default_value');
							$regexpr_arr = $this->_request->getPost('regexpr');
							$display_admin_arr = $this->_request->getPost('display_admin');
							$display_frontend_arr = $this->_request->getPost('display_frontend');
							$required_arr = $this->_request->getPost('required');
							$field_order_arr = $this->_request->getPost('field_order');							
													
							if(!empty($field_name_arr[0]))
							{
								$fields = new Members_Model_Fields();
								$i = 0;
								foreach($field_name_arr as $key=>$value)
								{
									if(!empty($value))
									{
										$fields->setField_name($value);
										$fields->setField_group($field_group_arr[$i]);
										$fields->setGroup_order($group_order_arr[$i]);
										$fields->setForm_id($form_id);										
										$fields->setField_id($field_id_arr[$i]);
										$fields->setField_class($field_class_arr[$i]);
										$fields->setField_type($field_type_arr[$i]);
										$fields->setField_width($field_width_arr[$i]);
										$fields->setField_height($field_height_arr[$i]);
										$fields->setField_separator($field_separator_arr[$i]);
										$fields->setField_label($field_label_arr[$i]);
										$fields->setField_title($field_title_arr[$i]);
										$fields->setField_desc($field_desc_arr[$i]);
										$fields->setField_option($field_option_arr[$i]);
										$fields->setField_default_value($field_default_value_arr[$i]);
										$fields->setRegexpr($regexpr_arr[$i]);
										$fields->setDisplay_admin($display_admin_arr[$i]);
										$fields->setDisplay_frontend($display_frontend_arr[$i]);
										$fields->setRequired($required_arr[$i]);
										$fields->setField_order($field_order_arr[$i]);
																		
										try
										{
											$field_result = $fields->saveFields();
											if($field_result['status'] == 'ok')
											{
												$msg = $translator->translator("members_form_save_success");
												$json_arr = array('status' => 'ok','msg' => $msg);
											}
											else
											{
												$msg = $translator->translator("members_form_save_err");
												$json_arr = array('status' => 'err','msg' => $msg);
											}
										}
										catch(Exception $e)
										{
											$msg = $translator->translator("members_form_save_err");
											$json_arr = array('status' => 'err','msg' => $msg." ".$e->getMessage());
											break;
										}
									}
									$i++;								
								}
							}
							else
							{
								$msg = $translator->translator("members_form_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
							}													
						}
						else
						{
							$msg = $translator->translator("members_form_save_err");
							$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
						}
					}
					catch(Exception $e)
					{
						$json_arr = array('status' => 'err','msg' => $e->getMessage());
					}						
				}
				else
				{
					$Msg =  $translator->translator("members_form_add_action_deny");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->CustomForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{			
			$this->view->CustomForm = $this->CustomForm;			
			$this->render();
		}	
	}
	
	public function editAction()
	{	
		$form_id = $this->_request->getParam('id');
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->CustomForm->isValid($this->_request->getPost())) 
			{	
				$form_id = $this->_request->getPost('form_id');
				$forms = new Members_Model_Forms($this->CustomForm->getValues());
				$forms->setId($form_id);
				
				$perm = new Members_View_Helper_Allow();
				if($perm->allow())
				{
					try
					{
						$result = $forms->saveForms();
						if($result['status'] == 'ok')
						{
							$id_arr = $this->_request->getPost('id');	
							$field_group_arr = $this->_request->getPost('field_group');
							$group_order_arr = $this->_request->getPost('group_order');						
							$field_name_arr = $this->_request->getPost('field_name');
							$field_id_arr =$this->_request->getPost('field_id');
							$field_class_arr = $this->_request->getPost('field_class');
							$field_type_arr = $this->_request->getPost('field_type');
							$field_width_arr = $this->_request->getPost('field_width');
							$field_height_arr = $this->_request->getPost('field_height');
							$field_separator_arr = $this->_request->getPost('field_separator');
							$field_label_arr = $this->_request->getPost('field_label');
							$field_title_arr = $this->_request->getPost('field_title');
							$field_desc_arr = $this->_request->getPost('field_desc');
							$field_option_arr =$this->_request->getPost('field_option');
							$field_default_value_arr = $this->_request->getPost('field_default_value');
							$regexpr_arr = $this->_request->getPost('regexpr');
							$display_admin_arr = $this->_request->getPost('display_admin');
							$display_frontend_arr = $this->_request->getPost('display_frontend');
							$required_arr = $this->_request->getPost('required');
							$field_order_arr = $this->_request->getPost('field_order');							
													
							if(!empty($field_name_arr[0]))
							{
								$fields = new Members_Model_Fields();
								$i = 0;
								$field_id_array = array();
								foreach($field_name_arr as $key=>$value)
								{
									if(!empty($value))
									{
										if(!empty($id_arr[$i]))
										{
											$fields->setId($id_arr[$i]);
										}
										else
										{
											$fields->setId('');
										}
										$fields->setField_name($value);
										$fields->setField_group($field_group_arr[$i]);
										$fields->setGroup_order($group_order_arr[$i]);
										$fields->setForm_id($form_id);										
										$fields->setField_id($field_id_arr[$i]);
										$fields->setField_class($field_class_arr[$i]);
										$fields->setField_type($field_type_arr[$i]);
										$fields->setField_width($field_width_arr[$i]);
										$fields->setField_height($field_height_arr[$i]);
										$fields->setField_separator($field_separator_arr[$i]);
										$fields->setField_label($field_label_arr[$i]);
										$fields->setField_title($field_title_arr[$i]);
										$fields->setField_desc($field_desc_arr[$i]);
										$fields->setField_option($field_option_arr[$i]);
										$fields->setField_default_value($field_default_value_arr[$i]);
										$fields->setRegexpr($regexpr_arr[$i]);
										$fields->setDisplay_admin($display_admin_arr[$i]);
										$fields->setDisplay_frontend($display_frontend_arr[$i]);
										$fields->setRequired($required_arr[$i]);
										$fields->setField_order($field_order_arr[$i]);
																		
										try
										{
											$field_result = $fields->saveFields();
											if($field_result['status'] == 'ok')
											{
												$field_id_array[$i] = $field_result['id'];
												$msg = $translator->translator("members_form_save_success");
												$json_arr = array('status' => 'ok','msg' => $msg,'field_id_array' => $field_id_array);
											}
											else
											{
												$msg = $translator->translator("members_form_save_err");
												$json_arr = array('status' => 'err','msg' => $msg.' '.$field_result['msg']);
											}
										}
										catch(Exception $e)
										{
											$msg = $translator->translator("members_form_save_err");
											$json_arr = array('status' => 'err','msg' => $msg." ".$e->getMessage());
											break;
										}
									}
									$i++;								
								}
							}
							else
							{
								$msg = $translator->translator("members_form_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
							}													
						}
						else
						{
							$msg = $translator->translator("members_form_save_err");
							$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
						}
					}
					catch(Exception $e)
					{
						$json_arr = array('status' => 'err','msg' => $e->getMessage());
					}						
				}
				else
				{
					$Msg =  $translator->translator("members_form_add_action_deny");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->CustomForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$global_conf = Zend_Registry::get('global_conf');
			$forms = new Members_Model_DbTable_Forms();
			$this->CustomForm->populate($forms->getFormsInfo($form_id));
			if($this->CustomForm->email_to->getValue() == null){ $this->CustomForm->email_to->setValue($this->view->escape($global_conf["global_email"])); } 	
			$this->view->form_id = 	$form_id;	
			$this->view->CustomForm = $this->CustomForm;			
			$this->render();
		}	
	}
	
	public function deletefieldAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$id = $this->_request->getPost('id');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Remove Fiels from Table
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields', $where);
				$msg = 	$translator->translator('form_dy_field_delete_success');			
				$json_arr = array('status' => 'ok','msg' => $msg);
			}
			catch (Exception $e) 
			{
				$msg = 	$translator->translator('form_dy_field_delete_err');			
				$json_arr = array('status' => 'err','msg' => $msg." ".$e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id = $this->_request->getPost('id');
			$form_name = $this->_request->getPost('form_name');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			//Check Role Assigned
			$role_db = new Members_Model_DbTable_Role();  
			$num_role = $role_db->getRoleNum($id); 
							
			if($num_role == 0)
			{
				try
				{
					//GET DB CLASS
					$form_db = new Members_Model_DbTable_Forms();
					$fields_db	= new Members_Model_DbTable_Fields(); 
					$field_value_db = new Members_Model_DbTable_FieldsValue();
					
					$fields_infos = $fields_db->getFieldsInfo($id);
					$form_info	 = $form_db->getFormsInfo($id);
								
					foreach($fields_infos as $fields)
					{
						if(trim($fields->field_type)	==	'file')
						{
							$field_values = $field_value_db->getFieldsValue2($id,$fields->id);
							foreach($field_values as $field_value_arr)
							{
								if(!empty($field_value_arr['field_value']))
								{														
									$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$field_value_arr['field_value'];
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);										
								}
							}
						}
					}
					
					// Remove Value Form forms_general DB TABLE
					$whereG = array();
					$whereG[] = 'form_id = '.$conn->quote($id);
					$conn->delete(Zend_Registry::get('dbPrefix').'forms_general', $whereG);
					
					// Remove Form Fields Values from DB TABLE
					$whereV = array();
					$whereV[] = 'form_id = '.$conn->quote($id);	
					$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereV);
					
					// Remove Form from DB TABLE
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					$conn->delete(Zend_Registry::get('dbPrefix').'forms', $where);
					
					// Remove Form Fields from DB TABLE
					$whereF = array();
					$whereF[] = 'form_id = '.$conn->quote($id);	
					$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields', $whereF);
						
					$msg = 	$translator->translator('form_list_delete_success',$form_name);			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$e->getMessage();			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('form_list_delete_role_err',$form_name);			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('form_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Members_View_Helper_Allow();			
			if ($perm->allow('delete','form','Members'))
			{
				if(!empty($id_str))
				{	
					$id_str = explode(',',$id_str);
					
					//GET DB CLASS
					$form_db = new Members_Model_DbTable_Forms();
					$fields_db	= new Members_Model_DbTable_Fields(); 
					$field_value_db = new Members_Model_DbTable_FieldsValue();
										
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					$non_del_arr = array();
					$k = 0;
					foreach($id_str as $id)
					{
						//Check Role Assigned
						$role_db = new Members_Model_DbTable_Role();  
						$num_role = $role_db->getRoleNum($id); 				
						if($num_role == 0)
						{							
							try
							{							
								$fields_infos = $fields_db->getFieldsInfo($id);
								$form_info	 = $form_db->getFormsInfo($id);
											
								foreach($fields_infos as $fields)
								{
									if(trim($fields->field_type)	==	'file')
									{
										$field_values = $field_value_db->getFieldsValue2($id,$fields->id);
										foreach($field_values as $field_value_arr)
										{
											if(!empty($field_value_arr['field_value']))
											{														
												$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$field_value_arr['field_value'];
												$res = Eicra_File_Utility::deleteRescursiveDir($dir);										
											}
										}
									}
								}
								
								// Remove Value Form forms_general DB TABLE
								$whereG = array();
								$whereG[] = 'form_id = '.$conn->quote($id);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_general', $whereG);
								
								// Remove Form Fields Values from DB TABLE
								$whereV = array();
								$whereV[] = 'form_id = '.$conn->quote($id);	
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereV);
							
								// Remove Form from DB TABLE
								$where = array();
								$where[] = 'id = '.$conn->quote($id);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms', $where);
								
								// Remove Form Fields from DB TABLE
								$whereF = array();
								$whereF[] = 'form_id = '.$conn->quote($id);	
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields', $whereF);
								$msg = 	$translator->translator('form_list_delete_success');			
								$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
							catch (Exception $e) 
							{
								$non_del_arr[$k] = $id;
								$k++;
								$msg = 	$translator->translator('form_list_delete_err');			
								$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
						}
						else
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('form_list_delete_role_err','selected forms');			
							$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
				}
				else
				{
					$msg = $translator->translator("member_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('Members_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('form_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function publishAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$form_name = $this->_request->getPost('form_name');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$menu_status = '1';
					break;
				case 'unpublish':
					$menu_status = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
					
			try
			{	
				$conn->update(Zend_Registry::get('dbPrefix').'forms',array('active' => $menu_status), $where);		
				$json_arr = array('status' => 'ok','active' => $menu_status);
			}
			catch (Exception $e) 
			{
				$msg = $translator->translator('form_list_publish_err',$form_name);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
			
	}
	
	public function publishallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id_str  = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{	
						$conn->update(Zend_Registry::get('dbPrefix').'forms',array('active' => $active), $where);
						$return = true;
					}
					catch (Exception $e) 
					{
						$return = false;
					}
				}
			
				if($return)
				{			
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('form_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = $translator->translator("member_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	//FORM VALUE LIST FUNCTIONS
	public function addvalueAction()
	{
		$form_id = ($this->_request->getPost('form_id'))? $this->_request->getPost('form_id') : $this->_getParam('form_id', 0);
		if($form_id)
		{	
			$translator = Zend_Registry::get('translator');								
			$form_db = new Members_Model_DbTable_Forms();
			$form_info = $form_db->getFormsInfo($form_id);
			$form_info['common_captcha_show_backend']	=	$translator->translator('common_captcha_show_backend');
			$form_info['display_type']	=	'display_admin' ;	
			$generalForm =  new Members_Form_GeneralForm ($form_info);
						
			if ($this->_request->isPost()) 
			{	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$this->_helper->viewRenderer->setNoRender();
				$this->_helper->layout->disableLayout();							
				
				$elements = $generalForm->getElements();
				foreach($elements as $element)
				{
					if($element->getType() == 'Zend_Form_Element_File')
					{
						$element_name = $element->getName();
						$generalForm->removeElement($element_name);
					}
				}
				if ($generalForm->isValid($this->_request->getPost())) 
				{					
					$generalForm =  new Members_Form_GeneralForm ($form_info);
					$generalForm->populate($this->_request->getPost());
					$fromValues = $generalForm;	
					if($form_info['db_set'] == '1')
					{
						$general = new Members_Model_General(array('form_id' => $form_info['id'],'active' => '2'));
						$result = $general->saveGeneral();
					}
					else
					{
						$result['status'] = 'ok';
						$result['id'] = 0;
					}
					
					if($result['status'] == 'ok')
					{				
						$field_db = new Members_Model_DbTable_Fields();
						$field_groups = $field_db->getGroupNames($form_info['id']); 
						
						//Add Data To Database
						$attach_file_arr = array();
						foreach($field_groups as $group)
						{
							$group_name = $group->field_group;
							$displaGroup = $generalForm->getDisplayGroup($group_name);
							$elementsObj = $displaGroup->getElements();
							foreach($elementsObj as $element)
							{
								$table_id = $result['id'];
								$form_id = $form_info['id'];
								$field_id	=	$element->getAttrib('rel');
								$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
								if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
								{ 
									if(is_array($field_value)) { $field_value = ''; }
								}
								if($element->getType() == 'Zend_Form_Element_Multiselect') 
								{ 
									$field_value	=	$this->_request->getPost($element->getName());
									if(is_array($field_value)) { $field_value = implode(',',$field_value); }
								}
								if($element->getType() == 'Zend_Form_Element_File') 
								{ 
									$post_data = $this->_request->getPost($element->getName());  
									$post_data_arr = explode(',',$post_data);									
									$attach_file_arr =  array_merge((array)$attach_file_arr, (array)$post_data_arr);
								}
								try
								{
									$msg = $translator->translator("member_form_submit_successfull");
									$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
									
									if($form_info['db_set'] == '1')
									{
										$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
										$this->_DBconn = Zend_Registry::get('msqli_connection');
										$this->_DBconn->getConnection();
										$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
										
										$msg = $translator->translator("member_form_submit_successfull");
										$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
									}								
									
								}
								catch(Exception $e)
								{
									$msg = $translator->translator("member_form_submit_err");
									$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
								}
							}						
						}
						
						//Send Data To Email
						if($form_info['email_set'] == '1')
						{
							$register_helper = new Members_Controller_Helper_Registers();	
							$allDatas = $fromValues->getValues();	
							try 
							{
								$register_helper->sendGeneralMail($allDatas,$form_info,$attach_file_arr);
								$from_mail_field = Eicra_File_Constants::FROM_MAIL;
								$to_mail_field = Eicra_File_Constants::TO_MAIL;
								$allDatas[$from_mail_field] = trim($this->_request->getPost($from_mail_field));
								$to_mail	=	trim($this->_request->getPost($to_mail_field));
								if($to_mail)
								{
									$send_result = $register_helper->sendEmailToFriend($allDatas,$form_info,$attach_file_arr,$to_mail);
								}
								$msg = $translator->translator("member_form_submit_successfull");
								$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
							}
							catch (Exception $e) 
							{
								$msg = $e->getMessage();
								$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
							}
						}
						
						//Delete Attached Files
						if($form_info['attach_file_delete'] == '1')
						{
							$elements = $generalForm->getElements();
							foreach($elements as $element)
							{
								if($element->getType() == 'Zend_Form_Element_File')
								{
									$element_name = $element->getName();
									$element_value	= $this->_request->getPost($element_name);
									if(!empty($element_value))
									{	
										$element_value_arr = explode(',',$element_value);	
										foreach($element_value_arr as $key => $e_value)
										{
											if(!empty($e_value))
											{
												$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
												$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
											}
										}	
									}
								}
							}
						}
					}
					else
					{
						$json_arr = array('status' => 'err','msg' => $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
					}
												
				}
				else
				{
					$validatorMsg = $generalForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
				}			
				$res_value = Zend_Json_Encoder::encode($json_arr);			
				$this->_response->setBody($res_value);
			}
			
			$this->view->form_info	= $form_info;
			$this->view->generalForm = $generalForm;
			$this->dynamicUploaderSettings($this->view->form_info);	
		}
	}
	
	public function editvalueAction()
	{
		$id = ($this->_request->getPost('id'))? $this->_request->getPost('id') : $this->_getParam('id', 0);
		if(!empty($id))
		{
			$general_db = new Members_Model_DbTable_General();
			$general_info = $general_db->getInfo($id);
			$translator = Zend_Registry::get('translator');	
			try
			{
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($general_info['form_id']);
				$form_info['common_captcha_show_backend']	=	$translator->translator('common_captcha_show_backend');
				$form_info['display_type']	=	'display_admin' ;	
				$this->view->form_info = $form_info;
			}
			catch(Exception $e)
			{
				$form_info = null;
			}
		}
		else
		{
			$form_info = null;
		}
		$generalForm =  new Members_Form_GeneralForm ($form_info);
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$elements = $generalForm->getElements();
			foreach($elements as $element)
			{
				if($element->getType() == 'Zend_Form_Element_File')
				{
					$element_name = $element->getName();
					$generalForm->removeElement($element_name);
				}
			}
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();			
						
			if ($generalForm->isValid($this->_request->getPost())) 
			{					
				$generalForm =  new Members_Form_GeneralForm ($form_info);
				$generalForm->populate($this->_request->getPost());
				
				$fromValues = $generalForm;	
				
				if(!empty($form_info) && !empty($form_info['id']))
				{
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($form_info['id']); 
					foreach($field_groups as $group)
					{
						$group_name = $group->field_group;
						$displaGroup = $generalForm->getDisplayGroup($group_name);
						$elementsObj = $displaGroup->getElements();
						foreach($elementsObj as $element)
						{
							if(substr($element->getName(), -5) != '_prev')
							{
								$table_id = $id;
								$form_id = $form_info['id'];
								$field_id	=	$element->getAttrib('rel');
								$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
								if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
								{ 
									if(is_array($field_value)) { $field_value = ''; }
								}
								if($element->getType() == 'Zend_Form_Element_Multiselect') 
								{ 
									$field_value	=	$this->_request->getPost($element->getName());
									if(is_array($field_value)) { $field_value = implode(',',$field_value); }
								}
								
								try
								{
									$DBconn = Zend_Registry::get('msqli_connection');
									$DBconn->getConnection();
									
									// Remove from Value
									$where = array();
									$where[0] = 'table_id = '.$DBconn->quote($table_id);
									$where[1] = 'form_id = '.$DBconn->quote($form_id);
									$where[2] = 'field_id = '.$DBconn->quote($field_id);
									$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
									
									//Add Value
									$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
									$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
									if($element->getType() == 'Zend_Form_Element_File' && $this->_request->getPost($element->getName()) != $this->_request->getPost($element->getName().'_prev'))
									{
										$element_value	= $this->_request->getPost($element->getName().'_prev');
										if(!empty($element_value))
										{
											$element_value_arr = explode(',',$element_value);	
											foreach($element_value_arr as $key => $e_value)
											{
												if(!empty($e_value))
												{	
													$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
													$res = Eicra_File_Utility::deleteRescursiveDir($dir);
												}
											}	
										}
									}
									$msg = $translator->translator("member_form_submit_successfull");
									$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
								}
								catch(Exception $e)
								{
									$msg = $translator->translator("member_form_submit_err");
									$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
								}
								
							}
						}
					}
					
					//Send Data To Email
					if($form_info['email_set'] == '1')
					{
						$register_helper = new Members_Controller_Helper_Registers();	
						$allDatas = $fromValues->getValues();	
						try 
						{
							$register_helper->sendGeneralMail($allDatas,$form_info,$attach_file_arr);
							$from_mail_field = Eicra_File_Constants::FROM_MAIL;
							$to_mail_field = Eicra_File_Constants::TO_MAIL;
							$allDatas[$from_mail_field] = trim($this->_request->getPost($from_mail_field));
							$to_mail	=	trim($this->_request->getPost($to_mail_field));
							if($to_mail)
							{
								$send_result = $register_helper->sendEmailToFriend($allDatas,$form_info,$attach_file_arr,$to_mail);
							}
							$msg = $translator->translator("member_form_submit_successfull");
							$json_arr = array('status' => 'ok','msg' => $msg, 'attach_file_arr' => $attach_file_arr, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
						}
						catch (Exception $e) 
						{
							$msg = $e->getMessage();
							$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
						}
					}
				}
				
				$res_value = Zend_Json_Encoder::encode($json_arr);			
				$this->_response->setBody($res_value);
			}
			else
			{
				$validatorMsg = $generalForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
			}
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{		
			$dynamic_value_db = new Members_Model_DbTable_FieldsValue();				
			$data_info	=	array();
			$data_info	=	$dynamic_value_db->getFieldsValueInfo($data_info,$id);			
			$generalForm->populate($data_info);	
			$this->view->data_info = 	$data_info;
			$this->view->id = 	$id;	
			$this->view->generalForm = $generalForm;
			$this->dynamicUploaderSettings($this->view->form_info);	
		}
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	public function deletevalueAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			
			$perm = new Members_View_Helper_Allow();			
			if ($perm->allow('deletevalue','form','Members'))
			{			
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				//GET DB CLASS
				$general_db = new Members_Model_DbTable_General();  
				$general_info = $general_db->getInfo($id);				 
								
				if(!empty($general_info['form_id']))
				{
					try
					{
						//Delete Files From Folder
						$form_db = new Members_Model_DbTable_Forms();  
						$form_info = $form_db->getFormsInfo($general_info['form_id']); 
						
						$fields_db	= new Members_Model_DbTable_Fields();
						$fields_infos = $fields_db->getFieldsInfo($general_info['form_id']);
						
						$field_value_db = new Members_Model_DbTable_FieldsValue();
						
						if($fields_infos)
						{
							foreach($fields_infos as $fields)
							{
								if(trim($fields->field_type)	==	'file')
								{
									$field_value_arr = $field_value_db->getFieldsValue($id,$general_info['form_id'],$fields->id);
									if(!empty($field_value_arr[0]) && $field_value_arr)
									{
										if(!empty($field_value_arr[0]['field_value']))
										{	
											$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
											foreach($element_value_arr as $key => $e_value)
											{
												if(!empty($e_value))
												{	
													$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
													$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
												}
											}
										}
									}
								}
							}
						}
						
						// Remove Value Form forms_general DB TABLE
						$whereG = array();
						$whereG[] = 'id = '.$conn->quote($id);
						$conn->delete(Zend_Registry::get('dbPrefix').'forms_general', $whereG);
						
						// Remove Form Fields Values from DB TABLE
						$whereV = array();
						$whereV[] = 'table_id = '.$conn->quote($id);	
						$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereV);
							
						$msg = 	$translator->translator('form_value_list_delete_success');			
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					catch (Exception $e) 
					{
						$msg = 	$e->getMessage();			
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = 	$translator->translator('form_value_list_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('common_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('form_value_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deletevalueallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id_str = $this->_request->getPost('id_st');
			
			$perm = new Members_View_Helper_Allow();			
			if ($perm->allow('deletevalue','form','Members'))
			{					
				if(!empty($id_str))
				{	
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					//GET DB CLASS
					$general_db = new Members_Model_DbTable_General(); 
					$form_db = new Members_Model_DbTable_Forms();
					$fields_db	= new Members_Model_DbTable_Fields(); 
					$field_value_db = new Members_Model_DbTable_FieldsValue();
				
					$id_str = explode(',',$id_str);		
					
					$non_del_arr = array();
					$k = 0;
					foreach($id_str as $id)
					{
						$general_info = $general_db->getInfo($id);				 
										
						if(!empty($general_info['form_id']))
						{
							try
							{
								//Delete Files From Folder  
								$form_info = $form_db->getFormsInfo($general_info['form_id']); 
															
								$fields_infos = $fields_db->getFieldsInfo($general_info['form_id']);
								
								foreach($fields_infos as $fields)
								{
									if(trim($fields->field_type)	==	'file')
									{
										$field_value_arr = $field_value_db->getFieldsValue($id,$general_info['form_id'],$fields->id);
										if(!empty($field_value_arr[0]) && $field_value_arr)
										{
											if(!empty($field_value_arr[0]['field_value']))
											{
												$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
												foreach($element_value_arr as $key => $e_value)
												{
													if(!empty($e_value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}
												}
											}
										}
									}
								}
								
								// Remove Value Form forms_general DB TABLE
								$whereG = array();
								$whereG[] = 'id = '.$conn->quote($id);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_general', $whereG);
								
								// Remove Form Fields Values from DB TABLE
								$whereV = array();
								$whereV[] = 'table_id = '.$conn->quote($id);	
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereV);
									
								$msg = 	$translator->translator('form_value_list_delete_success');			
								$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
							catch (Exception $e) 
							{
								$non_del_arr[$k] = $id;
								$k++;
								$msg = 	$e->getMessage();			
								$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
						}
						else
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('form_value_list_delete_err');			
							$json_arr = array('status' => 'ok','msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
				}
				else
				{
					$msg = $translator->translator("member_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('Members_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('form_value_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function publishvalueAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$perm = new Members_View_Helper_Allow();			
			if ($perm->allow('publishvalue','form','Members'))
			{	
				$id = $this->_request->getPost('id');
				$paction = $this->_request->getPost('paction');
				
				switch($paction)
				{
					case 'publish':
						$menu_status = '1';
						break;
					case 'unpublish':
						$menu_status = '0';
						break;
				}
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Update Article status
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
						
				try
				{	
					$conn->update(Zend_Registry::get('dbPrefix').'forms_general',array('active' => $menu_status), $where);		
					$json_arr = array('status' => 'ok','active' => $menu_status);
				}
				catch (Exception $e) 
				{
					$msg = $translator->translator('form_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('common_publish_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
}