<?php
//Members Frontend controller
class Members_MembersController extends Zend_Controller_Action
{
	private $loginForm ;
	private $_page_id;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	
    public function init()
    {	
		$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
		Eicra_Global_Variable::checkSession($this->_response,$url);
		Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();
		
		/* Initialize Template */
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout(true));
		$this->translator =	Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->translator);	
	
        /* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();			
		
		
		$this->view->menu_id = $this->_request->getParam('menu_id');
		$getAction = $this->_request->getActionName();
		
			
		if($this->_request->getParam('menu_id'))
		{			
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
			
    }
	
	public function preDispatch() 
	{	
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;
		$this->view->setEscape('stripslashes');					
	}
	
	public function dashboardAction()
	{
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title'), $this->view->url()));
		//echo 'ok'; 
	}
	
	
	public function listAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title'), 'Members-Dashboard'), array($this->translator->translator('members_frontend_list_page_name'), $this->view->url()));
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		//Get Role List
		$roles = new Members_Model_DbTable_Role();
		$this->view->roleList = $roles->fetchAll();
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				$pageNumber 	= ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');	
				$approve 	= $this->_request->getParam('approve');		
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontendRout = ($pageNumber == '1' || empty($pageNumber)) ? 'Members-Frontend-List' : 'Members-Frontend-List/:page';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRout,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($member_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{		
					$member = new Members_Model_MemberListMapper();			
					$member_datas =  $member->fetchAll($pageNumber, $approve, $posted_data);				
					$this->_controllerCache->save($member_datas , $uniq_id);
				}
				$data_result = array();
				$total	=	0;				
				if($member_datas)
				{	
					$key = 0;				
					foreach($member_datas as $entry)
					{
							$entry_arr = $entry->toArray();
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_username'] = str_replace('_', '-', $entry_arr['username']);		
							$entry_arr['last_access_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['last_access']));
							$entry_arr['register_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['register_date']));
							$entry_arr['edit_enable']	= (($this->_auth_obj->access_other_user_profile == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) && ($entry_arr['role_lock'] == '1' || $this->_auth_obj->role_id == $entry_arr['role_id']) ) ? true : false;						
							$data_result[$key]	=	$entry_arr;	
							$key++;					
					}
					$total	=	$member_datas->getTotalItemCount();
				}
				$json_arr = array('status' => 'ok', 'data_result' => $data_result, 'total' => $total, 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	public function editAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title'), 'Members-Dashboard'), array($this->translator->translator('members_frontend_list_page_name'), 'Members-Frontend-List'), array($this->translator->translator('Member_edit_page_name'), $this->view->url()));
		
		$user_id = ($this->_request->getPost('user_id'))? $this->_request->getPost('user_id') : $this->_getParam('user_id', 0);		
		if(!empty($user_id))
		{
			$memberData = new Members_Model_DbTable_MemberList();
			$members_info = $memberData->getMemberInfo($user_id);
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);
				$role_info['display_type']	=	'display_admin' ;	
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);				
				$role_info['attach_file_max_size'] = $form_info['attach_file_max_size'];
				$role_info['common_captcha_show_backend']	=	$this->translator->translator('common_captcha_show_backend');
				$this->view->form_info = $form_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}
		$registrationForm =  new Members_Form_UserForm ($role_info);
		$registrationForm->removeElement('password');
		$registrationForm->removeElement('confirmPassword');
		
        if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				$element_name = $element->getName();
				$group_arr[$element_name] = $element_name;
				if($element->getType() == 'Zend_Form_Element_File')
				{	
					$registrationForm->removeElement($element_name);
				}					
			}				
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			
			$username = $this->_request->getPost('username');
			$user_check = new Members_Controller_Helper_Registers(); 
			
			if($user_check->getUsernameAvailable($username,$user_id))
			{		
				if ($registrationForm->isValid($this->_request->getPost())) 
				{					
					$registrationForm =  new Members_Form_UserForm ($role_info);
					$registrationForm->removeElement('password');
					$registrationForm->removeElement('confirmPassword');
					$registrationForm->populate($this->_request->getPost());
					
					$fromValues = $registrationForm;
					$members = new Members_Model_Members($this->_request->getPost());
					$roles_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $members_info['role_id'];
					$members->setUser_id($user_id);	
					$members->setRole_id($roles_id);			
					$result = $members->saveRegister();
						
					if($result['status'] == 'ok')
					{
						$msg = $this->translator->translator("member_save_successfull");
						$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						if(!empty($role_info) && !empty($role_info['form_id']))
						{
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($form_info['id']); 
							foreach($field_groups as $group)
							{
								$group_name = $group->field_group;
								$displaGroup = $registrationForm->getDisplayGroup($group_name);
								$elementsObj = $displaGroup->getElements();
								foreach($elementsObj as $element)
								{
									if(!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select'))
									{
										if(substr($element->getName(), -5) != '_prev')
										{
											$table_id = $result['id'];
											$form_id = $form_info['id'];
											$field_id	=	$element->getAttrib('rel');
											$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
											if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
											{ 
												if(is_array($field_value)) { $field_value = ''; }
											}
											if($element->getType() == 'Zend_Form_Element_Multiselect') 
											{ 
												$field_value	=	$this->_request->getPost($element->getName());
												if(is_array($field_value)) { $field_value = implode(',',$field_value); }
											}
											
											try
											{
												$DBconn = Zend_Registry::get('msqli_connection');
												$DBconn->getConnection();
												
												// Remove from Value
												$where = array();
												$where[0] = 'table_id = '.$DBconn->quote($table_id);
												$where[1] = 'form_id = '.$DBconn->quote($form_id);
												$where[2] = 'field_id = '.$DBconn->quote($field_id);
												$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
												
												//Add Value
												$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
												$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
												if($element->getType() == 'Zend_Form_Element_File' && $this->_request->getPost($element->getName()) != $this->_request->getPost($element->getName().'_prev'))
												{
													$element_value	= $this->_request->getPost($element->getName().'_prev');
													if(!empty($element_value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$element_value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}
												}
												$msg = $this->translator->translator("member_registered_successfull");
												$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											catch(Exception $e)
											{
												$msg = $this->translator->translator("member_registered_err");
												$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											
										}
									}
								}
							}
						}
					}
					else
					{
						$msg = $this->translator->translator("member_save_err");
						$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}
					$res_value = Zend_Json_Encoder::encode($json_arr);			
					$this->_response->setBody($res_value);
				}
				else
				{
					$validatorMsg = $registrationForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			else
			{
				$msg = $this->translator->translator("member_availability",$username);
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			if($members_info)
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);
				$members_info['form_id'] = 	$role_info['form_id'];
				$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
				$members_info	=	$dynamic_valu_db->getFieldsValueInfo($members_info,$user_id);
				$registrationForm->populate($members_info);	
				$this->view->members_info = 	$members_info;
				$this->view->role_info = 	$role_info;
				$this->view->user_id = 	$user_id;	
				$this->view->registrationForm = $registrationForm;
				$this->dynamicUploaderSettings($this->view->form_info);	
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Frontend-List', array());
			}
		}	
    }
	
	public function profileAction()
    {	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title'), 'Members-Dashboard'), array($this->translator->translator('Member_profile_page_name'), $this->view->url()));
			
		$globalIdentity = $this->_auth_obj;			
		$user_id = $globalIdentity->user_id;
		
		if(!empty($user_id))
		{
			$memberData = new Members_Model_DbTable_MemberList();
			$members_info = $memberData->getMemberInfo($user_id);
		
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($members_info['role_id']);					
				$form_db = new Members_Model_DbTable_Forms();				
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$role_info['common_captcha_show_backend']	=	$this->translator->translator('common_captcha_show_backend');
				$role_info['display_type']	=	'display_admin' ;	
				$this->view->form_info = $form_info;				
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}
		$registrationForm =  new Members_Form_UserForm ($role_info);
		$registrationForm->removeElement('password');
		$registrationForm->removeElement('confirmPassword');
		
        if ($this->_request->isPost() && $this->_request->getPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				if($element->getType() == 'Zend_Form_Element_File')
				{
					$element_name = $element->getName();
					$registrationForm->removeElement($element_name);
				}
			}
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$username = $this->_request->getPost('username');
			$user_check = new Members_Controller_Helper_Registers(); 
			$roles_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $members_info['role_id'];
			
			if($user_check->getUsernameAvailable($username,$user_id))
			{		
				if ($registrationForm->isValid($this->_request->getPost())) 
				{
					$registrationForm =  new Members_Form_UserForm ($role_info);
					$registrationForm->removeElement('password');
					$registrationForm->removeElement('confirmPassword');
					$registrationForm->populate($this->_request->getPost());
					
					$fromValues = $registrationForm;
					$data = new Members_Model_Members($this->_request->getPost());
					$data->setUser_id($user_id);
					$data->setRole_id($roles_id);				
					$result = $data->saveRegister();
						
					if($result['status'] == 'ok')
					{
						$msg = $this->translator->translator("member_save_successfull");
						$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						$globalIdentity->username = $data->getUsername();
						$globalIdentity->title = $data->getTitle();
						$globalIdentity->firstName = $data->getFirstName();						
						$globalIdentity->lastName = $data->getLastName();
						$globalIdentity->role_id = $data->getRole_id();
						$updated_role_info = $role_db->getRoleInfo($globalIdentity->role_id);
						$globalIdentity->role_name = stripslashes($updated_role_info['role_name']);
						Eicra_Global_Variable::getSession()->admin_top_menu  = '';
						Eicra_Global_Variable::getSession()->acl_obj = '';
						if(!empty($role_info) && !empty($role_info['form_id']))
						{
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($form_info['id']); 
							foreach($field_groups as $group)
							{
								$group_name = $group->field_group;
								$displaGroup = $registrationForm->getDisplayGroup($group_name);
								$elementsObj = $displaGroup->getElements();
								foreach($elementsObj as $element)
								{
									if(!($element->getName() == 'package_id' && $element->getType() == 'Zend_Form_Element_Select'))
									{
										if(substr($element->getName(), -5) != '_prev')
										{
											$table_id = $result['id'];
											$form_id = $form_info['id'];
											$field_id	=	$element->getAttrib('rel');
											$field_value = ($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
											if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
											{ 
												if(is_array($field_value)) { $field_value = ''; }
											}
											if($element->getType() == 'Zend_Form_Element_Multiselect') 
											{ 
												$field_value	=	$this->_request->getPost($element->getName());
												if(is_array($field_value)) { $field_value = implode(',',$field_value); }
											}
											
											try
											{
												$DBconn = Zend_Registry::get('msqli_connection');
												$DBconn->getConnection();
												
												//Remove from Value
												$where = array();
												$where[0] = 'table_id = '.$DBconn->quote($table_id);
												$where[1] = 'form_id = '.$DBconn->quote($form_id);
												$where[2] = 'field_id = '.$DBconn->quote($field_id);
												$DBconn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values',$where);
												
												//Add Value
												$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
												$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
												if($element->getType() == 'Zend_Form_Element_File' && $this->_request->getPost($element->getName()) != $this->_request->getPost($element->getName().'_prev'))
												{
													$element_value	= $this->_request->getPost($element->getName().'_prev');
													if(!empty($element_value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$element_value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}
												}
												$msg = $this->translator->translator("member_save_successfull");
												$json_arr = array('status' => 'ok','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											catch(Exception $e)
											{
												$msg = $this->translator->translator("member_save_err");
												$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
											}
											
										}
									}
								}
							}
						}
					}
					else
					{
						$msg = $this->translator->translator("member_save_err");
						$json_arr = array('status' => 'err','msg' => $msg.' '.$result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}
					$res_value = Zend_Json_Encoder::encode($json_arr);			
					$this->_response->setBody($res_value);
				}
				else
				{
					$validatorMsg = $registrationForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			else
			{
				$msg = $this->translator->translator("member_availability",$username);
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
			$members_info	=	$dynamic_valu_db->getFieldsValueInfo($members_info,$user_id);
			$registrationForm->populate($members_info);	
			$this->view->members_info = 	$members_info;
			$this->view->user_id = 	$user_id;	
			$this->view->registrationForm = $registrationForm;
			$this->dynamicUploaderSettings($this->view->form_info);	
		}	
    }
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	public function changeAction() 
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title'), 'Members-Dashboard'), array($this->translator->translator('Member_pass_page_name'), $this->view->url()));
		
		$this->passForm = new Members_Form_ChangePassForm(null);
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			if($this->view->allow())
			{
				if($this->passForm->isValid($this->_request->getPost())) 
				{
					//Get DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					$oldpass = trim($this->_request->getPost('oldpass'));
					$password = str_replace(' ', '', trim($this->_request->getPost('password')));
					$confirmPassword = str_replace(' ', '', trim($this->_request->getPost('confirmPassword')));
					$user = Zend_Auth::getInstance()->getIdentity();
					if($this->_request->getPost('user_id'))
					{
						$user_id = $this->_request->getPost('user_id');
						if($user->allow_reset_user_password == '1')
						{
							$ckOldpass = true;
						}
						else
						{
							$ckOldpass = $this->checkOldPass($oldpass,$user_id,$conn);
						}
					}
					else
					{	
						$user_id = $user->user_id;
						$ckOldpass = $this->checkOldPass($oldpass,$user_id,$conn);
					}				
					
					if($ckOldpass)
					{
						if($password == $confirmPassword)
						{
													
							//Get Salt
							$selectSalt = $conn->select()
								->from(array('u' => Zend_Registry::get('dbPrefix').'user_profile'), array('salt'))
								->where('u.user_id = ?', $user_id)
								->limit(1);
							
							$selectRes = $selectSalt->query()->fetchAll();
							$salt = $selectRes[0]['salt'];
							$newPass = $this->genPassword($password,$salt);
							
							
							$sql = 'UPDATE '.Zend_Registry::get('dbPrefix').'user_profile SET password = "'.$newPass.'" WHERE user_id = '.$conn->quote($user_id).' ';
							
							if($conn->query($sql))
							{
								$msg = $translator->translator('password_change_success');
								$json_arr = array('status' => 'ok','msg' => $msg);
							}
							else
							{
								$msg = $translator->translator('password_change_err');
								$json_arr = array('status' => 'err','msg' => $msg);
							}
						}
						else
						{
							$msg = $translator->translator('password_not_match');
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator('old_password_check_err');
						$json_arr = array('status' => 'err','msg' => $msg);
					}
					
				}
				else
				{
					$validatorMsg = $this->passForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$msg = $translator->translator('common_member_access_deny_to_perform_action');
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$user_id = $this->_request->getParam('user_id');
			$this->view->user_id = $user_id;
			$this->view->passForm = $this->passForm;
		}		
	}
	
	public function genPassword($newPass,$salt)
	{			
		$pass = md5($salt.$newPass);
		return $pass;
	}
	
	public function checkOldPass($oldpass,$user_id,$conn)
	{			
		//Get Salt
		$selectSalt = $conn->select()
			->from(array('u' => Zend_Registry::get('dbPrefix').'user_profile'), array('salt','password'))
			->where('u.user_id = ?', $user_id)
			->limit(1);
		
		$selectRes = $selectSalt->query()->fetchAll();
		$salt = $selectRes[0]['salt'];
		$encodePass = md5($salt.$oldpass);
		
		
		
		$password = $selectRes[0]['password'];
		
		if($encodePass == $password)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}