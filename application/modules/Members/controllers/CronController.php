<?php
/*****
*
*	PLEASE RUN THE FOLLOWING CRON COMMAND IN CPANEL
*	wget -O - -q -t 1 http://demo.realestates.com.bd/Members/cron/account
*
****/

class Members_CronController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_translator;
	private $_controllerCache;
	
    public function init()
    {
        /* Initialize action controller here */				
		$translator = Zend_Registry::get('translator');
		$this->_translator = $translator;	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();				
    }
	
	public function preDispatch() 
	{			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
	}
	
	public function accountAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$mem_db = new Members_Model_DbTable_MemberList();
		$today = date("Y-m-d");	
		$thisMonth = date("m");	
		$module = $this->_request->getModuleName();
		$controller = $this->_request->getControllerName();
		$action = $this->_request->getActionName();
		
		$packages = new Members_Controller_Helper_Packages();
		$field_name = $module.'_'.$controller.'_'.$action;
		
		$mem_info = $mem_db->getMembers();
		if($mem_info)
		{
			foreach($mem_info as $members)
			{
			    $user_id	= $members['user_id'];	
				$role_id	= $members['role_id'];
				$register_date	=	$members['register_date'];	
				$package_info = $packages->getPackageFrontendFieldValue($module,$controller,$action,$user_id,$role_id);
				if($package_info)
				{
					$expire_date = $package_info;					
					$diff = abs(strtotime($register_date) - strtotime($today));
					$years = floor($diff / (365*60*60*24));
					$month = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
					$month_diff = $years*12 + $month;
					if(($month_diff > $expire_date) && $expire_date != '0')
					{
						try
						{
							$where = array();
							$where[] = 'user_id = '.$this->_DBconn->quote($user_id);
							$data = array('status' => '0');
							$this->_DBconn->update(Zend_Registry::get('dbPrefix').'user_profile',$data, $where);
						}
						catch(Exception $e)
						{
							echo $e->getMessage();
						}
					}
				}
			}			
		}
		echo "Cron Successfully Run";		
	}
}

