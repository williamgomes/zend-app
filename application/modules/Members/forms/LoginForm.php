<?php
class Members_Form_LoginForm extends Zend_Form
{
    public function init()
    {
		$global_conf = Zend_Registry::get('global_conf');
		$translator =  Zend_Registry::get('translator');
        $username = $this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower','StripTags'),
            'validators' => array(
                'EmailAddress',
                array('StringLength', false, array(8, 100)),
            ),
            'required'   => true,
            'label'      => $translator->translator('administrator_login_username_label', '', 'Administrator'),
        ));

        $password = $this->addElement('password', 'password', array(
            'filters'    => array('StringTrim'),					
            'validators' => array(				
                array('StringLength', false, array(6, 100))
            ),
            'required'   => true,
            'label'      => $translator->translator('administrator_login_password_label', '', 'Administrator'),
        ));
		   
		
        $login = $this->addElement('submit', 'login', array(
            'required' => false,
            'ignore'   => true,
            'label'    => 'Login',
        ));        
       
        // decorator.
        $this->setElementDecorators(array(
            'ViewHelper','FormElements',
        ));
		
		//Captcha Security
		if($global_conf['captcha_enable'] == '1')
		{
			$this->addElement('Captcha', 'login_captcha',
													array(
															'label' 			=> 	$translator->translator('common_captcha_label'),
															'title' 			=> 	$translator->translator('common_captcha_title'), 
															'info'				=> 	$translator->translator('common_captcha_info'),
															'class' 			=> 	'mod-field login-field',
															'style'   			=>	'width:100px;',							  
															'required'    		=> 	false,
															'captcha' 			=> array(
																					'captcha' 						=> 		'image',
																					'name' 							=> 		'login_captcha',
																					'wordLen' 						=> 		5,
																					'width' 						=> 		100,
																					'height' 						=> 		35,
																					'dotNoiseLevel' 				=> 		1,
																					'lineNoiseLevel' 				=> 		1,
																					'gcFreq' 						=> 		1,																				
																					'fontColor' 					=> 		array(0, 138, 240),
																					'backgroundColor' 				=> 		array(232,232, 232),
																					'font' 							=> 		'data/adminImages/captcha/monofont.ttf',
																					'fontSize' 						=> 		20,
																					'imgDir' 						=> 		'temp/cache',
																					'imgUrl' 						=> 		'temp/cache',
																					'timeout' 						=> 		500
																				)														
														));
			$captcha = $this->getElement('login_captcha');
			$captcha->setDecorators(array());
		}
		
    	$this->doSecurityFiltering();
	}
		
	//set Filters
	public function doSecurityFiltering()
	{
		$filters = array();
		$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
										'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
		$filters[0] = $filter;
		$this->addElementFilters($filters);
	}
	
	//Add Global Filters
	public function addElementFilters(array $filters)
	{
		foreach ($this->getElements() as $element) {
			$element->addFilters($filters);
		}
		return $this;
	}
}
 
?>