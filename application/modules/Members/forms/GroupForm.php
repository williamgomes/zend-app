<?php
class Members_Form_GroupForm  extends Zend_Form 
{
	public function __construct($options = null) 
	{
		$translator = Zend_Registry::get('translator');
		$config = (file_exists( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.group.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.group.ini', 'group') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/en_US.group.ini', 'group');
		parent::__construct($config->group );
	}

	public function init()
	{
		 $this->createForm();
	}

	private function createForm ()
	{	
		$this->elementDecorator();
	 	$this->loadUserGroup ();
		$this->doSecurityFiltering();
	}
		
	//set Filters
	public function doSecurityFiltering()
	{
		$filters = array();
		$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
										'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
		$filters[0] = $filter;
		$this->addElementFilters($filters);
	}
	
	//Add Global Filters
	public function addElementFilters(array $filters)
	{
		foreach ($this->getElements() as $element) {
			$element->addFilters($filters);
		}
		return $this;
	}
	
	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array(
				'ViewHelper','FormElements',						
			));
	}  
	//Load File User Group List in combobox
	public function loadUserGroup ()
	{	
		$translator = Zend_Registry::get('translator');
		$group = new Members_Model_DbTable_Role();						
		$group_options = $group->getOptions();
		if($this->role_id->getType() == 'Zend_Form_Element_Select')
		{
			$this->role_id->addMultiOption("",$translator->translator('common_select_user_group'));	
		}			
		$this->role_id->addMultiOptions($group_options);									 
	}
}