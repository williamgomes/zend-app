<?php
//Common Form generation Class
class Members_Form_GeneralForm  extends Zend_Form 
{
		protected $_options;
		protected $_dynamicElements;
		protected $_editor;
		protected $_tagsAllowed;
		protected $_allowAttribs;
		
		public function __construct($options = null) 
		{
				$this->_options = $options;
				if(!empty($this->_options))
			    {
					$this->_tagsAllowed = explode(',', $this->_options['support_tags']); 
					$this->_allowAttribs = explode(',', $this->_options['support_attribs']);
				}
				parent::__construct($this);
        }
		
        public function init()
        {
             $this->createForm();
        }

        private function createForm ()
		{
			 $this->elementDecorator();	        
			 if(!empty($this->_options))
			 {
				$this->dynamicElements();	
			 }	
			 if(!empty($this->_tagsAllowed[0]) && !empty($this->_options))
			 {								
				$this->setElementFilters(array('StringTrim', new Zend_Filter_StripTags(array('allowTags' => $this->_tagsAllowed,'allowAttribs' => $this->_allowAttribs))));			
        	 } 
			 $this->doSecurityFiltering();
		}
		
		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}		
		
		//Dynamic Elements
		private function dynamicElements()
		{
			$form_info = $this->_options;			
			try
			{
				$field_db = new Members_Model_DbTable_Fields();
				$field_groups = $field_db->getGroupNames($this->_options['id']); 
				
				foreach($field_groups as $group)
				{
					$group_arr = array();
					$group_name = $group->field_group;					
					$field_info = $field_db->getFieldsInfo($this->_options['id'],$group_name);	
					foreach($field_info as $fields)
					{						
						$group_arr[$fields->field_name] = $fields->field_name;
						if($this->_options['display_type'])
						{								
							$required = ($fields->required == '1' && $fields[$this->_options['display_type']] == '1' ) ? true : false;								
						}
						else
						{
							$required = ($fields->required == '1' && $fields->display_admin == '1' && $fields->display_frontend == '1') ? true : false;
						}
						$checked  = ($fields->field_default_value) ? true : false;
						$multiOptions = explode(',',$fields->field_option);
						$multiOptions_arr =	$this->getMultiOptionsValueSerialize($fields);
						
						switch($fields->field_type)
						{
							case 'text':
								if($fields->regexpr)
								{
									if(preg_match("/^[a-zA-Z0-9_]+$/",$fields->regexpr))
									{
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value,
														'validators' => array($fields->regexpr)
														));
									}
									else
									{			
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value));
										$new_element = $this->getElement($fields->field_name);
										$new_element->addValidator('regex', false, array($fields->regexpr));
									}
								}
								else
								{
									$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
								}
								break;							
							case 'file':								
									$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required));
									$this->setFileDecorators($fields->field_name);
								break;
							case 'textarea':
								$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'cols' => $fields->field_width, 'rows' => $fields->field_height, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
								break;
							case 'hidden':
								$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
								break;
							case 'radio':
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> 	$fields->field_label, 
													  'id' 				=> 	$fields->field_id,
													  'required'    	=> 	$required,
													  'rel' 			=> 	$fields->id,
													  'admin' 			=> 	$fields->display_admin, 
													  'frontend' 		=> 	$fields->display_frontend,
													  'info' 			=> 	$fields->field_desc,
													  'class' 			=> 	$fields->field_class, 
													  'title' 			=> 	$fields->field_title,
													  'value' 			=> 	$fields->field_default_value,
													  'separator' 		=> 	$fields->field_separator,
													  'multiOptions' 	=> 	$multiOptions_arr
													));
								  $this->setElementDecorator($fields->field_name);
								break;							
							case 'checkbox':
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> 	$fields->field_label, 
													  'id' 				=> 	$fields->field_id,
													  'required'    	=> 	$required,
													  'rel' 			=> 	$fields->id,
													  'admin' 			=> 	$fields->display_admin, 
													  'frontend' 		=> 	$fields->display_frontend,
													  'info' 			=> 	$fields->field_desc,
													  'class' 			=> 	$fields->field_class, 
													  'title' 			=> 	$fields->field_title,
													  'checkedValue' 	=> 	$multiOptions[0],
													  'uncheckedValue' 	=> 	$multiOptions[1],
													  'checked' 		=>  $checked
													));
									$this->setElementDecorator($fields->field_name);
								break;
							case 'multiCheckbox':								
								$dfault_value_arr = explode(',',$fields->field_default_value);
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> 	$fields->field_label, 
													  'id' 				=> 	$fields->field_id,
													  'required'    	=> 	$required,
													  'rel' 			=> 	$fields->id,
													  'admin' 			=> 	$fields->display_admin, 
													  'frontend' 		=> 	$fields->display_frontend,
													  'info' 			=> 	$fields->field_desc,
													  'class' 			=> 	$fields->field_class, 
													  'title' 			=> 	$fields->field_title,
													  'separator' 		=> 	$fields->field_separator,
													  'multiOptions' 	=> 	$multiOptions_arr,
													  'value' 			=>  $dfault_value_arr
													));
									$this->setElementDecorator($fields->field_name);
									$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
								break;
							case 'select':
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> $fields->field_label, 
													  'id' 				=> $fields->field_id,
													  'class' 			=> $fields->field_class, 
													  'title' 			=> $fields->field_title,
													  'size' 			=> $fields->field_width,
													  'rel' 			=> $fields->id,
													  'admin' 			=> $fields->display_admin, 
													  'info' 			=> 	$fields->field_desc,
													  'frontend' 		=> $fields->display_frontend,
													  'required'   		=> $required,
													  'value' 			=> $fields->field_default_value,
													  'multiOptions' 	=> $multiOptions_arr
													));
								$this->setElementDecorator($fields->field_name);
								$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
								$this->loadCountries ($fields->field_name);
								break;
							case 'multiselect':								
								$dfault_value_arr = explode(',',$fields->field_default_value);
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> $fields->field_label, 
													  'id' 				=> $fields->field_id,
													  'class' 			=> $fields->field_class, 
													  'title' 			=> $fields->field_title,
													  'size' 			=> $fields->field_height,
													  'rel' 			=> $fields->id,
													  'admin' 			=> $fields->display_admin, 
													  'info' 			=> 	$fields->field_desc,
													  'frontend' 		=> $fields->display_frontend,
													  'required'   		=> $required,
													  'value' 			=> $dfault_value_arr,
													  'multiOptions' 	=> $multiOptions_arr
													));
									$this->setElementDecorator($fields->field_name);
									$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
								break;
						}						
					}
					$this->addDisplayGroup($group_arr, $group_name,array('disableLoadDefaultDecorators' => true,'title' => $group_name, 'legend' => $group_name));								
					$last_group_name = $group_name;
				}				
				//Set Captcha in the form				
				if($form_info['captcha_enable'] == '1' && !empty($form_info['captcha_field_name']))
				{					
					$group_arr[$form_info['captcha_field_name']] = $form_info['captcha_field_name'];
					$translator = Zend_Registry::get('translator');	
					if($form_info['common_captcha_show_backend'] || $form_info['common_captcha_show_backend'] == '0')
					{
						if($form_info['common_captcha_show_backend'] == '1')
						{
							$show = true;
						}
						else
						{
							$show = false;
						}
					}
					else
					{
						$show = true;
					}
					
					if ($show) 
					{
						$this->addElement('Captcha', $form_info['captcha_field_name'],
													array(
															'label' 			=> 	$translator->translator('common_captcha_label'),
															'title' 			=> 	$translator->translator('common_captcha_title'), 
															'info'				=> 	$translator->translator('common_captcha_info'),
															'class' 			=> 	$translator->translator('common_captcha_field_class'), 
															'frontend' 			=>  $form_info['captcha_enable'],	
															'admin' 			=> $translator->translator('common_captcha_show_backend'), 												  
															'required'    		=> 	false,
															'captcha' 			=> array(
																					'captcha' 						=> 		'image',
																					'name' 							=> 		$form_info['captcha_field_name'],
																					'wordLen' 						=> 		$form_info['captcha_word_length'],
																					'width' 						=> 		$form_info['captcha_width'],
																					'height' 						=> 		$form_info['captcha_height'],
																					'dotNoiseLevel' 				=> 		$form_info['captcha_dot_noise_level'],
																					'lineNoiseLevel' 				=> 		$form_info['captcha_line_noise_level'],
																					'gcFreq' 						=> 		$form_info['captcha_gc_freq'],																				
																					'fontColor' 					=> 		array($form_info['captcha_font_color_r'], $form_info['captcha_font_color_g'], $form_info['captcha_font_color_b']),
																					'backgroundColor' 				=> 		array($form_info['captcha_background_color_r'], $form_info['captcha_background_color_g'], $form_info['captcha_background_color_b']),
																					'font' 							=> 		$form_info['captcha_font'],
																					'fontSize' 						=> 		$form_info['captcha_font_size'],
																					'imgDir' 						=> 		$form_info['captcha_img_dir'],
																					'imgUrl' 						=> 		$form_info['captcha_img_url'],
																					'timeout' 						=> 		$form_info['captcha_timeout']
																				)														
														));
						$this->setElementDecorator($form_info['captcha_field_name']);	
						$this->addDisplayGroup($group_arr, $last_group_name,array('disableLoadDefaultDecorators' => true,'title' => $last_group_name, 'legend' => $last_group_name));	
					}
				}				
			}
			catch(Exception $e)
			{
				throw new Exception($e->getMessage());				
			}
		}
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));
		} 
		//Each Element Decorator
		private function setElementDecorator($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_Captcha')
			{
				$element->setDecorators(array());
			}
			else
			{
				$element->setDecorators(array(
						'ViewHelper','FormElements',										
						
					));
			}
		} 
		//Load File Decorator Element
		private function setFileDecorators($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_File')
			{
				$element->setDecorators(array('file','file'));			
				//$element->setMaxFileSize($this->_options['attach_file_max_size']);
			}			
		} 
		//Get Multioptions
		private function getMultiOptionsValueSerialize($fields)
		{
			if($fields->field_type == 'select' || $fields->field_type == 'multiselect')
			{
				if(strpos($fields->field_option,';;') && strpos($fields->field_option,'::'))
				{
					$main_arr = explode(';;', $fields->field_option);
					if(!empty($main_arr[0]))
					{
						$multiOptions_arr = array();					
						foreach($main_arr as $main_arr_key => $main_arr_value)
						{
							$group_arr = explode('::', $main_arr_value);
							if(!empty($group_arr[0]) && !empty($group_arr[1]))
							{
								$value_arr = explode(',', $group_arr[1]);
								$options_arr = array();
								foreach($value_arr as $value_arr_key=>$value_arr_value)
								{
									$options_arr[$value_arr_value] = $value_arr_value;
								}							
								$multiOptions_arr[$group_arr[0]] = $options_arr;
							}
						}
					}
					else
					{
						$multiOptions = explode(',',$fields->field_option);
						$multiOptions_arr = array();
						foreach($multiOptions as $key=>$value)
						{
							$multiOptions_arr[$value] = $value;
						}
					}
				}
				else
				{
					$multiOptions = explode(',',$fields->field_option);
					$multiOptions_arr = array();
					foreach($multiOptions as $key=>$value)
					{
						$multiOptions_arr[$value] = $value;
					}
				}
			}
			else
			{
				$multiOptions = explode(',',$fields->field_option);
				$multiOptions_arr = array();
				foreach($multiOptions as $key=>$value)
				{
					$multiOptions_arr[$value] = $value;
				}
			}
			return $multiOptions_arr;
		}      
		//Load File Country List in combobox
        public function loadCountries ($element_name)
		{			
			$element = $this->getElement($element_name);
			$type = $element->getType();
			$element_name_arr = explode('_',$element_name);
			if(in_array(Eicra_File_Constants::COUNTRY, $element_name_arr) && $type == 'Zend_Form_Element_Select')
			{	
				$translator = Zend_Registry::get('translator');			
				$global_conf = Zend_Registry::get('global_conf');
				$countries = new Eicra_Model_DbTable_Country();
				$countries_options = $countries->getOptions();
				$selected = $global_conf['default_country'];
				$option = array('' => $translator->translator('common_select_country'));
				$element->setMultiOptions($option);
				foreach($countries_options as $key=>$value)
				{
					if($selected == $key)
					{
						$element->addMultiOption($value,$value);
						$element->setValue($value);
					}
					else
					{
						$element->addMultiOption($value,$value);
					}
				}
			}								 
		}
		//Load File State List in combobox
		public function loadStates ()
		{
			$translator = Zend_Registry::get('translator');
			$global_conf = Zend_Registry::get('global_conf');
			$country_id = $global_conf['default_country'];
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);			
			$this->state->addMultiOption('0',$translator->translator('common_select_state'));
			$this->state->addMultiOptions($states_options);									 
		}
		//Load File Area List in combobox
		public function loadCities ()
		{	
			$translator = Zend_Registry::get('translator');				
			$this->city->addMultiOption('0',$translator->translator('common_select_area'));											 
		}
		//Load File User Group List in combobox
		public function loadUserGroup ()
		{
			$translator = Zend_Registry::get('translator');	
			$group = new Members_Model_DbTable_Role();						
        	$group_options = $group->getOptions();
			$this->role_id->addMultiOption("",$translator->translator('common_select_user_group'));				
			$this->role_id->addMultiOptions($group_options);			
			$perm = new Members_View_Helper_Allow();
			if(!$perm->allow('add','role','Administrator'))
			{							
				$this->removeElement('role_id');
			}										 
		}
				
		public function setEditor($baseURL,$editor_name)
		{			
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "exact",
								elements: "'.$editor_name.'",
								theme : "advanced",
								plugins : "emotions,inlinepopups,visualchars,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,forecolor,backcolor,fontsizeselect,|,emotions",
								theme_advanced_buttons2 : "",
								theme_advanced_buttons3 : "",
								theme_advanced_buttons4 : "",
								theme_advanced_toolbar_location : "bottom",
								theme_advanced_toolbar_align : "center",
								theme_advanced_statusbar_location : "false",
								
								
								extended_valid_elements : "link[href|src|rel|type]",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
								remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});						
						</script>';
		}
		
		public function setEditorMobile($baseURL,$editor_name)
		{			
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "exact",
								elements: "'.$editor_name.'",
								theme : "advanced",
								plugins : "emotions,inlinepopups,visualchars,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "bold,italic,underline,strikethrough,fontsizeselect",
								theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,forecolor,backcolor,|,emotions",
								theme_advanced_buttons3 : "",
								theme_advanced_buttons4 : "",
								theme_advanced_toolbar_location : "bottom",
								theme_advanced_toolbar_align : "center",
								theme_advanced_statusbar_location : "false",
								
								
								extended_valid_elements : "link[href|src|rel|type]",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
								remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});						
						</script>';
		}
		
		public function setFullEditor($baseURL)
		{
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "none",
								theme : "advanced",
								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "insertimage,image,media,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,insertdate,inserttime,preview,print,|,ltr,rtl",
								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help",
								theme_advanced_buttons3 : "tablecontrols,hr,removeformat,visualaid,sub,sup,charmap,emotions,advhr",
								theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,|,styleprops,del,ins,attribs,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor,|,fullscreen",
								theme_advanced_buttons5 : "fontsizeselect,formatselect,fontselect,styleselect,|,code,iespell",
								theme_advanced_toolbar_location : "top",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								
								
								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type]",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
        						remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});							
							function loadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceAddControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\';
								
							}
							function unloadTinyMCE(id,num)
							{
								tinyMCE.execCommand(\'mceRemoveControl\', false, id);
								document.getElementById(\'loaderLink\'+num).innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ',\'+num+\');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\';
							}
						</script>';
		}
		
		public function getEditor()
		{
			echo $this->_editor;
		}  
}