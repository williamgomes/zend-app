<?php

class Members_Form_UserForm  extends Zend_Form 
{
		protected $_options;
		protected $_dynamicElements;
		protected $_tagsAllowed;
		protected $_allowAttribs;
		
		public function __construct($options = null) 
		{				
				$this->_options = $options;				
				$translator = Zend_Registry::get('translator');
                $config = (file_exists( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.form.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.form.ini', 'registration') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/en_US.form.ini', 'registration');
                parent::__construct($config->registration,array('disableLoadDefaultDecorators' => true) );
        }

        public function init()
        {
             $this->createForm();
        }

        private function createForm ()
		{		 
			 $this->elementDecorator();			 
			 if($this->country)
			 {			 
				$this->loadCountries ();
			 }
			 if($this->role_id)
			 {	
				$this->loadUserGroup ();
			 }			
			 if(!empty($this->_options))
			 {
				$this->dynamicElements();	
			 }	
			 if(!empty($this->_tagsAllowed[0]) && !empty($this->_options['form_id']))
			 {
				$this->setElementFilters(array('StringTrim', new Zend_Filter_StripTags(array('allowTags' => $this->_tagsAllowed,'allowAttribs' => $this->_allowAttribs))));	
			 }			
        	 $this->doSecurityFiltering();
		}
		
		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}	
		
		//Dynamic Elements
		private function dynamicElements()
		{
			if(!empty($this->_options['form_id']))
			{
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($this->_options['form_id']);
				$form_info['common_captcha_show_backend']	=	($this->_options['common_captcha_show_backend'] || $this->_options['common_captcha_show_backend'] == '0') ? $this->_options['common_captcha_show_backend'] : '';
				if(!empty($form_info))
			    {
					$this->_tagsAllowed = explode(',', $form_info['support_tags']); 
					$this->_allowAttribs = explode(',', $form_info['support_attribs']);
				}
				try
				{
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($this->_options['form_id']); 
					$last_group_name = 'Member_profile_page_captcha_info';
					$last_group_order = 6;
					
					foreach($field_groups as $group)
					{
						$group_arr = array();
						$group_name = $group->field_group;						
						$field_info = $field_db->getFieldsInfo($this->_options['form_id'],$group_name);	
						foreach($field_info as $fields)
						{						
							$group_arr[$fields->field_name] = $fields->field_name;
							if($this->_options['display_type'])
							{								
								$required = ($fields->required == '1' && $fields[$this->_options['display_type']] == '1' ) ? true : false;								
							}
							else
							{
								$required = ($fields->required == '1' && $fields->display_admin == '1' && $fields->display_frontend == '1') ? true : false;
							}
							$checked  = ($fields->field_default_value) ? true : false;
							$multiOptions = explode(',',$fields->field_option);
							$multiOptions_arr =	$this->getMultiOptionsValueSerialize($fields);
							
							switch($fields->field_type)
							{
								case 'text':
									if($fields->regexpr)
									{
										if(preg_match("/^[a-zA-Z0-9_]+$/",$fields->regexpr))
										{
											$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value,
															'validators' => array($fields->regexpr)
															));
										}
										else
										{			
											$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'  => $required, 'value' => $fields->field_default_value));
											$new_element = $this->getElement($fields->field_name);
											$new_element->addValidator('regex', false, array($fields->regexpr));
										}
									}
									else
									{
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
									}
									break;
								case 'file':								
										$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'size' => $fields->field_width, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required));
										$this->setFileDecorators($fields->field_name);
									break;
								case 'textarea':
									$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'cols' => $fields->field_width, 'rows' => $fields->field_height, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'info' => $fields->field_desc, 'frontend' => $fields->display_frontend, 'required'    => $required, 'value' => $fields->field_default_value));
									break;
								case 'hidden':
								$this->addElement($fields->field_type, $fields->field_name, array('label' => $fields->field_label,'id' => $fields->field_id, 'class' => $fields->field_class, 'title' => $fields->field_title, 'rel' => $fields->id, 'admin' => $fields->display_admin, 'frontend' => $fields->display_frontend, 'info' => $fields->field_desc, 'required'    => $required, 'value' => $fields->field_default_value));
								break;
								case 'radio':
									$this->addElement($fields->field_type, $fields->field_name,
													array('label' 			=> 	$fields->field_label, 
														  'id' 				=> 	$fields->field_id,
														  'required'    	=> 	$required,
														  'rel' 			=> 	$fields->id,
														  'admin' 			=> 	$fields->display_admin, 
														  'frontend' 		=> 	$fields->display_frontend,
														  'info' 			=> 	$fields->field_desc,
														  'class' 			=> 	$fields->field_class, 
														  'title' 			=> 	$fields->field_title,
														  'value' 			=> 	$fields->field_default_value,
														  'separator' 		=> 	$fields->field_separator,
														  'multiOptions' 	=> 	$multiOptions_arr
														));
										$this->setElementDecorator($fields->field_name);
									break;							
								case 'checkbox':
									$this->addElement($fields->field_type, $fields->field_name,
													array('label' 			=> 	$fields->field_label, 
														  'id' 				=> 	$fields->field_id,
														  'required'    	=> 	$required,
														  'rel' 			=> 	$fields->id,
														  'admin' 			=> 	$fields->display_admin, 
														  'frontend' 		=> 	$fields->display_frontend,
														  'info' 			=> 	$fields->field_desc,
														  'class' 			=> 	$fields->field_class, 
														  'title' 			=> 	$fields->field_title,
														  'checkedValue' 	=> 	$multiOptions[0],
														  'uncheckedValue' 	=> 	$multiOptions[1],
														  'checked' 		=>  $checked
														));
										$this->setElementDecorator($fields->field_name);
									break;
								case 'multiCheckbox':								
								$dfault_value_arr = explode(',',$fields->field_default_value);
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> 	$fields->field_label, 
													  'id' 				=> 	$fields->field_id,
													  'required'    	=> 	$required,
													  'rel' 			=> 	$fields->id,
													  'admin' 			=> 	$fields->display_admin, 
													  'frontend' 		=> 	$fields->display_frontend,
													  'info' 			=> 	$fields->field_desc,
													  'class' 			=> 	$fields->field_class, 
													  'title' 			=> 	$fields->field_title,
													  'separator' 		=> 	$fields->field_separator,
													  'multiOptions' 	=> 	$multiOptions_arr,
													  'value' 			=>  $dfault_value_arr
													));
									$this->setElementDecorator($fields->field_name);
									$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
								break;
								case 'select':
									$this->addElement($fields->field_type, $fields->field_name,
													array('label' 			=> $fields->field_label, 
														  'id' 				=> $fields->field_id,
														  'class' 			=> $fields->field_class, 
														  'title' 			=> $fields->field_title,
														  'size' 			=> $fields->field_width,
														  'rel' 			=> $fields->id,
														  'admin' 			=> $fields->display_admin, 
														  'frontend' 		=> $fields->display_frontend,
														  'info' 			=> 	$fields->field_desc,
														  'required'   		=> $required,
														  'value' 			=> $fields->field_default_value,
														  'multiOptions' 	=> $multiOptions_arr
														));
									  $this->setElementDecorator($fields->field_name);
									  $this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
									  $this->loadCountriesDynamic ($fields->field_name);
									  $this->loadPackages ($fields->field_name);
									break;
								case 'multiselect':								
								$dfault_value_arr = explode(',',$fields->field_default_value);
								$this->addElement($fields->field_type, $fields->field_name,
												array('label' 			=> $fields->field_label, 
													  'id' 				=> $fields->field_id,
													  'class' 			=> $fields->field_class, 
													  'title' 			=> $fields->field_title,
													  'size' 			=> $fields->field_height,
													  'rel' 			=> $fields->id,
													  'admin' 			=> $fields->display_admin, 
													  'info' 			=> 	$fields->field_desc,
													  'frontend' 		=> $fields->display_frontend,
													  'required'   		=> $required,
													  'value' 			=> $dfault_value_arr,
													  'multiOptions' 	=> $multiOptions_arr
													));
									$this->setElementDecorator($fields->field_name);
									$this->getElement($fields->field_name)->setRegisterInArrayValidator(false);
								break;
							}
						}
						$this->addDisplayGroup($group_arr, $group_name,array('disableLoadDefaultDecorators' => true,'title' => $group_name, 'legend' => $group_name, 'order' => $fields->group_order ));								
						$last_group_name = $group_name;
						$last_group_order = $fields->group_order;
					}
					
					//Set Captcha in the form
					if($form_info['captcha_enable'] == '1' && !empty($form_info['captcha_field_name']))
					{					
						$group_arr[$form_info['captcha_field_name']] = $form_info['captcha_field_name'];
						$translator = Zend_Registry::get('translator');	
						if($form_info['common_captcha_show_backend'] || $form_info['common_captcha_show_backend'] == '0')
						{
							if($form_info['common_captcha_show_backend'] == '1')
							{
								$show = true;
							}
							else
							{
								$show = false;
							}
						}
						else
						{
							$show = true;
						}
						if ($show) 
						{
							$this->addElement('Captcha', $form_info['captcha_field_name'],
														array(
																'label' 			=> 	$translator->translator('common_captcha_label'),
																'title' 			=> 	$translator->translator('common_captcha_title'), 
																'info'				=> 	$translator->translator('common_captcha_info'),
																'class' 			=> 	$translator->translator('common_captcha_field_class'), 
																'frontend' 			=>  $form_info['captcha_enable'],	
																'admin' 			=> $translator->translator('common_captcha_show_backend'), 												  
																'required'    		=> 	false,
																'captcha' 			=> array(
																						'captcha' 						=> 		'image',
																						'name' 							=> 		$form_info['captcha_field_name'],
																						'wordLen' 						=> 		$form_info['captcha_word_length'],
																						'width' 						=> 		$form_info['captcha_width'],
																						'height' 						=> 		$form_info['captcha_height'],
																						'dotNoiseLevel' 				=> 		$form_info['captcha_dot_noise_level'],
																						'lineNoiseLevel' 				=> 		$form_info['captcha_line_noise_level'],
																						'gcFreq' 						=> 		$form_info['captcha_gc_freq'],																				
																						'fontColor' 					=> 		array($form_info['captcha_font_color_r'], $form_info['captcha_font_color_g'], $form_info['captcha_font_color_b']),
																						'backgroundColor' 				=> 		array($form_info['captcha_background_color_r'], $form_info['captcha_background_color_g'], $form_info['captcha_background_color_b']),
																						'font' 							=> 		$form_info['captcha_font'],
																						'fontSize' 						=> 		$form_info['captcha_font_size'],
																						'imgDir' 						=> 		$form_info['captcha_img_dir'],
																						'imgUrl' 						=> 		$form_info['captcha_img_url'],
																						'timeout' 						=> 		$form_info['captcha_timeout']
																					)														
															));
							$this->setElementDecorator($form_info['captcha_field_name']);	
							$this->addDisplayGroup($group_arr, $last_group_name,array('disableLoadDefaultDecorators' => true,'title' => $last_group_name, 'legend' => $last_group_name, 'order' => $last_group_order));	
						}
					}				
				}
				catch(Exception $e)
				{
					throw new Exception($e->getMessage());				
				}
			}
		}
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));
		} 
		//Each Element Decorator
		private function setElementDecorator($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_Captcha')
			{
				$element->setDecorators(array());
			}
			else
			{
				$element->setDecorators(array(
						'ViewHelper','FormElements',										
						
					));
			}
		} 
		//Load File Decorator Element
		private function setFileDecorators($field_name)
		{
			$element = $this->getElement($field_name);
			if($element->getType() == 'Zend_Form_Element_File')
			{
				$element->setDecorators(array('file','file'));			
				//$element->setMaxFileSize($this->_options['attach_file_max_size']);
			}			
		}
		
		//Get Multioptions
		private function getMultiOptionsValueSerialize($fields)
		{
			if($fields->field_type == 'select' || $fields->field_type == 'multiselect')
			{
				if(strpos($fields->field_option,';;') && strpos($fields->field_option,'::'))
				{
					$main_arr = explode(';;', $fields->field_option);
					if(!empty($main_arr[0]))
					{
						$multiOptions_arr = array();					
						foreach($main_arr as $main_arr_key => $main_arr_value)
						{
							$group_arr = explode('::', $main_arr_value);
							if(!empty($group_arr[0]) && !empty($group_arr[1]))
							{
								$value_arr = explode(',', $group_arr[1]);
								$options_arr = array();
								foreach($value_arr as $value_arr_key=>$value_arr_value)
								{
									$options_arr[$value_arr_value] = $value_arr_value;
								}							
								$multiOptions_arr[$group_arr[0]] = $options_arr;
							}
						}
					}
					else
					{
						$multiOptions = explode(',',$fields->field_option);
						$multiOptions_arr = array();
						foreach($multiOptions as $key=>$value)
						{
							$multiOptions_arr[$value] = $value;
						}
					}
				}
				else
				{
					$multiOptions = explode(',',$fields->field_option);
					$multiOptions_arr = array();
					foreach($multiOptions as $key=>$value)
					{
						$multiOptions_arr[$value] = $value;
					}
				}
			}
			else
			{
				$multiOptions = explode(',',$fields->field_option);
				$multiOptions_arr = array();
				foreach($multiOptions as $key=>$value)
				{
					$multiOptions_arr[$value] = $value;
				}
			}
			return $multiOptions_arr;
		} 
		
		//Load Membership Packages
        public function loadPackages ($element_name)
		{
			$translator = Zend_Registry::get('translator');
			$GroupsObj = $this->getDisplayGroups(); 
			$packageObj = new Members_Controller_Helper_Packages();
			$element = $this->getElement($element_name);
			$name = $element->getName();
			if($packageObj->checkPackageForm($name))
			{
				if($element->getType() == 'Zend_Form_Element_Select')
				{												
					$element->addMultiOption("",$translator->translator('common_select'));	
					$datas = $packageObj->getFormDatas($name);
					if(!empty($datas))
					{	
						foreach($datas as $data)
						{
							$element->addMultiOption($data['id'],stripslashes($data['name']).' ('.$data['price'].' '.$data['currency'].')');	
							if(empty($data['price']))
							{
								$element->setValue($data['id']);
							}
						}
					}
				}
			}
			
			if(class_exists(Members_Model_DbTable_Package))
			{				
				if($name == 'package_id')
				{
					if($element->getType() == 'Zend_Form_Element_Select')
					{
						$element->addMultiOption("",$translator->translator('common_select'));	
						$package_db = new Members_Model_DbTable_Package();
						$package_info = $package_db->getAllPackages ();						
						if($package_info)
						{
							$global_conf = Zend_Registry::get('global_conf');
							$currency = new Zend_Currency($global_conf['default_locale']);
							$c_symbol = $currency->getSymbol();
							foreach($package_info as $info)
							{
								$package_price = (empty($info['price']))? $translator->translator('b2b_package_member_free', '','B2b') : $info['price'].' '.$c_symbol;
								$element->addMultiOption($info['id'],stripslashes($info['name']).' ('.$package_price.')');	
								if(empty($info['price']))
								{
									$element->setValue($info['id']);
								}
							}
						}
					}
				}
			}
		}
		       
		//Load File Country List in combobox
        public function loadCountries ()
		{
			$global_conf = Zend_Registry::get('global_conf');
			$countries = new Eicra_Model_DbTable_Country();
        	$countries_options = $countries->getOptions();
			$selected = $global_conf['default_country'];
			foreach($countries_options as $key=>$value)
			{
				if($selected == $key)
				{
					$this->country->addMultiOption($key,$value);
					$this->country->setValue($selected);
				}
				else
				{
					$this->country->addMultiOption($key,$value);
				}
			}						 
		}
		
		public function loadCountriesDynamic ($element_name)
		{			
			$element = $this->getElement($element_name);
			$type = $element->getType();
			$element_name_arr = explode('_',$element_name);
			if(in_array(Eicra_File_Constants::COUNTRY, $element_name_arr) && $type == 'Zend_Form_Element_Select')
			{	
				$translator = Zend_Registry::get('translator');			
				$global_conf = Zend_Registry::get('global_conf');
				$countries = new Eicra_Model_DbTable_Country();
				$countries_options = $countries->getOptions();
				$selected = $global_conf['default_country'];
				$option = array('' => $translator->translator('common_select_country'));
				$element->setMultiOptions($option);
				foreach($countries_options as $key=>$value)
				{
					if($selected == $key)
					{
						$element->addMultiOption($value,$value);
						$element->setValue($value);
					}
					else
					{
						$element->addMultiOption($value,$value);
					}
				}
			}								 
		}
		//Load File State List in combobox
		public function loadStates ()
		{
			$translator = Zend_Registry::get('translator');
			$global_conf = Zend_Registry::get('global_conf');
			$country_id = $global_conf['default_country'];
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);			
			$this->state->addMultiOption('0',$translator->translator('common_select_state'));
			$this->state->addMultiOptions($states_options);									 
		}
		//Load File Area List in combobox
		public function loadCities ()
		{	
			$translator = Zend_Registry::get('translator');				
			$this->city->addMultiOption('0',$translator->translator('common_select_area'));											 
		}
		//Load File User Group List in combobox
		public function loadUserGroup ()
		{	
			$translator = Zend_Registry::get('translator');
			$group = new Members_Model_DbTable_Role();	
			$auth = Zend_Auth::getInstance ();
			$perm = new Members_View_Helper_Allow();
			
			$frontController = Zend_Controller_Front::getInstance();
			$action = $frontController->getRequest()->getActionName();
			
			if ($auth->hasIdentity ())
			{	
				if($action == 'profile' || ($action == 'edit' && $auth->getIdentity ()->user_id == $frontController->getRequest()->getParam('user_id')))
				{
					
					if($auth->getIdentity ()->allow_change_own_profile_role == '1')
					{	
						$group_options = $group->getAllOptions();
						$this->role_id->addMultiOption("",$translator->translator('common_select_user_group'));				
						$this->role_id->addMultiOptions($group_options);
					}
					else
					{
						$this->removeElement('role_id');
					}					
				}
				else
				{
					if($auth->getIdentity ()->allow_change_user_role == '1')
					{				
						
						$group_options = $group->getOptions();
						
						$this->role_id->addMultiOption("",$translator->translator('common_select_user_group'));				
						$this->role_id->addMultiOptions($group_options);
					}
					else
					{
						$this->removeElement('role_id');
					}
				}
			}
			else
			{				
        		$this->removeElement('role_id');
			}
			
			
			/*if(!$perm->allow('add','role','Administrator'))
			{								
				$this->removeElement('role_id');
			}*/										 
		}
		
		
		public function addDisplayGroups(array $groups_arr)
		{
			foreach($groups_arr as $groups)
			{
			
				foreach ($groups as $key => $spec) {
					$name = null;
					if (!is_numeric($key)) {
						$name = $key;
					}
		
					if ($spec instanceof Zend_Form_DisplayGroup) {
						$this->_addDisplayGroupObject($spec);
					}
		
					if (!is_array($spec) || empty($spec)) {
						continue;
					}
		
					$argc    = count($spec);
					$options = array();
		
					if (isset($spec['elements'])) {
						$elements = $spec['elements'];
						if (isset($spec['name'])) {
							$name = $spec['name'];
						}
						if (isset($spec['options'])) {
							$options = $spec['options'];
						}
						$this->addDisplayGroup($elements, $name, $options);
					} else {
						switch ($argc) {
							case (1 <= $argc):
								$elements = array_shift($spec);
								if (!is_array($elements) && (null !== $name)) {
									$elements = array_merge((array) $elements, $spec);
									$this->addDisplayGroup($elements, $name);
									break;
								}
							case (2 <= $argc):
								if (null !== $name) {
									$options = array_shift($spec);
									$this->addDisplayGroup($elements, $name, $options);
									break;
								}
								$name = array_shift($spec);
							case (3 <= $argc):
								$options = array_shift($spec);
							default:
								$this->addDisplayGroup($elements, $name, $options);
						}
					}
				}
			}
			return $this;
		}
}