<?php
class Members_Form_ChangePassForm  extends Zend_Form 
{
	public function __construct($options = null) 
	{
		$translator = Zend_Registry::get('translator');
		$config = (file_exists( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.ChangePassForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/'.$translator->getLangFile().'.ChangePassForm.ini', 'registration') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Members/forms/source/en_US.ChangePassForm.ini', 'registration');
		parent::__construct($config->registration );
		if($options != null)
		{
			if($options->allow_reset_user_password == '1')
			{
				$this->oldpass->setRequired(false);
			}
		}
	}

	public function init()
	{
		 $this->createForm();
	}

	private function createForm ()
	{
		 $this->elementDecorator();	
	}
	
	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array(
				'ViewHelper','FormElements'
				));
	}      
}