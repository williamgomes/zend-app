<?php
class Articles_Controller_Helper_Orders
{
	protected $_article_id;
	protected $_high_article_order;
	protected $_order;
	protected $_conn;
	protected $_translator; 
	
	public function __construct($article_id = null)
	{		
		//DB Connection
		$this->_translator 	=   Zend_Registry::get('translator');
		$this->_conn = Zend_Registry::get('msqli_connection');
		if($article_id != null && $article_id != '')
		{
			$this->setArticleInfo($article_id);
			$this->setHeighestOrder();
		}		
	}
	
	public function setArticleInfo($article_id) 
	{
		$this->_conn->getConnection();
		$this->_article_id = $article_id;
		//Get It's Info
		$select = $this->_conn->select()
					   ->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.order'))
					   ->where('article_id = ?',$article_id);
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_order = (int)$row['order'];
			}
		}
	}
	
	public function setHeighestOrder() 
	{
		$this->_conn->getConnection();
		//Get Heighest Order
		
		$select = $this->_conn->select()
					   ->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.order'))
					   ->order(array('a.order DESC'))
					   ->limit(1);
		
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_high_article_order = (int)$row['order'];				
			}
		}
	}
	
	public function setNewOrder($article_id) 
	{
		$this->_conn->getConnection();
		$this->_high_article_order = $this->_high_article_order + 1;
		//Get Heighest Order
		$where = array();
		$where[] = 'article_id = '.$this->_conn->quote($article_id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $this->_high_article_order),$where);	
	}
	
	public function decreaseOrder($article_id = null,$order = null)
	{
		try
		{
			if(empty($this->_article_id))
			{
				$this->_article_id = $article_id;
			}
			if(empty($this->_order))
			{
				$this->_order = $order;
			}
			
			if($this->_order > 1)
			{
				$up_order = $this->_order - 1;
				
				$this->_conn->getConnection();
				
				//Get Articles which Have New Article Order
				$select = $this->_conn->select()
								   ->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.article_id'))
								   ->where('a.order = ?',$up_order);
				$rs = $select->query()->fetchAll();
				
				if($rs)
				{	
					$article_id_arr = array();		
					$i=0;
					foreach($rs as $row)
					{
						$article_id_arr[$i] = (int)$row['article_id'];
						//Update existing Articles with new Article order
						$where = array();
						$where[] = 'article_id = '.$this->_conn->quote($row['article_id']);
						$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $this->_order),$where);	
						$i++;			
					}
				}
				else
				{
					$article_id_arr = null;
				}				
				
				//Increase Article Order
				$whereI = array();
				$whereI[] = 'article_id = '.$this->_conn->quote($this->_article_id);
				$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $up_order),$whereI);
				$return_arr = array('status' => 'ok','id_arr' => $article_id_arr, 'msg' => '');	
			}
			else
			{
				$article_id_arr = null;
				$return_arr = array('status' => 'err','id_arr' => $article_id_arr, 'msg' => $this->_translator->translator('common_order_up_error'));
			}
		}
		catch(Exception $e)
		{
			$return_arr = array('status' => 'err','id_arr' => $article_id_arr, 'msg' => $e->getMessage());
		}
		
		return $return_arr;
	}
	
	public function increaseOrder($article_id = null,$order = null)
	{
		try
		{
			if(empty($this->_article_id))
			{
				$this->_article_id = $article_id;
			}
			if(empty($this->_order))
			{
				$this->_order = $order;
			}
			
			if($this->_order)
			{
				$new_order = $this->_order + 1;
				
				$this->_conn->getConnection();
				
				//Get Articles which Have New Article Order
				$select = $this->_conn->select()
								   ->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.article_id'))
								   ->where('a.order = ?',$new_order);
				$rs = $select->query()->fetchAll();
				
				if($rs)
				{	
					$article_id_arr = array();		
					$i=0;
					foreach($rs as $row)
					{
						$article_id_arr[$i] = (int)$row['article_id'];	
						//Update existing Articles with new Article order
						$where = array();
						$where[] = 'article_id = '.$this->_conn->quote($row['article_id']);
						$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $this->_order),$where);	
						$i++;			
					}
				}
				else
				{
					$article_id_arr = null;
				}			
				
				//Increase Article Order
				$whereI = array();
				$whereI[] = 'article_id = '.$this->_conn->quote($this->_article_id);
				$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $new_order),$whereI);
				$return_arr = array('status' => 'ok','id_arr' => $article_id_arr, 'msg' => '');	
			}
			else
			{
				$article_id_arr = null;
				$return_arr = array('status' => 'err','id_arr' => $article_id_arr, 'msg' => $this->_translator->translator('common_order_down_error'));
			}
		}
		catch(Exception $e)
		{
			$return_arr = array('status' => 'err','id_arr' => $article_id_arr, 'msg' => $e->getMessage());
		}
		
		return $return_arr;
	}
	
	public function saveOrder($new_order)
	{
		try
		{
			$this->_conn->getConnection();
			
			//Get Articles which Have New Article Order
			$select = $this->_conn->select()
							   ->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.article_id'))
							   ->where('a.order = ?',$new_order);
			$rs = $select->query()->fetchAll();
			
			if($rs)
			{	
				$article_id_arr = array();		
				$i=0;
				foreach($rs as $row)
				{
					$article_id_arr[$i] = (int)$row['article_id'];	
					//Update existing Articles with new Article order
					$where = array();
					$where[] = 'article_id = '.$this->_conn->quote($row['article_id']);
					$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $this->_order),$where);	
					$i++;			
				}
			}		
			
			//Increase Article Order
			$whereI = array();
			$whereI[] = 'article_id = '.$this->_conn->quote($this->_article_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'articles',array('order' => $new_order),$whereI);
			$return_arr = array('status' => 'ok', 'msg' => '');
		}
		catch(Exception $e)
		{
			$return_arr = array('status' => 'err', 'msg' => $e->getMessage());
		}	
		return $return_arr; 	
	}	
}
?>