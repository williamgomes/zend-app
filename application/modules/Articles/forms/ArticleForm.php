<?php
class Articles_Form_ArticleForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Articles/forms/source/'.$translator->getLangFile().'.ArticleForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Articles/forms/source/'.$translator->getLangFile().'.ArticleForm.ini', 'registration') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Articles/forms/source/en_US.ArticleForm.ini', 'registration');
            parent::__construct($config->registration );
        }

        public function init()
        {
            $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
		 	$this->elementDecorator();
			$this->doSecurityFiltering();			
        }
		
		//set Filters
		public function doSecurityFiltering()
		{
			$filters = array();
			$filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
                                            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
			$filters[0] = $filter;
			$this->addElementFilters($filters);
		}
		
		//Add Global Filters
		public function addElementFilters(array $filters)
		{
			foreach ($this->getElements() as $element) {
				$element->addFilters($filters);
			}
			return $this;
		}
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements'					
				));
		}
				
		public function setEditor($baseURL)
		{
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/lib/codemirror.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/ui/codemirror-ui.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/javascript/javascript.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/htmlmixed/htmlmixed.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/css/css.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/xml/xml.js"></script>';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/lib/codemirror.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/ui/codemirror-ui.css" media="screen" rel="stylesheet" type="text/css" />';			
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/javascript/javascript.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/xml/xml.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/css/css.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "none",
								theme : "advanced",
								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "insertimage,save,newdocument,|,charmap,emotions,iespell,media,advhr,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,backcolor,|,insertdate,styleselect",
								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,forecolor,|,inserttime,formatselect",
								theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,ltr,rtl,|,print,|,fullscreen,code,|,preview,fontselect",
								theme_advanced_buttons4 : "fontsizeselect,visualchars,nonbreaking,template,pagebreak,|,cite,abbr,acronym,del,ins,attribs",
								theme_advanced_toolbar_location : "top",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								
								
								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type],style[type|title|disabled|media],script[type|src],center",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
        						remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});
							function loadTinyMCE(id)
							{
								tinyMCE.execCommand(\'mceAddControl\', false, id);
								document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\';
							}
							function unloadTinyMCE(id)
							{
								tinyMCE.execCommand(\'mceRemoveControl\', false, id);
								document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\';
								
							}
							function openCodeMirror(id)
							{
								//first set up some variables
								var textarea = document.getElementById(id);
								var uiOptions = { path : \'js/codemirror\', searchMode : \'popup\' }
								var codeMirrorOptions = { mode: "htmlmixed", lineNumbers: true}
								
								//then create the editor
								editor = new CodeMirrorUI(textarea,uiOptions,codeMirrorOptions);								
							}
							function closeCodeMirror(id)
							{
								$(\'#\'+id).css(\'display\',\'inline\');
								if(editor)
								{
									$(\'#\'+id).val(editor.mirror.getValue());
								}
								var element = $(\'#\'+id).parent();
								$(\'div.codemirror-ui-clearfix\').parent().remove();
								$(\'div.CodeMirror\').remove();				
							}
							$(document).ready(function() {
								var editor = \'\';
								$(\'a.loaderLink_class\').click(function() {
									var self = this;
									var rel = $(self).attr(\'rel\').split(\',\');
									switch(rel[0])
									{
										case \'wysiwyg\':
											$(self).attr(\'rel\',\'html,\'+rel[1]);
											closeCodeMirror(rel[1]);
											tinyMCE.execCommand(\'mceAddControl\', false, rel[1]);											
											$(self).html(\'<img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\');
											break;
										case \'html\':
											tinyMCE.execCommand(\'mceRemoveControl\', false, rel[1]);
											$(self).attr(\'rel\',\'wysiwyg,\'+rel[1]);
											$(self).html(\'<img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\');
											openCodeMirror(rel[1]);
											break;
									}
								});
							});
						</script>'; 
		}
		
		public function setEditorMobile($baseURL)
		{
			$translator = Zend_Registry::get('translator');	
			$this->_editor = '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/lib/codemirror.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/ui/codemirror-ui.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/javascript/javascript.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/htmlmixed/htmlmixed.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/css/css.js"></script>';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/codemirror/mode/xml/xml.js"></script>';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/lib/codemirror.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/ui/codemirror-ui.css" media="screen" rel="stylesheet" type="text/css" />';			
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/javascript/javascript.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/xml/xml.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<link href="'.$baseURL.'/js/codemirror/mode/css/css.css" media="screen" rel="stylesheet" type="text/css" />';
			$this->_editor .= '<script type="text/javascript" src="'.$baseURL.'/js/tiny_mce/tiny_mce.js"></script>';
			$this->_editor .= '<script language="javascript" type="text/javascript"   src="'.$baseURL.'/js/tiny_mce/plugins/tinybrowser/tb_standalone.js.php"></script>';
			$this->_editor .= '<script type="text/javascript">
								tinyMCE.init({
								// General options
								mode : "none",
								theme : "simple",
								plugins : "imagemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
								skin : "o2k7",
								skin_variant : "silver",
								// Theme options
								
								theme_advanced_buttons1 : "insertimage,save,newdocument,|,charmap,emotions,iespell,media,advhr,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleprops",
								theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,forecolor,|,inserttime",
								theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,preview",
								theme_advanced_buttons4 : "fontsizeselect,visualchars,nonbreaking,template,pagebreak,|,cite,abbr,acronym,del,ins,attribs,|,print,|,fullscreen,code",
								theme_advanced_buttons5 : "insertlayer,moveforward,movebackward,absolute,|,backcolor,|,insertdate,styleselect,|,link,unlink,anchor,image,cleanup,help",
								theme_advanced_buttons6 : "formatselect,|,sub,sup,ltr,rtl,fontselect",
								theme_advanced_toolbar_location : "top",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								
								
								extended_valid_elements : "iframe[src|width|height|name|align|border|style|frameborder|scrolling|marginheight|marginwidth],link[href|src|rel|type],style[type|title|disabled|media],script[type|src],center",
								convert_fonts_to_spans :  false,
								font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
								theme_advanced_default_font : "[arial|30] ",
								theme_advanced_resizing : true,
								
								forced_root_block : false,
								force_br_newlines : true,
								force_p_newlines : false,
								relative_urls : false,
								
								relative_urls : true,
        						remove_script_host : true,
								
								document_base_url : "'.$baseURL.'/",
						
								// Example content CSS (should be your site CSS)
								content_css : "js/tiny_mce/themes/advanced/skins/o2k7/content.css",
						
								// Drop lists for link/image/media/template dialogs
								template_external_list_url : "lists/template_list.js",
								external_link_list_url : "lists/link_list.js",
								external_image_list_url : "lists/image_list.js",
								media_external_list_url : "lists/media_list.js",
						
								// Replace values for the template plugin
								template_replace_values : {
									username : "Some User",
									staffid : "991234"
								},
								template_popup_width : "500",
								template_popup_height : "400",
								template_templates : [
									{
										title : "Newsletter Template",
										src : "'.$baseURL.'/newsletterTemplate/editor_details.html",
										description : "Adds Editors Name and Staff ID"
									}
								]
						
							});
							function loadTinyMCE(id)
							{
								tinyMCE.execCommand(\'mceAddControl\', false, id);
								document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="unloadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\';
							}
							function unloadTinyMCE(id)
							{
								tinyMCE.execCommand(\'mceRemoveControl\', false, id);
								document.getElementById(\'loaderLink\').innerHTML = \'<a href="javascript:void(0);" onclick="loadTinyMCE(';
								$this->_editor .= "\''+id+'\'";
								$this->_editor .= ');"><img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\';
								
							}
							function openCodeMirror(id)
							{
								//first set up some variables
								var textarea = document.getElementById(id);
								var uiOptions = { path : \'js/codemirror\', searchMode : \'popup\' }
								var codeMirrorOptions = { mode: "htmlmixed", lineNumbers: true}
								
								//then create the editor
								editor = new CodeMirrorUI(textarea,uiOptions,codeMirrorOptions);								
							}
							function closeCodeMirror(id)
							{
								$(\'#\'+id).css(\'display\',\'inline\');
								if(editor)
								{
									$(\'#\'+id).val(editor.mirror.getValue());
								}
								var element = $(\'#\'+id).parent();
								$(\'div.codemirror-ui-clearfix\').parent().remove();
								$(\'div.CodeMirror\').remove();				
							}
							$(document).ready(function() {
								var editor = \'\';
								$(\'a.loaderLink_class\').click(function() {
									var self = this;
									var rel = $(self).attr(\'rel\').split(\',\');
									switch(rel[0])
									{
										case \'wysiwyg\':
											$(self).attr(\'rel\',\'html,\'+rel[1]);
											closeCodeMirror(rel[1]);
											tinyMCE.execCommand(\'mceAddControl\', false, rel[1]);											
											$(self).html(\'<img src="application/modules/Administrator/layouts/scripts/images/common/html.gif" border="0" title="'.$translator->translator('common_editor_close').'" alt="'.$translator->translator('common_editor_close').'" /></a>\');
											break;
										case \'html\':
											tinyMCE.execCommand(\'mceRemoveControl\', false, rel[1]);
											$(self).attr(\'rel\',\'wysiwyg,\'+rel[1]);
											$(self).html(\'<img src="application/modules/Administrator/layouts/scripts/images/common/editors.gif" border="0" title="'.$translator->translator('common_editor_open').'" alt="'.$translator->translator('common_editor_open').'" /></a>\');
											openCodeMirror(rel[1]);
											break;
									}
								});
							});
						</script>';
		}
		
		public function getEditor()
		{
			echo $this->_editor;
		}    
}