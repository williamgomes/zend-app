<?php
class Articles_Form_PreferencesForm extends Zend_Form {
	protected $_editor;
	public function __construct($options = null)

	{
		$translator = Zend_Registry::get ( 'translator' );
		$config = (file_exists ( APPLICATION_PATH . '/modules/Articles/forms/source/' . $translator->getLangFile () . '.PreferencesForm.ini' )) ? new Zend_Config_Ini ( APPLICATION_PATH . '/modules/Articles/forms/source/' . $translator->getLangFile () . '.PreferencesForm.ini', 'preferences' ) : new Zend_Config_Ini ( APPLICATION_PATH . '/modules/Articles/forms/source/en_US.PreferencesForm.ini', 'preferences' );
		parent::__construct ( $config->preferences );
	}
	public function init()

	{
		$this->createForm ();
	}
	public function createForm() {
		$this->elementDecorator ();	
		$this->file_type->setRegisterInArrayValidator(false);
		$this->file_type->setIsArray(true); 
	}

	// Element Decorator
	private function elementDecorator()

	{
		$this->setElementDecorators ( array (
				'ViewHelper',
				'FormElements'
		) );
	}
	public function loadEmailTemplate($element_array) {
		$templates = new Newsletter_Model_DbTable_Templates ();
		$templates_options = $templates->getTemplatesData ();
		if ($element_array) {
			foreach ( $element_array as $element ) {
				$element->setRegisterInArrayValidator ( false );
				$element->addMultiOptions ( $templates_options );
			}
		}
	}
	private function setDB()

	{
		$elements = $this->getElements ();

		$setting_db = new Settings_Model_DbTable_Setting ();

		$setting_db->setDatabaseForm ( $elements );
	}
	public function loadPriceMargine()

	{
		$price = new Settings_Model_DbTable_Price ();

		$price_options = $price->getSelectOptions ();

		$this->price_margine->setRegisterInArrayValidator ( false );

		foreach ( $price_options as $key => $value )

		{

			$this->price_margine->addMultiOption ( $key, $value );
		}
	}
	private function loadReviewGroup() {
		$reviewGroup = new Review_Model_DbTable_Setting ();
		$reviewGroup_options = $reviewGroup->getAllReviews ();

	}
	public function loadCountries()

	{
		$global_conf = Zend_Registry::get ( 'global_conf' );

		$countries = new Eicra_Model_DbTable_Country ();

		$countries_options = $countries->getOptions ();

		$selected = $global_conf ['default_country'];

		foreach ( $countries_options as $key => $value )

		{

			if ($selected == $key)

			{

				$this->default_country->addMultiOption ( $key, $value );

				$this->default_country->setValue ( $selected );
			}

			else

			{

				$this->default_country->addMultiOption ( $key, $value );
			}
		}
	}
	public function loadLocales()

	{
		$global_conf = Zend_Registry::get ( 'global_conf' );

		$locals = new Zend_Locale ();

		$locales_options = $locals->getLocaleList ();

		$selected = $global_conf ['default_locale'];

		// $currency = new Zend_Currency();

		foreach ( $locales_options as $key => $value )

		{

			$locals->setLocale ( $key );

			$region = $locals->getRegion ();

			if (! empty ( $region ))

			{

				// $currency->setLocale($key);

				// $currency_view = ($currency->getName()) ? $currency->getSymbol().' ('.$currency->getShortName().')'.' ---> '.$currency->getName() : $currency->getSymbol().' ('.$currency->getShortName().')';

				if ($selected == $key)

				{

					$this->default_locale->addMultiOption ( $key, $key . ' ---> ' . $region );

					// $this->default_locale->addMultiOption($key,$key.' -- '.$currency_view);

					$this->default_locale->setValue ( $selected );
				}

				else

				{

					$this->default_locale->addMultiOption ( $key, $key . ' ---> ' . $region );

					// $this->default_locale->addMultiOption($key,$key.' -- '.$currency_view);
				}
			}
		}
	}
	
	public function addDisplayGroups(array $groups_arr) {
		foreach ( $groups_arr as $groups ) {

			foreach ( $groups as $key => $spec ) {
				$name = null;
				if (! is_numeric ( $key )) {
					$name = $key;
				}

				if ($spec instanceof Zend_Form_DisplayGroup) {
					$this->_addDisplayGroupObject ( $spec );
				}

				if (! is_array ( $spec ) || empty ( $spec )) {
					continue;
				}

				$argc = count ( $spec );
				$options = array ();

				if (isset ( $spec ['elements'] )) {
					$elements = $spec ['elements'];
					if (isset ( $spec ['name'] )) {
						$name = $spec ['name'];
					}
					if (isset ( $spec ['options'] )) {
						$options = $spec ['options'];
					}
					$this->addDisplayGroup ( $elements, $name, $options );
				} else {
					switch ($argc) {
						case (1 <= $argc) :
							$elements = array_shift ( $spec );
							if (! is_array ( $elements ) && (null !== $name)) {
								$elements = array_merge ( ( array ) $elements, $spec );
								$this->addDisplayGroup ( $elements, $name );
								break;
							}
						case (2 <= $argc) :
							if (null !== $name) {
								$options = array_shift ( $spec );
								$this->addDisplayGroup ( $elements, $name, $options );
								break;
							}
							$name = array_shift ( $spec );
						case (3 <= $argc) :
							$options = array_shift ( $spec );
						default :
							$this->addDisplayGroup ( $elements, $name, $options );
					}
				}
			}
		}
		return $this;
	}
}