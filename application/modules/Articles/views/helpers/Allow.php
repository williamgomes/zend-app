<?php
//Permission Helper Class
class Articles_View_Helper_Allow extends Zend_View_Helper_Abstract 
{
	public function allow($action = null, $controller = null, $module = null, $callback = null, $params = null) 
	{
		return Eicra_Service_RuleChecker::isAllowed($action, $controller, $module, $callback, $params);
	}	
}