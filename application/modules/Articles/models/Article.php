<?php
class Articles_Model_Article 
{
	protected $_article_id;
	protected $_entry_by;
	protected $_article_name;
	protected $_article_title;
	protected $_article_content;
	protected $_article_meta_title;
	protected $_article_meta_key;
	protected $_article_meta_desc;
	protected $_article_status;
	protected $_article_date;
	protected $_order;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveArticle()
		{
			$mapper  = new Articles_Model_ArticleMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setArticle_id($text)
		{
			$this->_article_id = $text;
			return $this;
		}
		
		public function setArticle_name($text)
		{
			$this->_article_name = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setArticle_title($text,$id = null)
		{
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			$pattern = Eicra_File_Constants::TITLE_PATTERN;
			$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
			$text = preg_replace($pattern, $replacement, trim($text));
			if(empty($id))
			{		
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'articles'), array('m.article_id'))
							->where('m.article_title = ?',$text);
				
			}
			else
			{
				$select = $conn->select()
							->from(array('m' => Zend_Registry::get('dbPrefix').'articles'), array('m.article_id'))
							->where('m.article_title = ?',$text)->where('m.article_id != ?',$id);
			}
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$ids = (int)$row['article_id'];
				}
			}
			if(empty($ids))
			{
				$this->_article_title = $text;
			}
			else
			{
				$select = $conn->select()
						->from(array('m' => Zend_Registry::get('dbPrefix').'articles'), array('m.article_id'))
						->order(array('m.article_id DESC'))->limit(1);
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_id = (int)$row['article_id'];
					}
				}
				if(empty($id))
				{
					$this->_article_title = $text.'-'.($last_id+1);
				}
				else
				{
					$this->_article_title = $text.'-'.$id;
				}
			}
			return $this;
		}
		
		public function setArticle_content($text)
		{
			$this->_article_content = $text;
			return $this;
		}
		
		public function setArticle_meta_title($text)
		{   
			$this->_article_meta_title = $text;      
			return $this;
		}
		
		public function setArticle_meta_key($text)
		{
			$this->_article_meta_key = $text;
			return $this;
		}
		
		
		public function setArticle_meta_desc($text)
		{
			$this->_article_meta_desc = $text;
			return $this;
		}	
		
		public function setArticle_status($text)
		{
			$this->_article_status = $text;
			return $this;
		}
		
		public function setArticle_date($text)
		{
			$this->_article_date = date("Y-m-d h:i:s");
			return $this;
		}
		
		public function setOrder($text,$article_id = null)
		{
			if(!empty($text))
			{
				$this->_order = $text;
			}
			else
			{				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				if(empty($article_id))
				{
					//Get Order
					$select = $conn->select()
									->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.order'))									
									->order(array('a.order DESC'))
									->limit(1);
				}
				else
				{
					//Get Order
					$select = $conn->select()
									->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('a.order'))									
									->where('m.article_id != ?',$article_id)									
									->order(array('a.order DESC'))
									->limit(1);
				}
								
				$rs = $select->query()->fetchAll();
				if($rs)
				{
					foreach($rs as $row)
					{
						$last_order = (int)$row['order'];
					}
				}
				
				$this->_order = $last_order + 1;
			}
			return $this;
		}
		
		public function getOrder($article_id = null)
		{   
			if(empty($this->_order))
			{ 
				$this->setOrder('',$article_id); 
			}    
			return $this->_order;
		}
		
		public function getArticle_id()
		{         
			return $this->_article_id;
		}
		
		public function getEntry_by()
		{   
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}     
			return $this->_entry_by;
		}
		
		public function getArticle_name()
		{         
			return $this->_article_name;
		}
		
		public function getArticle_title()
		{         
			return $this->_article_title;
		}
		
		public function getArticle_content()
		{         
			return $this->_article_content;
		}
			
		
		public function getArticle_meta_title()
		{         
			return $this->_article_meta_title;
		}
		
		public function getArticle_meta_key()
		{         
			return $this->_article_meta_key;
		}
		
		public function getArticle_meta_desc()
		{         
			return $this->_article_meta_desc;
		}
		
		public function getArticle_status()
		{         
			return $this->_article_status;
		}
		
		public function getArticle_date()
		{         
			return $this->_article_date;
		}
}
?>