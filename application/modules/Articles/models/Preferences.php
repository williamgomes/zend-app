<?php

class Articles_Model_Preferences
{
    protected $_preferences;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->_preferences = $options;
            //$this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth Method');
        }
        $this->$method($value);
    }


    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveSetting()
    {
        $mapper  = new Articles_Model_PreferencesMapper();
        $return = $mapper->save($this);
        return $return;
    }

    public function getPreferences()
    {
        return $this->_preferences;
    }

}