<?php
class Articles_Model_ArticleMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Articles_Model_DbTable_ArticleList');
        }
        return $this->_dbTable;
    }
	
	public function save(Articles_Model_Article $obj)
		{
				 
			if ((null === ($article_id = $obj->getArticle_id())) || empty($article_id)) 
			{
				unset($data['article_id']);
				
				$data = array(
					'article_name' 		=> 	$obj->getArticle_name(),				
					'article_title' 	=> 	$obj->getArticle_title(),
					'entry_by' 			=> 	$obj->getEntry_by(),
					'article_content' 	=>	$obj->getArticle_content(),
					'article_meta_title'=>	$obj->getArticle_meta_title(),
					'article_meta_key' 	=>	$obj->getArticle_meta_key(),
					'article_meta_desc'	=>	$obj->getArticle_meta_desc(),
					'article_status' 	=>	$obj->getArticle_status(),
					'article_date' 		=>	$obj->getArticle_date(),
					'order'				=>	$obj->getOrder()				
				);			
				
				$id = $this->getDbTable()->insert($data);
						
				if($id)
				{
					$return = $id;
				}
				else
				{
					$return = false;
				}			
			} 
			else 
			{
				$data = array(	
					'article_name' 		=> 	$obj->getArticle_name(),				
					'article_title' 	=> 	$obj->getArticle_title(),
					'article_content' 	=>	$obj->getArticle_content(),
					'article_meta_title'=>	$obj->getArticle_meta_title(),
					'article_meta_key' 	=>	$obj->getArticle_meta_key(),
					'article_meta_desc'	=>	$obj->getArticle_meta_desc()				
				);
				try 
				{			
					$this->getDbTable()->update($data, array('article_id = ?' => $article_id));
					$return = true;
				} 
				catch (Exception $e) 
				{
					$return = false;
				}
			}
			return $return;
		}
   	
	

}
?>