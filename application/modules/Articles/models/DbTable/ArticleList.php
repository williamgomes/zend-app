<?php
/**
* This is the DbTable class for the articles table.
*/
class Articles_Model_DbTable_ArticleList extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'articles';		
	protected $_cols	=	null;
	
	//Get Datas
	public function getArticleInfo($article_id) 
    {
        $article_id = (int)$article_id;
        $row = $this->fetchRow('article_id = ' . $article_id);
        if (!$row) {
           // throw new Exception("Count not find row $article_id");
		   $data = null;
        }
		else
		{
			$data = $row->toArray();
			$data = is_array($data) ? array_map('stripslashes', $data) : stripslashes($data);
		}
        return $data;    
    }
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('a' => $this->_name),array('a.*'))
						->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'a.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => "CONCAT(up.title, ' ', up.firstName, ' ', up.lastName)")); 
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1')
		{
			$select->where('a.entry_by = ?', $user_id);
		}
		
		if($approve != null)
		{
			$select->where("a.article_status = ?", $approve);
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);	
					}
				}
			}
			else
			{
				$select->order("a.article_id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{						
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("a.article_id ASC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options; 
	}
	
	public function getArticlesInfo($draft_page = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		if($draft_page == null)
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('a' => $this->_name),array('a.*'))
							   ->where('a.entry_by = ?',$user_id)
							   ->order('a.article_id ASC'); 
			}
			else
			{
				$select = $this->select()
							   ->from(array('a' => $this->_name),array('a.*'))
							   ->order('a.article_id ASC'); 
			}
		}	
		else
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('a' => $this->_name),array('a.*'))
							   ->where('a.entry_by = ?',$user_id)
							   ->where("a.article_status = ?",$draft_page)
							   ->order('a.article_id ASC'); 
			}
			else
			{
				$select = $this->select()
							   ->from(array('a' => $this->_name),array('a.*'))
							   ->where("a.article_status = ?",$draft_page)
							   ->order('a.article_id ASC'); 
			}
		}	
		
		
        $options = $this->fetchAll($select);
        if (!$options) 
		{
			$options = null;            
        }
        return $options;    
    }
	
	public function getFirstArticle($draft_page = 1) 
    {
		try
		{
			$select = $this->select()
								   ->from(array('a' => $this->_name),array('a.*'))
								   ->where("a.article_status = ?",$draft_page)
								   ->order('a.article_id ASC')
								   ->limit(1); 
			$options = $this->fetchAll($select);
			if (!$options) 
			{
				$options = null;            
			}
			else
			{
				$options = $options[0];	
			}
		}
		catch(Exception $e)
		{
			$options = null;
		}
        return $options;
	}
	
	public function saveArticle($article_id,$field_name,$field_falue) 
    {
		try
		{
			$data = array($field_name => $field_falue);			
			$where	=	$this->getAdapter()->quoteInto('article_id	=	?',$article_id);
			$this->update($data,$where);
			$json_arr = array('status' =>'ok');
		}
		catch(Exception $e)
		{
			$json_arr = array('status' =>'err','msg' => $e->getMesssage());
		}
		return $json_arr;	
	}
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'a.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(m.article_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(m.article_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(m.article_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}	
}

?>