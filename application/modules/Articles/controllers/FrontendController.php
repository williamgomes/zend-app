<?php

//frontend class controller
class Articles_FrontendController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_controllerCache;
	
    public function init()
    {
        /* Initialize action controller here */
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
		
    }
	
	public function preDispatch() 
	{	
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();	
		$front_template = Zend_Registry::get('front_template');		
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
		$this->view->front_template = $front_template;
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);	
		$this->view->setEscape('stripslashes');
		
		$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
		$page_id_arr = $viewHelper->_getContentId();
		
		$this->_page_id = implode(',',$page_id_arr);
	}
	
	public function viewAction()
    {			
			
			$page_id = $this->_page_id;	
			
			$page_id_arr = explode(',',$page_id);
			//Create Where Clause
			$where = array();
			$article_content = array();
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url());
			if( (($article_content = $this->_controllerCache->load($uniq_id)) === false)) 
			{				
				foreach($page_id_arr as $key=>$value)
				{	
					$selectArticles = $this->_DBconn->select()
								->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('article_content','article_meta_title','article_title','article_name','article_meta_key','article_meta_desc'))
								->where('a.article_id = ?', $value)
								->where('a.article_status = ?', '1')
								->limit(1);
					$rsA = $selectArticles->query()->fetchAll();
					if ($rsA) 
					{
						foreach ($rsA as $row) 
						{
							$headTitle = ($row['article_meta_title']) ? $row['article_title'].' - '.$row['article_meta_title'] : $row['article_title'];
							$article_content[] = array('article_id' => $value, 'article_content' => $row['article_content'],'article_name' => $row['article_name'],'article_title' => $row['article_title'], 'article_meta_key' => $row['article_meta_key'],'article_meta_desc' => $row['article_meta_desc'],'article_meta_title' => $row['article_meta_title'], 'head_title' => $headTitle);
						}				
					}
				}
				$this->_controllerCache->save($article_content, $uniq_id);
			}	
			$pageNumber = $this->getRequest()->getParam('page');		
			$viewPageNum = 1; 
			if(empty($article_content)){ $article_content = array(); }
			$paginator = Zend_Paginator::factory($article_content);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);		
			$this->view->view_datas = $paginator;
	}
	
	public function editAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
				
			$article_content = $this->_request->getPost('article_content');
			$article_id = $this->_request->getPost('article_id');
			
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$article_db = new Articles_Model_DbTable_ArticleList();					
				$perm = new Articles_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $article_db->saveArticle($article_id,'article_content',$article_content);
					if($result['status']	==	'ok')
					{
						$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						$msg = $translator->translator("article_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("article_save_err").' '.$result['msg'];
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $translator->translator("article_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}	
			}
			else
			{
				$msg = $translator->translator("common_login_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}		
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}	
		else
		{
			$module_name = $this->_request->getModuleName();	
			$controller_name = strtolower($this->_request->getControllerName());
			$action_name = strtolower($this->_request->getActionName());
					
			$page_id = $this->_page_id;	
			
			$page_id_arr = explode(',',$page_id);
			//Create Where Clause
			$where = array();
			$article_content = array();
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url());
			if( (($article_content = $this->_controllerCache->load($uniq_id)) === false)) 
			{				
				foreach($page_id_arr as $key=>$value)
				{	
					$selectArticles = $this->_DBconn->select()
								->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('article_content','article_meta_title','article_title','article_name','article_meta_key','article_meta_desc'))
								->where('a.article_id = ?', $value)
								->where('a.article_status = ?', '1')
								->limit(1);
					$rsA = $selectArticles->query()->fetchAll();
					if ($rsA) 
					{
						foreach ($rsA as $row) 
						{
							$headTitle = ($row['article_meta_title']) ? $row['article_title'].' - '.$row['article_meta_title'] : $row['article_title'];
							$article_content[] = array('article_id' => $value, 'article_content' => $row['article_content'],'article_name' => $row['article_name'],'article_title' => $row['article_title'], 'article_meta_key' => $row['article_meta_key'],'article_meta_desc' => $row['article_meta_desc'],'article_meta_title' => $row['article_meta_title'], 'head_title' => $headTitle);
						}				
					}
				}
				$this->_controllerCache->save($article_content, $uniq_id);
			}	
			$pageNumber = $this->getRequest()->getParam('page');		
			$viewPageNum = 1; 
			if(empty($article_content)){ $article_content = array(); }
			$paginator = Zend_Paginator::factory($article_content);
			$paginator->setItemCountPerPage($viewPageNum);
			$paginator->setCurrentPageNumber($pageNumber);		
			$this->view->view_datas = $paginator;		
		}
	}
}
