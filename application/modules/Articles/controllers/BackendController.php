<?php

class Articles_BackendController extends Zend_Controller_Action
{
	private $articleForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {			
        /* Initialize action controller here */	
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
		
		$this->articleForm =  new Articles_Form_ArticleForm ();
    }
	
	public function preDispatch() 
	{
		$this->view->setEscape('stripslashes');
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);		
	}

    public function listAction()
    {			
		$link_menu = $this->_request->getParam('link');	
		$draft_page = $this->_request->getParam('draft');
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$global_conf = 	Zend_Registry::get('global_conf');
		$this->view->assign('global_conf', $global_conf);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $draft_page;				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'draft' => $draft_page, 'link'	=> $link_menu, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$articles = new Articles_Model_ArticleListMapper();			
					$articles_datas =  $articles->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas['data_result'] = array();	
					$view_datas['total']	=	0;			
					if($view_datas)
					{							
						$menu_db = new Menu_Model_DbTable_Menu();
						$menu_info = $menu_db->fetchAll();
						$menu_assign_db = new Menu_Model_DbTable_MenuAssign();
						$menu_assign_info = $menu_assign_db->fetchAll();					
						$key = 0;				
						foreach($articles_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_article_name'] = str_replace('_', '-', $entry_arr['product_name']);	
								$entry_arr['article_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['article_date']));
								$entry_arr['link_menu']=  $link_menu;
								
								$linked_menus = ($global_conf['memory_management'] == '3') ? Menu_View_Helper_SubMenu::getSelectedMenuList($entry_arr['article_id'], $menu_info, $menu_assign_info) : null;
								$linked_menu_arr = array();
								if($linked_menus)
								{
									foreach($linked_menus as $linke_key => $linked_arr)
									{
										$linked_menu_arr[$linke_key]  = $this->view->escape($linked_arr['menu_name']).' ('.$linked_arr['group_id'].')';
									}
								}
								$entry_arr['linked_menu'] = implode('&nbsp;&nbsp; ||  &nbsp;&nbsp;', $linked_menu_arr);
								
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$articles_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}			
    }

	public function addAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->articleForm->isValid($this->_request->getPost())) 
			{	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$auth = Zend_Auth::getInstance ();
				$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';
				$articles = new Articles_Model_Article($this->articleForm->getValues());				
				$articles->setArticle_status($stat);
				$articles->setArticle_date('');
				
				$perm = new Articles_View_Helper_Allow();
				if($perm->allow())
				{
					$last_id = $articles->saveArticle();
					if($last_id)
					{
						$msg = $translator->translator("article_save_success");							
						$menu_list_arr = $this->_request->getPost('menu_list');
						if(is_array($menu_list_arr) && !empty($menu_list_arr[0]))
						{
							$datas = array('module_name' => $this->_request->getModuleName(), 'controller_name' => 'frontend', 'action_name' => 'view', 'page_id' => $last_id);
							$menu_model = new Menu_Model_Menus();
							$menu_model->onlyEdit($menu_list_arr, $datas);
						}
						$json_arr = array('status' => 'ok','msg' => $msg,'last_id' => $last_id);
					}
					else
					{
						$msg = $translator->translator("article_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $translator->translator("article_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->articleForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			$this->view->popup = $this->_request->getParam('popup');
			$this->view->articleForms = $this->articleForm;	
			$this->render();
		}	
	}
	
	public function editAction()
	{
		$article_id = $this->_getParam('article_id', 0);
		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->articleForm->isValid($this->_request->getPost())) 
			{	
				$articles = new Articles_Model_Article($this->articleForm->getValues());
				$article_id = $this->_request->getPost('article_id');
				$articles->setArticle_id($article_id);
				$articles->setArticle_title($this->articleForm->getValue('article_title'),$article_id);		
				$perm = new Articles_View_Helper_Allow();
				if($perm->allow())
				{
					if($articles->saveArticle())
					{
						$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						$msg = $translator->translator("article_save_success");
						$menu_list_arr = $this->_request->getPost('menu_list');
						if(is_array($menu_list_arr) && !empty($menu_list_arr[0]))
						{
							$datas = array('module_name' => $this->_request->getModuleName(), 'controller_name' => 'frontend', 'action_name' => 'view', 'page_id' => $article_id);
							$menu_model = new Menu_Model_Menus();
							$menu_model->onlyEdit($menu_list_arr, $datas);
						}
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("article_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $translator->translator("article_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}
			
			}
			else
			{
				$validatorMsg = $this->articleForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$preferences_db = new Articles_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$this->view->preferences_data = $preferences_data;
			
			$articleData = new Articles_Model_DbTable_ArticleList();
			$article_info = $articleData->getArticleInfo($article_id);	
			if($article_info)
			{			
				$this->articleForm->populate($article_info);	
				$this->view->article_id = $article_id;
				$this->view->entry_by = $article_info['entry_by'];
				$this->view->articleForms = $this->articleForm;	
				$this->render();
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}	
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				
			$article_id = $this->_request->getPost('article_id');
			$article_title = $this->_request->getPost('article_name');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Remove from Articles
			$where = array();
			$where[] = 'article_id = '.$conn->quote($article_id);
			$conn->delete(Zend_Registry::get('dbPrefix').'articles', $where);
			
			$msg = 	$translator->translator('articles_list_delete_success',$article_title);			
			$json_arr = array('status' => 'ok','msg' => $msg);
		}
		else
		{
		
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			$article_id_str = $this->_request->getPost('id_st');
			$perm = new Menu_View_Helper_Allow();			
			if ($perm->allow('delete','backend','Articles'))
			{
				if(!empty($article_id_str))
				{	
					$article_id_arr = explode(',',$article_id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					$tmp = 'ok';
					foreach($article_id_arr as $article_id)
					{
						// Remove from Articles
						$where = array();
						$where[] = 'article_id = '.$conn->quote($article_id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'articles', $where);
						}
						catch (Exception $e) 
						{
							$tmp = 'err';
						}						
					}
					
					if($tmp == 'ok')
					{
						$msg = 	$translator->translator('articles_list_delete_success');			
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("article_selected_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator("article_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('article_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('article_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function publishAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$article_id = $this->_request->getPost('article_id');
			$article_title = $this->_request->getPost('article_name');
			$paction = $this->_request->getPost('paction');
			
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			switch($paction)
			{
				case 'publish':
					$article_status = '1';
					break;
				case 'unpublish':
					$article_status = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Article status
			$where = array();
			$where[] = 'article_id = '.$conn->quote($article_id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'articles',array('article_status' => $article_status), $where);
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','active' => $article_status);
			}
			else
			{
				$msg = $translator->translator('articles_list_publish_err',$article_title);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
			
	}
	
	public function publishallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$article_id_str = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');	
			switch($paction)
			{
				case 'publish':
					$article_status = '1';
					break;
				case 'unpublish':
					$article_status = '0';
					break;
			}
			
				
			if(!empty($article_id_str))	
			{	
				$article_id_arr = explode(',',$article_id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($article_id_arr as $article_id)
				{
					// Update Article status
					$where = array();
					$where[] = 'article_id = '.$conn->quote($article_id);								
					try
					{	
						$conn->update(Zend_Registry::get('dbPrefix').'articles',array('article_status' => $article_status), $where);		
						$json_arr = array('status' => 'ok');
					}
					catch (Exception $e) 
					{
						$msg = $translator->translator('articles_list_publish_err');	
						$json_arr = array('status' => 'err','msg' => $msg." ".$e->getMessage());
					}
				}
			}
			else
			{
				$msg = $translator->translator("article_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
			
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function previewAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		$article_id = $this->_getParam('article_id');
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
				
		// Get resources
		$select = $conn->select()
					->from(array('a' => Zend_Registry::get('dbPrefix').'articles'), array('article_content'))
					->where('a.article_id = ?', $article_id);
		$rs = $select->query()->fetchAll();
		if ($rs) 
		{
			foreach ($rs as $row) 
			{			
				$article_content = stripslashes($row['article_content']);
			}
			$this->_response->setBody('<div>'.$article_content.'</div>');		
		}
		else
		{
			$msg = $translator->translator('articles_list_preview_err');
			$this->_response->setBody($msg);	
		}
	}
	
	public function upAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		
		$article_id = $this->_request->getPost('article_id');
		$order = $this->_request->getPost('order');	
		
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
				
		$articleOrderObj = new Articles_Controller_Helper_Orders($article_id);
		$returnV =	$articleOrderObj->decreaseOrder();
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function downAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();			
		
		$article_id = $this->_request->getPost('article_id');
		$order = $this->_request->getPost('order');
		
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						
		$articleOrderObj = new Articles_Controller_Helper_Orders($article_id);
		$returnV =	$articleOrderObj->increaseOrder();
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function orderallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			$article_id_str = $this->_request->getPost('article_id_arr');
			$order_str = $this->_request->getPost('order_arr');			
			
			$article_id_arr = explode(',',$article_id_str);
			$order_arr = explode(',',$order_str);
			
			$order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
			$checkOrder = $order_numbers->checkOrderNumbers($order_arr);
			if($checkOrder['status'] == 'err')
			{
				$json_arr = array('status' => 'err','msg' => $checkOrder['msg']);
			}
			else
			{
				//Save Menu Order
				$i = 0;
				foreach($article_id_arr as $article_id)
				{
					$articleOrderObj = new Articles_Controller_Helper_Orders($article_id);
					$result = $articleOrderObj->saveOrder($order_arr[$i]);
					if($result['status'] == 'err')
					{
						$json_arr = array('status' => 'err','msg' => $result['msg']);
						break;
					}
					$i++;
				}
				$msg = $translator->translator("article_zero_order_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}			
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
}

