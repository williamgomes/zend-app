<?php
//Members Frontend controller
class Articles_MembersController extends Zend_Controller_Action
{	
	private $_page_id;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	private $articleForm;
	
    public function init()
    {	
		$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
		Eicra_Global_Variable::checkSession($this->_response,$url);
		Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();
		
		/* Initialize Template */
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout(true));
		$this->translator =	Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->translator);	
	
        /* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();			
		
		
		$this->view->menu_id = $this->_request->getParam('menu_id');
		$getAction = $this->_request->getActionName();
		
			
		if($this->_request->getParam('menu_id'))
		{			
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
			
    }
	
	public function preDispatch() 
	{	
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;
		$this->view->setEscape('stripslashes');					
	}
	
	
	public function listAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('article_frontend_list_page_name'), $this->view->url()));
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$global_conf = 	Zend_Registry::get('global_conf');
		$this->view->assign('global_conf', $global_conf);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');	
				$link_menu = $this->_request->getParam('link');				
					
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontendRout = 'Members-Article-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'link'	=> $link_menu, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRout,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{		
					$articles = new Articles_Model_ArticleListMapper();	
					$articles_datas =  $articles->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas['data_result'] = array();
					$view_datas['total']	=	0;				
					if($articles_datas)
					{
						$menu_db = new Menu_Model_DbTable_Menu();
						$menu_info = $menu_db->fetchAll();
						$menu_assign_db = new Menu_Model_DbTable_MenuAssign();
						$menu_assign_info = $menu_assign_db->fetchAll();						
						$key = 0;				
						foreach($articles_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_article_name'] = str_replace('_', '-', $entry_arr['product_name']);	
								$entry_arr['article_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['article_date']));
								$entry_arr['link_menu']=  $link_menu;
								
								$linked_menus = ($global_conf['memory_management'] == '3') ? Menu_View_Helper_SubMenu::getSelectedMenuList($entry_arr['article_id'], $menu_info, $menu_assign_info) : null;
								$linked_menu_arr = array();
								if($linked_menus)
								{
									foreach($linked_menus as $linke_key => $linked_arr)
									{
										$linked_menu_arr[$linke_key]  = $this->view->escape($linked_arr['menu_name']).' ('.$linked_arr['group_id'].')';
									}
								}
								$entry_arr['linked_menu'] = implode('&nbsp;&nbsp; ||  &nbsp;&nbsp;', $linked_menu_arr);
								
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;				
						}
						$view_datas['total']	=	$articles_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	public function addAction()
	{		
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('article_frontend_list_page_name'), 'Members-Article-List'), array($this->translator->translator('article_frontend_list_add_page_name'), $this->view->url()));
		$this->articleForm = new Articles_Form_ArticleForm();
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->articleForm->isValid($this->_request->getPost())) 
			{	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$auth = $this->view->auth;
				$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';
				$articles = new Articles_Model_Article($this->articleForm->getValues());				
				$articles->setArticle_status($stat);
				$articles->setArticle_date('');
				
				$perm = new Articles_View_Helper_Allow();
				if($perm->allow())
				{
					$last_id = $articles->saveArticle();
					if($last_id)
					{
						$msg = $translator->translator("article_save_success");							
						$menu_list_arr = $this->_request->getPost('menu_list');
						if(is_array($menu_list_arr) && !empty($menu_list_arr[0]))
						{
							$datas = array('module_name' => $this->_request->getModuleName(), 'controller_name' => 'frontend', 'action_name' => 'view', 'page_id' => $last_id);
							$menu_model = new Menu_Model_Menus();
							$menu_model->onlyEdit($menu_list_arr, $datas);
						}
						$json_arr = array('status' => 'ok','msg' => $msg,'last_id' => $last_id);
					}
					else
					{
						$msg = $translator->translator("article_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $translator->translator("article_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->articleForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			$this->view->popup = $this->_request->getParam('popup');
			$this->view->articleForms = $this->articleForm;
		}		
	}
	
	public function editAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('article_frontend_list_page_name'), 'Members-Article-List/*'), array($this->translator->translator('article_frontend_list_edit_page_name'), $this->view->url()));
		
		$article_id = $this->_getParam('article_id', 0);
		$this->articleForm = new Articles_Form_ArticleForm();
		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->articleForm->isValid($this->_request->getPost())) 
			{	
				$articles = new Articles_Model_Article($this->articleForm->getValues());
				$article_id = $this->_request->getPost('article_id');
				$articles->setArticle_id($article_id);
				$articles->setArticle_title($this->articleForm->getValue('article_title'),$article_id);		
				
				if($this->view->allow())
				{
					if($articles->saveArticle())
					{
						$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
						$msg = $translator->translator("article_save_success");
						$menu_list_arr = $this->_request->getPost('menu_list');
						if(is_array($menu_list_arr) && !empty($menu_list_arr[0]))
						{
							$datas = array('module_name' => $this->_request->getModuleName(), 'controller_name' => 'frontend', 'action_name' => 'view', 'page_id' => $article_id);
							$menu_model = new Menu_Model_Menus();
							$menu_model->onlyEdit($menu_list_arr, $datas);
						}
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("article_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}		
				}
				else
				{
					$Msg =  $translator->translator("article_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}
			
			}
			else
			{
				$validatorMsg = $this->articleForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{
			$articleData = new Articles_Model_DbTable_ArticleList();
			$article_info = $articleData->getArticleInfo($article_id);	
			if($article_info)
			{			
				$this->articleForm->populate($article_info);	
				$this->view->article_id = $article_id;
				$this->view->article_info = $article_info;
				$this->view->articleForms = $this->articleForm;	
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Article-Add', array());
			}
		}		
    }	
}