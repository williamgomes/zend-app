<?php

class Shipping_Form_SettingsForm extends Zend_Form {

    public function __construct($options = null) {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(APPLICATION_PATH . '/modules/Shipping/forms/source/' . $translator->getLangFile() . '.ShippingSettingsForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/Shipping/forms/source/' . $translator->getLangFile() . '.ShippingSettingsForm.ini', 'shipping_settings') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/Shipping/forms/source/en_US.ShippingSettingsForm.ini', 'shipping_settings');
        parent::__construct($config->shipping);
    }

    public function init() {
        $this->createForm();
    }

    public function createForm() {
        $this->elementDecorator();
//        $this->loadUserSettings();
//        $this->loadDynamicForms();
//        $this->loadReviewGroup();
//        $this->file_type->setRegisterInArrayValidator(false);
//        $this->doSecurityFiltering();
    }

    private function setElementTrueValue($element) {
        $element->setRegisterInArrayValidator(false);
        $options = $element->getMultiOptions();
        $element->clearMultiOptions();
        foreach ($options as $key => $value) {
            $element->addMultiOption($value, $value);
        }
        $element->setRegisterInArrayValidator(false);
    }

    //set Filters
    public function doSecurityFiltering() {
        $filters = array();
        $filter = new Zend_Filter_PregReplace(array('match' => Eicra_File_Constants::FILTER_PATTERN,
            'replace' => Eicra_File_Constants::FILTER_REPLACEMENT));
        $filters[0] = $filter;
        $this->addElementFilters($filters);
    }

    //Add Global Filters
    public function addElementFilters(array $filters) {
        foreach ($this->getElements() as $element) {
            $element->addFilters($filters);
        }
        return $this;
    }

    //Element Decorator
    private function elementDecorator() {
        $this->setElementDecorators(array('ViewHelper', 'FormElements'));
        
//        $this->upload_file->setDecorators(array(
//                        'file','file',										
//
//                ));
        //$this->upload_file->setMaxFileSize($this->_option['file_size_max']);		
    }

    private function loadDynamicForms() {
        $dynamicForm = new Members_Model_DbTable_Forms();
        $dynamicForm_options = $dynamicForm->getAllForms();
        if ($dynamicForm_options) {
            $this->dynamic_form->addMultiOptions($dynamicForm_options);
        }
    }

    private function loadReviewGroup() {
        $reviewGroup = new Review_Model_DbTable_Setting();
        $reviewGroup_options = $reviewGroup->getAllReviews();
        $this->review_id->addMultiOptions($reviewGroup_options);
    }

    private function loadUserGroup() {
        $userGroup = new Members_Model_DbTable_Role();
        $userGroup_options = $userGroup->getOptions();
        $this->role_id->addMultiOptions($userGroup_options);
    }

}
