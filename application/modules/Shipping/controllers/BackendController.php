<?php

class Shipping_BackendController extends Zend_Controller_Action {

    private $shippingSettingsForm;
    private $_controllerCache;
    private $_auth_obj;
    private $translator;

    public function init() {
        /* Initialize action controller here */
        $this->shippingSettingsForm = new Shipping_Form_SettingsForm ();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {

        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');

        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
        Eicra_Global_Variable::checkSession($this->_response, $url);

        /* Check Module License */
        $modules_license = new Administrator_Controller_Helper_ModuleLoader();
        $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
    }

    public function settingsAction() {
        $id = $this->_getParam('id', 0);
        $this->view->id = $id; 
        
        if ($this->_request->isPost()) {

            try {
                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->shippingSettingsForm->isValid($this->_request->getPost())) {

                    $highestRow = 0;
                    $arrayExcelContent = '';
                    
                    if ($this->_request->getPost('upload_file') != '') {
                        $stripeHelper = new Shipping_Controller_Helper_PHPExcel();
                        $excelPath = 'temp/' . $this->_request->getPost('upload_file');
                        $objPHPExcel = PHPExcel_IOFactory::load($excelPath);

                        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
                        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                    }
                    
                    $shippingInfo = new Shipping_Model_ShippingSetting($this->shippingSettingsForm->getValues());
                    $result = $shippingInfo->saveShippingSettings($id);

                    if ($result['status'] == 'ok') {

                        $lastID = $result['id'];
                        $msg = $this->view->translator->translator("page_save_success");
                        
                        try{
                            if ($this->_request->getPost('upload_file') != '') {
                                for ($row = 2; $row <= $highestRow; ++$row) {

                                    $arrayExcelContent[$row]['ID'] = $objWorksheet->getCell('A' . $row)->getValue();
                                    $arrayExcelContent[$row]['Config_Id'] = $lastID;
                                    $arrayExcelContent[$row]['User'] = $this->_auth_obj->user_id;
                                    $arrayExcelContent[$row]['Country'] = $objWorksheet->getCell('B' . $row)->getValue();
                                    $arrayExcelContent[$row]['State'] = $objWorksheet->getCell('C' . $row)->getValue();
                                    $arrayExcelContent[$row]['Zip'] = $objWorksheet->getCell('D' . $row)->getValue();
                                    $arrayExcelContent[$row]['Range'] = $objWorksheet->getCell('E' . $row)->getValue();
                                    $arrayExcelContent[$row]['Rate'] = $objWorksheet->getCell('F' . $row)->getValue();
                                }
                                try{
                                    $shippingRate = new Shipping_Model_ShippingRate();
                                    $finalResult = $shippingRate->saveShippingRates(array("rates" => $arrayExcelContent));
                                } catch (Exception $e){
                                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                                }
                                unlink($excelPath);
                                if ($finalResult['status'] == 'ok') {
                                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                                } else {
                                    $json_arr = array('status' => 'err', 'msg' => $finalResult['msg']);
                                }
                            }
                        } catch (Exception $e) {
                            $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                        }
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->shippingSettingsForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            if($id > 0){
                $settingObj = new Shipping_Model_DbTable_ShippingSettings();
                $settingsInfo = $settingObj->fetchRow('id=' . $id);
                $settingsInfo = (!is_array($settingsInfo)) ? $settingsInfo->toArray() : $settingsInfo;
                $this->shippingSettingsForm->populate($settingsInfo);
            } else {
                $user_id = $this->_auth_obj->user_id;
                $this->shippingSettingsForm->user_id->setValue($user_id);
            }
        }
   
        $this->view->settingsForm = $this->shippingSettingsForm;
    }

    public function settingslistAction() {
        $user_id = $this->_auth_obj->user_id;
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');



                $getViewPageNum = $this->_request->getParam('pageSize');
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $settingsListMap = new Shipping_Model_ShippingSettingMapper();
                    $list_datas = $settingsListMap->settingsList($user_id, $pageNumber, $posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['Default_Rate'] = $this->view->numbers($entry_arr['default_rate']);
                            $entry_arr['updated_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['updated'])));
                            $entry_arr['updated_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['updated'])));
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total']	=	$list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function deleteAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

            $perm = new Shipping_View_Helper_Allow();
            if ($perm->allow('delete', 'backend', 'Shipping')) {
                $id = $this->_request->getPost('id');

                //DB Connection
                $conn = Zend_Registry::get('msqli_connection');
                $conn->getConnection();

                // Remove from Autos
                try {
                    $where1 = array();
                    $where1[] = 'shipping_config_id = ' . $conn->quote($id);
                    $conn->delete(Zend_Registry::get('dbPrefix') . 'shipping_rate', $where1);
                    
                    $where2 = array();
                    $where2[] = 'id = ' . $conn->quote($id);
                    $conn->delete(Zend_Registry::get('dbPrefix') . 'shipping_config', $where2);

                    $msg = $translator->translator('page_list_delete_success', $file);
                    $json_arr = array('status' => 'ok', 'msg' => $msg);
                } catch (Exception $e) {
                    $msg = $translator->translator('page_list_delete_err', $file);
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('page_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $perm = new Shipping_View_Helper_Allow();
            if ($perm->allow('delete', 'backend', 'Shipping')) {
                if (!empty($id_str)) {
                    $id_arr = explode(',', $id_str);
                    //DB Connection
                    $conn = Zend_Registry::get('msqli_connection');
                    $conn->getConnection();

                    //Delete Files From Folder
                    $form_db = new Members_Model_DbTable_Forms();
                    $fields_db = new Members_Model_DbTable_Fields();
                    $field_value_db = new Members_Model_DbTable_FieldsValue();

                    foreach ($id_arr as $id) {
					
                        try {
                            
                            $where1 = array();
                            $where1[] = 'shipping_config_id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'shipping_rate', $where1);
                            

                            $where2 = array();
                            $where2[] = 'id = ' . $conn->quote($id);
                            $conn->delete(Zend_Registry::get('dbPrefix') . 'shipping_config', $where2);
                            
                            $msg = $translator->translator('page_list_file_delete_success', $file);
                            $json_arr = array('status' => 'ok', 'msg' => $msg);

                        } catch (Exception $e) {
                            $msg = $translator->translator('category_list_delete_err');
                            $json_arr = array('status' => 'err', 'msg' => $msg);
                        }
                    }
                } else {
                    $msg = $translator->translator("category_selected_err");
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator('page_delete_perm');
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        } else {
            $msg = $translator->translator('category_list_delete_err');
            $json_arr = array('status' => 'err', 'msg' => $msg);
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id = $this->_request->getPost('id');
            $paction = $this->_request->getPost('paction');
            $data = array();

            $shippingSettingsDB = new Shipping_Model_DbTable_ShippingSettings();
            $settingsInfo = $shippingSettingsDB->fetchRow('id=' . $id);

            switch ($paction) {
                case 'publish':
                    $active = '1';
                    $letter_type = 'activation';
                    $data = array('status' => $active);
                    break;
                case 'unpublish':
                    $active = '0';
                    $letter_type = 'deactivation';
                    $data = array('status' => $active);
                    break;
            }

            //DB Connection
            $conn = Zend_Registry::get('msqli_connection');
            $conn->getConnection();

            // Update Autos status
            $where = array();
            $where[] = 'id = ' . $conn->quote($id);
            try {
                $conn->update(Zend_Registry::get('dbPrefix') . 'shipping_config', $data, $where);
                $return = true;
            } catch (Exception $e) {
                $return = false;
            }
            if ($return) {
                $json_arr = array('status' => 'ok', 'active' => $active);
            } else {
                $msg = $translator->translator('page_list_publish_err', $autos_model_name);
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function publishallAction() {
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $id_str = $this->_request->getPost('id_st');
            $paction = $this->_request->getPost('paction');

            switch ($paction) {
                case 'publish':
                    $active = '1';
                    break;
                case 'unpublish':
                    $active = '0';
                    break;
            }

            if (!empty($id_str)) {
                $id_arr = explode(',', $id_str);

                //DB Connection
                $conn = Zend_Registry::get('msqli_connection');
                $conn->getConnection();

                foreach ($id_arr as $id) {
                    // Update Article status
                    $where = array();
                    $where[] = 'id = ' . $conn->quote($id);
                    try {
                        $conn->update(Zend_Registry::get('dbPrefix') . 'shipping_config', array('status' => $active), $where);
                        $return = true;
                    } catch (Exception $e) {
                        $return = false;
                    }
                }

                if ($return) {
                    $json_arr = array('status' => 'ok');
                } else {
                    $msg = $translator->translator('page_list_publish_err');
                    $json_arr = array('status' => 'err', 'msg' => $msg);
                }
            } else {
                $msg = $translator->translator("page_selected_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
        }

        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

}
