<?php

class Shipping_Controller_Helper_PHPExcel {

    
    private $_translator;

    public function __construct() {
        $this->_translator = Zend_Registry::get('translator');
        $this->getAllFiles();
        
    }

    public function getAllFiles() {
        
        $fullPath = APPLICATION_PATH . '/modules/Shipping/lib/PHPExcel/';
        // This snippet (and some of the curl code) due to the Facebook SDK.
       
        //This library is required for excel export
        require_once $fullPath . 'PHPExcel.php';
        
        //This library is required for excel import
        require_once $fullPath . 'PHPExcel/IOFactory.php';
        
    }

}

?>