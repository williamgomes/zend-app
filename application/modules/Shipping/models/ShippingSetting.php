<?php

class Shipping_Model_ShippingSetting {

    protected $_id;
    protected $_shipping_name;
    protected $_user_id;
    protected $_param_condition;
    protected $_handling_fee_type;
    protected $_handling_fee_rate;
    protected $_default_rate;
    protected $_updated;
    protected $_status;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    
    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setShipping_name($text) {
        $this->_shipping_name = addslashes($text);
        return $this;
    }

    public function setUser_id($text) {
        $this->_user_id = $text;
        return $this;
    }
    
    public function setParam_condition($text) {
        $this->_param_condition = $text;
        return $this;
    }
    
    public function setHandling_fee_type($text) {
        $this->_handling_fee_type = $text;
        return $this;
    }
    
    public function setHandling_fee_rate($text) {
        $this->_handling_fee_rate = addslashes($text);
        return $this;
    }
    
    public function setDefault_rate($text) {
        $this->_default_rate = addslashes($text);
        return $this;
    }
    
    public function setUpdated($text) {
        $this->_updated = $text;
        return $this;
    }
    
    public function setStatus() {
        $this->_status = '1';
        return $this;
    }

    

    public function getId() {
        return $this->_id;
    }

    public function getShipping_name() {
        return $this->_shipping_name;
    }

    public function getUser_id() {
        return $this->_user_id;
    }
    
    public function getParam_condition() {
        return $this->_param_condition;
    }
    
    public function getHandling_fee_type() {
        return $this->_handling_fee_type;
    }
    
    public function getHandling_fee_rate() {
        return $this->_handling_fee_rate;
    }
    
    public function getDefault_rate() {
        return $this->_default_rate;
    }
    
    public function getUpdated() {
        return $this->_updated;
    }
    
    public function getStatus() {
        if (empty($this->_entry_by)) {
            $this->setStatus();
        }
        return $this->_status;
    }


    
    public function saveShippingSettings($id = 0) {
        $mapper = new Shipping_Model_ShippingSettingMapper();
        $return = $mapper->save($this, $id);
        return $return;
    }
}

?>