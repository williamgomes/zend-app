<?php

/**
 * This is the DbTable class for the b2b_business_type table.
 */
class Shipping_Model_DbTable_ShippingSettings extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'shipping_config';
    protected $_cols = null;
    
    
    public function getAllData($userID, $pageNumber, $search_params = null) {


        try {
            $select = $this->select()
                    ->from(array('sconf' => $this->_name))
                    ->where('sconf.user_id =?', $userID);


            if ($search_params != null) {
                if ($search_params['sort']) {
                    foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                        if ($sort_value_arr['dir'] == 'exp') {
                            $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                        } else {
                            $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                        }
                    }
                } else {
                    $select->order("sconf.id DESC");
                }

                if ($search_params['filter'] && $search_params['filter']['filters']) {
                    $search_services = new Search_Service_Services($this);
                    $where = $search_services->getSearchWhereClause($search_params);
                    if (!empty($where)) {
                        $select->where($where);
                    }
                }
            } else {
                $select->order('sconf.id DESC');
            }
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'sconf.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        
         $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'updated':                
                $data_arr = preg_split('/[- :]/', $operator_arr['value']);
                if ($data_arr[0]) {
                    $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                    $operator_arr['value'] = date("Y-m-d H:i:s", $time);
                }
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(sconf.updated)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(sconf.updated, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(sconf.updated, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>