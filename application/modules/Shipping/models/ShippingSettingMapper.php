<?php

class Shipping_Model_ShippingSettingMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {

        if (is_string($dbTable)) {

            $dbTable = new $dbTable();
        }

        if (!$dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;

        return $this;
    }

    public function getDbTable() {

        if (null === $this->_dbTable) {

            $this->setDbTable('Shipping_Model_DbTable_ShippingSettings');
        }

        return $this->_dbTable;
    }

    public function save(Shipping_Model_ShippingSetting $obj, $id) {
        if (null === $id || empty($id)) {
            unset($data['id']);
            $data = array(
                'shipping_name' => $obj->getShipping_name(),
                'user_id' => $obj->getUser_id(),
                'param_condition' => $obj->getParam_condition(),
                'handling_fee_type' => $obj->getHandling_fee_type(),
                'handling_fee_rate' => $obj->getHandling_fee_rate(),
                'default_rate' => $obj->getDefault_rate(),
                'status' => $obj->getStatus()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            $data = array(
                'shipping_name' => $obj->getShipping_name(),
                'user_id' => $obj->getUser_id(),
                'param_condition' => $obj->getParam_condition(),
                'handling_fee_type' => $obj->getHandling_fee_type(),
                'handling_fee_rate' => $obj->getHandling_fee_rate(),
                'default_rate' => $obj->getDefault_rate(),
                'status' => $obj->getStatus()
            );
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

    public function settingsList($userID, $pageNumber, $search_params = null) {
        $resultSet = $this->getDbTable()->getAllData($userID, $pageNumber, $search_params);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }

}

?>