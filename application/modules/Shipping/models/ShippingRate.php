<?php

class Shipping_Model_ShippingRate {

    protected $_id;
    protected $_shipping_config_id;
    protected $_user_id;
    protected $_country_code;
    protected $_state_code;
    protected $_zip_code;
    protected $_condition_param;
    protected $_rate;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    
    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setShipping_config_id($text) {
        $this->_shipping_config_id = $text;
        return $this;
    }

    public function setUser_id($text) {
        $this->_user_id = $text;
        return $this;
    }
    
    public function setCountry_code($text) {
        $this->_country_code = $text;
        return $this;
    }
    
    public function setState_code($text) {
        $this->_state_code = $text;
        return $this;
    }
    
    public function setZip_code($text) {
        $this->_zip_code = addslashes($text);
        return $this;
    }
    
    public function setCondition_param($text) {
        $this->_condition_param = addslashes($text);
        return $this;
    }
    
    public function setRate($text) {
        $this->_rate = $text;
        return $this;
    }
    

    
    public function getId() {
        return $this->_id;
    }

    public function getShipping_config_id() {
        return $this->_shipping_config_id;
    }

    public function getUser_id() {
        return $this->_user_id;
    }
    
    public function getCountry_code() {
        return $this->_country_code;
    }
    
    public function getState_code() {
        return $this->_state_code;
    }
    
    public function getZip_code() {
        return $this->_zip_code;
    }
    
    public function getCondition_param() {
        return $this->_condition_param;
    }
    
    public function getRate() {
        return $this->_rate;
    }
    

    
    public function saveShippingRates(array $arrayRates = NULL) {
        $mapper = new Shipping_Model_ShippingRateMapper();
        $return = $mapper->save($arrayRates);
        return $return;
    }
}

?>