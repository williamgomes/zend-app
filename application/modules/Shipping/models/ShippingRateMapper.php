<?php

class Shipping_Model_ShippingRateMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {

        if (is_string($dbTable)) {

            $dbTable = new $dbTable();
        }

        if (!$dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;

        return $this;
    }

    public function getDbTable() {

        if (null === $this->_dbTable) {

            $this->setDbTable('Shipping_Model_DbTable_ShippingRate');
        }

        return $this->_dbTable;
    }

    public function save(array $arrayRates = NULL) {
        
        foreach($arrayRates["rates"] AS $rates){
            if($rates['ID'] == null OR $rates['ID'] == 0){
                $data = array(
                    'shipping_config_id' => $rates['Config_Id'],
                    'user_id' => $rates['User'],
                    'country_code' => $rates['Country'],
                    'state_code' => $rates['State'],
                    'zip_code' => $rates['Zip'],
                    'condition_param' => $rates['Range'],
                    'rate' => $rates['Rate']
                );
                try {
                    $last_id = $this->getDbTable()->insert($data);
                    $result = array('status' => 'ok', 'id' => $last_id);
                } catch (Exception $e) {
                    $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
                }
            } else {
                $data = array(
                    'shipping_config_id' => $rates['Config_Id'],
                    'user_id' => $rates['User'],
                    'country_code' => $rates['Country'],
                    'state_code' => $rates['State'],
                    'zip_code' => $rates['Zip'],
                    'condition_param' => $rates['Range'],
                    'rate' => $rates['Rate']
                );
                try {
                    $last_id = $this->getDbTable()->update($data, array('id = ?' => $rates['ID']));
                    $result = array('status' => 'ok', 'id' => $rates['ID']);
                } catch (Exception $e) {
                    $result = array('status' => 'err', 'id' => $rates['ID'], 'msg' => $e->getMessage());
                }
            }
        }
        
        return $result;
    }
    
}

?>