<?php
class Hotels_Controller_Helper_HotelLink
{
	protected $_data;
	protected $_hotel_db;
	protected $_hotel_datas;
	
	public function __construct($data = null)
	{
		
		//DB Connection
		$this->_data = (is_array($data)) ? $data : $data->toArray();		
		$this->_hotel_db 	=   new Hotels_Model_DbTable_Hotels();				
	}
	
	public function isLinkedToHotel($data_arr)
	{		
		$tmp = false;			
		foreach($this->_hotel_datas as $key => $hotel_datas_arr)
		{
			$room_type_arr = explode(',',$hotel_datas_arr['room_type_id']);
			
			if(in_array($data_arr['id'], $room_type_arr))
			{
				$tmp = true;
				break;
			}
		}
		return $tmp;
	}
	
	public function setRoomTypeLinkedToSelectedHotel($hotel_id)
	{
		$hotel_datas = $this->_hotel_db->getHotelsInfo($hotel_id);	
		
		if($hotel_datas)
		{
			$room_type_arr = explode(',', $hotel_datas['room_type_id']);					
			foreach($this->_data as $key => $data_arr)
			{								
				if(!in_array($data_arr['id'], $room_type_arr))
				{
					
					unset($this->_data[$key]);					
				}
			}			
		}
	}
	
	public function getHotelLinkedRoomType($hotel_id = null)
	{
		if(!empty($hotel_id))
		{			
			$this->setRoomTypeLinkedToSelectedHotel($hotel_id);
		}
		else
		{	
			$this->_hotel_datas = $this->_hotel_db->fetchAll();		
			foreach($this->_data as $key => $data_arr)
			{
				if($this->isLinkedToHotel($data_arr))
				{
					unset($this->_data[$key]);
				}
			}			
		}
		return $this->_data;
	}
}
?>