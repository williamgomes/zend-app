<?php
//Members Frontend controller
class Hotels_MembersController extends Zend_Controller_Action
{
	private $_page_id;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	private $ProductForm;
	private $hotelsRoomTypeForm;
	
    public function init()
    {	
		$url = $this->view->url(array('module'=> 'Members', 'controller' => 'frontend', 'action'     => 'login' ), 'Frontend-Login',    true);
		Eicra_Global_Variable::checkSession($this->_response,$url);
		Eicra_Global_Variable::getSession()->returnLink = $this->_request->getRequestUri();
		
		/* Initialize Template */
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout(true));
		$this->translator =	Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->translator);	
	
        /* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();			
		
		
		$this->view->menu_id = $this->_request->getParam('menu_id');
		$getAction = $this->_request->getActionName();
		
			
		if($this->_request->getParam('menu_id'))
		{			
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}
			
    }
	
	public function preDispatch() 
	{	
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->auth = $auth;
		$this->view->setEscape('stripslashes');					
	}
	
	
	public function listAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_list_header_title'), $this->view->url()));
		
		$group_id =	$this->_request->getParam('group');
		$this->view->group_id = $group_id;
		$approve = $this->getRequest()->getParam('approve');	
		$this->view->approve = $approve;
		$category_id = $this->getRequest()->getParam('category');
		$this->view->category_id = $category_id;
		
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		$group_info	=	($group_id) ? $group_db->getGroupName($group_id) : null;
		$this->view->group_info = $group_info;
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				if($group_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $group_id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}
				if($category_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $category_id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
					
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontendRoute = 'Members-Hotel-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group' => $group_id, 'category' => $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontendRoute,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{		
					$list_mapper = new Hotels_Model_HotelsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);								
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_hotels_name'] = str_replace('_', '-', $entry_arr['hotels_name']);
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);								
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Hotels_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Hotels_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Hotels_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());	
		
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('distanceFromAirport_info', $hotels_db->getDistanceFromAirport());
	}
	
	public function addAction()
	{		
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_list_header_title'), 'Members-Hotel-List'), array($this->translator->translator('hotels_frontend_list_add_page_name'), $this->view->url()));
		$this->ProductForm = new Hotels_Form_ProductForm ();
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();			
			
			try
			{
				//Checking Membership Package Option
				$packages = new Members_Controller_Helper_Packages();	
				if($packages->isPackage())
				{
					$module = $this->_request->getModuleName();
					$controller = 'backendpro';
					$action = $this->_request->getActionName();
					if($packages->isPackageField($module,$controller,$action))
					{
						$package_no = $packages->getPackageFieldValue($module,$controller,$action);				
					}
					else
					{
						$package_no = 0;
					}
				}
				else
				{
					$package_no = 0;
				}	
				
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$p_num = $hotels_db->numOfHotels();
				if(empty($package_no) || ($p_num < $package_no))
				{
					$group_id = $this->_request->getPost('group_id');
					if(!empty($group_id))
					{						
						$group_info = new Hotels_Model_DbTable_HotelsGroup();
						$option = $group_info->getGroupName($group_id);
						if($option['dynamic_form'])
						{
							$option['form_id'] = $option['dynamic_form'];
							$this->ProductForm = new Hotels_Form_ProductForm ($option);
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($option['form_id']); 
							foreach($field_groups as $group)
							{						
								$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
								$elements = $displaGroup->getElements();
								foreach($elements as $element)
								{
									$group_arr[$element->getName()] = $element->getName();
									if($element->getType() == 'Zend_Form_Element_File')
									{
										$element_name = $element->getName();
										$required = ($element->isRequired()) ? true : false;
										$element_info = $element;
										$this->ProductForm->removeElement($element_name);
										$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
									}
									$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group, 'legend' => $group->field_group));
								}
							}				
						}
					}
					if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
					}
					if($this->ProductForm->isValid($this->_request->getPost())) 
					{												
						$hotels = new Hotels_Model_Hotels($this->ProductForm->getValues());
						$auth = $this->view->auth;
						$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';	
						$hotels->setActive($stat); 
						$hotels->setFeatured('0');	
						$hotels->setHotels_primary_image($this->_request->getPost('hotels_primary_image'));
						$hotels->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));						
						$hotels->setRelated_items($this->_request->getPost('related_items'));
						$hotels->setRoom_type_id($this->_request->getPost('room_type_id'));
						$hotels->setFeature_facilities($this->_request->getPost('feature_facilities'));
						$hotels->setFeature_sports_recreations($this->_request->getPost('feature_sports_recreations'));										
						
						if($this->view->allow())
						{
							$result = $hotels->saveHotels();
							if($result['status'] == 'ok')
							{
								$msg = $this->translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{									
									$result2 = $hotels->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $this->translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $this->translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}
							}
							else
							{
								$msg = $this->translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}												
						}
						else
						{
							$Msg =  $this->translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $Msg);
						}									
					}
					else
					{
						$validatorMsg = $this->ProductForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg);
					}	
				}
				else
				{
					$msg = $this->translator->translator("hotels_add_limit_err",$package_no);
					$json_arr = array('status' => 'err','msg' => $msg);
				}					
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}		
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{				
			$this->view->proForm = $this->ProductForm;
		}	
	}
	
	public function editAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_list_header_title'), 'Members-Hotel-List'), array($this->translator->translator('hotels_frontend_list_edit_page_name'), $this->view->url()));
		
		
		$id = $this->_getParam('id', 0);
		$translator = $this->translator;
				
		//Get Hotels Info
		$this->ProductForm = new Hotels_Form_ProductForm ();
		$hotelsData = new Hotels_Model_DbTable_Hotels();		
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$id = $this->_request->getPost('id');
			if(!empty($id))
			{
				$hotelsInfo = $hotelsData->getHotelsInfo($id);
				$group_info = new Hotels_Model_DbTable_HotelsGroup();
				$option = $group_info->getGroupName($hotelsInfo['group_id']);
				if($option['dynamic_form'])
				{
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Hotels_Form_ProductForm ($option);
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($option['form_id']); 
					foreach($field_groups as $group)
					{						
						$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
						$elements = $displaGroup->getElements();
						foreach($elements as $element)
						{
							$group_arr[$element->getName()] = $element->getName();
							if($element->getType() == 'Zend_Form_Element_File')
							{
								$element_name = $element->getName();
								$required = ($element->isRequired()) ? true : false;
								$element_info = $element;
								$this->ProductForm->removeElement($element_name);
								$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
							}
							$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group));
						}
					}				
				}
			}
			
			
			try
			{
				if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
				}
				if($this->ProductForm->isValid($this->_request->getPost())) 
				{										
					if(!empty($id))
					{					
						$hotels = new Hotels_Model_Hotels($this->ProductForm->getValues());
						$hotels->setPrev_category($hotelsInfo['category_id']);
						$hotels->setPrev_group($hotelsInfo['group_id']);
						$hotels->setId($id);
						$hotels->setHotels_primary_image($this->_request->getPost('hotels_primary_image'));
						$hotels->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));					
						$hotels->setHotels_title($this->ProductForm->getValue('hotels_title'),$id);						
						$hotels->setRelated_items($this->_request->getPost('related_items'));
						$hotels->setRoom_type_id($this->_request->getPost('room_type_id'));
						$hotels->setFeature_facilities($this->_request->getPost('feature_facilities'));
						$hotels->setFeature_sports_recreations($this->_request->getPost('feature_sports_recreations'));									
						
						$perm = new Hotels_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $hotels->saveHotels();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{									
									$result2 = $hotels->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}						
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}						
						}
						else
						{
							$msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $msg);
						}					
					}
					else
					{
						$msg = $translator->translator("page_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}				
				}
				else
				{
					$validatorMsg = $this->ProductForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}			
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);		
		}
		else
		{	
			//Get Hotels Info				
			$hotelsInfo = $hotelsData->getHotelsInfo($id);	
			if($hotelsInfo)
			{
				//Put Group Information in the view
				$group_info = new Hotels_Model_DbTable_HotelsGroup();
				$option = $group_info->getGroupName($hotelsInfo['group_id']);
				
				//Dynamic Form
				if(!empty($option['dynamic_form']))
				{   
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Hotels_Form_ProductForm ($option);	
						$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
					$hotelsInfo	=	$dynamic_valu_db->getFieldsValueInfo($hotelsInfo,$hotelsInfo['id']);					
				}
				
						
				//Get Category Info
				$categoryData = new Hotels_Model_DbTable_Category();	
				$group_id = $hotelsInfo['group_id'];
				
				
				if($hotelsInfo['category_id'] == '0')
				{
					$selected = 'style="background-color:#D7D7D7"';
					$category_name = $translator->translator("hotels_tree_root");
				}
				else
				{
					$categoryInfo = $categoryData->getCategoryInfo($hotelsInfo['category_id']);
					$category_name = $categoryInfo['category_name'];	
					$selected = '';
				}		
				
				
				//ASSIGN CATEGORY TREE
				$this->view->categoryTree = '<table class="example" id="dnd-example">'.
											'<tbody ><tr id="node-0">'.
											 '<td '.$selected.'>'.
												'<span class="folder">'.$translator->translator("hotels_tree_root").'</span>'.
											'</td>'.
										'</tr>'.Hotels_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,$hotelsInfo['category_id']).'</tbody></table>';
				
				//ASSIGN PROPERTY TYPE
				$r = true;			
				if(is_numeric($hotelsInfo['hotels_agent']))
				{ 
					if(is_int((int)$hotelsInfo['hotels_agent']))
					{
						$r = false;
					}				
				}
				if($r){ $this->ProductForm->hotels_agent->addMultiOption($hotelsInfo['hotels_agent'],stripslashes($hotelsInfo['hotels_agent'])); }
				
				//ASSIGN PROPERTY TYPE
				$hotelsBusinessType = new Hotels_Model_DbTable_BusinessType();			
				$hotelsBusinessType_options = $hotelsBusinessType->getSelectOptions($hotelsInfo['group_id']);
				$this->ProductForm->hotels_type->addMultiOptions($hotelsBusinessType_options);
				
				//ASSIGN STATE
				$states = new Eicra_Model_DbTable_State();			
				$states_options = $states->getOptions($hotelsInfo['country_id']);			
				$this->ProductForm->state_id->addMultiOptions($states_options);
				
				//ASSIGN AREA / CITY
				$areas = new Eicra_Model_DbTable_City();			
				$areas_options = $areas->getOptions($hotelsInfo['state_id']);			
				$this->ProductForm->area_id->addMultiOptions($areas_options);
				
				//ASSIGN CHECKIN TIME
				$checkin_arr = explode(':',$hotelsInfo['checkin_date']);
				$hotelsInfo['checkin_hour'] = $checkin_arr[0];
				$hotelsInfo['checkin_min'] = $checkin_arr[1];
				
				//ASSIGN CHECKOUT TIME
				$checkout_arr = explode(':',$hotelsInfo['checkout_date']);
				$hotelsInfo['checkout_hour'] = $checkout_arr[0];
				$hotelsInfo['checkout_min'] = $checkout_arr[1];	
				
				//ASSIGN ROOM TYPE
				$this->ProductForm->room_type_id->setIsArray(true); 
				$room_type_id_arr = explode(',',$hotelsInfo['room_type_id']);	
				$hotelsInfo['room_type_id'] = 	$room_type_id_arr;	
				
				//ASSIGN RELETED ITEMS
				$this->ProductForm->related_items->setIsArray(true); 
				$related_items_arr = explode(',',$hotelsInfo['related_items']);	
				$hotelsInfo['related_items'] = 	$related_items_arr;	
				
				//ASSIGN FEATURED FACILITIES
				$this->ProductForm->feature_facilities->setIsArray(true); 
				$feature_facilities_arr = explode(',',$hotelsInfo['feature_facilities']);	
				$hotelsInfo['feature_facilities'] = 	$feature_facilities_arr;	
				
				//ASSIGN FEATURED SPORTS RECREATIONS
				$this->ProductForm->feature_sports_recreations->setIsArray(true); 
				$feature_sports_recreations_arr = explode(',',$hotelsInfo['feature_sports_recreations']);	
				$hotelsInfo['feature_sports_recreations'] = 	$feature_sports_recreations_arr;
						
				$this->view->option =  $option;	
				$this->view->hotelsInfo = $hotelsInfo;					
				$this->ProductForm->populate($hotelsInfo);										
				$this->view->id = $id;
				$this->view->entry_by = $hotelsInfo['entry_by'];
				$this->view->category_name = $category_name;
				$this->view->hotels_primary_image = $hotelsInfo['hotels_primary_image'];
				$this->view->feature_primary_interior_image = $hotelsInfo['feature_primary_interior_image'];			
				$this->view->proForm = $this->ProductForm;	
				$this->view->form_info = $this->ProductForm->getFormInfo ();
				$this->dynamicUploaderSettings($this->view->form_info);	
				$this->userUploaderSettings($hotelsInfo);
				$this->render();
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Hotel-Add', array());
			} 
		}	
			
    }
	
	private function userUploaderSettings($info)
	{
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		$group_info = $group_db->getGroupName($info['group_id']);
		$param_fields = array(
								'table_name' => 'hotels_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_hotels_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));	
		
		/**************************************************For Secondary**************************************************/	
			$secondary_requested_data	=	$requested_data;
			$secondary_requested_data['file_path_field'] = 'file_path_feature_interior_plan_image';			
			
			$this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));		
			$this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info));
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);		
	}
	
	public function favoriteAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_favorite_list_header_title'), $this->view->url()));
		
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Favorite-Hotel-List/*',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_SavedHotelsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_hotels_name'] = str_replace('_', '-', $entry_arr['hotels_name']);
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);								
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
    }	
	
	public function listroomtypeAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_type_list_header_title'), $this->view->url()));
		
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$list_mapper = new Hotels_Model_RoomTypeListMapper();	
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Hotel-Room-Type-List/*',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{					
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{						
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $entry_arr['room_type']);	
							$img_file_arr = explode(',',$entry_arr['room_type_image']);
							$entry_arr['room_type_image_format'] = ($this->view->escape($entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
							$entry_arr['id_format']	=	$this->view->numbers($this->view->escape($entry_arr['id']));
							$entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($entry_arr['max_people']));
							$entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($entry_arr['room_num']));
							$entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($entry_arr['basic_price']));
							$entry_arr['descount_price_format']	=	$this->view->numbers($this->view->escape($entry_arr['descount_price']));
							$entry_arr['price_per_night_format']		=	$this->view->numbers($this->view->escape($entry_arr['price_per_night']));
							$entry_arr['price_after_discount_format']	=	$this->view->numbers($this->view->escape($entry_arr['price_after_discount']));
							$entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($entry_arr['room_type']), 'Members');
							$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;						
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}
		
		$mem_db	=	new Members_Model_DbTable_MemberList();
		$this->view->assign('user_list', $mem_db->getAllMembers());
		
		$hotel_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('hotel_list', $hotel_db->getHotelsInfoByGroup());
		
		$this->view->assign('people_list', $list_mapper->getDbTable()->maxPeopleListRoomTypeUnic('ASC'));
		
		$price_db = new Hotels_Model_DbTable_Price();
		$this->view->assign('price_options', $price_db->getOptions());
		
		//$this->view->assign('price_min_data', $list_mapper->getDbTable()->priceListRoomTypeUnic('ASC'));
		
		//$this->view->assign('price_max_data', $list_mapper->getDbTable()->priceListRoomTypeUnic('DESC'));
	}
	
	public function addroomtypeAction()
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_type_list_header_title'), 'Members-Hotel-Room-Type-List'), array($this->translator->translator('hotels_frontend_room_type_list_add_page_name'), $this->view->url()));
		
		$this->hotelsRoomTypeForm =  new Hotels_Form_RoomTypeForm ();
		$this->view->hotelsRoomTypeForm =  $this->hotelsRoomTypeForm;
			
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->hotelsRoomTypeForm->isValid($this->_request->getPost())) 
			{	
				$room = new Hotels_Model_RoomType($this->hotelsRoomTypeForm->getValues());
								
				$perm = new Hotels_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $room->saveRoomType();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];		
						$hotel_id_arr = $this->_request->getPost('hotel_id');	
						$hotel_db = new Hotels_Model_DbTable_Hotels();
						$linkResult = $hotel_db->linkToHotels($last_id, $hotel_id_arr);		
						if($linkResult['status'] == 'ok')
						{					
							$msg = $translator->translator("hotels_room_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $linkResult['msg'];
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator("hotels_room_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->hotelsRoomTypeForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoomType)
				{					
					foreach($errRoomType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		$this->userUploaderRoomTypeSettings();		
	}
	
	public function editroomtypeAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_type_list_header_title'), 'Members-Hotel-Room-Type-List'), array($this->translator->translator('hotels_frontend_room_type_list_edit_page_name'), $this->view->url()));
		
		$this->hotelsRoomTypeForm =  new Hotels_Form_RoomTypeForm ();
		$this->view->hotelsRoomTypeForm =  $this->hotelsRoomTypeForm;
		
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			$price = $this->_request->getPost('descount_price');		
				
			$perm = new Hotels_View_Helper_Allow();
			if($perm->allow())
			{
				if($this->hotelsRoomTypeForm->isValid($this->_request->getPost())) 
				{	
					$room = new Hotels_Model_RoomType($this->hotelsRoomTypeForm->getValues());
					$room->setId($id);	
					$room->setRoom_available_information($id, $price);
					$result = $room->saveRoomType();
					if($result['status'] == 'ok')
					{								
						$hotel_id_arr = $this->_request->getPost('hotel_id');	
						$hotel_db = new Hotels_Model_DbTable_Hotels();
						$linkResult = $hotel_db->linkToHotels($id, $hotel_id_arr);		
						if($linkResult['status'] == 'ok')
						{
							$msg = $translator->translator("hotels_room_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $linkResult['msg'];
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator("hotels_room_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->hotelsRoomTypeForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoomType)
					{					
						foreach($errRoomType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$room_type_db = new Hotels_Model_DbTable_RoomType();
			$room_type_info	=	$room_type_db->getRoomTypeInfo($id);
			if($room_type_info)
			{
				$this->hotelsRoomTypeForm->hotel_id->setIsArray(true); 
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$hotels_info= $hotels_db->getHotelsInfoByGroup(null);
				$file_type_arr = array();
				$t	=	0;
				foreach($hotels_info as $hotel_key =>$hotel_arr)
				{
					$hotel_room_type_id_arr = explode(',', $hotel_arr['room_type_id']);
					if(in_array($id, $hotel_room_type_id_arr))
					{
						$file_type_arr[$t] = $hotel_arr['id'];
						$t++;
					}				
				}
				$this->hotelsRoomTypeForm->hotel_id->setValue($file_type_arr);
				$this->userUploaderRoomTypeSettings();
				$this->hotelsRoomTypeForm->populate($room_type_info);
				$this->view->entry_by = $room_type_info['entry_by'];
				$this->view->id = $id;
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Hotel-Room-Type-List-Add', array());
			}
		}			
	}
	
	private function userUploaderRoomTypeSettings($info = null)
	{
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		if($info && $info['group_id'])
		{
			$group_info = $group_db->getGroupName($info['group_id']);
		}
		else
		{
			$info['group_id'] = $group_db->getFirstGroupId();
			$group_info = $group_db->getGroupName($info['group_id']);
			$this->view->assign('first_group_id' ,	$info['group_id']);
		}
		$param_fields = array(
								'table_name' => 'hotels_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_room_type_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));
	}
	
	public function listroomAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), $this->view->url()));
		
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->room_type_id = $posted_data['room_type_id'];
		
		$list_mapper = new Hotels_Model_RoomListMapper();	
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');
				$room_type_id = ($this->_request->getParam('room_type_id')) ? $this->_request->getParam('room_type_id') : null;	
				
				if($room_type_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $room_type_id);					
				}			
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = ($posted_data['browser_url'] == 'no') ? '' : $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'room_type_id' => $room_type_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{					
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{						
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_room_name'] = str_replace('_', '-', $entry_arr['room_name']);	
							$entry_arr['room_entry_date_format']=  date('Y-m-d h:i:s A', strtotime($entry_arr['room_entry_date']));
							$entry_arr['id_format']	=	$this->view->numbers($this->view->escape($entry_arr['id']));
							$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;						
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}
		
		$mem_db	=	new Members_Model_DbTable_MemberList();
		$this->view->assign('user_list', $mem_db->getAllMembers());	
		
		$hotel_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('hotel_list', $hotel_db->getHotelsInfoByGroup());	
	}
	
	public function addroomAction()
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), 'Members-Hotel-Room-List'), array($this->translator->translator('hotels_frontend_room_list_add_page_name'), $this->view->url()));
		$this->hotelsRoomForm =  new Hotels_Form_RoomForm ();
		$this->view->hotelsRoomForm =  $this->hotelsRoomForm;
			
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->hotelsRoomForm->isValid($this->_request->getPost())) 
			{	
				$room = new Hotels_Model_Room($this->hotelsRoomForm->getValues());
								
				$perm = new Hotels_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $room->saveRoom();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];						
						$msg = $translator->translator("hotels_room_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("hotels_room_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->hotelsRoomForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoom)
				{					
					foreach($errRoom as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}		
	}
	
	public function editroomAction()
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), 'Members-Hotel-Room-List'), array($this->translator->translator('hotels_frontend_room_list_edit_page_name'), $this->view->url()));
		$this->hotelsRoomForm =  new Hotels_Form_RoomForm ();
		$this->view->hotelsRoomForm =  $this->hotelsRoomForm;
		
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			$id = $this->_request->getPost('id');
					
				
			$perm = new Hotels_View_Helper_Allow();
			if($perm->allow())
			{
				if($this->hotelsRoomForm->isValid($this->_request->getPost())) 
				{	
					$room = new Hotels_Model_Room($this->hotelsRoomForm->getValues());
					$room->setId($id);						
					$result = $room->saveRoom();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("hotels_room_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("hotels_room_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->hotelsRoomForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoom)
					{					
						foreach($errRoom as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$room_db = new Hotels_Model_DbTable_Room();
			$room_info	=	$room_db->getRoomInfo($id);
						
			if($room_info)
			{
				if($room_info['hotel_id'])
				{
					$this->hotelsRoomForm =  new Hotels_Form_RoomForm ($room_info);
					$this->view->hotelsRoomForm =  $this->hotelsRoomForm;
				}
				else
				{
					$this->hotelsRoomForm->loadRoomType ();
				}		
				$this->hotelsRoomForm->populate($room_info);
				$this->view->entry_by = $room_info['entry_by'];
				$this->view->id = $id;
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Hotel-Room-List-Add', array());
			}
		}			
	}	
	
	public function schedulerAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), 'Members-Hotel-Room-List'), array($this->translator->translator('hotels_page_scheduler_page_name'), $this->view->url()));
		$id = $this->_getParam('id', 0);
		//Get Room Info
		$room_db = new Hotels_Model_DbTable_Room();
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				if(!empty($id))
				{
					$posted_data['filter']['filters'][] = array('field' => 'room_id', 'operator' => 'eq', 'value' => $id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				
										
					$list_db = new Hotels_Model_DbTable_Scheduler();	
					$list_datas =  $list_db->getListInfo(null, $posted_data, array('userChecking' => true));
					$view_datas = array('data_result' => array());			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								//$entry_arr['start'] = "/Date(".(strtotime($entry_arr['start_date']) * 1000 ).")/";
								//$entry_arr['end'] = "/Date(".(strtotime($entry_arr['end_date']) * 1000 ).")/";								
								$entry_arr['Start'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['Start']));
								$entry_arr['End'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['End']));
								$entry_arr['IsAllDay'] = (boolean)$entry_arr['IsAllDay'];
								
								$view_datas['data_result'][$key]	=	$entry_arr; //array('hotels_name' => $entry_arr['hotels_name'], 'start' => $entry_arr['start'], 'end' => $entry_arr['end'] );	
								$key++;					
						}					
					}
					$json_arr = array('id' => $id, 'status' => 'ok', 'data_result' => $view_datas['data_result'], 'posted_data' => $posted_data);
				}
				else
				{
					$json_arr = array('status' => 'ok', 'data_result' => '', 'posted_data' => $posted_data);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		else
		{		
			if(!empty($id))
			{
				//Get Vacationrentals Info
				$roomInfo = $room_db->getRoomInfo($id);
				if($roomInfo)
				{				
					$this->view->id = $id;
					$this->view->entry_by = $roomInfo['entry_by'];	
					$this->view->roomInfo = $roomInfo;			
				}
				else
				{					
					$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Hotel-Room-List', array());
				}
			}
			else
			{
				$this->_helper->redirector->gotoUrlAndExit($this->view->serverUrl().$this->view->baseUrl().'/Members-Hotel-Room-List', array());
			}
		}
	}
	
	public function listpriceAction()
    {
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('hotels_price_plan_list_page_name'), $this->view->url()));
					
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'Members-Hotel-Price-List/*',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_PriceListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_plan_name'] = str_replace('_', '-', $entry_arr['plan_name']);
								
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Hotels_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Hotels_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Hotels_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());	
		
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('distanceFromAirport_info', $hotels_db->getDistanceFromAirport());
    }	
	
	
	//PROPERTY FUNCTIONS		
	public function addpriceAction()
	{	
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), 'Members-Hotel-Price-List'), array($this->translator->translator('hotels_price_plan_add_page_name'), $this->view->url()));
		$this->priceForm =  new Hotels_Form_PriceForm ();	
		$this->view->assign('priceForm', $this->priceForm);	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			$seasonal_price = $this->_request->getPost('seasonal_price_per_night');
			if(!empty($seasonal_price) || ($this->_request->getPost('season_start_date') != null && $this->_request->getPost('season_start_date') != '0000-00-00') || ($this->_request->getPost('season_end_date') != null && $this->_request->getPost('season_end_date') != '0000-00-00'))
			{
				$this->priceForm->seasonal_price_per_night->setRequired(true);
				$this->priceForm->season_start_date->setRequired(true);
				$this->priceForm->season_end_date->setRequired(true);
			}
			$this->priceForm->season_start_date->getValidator('LessThan')->setMax($this->_request->getPost('season_end_date'));
			$this->priceForm->season_end_date->getValidator('GreaterThan')->setMin($this->_request->getPost('season_start_date'));
			
			if($this->priceForm->isValid($this->_request->getPost())) 
			{	
				$price = new Hotels_Model_Price($this->priceForm->getValues());
				if($this->view->allow())
				{
					$result = $price->savePrice();
					if($result['status'] == 'ok')
					{										
						$msg = $translator->translator("hotels_price_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);						
					}
					else
					{
						$msg = $translator->translator("hotels_price_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->priceForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoomType)
				{					
					foreach($errRoomType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}				
	}
	
	public function editpriceAction()
	{
		//BreadCrumb
		Eicra_Global_Variable::getSession()->breadcrumb = array(array($this->translator->translator('members_frontend_breadcrumb_dashboard_title', '', 'Members'), 'Members-Dashboard'), array($this->translator->translator('members_dashboard_block_hotels_room_list_header_title'), 'Members-Hotel-Price-List'), array($this->translator->translator('hotels_price_plan_edit_page_name'), $this->view->url()));
		$this->priceForm =  new Hotels_Form_PriceForm ();
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			$id = $this->_request->getPost('id');					
			
			if($this->view->allow())
			{
				$seasonal_price = $this->_request->getPost('seasonal_price_per_night');
				if(!empty($seasonal_price) || ($this->_request->getPost('season_start_date') != null && $this->_request->getPost('season_start_date') != '0000-00-00') || ($this->_request->getPost('season_end_date') != null && $this->_request->getPost('season_end_date') != '0000-00-00'))
				{
					$this->priceForm->seasonal_price_per_night->setRequired(true);
					$this->priceForm->season_start_date->setRequired(true);
					$this->priceForm->season_end_date->setRequired(true);
				}
				$this->priceForm->season_start_date->getValidator('LessThan')->setMax($this->_request->getPost('season_end_date'));
				$this->priceForm->season_end_date->getValidator('GreaterThan')->setMin($this->_request->getPost('season_start_date'));
				if($this->priceForm->isValid($this->_request->getPost())) 
				{	
					$price = new Hotels_Model_Price($this->priceForm->getValues());
					$price->setId($id);	
					
					$result = $price->savePrice();
					if($result['status'] == 'ok')
					{										
						$msg = $translator->translator("hotels_price_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);						
					}
					else
					{
						$msg = $translator->translator("hotels_price_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->priceForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoomType)
					{					
						foreach($errRoomType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$price_plan_db = new Hotels_Model_DbTable_Price();
			$price_plan_info	=	$price_plan_db->getInfo($id);
			if($price_plan_info)
			{
				$this->priceForm->room_type_id->setIsArray(true); 
				$room_type_db = new Hotels_Model_DbTable_RoomType();
				$room_type_info = $room_type_db->getListInfo(null, array('filter' => array('filters' => array(array('field' => 'price_plan_id', 'operator' => 'eq', 'value' => $id)))), false);
				$file_type_arr = array();
				$t	=	0;
				foreach($room_type_info as $room_type_key => $room_type_arr)
				{
					$file_type_arr[$t] = $room_type_arr['id'];
					$t++;								
				}
				$this->priceForm->room_type_id->setValue($file_type_arr);
				
				$this->priceForm->populate($price_plan_info);
				$this->view->entry_by = $price_plan_info['entry_by'];
				$this->view->id = $id;
				$this->view->assign('priceForm', $this->priceForm);	
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}			
	}	
}