<?php

class Hotels_TravelstartController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_group_form_id_field = 'dynamic_form';
    private $_group_table = 'hotels_group';
    private $_static_table = 'hotels_page';
    private $_controllerCache;
    private $_translator;
    private $_api_settings;

    public function init() {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        // DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache ();
        $this->_controllerCache = $cache->getCache();
        $this->_api_settings = new Hotels_Model_TravelstartModel();

        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template ();
        $template_obj->setFrontendTemplate();

        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;

        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr [0])) ? $page_id_arr [0] : null;
        } else {
            $this->_page_id = 1;
        }
    }

    public function frontshowAction(){
//        $this->_helper->viewRenderer->setNoRender();
//        $this->_helper->layout->disableLayout();
        $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $affID = $this->_api_settings->getAffiliateID();
        $this->view->affID = $affID; 
    }
}
