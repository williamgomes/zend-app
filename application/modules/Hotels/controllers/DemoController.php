<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hotels_DemoController extends Zend_Controller_Action {

    protected $_demotestform;
    
    public function init() {
        /* Initialize action controller here */
        $this->_demotestform = new Hotels_Form_DemotestForm();
        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->auth = $auth;
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');
        $translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $translator);
        $this->view->setEscape('stripslashes');
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);
            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }

    function demoAction() {
        $myArray = array();
        $myArray[] = 1;
        $myArray[] = 2;
        $myArray[] = 3;
        $myArray[] = 4;
        $myArray[] = 5;
        $myArray[] = 6;
        $myArray[] = 7;

//        $modelPackage = new B2b_Model_PackageMapper();
//        $arrPackage = $modelPackage->getPackages(true);
//
//        $this->view->assign("array", $arrPackage);
        

        if ($this->_request->isPost()) {
            try {

                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->_demotestform->isValid($this->_request->getPost())) {
                    $saveUserManager = new Hotels_Model_Demotest($this->_demotestform->getValues());
                    $result = $saveUserManager->saveDemotest();
                   
                    if ($result['status'] == 'ok') {
                        $msg = $this->view->translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->_demotestform->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            
        $this->view->assign('demotestform', $this->_demotestform);
        }
    }

}
