<?php

/**

 * ******************************************************************************

 * Eicra Soft Ltd - is the developer of this application

 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved

 *

 ******************************************************************************

 * @Email   : info@eicra.com                                              *

 * @Modules : Expedia Hotel API                                           *

 * @author  : Eicra Soft Limited                                          *

 * @Website : http://www.eicra.com                                        *

 ******************************************************************************

 *                                                                       *

 * This software is furnished under a license and may be used and copied *

 * only  in  accordance  with  the  terms  of such  license and with the *

 * inclusion of the above copyright notice.  This software  or any other *

 * copies thereof may not be provided or otherwise made available to any *

 * other person.  No title to and  ownership of the  software is  hereby *

 * transferred.                                                          *

 *                                                                       *

 * You may not reverse  engineer, decompile, defeat  license  encryption *

 * mechanisms, or  disassemble this software product or software product *

 * license.  We  may terminate this license if you don't *

 * comply with any of the terms and conditions set forth in our end user *

 * license agreement (EULA).  In such event,  licensee  agrees to return *

 * licensor  or destroy  all copies of software  upon termination of the *

 * license.                                                              *

 *                                                                       *

 * Please see the EULA file for the full End User License Agreement.     *

 ******************************************************************************

 */

 

class Hotels_BackendexpediaController extends Zend_Controller_Action
{

	private $_translator;
	private $_controllerCache;
	private $_auth_obj;	

 	public function init()
    {
        /* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	

		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }

	

	public function preDispatch() 	
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);
		$this->view->setEscape('stripslashes');	
	}

	

	public function itineraryAction()
	{	

		// action body
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
							
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'group_id'	=> $group_id, 'category_id'	=> $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_ExpediaMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['itineraryId_format']=  $this->view->numbers($entry_arr['itineraryId']);
								$entry_arr['arrivalDate_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['arrivalDate'])));	
								$entry_arr['arrivalDate_format']=  $this->view->numbers(date('Y-m-d ',strtotime($entry_arr['arrivalDate'])));
								$entry_arr['departureDate_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['departureDate'])));	
								$entry_arr['departureDate_format']=  $this->view->numbers(date('Y-m-d',strtotime($entry_arr['departureDate'])));
								$entry_arr['view_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}

	}

	

	public function viewAction()

	{

		

		$id = $this->_request->getParam('id');

		$itinerary = new Hotels_Model_DbTable_Expedia();	

		$eanItinerary = $itinerary->getReservationInfo($id);

		

		if (! empty( $eanItinerary)){

			

			$this->view->assign('itinerary_arr', $eanItinerary );

			

		

		}

		else {

			// redirect to 	itinerary action 

		}

		

	}

	

	public function cancelAction()

	{

		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

		$this->_helper->viewRenderer->setNoRender();

		$this->_helper->layout->disableLayout();

		

		$id = $this->_request->getParam('id');

		$itineraryMapper = new Hotels_Model_ExpediaMapper();

		$itinerary = new Hotels_Model_DbTable_Expedia();		

		$eanItinerary = $itinerary->getReservationInfo($id);

		$cancellation = $eanItinerary ['cancel'] ;

		$return_msg = null;

		if (!empty( $id ) && empty( $cancellation) ){

			$isCancelled = $this->cancelRequestToExpedia($eanItinerary);

			

			if (!empty($isCancelled['result'])){

				 

				// Save cancellation state to Database.

				$result = $itineraryMapper->cancelItinerary($id);

				

				if ($result['status'] == 'ok'){

					$massage = $this->_translator->translator('hotels_api_expedia_backend_view_itinerary_cancel_successful') ;

					$cancellationNumber = $this->_translator->translator('hotels_api_expedia_itinerary_cancellationNumber') ;	

					$displayMassage = $massage . '<br />' .$cancellationNumber . $isCancelled['cancellationNumber'];

					

					$return_msg = array('status' => 'ok', 'id' => $id,'msg' => $displayMassage, 'cancel' => '1');

						

				}

				

				else {

					$massage = '<span style="color:red">'.$this->_translator->translator('hotels_api_expedia_backend_view_itinerary_cancel_fail')  . $result['msg'].'</span>';

					$return_msg = array('status' => 'err', 'id' => $id,'msg' => $massage, 'cancel' => '0');	  	

				}

			

			}

			else {

				

				$massage = '<span style="color:red">'.$isCancelled['verboseMessage'].'</span>';

				$return_msg = array('status' => 'err', 'id' => $id,'msg' => $massage, 'cancel' => '0');	  

			}

			

		}

		else {

			$massage = array('status' => 'err', 'id' => $id,

                        'msg' => '<span style="color:red">'.$this->_translator->translator('hotels_api_expedia_backend_view_itinerary_id_null').'</span>');	

			$return_msg = array('status' => 'err', 'id' => $id,'msg' => $massage, 'cancel' => '0');	

		}

		

		

		$res_value = Zend_Json_Encoder::encode($return_msg) ;

		$this->_response->setBody($res_value);		

	}

	

	public function deleteAction()

	{

		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

		$this->_helper->viewRenderer->setNoRender();

		$this->_helper->layout->disableLayout();

		

		$id = $this->_request->getPost('id');

		$itineraryMapper = new Hotels_Model_ExpediaMapper();

		

		$return_msg = null;

		if (! empty( $id ))

		{

			$result = $itineraryMapper->deleteItinerary($id);

			if ($result['status'] == 'ok')

			{

				$massage = $this->_translator->translator('hotels_api_expedia_backend_view_itinerary_delete_successful') ;

				$return_msg = array('status' => 'ok', 'id' => $id,'msg' => $massage);	  		

			}

			

			else 

			{

				$massage = $this->_translator->translator('hotels_api_expedia_backend_view_itinerary_delete_fail')  . $result['msg'];

				

				$return_msg = array('status' => 'err', 'id' => $id,'msg' => $massage);	  	

			}

		}

		else 

		{

			$return_msg = array('status' => 'err', 'id' => $id, 'msg' => $this->_translator->translator('hotels_api_expedia_backend_view_itinerary_id_null'));	

		}

		$json_arr = Zend_Json_Encoder::encode($return_msg) ;

		$this->_response->setBody($json_arr);

		

	}

	

	public function deleteallAction()

	{

		$this->_helper->viewRenderer->setNoRender();

		$this->_helper->layout->disableLayout();

		$translator = Zend_Registry::get('translator');

		

		if ($this->_request->isPost()) 

		{

			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	

			$id_str = $this->_request->getPost('id_st');

			$perm = new Hotels_View_Helper_Allow();			

			if ($perm->allow('delete','backendexpedia','Hotels'))

			{

				if(!empty($id_str))

				{	

					$id_arr = explode(',',$id_str);		

					$itineraryMapper = new Hotels_Model_ExpediaMapper();

					

					foreach($id_arr as $id)

					{						

						if (! empty( $id ))

						{

							$result = $itineraryMapper->deleteItinerary($id);

							if ($result['status'] == 'ok')

							{

								$massage = $this->_translator->translator('hotels_api_expedia_backend_view_itinerary_delete_successful') ;

								$json_arr = array('status' => 'ok', 'id' => $id,'msg' => $massage);	  		

							}

						}

					}

				}

				else

				{

					$msg = $translator->translator("category_selected_err");

					$json_arr = array('status' => 'err','msg' => $msg);

				}

			}

			else

			{

				$msg = 	$translator->translator('category_delete_perm');			

				$json_arr = array('status' => 'err','msg' => $msg);

			}

		}

		else

		{

			$msg = 	$translator->translator('category_list_delete_err');			

			$json_arr = array('status' => 'err','msg' => $msg);

		}

		//Convert To JSON ARRAY	

		$res_value = Zend_Json_Encoder::encode($json_arr);	

		$this->_response->setBody($res_value);

	}		

	

	

	private function cancelRequestToExpedia ($eanItinerary){

		

		$itineraryId = $eanItinerary ['itineraryId'] ;

		$email = $eanItinerary ['email'] ;

		$confirmationNumbers = $eanItinerary ['confirmationNumbers'] ;

		$resultSetObject   = $eanItinerary ['resultSetObject'];
		
		$resultSetObject_arr = Zend_Json_Decoder::decode($resultSetObject) ;

		$api_settings  = new Hotels_Model_ExpediaApiModel();

		$expedia_Api_helper = new Hotels_Controller_Helper_Expedia();

		

        $apiKey = $api_settings->getApiKey();

        $cid = $api_settings->getCid() ;

        $locale = $api_settings->getLocale();

        $minorRev = $api_settings->getMinorRev();

        $currencyCode = $api_settings->getCurrencyCode();

        $numberOfResults = $api_settings->getNumberOfResults();

        $supplierType = $api_settings->getSupplierType();

		

		

		/****************************************************

		 ** Building HTTP URL-encoded query string

		****************************************************/

		$httpData =  new Zend_Http_Client();

		$httpData->setParameterGet('minorRev' , $minorRev);

		$httpData->setParameterGet('cid' , $cid);

		$httpData->setParameterGet('apiKey' , $apiKey);

		$httpData->setParameterGet('customerUserAgent' , $_SERVER['HTTP_USER_AGENT']);

		$httpData->setParameterGet('customerIpAddress' , $expedia_Api_helper->getRealIpAddr());
		
		$httpData->setParameterGet('customerSessionId' , $resultSetObject_arr['HotelRoomReservationResponse']['customerSessionId']);

		$httpData->setParameterGet('locale' , $locale);

		$httpData->setParameterGet('currencyCode' , $currencyCode);
		

		/****************************************************

		 ** Building itinerary Details query string

		****************************************************/

		$httpData->setParameterGet('itineraryId' , $itineraryId);

		$httpData->setParameterGet('email' , $email);

		$httpData->setParameterGet('reason' , 'COP');

		$httpData->setParameterGet('confirmationNumber' , $confirmationNumbers);
		
		/*$xml= '<HotelRoomCancellationRequest>
				<itineraryId>'.$itineraryId.'</itineraryId>
				<email>'.$email.'</email>
				<reason>COP</reason>
				<confirmationNumber>'.$confirmationNumbers.'</confirmationNumber>
			</HotelRoomCancellationRequest>';
			
		$httpData->setParameterGet('xml' , $xml);*/

		/****************************************************

		 ** Building query string params

		****************************************************/

		

		$httpData->setUri($api_settings->getCancellationurl($api_settings->getisTest ()));

		//$httpData->setParameterGet('supplierCacheTolerance' , 'MED_ENHANCED');
		
		
		

		

		$httpData->setConfig(array(

									'maxredirects' => 10,

									'timeout'      => 60,

									'keepalive'    => true

									));

		$httpData->setHeaders(array(

									'Accept' => 'application/json',

									'Accept-encoding' => 'gzip,deflate',

									'X-Powered-By' => $siteName ));

		

							

		

	   try {

			

			$resultSet = $httpData->request('GET');
			//$b = urldecode( $httpData->getLastRequest() );
			//file_put_contents('cancelReqUri.txt', $b);
			$responseValue = $resultSet->getBody();

			$responseValue_arr = Zend_Json::decode($responseValue , Zend_Json::TYPE_ARRAY);
		
			$errorMassage = $responseValue_arr['HotelRoomCancellationResponse']['EanWsError']['verboseMessage'] ;

			$successMassage  =  $responseValue_arr['HotelRoomCancellationResponse']['cancellationNumber'] ;

		}

		catch (Exception $e){

			

			return array (

						  'result' => 1,

						  'verboseMessage' => 	$e->getMessage(),

						  'cancellationNumber' => $cancellationNumber

			);

			

		}		

		

		

		if (empty ($errorMassage)){

		

			return array (

						  'result' => 1,

						  'verboseMessage' => 	$successMassage,

						  'cancellationNumber' => $cancellationNumber

			);

			

		}

		

		else {

		

			return array (

						  'result' => 0,

						  'verboseMessage' => 	$errorMassage

						  

			);

			

		}		

	}			

	

}