<?php
class Hotels_BackendpriceController extends Zend_Controller_Action
{
	private $priceForm;
	private $uploadForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;	
	
    public function init()
    {				
        /* Initialize action controller here */	
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
    }
	
	public function preDispatch() 
	{		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');	
		
		
		if($getAction != 'uploadfile')
		{	
			$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->view->getModule);
		}	
	}
	
	//PROPERTY LIST FUNCTION

    public function listAction()
    {			
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'group_id'	=> $group_id, 'category_id'	=> $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_PriceListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_plan_name'] = str_replace('_', '-', $entry_arr['plan_name']);
								
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Hotels_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Hotels_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Hotels_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());	
		
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('distanceFromAirport_info', $hotels_db->getDistanceFromAirport());
    }	
	
	
	
	//PROPERTY FUNCTIONS		
	public function addAction()
	{	
		$this->priceForm =  new Hotels_Form_PriceForm ();	
		$this->view->assign('priceForm', $this->priceForm);	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			$seasonal_price = $this->_request->getPost('seasonal_price_per_night');
			if(!empty($seasonal_price) || ($this->_request->getPost('season_start_date') != null && $this->_request->getPost('season_start_date') != '0000-00-00') || ($this->_request->getPost('season_end_date') != null && $this->_request->getPost('season_end_date') != '0000-00-00'))
			{
				$this->priceForm->seasonal_price_per_night->setRequired(true);
				$this->priceForm->season_start_date->setRequired(true);
				$this->priceForm->season_end_date->setRequired(true);
			}
			$this->priceForm->season_start_date->getValidator('LessThan')->setMax($this->_request->getPost('season_end_date'));
			$this->priceForm->season_end_date->getValidator('GreaterThan')->setMin($this->_request->getPost('season_start_date'));
			
			if($this->priceForm->isValid($this->_request->getPost())) 
			{	
				$price = new Hotels_Model_Price($this->priceForm->getValues());
				if($this->view->allow())
				{
					$result = $price->savePrice();
					if($result['status'] == 'ok')
					{										
						$msg = $translator->translator("hotels_price_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);						
					}
					else
					{
						$msg = $translator->translator("hotels_price_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->priceForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoomType)
				{					
					foreach($errRoomType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}				
	}
	
	public function editAction()
	{
		$this->priceForm =  new Hotels_Form_PriceForm ();
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			$id = $this->_request->getPost('id');					
			
			if($this->view->allow())
			{
				$seasonal_price = $this->_request->getPost('seasonal_price_per_night');
				if(!empty($seasonal_price) || ($this->_request->getPost('season_start_date') != null && $this->_request->getPost('season_start_date') != '0000-00-00') || ($this->_request->getPost('season_end_date') != null && $this->_request->getPost('season_end_date') != '0000-00-00'))
				{
					$this->priceForm->seasonal_price_per_night->setRequired(true);
					$this->priceForm->season_start_date->setRequired(true);
					$this->priceForm->season_end_date->setRequired(true);
				}
				$this->priceForm->season_start_date->getValidator('LessThan')->setMax($this->_request->getPost('season_end_date'));
				$this->priceForm->season_end_date->getValidator('GreaterThan')->setMin($this->_request->getPost('season_start_date'));
				if($this->priceForm->isValid($this->_request->getPost())) 
				{	
					$price = new Hotels_Model_Price($this->priceForm->getValues());
					$price->setId($id);	
					
					$result = $price->savePrice();
					if($result['status'] == 'ok')
					{										
						$msg = $translator->translator("hotels_price_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);						
					}
					else
					{
						$msg = $translator->translator("hotels_price_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->priceForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoomType)
					{					
						foreach($errRoomType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$price_plan_db = new Hotels_Model_DbTable_Price();
			$price_plan_info	=	$price_plan_db->getInfo($id);
			if($price_plan_info)
			{
				$this->priceForm->room_type_id->setIsArray(true); 
				$room_type_db = new Hotels_Model_DbTable_RoomType();
				$room_type_info = $room_type_db->getListInfo(null, array('filter' => array('filters' => array(array('field' => 'price_plan_id', 'operator' => 'eq', 'value' => $id)))), false);
				$file_type_arr = array();
				$t	=	0;
				foreach($room_type_info as $room_type_key => $room_type_arr)
				{
					$file_type_arr[$t] = $room_type_arr['id'];
					$t++;								
				}
				$this->priceForm->room_type_id->setValue($file_type_arr);
				
				$this->priceForm->populate($price_plan_info);
				$this->view->entry_by = $price_plan_info['entry_by'];
				$this->view->id = $id;
				$this->view->assign('priceForm', $this->priceForm);	
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}			
	}
		
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);					
			if ($this->view->allow('delete','backendprice','Hotels'))
			{			
				// Remove from Hotels
				try
				{
					$id = $this->_request->getPost('id');			
					$plan_name = $this->_request->getPost('plan_name');
					
					//price info
					$room_type_db = new Hotels_Model_DbTable_RoomType();
					if(!empty($id))
					{						
						$room_type_db->update(array('price_plan_id' => '0'), 'price_plan_id = '. $id);
					}
					
					$price_db = new Hotels_Model_DbTable_Price();					
					$price_db->delete('id = ' . $id);					
					$msg = 	$translator->translator('hotels_price_plan_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$e->getMessage();			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
					
			if ($this->view->allow('delete','backendprice','Hotels'))
			{
				if(!empty($id_str))
				{	
					$room_type_db = new Hotels_Model_DbTable_RoomType();	
					$price_db = new Hotels_Model_DbTable_Price();
						
					$id_arr = explode(',',$id_str);	
						
					foreach($id_arr as $id)
					{
						//price info
						
						if(!empty($id))
						{						
							$room_type_db->update(array('price_plan_id' => '0'), 'price_plan_id = '. $id);
						}					
										
						$price_db->delete('id = ' . $id);
						
						$msg = 	$translator->translator('hotels_price_plan_delete_success');			
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}				
}

