<?php

class Hotels_BackendareaController extends Zend_Controller_Action
{	
	private $hotelsAreaForm;
	private $_controllerCache;
	
    public function init()
    {		
        /* Initialize action controller here */		
		$this->hotelsAreaForm =  new Hotels_Form_AreaForm ();
		$this->view->hotelsAreaForm =  $this->hotelsAreaForm;
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $translator);	
		$this->view->setEscape('stripslashes');
		
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {
		$state_id = $this->getRequest()->getParam('group_id');
		// action body
		$pageNumber = $this->getRequest()->getParam('page');		
		$getViewPageNum = $this->getRequest()->getParam('viewPageNum'); 		
		$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
		
		if(empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = '30';
		}
		else if(!empty($getViewPageNum) && empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
		else if(empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $viewPageNumSes;
		}
		else if(!empty($getViewPageNum) && !empty($viewPageNumSes))
		{
			$viewPageNum = $getViewPageNum;
		}
					
		Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;	
		$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_'.$viewPageNum;
		if( ($datas = $this->_controllerCache->load($uniq_id)) === false ) 
		{	
			$areas = new Hotels_Model_AreaListMapper();					
			$datas =  $areas->fetchAll($pageNumber,$state_id);
			$this->_controllerCache->save($datas, $uniq_id);	
		}
		$this->view->datas = $datas;
		$this->view->state_id	=	$state_id;
		$this->view->create	= $this->_request->getParam('create');		
	}
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function addAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->hotelsAreaForm->isValid($this->_request->getPost())) 
			{	
				$areas = new Hotels_Model_Areas($this->hotelsAreaForm->getValues());
							
				$perm = new Hotels_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $areas->saveAreas();						
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];
						$areasInfo = new Hotels_Model_DbTable_Area();
						$datas = $areasInfo->getAreaInfo($last_id);
						
						$msg = $translator->translator("hotels_area_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg,'datas' => $datas);
					}
					else
					{
						$msg = $translator->translator("hotels_area_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_area_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->hotelsAreaForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}	
	}
	
	public function editAction()
	{	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$state_id = $this->_request->getPost('state_id');
			$city_id = $this->_request->getPost('city_id');
			$city = $this->_request->getPost('city');
			
			$areas = new Hotels_Model_Areas();
			$areas->setState_id($state_id);	
			$areas->setCity_id($city_id);	
			$areas->setCity($city);								
				
			$perm = new Hotels_View_Helper_Allow();
			if($perm->allow())
			{
				$result = $areas->saveAreas();
				if($result['status'] == 'ok')
				{
					$msg = $translator->translator("hotels_area_save_success");
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				else
				{
					$msg = $translator->translator("hotels_area_save_err");
					$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
				}	
			}
			else
			{
				$Msg =  $translator->translator("hotels_area_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}			
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			$check_num_hotels = new Hotels_View_Helper_HotelsGroup();
					
			if($check_num_hotels->getNumOfProForArea($id) == '0')
			{
				// Remove from Group
				$where = array();
				$where[] = 'city_id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'cities', $where);
					$msg = 	$translator->translator('hotels_area_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('hotels_area_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('hotels_area_for_page_delete_err');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_area_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
					
				$check_num_area = new Hotels_View_Helper_HotelsGroup();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{			
					if($check_num_area->getNumOfProForArea($id) == '0')
					{
						// Remove from Group
						$where = array();
						$where[] = 'city_id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'cities', $where);
							$msg = 	$translator->translator('hotels_area_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e) 
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('hotels_area_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('hotels_area_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("hotels_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_area_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
		
}

