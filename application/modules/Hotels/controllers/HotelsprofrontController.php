<?php

/*
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 * *****************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : HotelsPro Front End                                         *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 * *****************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 * *****************************************************************************
 */

class Hotels_HotelsprofrontController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_group_form_id_field = 'dynamic_form';
    private $_group_table = 'hotels_group';
    private $_static_table = 'hotels_page';
    private $_controllerCache;
    private $_translator;
    private $_api_settings;

    public function init() {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        //DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();
        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
        //Initialize API Details


        $this->_api_settings = new Hotels_Model_HotelsproApiModel();

        $global_conf = Zend_Registry::get('global_conf');
        $this->api_key = $global_conf['hotels_pro_api_key'];
        $this->api_uri = $global_conf['hotels_pro_uri'];
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template ();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;
        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = 1;
        }
    }

    
     public function xml2array($xmlObject) {
        $arr = array();
        foreach ($xmlObject->children() as $r) {
            $t = array();
            if (count($r->children()) == 0) {
                $arr[$r->getName()] = strval($r);
            } else {
                $arr[$r->getName()][] = $this->xml2array($r);
            }
        }
        return $arr;
    }
    
    
    public function itemsAction() {
        
        $dbCountry = new Geo_Model_DbTable_Country();
        $listCountry = $dbCountry->getListInfo('1', '');
        $this->view->assign('arrCountry',$listCountry->toArray());
        
        /******************************************************Before Post Start***************************************************/
        $httpData = new Zend_Http_Client(null, array(
            'timeout' => 60
        ));
        $httpData->setUri($this->_api_settings->getApiUri());
        $httpData->setParameterGet('method', 'getAvailableHotel');
        $httpData->setParameterGet('apiKey', 'eUlpTytLUldMUjdvcnpmeEFOYnRQZ1FPdjlTbXNmd0cyZ2JCdSszWGVCOG5Pc0IzWFM5YzVqWU9wa3EwKzZ3aw=='); //
        $httpData->setParameterGet('destinationId', 'LD6J');
        $httpData->setParameterGet('checkIn', date('Y-m-d', strtotime("+1 month"))); 
        $httpData->setParameterGet('checkOut', date('Y-m-d', strtotime("+35 day"))); 
        $httpData->setParameterGet('currency', 'USD');
        $httpData->setParameterGet('clientNationality', 'US');
        $httpData->setParameterGet('onRequest', 'false');
        $httpData->setParameterGet('rooms['. $a .']['. $i .'][paxType]', 'Adult');
        $httpData->setParameterGet('rooms[0][1][paxType]', 'Adult');
        $httpData->setParameterGet('rooms[0][2][paxType]', 'Adult');
        $httpData->setParameterGet('filters[0][filterType]', 'hotelStar');
        $httpData->setParameterGet('filters[0][filterValue]', '3'); //$postData['rating']
        $httpData->setParameterGet('filters[1][filterType]', 'resultLimit');
        $httpData->setParameterGet('filters[1][filterValue]', 8);
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));

        $resultSet = $httpData->request('GET');
        $responseValue = $resultSet->getBody();
        $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);


        $hotels = $responseValue_arr['availableHotels'];
        $hotelsproDB = new Hotels_Model_DbTable_Hotelspro();
        $hotelInfoDB = array();

        for($x = 0; $x < count($hotels); $x++){
            $hotelCode = $hotels[$x]['hotelCode'];
            $hotelData = $hotelsproDB->getHotelsproInfo($hotelCode);
            if(!isset($hotelData['name']) OR $hotelData['name'] == ''){
                $hotelData['name'] = $this->_translator->translator('hotels_hotelspro_unknown_hotel');
            }
            if(!isset($hotelData['image']) OR $hotelData['image'] == ''){
                $hotelData['image'] = 'data/frontImages/hotels/no_image.png';
            }
            if(!isset($hotelData['address']) OR $hotelData['address'] == ''){
                $hotelData['address'] = $this->_translator->translator('hotels_hotelspro_unknown_address');
            }
            $responseValue_arr['availableHotels'][$x]['hotelInfoDb'] = $hotelData;
            $responseValue_arr['clientCountry'] = 'US';//
        }
        
       
        
        $this->view->responseVal = $responseValue_arr;
        /******************************************************Before Post End***************************************************/
        
        
        
        /*******************************************After Post Start*************************************************************/
        if ($this->_request->isPost()) {
            
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
                
            
            $postData = $this->_request->getPost();
            
            $httpData = new Zend_Http_Client(null, array(
                'timeout' => 60
            ));
            $httpData->setUri($this->_api_settings->getApiUri());
            $httpData->setParameterGet('method', 'getAvailableHotel');
            $httpData->setParameterGet('apiKey', $this->_api_settings->getApiKey()); //
            $httpData->setParameterGet('destinationId', $postData['destination']);
            $httpData->setParameterGet('checkIn', $postData['check_in']); 
            $httpData->setParameterGet('checkOut', $postData['check_out']); 
            $httpData->setParameterGet('currency', $this->_api_settings->getCurrency());
            $httpData->setParameterGet('clientNationality', $postData['country']);
            $httpData->setParameterGet('onRequest', 'false');
            
            $url = '';
            try{
                for($a = 0; $a < 5; $a++){ //this is for loop for per room
                    if($postData['adult-select-' . $a] > 0){
                        //generating adult calculation
                        for($i = 0; $i < $postData['adult-select-' . $a]; $i++){ //this is for loop for adult per room
                            $httpData->setParameterGet('rooms['. $a .']['. $i .'][paxType]', 'Adult');
                        }
                        //generating child calculation
                        if($postData['child-select-' . $a] > 0){
                            $k = 0;
                            for($j = $i; $j < ($i + $postData['child-select-' . $a]); $j++){ //this is for loop for child per room
                                $httpData->setParameterGet('rooms['. $a .']['. $j .'][paxType]', 'Child');
                                $url .= 'rooms['. $a .']['. $j .'][paxType]=Child&';
                                $httpData->setParameterGet('rooms['. $a .']['. $j .'][age]', $postData['child-age-' . $a][$k]);
                                $url .= 'rooms['. $a .']['. $j .'][age]=' . $postData['child-age-' . $a][$k] . '&';
                                if(($k+1) == $postData['child-select-' . $a]){
                                    break;
                                }
                                $k++;
                            }
                        }
                    
                        
                                }
                    if($postData['adult-select-' . $a] == 0){
                        break;
                    }
                }
                
            } catch(Exception $e){
                echo $e->getMessage();
            }
            
            $httpData->setParameterGet('filters[0][filterType]', 'hotelStar');
            $httpData->setParameterGet('filters[0][filterValue]', $postData['rating']); //$postData['rating']
            $httpData->setParameterGet('filters[1][filterType]', 'resultLimit');
            $httpData->setParameterGet('filters[1][filterValue]', 20);
            $httpData->setHeaders(array(
                'Accept' => 'application/json',
                'Accept-encoding' => 'gzip,deflate',
                'X-Powered-By' => $siteName));
            
            $resultSet = $httpData->request('GET');
            $responseValue = $resultSet->getBody();
            $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);

            
            $hotels = $responseValue_arr['availableHotels'];
            $hotelsproDB = new Hotels_Model_DbTable_Hotelspro();
            $hotelInfoDB = array();
            
            for($x = 0; $x < count($hotels); $x++){
                $hotelCode = $hotels[$x]['hotelCode'];
                $hotelData = $hotelsproDB->getHotelsproInfo($hotelCode);
                if(!isset($hotelData['name']) OR $hotelData['name'] == ''){
                    $hotelData['name'] = 'UNKNOWN';
                }
                if(!isset($hotelData['image']) OR $hotelData['image'] == ''){
                    $hotelData['image'] = 'data/frontImages/hotels/no_image.png';
                }
                if(!isset($hotelData['address']) OR $hotelData['address'] == ''){
                    $hotelData['address'] = 'UNKNOWN ADDRESS';
                }
                $responseValue_arr['availableHotels'][$x]['hotelInfoDb'] = $hotelData;
                $responseValue_arr['clientCountry'] = $postData['country'];//
            }
            
            $msg = $this->view->translator->translator("hotels_hotelspro_success");
            $json_arr = array('status' => 'ok', 'msg' => $msg, "dataArr" => $responseValue_arr);
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);

            /*******************************************After Post Start*************************************************************/
            
        } else {
            
            $xmlDestination = simplexml_load_file("http://www.hotelspro.com/xf_3.0/downloads/PM4090destinations.xml");
            $arrDestinations = $this->xml2array($xmlDestination);
            $destinations = $arrDestinations['Destinations'][0]['Destination'];
            $this->view->assign('destinations', $destinations);
        }

    }

    public function detailsAction() {
        $hotel_code = $this->getRequest()->getParam("hotel_code");
        $search_id = $this->getRequest()->getParam("search_id");
        $country = $this->getRequest()->getParam("country");
        $condition = $this->getRequest()->getParam("condition");
        $obj = new Hotels_Model_DbTable_Hotelspro();
        $info = $obj->getHotelsproInfo($hotel_code);
        $this->view->info = $info;
        $this->view->search_id = $search_id;
        $this->view->country = $country;
        $this->view->condition = $condition;
        
        /*Calling API for allocateHotelCode method*/
        $httpData = new Zend_Http_Client(null, array(
            'timeout' => 60
        ));

        $httpData->setUri($this->_api_settings->getApiUri());
        $httpData->setParameterGet('method', 'allocateHotelCode');
        $httpData->setParameterGet('apiKey', $this->_api_settings->getApiKey());
        $httpData->setParameterGet('searchId', $search_id);
        $httpData->setParameterGet('hotelCode', $hotel_code);
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));
        $resultSet = $httpData->request('GET');

        $responseValue = $resultSet->getBody();
        $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
        $this->view->view_datas = $responseValue_arr;
        /*Calling API for allocateHotelCode method*/
        
        
        
        /*Calling API for getHotelCancellationPolicy method*/
        $httpData = new Zend_Http_Client(null, array(
            'timeout' => 60
        ));

        $httpData->setUri($this->_api_settings->getApiUri());
        $httpData->setParameterGet('method', 'getHotelCancellationPolicy');
        $httpData->setParameterGet('apiKey', $this->_api_settings->getApiKey());
        $httpData->setParameterGet('trackingId', $responseValue_arr['availableHotels'][0]['processId']);
        $httpData->setParameterGet('hotelCode', $hotel_code);
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));
        $resultSet = $httpData->request('GET');

        $policyValue = $resultSet->getBody();
        if(is_object($policyValue)){
            $policyValue_arr = Zend_Json::decode($policyValue, Zend_Json::TYPE_ARRAY);
        } else {
            $policyValue_arr = array();;
        }
        
        $this->view->cancelPolicy = $policyValue_arr;
        /*Calling API for allocateHotelCode method*/
    }

    
    
     public function reservationAction() {
        $hotel_code = $this->getRequest()->getParam("hotel_code");
        $search_id = $this->getRequest()->getParam("search_id");
        $process_id = $this->getRequest()->getParam("process_id");
        $room_data = $this->getRequest()->getParam("room_data");
        $country = $this->getRequest()->getParam("country");
        $room_data = base64_decode($room_data);
        $room_data = Zend_Json_Decoder::decode($room_data);
        $hotelsProObj = new Hotels_Model_DbTable_Hotelspro();
        $hotelInfo = $hotelsProObj->getHotelsproInfo($hotel_code);
        $currency = $this->_api_settings->getCurrency();
        
        $this->view->hotelInfo = $hotelInfo; 
        $this->view->roomData = $room_data; 
        $this->view->currency = $currency; 
        $this->view->hotel_code = $hotel_code; 
        $this->view->search_id = $search_id; 
        $this->view->process_id = $process_id; 
        $this->view->country = $country; 
        $this->view->room_data = $this->getRequest()->getParam("room_data"); 
        
        if ($this->_request->isPost()) {
            
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            
            //getting values from array
            $postInfo = $this->_request->getPost();
            $otherInfo = $postInfo['otherInfo'];
            $otherInfo = explode('/', $otherInfo);
            $hotelCode = $otherInfo[0];
            $processID = $otherInfo[2];
            $searchID = $otherInfo[1];
            $roomData = $otherInfo[3];
            $clientCountry = $otherInfo[4];
            $specialNote = $otherInfo[5];
            
            //getting database record from values
            $roomData = base64_decode($roomData);
            $roomData = Zend_Json_Decoder::decode($roomData);
            $hotelsProObj = new Hotels_Model_DbTable_Hotelspro();
            $hotelInfo = $hotelsProObj->getHotelsproInfo($hotelCode);
            $currency = $this->_api_settings->getCurrency();
            
            //getting pax details info
            unset($postInfo['otherInfo']);
            $paxInfo = $postInfo;
//            print_r($otherInfo);
            
            
            //generating array for invoice
            $hotelInfoArray = array();
            $hotelInfoArray[0]['hotelID'] = $hotelInfo['hotel_code'];
            $hotelInfoArray[0]['hotelName'] = $hotelInfo['name'];
            $hotelInfoArray[0]['hotelImg'] = $hotelInfo['image'];
            $hotelInfoArray[0]['destination'] = $hotelInfo['dest'];
            $hotelInfoArray[0]['searchID'] = $searchID;
            $hotelInfoArray[0]['processID'] = $processID;
            $hotelInfoArray[0]['clientCountry'] = $clientCountry;
            $hotelInfoArray[0]['totalPrice'] = $roomData['totalPrice'];
            $hotelInfoArray[0]['totalSalePrice'] = $roomData['totalSalePrice'];
            $hotelInfoArray[0]['currency'] = $roomData['currency'];
            $hotelInfoArray[0]['roomCategory'] = $roomData['rooms'][0]['roomCategory'];
            $hotelInfoArray[0]['paxType'] = $roomData['rooms'][0]['paxes'];
            $hotelInfoArray[0]['paxInfo'] = $paxInfo;
            $hotelInfoArray[0]['specialNote'] = $specialNote;
            $hotelInfoArray[0]['ratesPerNight'] = $roomData['rooms'][0]['ratesPerNight'];

            $json_arr = array('status' => 'err');
            if (!empty($hotelInfoArray)) {
                $invoice_arr = ($hotelInfoArray && $hotelInfoArray[0]) ? $this->generateCartInvoice($hotelInfoArray) : '';
                if ($invoice_arr['status'] == 'ok') {
                    Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];
                    $json_arr = array('status' => 'ok');
                }
            }
            
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);

        }
        
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            foreach ($globalIdentity as $globalIdentity_key => $globalIdentity_value) {
                $request_array[$globalIdentity_key] = $globalIdentity_value;
            }
        }
        $request_array["eMail"] = $globalIdentity->username;
        $this->view->view_datas = $request_array;
        $this->view->mem_info = $request_array;
        $this->assignLogin();
        $this->view->logindetails = new Members_Form_LoginForm();
        $this->dynamicUploaderSettings($this->view->form_info);
    }
    
    
    

    private function assignLogin() {
        $template_id_field = 'role_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule('Hotels');
        if ($template_info) {
            $selected_role_id = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator('hotels_invoice_register_role_id');
        } else {
            $selected_role_id = $this->_translator->translator('hotels_invoice_register_role_id');
        }
        if (!empty($selected_role_id)) {
            try {
                $role_db = new Members_Model_DbTable_Role();
                $role_info = $role_db->getRoleInfo($selected_role_id);
                $form_db = new Members_Model_DbTable_Forms();
                $form_info = $form_db->getFormsInfo($role_info['form_id']);
                $this->view->form_info = $form_info;
                $this->view->role_info = $role_info;
            } catch (Exception $e) {
                $role_info = null;
            }
        } else {
            $role_info = null;
        }
        if ($role_info['allow_register_to_this_role'] == '1' && $role_info != null) {
            $registrationForm = new Members_Form_UserForm($role_info);
            if ($registrationForm->role_id) {
                $registrationForm->removeElement('role_id');
            }
            $loginUrl = $this->view->serverUrl() . $this->view->baseUrl() . '/Members-Login';
            $this->view->selected_role_id = $selected_role_id;
            $this->view->registrationForm = $registrationForm;
        } else {
            throw new Exception("Register page not found");
        }
    }

    
    
    
    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json_Encoder::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }
       
    
    private function generateCartInvoice($cart_result) {
        $hotelspro_db = new Hotels_Model_DbTable_Hotelspro();
        $country_db = new Eicra_Model_DbTable_Country();
        $payment_db = new Paymentgateway_Model_DbTable_Gateway();
        $payment_info = $payment_db->getDefaultGateway();
        $auth = Zend_Auth::getInstance();

        $global_conf = Zend_Registry::get('global_conf');
        $marchent = new Invoice_Controller_Helper_Marchent($global_conf);
        $currency = new Zend_Currency($global_conf['default_locale']);
        $currencySymbol = $currency->getSymbol();
        $currencyShortName = $currency->getShortName();

        if ($auth->hasIdentity()) {

            $globalIdentity = $auth->getIdentity();
            $user_id = $globalIdentity->user_id;
            foreach ($globalIdentity as $globalIdentity_key => $globalIdentity_value) {
                $mem_info[$globalIdentity_key] = $globalIdentity_value;
                $invoice_arr[$globalIdentity_key] = $globalIdentity_value;
            }
            if ($mem_info['country']) {
                $country_info = $country_db->getInfo($mem_info['country']);
                $mem_info['country_name'] = stripslashes($country_info['value']);
            }

            //invoice_arr assigning started
            $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID] = $global_conf['payment_user_id'];
            $invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']) . ' ' . stripslashes($mem_info['firstName']) . ' ' . stripslashes($mem_info['lastName']) . '<br />' . stripslashes($mem_info['mobile']) . '<br />' . stripslashes($mem_info['city']) . ', ' . stripslashes($mem_info['state']) . ', ' . stripslashes($mem_info['postalCode']) . '<br />' . stripslashes($country_info['value']) . '<BR />' . stripslashes($mem_info['username']);
            $invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
            $invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
            $invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = date("Y-m-d h:i:s");
            $invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y", strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
            $invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
            $invoice_arr['site_name'] = stripslashes($global_conf['site_name']);
            $invoice_arr['site_url'] = stripslashes($global_conf['site_url']);
            $this->view->mem_info = $mem_info;
        }


        //invoice_items_arr assigning started
        $total_amount = 0;
        $total_tax = 0;
        $invoice_items_arr = array();
        
        
        
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
            <tbody>
                <tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
                    <td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center">
                        <strong>Description</strong>
                    </td>
                    <td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center">
                        <strong>Amount</strong>
                    </td>		
                </tr>';
        
        $item_arr = 0;
        $marchent_email_id_arr = array();
        
        foreach ($cart_result as $cart_result_key => $dataInfo) {
            if ($dataInfo) {
                
                if($dataInfo['hotelImg'] == ""){
                    $hotelImg = "data/frontImages/hotels/no_image.png";
                } else {
                    $hotelImg = $dataInfo['hotelImg'];
                }
                
                if($dataInfo['hotelName'] == ""){
                    $hotelName = $this->_translator->translator('hotels_hotelspro_unknown_hotel');
                } else {
                    $hotelName = $dataInfo['hotelName'];
                }
                $dataInfo[Eicra_File_Constants::MAIN_TABLE_ID] = $dataInfo['hotelID'];
                $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] = Zend_Json_Encoder::encode($dataInfo);
                
                $item_details = '<tr>
                        <td colspan="2" style="height:8px"></td>
                    </tr>
                    <tr>
                        <td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
                            <strong>' . stripslashes($hotelName) . '</strong>
                        </td>
                    </tr>';
                    
                $invoice_items_arr[$item_arr]['inv']['name'] = ($dataInfo['item']['table_name'] == 'autos_page') ? $this->view->escape(($dataInfo['item']['autos_model_name'])) : $this->view->escape(($dataInfo['item']['name']));
                $item_total = 0;

                $sub_total = $this->view->price($dataInfo['totalSalePrice']);
                
                $item_details .= '<tr>
                        <td colspan="2" style="height:2px"></td>
                    </tr>
                    <tr>
                        <td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="' . $hotelImg . '" height="60" title="' . stripslashes($hotelName) . '" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;">
                                            <br>
                                            <span style="color:#06C; line-height:20px; font-weight:bold;">' . $this->_translator->translator('hotels_front_page_list_head_room_type') . ' : </span>' . stripslashes($dataInfo['roomCategory']) . '
                                            <br>
                                            <span style="color:#06C; line-height:20px; font-weight:bold;">' . $this->_translator->translator('hotels_hotelspro_destination') . ' : </span>' . stripslashes($dataInfo['destination']) . '

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6">
                            <table border="0" width="100%">
                                <tbody>';

                                if (is_array($dataInfo['ratesPerNight'])) :
                                    foreach ($dataInfo['ratesPerNight'] AS $rate): 
                                        $item_details .= '<tr><td>Rate for ' . date("d M", strtotime($rate['date'])) . ' is <strong>' . $currencySymbol . ' ' . number_format($rate['amount'], 2) . $currencyShortName . '</strong></td></tr>';
                                    endforeach; 
                                endif; //if (is_array($dataInfo['ratesPerNight'])) : 

                                $item_details .= '</tbody>
                            </table>
                        </td>
                    </tr>';
                                
            $invoice_items_arr[$item_arr]['inv']['quantity'] = 1;
            $invoice_items_arr[$item_arr]['inv']['original_price'] = $dataInfo['item']['original_price'];
            $invoice_items_arr[$item_arr]['inv']['price'] = $dataInfo['totalSalePrice'];
            $invoice_items_arr[$item_arr]['inv']['sub_total'] = $dataInfo['totalPrice'];

            $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $item_total;
            $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
            $total_tax = 0;
            $total_amount += $item_total;
            $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;
            $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
            $item_arr++;                    
                                
            }
        }

//echo $item_details;
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
					<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
					<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">' . $this->_translator->translator("hotels_invoice_total_title") . '&nbsp;</div>
					</td>
					<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>' . $currencySymbol . ($dataInfo['totalSalePrice']) . $currencyShortName . '</strong></td>
				</tr>';
        
        $invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
                                                            </table>';        
        

        $services_charge = 0;

        $now_payable = $dataInfo['totalSalePrice'];
        $invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $dataInfo['totalSalePrice'];
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $dataInfo['totalSalePrice'];
        $invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;
        $invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
        $invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format((($now_payable + $total_tax + $services_charge)), 2, '.', ',') . ' ' . $currencyShortName;
        //Initialize Invoice Action
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] = 'Hotels';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] = 'createHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] = 'updateHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] = 'deleteHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] = 'queryHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] = 'paidHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] = 'unpaidHotelspro';
        $invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] = 'cancelHotelspro';

        //Initialize Email Template
        $template_id_field = 'default_template_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
        if ($template_info) {
            $invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("autos_invoice_template_id");

            $templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
            $invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
            $invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
        } else {
            $invoice_arr[Eicra_File_Constants::INVOICE_DESC] = '<div class="infoMess">' . $this->_translator->translator('invoice_no_invoice_template_found', '', 'Invoice') . '</div>';
            $invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = $this->_translator->translator('invoice_no_invoice_template_found', '', 'Invoice');
        }
        $invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;


        $return = array('status' => 'ok', 'invoice_arr' => $invoice_arr);

        return $return;
    }
    
    
    public function getLetterBody($datas, $letter_id = null) {
        //DB Connection
        $conn = Zend_Registry::get('msqli_connection');
        $conn->getConnection();
        if (empty($letter_id)) {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.templates_page = ?', 'invoice_template')
                    ->limit(1);
        } else {
            $select = $conn->select()
                    ->from(array('nt' => Zend_Registry::get('dbPrefix') . 'newsletter_templates'), array('*'))
                    ->where('nt.id = ?', $letter_id)
                    ->limit(1);
        }

        $rs = $select->query()->fetchAll();
        if ($rs) {
            foreach ($rs as $row) {
                $templates_arr = $row;
            }
        } else {
            $templates_arr['templates_desc'] = '';
        }
        foreach ($datas as $key => $value) {
            $templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? str_replace('%' . $key . '%', $value, $templates_arr['templates_desc']) : str_replace('%' . $key . '%', '&nbsp;', $templates_arr['templates_desc']);
            $templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? str_replace('%' . $key . '%', $value, $templates_arr['templates_title']) : str_replace('%' . $key . '%', '&nbsp;', $templates_arr['templates_title']);
        }
        return $templates_arr;
    }

}

?>