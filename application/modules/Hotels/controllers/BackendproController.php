<?php
class Hotels_BackendproController extends Zend_Controller_Action
{
	private $ProductForm;
	private $uploadForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;	
	
    public function init()
    {				
        /* Initialize action controller here */
		$this->ProductForm =  new Hotels_Form_ProductForm ();	
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();	
    }
	
	public function preDispatch() 
	{		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');		
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{	
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($getModule);
		}	
	}
	
	//PROPERTY LIST FUNCTION

    public function listAction()
    {				
		$group_id =	$this->_request->getParam('group_id');
		$this->view->group_id = $group_id;
		$approve = $this->getRequest()->getParam('approve');	
		$this->view->approve = $approve;
		$category_id = $this->getRequest()->getParam('category_id');
		$this->view->category_id = $category_id;
		
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		$group_info	=	($group_id) ? $group_db->getGroupName($group_id) : null;
		$this->view->group_info = $group_info;
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				if($group_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_request->getParam('group_id'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}
				if($category_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $this->_request->getParam('category_id'));
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				}	
				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'group_id'	=> $group_id, 'category_id'	=> $category_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_HotelsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_hotels_name'] = str_replace('_', '-', $entry_arr['hotels_name']);
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);								
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		
		$business_type_db = new Hotels_Model_DbTable_BusinessType();
		$this->view->assign('type_info', $business_type_db->getOptions($group_id));
		
		$cat_Info = new Hotels_Model_DbTable_Category();
		$this->view->assign('cat_data', $cat_Info->getOptions($group_id)); 
		
		$country_db = new Hotels_Model_DbTable_Country();
		$this->view->assign('country_data', $country_db->getCountryInfo());
		
		$mem_db = new Members_Model_DbTable_MemberList();
		$this->view->assign('mem_data', $mem_db->getAllMembers());	
		
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('distanceFromAirport_info', $hotels_db->getDistanceFromAirport());
    }	
	
	public function savedAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Hotels_Model_SavedHotelsListMapper();	
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_hotels_name'] = str_replace('_', '-', $entry_arr['hotels_name']);
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);								
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
    }	
	
	//PROPERTY FUNCTIONS	
	
	
	public function uploadfileAction()
	{
		$theme = Zend_Registry::get('jtheme');
		$this->view->theme = $theme;
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		$group_id = $this->_getParam('group_id', 0);
		$rel = $this->_getParam('rel', 0);
		$file_content = ($this->_getParam('file_content', 0)) ? $this->_getParam('file_content', 0) : null ;
				
		//Put Group Information in the form
		$group_info = new Hotels_Model_DbTable_HotelsGroup();
		$option = $group_info->getGroupName($group_id);		
		$this->uploadForm =  new Hotels_Form_UploadForm($option);
		
		switch($rel)
		{
			case 'general_plan':
					$path = 'data/frontImages/hotels/hotels_image';
				break;
			case 'interior_plan':
					$path = 'data/frontImages/hotels/interior_plan_image';
				break;
		}
				
		if(!empty($group_id))
		{
			if ($this->_request->getPost()) 
			{
				$this->_helper->viewRenderer->setNoRender();
				
				if($this->uploadForm->isValid($this->_request->getPost())) 
				{				
					Members_Controller_Helper_FileRename::fileRename($this->uploadForm->upload_file, $path);
					$Filename = $this->uploadForm->upload_file->getFileName();
					$ext = Eicra_File_Utility::GetExtension($Filename);
					$file_obj = new Hotels_Controller_Helper_File($path,$Filename,$ext,$option);
										
					if($file_obj->checkCategoryThumbExt())
					{				
						if($this->uploadForm->upload_file->receive())
						{
							$msg = $translator->translator('File_upload_success');
							$json_arr = array('status' => 'ok','msg' => $msg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));						
						}
						else
						{
							$validatorMsg = $this->uploadForm->upload_file->getMessages();
							$vMsg = implode("\n", $validatorMsg);	
							$json_arr = array('status' => 'err','msg' => $vMsg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
						}
					}
					else
					{
						$msg = $translator->translator('File_upload_ext_err',$ext);
						$json_arr = array('status' => 'err','msg' => $msg, 'rel' => $rel, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
					}
				}
				else
				{
					$validatorMsg = $this->uploadForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'err','msg' => $vMsg, 'rel' => $rel);	
				}
				//Convert To JSON ARRAY	
				$res_value = Zend_Json_Encoder::encode($json_arr);	
				$this->_response->setBody($res_value);						
			}	
			else
			{	
				$this->view->rel			=	$rel;
				$this->view->file_content	=	$file_content;
				$this->view->group_id		=	$group_id;
				$this->view->group_info		=	$option;
				$this->view->upload_path	=	$path;
				$this->view->uploadForm 	= 	$this->uploadForm;
			}	
		}	
	}
	
	public function deletefileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		$this->view->translator	= $translator;
		
		if ($this->_request->isPost()) 
		{
			$file_info = $this->_request->getPost('file_info');
			if(empty($file_info))
			{
				$msg = $translator->translator("insert_selected_file_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			else
			{
				$each_file_arr = explode('; ',$file_info);
				$deleted_file_name = '';
				foreach($each_file_arr as $key => $each_file)
				{
					$file_info_arr = explode(',',$each_file);
					if($file_info_arr[1])
					{				
						$dir = $file_info_arr[0].DS.$file_info_arr[1];
						$res = Eicra_File_Utility::deleteRescursiveDir($dir);
					}
					
					if($res)
					{
						$deleted_file_name .= $file_info_arr[1].', ';
						$msg = $translator->translator("file_delete_success",$deleted_file_name);
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("file_delete_err",$file_info_arr[1]);
						$json_arr = array('status' => 'err','msg' => $msg);
						break;
					}
				}
			}
		}
		else
		{
			$msg = $translator->translator("file_delete_err");
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}	
	
	public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Geo_Model_DbTable_State();			
        	$states_options = $states->getSelectOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Geo_Model_DbTable_City();			
        	$cities_options = $cities->getSelectOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function hotelsgroupAction()
	{		
		if ($this->_request->isPost()) 
		{
			try
			{
				$this->_helper->viewRenderer->setNoRender();
				$this->_helper->layout->disableLayout();
				$translator = $this->translator;
			
				$group_id = $this->_request->getPost('grp_id');
				$parant = $this->_request->getPost('id');
				$expanded = ($this->_request->getPost('expanded')) ? $this->_request->getPost('expanded') : false;				
						
				$group_db = new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($group_id);
				
				//$hotelsGroup = Hotels_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,null);
				$businessType = new Hotels_Model_DbTable_BusinessType();	
				$businessType_options = $businessType->getOptions($group_id);				
				
				$dynamic_field_arr = $this->getDynamicfield($group_id);
				$param_fields = array(
									'table_name' => 'hotels_group', 
									'primary_id_field'	=>	'id', 
									'primary_id_field_value'	=>	$group_info['id'],
									'file_path_field'	=>	'', 
									'file_extension_field'	=>	'file_type', 
									'file_max_size_field'	=>	'file_size_max'
							);
							
				$treeDataSource = Hotels_View_Helper_Categorytree::getTreeDataSource($parant,$this->view,$group_id,null,$expanded);
				
				if($treeDataSource)
				{			
					$json_arr = array('status' => 'ok','hotelsGroup' => $hotelsGroup, 'TreeDataSource' => $treeDataSource,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
				}
				else
				{
					$msg = $translator->translator('hotels_group_err');	
					$json_arr = array('status' => 'err','msg' => $msg,'businessType_options' => $businessType_options, 'dynamic_field_arr' => $dynamic_field_arr, 'group_info' => $group_info, 'param_fields'	=> $param_fields);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}	
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	private function getDynamicfield($group_id)
	{
		try
		{
			$translator = Zend_Registry::get('translator');
			$group_info = new Hotels_Model_DbTable_HotelsGroup();
			$option = $group_info->getGroupName($group_id);
			if($option['dynamic_form'])
			{
				$option['form_id'] = $option['dynamic_form'];
				$this->ProductForm = new Hotels_Form_ProductForm ($option);
				$groupsObj = $this->ProductForm->getDisplayGroups();				
				if(!empty($groupsObj))
				{
					$dynamic_field_obj = array();	
					$group_key	= 0;			
					foreach($groupsObj as $group)
					{
						$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_TITLE] = $translator->translator($group->getAttrib('title'));
						$elementsObj =  $group->getElements();
						if($elementsObj)
						{
							$element_key = 0;
							foreach($elementsObj as  $element)
							{
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['name']		=	$element->getName();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['value']		=	$element->getValue();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['label']		=	$element->getLabel();
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['required']	=	($element->isRequired()) ? true : false;
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['type']		= 	strtolower(str_replace('Zend_Form_Element_', '', $element->getType()));
																
								$elements_attributes = $element->getAttribs();
								if($elements_attributes)
								{
									foreach($elements_attributes as $attributes_key => $attribute_value)
									{
										$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key][$attributes_key]		=	$attribute_value;
									}
								}
								$dynamic_field_obj[$group_key][Eicra_File_Constants::DYNAMIC_FIELD_GROUP_ELEMENTS][$element_key]['element']		=		$element->render();
								$element_key++;								
							}
						}	
						$group_key++;					
					}
				}
				$dynamic_field_arr = array('status' => 'ok', 'dynamic_fields' => $dynamic_field_obj, 'form_info' => $this->ProductForm->getFormInfo());
			}
			else
			{
				$dynamic_field_arr = array('status' => 'err', 'dynamic_fields' => null);
			}
		}
		catch(Exception $e)
		{
			$dynamic_field_arr = array('status' => 'err', 'msg' => $e->getMessage(), 'dynamic_fields' => null);
		}
		return $dynamic_field_arr;
	}
	
	public function addAction()
	{			
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			$this->view->translator	= $translator;
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			try
			{
				//Checking Membership Package Option
				$packages = new Members_Controller_Helper_Packages();	
				if($packages->isPackage())
				{
					$module = $this->_request->getModuleName();
					$controller = $this->_request->getControllerName();
					$action = $this->_request->getActionName();
					if($packages->isPackageField($module,$controller,$action))
					{
						$package_no = $packages->getPackageFieldValue($module,$controller,$action);				
					}
					else
					{
						$package_no = 0;
					}
				}
				else
				{
					$package_no = 0;
				}	
				
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$p_num = $hotels_db->numOfHotels();
				if(empty($package_no) || ($p_num < $package_no))
				{
					$group_id = $this->_request->getPost('group_id');
					if(!empty($group_id))
					{						
						$group_info = new Hotels_Model_DbTable_HotelsGroup();
						$option = $group_info->getGroupName($group_id);
						if($option['dynamic_form'])
						{
							$option['form_id'] = $option['dynamic_form'];
							$this->ProductForm = new Hotels_Form_ProductForm ($option);
							$field_db = new Members_Model_DbTable_Fields();
							$field_groups = $field_db->getGroupNames($option['form_id']); 
							foreach($field_groups as $group)
							{						
								$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
								$elements = $displaGroup->getElements();
								foreach($elements as $element)
								{
									$group_arr[$element->getName()] = $element->getName();
									if($element->getType() == 'Zend_Form_Element_File')
									{
										$element_name = $element->getName();
										$required = ($element->isRequired()) ? true : false;
										$element_info = $element;
										$this->ProductForm->removeElement($element_name);
										$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
									}
									$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group, 'legend' => $group->field_group));
								}
							}				
						}
					}
					if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
					}
					if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
					}
					else
					{
						$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
					}
					if($this->ProductForm->isValid($this->_request->getPost())) 
					{												
						$hotels = new Hotels_Model_Hotels($this->ProductForm->getValues());
						$auth = Zend_Auth::getInstance ();
						$stat = ($auth->hasIdentity () && $auth->getIdentity ()->auto_publish_article == '1') ? '1' : '0';	
						$hotels->setActive($stat); 
						$hotels->setFeatured('0');	
						$hotels->setHotels_primary_image($this->_request->getPost('hotels_primary_image'));
						$hotels->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));						
						$hotels->setRelated_items($this->_request->getPost('related_items'));
						$hotels->setRoom_type_id($this->_request->getPost('room_type_id'));
						$hotels->setFeature_facilities($this->_request->getPost('feature_facilities'));
						$hotels->setFeature_sports_recreations($this->_request->getPost('feature_sports_recreations'));										
						
						$perm = new Hotels_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $hotels->saveHotels();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{									
									$result2 = $hotels->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}												
						}
						else
						{
							$Msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $Msg);
						}									
					}
					else
					{
						$validatorMsg = $this->ProductForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg);
					}	
				}
				else
				{
					$msg = $translator->translator("hotels_add_limit_err",$package_no);
					$json_arr = array('status' => 'err','msg' => $msg);
				}					
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{						
			$this->view->proForm = $this->ProductForm;			
		}	
	}	
	
	public function editAction()
	{
		$id = $this->_getParam('id', 0);
		$translator = $this->translator;
				
		//Get Hotels Info
		$hotelsData = new Hotels_Model_DbTable_Hotels();		
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			
			$id = $this->_request->getPost('id');
			if(!empty($id))
			{
				$hotelsInfo = $hotelsData->getHotelsInfo($id);
				$group_info = new Hotels_Model_DbTable_HotelsGroup();
				$option = $group_info->getGroupName($hotelsInfo['group_id']);
				if($option['dynamic_form'])
				{
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Hotels_Form_ProductForm ($option);
					$field_db = new Members_Model_DbTable_Fields();
					$field_groups = $field_db->getGroupNames($option['form_id']); 
					foreach($field_groups as $group)
					{						
						$displaGroup = $this->ProductForm->getDisplayGroup($group->field_group);
						$elements = $displaGroup->getElements();
						foreach($elements as $element)
						{
							$group_arr[$element->getName()] = $element->getName();
							if($element->getType() == 'Zend_Form_Element_File')
							{
								$element_name = $element->getName();
								$required = ($element->isRequired()) ? true : false;
								$element_info = $element;
								$this->ProductForm->removeElement($element_name);
								$this->ProductForm->addElement('hidden', $element_name, array('label' => $element_info->getLabel(),'id' => $element_info->getAttrib('id'), 'class' => $element_info->getAttrib('class'), 'title' => $element_info->getAttrib('title'), 'size' => $element_info->getAttrib('title'), 'rel' => $element_info->getAttrib('rel'), 'admin' => $element_info->getAttrib('admin'), 'frontend' => $element_info->getAttrib('frontend'), 'info' => $element_info->getAttrib('info'), 'required'    => $required));
							}
							$this->ProductForm->addDisplayGroup($group_arr, $group->field_group,array('disableLoadDefaultDecorators' => true,'title' => $group->field_group));
						}
					}				
				}
			}
			
			
			try
			{
				if($this->_request->getPost('billing_user_places_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_places_order_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_pay_invoice_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_pay_invoice_email_address->setRequired(false);
				}
				if($this->_request->getPost('billing_user_cancel_order_email_enable') == 'yes')
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(true);
				}
				else
				{
					$this->ProductForm->billing_user_cancel_order_email_address->setRequired(false);
				}
				if($this->ProductForm->isValid($this->_request->getPost())) 
				{										
					if(!empty($id))
					{					
						$hotels = new Hotels_Model_Hotels($this->ProductForm->getValues());
						$hotels->setPrev_category($hotelsInfo['category_id']);
						$hotels->setPrev_group($hotelsInfo['group_id']);
						$hotels->setId($id);
						$hotels->setHotels_primary_image($this->_request->getPost('hotels_primary_image'));
						$hotels->setFeature_primary_interior_image($this->_request->getPost('feature_primary_interior_image'));					
						$hotels->setHotels_title($this->ProductForm->getValue('hotels_title'),$id);						
						$hotels->setRelated_items($this->_request->getPost('related_items'));
						$hotels->setRoom_type_id($this->_request->getPost('room_type_id'));
						$hotels->setFeature_facilities($this->_request->getPost('feature_facilities'));
						$hotels->setFeature_sports_recreations($this->_request->getPost('feature_sports_recreations'));									
						
						$perm = new Hotels_View_Helper_Allow();
						if($perm->allow())
						{
							$result = $hotels->saveHotels();
							if($result['status'] == 'ok')
							{
								$msg = $translator->translator("page_save_success");
								$json_arr = array('status' => 'ok','msg' => $msg);
								if($option['dynamic_form'])
								{									
									$result2 = $hotels->saveDynamicForm($option,$this->ProductForm,$result,$this->_request);
									if($result2['status'] == 'ok')
									{
										$msg = $translator->translator("page_save_success");
										$json_arr = array('status' => 'ok','msg' => $msg);
									}
									else
									{
										$msg = $translator->translator("page_save_err");
										$json_arr = array('status' => 'err','msg' => $msg." ".$result2['msg']);
									}	
								}						
							}
							else
							{
								$msg = $translator->translator("page_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
							}						
						}
						else
						{
							$msg =  $translator->translator("page_add_action_deny_desc");
							$json_arr = array('status' => 'errP','msg' => $msg);
						}					
					}
					else
					{
						$msg = $translator->translator("page_save_err");
						$json_arr = array('status' => 'err','msg' => $msg);
					}				
				}
				else
				{
					$validatorMsg = $this->ProductForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errType)
					{					
						foreach($errType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}			
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);		
		}
		else
		{	
			//Get Hotels Info 
			$hotelsInfo = $hotelsData->getHotelsInfo($id);	
			if($hotelsInfo)
			{
				//Put Group Information in the view
				$group_info = new Hotels_Model_DbTable_HotelsGroup();
				$option = $group_info->getGroupName($hotelsInfo['group_id']);
				
				//Dynamic Form
				if(!empty($option['dynamic_form']))
				{   
					$option['form_id'] = $option['dynamic_form'];
					$this->ProductForm = new Hotels_Form_ProductForm ($option);	
						$dynamic_valu_db = new Members_Model_DbTable_FieldsValue();	
					$hotelsInfo	=	$dynamic_valu_db->getFieldsValueInfo($hotelsInfo,$hotelsInfo['id']);					
				}
				
						
				//Get Category Info
				$categoryData = new Hotels_Model_DbTable_Category();	
				$group_id = $hotelsInfo['group_id'];
				
				
				if($hotelsInfo['category_id'] == '0')
				{
					$selected = 'style="background-color:#D7D7D7"';
					$category_name = $translator->translator("hotels_tree_root");
				}
				else
				{
					$categoryInfo = $categoryData->getCategoryInfo($hotelsInfo['category_id']);
					$category_name = $categoryInfo['category_name'];	
					$selected = '';
				}		
				
				
				//ASSIGN CATEGORY TREE
				$this->view->categoryTree = '<table class="example" id="dnd-example">'.
											'<tbody ><tr id="node-0">'.
											 '<td '.$selected.'>'.
												'<span class="folder">'.$translator->translator("hotels_tree_root").'</span>'.
											'</td>'.
										'</tr>'.Hotels_View_Helper_Categorytree::getSubCategory('0',$this->view,$group_id,$hotelsInfo['category_id']).'</tbody></table>';
				
				//ASSIGN PROPERTY TYPE
				$r = true;			
				if(is_numeric($hotelsInfo['hotels_agent']))
				{ 
					if(is_int((int)$hotelsInfo['hotels_agent']))
					{
						$r = false;
					}				
				}
				if($r){ $this->ProductForm->hotels_agent->addMultiOption($hotelsInfo['hotels_agent'],stripslashes($hotelsInfo['hotels_agent'])); }
				
				//ASSIGN PROPERTY TYPE
				$hotelsBusinessType = new Hotels_Model_DbTable_BusinessType();			
				$hotelsBusinessType_options = $hotelsBusinessType->getSelectOptions($hotelsInfo['group_id']);
				$this->ProductForm->hotels_type->addMultiOptions($hotelsBusinessType_options);
				
				//ASSIGN STATE
				$states = new Eicra_Model_DbTable_State();			
				$states_options = $states->getOptions($hotelsInfo['country_id']);			
				$this->ProductForm->state_id->addMultiOptions($states_options);
				
				//ASSIGN AREA / CITY
				$areas = new Eicra_Model_DbTable_City();			
				$areas_options = $areas->getOptions($hotelsInfo['state_id']);			
				$this->ProductForm->area_id->addMultiOptions($areas_options);
				
				//ASSIGN CHECKIN TIME
				$checkin_arr = explode(':',$hotelsInfo['checkin_date']);
				$hotelsInfo['checkin_hour'] = $checkin_arr[0];
				$hotelsInfo['checkin_min'] = $checkin_arr[1];
				
				//ASSIGN CHECKOUT TIME
				$checkout_arr = explode(':',$hotelsInfo['checkout_date']);
				$hotelsInfo['checkout_hour'] = $checkout_arr[0];
				$hotelsInfo['checkout_min'] = $checkout_arr[1];	
				
				//ASSIGN ROOM TYPE
				$this->ProductForm->room_type_id->setIsArray(true); 
				$room_type_id_arr = explode(',',$hotelsInfo['room_type_id']);	
				$hotelsInfo['room_type_id'] = 	$room_type_id_arr;	
				
				//ASSIGN RELETED ITEMS
				$this->ProductForm->related_items->setIsArray(true); 
				$related_items_arr = explode(',',$hotelsInfo['related_items']);	
				$hotelsInfo['related_items'] = 	$related_items_arr;	
				
				//ASSIGN FEATURED FACILITIES
				$this->ProductForm->feature_facilities->setIsArray(true); 
				$feature_facilities_arr = explode(',',$hotelsInfo['feature_facilities']);	
				$hotelsInfo['feature_facilities'] = 	$feature_facilities_arr;	
				
				//ASSIGN FEATURED SPORTS RECREATIONS
				$this->ProductForm->feature_sports_recreations->setIsArray(true); 
				$feature_sports_recreations_arr = explode(',',$hotelsInfo['feature_sports_recreations']);	
				$hotelsInfo['feature_sports_recreations'] = 	$feature_sports_recreations_arr;
						
				$this->view->option =  $option;	
				$this->view->hotelsInfo = $hotelsInfo;					
				$this->ProductForm->populate($hotelsInfo);										
				$this->view->id = $id;
				$this->view->entry_by = $hotelsInfo['entry_by'];
				$this->view->category_name = $category_name;
				$this->view->hotels_primary_image = $hotelsInfo['hotels_primary_image'];
				$this->view->feature_primary_interior_image = $hotelsInfo['feature_primary_interior_image'];			
				$this->view->proForm = $this->ProductForm;	
				$this->view->form_info = $this->ProductForm->getFormInfo ();
				$this->dynamicUploaderSettings($this->view->form_info);	
				$this->userUploaderSettings($hotelsInfo);
				$this->render(); 
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}	
	}	
	
	private function userUploaderSettings($info)
	{
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		$group_info = $group_db->getGroupName($info['group_id']);
		$param_fields = array(
								'table_name' => 'hotels_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_hotels_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));	
		
		/**************************************************For Secondary**************************************************/	
			$secondary_requested_data	=	$requested_data;
			$secondary_requested_data['file_path_field'] = 'file_path_feature_interior_plan_image';			
			
			$this->view->assign('secondary_settings_info' ,	array_merge($secondary_requested_data, $settings_info));		
			$this->view->assign('secondary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->secondary_settings_info));
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$perm = new Hotels_View_Helper_Allow();			
			if ($perm->allow('delete','backendpro','Hotels'))
			{			
				// Remove from Hotels
				try
				{
					$id = $this->_request->getPost('id');			
					$group_id = $this->_request->getPost('group_id');
					
					//page_info
					$proObj = new Hotels_Model_DbTable_Hotels();
					$page_info = $proObj->getHotelsInfo($id);
					
					//Put Group Information in the view
					$group_db = new Hotels_Model_DbTable_HotelsGroup();
					$group_info = $group_db->getGroupName($page_info['group_id']);
					
					$hotels_name = $page_info['hotels_name'];
					$hotels_image = explode(',',$page_info['hotels_image']);
					$feature_interior_plan_image = explode(',',$page_info['feature_interior_plan_image']);			
					
					$hotels_image_path = 'data/frontImages/hotels/hotels_image';
						
					$interior_plan_image_path = 'data/frontImages/hotels/interior_plan_image';
							
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
				
					if(!empty($group_info['dynamic_form']))
					{
						//Delete Files From Folder
						$form_db = new Members_Model_DbTable_Forms(); 
						$fields_db	= new Members_Model_DbTable_Fields(); 
						$field_value_db = new Members_Model_DbTable_FieldsValue();
						
						$form_info = $form_db->getFormsInfo($group_info['dynamic_form']); 						
						$fields_infos = $fields_db->getFieldsInfo($group_info['dynamic_form']);		
						
						
						foreach($fields_infos as $fields)
						{
							if(trim($fields->field_type)	==	'file')
							{
								$field_value_arr = $field_value_db->getFieldsValue($id,$group_info['dynamic_form'],$fields->id);
								$field_value_arr = $field_value_arr->toArray();								
								if(!empty($field_value_arr[0]['field_value']))
								{	
									$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
									foreach($element_value_arr as $key => $e_value)
									{
										if(!empty($e_value))
										{	
											$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
											$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
										}
									}
								}								
							}
						}
					
						$whereD = array();
						$whereD[] = 'form_id = '.$conn->quote($group_info['dynamic_form']);
						$whereD[] = 'table_id = '.$conn->quote($id);
						$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereD);
					}
					
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					
					$conn->delete(Zend_Registry::get('dbPrefix').'hotels_page', $where);
					
					foreach($hotels_image as $key=>$file)
					{
						if($file)
						{
							$dir = $hotels_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('page_list_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('page_list_file_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
					
					foreach($feature_interior_plan_image as $key=>$file)
					{
						if($file)
						{
							$dir = $interior_plan_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('page_list_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('page_list_file_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
				}
				catch (Exception $e) 
				{
					$msg = 	$e->getMessage();			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deletesavedAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$perm = new Hotels_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendpro','Hotels'))
			{
				$id = $this->_request->getPost('id');	
									
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				// Remove from Hotels
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'hotels_saved', $where);
					$msg = 	$translator->translator('hotels_list_saved_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$translator->translator('hotels_list_saved_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg.' '.$e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_list_saved_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Hotels_View_Helper_Allow();			
			if ($perm->allow('delete','backendpro','Hotels'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					//Delete Files From Folder
					$form_db = new Members_Model_DbTable_Forms(); 
					$fields_db	= new Members_Model_DbTable_Fields(); 
					$field_value_db = new Members_Model_DbTable_FieldsValue();
					
					foreach($id_arr as $id)
					{						
						//Get Category Thumbnil
						$select = $conn->select()
									->from(array('gc' => Zend_Registry::get('dbPrefix').'hotels_page'), array('gc.hotels_image','gc.feature_interior_plan_image','gc.group_id'))
									->where('gc.id = ?',$id);
						$rs = $select->query()->fetchAll();
						if($rs)
						{				
							foreach($rs as $row)
							{
								$hotels_image = explode(',',$row['hotels_image']);
								$feature_interior_plan_image = explode(',',$row['feature_interior_plan_image']);
								$group_id	=	$row['group_id'];
							}
						}
												
						
						$hotels_image_path = 'data/frontImages/hotels/hotels_image';
							
						$interior_plan_image_path = 'data/frontImages/hotels/interior_plan_image';
						
						//Put Group Information in the view
						$group_db = new Hotels_Model_DbTable_HotelsGroup();
						$group_info = $group_db->getGroupName($group_id);	
						
						//Remove from Category						
						try
						{
							if(!empty($group_info['dynamic_form']))
							{
								$form_info = $form_db->getFormsInfo($group_info['dynamic_form']); 						
								$fields_infos = $fields_db->getFieldsInfo($group_info['dynamic_form']);		
								
								
								foreach($fields_infos as $fields)
								{
									if(trim($fields->field_type)	==	'file')
									{
										$field_value_arr = $field_value_db->getFieldsValue($id,$group_info['dynamic_form'],$fields->id);
										$field_value_arr = $field_value_arr->toArray();	
										if(!empty($field_value_arr[0]['field_value']))
										{	
											$element_value_arr = explode(',',$field_value_arr[0]['field_value']);	
											foreach($element_value_arr as $key => $e_value)
											{
												if(!empty($e_value))
												{	
													$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$e_value;
													$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
												}
											}
										}
									}
								}
						
								$whereD = array();
								$whereD[] = 'form_id = '.$conn->quote($group_info['dynamic_form']);
								$whereD[] = 'table_id = '.$conn->quote($id);
								$conn->delete(Zend_Registry::get('dbPrefix').'forms_fields_values', $whereD);
							}
							
							$where = array();
							$where[] = 'id = '.$conn->quote($id);
							
							$conn->delete(Zend_Registry::get('dbPrefix').'hotels_page', $where);
							foreach($hotels_image as $key=>$file)
							{
								if($file)
								{
									$dir = $hotels_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								}
								if($res)
								{
									$msg = 	$translator->translator('page_list_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('page_list_file_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}					
							
							foreach($feature_interior_plan_image as $key=>$file)
							{
								if($file)
								{
									$dir = $interior_plan_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
								}
								if($res)
								{
									$msg = 	$translator->translator('page_list_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('page_list_file_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
						}
						catch (Exception $e) 
						{			
							$json_arr = array('status' => 'err','msg' =>$e->getMessage().' '.$id);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deletesavedallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Hotels_View_Helper_Allow();			
			if ($perm->allow('deletesaved','backendpro','Hotels'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
					
					foreach($id_arr as $id)
					{	
						// Remove from Category
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'hotels_saved', $where);
							$msg = 	$translator->translator('page_list_delete_success');			
							$json_arr = array('status' => 'ok','msg' => $msg);	
						}
						catch (Exception $e) 
						{
							$msg = 	$translator->translator('category_list_delete_err');			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
				}
				else
				{
					$msg = $translator->translator("category_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('page_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function isnot_int($hotels_agent)
	{
		$r = true;			
		if(is_numeric($hotels_agent))
		{ 
			if(is_int((int)$hotels_agent))
			{
				$r = false;
			}				
		}
		return $r;
	}
	
	public function publishAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id = $this->_request->getPost('id');
			$hotels_name = $this->_request->getPost('hotels_name');
			$paction = $this->_request->getPost('paction');
			$data = array();
					
			switch($paction)
			{
				case 'publish':
						$active = '1';
						$data = array('active' => $active);
					break;
				case 'unpublish':
						$active = '0';
						$data = array('active' => $active);
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Hotels status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'hotels_page',$data, $where);				
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('page_list_publish_err',$hotels_name);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function publishallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str  = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{	
						$conn->update(Zend_Registry::get('dbPrefix').'hotels_page',array('active' => $active), $where);
						$return = true;
					}
					catch (Exception $e) 
					{
						$return = false;
					}
				}
			
				if($return)
				{			
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('page_list_publish_err');	
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = $translator->translator("page_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
			
	public function featuredAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$hotels_name = $this->_request->getPost('hotels_name');
			$paction = $this->_request->getPost('paction');
			
			switch($paction)
			{
				case 'featured':
					$feature = '1';
					break;
				case 'unfeatured':
					$feature = '0';
					break;
			}
			
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
			 	$conn->update(Zend_Registry::get('dbPrefix').'hotels_page',array('featured' => $feature), $where);
				$return = true;
			}
			catch (Exception $e) 
			{
				$return = false;
			}
			if($return)
			{			
				$json_arr = array('status' => 'ok','featured' => $feature);
			}
			else
			{
				$msg = $translator->translator('page_list_feature_err',$hotels_name);	
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);			
	}
	
	public function upAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		
		$id 	= $this->_request->getPost('id');
		$page_order 	= $this->_request->getPost('page_order');				
				
		$OrderObj = new Hotels_Controller_Helper_HotelsOrders($id);
		$returnV =	$OrderObj->decreaseOrder();
		
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function downAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
		
		$id = $this->_request->getPost('id');
		$page_order = $this->_request->getPost('page_order');
						
		$OrderObj = new Hotels_Controller_Helper_HotelsOrders($id);
		$returnV =	$OrderObj->increaseOrder();
		if($returnV['status'] == 'err')
		{
			$json_arr = array('status' => 'err','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}
		else
		{
			$json_arr = array('status' => 'ok','id_arr' => $returnV['id_arr'], 'msg' => $returnV['msg']);
		}						
		
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function orderallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_str = $this->_request->getPost('id_arr');
			$hotels_order_str = $this->_request->getPost('hotels_order_arr');			
			
			$id_arr = explode(',',$id_str);
			$hotels_order_arr = explode(',',$hotels_order_str);
			
			$order_numbers = new Administrator_Controller_Helper_GlobalOrdersNumbers();
			$checkOrder = $order_numbers->checkOrderNumbers($hotels_order_arr);
			if( $checkOrder['status'] == 'err')
			{
				$json_arr = array('status' => 'err','msg' => $checkOrder['msg']);
			}
			else
			{
				//Save Category Order
				$i = 0;
				foreach($id_arr as $id)
				{
					$OrderObj = new Hotels_Controller_Helper_HotelsOrders($id);
					$OrderObj->saveOrder($hotels_order_arr[$i]);
					$i++;
				}
				$msg = $translator->translator("page_zero_order_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}			
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function searchAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Hotels_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_obj = new Hotels_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function linkroomtypeAction()
	{
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			$id_str	=	$this->_request->getPost('id_str');			
			
			$hotels_db = new Hotels_Model_DbTable_Hotels();
			$result = $hotels_db->linkToRoomType($id, $id_str);
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_room_type_linked_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}
			else
			{				
				$json_arr = array('status' => 'err','msg' => $result['msg']);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
		else
		{
			$id = $this->_request->getParam('id');
			if(!empty($id))
			{
				$roomType_db = new Hotels_Model_DbTable_RoomType();
				$roomType_info = $roomType_db->getOptions(null);
				
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$hotels_info= $hotels_db->getHotelsInfo($id);
				
				$this->view->roomType_info	=	$roomType_info;
				$this->view->hotels_info	=	$hotels_info;
			}
		}
	}	
}

