<?php
class Hotels_SearchController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_group_form_id_field = 'dynamic_form';
	private $_group_table = 'hotels_group';
	private $_static_table = 'hotels_page';
	private $_controllerCache;
	private $_translator;
	private $_auth_obj;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template ();	
		$template_obj->setFrontendTemplate();			
		
		$front_template = Zend_Registry::get('front_template');		
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
		$this->view->front_template = $front_template;
		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = 1;
		}	
			
	}
	
	public function itemsAction()
	{
		$group_id = $this->_page_id;
		if ($this->_request->isPost()) 
		{			
			$postValue = $this->_request->getPost();
			Eicra_Global_Variable::getSession()->hotel_search_info = $postValue;
			foreach($postValue as $post_key => $post_value)
			{
				if($post_value == $this->_translator->translator('common_any_language'))
				{
					$postValue[$post_key] = '';
				}
			}
			$group_id  = ($postValue['group_id_=']) ? $postValue['group_id_='] : $this->_page_id ;
			$search_db = new Settings_Model_DbTable_Search();
			$search_settings_info = $search_db->getInfo($group_id, $this->_group_table, $this->_group_form_id_field, $this->_static_table);
			
			if($search_settings_info)
			{
				$search_obj = new Settings_Controller_Helper_Search($group_id, $this->_group_table, $this->_group_form_id_field, $this->_static_table, $search_settings_info, $postValue);
				$view_datas = $search_obj->search();
				if($view_datas)
				{
					$room_type_db = new Hotels_Model_DbTable_RoomType();
					$room_db = new Hotels_Model_DbTable_Room();
					$today = date('Y-m-d');
					foreach($view_datas as $view_datas_key => $entry)
					{						
						if(!empty($entry['room_type_id']))
						{
							try
							{							
								  $room_type_info = $room_type_db->getAllRoomTypeByIds($entry['room_type_id']);
								  $view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
								  if($room_type_info)
								  {
										foreach($room_type_info as $room_type_info_key => $room_type_info_arr)
										{
											  if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
											  {
												  $room_info = $room_db->getAvailableRooms($room_type_info_arr['id'], $postValue);																	
											  }
											  else
											  {
												   $room_info = $room_db->getAvailableRooms($room_type_info_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
											  }
											  $view_datas[$view_datas_key]['room_type_info'][$room_type_info_key]['room_info'] = $room_info;
										 }	
								  }	
							}
							catch(Exception $e)
							{
																
							}						
						}
					}
				}
				$group_datas = $search_obj->returnGroupInfo();				
				$this->view->assign('group_datas', $group_datas);
				$this->view->assign('view_datas', $view_datas);
				if(!$view_datas)
				{
					$this->view->assign('errorMessage', $this->_translator->translator('hotels_search_settings_not_found'));
				}
			}
			else
			{
				$this->view->assign('errorMessage', $this->_translator->translator('hotels_search_settings_not_found'));
			}			
			$this->view->assign('postValue', $postValue);	
		}
	}
	
	public function searchAction()
	{
		$posted_data	=	$this->_request->getParams();			
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		if($this->_request->isPost())
		{
			Eicra_Global_Variable::getSession()->hotel_search_info = $this->_request->getPost();
			if($posted_data && $posted_data['filter'] && $posted_data['filter']['filters'])
			{
				foreach($posted_data['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}			
		}
		
		if ($this->_request->isPost() && empty($posted_data['block_search'])) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();	
				$device = new Administrator_Controller_Helper_Device();	
				
				if ($preferences_data && $preferences_data['hotels_list_by_search_other_page_sortby'] && $preferences_data['hotels_list_by_search_other_page_sortby'] != '_') 
				{
					$sort_arr = explode('-', $preferences_data['hotels_list_by_search_other_page_sortby']);
					$posted_data['sort'][] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
				}		
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
					
				$getViewPageNum = $preferences_data['hotels_list_by_search_on_other_page']; 
				$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Search-Hotel-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_postValue = Zend_Json_Encoder::encode($postValue);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{	
					$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
					$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
					$review_helper = new Review_View_Helper_Review();
					$room_type_db = new Hotels_Model_DbTable_RoomType();
					$room_db = new Hotels_Model_DbTable_Room();
					
					$today = date('Y-m-d');					
					$list_mapper = new Hotels_Model_HotelsListMapper();	
					$hotel_num = $list_mapper->getDbTable()->numOfHotels(array('field' => 'active', 'operator' => '=', 'value' => '1'), false);
					$tableColumns =  ($hotel_num <= 30) ? array('userChecking' => false, 'controller_obj' => array('view' => $this->view, 'translator' => $this->_translator, 'review_helper'  => $review_helper, 'room_type_db' => $room_type_db, 'room_db' => $room_db, 'preferences_data' => $preferences_data, 'currency' => $this->view->currency, 'postValue' => $postValue, 'vote' => $vote, 'maximum_stars_digit' => $maximum_stars_digit, 'device' => $device)) : array('userChecking' => false);
					$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, $tableColumns);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;	
									
						foreach($list_datas as $entry)
						{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							if($hotel_num > 30)
							{
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
								$entry_arr['hotels_phone_format'] = $this->view->numbers($entry_arr['hotels_phone']);
								$entry_arr['feature_distance_from_airport_format'] = $this->view->numbers($entry_arr['feature_distance_from_airport']);
								$entry_arr['hotels_desc']		=	strip_tags( $entry_arr['hotels_desc']);
								$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
								$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['hotels_image_no_format'] = $this->_translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
								//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
								$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
								
								$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
								$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
								$hotel_stars = '';
								for($i = 1; $i < $maximum_stars_digit; $i++)
								{
									$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
									
								}
								$entry_arr['hotel_stars_format']		= 	$hotel_stars;
								$entry_arr['vote_format']				= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
								
								$entry_arr['total_nights']				=	$list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]);
								$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
								$entry_arr['total_nights_format']		=	$this->view->numbers($entry_arr['total_nights']);
								$entry_arr['total_nights_format_lang']	=	$this->_translator->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
								$entry_arr['room_type_info']			=	null; 
								$entry_arr['adult_info']				=	$postValue['search_adult'];
								$entry_arr['child_info']				=	($postValue['search_child']) ? $postValue['search_child'] : 0;
								
								//Room Type Start
								if(!empty($entry_arr['room_type_id']))
								{	
									try
									{	
										$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
										$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';	
										$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
										if(!empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
										{
											$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																							'filters' => array(																			  
																											 array(
																													'logic' => 'AND',
																													'filters' => array( 
																																		array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																		array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																	)
																												),
																											 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																											)
																						 );	
										}
										$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
										//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
										
										if($room_type_info)
										{
											$room_type_info_key = 0;				
											foreach($room_type_info as $room_type_info_entry)
											{	
												if($list_mapper->searchPrice($room_type_info_entry, $postValue, array('controller_obj' => array('view' => $this->view))))
												{					
													$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
													$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
													$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
													$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
													$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
													$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
													$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
													$room_type_info_entry_arr['max_people_lang_format']	=	$this->_translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
													$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
													$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
													$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
													$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
													$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
													$room_type_info_entry_arr['form_data_view_title_format']	=	$this->_translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
													$save = 0;
													if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
													{
														$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
														$save = round($save);
													}
													$room_type_info_entry_arr['save_price'] = $save;
													$room_type_info_entry_arr['save_format'] = $this->_translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
													$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
													$room_type_info_entry_arr['just_booked'] = $booked;
													
													//Room Info Start
													if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
													{															
														$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
													}
													else
													{
														 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
													}
													$room_type_info_entry_arr['room_info'] = $room_info;
													$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
													if($device->isMobile())
													{
														$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
														$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
													}
													else
													{
														$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_list_available_type_llast', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
														$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_list_available_type_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
													}
													$room_type_info_entry_arr['room_info_count_format2'] = $this->_translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
													$room_type_info_entry_arr['room_info_count_format3'] = $this->_translator->translator('hotels_search_sugges_room_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
													$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
													//Room Info End
													if($room_type_info_entry_arr['show_room_type'] == true)
													{
														$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
														$room_type_info_key++;	
													}
												}
											}
										}	
									}
									catch(Exception $e)
									{
																		
									}
								}		
							}
							//Room Type End
							$entry_arr['booking_suggestion']	=	$this->view->suggestion($postValue, $entry_arr, $this);						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;												
						}
						
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
		
	}
	
		
	public function autosearchAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();		
			$search_params['filter']['logic'] 	=	'OR';
			foreach($postData_arr as $key=>$value)
			{
				$search_params['filter']['filters'][]	=  array( 'field' =>  $key, 'operator' => 'contains', 'value' =>	$value);	
			}
			$hotels_db = new Hotels_Model_DbTable_Hotels();
			$list_datas = $hotels_db->getListInfo('1', $search_params,  array('userChecking' => false));
			if($list_datas)
			{
				$data_result = array();
				foreach($list_datas as $key => $entry)
				{
					$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
					$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
					$entry_arr['hotels_location'] = '<br />'.$this->view->escape($entry_arr['location_for_search']).'<br />'.$this->view->escape($entry_arr['city']).', '.$this->view->escape($entry_arr['state_name']).', '.$this->view->escape($entry_arr['country_name']).'<hr>';
					$data_result[$key]	=	$entry_arr;									
				}
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $data_result);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
		
	
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}
}
