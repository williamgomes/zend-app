<?php
class Hotels_WegoController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_group_form_id_field = 'dynamic_form';
	private $_group_table = 'hotels_group';
	private $_static_table = 'hotels_page';
	private $_controllerCache;
	private $_translator;
	private $_api_settings;
	public function init()
	{
		/* Initialize action controller here */
		$this->_translator = Zend_Registry::get ( 'translator' );
		$this->view->assign ( 'translator', $this->_translator );
		$this->view->setEscape ( 'stripslashes' );
		// DB Connection
		$this->_DBconn = Zend_Registry::get ( 'msqli_connection' );
		$this->_DBconn->getConnection ();
		// Initialize Cache
		$cache = new Eicra_View_Helper_Cache ();
		$this->_controllerCache = $cache->getCache ();
		$this->_api_settings = new Hotels_Model_WegoApiModel ();
		
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
	}
	public function preDispatch()
	{
		$template_obj = new Eicra_View_Helper_Template ();
		$template_obj->setFrontendTemplate ();
		
		$front_template = Zend_Registry::get('front_template');		
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
		$this->view->front_template = $front_template;

		if ($this->_request->getParam ( 'menu_id' ))
		{
			$viewHelper = new Eicra_VHelper_ViewHelper ( $this->_request );
			$page_id_arr = $viewHelper->_getContentId ();
			$this->_page_id = (! empty ( $page_id_arr [0] )) ? $page_id_arr [0] : null;
		}
		else
		{
			$this->_page_id = 1;
		}
	}
	public function itemsAction()
	{
		$group_id = $this->_page_id;
		if ($this->_request->isPost ())
		{
			// Getting inpute value from fontend via request object
			// ====================================================
			$postValue = $this->_request->getPost ();
			$cityName = $postValue ['location_for_search_LIKE'];
			$checkIN = $postValue ['check_in'];
			$checkOUT = $postValue ['check_out'];
			try
			{
				// Getting data from Admin API Settings
				// ====================================================
				$apiKey = $this->_api_settings->getApiKey ();
				$tsCode = $this->_api_settings->getTsCode ();
				$baseUri = $this->_api_settings->getBaseUri ();
				// $baseUri = 'http://api.wego.com/hotels/api/locations/search';
				// Building WeGo Links to get data
				// ====================================================
				$httpData = new Zend_Http_Client ();
				$httpData->setParameterGet ( 'q', $cityName );
				$httpData->setParameterGet ( 'api_key', $apiKey );
				$httpData->setParameterGet ( 'ts_code', $tsCode );
				$httpData->setUri ( $baseUri );
				$resultSet = $httpData->request ( 'GET' );
				$responseValue = $resultSet->getBody ();
				$responseValue_arr = Zend_Json::decode ( $responseValue, Zend_Json::TYPE_ARRAY );
				if (! array_key_exists ( 'error', $responseValue_arr ))
				{
					// Total Number of location found
					$totalResult = $responseValue_arr ['count'];
					// echo 'Total Number of location found ' . $totalResult . '</br>';
					$eachLocation = $responseValue_arr ['locations'];
					
					foreach ( $eachLocation as $eachLocationKey => $locationRow )
					{
						$locationName = $locationRow ['name'];
						$locationCountry = $locationRow ['country_name'];
						$locationCountryCode = $locationRow ['country_code'];
						$locationState = $locationRow ['state_name'];
						$locationStateCode = $locationRow ['state_code'];
						$locationID = $locationRow ['id'];
						 //echo '</br>'.$locationDetails . ' ==> ' . $locationID . ' ==> ' . $locationState . ' ==> ' . $locationCountry;
						// create an hyperlink on $locationName when it clicked.
						// Getting the short details of each hotels when $locationName is clicked.
						$hotelDetails = $this->getHotelDetails ( $locationID, $checkIN, $checkOUT, $apiKey,$tsCode ); 
						//$c = 1;
						/* if ($hotelDetails ['count'] == 0)
						{
							$hotelDetails = $this->getHotelDetails ( $locationID, $checkIN, $checkOUT, $apiKey,$tsCode );
							$c = 2;
						} */
						/* if ($hotelDetails ['count'] == 0)
						{
							$hotelDetails = $this->getHotelDetails ( $locationID, $checkIN, $checkOUT, $apiKey,$tsCode );
							$c = 3;
						}
						if ($hotelDetails ['count'] == 0)
						{
							$hotelDetails = $this->getHotelDetails ( $locationID, $checkIN, $checkOUT, $apiKey,$tsCode );
							$c = 4;
						} */
						// echo $c;
						$hotelDetails_array = array ();
						// Means, if any result if found.
						if ($hotelDetails ['count'] > 0)
						{
							foreach ( $hotelDetails as $number => $row )
							{
								if (is_array ( $row ))
								{
									//echo '<pre>'; print_r($row); echo '</pre>'; echo "</br></br></br></br> <hr>";
									$hotelDetails_array = array_merge ( $hotelDetails_array, $row );
								}
							}
						}
						if (! empty ( $hotelDetails_array [0] ))
						{
							$eachLocation [$eachLocationKey] ['hotel_info'] = $hotelDetails_array;
							/*
							 * echo '<pre>'; print_r($eachLocation[$number]['hotel_info']); echo '</pre>'; echo "</br></br></br></br> <hr>";
							 *
							 */
						}
						else
						{
							unset ( $eachLocation [$eachLocationKey] );
						}
					}
				}
				else
				{
					$json_arr = array (
							'status' => 'err',
							'msg' => $responseValue_arr ['error'],
							'post_data' => $this->_request->getPost ()
					);
					$this->errorMessage = $responseValue_arr ['error'];
				}
			}
			catch ( Exception $e )
			{
				$json_arr = array (
						'status' => 'err',
						'msg' => $e->getMessage (),
						'post_data' => $this->_request->getPost ()
				);
				$this->errorMessage = $e->getMessage ();
			}
			$this->view->assign ( 'postValue', $postValue );
			$this->view->assign ( 'view_datas', $eachLocation );
		}
	}
	public function detailsAction()
	{
		try
		{
			// Getting inpute value from fontend via request object
			// ====================================================
			$search_id = $this->_request->getParam ( 'search_id' );
			$hotel_id = $this->_request->getParam ( 'hotel_id' );
			$tab = $this->_request->getParam ( 'tab' );
			// Getting data from Admin API Settings
			// ====================================================
			$apiKey = $this->_api_settings->getApiKey ();
			$tsCode = $this->_api_settings->getTsCode ();
			$baseUri = $this->_api_settings->getBaseUri ();
			$details = $this->getHotelDetailsById ( $search_id, $hotel_id, $apiKey ,$tsCode );
			$this->view->view_datas = $details;
			$this->view->hotel_id = $hotel_id;
			$this->view->search_id = $search_id;
			$this->view->apiKey = $apiKey;
			$this->view->tab = (! empty ( $tab )) ? $tab : '0';
		}
		catch ( Exception $e )
		{
			$this->errorMessage = $e->getMessage ();
			$this->view->view_datas = null;
			$this->view->tab = '0';
		}
	}
	private function getHotelDetailsById($search_id, $hotel_id, $apiKey, $tsCode)
	{
		$baseUri = 'http://api.wego.com/hotels/api/search/show/' . $search_id;
		$httpData = new Zend_Http_Client ();
		$httpData->setParameterGet ( 'hotel_id', $hotel_id );
		$httpData->setParameterGet ( 'key', $apiKey );
		$httpData->setParameterGet ( 'ts_code', $tsCode );
		$httpData->setUri ( $baseUri );
		$resultSet = $httpData->request ( 'GET' );
		$responseValue = $resultSet->getBody ();
		$responseValue_arr = Zend_Json::decode ( $responseValue, Zend_Json::TYPE_ARRAY );
		return $responseValue_arr;
	}
	private function getHotelDetails($locationID, $checkIN, $checkOUT, $apiKey, $tsCode)
	{
		$baseUri = 'http://api.wego.com/hotels/api/search/new';
		$httpData = new Zend_Http_Client ();
		$httpData->setParameterGet ( 'location_id', $locationID );
		$httpData->setParameterGet ( 'check_in', $checkIN );
		$httpData->setParameterGet ( 'check_out', $checkOUT );
		$httpData->setParameterGet ( 'key', $apiKey );
		$httpData->setParameterGet ( 'ts_code', $tsCode );
		$httpData->setParameterGet ( 'user_ip', $this->getRealIpAddr() );
		$httpData->setUri ( $baseUri );
		$resultSet = $httpData->request ( 'GET' );	
		
		$responseValue = $resultSet->getBody ();
		
		$responseValue_arr = Zend_Json::decode ( $responseValue, Zend_Json::TYPE_ARRAY );
		$hotelDetails = $this->getHotelBySearchID ( $responseValue_arr ['search_id'], $apiKey , $tsCode );
		foreach ( $hotelDetails as $key => $array )
		{
			if ($key == 'hotels')
			{
				foreach ( $array as $array_key => $array_val )
				{
					$hotelDetails [$key] [$array_key] ['search_id'] = $responseValue_arr ['search_id'];
				}
			}
		}
		return $hotelDetails;
	}
	private function getHotelBySearchID($searchID, $apiKey , $tsCode)
	{
		$baseUri = 'http://api.wego.com/hotels/api/search/' . $searchID;
		$httpData = new Zend_Http_Client ();
		$httpData->setParameterGet ( 'key', $apiKey );
		$httpData->setParameterGet ( 'ts_code', $tsCode );
		$httpData->setUri ( $baseUri );
		$resultSet = $httpData->request ('GET');
		$responseValue = $resultSet->getBody ();
		$hotelDetails_arr = Zend_Json::decode ( $responseValue, Zend_Json::TYPE_ARRAY );
		return $hotelDetails_arr;
	}
	public function getRealIpAddr (){
		if (! empty($_SERVER['HTTP_CLIENT_IP']))         // check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR']))         // to check ip is pass
		// from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}