<?php

/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 * *****************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 * *****************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 * *****************************************************************************
 */
class Hotels_ExpediaController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_group_form_id_field = 'dynamic_form';
    private $_group_table = 'hotels_group';
    private $_static_table = 'hotels_page';
    private $_controllerCache;
    private $_translator;
    private $_api_settings;
    private $_expedia_Api_helper;
    protected $_sessionCheckInInst;

    public function init() {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
        //DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();
        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
        //Initialize API Details
        $this->_api_settings = new Hotels_Model_ExpediaApiModel();
        $this->_expedia_Api_helper = new Hotels_Controller_Helper_Expedia();


        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        
        
//        Zend_Session::regenerateId();
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template ();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;
        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = 1;
        }
    }

    public function indexAction() {
        /*         * **************************************************
         * * Extracting list of EAN API Param
         * ************************************************** */
        $apiKey = $this->_api_settings->getApiKey();
        $cid = $this->_api_settings->getCid();
        $locale = $this->_api_settings->getLocale();
        $minorRev = $this->_api_settings->getMinorRev();
        $currencyCode = $this->_api_settings->getCurrencyCode();
        $numberOfResults = $this->_api_settings->getNumberOfResults();
        $supplierType = $this->_api_settings->getSupplierType();
        $sortBy = $this->_api_settings->getSortedBy();
        $city = $this->_api_settings->getCityName();
        $isTest = $this->_api_settings->getisTest();
        $this->view->assign('cID', $cid);

        /*         * **************************************************
         * * Building HTTP URL-encoded query string
         * ************************************************** */
        $sessionID = Zend_Session::getId();
        
        $httpData = new Zend_Http_Client();
        $httpData->setUri($this->_api_settings->getRequestHotelList($isTest));
        $httpData->setParameterGet('cid', $cid);
        $httpData->setParameterGet('minorRev', $minorRev);
        $httpData->setParameterGet('apiKey', $apiKey);
        $httpData->setParameterGet('locale', $locale);
        $httpData->setParameterGet('currencyCode', $currencyCode);
        $httpData->setParameterGet('customerSessionId', $sessionID);
        $httpData->setParameterGet('customerIpAddress', $this->_expedia_Api_helper->getRealIpAddr());
        $httpData->setParameterGet('customerUserAgent', $_SERVER['HTTP_USER_AGENT']);
        $httpData->setParameterGet('numberOfResults', $numberOfResults);
        $httpData->setParameterGet('supplierType', $supplierType);

        /*         * **************************************************
         * * Loading Default Data to display few relevant hotel information on homepage while page loaded. 
         * ************************************************** */

        $global_conf = Zend_Registry::get('global_conf');
        $siteName = empty($global_conf ['site_title']) ? $global_conf ['site_name'] : $global_conf ['site_title'];


        // Now Adding User Search param
        // Folloiwng data are fatched from user submitted search forms.
        $checkIN = Date('m/d/y', strtotime("+2 days"));
        $checkOUT = Date('m/d/y', strtotime("+7 days"));

        $httpData->setParameterGet('city', $countryCode);
        $httpData->setParameterGet('arrivalDate', $checkIN);
        $httpData->setParameterGet('departureDate', $checkOUT);
        $httpData->setParameterGet('room1', 2);
        $httpData->setParameterGet('city', $city);
        $httpData->setParameterGet('minStarRating', '1');
        // Now Adding additional optimisation Parameters for faster output and minimize load time
        $httpData->setParameterGet('supplierCacheTolerance', 'MED_ENHANCED');
        $httpData->setParameterGet('sort', $sortBy);
        $httpData->setConfig(array(
            'maxredirects' => 10,
            'timeout' => 60,
            'keepalive' => true
        ));
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));
        try {

            $resultSet = $httpData->request('GET');

            if (empty($resultSet)) {

                $this->view->errorMassage = $this->_translator->translator('hotels_api_expedia_error_post_request');
                return;
            }
            $responseValue = $resultSet->getBody();
            $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);

            $pageNumber = $this->getRequest()->getParam('page');
            $viewPageNum = 5;
            $paginator = Zend_Paginator::factory($responseValue_arr['HotelListResponse'] ['HotelList']['HotelSummary']);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);

            $this->view->view_datas = $responseValue_arr['HotelListResponse'] ['HotelList'];
            $this->view->view_datas_paginator = $paginator;
            $this->view->expedia_error = $responseValue_arr['HotelListResponse'] ['EanWsError'];

            $this->view->postValue = array('check_in' => Date('Y-m-d', strtotime("+2 days")),
                'check_out' => Date('Y-m-d', strtotime("+7 days"))
            );
        } catch (Exception $e) {

            $this->view->errorMassage = '<h1>' . $e->getMessage() . '</h1>';
            $this->view->verboseMessage = $responseValue;
        }
    }

    public function itemsAction() {
        /*         * **************************************************
         * * Extracting list of EAN API Param
         * ************************************************** */
        $apiKey = $this->_api_settings->getApiKey();
        $cid = $this->_api_settings->getCid();
        $locale = $this->_api_settings->getLocale();
        $minorRev = $this->_api_settings->getMinorRev();
        $currencyCode = $this->_api_settings->getCurrencyCode();
        $numberOfResults = $this->_api_settings->getNumberOfResults();
        $supplierType = $this->_api_settings->getSupplierType();
        $sortBy = $this->_api_settings->getSortedBy();
        $isTest = $this->_api_settings->getisTest();
        $this->view->assign('cID', $cid);

        if ($this->_request->isPost()) {
            /*             * **************************************************
             * * Extracting http Request Get param from search query.
             * ************************************************** */
            $postValue = $this->_request->getPost();
            Eicra_Global_Variable::getSession()->postValue = $postValue;
            $cityName = $postValue['location_for_search_LIKE'];
            $checkIN = $postValue['check_in'];
            $checkOUT = $postValue['check_out'];
            $countryCode = $postValue['countries'];
            $numberOfBedRooms = $postValue['numberOfBedRooms'];
            $numberOfAdults = $postValue['numberOfAdults'];
            $numberOfChildren = $postValue['numberOfChildren'];
            $minStarRating = $postValue['minStarRating'];
            $amenities = $postValue['amenities'];


            /*             * **************************************************
             * * Building HTTP URL-encoded query string
             * ************************************************** */
            $httpData = new Zend_Http_Client();
            $httpData->setUri($this->_api_settings->getRequestHotelList($isTest));
            $httpData->setParameterGet('cid', $cid);
            $httpData->setParameterGet('minorRev', $minorRev);
            $httpData->setParameterGet('apiKey', $apiKey);
            $httpData->setParameterGet('locale', $locale);
            $httpData->setParameterGet('currencyCode', $currencyCode);
            $httpData->setParameterGet('customerIpAddress', $this->_expedia_Api_helper->getRealIpAddr());
            $httpData->setParameterGet('customerUserAgent', $_SERVER['HTTP_USER_AGENT']);
            $httpData->setParameterGet('numberOfResults', $numberOfResults);
            $httpData->setParameterGet('supplierType', $supplierType);

            // Now Adding User Search param
            // Folloiwng data are fatched from user submitted search forms.
            $httpData->setParameterGet('city', $cityName);
            $httpData->setParameterGet('arrivalDate', $this->formatData($checkIN));
            $httpData->setParameterGet('departureDate', $this->formatData($checkOUT));
            $roomDetails = '';

            for ($i = 0; $i <= 7; $i++) {

                $roomRow = '';
                $adult = $postValue['room-' . $i . '-adult-total'];
                $age1 = (int) $postValue['room-' . $i . '-child-0-age'];
                $age2 = (int) $postValue['room-' . $i . '-child-1-age'];
                $age3 = (int) $postValue['room-' . $i . '-child-2-age'];

                if (!empty($adult) && $adult != -1) {
                    $roomRow = $adult;
                } else {

                    continue;
                }

                if (!empty($age1) && $age1 != -1) {
                    $roomRow .= ',' . $age1;
                }

                if (!empty($age2) && $age2 != -1) {
                    $roomRow .= ',' . $age2;
                }

                if (!empty($age3) && $age3 != -1) {
                    $roomRow .= ',' . $age3;
                }

                if (!empty($adult)) {
                    $roomDetails .= 'room' . ($i + 1) . '=' . $roomRow . '-';
                    $httpData->setParameterGet('room' . ($i + 1), $roomRow);
                }
            }


            if (!empty($numberOfAdults)) {
                $httpData->setParameterGet('numberOfAdults', $numberOfAdults);
            }
            if (!empty($numberOfChildren)) {
                $httpData->setParameterGet('numberOfChildren', $numberOfChildren);
            }
            if (!empty($minStarRating)) {
                $httpData->setParameterGet('minStarRating', $minStarRating);
            }
            if (!empty($amenities)) {
                $httpData->setParameterGet('amenities', $amenities);
            }

            if (!empty($countryCode)) {
                $httpData->setParameterGet('countryCode', $countryCode);
            }
            $global_conf = Zend_Registry::get('global_conf');
            $siteName = empty($global_conf ['site_title']) ? $global_conf ['site_name'] : $global_conf ['site_title'];

            // Now Adding additional optimisation Parameters for faster output and minimize load time
            $httpData->setParameterGet('supplierCacheTolerance', 'MED_ENHANCED');
            $httpData->setParameterGet('sort', $sortBy);
            
            $httpData->setConfig(array(
                'maxredirects' => 10,
                'timeout' => 60,
                'keepalive' => true
            ));
            $httpData->setHeaders(array(
                'Accept' => 'application/json',
                'Accept-encoding' => 'gzip,deflate',
                'X-Powered-By' => $siteName));



            try {

                $resultSet = $httpData->request('GET');

                if (empty($resultSet)) {

                    $this->view->errorMassage = $this->_translator->translator('hotels_api_expedia_error_post_request');
                    return;
                }

                $responseValue = $resultSet->getBody();
                
                $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
                
                $this->view->view_datas = $responseValue_arr['HotelListResponse'] ['HotelList'];
                $this->view->postValue = $this->_request->getPost();

                if (empty($roomDetails)) {
                    $this->view->rooms = 'room1=1-';
                } else {

                    $this->view->rooms = $roomDetails;
                    
                }
            } catch (Exception $e) {

                $this->view->errorMassage = '<h1>' . $e->getMessage() . '</h1>';
                $this->view->verboseMessage = $responseValue;
            }
        }
    }

    public function detailsAction() {
        /*         * **************************************************
         * * Extracting list of EAN API Param
         * ************************************************** */
        $apiKey = $this->_api_settings->getApiKey();
        $cid = $this->_api_settings->getCid();
        $locale = $this->_api_settings->getLocale();
        $minorRev = $this->_api_settings->getMinorRev();
        $currencyCode = (string) $this->_api_settings->getCurrencyCode();
        $numberOfResults = $this->_api_settings->getNumberOfResults();
        $supplierType = $this->_api_settings->getSupplierType();
        $hotel_id = $this->_request->getParam('hotel_id');
        $customerIpAddress = $this->_expedia_Api_helper->getRealIpAddr();
        $isTest = $this->_api_settings->getisTest();

        /*         * **************************************************
         * * Extracting http Request Get param from search query.
         * ************************************************** */
        $cityName = $postValue['location_for_search_LIKE'];
        $checkIN = $postValue['check_in'];
        $checkOUT = $postValue['check_out'];
        $numberOfBedRooms = $postValue['numberOfBedRooms'];
        $numberOfAdults = $postValue['numberOfAdults'];
        $numberOfChildren = $postValue['numberOfChildren'];
        $minStarRating = $postValue['minStarRating'];
        $amenities = $postValue['amenities'];
        $countryCode = $postValue['countries'];
        /*         * **************************************************
         * * Building HTTP URL-encoded query string
         * ************************************************** */
        $httpData = new Zend_Http_Client();
        $httpData->setUri($this->_api_settings->getRequestHotelInfo($isTest));
        $httpData->setParameterGet('apiKey', $apiKey);
        $httpData->setParameterGet('cid', $cid);
        $httpData->setParameterGet('locale', $locale);
        $httpData->setParameterGet('hotelId', $hotel_id);
        $httpData->setParameterGet('currencyCode', $currencyCode);
        $httpData->setParameterGet('supplierType', $supplierType);
        // Now Adding User Search param
        // Folloiwng data are fatched from user submitted search forms.
        $httpData->setParameterGet('customerIpAddress', $customerIpAddress);
        $httpData->setParameterGet('arrivalDate', $this->formatData($checkIN));
        $httpData->setParameterGet('departureDate', $this->formatData($checkOUT));
        $httpData->setParameterGet('option', '0');
        $httpData->setParameterGet('room1', $numberOfBedRooms);
        if (!empty($numberOfAdults)) {
            $httpData->setParameterGet('numberOfAdults', $numberOfAdults);
        }
        if (!empty($numberOfChildren)) {
            $httpData->setParameterGet('numberOfChildren', $numberOfChildren);
        }
        if (!empty($minStarRating)) {
            $httpData->setParameterGet('minStarRating', $minStarRating);
        }
        if (!empty($amenities)) {
            $httpData->setParameterGet('amenities', $amenities);
        }
        if (!empty($countryCode)) {
            $httpData->setParameterGet('countryCode', $countryCode);
        }
        $global_conf = Zend_Registry::get('global_conf');
        $siteName = empty($global_conf ['site_title']) ? $global_conf ['site_name'] : $global_conf ['site_title'];
        // Now Adding additional optimisation Parameters for faster output and minimize load time
        $httpData->setParameterGet('supplierCacheTolerance', 'MED_ENHANCED');
        $httpData->setParameterGet('sort', 'PRICE');
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));
        $httpData->setConfig(array(
            'maxredirects' => 10,
            'timeout' => 60,
            'keepalive' => true
        ));


        try {

            $resultSet = $httpData->request('GET');
            if (empty($resultSet)) {
                $this->view->errorMassage = $this->_translator->translator('hotels_api_expedia_error_post_request');
                return;
            }
            
            $responseValue = $resultSet->getBody();

            
            
            $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
            
            $this->view->view_datas = $responseValue_arr['HotelInformationResponse'];
            $this->view->affiliateId = $cid;
            /*             * **************************************************
             * * Getting Room Details.
             * ************************************************** */
            $arrival = $this->_request->getParam('arrival');
            $departure = $this->_request->getParam('departure');
            $hotel_id = $this->_request->getParam('hotel_id');
            $datesArray = explode(',', $dates);
            $roomDetails = $this->_request->getParam('tab');
            $this->view->roomDetails = $roomDetails;
            $this->view->room_collection = $this->getRooms($arrival, $departure, $hotel_id, $roomDetails);
            Eicra_Global_Variable::getSession()->_sessionCheckInInst = htmlentities($this->view->room_collection['HotelRoomAvailabilityResponse']['checkInInstructions']);
        } catch (Exception $e) {
            $this->view->errorMassage = '<h1>' . $e->getMessage() . '</h1>';
            $this->view->verboseMessage = $responseValue;
        }
    }

    public function reservationAction() {
        $queryValue = $this->_request->getParams(); //Getting Get Param;
        $request_object = base64_decode(urldecode($queryValue['bookingInfo']));
        
        $request_array =  Zend_Json_Decoder::decode($request_object);
        
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            foreach ($globalIdentity as $globalIdentity_key => $globalIdentity_value) {
                $request_array[$globalIdentity_key] = $globalIdentity_value;
            }
        }
        $request_array["eMail"] = $globalIdentity->username;
        Eicra_Global_Variable::getSession()->invoice_arr = $request_array;
        
        echo "<pre>";
        print_r($request_array);
        echo "</pre>";
//        exit();
        $this->view->view_datas = $request_array;
        $this->view->mem_info = $request_array;
        $this->view->oldSession = Eicra_Global_Variable::getSession()->session_id;
        $this->view->newSession = Zend_Session::getId();
        $this->assignLogin();
        $this->view->logindetails = new Members_Form_LoginForm();

        $this->dynamicUploaderSettings($this->view->form_info);
    }

    private function assignLogin() {
        $template_id_field = 'role_id';
        $settings_db = new Invoice_Model_DbTable_Setting();
        $template_info = $settings_db->getInfoByModule('Hotels');
        if ($template_info) {
            $selected_role_id = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator('hotels_invoice_register_role_id');
        } else {
            $selected_role_id = $this->_translator->translator('hotels_invoice_register_role_id');
        }
        //echo $selected_role_id;
        if (!empty($selected_role_id)) {
            try {
                $role_db = new Members_Model_DbTable_Role();
                $role_info = $role_db->getRoleInfo($selected_role_id);
                $form_db = new Members_Model_DbTable_Forms();
                $form_info = $form_db->getFormsInfo($role_info['form_id']);
                $this->view->form_info = $form_info;
                $this->view->role_info = $role_info;
            } catch (Exception $e) {
                $role_info = null;
            }
        } else {
            $role_info = null;
        }
        if ($role_info['allow_register_to_this_role'] == '1' && $role_info != null) {
            $registrationForm = new Members_Form_UserForm($role_info);
            if ($registrationForm->role_id) {
                $registrationForm->removeElement('role_id');
            }
            $loginUrl = $this->view->serverUrl() . $this->view->baseUrl() . '/Members-Login';
            $this->view->selected_role_id = $selected_role_id;
            $this->view->registrationForm = $registrationForm;
        } else {
            throw new Exception("Register page not found");
        }
    }

    public function cardAction() {
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
        Eicra_Global_Variable::checkSession($this->_response, $url);

        $form_id = $this->_translator->translator('hotels_api_expedia_form_id');
        if ($form_id) {
            $form_db = new Members_Model_DbTable_Forms();
            $form_info = $form_db->getFormsInfo($form_id);
            $generalForm = new Members_Form_GeneralForm($form_info);
            if ($form_info['login_set'] == '1') {
                $getAction = $this->_request->getActionName();
                if ($getAction != 'uploadfile') {
                    $url = $this->view->url(array('module' => 'Members', 'controller' => 'frontend', 'action' => 'login'), 'Frontend-Login', true);
                    Eicra_Global_Variable::checkSession($this->_response, $url);
                }
            }

            /*             * *******************************Extra Passenger Field Start******************************* */
            $invoice_arr = Eicra_Global_Variable::getSession()->invoice_arr;

            $rooms = $invoice_arr['GustNumber'];
            $gustList = explode('-', $rooms);

            $group_arr = array();
            foreach ($gustList as $gustListData) {
                if (!empty($gustListData)) {
                    $gustListData_arr = explode('=', $gustListData);
                    $person_arr = explode(',', $gustListData_arr[1]);
                    $room_name = $gustListData_arr[0];
                    $adult_no = $person_arr[0];
                    for ($r = 1; $r <= $adult_no; $r++) {
                        $group_arr[$room_name . '_first_name_' . $r] = $room_name . '_first_name_' . $r;
                        $group_arr[$room_name . '_last_name_' . $r] = $room_name . '_last_name_' . $r;
                        $generalForm->addElement('text', $room_name . '_first_name_' . $r, array('label' => ucfirst($room_name) . ' Adult First Name :', 'id' => $room_name . '_first_name_' . $r, 'class' => 'ui-widget-content ui-corner-all', 'title' => ucfirst($room_name) . ' Adult First Name', 'rel' => 'first', 'size' => '50', 'admin' => '1', 'frontend' => '1', 'info' => ucfirst($room_name) . ' Adult First Name', 'required' => true, 'value' => ''));
                        $generalForm->addElement('text', $room_name . '_last_name_' . $r, array('label' => ucfirst($room_name) . ' Adult Last Name :', 'id' => $room_name . '_last_name_' . $r, 'class' => 'ui-widget-content ui-corner-all', 'title' => ucfirst($room_name) . ' Adult Last Name', 'rel' => 'last', 'size' => '50', 'admin' => '1', 'frontend' => '1', 'info' => ucfirst($room_name) . ' Adult Last Name', 'required' => true, 'value' => ''));
                        break;
                    }
                }
            }

            $multiOptions = $generalForm->creditCardType->getMultiOptions();
            $generalForm->creditCardType->clearMultiOptions();
            $cmt = 0;

            foreach ($multiOptions as $multiOptionsKey => $multiOptionsValue) {
                if ($cmt == 0) {
                    $generalForm->creditCardType->addMultiOption('', $multiOptionsValue);
                } else {
                    $generalForm->creditCardType->addMultiOption($multiOptionsKey, $multiOptionsValue);
                }
                $cmt++;
            }

            $generalForm->addDisplayGroup($group_arr, 'Information', array('disableLoadDefaultDecorators' => true, 'title' => 'Passenger Information', 'legend' => 'Passenger Information'));


            /*             * *******************************Extra Passenger Field End******************************* */
            if ($this->_request->isPost()) {
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
                $translator = Zend_Registry::get('translator');
                $elements = $generalForm->getElements();
                foreach ($elements as $element) {
                    if ($element->getType() == 'Zend_Form_Element_File') {
                        $element_name = $element->getName();
                        $generalForm->removeElement($element_name);
                    }
                }
                if ($generalForm->isValid($this->_request->getPost())) {
                    $generalForm = new Members_Form_GeneralForm($form_info);

                    /*                     * *******************************Extra Passenger Field Start******************************* */

                    $group_arr = array();
                    foreach ($gustList as $gustListData) {
                        if (!empty($gustListData)) {
                            $gustListData_arr = explode('=', $gustListData);
                            $person_arr = explode(',', $gustListData_arr[1]);
                            $room_name = $gustListData_arr[0];
                            $adult_no = $person_arr[0];
                            for ($r = 1; $r <= $adult_no; $r++) {
                                $group_arr[$room_name . '_first_name_' . $r] = $room_name . '_first_name_' . $r;
                                $group_arr[$room_name . '_last_name_' . $r] = $room_name . '_last_name_' . $r;
                                $generalForm->addElement('text', $room_name . '_first_name_' . $r, array('label' => ucfirst($room_name) . ' Adult First Name :', 'id' => $room_name . '_first_name_' . $r, 'class' => 'ui-widget-content ui-corner-all', 'title' => ucfirst($room_name) . ' Adult First Name', 'rel' => 'first', 'size' => '50', 'admin' => '1', 'frontend' => '1', 'info' => ucfirst($room_name) . ' Adult First Name', 'required' => true, 'value' => ''));
                                $generalForm->addElement('text', $room_name . '_last_name_' . $r, array('label' => ucfirst($room_name) . ' Adult Last Name :', 'id' => $room_name . '_last_name_' . $r, 'class' => 'ui-widget-content ui-corner-all', 'title' => ucfirst($room_name) . ' Adult Last Name', 'rel' => 'last', 'size' => '50', 'admin' => '1', 'frontend' => '1', 'info' => ucfirst($room_name) . ' Adult Last Name', 'required' => true, 'value' => ''));
                                break;
                            }
                        }
                    }
                    $generalForm->addDisplayGroup($group_arr, 'Information', array('disableLoadDefaultDecorators' => true, 'title' => 'Passenger Information', 'legend' => 'Passenger Information'));
                    /*                     * *******************************Extra Passenger Field End******************************* */
                    $generalForm->populate($this->_request->getPost());
                    $fromValues = $generalForm;
                    if ($form_info['db_set'] == '1') {
                        $general = new Members_Model_General(array('form_id' => $form_info['id'], 'active' => '2'));
                        $result = $general->saveGeneral();
                    } else {
                        $result['status'] = 'ok';
                        $result['id'] = 0;
                    }
                    if ($result['status'] == 'ok') {
                        $field_db = new Members_Model_DbTable_Fields();
                        $field_groups = $field_db->getGroupNames($form_info['id']);
                        //Add Data To Database
                        $attach_file_arr = array();
                        foreach ($field_groups as $group) {
                            $group_name = $group->field_group;
                            $displaGroup = $generalForm->getDisplayGroup($group_name);
                            $elementsObj = $displaGroup->getElements();
                            foreach ($elementsObj as $element) {
                                $table_id = $result['id'];
                                $form_id = $form_info['id'];
                                $field_id = $element->getAttrib('rel');
                                $field_value = ($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();

                                if ($element->getType() == 'Zend_Form_Element_MultiCheckbox') {
                                    if (is_array($field_value)) {
                                        $field_value = '';
                                    }
                                }
                                if ($element->getType() == 'Zend_Form_Element_Multiselect') {
                                    $field_value = $this->_request->getPost($element->getName());
                                    if (is_array($field_value)) {
                                        $field_value = implode(',', $field_value);
                                    }
                                }
                                if ($element->getType() == 'Zend_Form_Element_File') {
                                    $post_data = $this->_request->getPost($element->getName());
                                    $post_data_arr = explode(',', $post_data);
                                    $attach_file_arr = array_merge((array) $attach_file_arr, (array) $post_data_arr);
                                }
                                try {
                                    $msg = $translator->translator("member_form_submit_successfull", '', 'Members');
                                    $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                                    if ($form_info['db_set'] == '1') {
                                        $data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
                                        $this->_DBconn = Zend_Registry::get('msqli_connection');
                                        $this->_DBconn->getConnection();
                                        $this->_DBconn->insert(Zend_Registry::get('dbPrefix') . 'forms_fields_values', $data);
                                        $msg = $translator->translator("member_form_submit_successfull", '', 'Members');
                                        $json_arr = array('status' => 'ok', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                                    }
                                } catch (Exception $e) {
                                    $msg = $translator->translator("member_form_submit_err", '', 'Members');
                                    $json_arr = array('status' => 'err', 'msg' => $msg . ' ' . $e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                                }
                            }
                        }
                        //Send Data To Email
                        if ($form_info['email_set'] == '1') {
                            $register_helper = new Members_Controller_Helper_Registers();
                            $allDatas = $fromValues->getValues();
                            try {
                                $register_helper->sendGeneralMail($allDatas, $form_info, $attach_file_arr);
                                $to_mail = trim($this->_request->getPost('friend_email'));
                                if ($to_mail) {
                                    $send_result = $register_helper->sendEmailToFriend($allDatas, $form_info, $attach_file_arr, $to_mail);
                                }
                                $msg = $translator->translator("member_form_submit_successfull", '', 'Members');
                                $json_arr = array('status' => 'ok', 'msg' => $msg, 'attach_file_arr' => $attach_file_arr, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                            } catch (Exception $e) {
                                $msg = $e->getMessage();
                                $json_arr = array('status' => 'err', 'msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                            }
                        }
                        //Delete Attached Files
                        if ($form_info['attach_file_delete'] == '1') {
                            $elements = $generalForm->getElements();
                            foreach ($elements as $element) {
                                if ($element->getType() == 'Zend_Form_Element_File') {
                                    $element_name = $element->getName();
                                    $element_value = $this->_request->getPost($element_name);
                                    if (!empty($element_value)) {
                                        $element_value_arr = explode(',', $element_value);
                                        foreach ($element_value_arr as $key => $e_value) {
                                            if (!empty($e_value)) {
                                                $dir = BASE_PATH . DS . $form_info['attach_file_path'] . DS . $e_value;
                                                $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Your Code Start Here

                        $pGroup = $generalForm->getDisplayGroup('Information');
                        $pGroupElement = $pGroup->getElements();
                        $guest_arr = Eicra_Global_Variable::getSession()->invoice_arr;
                        $g = 0;
                        foreach ($pGroupElement as $group_element) {
                            //$field_name_arr = explode('_',$group_element->getName() );
                            //if($field_name_arr[3] == '1')
                            //{
                            if ($group_element->getAttrib('rel') == 'first') {
                                $guest_arr['guest_array'][$g]['first_name'] = $group_element->getValue();
                                $guest_arr['RoomGroup'][$g]['Room']['firstName'] = $group_element->getValue();
                                //$guest_arr['RoomGroup'][$g]['Room']['bedTypeId'] = $g;
                            }
                            if ($group_element->getAttrib('rel') == 'last') {
                                $guest_arr['guest_array'][$g]['last_name'] = $group_element->getValue();
                                $guest_arr['RoomGroup'][$g]['Room']['lastName'] = $group_element->getValue();
                                //$guest_arr['RoomGroup'][$g]['Room']['bedTypeId'] = $g;
                                $g++;
                            }
                            //}
                        }
                        Eicra_Global_Variable::getSession()->invoice_arr = $guest_arr;
                        $json_arr = $this->makeReservation();
                        //Your Code End Here
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                    }
                } else {
                    $validatorMsg = $generalForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($generalForm));
                }


                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
            $this->view->form_info = $form_info;
            $this->view->generalForm = $generalForm;
        }
    }

    private function formatData($source) {
        $date = new DateTime($source);
        return $date->format('m/d/Y'); // 31/07/2012
    }

    private function getRooms($arrivalDate, $departureDate, $hotelID, $gustList) {
        /*         * **************************************************
         * * Extracting list of EAN API Param
         * ************************************************** */
        $apiKey = $this->_api_settings->getApiKey();
        $cid = $this->_api_settings->getCid();
        $locale = $this->_api_settings->getLocale();
        $minorRev = $this->_api_settings->getMinorRev();
        $currencyCode = (string) $this->_api_settings->getCurrencyCode();
        $isTest = $this->_api_settings->getisTest();
        /*         * **************************************************
         * * Building HTTP URL-encoded query string
         * ************************************************** */
        $httpData = new Zend_Http_Client();
        $httpData->setParameterGet('cid', $cid);
        $httpData->setParameterGet('minorRev', $minorRev);
        $httpData->setParameterGet('apiKey', $apiKey);
        $httpData->setParameterGet('locale', $locale);
        $httpData->setParameterGet('currencyCode', $currencyCode);
        $httpData->setParameterGet('customerIpAddress', $this->_expedia_Api_helper->getRealIpAddr());
        $httpData->setParameterGet('customerUserAgent', $_SERVER['HTTP_USER_AGENT']);
        $global_conf = Zend_Registry::get('global_conf');
        $siteName = empty($global_conf ['site_title']) ? $global_conf ['site_name'] : $global_conf ['site_title'];
        // Now Adding User Search param
        // Folloiwng data are fatched from user submitted search forms.
        $httpData->setParameterGet('hotelId', $hotelID);
        $httpData->setParameterGet('arrivalDate', $this->formatData($arrivalDate));
        $httpData->setParameterGet('departureDate', $this->formatData($departureDate));
        $httpData->setParameterGet('includeDetails' . 'true');
        $httpData->setParameterGet('includeRoomImages' . 'true');

        for ($i = 0; $i < count($gustList); $i++) {

            $gust = $gustList[$i];
            if (!empty($gust)) {

                $gustVsRoom = explode('=', $gust);

                $httpData->setParameterGet($gustVsRoom[0], $gustVsRoom[1]);
            }
        }
        $httpData->setHeaders(array(
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip,deflate',
            'X-Powered-By' => $siteName));

        $httpData->setConfig(array(
            'maxredirects' => 10,
            'timeout' => 60,
            'keepalive' => true
        ));
        $httpData->setUri($this->_api_settings->getAvailabilityUrl($isTest));
        $resultSet = $httpData->request('GET');
        $responseValue = $resultSet->getBody();

        try {
            $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
            return $responseValue_arr;
        } catch (Exception $e) {
            return null;
        }
    }

    private function makeReservation() {
        $isTest = $this->_api_settings->getisTest();
        $bookingInfo = Eicra_Global_Variable::getSession()->invoice_arr;
        $httpClient = new Zend_Http_Client();
        $httpClient->setUri($this->_api_settings->getReservationUrl($isTest));
        /*         * **************************************************
         * * Extracting list of EAN API Param
         * ************************************************** */
        $apiKey = $this->_api_settings->getApiKey();
        $cid = $this->_api_settings->getCid();
        $locale = $this->_api_settings->getLocale();
        $minorRev = $this->_api_settings->getMinorRev();
        $currencyCode = (string) $this->_api_settings->getCurrencyCode();
        $supplierType = $this->_api_settings->getSupplierType();
        $ccNum = $this->_request->getParam('ccNumber');

        if (empty($bookingInfo['chargeableRate']) || empty($bookingInfo['hotelId'])) {
            return array('status' => 'err', 'msg' => $this->_translator->translator('hotels_api_expedia_error_no_session_info'));
        } else if (empty($apiKey) || empty($ccNum)) {
            return array('status' => 'err', 'msg' => $this->_translator->translator('hotels_api_expedia_error_no_session_info'));
        } else {
            $countryId = new Geo_Model_DbTable_Country ();
            $countryName = $countryId->getCountryInfo($bookingInfo['country']);
            $countryCode = $countryName['code']; // $this->_expedia_Api_helper->getCountryCode($countryName['value']); 

            /*             * **************************************************
             * * Getting Site & User Profile from Global Setting area
             * ************************************************** */

            $global_conf = Zend_Registry::get('global_conf');
            $siteName = empty($global_conf ['site_title']) ? $global_conf ['site_name'] : $global_conf ['site_title'];
            $address = $bookingInfo['address'];

            /*             * **************************************************
             * * Building HTTP URL-encoded query string
             * ************************************************** */
            $httpClient->setParameterPost('minorRev', $minorRev);
            $httpClient->setParameterPost('cid', $cid);
            $httpClient->setParameterPost('apiKey', $apiKey);
            $httpClient->setParameterPost('customerUserAgent', $_SERVER['HTTP_USER_AGENT']);
            $httpClient->setParameterPost('customerIpAddress', $this->_expedia_Api_helper->getRealIpAddr());
            $httpClient->setParameterPost('customerSessionId', $bookingInfo['customerSessionId']);
            $httpClient->setParameterPost('locale', $locale);
            $httpClient->setParameterPost('currencyCode', $currencyCode);



            /*             * ******************************************************
             * * Building Room Group URL-encoded query string
             * ******************************************************* */

            $gustList = explode('-', $gustNumber);
            $guest_array = $bookingInfo['guest_array'];
            $rooms = $bookingInfo ['GustNumber'];
            $gustList = explode('-', $rooms);

            $k = 0;
            for ($i = 0; $i <= count($gustList); $i++) {
                $eachRoom = '';
                $gust = $gustList[$i];
                if (!empty($gust)) {
                    $personName = $guest_array [$i];
                    $first_name = $personName['first_name'];
                    $lastName = $personName ['last_name'];
                    $gustPerRoom = explode('=', $gust);
                    if (!empty($gustPerRoom[0])) {
                        //$httpClient->setParameterPost($gustPerRoom[0] , $gustPerRoom[1]);  
                        //$httpClient->setParameterPost($gustPerRoom[0]. 'FirstName' , $first_name  );
                        //$httpClient->setParameterPost($gustPerRoom[0].'LastName' ,  $lastName );					
                    }
                }
            }

            /*             * ******************************************************
             * * Building User profile based URL-encoded query string
             * ******************************************************* */

            $AddressInfo = array(
                'address1' => $address,
                'city' => $bookingInfo['city'],
                'stateProvinceCode' => $bookingInfo['state'],
                'countryCode' => $countryCode,
                'postalCode' => $bookingInfo['postalCode']
            );
            $httpClient->setParameterPost('AddressInfo', $AddressInfo);


            /*             * ******************************************************
             * * Building User profile based URL-encoded query string
             * ******************************************************* */
            $ccType = $this->_request->getParam('creditCardType');
            $ccExpiry = explode('/', $this->_request->getParam('expire_date'));
            $ccExpiryMonth = $ccExpiry[0];
            $ccExpiryYear = $ccExpiry[1];
            $sessionID = Zend_Session::getId();
//            $AffConfirmationID = $this->generate();

            $ReservationInfo = array(
                'email' => $bookingInfo['username'],
                'firstName' => $bookingInfo['title'] . ' ' . $bookingInfo['firstName'],
                'lastName' => $bookingInfo['lastName'],
                'homePhone' => $bookingInfo['phone'],
                'workPhone' => $bookingInfo['mobile'],
                'creditCardType' => $this->getCCType($ccType),
                'creditCardNumber' => $this->_request->getParam('ccNumber'),
                'creditCardIdentifier' => $this->_request->getParam('cvv'),
                'creditCardExpirationMonth' => $ccExpiryMonth,
                'creditCardExpirationYear' => $ccExpiryYear,
                'creditCardPasHttpUserAgent' => $_SERVER['HTTP_USER_AGENT'],
                'creditCardPasHttpAccept' => $_SERVER['HTTP_ACCEPT']
            );


            $httpClient->setParameterPost('ReservationInfo', $ReservationInfo);


            /*             * ******************************************************
             * * XML VARIABLE FORMAT
             * ******************************************************* */

            $xml = '<HotelRoomReservationRequest>';
            $xml .= '<customerSessionId>' . $sessionID . '</customerSessionId>';
            $xml .= '<customerIpAddress>' . $this->_expedia_Api_helper->getRealIpAddr() . '</customerIpAddress>';
            $xml .= '<customerUserAgent>' . $_SERVER['HTTP_USER_AGENT'] . '</customerUserAgent>';
            $xml .= '<affiliateConfirmationId>' . $sessionID . '</affiliateConfirmationId>';
            $xml .= '<hotelId>' . $bookingInfo['hotelId'] . '</hotelId>';
            $xml .= '<arrivalDate>' . $bookingInfo['arrivalDate'] . '</arrivalDate>';
            $xml .= '<departureDate>' . $bookingInfo['departureDate'] . '</departureDate>';
            $xml .= '<supplierType>' . $supplierType . '</supplierType>';
            $xml .= '<rateKey>' . $bookingInfo['rateKey'] . '</rateKey>';
            $xml .= '<rateCode>' . $bookingInfo['rateCode'] . '</rateCode>';
            $xml .= '<roomTypeCode>' . $bookingInfo['roomTypeCode'] . '</roomTypeCode>';
            $xml .= '<chargeableRate>' . $bookingInfo['chargeableRate'] . '</chargeableRate>';
            $xml .= '<RoomGroup>';
            foreach ($bookingInfo['RoomGroup'] as $RoomGroupKey => $RoomGroupArr) {
                if ($RoomGroupArr['Room']['numberOfAdults'] || $RoomGroupArr['Room']['numberOfChildren'] || $RoomGroupArr['Room']['childAges'] || $RoomGroupArr['Room']['firstName'] || $RoomGroupArr['Room']['lastName']) {
                    $xml .= '<Room>';
                    $xml .= '<numberOfAdults>' . $RoomGroupArr['Room']['numberOfAdults'] . '</numberOfAdults>';
                    $xml .= '<numberOfChildren>' . $RoomGroupArr['Room']['numberOfChildren'] . '</numberOfChildren>';
                    $xml .= '<childAges>' . $RoomGroupArr['Room']['childAges'] . '</childAges>';
                    $xml .= '<firstName>' . $RoomGroupArr['Room']['firstName'] . '</firstName>';
                    $xml .= '<lastName>' . $RoomGroupArr['Room']['lastName'] . '</lastName>';
                    $xml .= '</Room>';
                }
            }
            $xml .= '</RoomGroup>';
            $xml .= '<ReservationInfo>';
            foreach ($ReservationInfo as $ReservationInfoKey => $ReservationInfoValue) {
                $xml .= '<' . $ReservationInfoKey . '>' . $ReservationInfoValue . '</' . $ReservationInfoKey . '>';
            }
            $xml .= '</ReservationInfo>';
            $xml .= '<AddressInfo>';
            foreach ($AddressInfo as $AddressInfoKey => $AddressInfoValue) {
                $xml .= '<' . $AddressInfoKey . '>' . $AddressInfoValue . '</' . $AddressInfoKey . '>';
            }
            $xml .= '</AddressInfo>';
            $xml .= '</HotelRoomReservationRequest>';
//            echo ($xml);
            $httpClient->setParameterPost('xml', $xml);
            /*             * ******************************************************
             * * Building Security Params  URL-encoded query string
             * ******************************************************* */

            $httpClient->setHeaders(array(
                'Accept' => 'application/json',
                'User-Agent' => $_SERVER['HTTP_USER_AGENT'],
                'Accept-encoding' => 'gzip,deflate',
                'X-Powered-By' => $siteName));



            $httpClient->setConfig(array(
                'maxredirects' => 10,
                'timeout' => 60,
                'keepalive' => true
            ));


            try {
                $resultSet = $httpClient->request('POST');
//                print_r($httpClient);


                $b = urldecode($httpClient->getLastRequest());
                // file_put_contents('reqUri.txt', $b);

                if ($resultSet) {
                    $responseValue = $resultSet->getBody();
//                    echo $responseValue;
                    $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
//                    file_put_contents('req.txt', json_encode($responseValue_arr, JSON_PRETTY_PRINT));

                    return $this->processBookingOutput($responseValue_arr['HotelRoomReservationResponse'], $responseValue, $bookingInfo,$sessionID);
                } else {
                    return array('status' => 'err', 'msg' => $this->_translator->translator('hotels_api_expedia_error_post_request'));
                }
            } catch (Exception $e) {
                return array('status' => 'err', 'msg' => $e->getMessage());
            }
        }
    }

    private function processBookingOutput($massage, $json_resultSet, $bookingInfo, $sessionID) {
        $successMsg = $massage;
//        print_r($successMsg);
//        print_r($json_resultSet);
//        print_r($bookingInfo);
        $errorMsg = $massage['EanWsError'];
        $return_msg = 'NOTHING';
        $countryId = new Geo_Model_DbTable_Country ();
        $countryName = $countryId->getCountryInfo($bookingInfo['country']);
        $countryCode = $this->_expedia_Api_helper->getCountryCode($countryName['value']);
        $nightlyRate = $bookingInfo ['chargableRateInfo']['NightlyRatesPerRoom']['NightlyRate'];
        $totalDay = $bookingInfo ['chargableRateInfo']['NightlyRatesPerRoom']['@size'];
        $checkInInstruction = html_entity_decode($bookingInfo ['checkInInstruction']);
        
        Eicra_Global_Variable::getSession()->session_id = $sessionID;
        
        
        $priceBreakdown = '';
        
        if (is_array($nightlyRate)) {
            if ($totalDay == 1) {
                $priceBreakdown .= '<h5><strong>' . $this->_translator->translator('hotels_expedia_rent_per_night') . '</strong></h5>';
                $day = date('d M', strtotime($bookingInfo['arrivalDate']));
                $priceBreakdown .= '<p>' . $day . ' rate: <strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($nightlyRate['@rate'], 2) . '</strong></p>';
            } else {
                $priceBreakdown .= '<h5><strong>' . $this->_translator->translator('hotels_expedia_rent_per_night') . '</strong></h5>';
                for ($i = 1; $i <= count($nightlyRate); $i++):
                    $day = date('d M', strtotime($bookingInfo['arrivalDate'] . "+" . ($i - 1) . " days"));
                    if ($nightlyRate[$i - 1]['@promo']) {
                        $priceBreakdown .= '<p>' . $day . ' rate: <strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($nightlyRate[$i - 1]['@rate'], 2) . '</strong></p>';
                    } else {
                        $priceBreakdown .= '<p>' . $day . ' rate: <strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($nightlyRate[$i - 1]['@rate'], 2) . ' ' . '</strong></p>';
                    }

                endfor;
            }
        }
        
        $priceBreakdown .= '<hr>';
        $priceBreakdown .= '<h5>';
        $priceBreakdown .= $this->_translator->translator('hotels_expedia_sub_total') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $priceBreakdown .= '<strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($bookingInfo ['chargableRateInfo'] ['@nightlyRateTotal'],2) . '</strong>';
        $priceBreakdown .= '</h5>';
        $priceBreakdown .= '<h5>';
        $priceBreakdown .= $this->_translator->translator('hotels_expedia_surcharge') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $priceBreakdown .= '<strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($bookingInfo ['chargableRateInfo'] ['@surchargeTotal'],2) . '</strong>';
        $priceBreakdown .= '</h5>';
        $priceBreakdown .= '<hr>';
        $priceBreakdown .= '<h5>';
        $priceBreakdown .= $this->_translator->translator('hotels_expedia_total') . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $priceBreakdown .= '<strong>' . ($bookingInfo ['chargableRateInfo'] ['@currencyCode']) . ' ' . number_format($bookingInfo ['chargableRateInfo'] ['@total'],2) . '</strong>';
        $priceBreakdown .= '</h5>';
        $priceBreakdown .= '<br>';

        // when reservation inform is loaded successfully
        if (empty($errorMsg) && !empty($successMsg)) {
            $itineraryId = (int) $successMsg['itineraryId'];
            // Check and process only if a valid $itineraryId is found
            if ($itineraryId != -1 && !empty($itineraryId)) {
                $msg = $this->_translator->translator('hotels_api_expedia_booking_successful');
                $paymentConfirm = $this->_translator->translator('hotels_expedia_card_charge_msg');
                $amountPaidMsg = $this->_translator->translator('hotels_expedia_total_amount_paid');
                $amountPaid = $successMsg['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'];
                $currency = $successMsg['RateInfos']['RateInfo']['ChargeableRateInfo']['@currencyCode'];
                $cancellationPolicy = $successMsg['RateInfos']['RateInfo']['cancellationPolicy'];
                $cancellationPolicyMsg = $this->_translator->translator('hotels_api_expedia_cancellation_policy');
                $arrivalDT = date("y-m-d", strtotime($bookingInfo['arrivalDate']));
                $departureDT = date("y-m-d", strtotime($bookingInfo['departureDate']));
                try {
                    $expediaDataModel = new Hotels_Model_Expedia ();
                    $expediaDataModel->setItineraryId($successMsg['itineraryId']);
                    $expediaDataModel->setArrivalDate($arrivalDT);
                    $expediaDataModel->setDepartureDate($departureDT);
                    $expediaDataModel->setHotelId($bookingInfo['hotelId']);
                    $expediaDataModel->setUser_id($bookingInfo['user_id']);
                    $expediaDataModel->setConfirmationNumbers($successMsg['confirmationNumbers']);
                    $expediaDataModel->setEmail($bookingInfo['username']);
                    $expediaDataModel->setRateCurrencyCode($bookingInfo['rateCurrencyCode']);
                    $expediaDataModel->setPrice($bookingInfo['chargeableRate']);
                    $expediaDataModel->setResultSetObject($json_resultSet);
                    $dbMassage = $expediaDataModel->saveReservation();
                    $isWrittenToBD = '';
                    if ($dbMassage['status'] == 'ok') {

                        $isWrittenToBD = 'Please check your Email and/or countrol panel for details itinerary';
                    } else {
                        $isWrittenToBD = 'Failed to add/save details itinerary to your countrol panel(DB) : ' . $dbMassage['msg'];
                    }
                } catch (Exception $e) {
                    $dbMassage = $e->getMessage();
                }
                $space = ' ';
                $return_msg = array('status' => 'ok', 'msg' => $msg . '<br/><strong>' . $paymentConfirm . '</strong><strong>' . $space . $this->_translator->translator('hotels_api_expedia_booking_itinerary_num') . '</strong>' . $itineraryId . $space . $this->_translator->translator('hotels_api_expedia_booking_confirmation_num') . $successMsg['confirmationNumbers'] . '<br />' . $cancellationPolicyMsg . ': ' . $cancellationPolicy . '<br/>' . $isWrittenToBD . '<br/><br/><strong>' . $this->_translator->translator('hotels_price_breakdown') . '</strong><br>' . $priceBreakdown . '<br/><strong>' . $this->_translator->translator('hotels_checkin_instruct') . '</strong><br>' . html_entity_decode(Eicra_Global_Variable::getSession()->_sessionCheckInInst));
                Eicra_Global_Variable::getSession()->_sessionCheckInInst = '';
            } else {
                $msg = $this->_translator->translator('hotels_api_expedia_unknown_error_in_Booking');
                $return_msg = array('status' => 'err', 'msg' => $msg);
            }
        } else if (empty($massage)) {
            $msg = $this->_translator->translator('hotels_api_expedia_unknown_error_in_Booking');
            $return_msg = array('status' => 'err', 'msg' => $msg);
        } else {
            $msg = $errorMsg['handling'] . ' (' . $errorMsg['category'] . ')   :   ' . $errorMsg['verboseMessage'];
            $return_msg = array('status' => 'err', 'msg' => $msg);
        }
        return $return_msg;
    }

    private function getCCType($ccType) {
        $ccTypeVendors = strtolower($ccType);
        switch (trim($ccTypeVendors)) {
            case 'american express':
                return 'AX';
            case 'visa':
                return 'VI';
            case 'master card':
                return 'MC';
            case 'diners club international':
                return 'DC';
            default :
                throw new Exception('Unknown and / or Unsupported Credit Card  Vendors');
        }
    }

    private function dynamicUploaderSettings($info) {
        $param_fields = array(
            'table_name' => 'forms',
            'primary_id_field' => 'id',
            'primary_id_field_value' => $info['id'],
            'file_path_field' => 'attach_file_path',
            'file_extension_field' => 'attach_file_type',
            'file_max_size_field' => 'attach_file_max_size'
        );
        $portfolio_model = new Portfolio_Model_Portfolio($param_fields);
        $requested_data = $portfolio_model->getRequestedData();
        $settings_info = $portfolio_model->getSettingInfo();
        $merge_data = array_merge($requested_data, $settings_info);
        $this->view->assign('settings_info', $merge_data);
        $settings_json_info = Zend_Json_Encoder::encode($merge_data);
        $this->view->assign('settings_json_info', $settings_json_info);
    }
    
    
    
    
    
    private function _nextChar() {
        return base_convert(mt_rand(0, 35), 10, 36);
    }

    private function generate() {
        $parts = explode('.', uniqid('', true));

        $id = str_pad(base_convert($parts[0], 16, 2), 56, mt_rand(0, 1), STR_PAD_LEFT)
            . str_pad(base_convert($parts[1], 10, 2), 32, mt_rand(0, 1), STR_PAD_LEFT);
        $id = str_pad($id, strlen($id) + (8 - (strlen($id) % 8)), mt_rand(0, 1), STR_PAD_BOTH);

        $chunks = str_split($id, 8);

        $id = array();
        foreach ($chunks as $key => $chunk) {
            if ($key & 1) {  // odd
                array_unshift($id, $chunk);
            } else {         // even
                array_push($id, $chunk);
            }
        }

        // add random seeds
        $prefix = str_pad(base_convert(mt_rand(), 10, 36), 6, self::_nextChar(), STR_PAD_BOTH);
        $id = str_pad(base_convert(implode($id), 2, 36), 19, self::_nextChar(), STR_PAD_BOTH);
        $suffix = str_pad(base_convert(mt_rand(), 10, 36), 6, self::_nextChar(), STR_PAD_BOTH);

        return $prefix . self::_nextChar() . $id . $suffix;
    }

}
