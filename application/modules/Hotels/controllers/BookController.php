<?php
class Hotels_BookController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;	
	private $_translator;
	private $_controllerCache;
	
    public function init()
    {
        /* Initialize action controller here */				
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->_translator);	
		$this->view->setEscape('stripslashes');	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();						
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template;	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/'.$front_template['theme_folder']);
		$this->_helper->layout->setLayout($template_obj->getLayout());	
		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = 1;
		}	
			
	}
	
	private function serializePostData($posted_data)
	{
		if($posted_data && $posted_data['filter'] && $posted_data['filter']['filters'])
		{
			foreach($posted_data['filter']['filters'] as $fieldObj)
			{
				$posted_data[$fieldObj['field']]  = $fieldObj['value'];
			}
		}
		return $posted_data;
	}
	
	public function bookingAction()
	{
		$hotels_title = $this->_request->getParam('hotels_title');
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		$view_datas = $hotels_db->getTitleToId($hotels_title);
		$group_datas = $group_db->getGroupName($view_datas[0]['group_id']);
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		$post_search_info = Eicra_Global_Variable::getSession()->hotel_search_info;	
		$post_search_info =	 	$this->serializePostData($post_search_info);
		if ($this->_request->isPost()) 
		{
			$posted_data = 		$this->_request->getPost();
			$posted_data =	 	$this->serializePostData($posted_data);		
			
			foreach($posted_data as $key => $variable)
			{
				if (preg_match("/apartments_no[0-9]+/", $key)) 
				{
					if(is_array($variable) && !empty($variable[0]))
					{
						$postValues[$key] = explode(',', $variable[0]);
					}
				}
				else
				{
					$postValues[$key] = $variable;
				}
			}
			$post_search_info = $postValues ;
			Eicra_Global_Variable::getSession()->hotel_search_info = $post_search_info;
			$this->view->assign('postValues', $postValues);				
		}	
		if($post_search_info)
		{
			$check_in_date = ($post_search_info[Eicra_File_Constants::HOTELS_CHECK_IN]) ? $post_search_info[Eicra_File_Constants::HOTELS_CHECK_IN] : '';
			$check_out_date = ($post_search_info[Eicra_File_Constants::HOTELS_CHECK_OUT]) ? $post_search_info[Eicra_File_Constants::HOTELS_CHECK_OUT] : '';
			if(!empty($check_in_date) && !empty($check_out_date))
			{
				$totaldayNight = $this->getNights($check_in_date, $check_out_date); 
				$notlimit_validator = $hotels_db->limitValidator($totaldayNight);
				$notlimit	= $notlimit_validator->isValid($view_datas[0]['id']);
			}
			else
			{
				$notlimit = false;	
			}
		}
		else
		{
			$notlimit = false;	
			$check_in_date = '';
			$check_out_date = '';
		}
		
		$this->view->assign('notlimit', $notlimit);
		$this->view->assign('post_search_info', $post_search_info);
		$this->view->assign('check_in_date', $check_in_date);
		$this->view->assign('check_out_date', $check_out_date);
		$this->view->assign('view_datas', $view_datas);	
		$this->view->assign('group_datas', $group_datas);	
		$this->view->assign('hotels_title', $hotels_title);	
		$this->view->roomCalendar_db = new Hotels_Model_DbTable_Calendar();		
	}	
	
	
	public function invoiceAction()
	{		
		Eicra_Global_Variable::getSession()->postValues  =	($this->_request->isPost() && $this->_request->getPost()) ? $this->_request->getPost() : Eicra_Global_Variable::getSession()->postValues;
		Eicra_Global_Variable::getSession()->returnLink = $this->view->url();	
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Frontend-Login';
		//Eicra_Global_Variable::checkSession($this->_response,$url);
		
		if($this->_request->isPost())
		{
			$posted_data = $this->_request->getPost();
			foreach($posted_data as $key => $variable)
			{
				if (preg_match("/apartments_no[0-9]+/", $key)) 
				{
					if(is_array($variable) && !empty($variable[0]))
					{
						$postValues[$key] = explode(',', $variable[0]);
					}
				}
				else
				{
					$postValues[$key] = $variable;
				}
			}
		}
		else if(Eicra_Global_Variable::getSession()->postValues)
		{
			$postValues = Eicra_Global_Variable::getSession()->postValues;		
		}
		
		if(Eicra_Global_Variable::getSession()->cart_result)
		{
			$cart_result = Eicra_Global_Variable::getSession()->cart_result;
			$next_count = count($cart_result);
			if($postValues != '' && $this->_request->isPost())
			{
				$cart_result[$next_count] = $postValues;
			}
		}
		else
		{
			if($postValues != '' && $this->_request->isPost())
			{
				$cart_result[0] = $postValues;
			}
			else
			{
				$cart_result = null;
			}
		}
		Eicra_Global_Variable::getSession()->cart_result = $cart_result;
		
		if(!empty($cart_result))
		{			
			$invoice_arr = $this->generateInvoice($cart_result);
			if($invoice_arr['status'] == 'ok')
			{
				Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
			}
		}
						
		$this->view->assign('invoice_arr', $invoice_arr);			
	}
	
	public function confirmAction()
	{	
		$cart_result = Eicra_Global_Variable::getSession()->cart_result;
		if(!empty($cart_result))
		{
			$invoice_arr = $this->generateInvoice($cart_result);
			if($invoice_arr['status'] == 'ok')
			{
				Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
			}
		}
		$this->view->assign('invoice_arr', $invoice_arr);		
		$this->assignLogin();
		$this->view->logindetails = new Members_Form_LoginForm();	
		$this->dynamicUploaderSettings($this->view->form_info);	
	}
	
	private function dynamicUploaderSettings($info)
	{
		$param_fields = array(
								'table_name' => 'forms', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['id'],
								'file_path_field'	=>	'attach_file_path', 
								'file_extension_field'	=>	'attach_file_type', 
								'file_max_size_field'	=>	'attach_file_max_size'
						);		
			$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
			$requested_data = $portfolio_model->getRequestedData();
			$settings_info = $portfolio_model->getSettingInfo();
			$merge_data = array_merge($requested_data, $settings_info);	
			$this->view->assign('settings_info' ,	$merge_data);		
			$settings_json_info = Zend_Json_Encoder::encode($merge_data);	
			$this->view->assign('settings_json_info' ,	$settings_json_info);	
	}
	
	private function assignLogin()
	{
		$template_id_field	=	'role_id';
		$settings_db = new Invoice_Model_DbTable_Setting();
		$template_info = $settings_db->getInfoByModule('Hotels');
		if($template_info)
		{			
			$selected_role_id = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator('hotels_invoice_register_role_id');									
		}
		else
		{					
			$selected_role_id =  $this->_translator->translator('hotels_invoice_register_role_id');
		}
		//echo $selected_role_id;
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
		{
			$registrationForm =  new Members_Form_UserForm ($role_info);
			if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
			$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';
			
			$this->view->selected_role_id	=	$selected_role_id;					
			$this->view->registrationForm = $registrationForm;
		}
		else
		{
			throw new Exception("Register page not found");
		}		
	}
	
	public function removeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		if($this->_request->isPost())
		{
			try
			{
				$cart_id = $this->_request->getPost('cart_id');
				$sub_cart_id = $this->_request->getPost('sub_cart_id');
				$cart_result = Eicra_Global_Variable::getSession()->cart_result;				
				unset($cart_result[$cart_id]['room_type_id'][$sub_cart_id]);			
				if($this->checkEmpty($cart_result[$cart_id]['room_type_id'], $cart_result[$cart_id]))
				{
					unset($cart_result[$cart_id]);
				}
				Eicra_Global_Variable::getSession()->cart_result = $cart_result;
				if(!empty($cart_result))
				{			
					$invoice_arr = $this->generateInvoice($cart_result);
					if($invoice_arr['status'] == 'ok')
					{
						Eicra_Global_Variable::getSession()->invoice_arr = $invoice_arr['invoice_arr'];									
					}
				}	
				$json_arr = array('status' => 'ok');
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
	}
	
	private function checkEmpty($room_type_id, $dataInfo)
	{
		$empty = true;
		foreach($room_type_id as $key=>$value)
		{														
			if(!empty($value) && is_array($dataInfo['apartments_no'.$key]) && !empty($dataInfo['apartments_no'.$key][0]))
			{
				$empty = false;
			}
		}
		return $empty;
	}
	
	private function generateInvoice($cart_result)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$country_db = new Eicra_Model_DbTable_Country();
		$payment_db = new Paymentgateway_Model_DbTable_Gateway();
		$payment_info = $payment_db->getDefaultGateway();
		$auth = Zend_Auth::getInstance ();		
		
			$global_conf = Zend_Registry::get('global_conf');
			$currency = new Zend_Currency($global_conf['default_locale']);
			$currencySymbol = $currency->getSymbol(); 
			$currencyShortName = $currency->getShortName();
			$marchent	=	new Invoice_Controller_Helper_Marchent($global_conf);
			$preferences_db = new Hotels_Model_DbTable_Preferences();
			$preferences_info = $preferences_db->getOptions();
							
			$room_type_db = new Hotels_Model_DbTable_RoomType();
			$room_db = new Hotels_Model_DbTable_Room();
			$hotels_db		=	new Hotels_Model_DbTable_Hotels();
			
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity();	
				$user_id = $globalIdentity->user_id;
				foreach($globalIdentity as $globalIdentity_key => $globalIdentity_value)
				{
					$mem_info[$globalIdentity_key] = $globalIdentity_value;
					$invoice_arr[$globalIdentity_key] = $globalIdentity_value;
				}
				if($mem_info['country'])
				{
					$country_info = $country_db->getInfo($mem_info['country']);
					$mem_info['country_name'] = stripslashes($country_info['value']);
				}
				//invoice_arr assigning started
				$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $global_conf['payment_user_id'];
				$invoice_arr[Eicra_File_Constants::INVOICE_TO] = stripslashes($mem_info['title']).' '.stripslashes($mem_info['firstName']).' '.stripslashes($mem_info['lastName']).'<br />'.stripslashes($mem_info['mobile']).'<br />'.stripslashes($mem_info['city']).', '.stripslashes($mem_info['state']).', '.stripslashes($mem_info['postalCode']).'<br />'.stripslashes($country_info['value']).'<BR />'.stripslashes($mem_info['username']);
				$invoice_arr[Eicra_File_Constants::PAY_TO] = nl2br($global_conf['payment_pay_to_text']);
				$invoice_arr[Eicra_File_Constants::INVOICE_LOGO] = stripslashes($global_conf['payment_logo_url']);
				$invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]	= date("Y-m-d h:i:s");
				$invoice_arr[Eicra_File_Constants::INVOICE_DATE] = strftime("%d/%m/%Y",strtotime($invoice_arr[Eicra_File_Constants::INVOICE_CREATE_DATE]));
				$invoice_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = $mem_info['username'];
				$invoice_arr['site_name']	=	stripslashes($global_conf['site_name']);
				$invoice_arr['site_url']	=	stripslashes($global_conf['site_url']);
				$this->view->mem_info = $mem_info;
			}
			
			//invoice_items_arr assigning started
			$total_amount = 0;
			$total_tax = 0;
			$invoice_items_arr = array();
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] = '<table id="invoiceitemstable" border="0" width="100%" cellspacing="0" align="center" style="border:1px solid #D6D6D6;">
												<tbody>
												<tr style="background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset; font-size:13px; color:#272727; text-shadow:1px 1px 1px #FFF;">
													<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="65%" height="48" align="center"><strong>'.$this->_translator->translator("hotels_invoice_desc_title").'</strong></td>
													<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6;" width="30%" height="48" align="center"><strong>'.$this->_translator->translator("hotels_invoice_amount_title").'</strong></td>
												</tr>';
			$marchent_email_id_arr = array();
			$item_arr = 0;
			foreach($cart_result as $cart_result_key => $dataInfo)
			{	
				if(!empty($dataInfo['hotel_id']) && !empty($dataInfo['hotel_id'][0]))
				{						
					$search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $dataInfo['hotel_id'][0]);	
					$list_info =  $hotels_db->getListInfo(null, $search_params, array('owner_country' => array( 'owner_country_name' => 'ocut.value'), 'userChecking' => false)) ;
					if($list_info)
					{	
						foreach($list_info as $info_arr)
						{		
							$hotels_info = (is_array($info_arr)) ? $info_arr : $info_arr->toArray();
						}
					}
					$marchent_email_id_arr[$cart_result_key] = $hotels_info['username'];
					$total_night = (int)$this->getNights($dataInfo[Eicra_File_Constants::HOTELS_CHECK_IN], $dataInfo[Eicra_File_Constants::HOTELS_CHECK_OUT]);
					$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_OBJECT_VALUE] =   Zend_Json_Encoder::encode($dataInfo);
					$item_details = '<tr><td colspan="2" style="height:8px"></td></tr>
									 <tr>
										<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
										'.stripslashes($hotels_info['hotels_name']).'
										</td>
									</tr>';
					$invoice_items_arr[$item_arr]['inv']['hotels_name']	=	stripslashes($hotels_info['hotels_name']);
					
					$item_total = 0;
					
					if($dataInfo['room_type_id'])
					{
						$room_type_id_str = implode(',', $dataInfo['room_type_id']); 
						$room_type_search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $room_type_id_str);
						$room_type_search_params['filter']['logic'] = ($room_type_search_params['filter']['logic']) ? $room_type_search_params['filter']['logic'] : 'and';	
						$room_type_search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
						if(!empty($dataInfo[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($dataInfo[Eicra_File_Constants::HOTELS_CHECK_OUT]))
						{
							$room_type_search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																			'filters' => array(																			  
																							 array(
																									'logic' => 'AND',
																									'filters' => array( 
																														array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $total_night),
																														array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																													)
																								),
																							 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																							)
																		 );	
						}
						$room_type_info_obj = $room_type_db->getListInfo('1', $room_type_search_params , false);
						
						if($room_type_info_obj)
						{
							foreach($dataInfo['room_type_id']	as $key => $room_type_id_value)
							{	
								$sub_total = 0;										
								foreach($room_type_info_obj as $room_type_info)
								{							
									$room_type_info['basic_price']			=	$room_type_info['price_per_night'];
									$room_type_info['descount_price']		=	(!empty($room_type_info['price_after_discount'])) ? $room_type_info['price_after_discount'] : $room_type_info['price_per_night'];
																				
									if(!empty($room_type_info) && ($room_type_info['id'] == $room_type_id_value)  && is_array($dataInfo['apartments_no'.$key]) && !empty($dataInfo['apartments_no'.$key][0]))
									{												
										$room_item_details = '';
										$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['price_setting_type'] = $room_type_info['price_setting_type'];
																			
										
											$room_info = $room_db->getAvailableRooms($room_type_info['id'], $dataInfo);
											$totalDays	=	$total_night + 1;
											if($room_info)
											{										
												$cd	 = 0;									
												foreach($room_info as $room_infos_key => $room_infos_arr )
												{	
																							
													if(in_array($room_infos_arr['id'],$dataInfo['apartments_no'.$key]))
													{
														$room_item_details .= '<table border="0" width="100%" align="center">';	
														$room_item_details .= '<tr>';
																									
														$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['room_name'] = stripslashes($room_infos_arr['room_name']);
																
																$room_item_details .= '<td colspan="2"><fieldset style="border:1px solid #0896FF; background-color:#FFF; margin:20px 0 10px 0;">';													
																$room_item_details .= '<legend style="font-weight:normal; font-size:17px; letter-spacing:0; color:#FFFFFF; margin:10px 0; background-color:#0896FF;">'.$this->view->numbers(stripslashes($room_infos_arr['room_name'])).' - ( '.$currencySymbol.' '.$this->view->numbers(number_format($this->view->price($room_type_info['descount_price']), 2, '.', ',')).' '.$currencyShortName.'&nbsp;'.$this->_translator->translator("hotels_front_page_list_head_no_apartments_per_night").' )</legend>';
																
																$room_item_details .= '<table border="1" bordercolor="#E5E5E5" style="width:98%; border:1px solid #E5E5E5; margin:auto; text-align:center; overflow:hidden; border-collapse:collapse">';
																	$bd = 0;
																	for($d = strtotime($dataInfo[Eicra_File_Constants::HOTELS_CHECK_IN]); $d <= strtotime("-1 day", strtotime($dataInfo[Eicra_File_Constants::HOTELS_CHECK_OUT])); $d = strtotime("+1 day", $d))
																	{
																		$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['per_night'] = date("Y-m-d", $d) .' -- '.date("Y-m-d", strtotime("+1 day", $d));
																		$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['is_seasonal_price']	=	((int)$d >= (int)strtotime($room_type_info['season_start_date']) && (int)$d <= (int)strtotime($room_type_info['season_end_date'])) ? true : false;
																		$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['per_night_price'] = ($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['is_seasonal_price'] === true) ? $this->view->price($room_type_info['seasonal_price_per_night']) : $this->view->price($room_type_info['descount_price']);
																		$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['plan_name'] = ($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['is_seasonal_price'] === true) ? $this->view->escape($room_type_info['plan_name']) : '';
																		$sub_total +=	($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['is_seasonal_price'] === true) ? $this->view->price($room_type_info['seasonal_price_per_night']) : $this->view->price($room_type_info['descount_price']);
																		
																		$room_item_details .= '<tr>';
																			$room_item_details .= '<td style="font-weight:normal; font-size:12px; line-height:22px; letter-spacing:0; color:#004FC4; margin:15px 0;">';
																			$room_item_details .= $this->view->numbers($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['per_night']);
																			$room_item_details .= '</td>';
																			$room_item_details .= '<td  style="font-weight:normal; font-size:12px; line-height:22px; letter-spacing:0; color:#004FC4; margin:15px 0; text-align:left; padding-left: 5px;">';
																			$room_item_details .= $currencySymbol.' '.$this->view->numbers(number_format($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['per_night_price'], 2, '.', ',')).' '.$this->_translator->translator("hotels_front_page_list_head_no_apartments_per_night");
																			$room_item_details .= ($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['is_seasonal_price'] === true) ? ' <span style="font-weight:bold;color:#F60;">('.$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['roomCalendar_info'][$cd]['per_book_date'][$bd]['plan_name'].')</span>' : '';
																			$room_item_details .= '</td>';																		
																		$room_item_details .= '</tr>';
																		$bd++;
																	}															
																$room_item_details .= '</table>';
															$room_item_details .= '</fieldset></td>';
														$room_item_details .= '</tr>';
														$room_item_details .= '</table>';
														$cd++;
													}
												}
												$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax'] = ($room_type_info['room_tax_type'] == '2') ? $sub_total * ($room_type_info['room_tax'] / 100) : $room_type_info['room_tax'];
												$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax_type'] = $room_type_info['room_tax_type'];
												$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax_original'] = $room_type_info['room_tax'];
												
												if(!empty($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax']))
												{
													$sub_total += $invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax'];
													$room_item_details .= '<table border="0" width="100%" align="center">';	
															$room_item_details .= '<tr>';
																$room_item_details .= '<td colspan="2"><fieldset style="border:1px solid #0896FF; background-color:#FFF; margin:20px 0 10px 0;">';													
																	$room_item_details .= '<legend style="font-weight:normal; font-size:17px; letter-spacing:0; color:#FFFFFF; margin:10px 0; background-color:#0896FF;">'.$this->_translator->translator("hotels_room_type_price_details_tax_title").' '.$currencySymbol.' '.$this->view->numbers(number_format($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax'], 2, '.', ',')).' '.$currencyShortName.(($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax_type'] == '2') ?  ' ('.$this->view->numbers($invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_tax_original']).'%)' : '').'</legend>';
																	
																$room_item_details .= '<table border="1" bordercolor="#E5E5E5" style="width:98%; border:1px solid #E5E5E5; margin:auto; text-align:center; overflow:hidden; border-collapse:collapse">';
																	$room_item_details .= '<tr>';
																		$room_item_details .= '<td style="font-weight:normal; font-size:12px; line-height:22px; letter-spacing:0; color:#004FC4; margin:15px 0;">';
																		$room_item_details .= $this->_translator->translator("hotels_invoice_total_title").' :';
																		$room_item_details .= '</td>';
																		$room_item_details .= '<td  style="font-weight:normal; font-size:12px; line-height:22px; letter-spacing:0; color:#004FC4; margin:15px 0; text-align:left; padding-left: 5px;">';
																		$room_item_details .= $currencySymbol.' '.$this->view->numbers(number_format($sub_total , 2, '.', ',')).' '.$currencyShortName;
																		
																		$room_item_details .= '</td>';																		
																	$room_item_details .= '</tr>';												
																$room_item_details .= '</table>';
																
															$room_item_details .= '</fieldset></td>';
														$room_item_details .= '</tr>';
													$room_item_details .= '</table>';
												}
											}
										
										$item_details .= '<tr><td colspan="2" style="height:2px"></td></tr><tr>
												<td id="invoiceitemsrow" style="padding:10px; border-bottom:1px solid #D6D6D6;">'.
													'<table width="100%">
														<tr>
															<td><img src="'.$this->view->serverUrl().$this->view->baseUrl().'/data/frontImages/hotels/room_type_image/'.$room_type_info['primary_image'].'" height="60" title="'.stripslashes($room_type_info['room_type']).'" style="border:1px solid #DFDFDF; background-color:#FFF; padding:5px; margin: 2px 4px 2px 0px; float:left;"/>'
															.'<span style="color:#F60; font-weight:bold; font-style:italic; text-decoration:underline;">'.stripslashes($room_type_info['room_type']).'</span>'.
															'<br />'
															.'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("hotels_invoice_check_in").' :</span> '.$this->view->numbers($dataInfo[Eicra_File_Constants::HOTELS_CHECK_IN]).
															'<br  />'.
															'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("hotels_invoice_check_out").' :</span> '.$this->view->numbers($dataInfo[Eicra_File_Constants::HOTELS_CHECK_OUT]).
															'<br  />'.
															'<span style="color:#06C; line-height:20px; font-weight:bold;">'.$this->_translator->translator("hotels_invoice_room_condition").' :</span> '.$room_type_info['room_condition'].
															'</td>'.														
														'</tr>'.
														'<tr>'.
															'<td style="padding-top:20px">'.
																$room_item_details.
															'</td>'.
														'</tr>'.
													'</table>'.
												'</td>'.
												'<td id="invoiceitemsrow" style="padding:10px; border-left:1px solid #D6D6D6; border-bottom:1px solid #D6D6D6; line-height: 22px;"valign="top">'.
													'<table border="0" width="100%">'.
														'<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_total_night").'</span> : '.$this->view->numbers($total_night).'</td>'.
														'</tr>';
														
										$item_details .= '<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_total_day").'</span> : '.$this->view->numbers(($total_night + 1)).'</td>'.
														'</tr>';
														
										$item_details .= '<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_room_no_chose").'</span> : '.$this->view->numbers(count($dataInfo['apartments_no'.$key])).'</td>'.
														'</tr>'.
														'<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_room_cost").'</span> : '.$currencySymbol.' '.$this->view->numbers(number_format($sub_total, 2, '.', ',')).' '.$currencyShortName.'</td>'.
														'</tr>';
													if(Settings_Service_Price::is_exists('3', $global_conf, $sub_total))
													{
														$booking_fee = Settings_Service_Price::getMargine('3');
														$booking_fee_amount	=	$this->view->price($sub_total, null, '3');
														$sub_total = $sub_total + $booking_fee_amount;
														$booking_fee_show	=	(preg_match("/%/i", $booking_fee)) ? $currencySymbol.' '.$booking_fee_amount.' '.$booking_fee : $booking_fee;
														$item_details .= '<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_booking_fee").'</span> : '.$this->view->numbers($booking_fee_show).'</td>'.
														'</tr>';
														$item_details .= '<tr>'.
															'<td><span style="font-weight:bold;">'.$this->_translator->translator("hotels_invoice_total_book_fee_title").' : </span><span style="font-weight:bold;line-height:22px;color:#F60;"> '.$currencySymbol.' '.$this->view->numbers(number_format($sub_total, 2, '.', ',')).' '.$currencyShortName.'</span></td>'.
														'</tr>';
													}							
									$item_details .= '</table>'.
												'</td>
											</tr>';	
											$item_total += 	$sub_total;	
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key][Eicra_File_Constants::INVOICE_BOOK_FEE_SHOW] = $booking_fee_show;
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['descount_price'] = $room_type_info['descount_price'];
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['sub_total'] = $sub_total;
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_type_id']	=	stripslashes($room_type_info['id']);
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_type']	=	stripslashes($room_type_info['room_type']);
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_type_image']	=	stripslashes($room_type_info['primary_image']);		
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key][Eicra_File_Constants::HOTELS_CHECK_IN]	=	$dataInfo[Eicra_File_Constants::HOTELS_CHECK_IN];	
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key][Eicra_File_Constants::HOTELS_CHECK_OUT]	=	$dataInfo[Eicra_File_Constants::HOTELS_CHECK_OUT];	
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['room_condition']	=	$room_type_info['room_condition'];	
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['session_key'] = $cart_result_key.'-'.$key;
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['total_night'] = $total_night;
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['apartments_no'] = count($dataInfo['apartments_no'.$key]);
											$invoice_items_arr[$item_arr]['inv']['hotels_room'][$key]['apartments_arr'] = $dataInfo['apartments_no'.$key];
									}																		
								}
							}
						}
					}	
					$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_AGENT_ID]	=	$hotels_info['hotels_agent'];					
					$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TOTAL] = $item_total;
					$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_TAX] = '0';
					$total_tax = 0;							
					$total_amount += $item_total;
					$invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS] = $item_details;
					$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= $invoice_items_arr[$item_arr][Eicra_File_Constants::INVOICE_ITEM_DETAILS];
					$item_arr++;
					if($hotels_info && $hotels_info['billing_item_desc'])
					{
						$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr><td colspan="2" style="padding:26px 0px 15px 10px;"><strong>'.$this->_translator->translator("hotels_block_featured_label_description").'</strong>'.stripslashes($hotels_info['billing_item_desc']).'</td></tr>';
					}
				}
			}
			
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">
						<td id="invoiceitemsheading" style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40">
						<div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("hotels_invoice_total_title").'</div>
						</td>
						<td id="invoiceitemsheading" style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding: 10px; text-align:right;"><strong>'.$currencySymbol.' '.$this->view->numbers(number_format(($total_amount+$total_tax), 2, '.', ',')).' '.$currencyShortName.'</strong></td>
					</tr>';	
			
			/*********ASSIGN PAYMENT INFO AND PAY TO START*******************/	
			$hotels_info['item'] = $preferences_info;
			if($marchent->isMarchentPaymentEnable(array('item_count' => $item_arr, 'dataInfo' => $hotels_info)))
			{
				$invoice_arr[Eicra_File_Constants::PAY_TO] = '<strong>'.stripslashes($hotels_info['owner_name']).'</strong>';
				$invoice_arr[Eicra_File_Constants::PAY_TO] .=  ($hotels_info['package_name']) ? '<br /><strong>'.$hotels_info['package_name'].'</strong>' : '';
				//if($dataInfo['item']['is_profile'] == 'YES')
				//{
					//if($dataInfo['item']['is_telephone'] == 'YES')
					//{
						$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($vacationrentals_info['owner_phone']) ? '<br /><strong>'.$this->_translator->translator('hotels_api_expedia_backend_view_client_phone_title').'</strong> '.stripslashes($hotels_info['owner_phone']) : '';
					//}
					$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($hotels_info['owner_address']) ? '<br />'.stripslashes($hotels_info['owner_address']) : '';
					$invoice_arr[Eicra_File_Constants::PAY_TO] .= ($hotels_info['owner_country_name']) ? '<br />'.stripslashes($hotels_info['owner_country_name']) : '';
					$invoice_arr[Eicra_File_Constants::PAY_TO] .= '<br />'.stripslashes($hotels_info['owner_email']);
				//}			
				$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_USER_ID]  = $hotels_info['hotels_agent'];
			}
			/*********ASSIGN PAYMENT INFO AND PAY TO END*******************/		
						
			$services_charge = 0;
			if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
			{
				$services_charge = $this->view->price($total_amount, null,'4');
				$services_charge_margine = Settings_Service_Price::getMargine('4');
				$services_charge_margine_show	=	(preg_match("/%/i", $services_charge_margine)) ? $currencySymbol.' '.number_format($services_charge, 2, '.', ',').' '.$services_charge_margine : $services_charge_margine;
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("hotels_invoice_service_charge").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$this->view->numbers($services_charge_margine_show).'</strong></td>'.
				'</tr>';
			}
			$now_payable = 0;
			$deposit_charge = 0;
			if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount))
			{	
				$deposit_charge		= $this->view->price($total_amount, null, '5');
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("hotels_invoice_deposit_charge", $this->view->numbers(Settings_Service_Price::getMargine('5'))).'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.$this->view->numbers(number_format($deposit_charge, 2, '.', ',')).' '.$currencyShortName.'</strong></td>'.
				'</tr>';
				
				$now_payable	=	$services_charge	+	$deposit_charge;
				
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#E7FFDA; font-size:13px; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#090; font-weight:bold; line-height:20px;">'.$this->_translator->translator("hotels_invoice_deposit_payable").'</div></td><td style="color:#090; font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.$this->view->numbers(number_format($now_payable, 2, '.', ',')).' '.$currencyShortName.'</strong></td>'.
				'</tr>';
			}
			if(Settings_Service_Price::is_exists('4', $global_conf, $total_amount))
			{	
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="text-align:right; background-color:#EFEFEF; box-shadow: 0 1px 1px #FFFFFF inset;">'.
					'<td style="border-bottom:1px solid #D6D6D6; padding: 10px;" width="65%" height="40"><div style="color:#06C; font-size:12px; font-weight:bold; line-height:20px;">'.$this->_translator->translator("hotels_invoice_grand_total").'</div></td><td style="font-size:13px; line-height:20px; border-bottom:1px solid #D6D6D6; border-left:1px solid #D6D6D6; padding:10px; text-align:right;"><strong>'.$currencySymbol.' '.$this->view->numbers(number_format(($total_amount+$total_tax+$services_charge), 2, '.', ',')).' '.$currencyShortName.'</strong></td>'.
				'</tr>';
			}
			if(Settings_Service_Price::is_exists('5', $global_conf, $total_amount) && !empty($now_payable))
			{
				$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .=  '<tr style="padding:10px 5px 10px 0px; background-color:#FDCEA4;">'.
					'<td style=" border-top:1px solid #FF9900; border-bottom:1px solid #FF9900; padding: 10px; text-align:right; font-weight:bold;" height="40" colspan="2"><strong>'.$this->_translator->translator("hotels_invoice_later_payable", $currencySymbol.' '.$this->view->numbers(number_format((($total_amount+$total_tax+$services_charge) - $now_payable), 2, '.', ',')).' '.$currencyShortName).'</strong></td>'.
				'</tr>';
			}
			$invoice_arr[Eicra_File_Constants::INVOICE_TABLE] .= '</tbody>
																</table>';
			
			$invoice_arr[Eicra_File_Constants::MARCHENT_EMAIL_ID] = ($marchent_email_id_arr[0]) ? implode(',', $marchent_email_id_arr) : '';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CHARGE] = $services_charge;
			$invoice_arr[Eicra_File_Constants::INVOICE_DEPOSIT_CHARGE] = $deposit_charge;
			$invoice_arr[Eicra_File_Constants::INVOICE_NOW_PAYABLE] = $now_payable;
			$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_AMOUNT] = $total_amount;
			$invoice_arr[Eicra_File_Constants::INVOICE_TOTAL_TAX] = $total_tax;	
			$invoice_arr[Eicra_File_Constants::INVOICE_PAYMENT_INFO] = $payment_info;
			$invoice_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT]	=	$currencySymbol.' '.number_format((($total_amount+$total_tax+$services_charge)), 2, '.', ',').' '.$currencyShortName;
			
			
			//Initialize Invoice Action
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE] 			= 'Hotels';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CREATE_ACTION] 	= 'createItinerary';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UPDATE_ACTION] 	= '';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_DELETE_ACTION] 	= 'deleteItinerary';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_QUERY_ACTION] 	= '';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_PAID_ACTION] 	= 'paidItinerary';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_UNPAID_ACTION] 	= 'unpaidItinerary';
			$invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_CANCEL_ACTION] 	= 'cancelItinerary';
			
			//Initialize Email Template
			$template_id_field	=	'default_template_id';
			$settings_db = new Invoice_Model_DbTable_Setting();
			$template_info = $settings_db->getInfoByModule($invoice_arr[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);			
			$invoice_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("hotels_invoice_template_id");									
			
									
			$templates_arr = $this->getLetterBody($invoice_arr, $invoice_arr['letter_id']);
			$invoice_arr[Eicra_File_Constants::INVOICE_DESC] = stripslashes($templates_arr['templates_desc']);
			$invoice_arr[Eicra_File_Constants::INVOICE_SUBJECT] = stripslashes($templates_arr['templates_title']);
			$invoice_arr[Eicra_File_Constants::INVOICE_ITEM_OBJECT] = $invoice_items_arr;
			
			$return = array('status' => 'ok','invoice_arr' => $invoice_arr);
		
		return 	$return;
	}
	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?','invoice_template')
								->limit(1);
		}
		else
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.id = ?',$letter_id)
								->limit(1);
		}
							
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$templates_arr = $row ;
			}
		}
		else
		{
			$templates_arr['templates_desc'] = '';
		}			
		foreach($datas as $key=>$value)
		{
			$templates_arr['templates_desc'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_desc']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_desc']);
			$templates_arr['templates_title'] = (!empty($value) && !is_array($value)) ? str_replace('%'.$key.'%',$value,$templates_arr['templates_title']) : str_replace('%'.$key.'%','&nbsp;',$templates_arr['templates_title']);
		}
		return $templates_arr;
	}
	
	private function getNights($sStartDate, $sEndDate)
	{		
		$sStartDate =strtotime($sStartDate);
		$sEndDate = strtotime($sEndDate);
		
		$datediff = $sEndDate - $sStartDate;
		$aDays	= floor($datediff/(60*60*24));
		
		return $aDays;
	}
	
	public function registerAction()
    {
		
		$selected_role_id = ($this->_request->getPost('role_id')) ? $this->_request->getPost('role_id') : $this->_request->getParam('role_id');	
		
		if(!empty($selected_role_id))
		{
			try
			{
				$role_db = new Members_Model_DbTable_Role();
				$role_info = $role_db->getRoleInfo($selected_role_id);
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($role_info['form_id']);
				$this->view->form_info = $form_info;
				$this->view->role_info = $role_info;
			}
			catch(Exception $e)
			{
				$role_info = null;
			}
		}
		else
		{
			$role_info = null;
		}		
		
		$registrationForm =  new Members_Form_UserForm ($role_info);
		if($registrationForm->role_id){ $registrationForm->removeElement('role_id'); }	
		$loginUrl = $this->view->serverUrl().$this->view->baseUrl().'/Members-Login';	
			
        if ($this->_request->isPost()) 
		{			
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$elements = $registrationForm->getElements();
			foreach($elements as $element)
			{
				$element_name = $element->getName();
				if($element->getType() == 'Zend_Form_Element_File')
				{					
					$registrationForm->removeElement($element_name);
				}
				$element_name_arr = explode('_',$element_name);
				if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
				{
					$package_msg = 'ok';
				}
			}
			
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			try
			{	
				if($role_info['role_lock'] == '1' && $role_info != null)
				{			
									
					if ($registrationForm->isValid($this->_request->getPost())) 
					{
						$registrationForm =  new Members_Form_UserForm ($role_info);
						$registrationForm->populate($this->_request->getPost());
						$username = $this->_request->getPost('username');
						$user_check = new Members_Controller_Helper_Registers(); 
									
						if($user_check->getUsernameAvailable($username))
						{
							if($this->_request->getPost('password') == $this->_request->getPost('confirmPassword'))
							{
								$fromValues = $registrationForm;
								$members = new Members_Model_Members($this->_request->getPost());
								$perm = new Members_View_Helper_Allow();
								$members->setRole_id($selected_role_id);							
																															
								if($role_info)
								{
									if($role_info['auto_approve'] == '1')
									{							
										$members->setStatus(1);
									}
									else
									{
										$members->setStatus(0);
									}
								}
								else
								{
									$members->setStatus($translator->translator("set_frontend_registration_auto_publish", '', 'Members'));
								}	
														
								$members->setLoginurl($loginUrl);
										
								$result = $members->saveRegister();
								
								if($result['status'] == 'ok')
								{
									$msg = $translator->translator("member_registered_successfull", '', 'Members');
									$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
									if(!empty($role_info) && !empty($role_info['form_id']))
									{
										$field_db = new Members_Model_DbTable_Fields();
										$packageObj = new Members_Controller_Helper_Packages();
										$field_groups = $field_db->getGroupNames($form_info['id']); 
										
										//Add Data To Database
										foreach($field_groups as $group)
										{
											$group_name = $group->field_group;
											$displaGroup = $registrationForm->getDisplayGroup($group_name);
											$elementsObj = $displaGroup->getElements();
											foreach($elementsObj as $element)
											{
												$table_id = $result['id'];
												$form_id = $form_info['id'];
												$field_id	=	$element->getAttrib('rel');
												$field_value	=	($element->getType() == 'Zend_Form_Element_File') ? $this->_request->getPost($element->getName()) : $element->getValue();
												if($element->getType() == 'Zend_Form_Element_MultiCheckbox') 
												{ 
													if(is_array($field_value)) { $field_value = ''; }
												}
												if($element->getType() == 'Zend_Form_Element_Multiselect') 
												{ 
													$field_value	=	$this->_request->getPost($element->getName());
													if(is_array($field_value)) { $field_value = implode(',',$field_value); }
												}
												try
												{
													$data = array('table_id' => $table_id, 'form_id' => $form_id, 'field_id' => $field_id, 'field_value' => $field_value);
													$DBconn = Zend_Registry::get('msqli_connection');
													$DBconn->getConnection();
													$DBconn->insert(Zend_Registry::get('dbPrefix').'forms_fields_values',$data);
													if($package_msg == 'ok')
													{
														$e_name = $element->getName();
														$element_name_arr = explode('_',$e_name);
														if($element->getType() == 'Zend_Form_Element_Select' && in_array(Eicra_File_Constants::PACKAGE,$element_name_arr,true))
														{
															$package_info = $packageObj->getFormAllDatas($field_value);
														}
													}
													$msg = $translator->translator("member_registered_successfull",'' ,'Members');
													$json_arr = array('status' => 'ok','msg' => $msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
												catch(Exception $e)
												{
													$msg = $translator->translator("member_registered_err",'' ,'Members');
													$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage(), 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
												}
											}
										}
										
										//Email Send
										$register_helper = new Members_Controller_Helper_Registers();
										$field_info = $field_db->getFieldsInfo($role_info['form_id']);
										$allDatas = $members->getAllDatas();
										
										if($field_info)
										{
											$atta_count = 0;
											$attach_file_arr = array();
											foreach($field_info as $element)
											{
												if($element->field_type == 'file')
												{
													$attach_file_arr[$atta_count] = $allDatas[$element->field_name];
													$atta_count++;
												}
											}
										}
										else
										{
											$attach_file_arr = null;
										}
										
										$allDatas['status'] = ($data['status'] == '1') ? 'active' : 'inactive';
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();
										try 
										{
											$register_helper->sendMail($allDatas,$form_info,$attach_file_arr);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
										//Delete Attached Files
										if($form_info['attach_file_delete'] == '1')
										{
											if($attach_file_arr != null)
											{
												foreach($attach_file_arr as $key=>$value)
												{
													if(!empty($value))
													{	
														$dir = BASE_PATH.DS.$form_info['attach_file_path'].DS.$value;
														$res = Eicra_File_Utility::deleteRescursiveDir($dir);	
													}								
												}
											}
										}
										if(!empty($package_msg) && !empty($package_info))
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => $package_msg, 'package_info' => $package_info, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
										else
										{
											$json_arr = array('status' => 'ok','msg' => $msg.' '.$mail_msg, 'package_msg' => 'err', 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}
									}
									else
									{
										$register_helper = new Members_Controller_Helper_Registers();
										$allDatas['status'] = ($members->getStatus() == '1') ? 'active' : 'inactive';
										$allDatas['username'] = $members->getUsername();
										$allDatas['password'] = $members->getReal_pass();
										$allDatas['loginurl'] = $members->getLoginurl();
										$allDatas['title'] = $members->getTitle();
										$allDatas['firstName'] = $members->getFirstName();
										$allDatas['lastName'] = $members->getLastName();
										$allDatas['member_package'] = '';
										try 
										{
											$register_helper->sendMail($allDatas,null,null);
										}
										catch (Exception $e) 
										{
											$mail_msg = $e->getMessage();
										}
									}
									
									//Authentication Start
									
									// Get our authentication adapter and check credentials						
									$adapter = $this->getAuthAdapter(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))) ));				
									$auth = Zend_Auth::getInstance();
									$result = $auth->authenticate($adapter);
									if (!$result->isValid()) 
									{	
										$pending_error = $this->checkPendingError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password')))) );	
											
										// Invalid credentials	
										if($pending_error)
										{
											$msg = $translator->translator("member_credentials_pending_err", '', 'Members');
										}
										else if($this->checkFrontendLoginError(array( 'username' => $members->getUsername(), 'password' => str_replace(' ' ,'',strip_tags($this->getRequest()->getPost('password'))))) )
										{
											$msg = $translator->translator("member_credentials_frontend_login_err", '', 'Members');
										}
										else
										{								
											$msg = $translator->translator("member_credentials_err", '', 'Members');
										}
										$datas['failedCause'] = $msg;										
										$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));					
									}
									else
									{
										
										Eicra_Global_Variable::getSession()->username= $result->getIdentity();
										$users= new Administrator_Model_DbTable_Users();
														
										$data= array ('last_access' => date('Y-m-d H:i:s'));
										$where= $users->getAdapter()->quoteInto('username = ?', Eicra_Global_Variable::getSession()->username);
										if (!$users->update($data,$where)) 
										{
										  $msg = $translator->translator("member_last_access_err", '', 'Members');
										  $json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
										}
										else
										{
											$userInfo = $adapter->getResultRowObject(null, array('password'));
											$authStorage = $auth->getStorage();
											$authStorage->write($userInfo);
												
											$msg = $translator->translator("member_registered_successfull",'' ,'Members').' '.$translator->translator("member_loggin_successfull",Eicra_Global_Variable::getSession()->username, 'Members').$translator->translator("member_loggin_redirecting", '', 'Members');
											$json_arr = array('status' => 'ok','msg' => $msg, 'role_id' => $userInfo->role_id, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
										}			
									 }
								}
								else
								{
									$msg = $translator->translator("member_registered_err",'' ,'Members');
									$json_arr = array('status' => 'err','msg' => $msg.' '.$result['msg'], 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
								}				
							}
							else
							{
								$msg = $translator->translator("password_not_match",'' ,'Members');
								$json_arr = array('status' => 'errP','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
							}
						}
						else
						{
							$msg = $translator->translator("member_availability",$username, 'Members');
							$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
						}
					}
					else
					{
						$validatorMsg = $registrationForm->getMessages();
						$vMsg = array();
						$i = 0;
						foreach($validatorMsg as $key => $errType)
						{					
							foreach($errType as $errkey => $value)
							{
								$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
								$i++;
							}
						}
						$json_arr = array('status' => 'errV','msg' => $vMsg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
					}			
				}
				else
				{
					$msg = $translator->translator("member_registered_not_permitted",$role_info['role_name'],'Members');
					$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));
				}
			}
			catch(Exception $e)
			{
				$msg = $e->getMessage();
				$json_arr = array('status' => 'err','msg' => $msg, 'captcha' => Members_Controller_Helper_Captcha::getCaptchaElement($registrationForm));	
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		else
		{	
			if($role_info['allow_register_to_this_role'] == '1' && $role_info != null)
			{
				$this->view->selected_role_id	=	$selected_role_id;					
				$this->view->registrationForm = $registrationForm;
			}
			else
			{
				throw new Exception("Register page not found");
			}
		}			
    }
	
	protected function getAuthAdapter($values)
	{
		$tbl = Zend_Registry::get('dbPrefix').'user_profile';
		$dbAdapter  = Zend_Registry::get('msqli_connection');		
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);			
		$authAdapter = new Zend_Auth_Adapter_DbTable(
                               $dbAdapter,
								array( 'mt' => $tbl),
								'mt.username',
								'mt.password',
                        	'MD5(CONCAT(mt.salt,?))'
                             );
		$authSelect = $authAdapter->getDbSelect();
		$authSelect->joinLeft(array('r' => Zend_Registry::get('dbPrefix').'roles'), 'mt.role_id = r.role_id', array('*'));
		$authSelect->where('r.allow_login_from_frontend = ?','1');
		$authAdapter->setIdentity($values['username']);	
		$authAdapter->setCredential($values['password']);
		
		return $authAdapter;
	}
	
	private function checkPendingError($datas)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => Zend_Registry::get('dbPrefix').'user_profile',
						'field' => 'status',
						'exclude' => 'username = "'.$datas['username'].'" AND  password = MD5(CONCAT(salt,"'.$datas['password'].'"))',
					)
				);
		return $validator->isValid(0);
	}
	
	private function checkFrontendLoginError($datas)
	{
		$mem_db = new Members_Model_DbTable_MemberList();
		$mem_info = $mem_db->getMemberInfoByUsername($datas['username']);
		if($mem_info && $mem_info['role_id'])
		{
			$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => Zend_Registry::get('dbPrefix').'roles',
							'field' => 'allow_login_from_frontend',
							'exclude' => 'role_id = "'.$mem_info['role_id'].'" ',
						)
					);
			return $validator->isValid(0);
		}
		else
		{
			return false;
		}
	}
}

