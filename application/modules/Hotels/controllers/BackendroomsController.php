<?php

class Hotels_BackendroomsController extends Zend_Controller_Action
{	
	private $hotelsRoomForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {	
        /* Initialize action controller here */		
		$this->hotelsRoomForm =  new Hotels_Form_RoomForm ();
		$this->view->hotelsRoomForm =  $this->hotelsRoomForm;
				
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);	
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {
		// action body	
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->room_type_id = $posted_data['room_type_id'];
		
		$list_mapper = new Hotels_Model_RoomListMapper();	
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');
				$room_type_id = ($this->_request->getParam('room_type_id')) ? $this->_request->getParam('room_type_id') : null;	
				
				if($room_type_id)
				{
					$posted_data['filter']['filters'][] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $room_type_id);					
				}			
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = ($posted_data['browser_url'] == 'no') ? '' : $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'room_type_id' => $room_type_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{					
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{						
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_room_name'] = str_replace('_', '-', $entry_arr['room_name']);	
							$entry_arr['room_entry_date_format']=  date('Y-m-d h:i:s A', strtotime($entry_arr['room_entry_date']));
							$entry_arr['id_format']	=	$this->view->numbers($this->view->escape($entry_arr['id']));
							$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;						
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}
		
		$mem_db	=	new Members_Model_DbTable_MemberList();
		$this->view->assign('user_list', $mem_db->getAllMembers());	
		
		$hotel_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('hotel_list', $hotel_db->getHotelsInfoByGroup());
	}
	
	public function roomtypeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			try
			{
				$ids = $this->_request->getPost('id');
				$ids_arr = explode('--',$ids);
				$id	=	$ids_arr[0];
				$RoomType_db = new Hotels_Model_DbTable_RoomType();
				$datas = $RoomType_db->getAllRoomTypeByIds($id);
				if($datas)
				{
					$json_arr = array('status' => 'ok','datas' => $datas);
				}
				else
				{
					$json_arr = array('status' => 'err');
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'msg' => $e->getMessage());
			}
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
	}
	
	public function addAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			if($this->hotelsRoomForm->isValid($this->_request->getPost())) 
			{	
				$room = new Hotels_Model_Room($this->hotelsRoomForm->getValues());
								
				$perm = new Hotels_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $room->saveRoom();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];						
						$msg = $translator->translator("hotels_room_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("hotels_room_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->hotelsRoomForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoom)
				{					
					foreach($errRoom as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}		
	}
	
	public function editAction()
	{
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			$id = $this->_request->getPost('id');
					
				
			$perm = new Hotels_View_Helper_Allow();
			if($perm->allow())
			{
				if($this->hotelsRoomForm->isValid($this->_request->getPost())) 
				{	
					$room = new Hotels_Model_Room($this->hotelsRoomForm->getValues());
					$room->setId($id);						
					$result = $room->saveRoom();
					if($result['status'] == 'ok')
					{
						$msg = $translator->translator("hotels_room_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("hotels_room_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->hotelsRoomForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoom)
					{					
						foreach($errRoom as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$room_db = new Hotels_Model_DbTable_Room();
			$room_info	=	$room_db->getRoomInfo($id);
						
			if($room_info)
			{
				if($room_info['hotel_id'])
				{
					$this->hotelsRoomForm =  new Hotels_Form_RoomForm ($room_info);
					$this->view->hotelsRoomForm =  $this->hotelsRoomForm;
				}
				else
				{
					$this->hotelsRoomForm->loadRoomType ();
				}				
				$this->view->linked_hotels_info = 	(!empty($room_info['room_type_id'])) ? $this->linkhotelsdata($room_info['room_type_id']) : null;
				$this->hotelsRoomForm->populate($room_info);
				$this->view->entry_by = $room_info['entry_by'];
				$this->view->id = $id;
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}			
	}	
	
	private function linkhotelsdata($id)
	{
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$hotels_info= $hotels_db->getHotelsInfoByGroup(null);
		
		if($hotels_info)
		{
			$hotels_arr = array();
			$c = 0; 
			foreach($hotels_info as $key=>$entry)
			{
				$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
				$room_id_arr = explode(',',$entry_arr['room_type_id']);
				if(in_array($id,$room_id_arr))
				{
				  $hotels_arr[$c] = $entry_arr;
				  $c++;
				}
			}
		}
		else
		{
			$hotels_arr = null;
		}
		
		return $hotels_arr;
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
						
			// Remove from Group
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			
			$where_calendar = array();
			$where_calendar[] = 'room_id = '.$conn->quote($id);
			try
			{
				$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room', $where);
				$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room_calendar', $where_calendar);
				
				$msg = 	$translator->translator('hotels_room_delete_success');			
				$json_arr = array('status' => 'ok','msg' => $msg);
			}
			catch (Exception $e) 
			{
				$msg = 	$e->getMessage();		
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			
		}
		else
		{
			$msg = 	$translator->translator('hotels_room_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();					
				
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{					
										
					// Remove from Group
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					
					$where_calendar = array();
					$where_calendar[] = 'room_id = '.$conn->quote($id);
					
					try
					{
						$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room', $where);
						$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room_calendar', $where_calendar);
						
						$msg = 	$translator->translator('hotels_room_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
					catch (Exception $e) 
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$e->getMessage();			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}					
				}
			}
			else
			{
				$msg = $translator->translator("hotels_room_delete_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_room_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function loadAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		try
		{		
			$id = $this->_request->getParam('id');
			$res_value = '';			
			if(!empty($id))
			{
				
				$res_value_arr = array();
				$roomCalendar_db = new Hotels_Model_DbTable_Calendar();				
				$roomCalendar_info = $roomCalendar_db->getCalendarInfo($id);				
				if($roomCalendar_info)
				{					
					foreach($roomCalendar_info as $key => $value_arr)
					{
						$res_value_arr[$key] = $value_arr['calendar_date'].';;'.$value_arr['room_status'].';;'.$value_arr['room_price'];
					}
					$res_value = implode(',', $res_value_arr);					
				}						
			}
		}
		catch(Exception $e)
		{
			$res_value = '';
		}		
		$this->_response->setBody($res_value);
	}
	
	public function updateAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			if ($this->_request->isPost()) 
			{
				$data = $this->_request->getPost('dop_booking_calendar');				
				$data_arr = explode(',',$data);
				$price_arr = explode(';;',$data_arr[0]);
				$price	 = $price_arr[2];
				
				$roomCalendar_db = new Hotels_Model_DbTable_Calendar();
				$roomCalendar_db->updateCalendar($id, $data);				
				//$roomCalendar_db->updatePrice($id, $price);	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			}
		}
	}
	
	public function schedulerAction()
	{
		$id = $this->_getParam('id', 0);
				
		//Get Room Info
		$room_db = new Hotels_Model_DbTable_Room();
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				if(!empty($id))
				{
					$posted_data['filter']['filters'][] = array('field' => 'room_id', 'operator' => 'eq', 'value' => $id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				
										
					$list_db = new Hotels_Model_DbTable_Scheduler();	
					$list_datas =  $list_db->getListInfo(null, $posted_data, array(/*'hotels_page' => array('hotels_name' => 'vp.hotels_name'),*/ 'userChecking' => true));
					$view_datas = array('data_result' => array());			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								//$entry_arr['start'] = "/Date(".(strtotime($entry_arr['start_date']) * 1000 ).")/";
								//$entry_arr['end'] = "/Date(".(strtotime($entry_arr['end_date']) * 1000 ).")/";								
								$entry_arr['Start'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['Start']));
								$entry_arr['End'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['End']));
								$entry_arr['IsAllDay'] = (boolean)$entry_arr['IsAllDay'];
								
								$view_datas['data_result'][$key]	=	$entry_arr; //array('hotels_name' => $entry_arr['hotels_name'], 'start' => $entry_arr['start'], 'end' => $entry_arr['end'] );	
								$key++;					
						}					
					}
					$json_arr = array('id' => $id, 'status' => 'ok', 'data_result' => $view_datas['data_result'], 'posted_data' => $posted_data);
				}
				else
				{
					$json_arr = array('status' => 'ok', 'data_result' => '', 'posted_data' => $posted_data);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		else
		{		
			if(!empty($id))
			{
				//Get Vacationrentals Info
				$roomInfo = $room_db->getRoomInfo($id);
				if($roomInfo)
				{				
					$this->view->id = $id;
					$this->view->entry_by = $roomInfo['entry_by'];	
					$this->view->roomInfo = $roomInfo;			
				}
				else
				{
					$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
				}
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}
	}
	
	public function schedulercreateAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
		
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{		
				try
				{				
					$posted_data	=	$this->_request->getParams();				
					$posted_data['models'] = (function_exists('get_magic_quotes_gpc')) ? Zend_Json_Decoder::decode(stripslashes($posted_data['models'])) : Zend_Json_Decoder::decode($posted_data['models']);
					if($posted_data['models'])
					{	
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{
							$scheduler_model = new Hotels_Model_Scheduler($posted_data_value);
							//$scheduler_model->setStart($posted_data_value['Start']);
							//$scheduler_model->setEnd($posted_data_value['End']);
							$save_result = $scheduler_model->saveScheduler();
							if($save_result['status'] == 'ok')
							{
								$posted_data['models'][$posted_data_key]['id'] 			= 	$save_result['id'];
								//$posted_data['models'][$posted_data_key]['TaskID'] 		= 	$save_result['id'];
								$posted_data['models'][$posted_data_key]['Start']		=	strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($scheduler_model->getStart()));
								$posted_data['models'][$posted_data_key]['End']			=	strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($scheduler_model->getEnd()));
								$posted_data['models'][$posted_data_key]['IsAllDay'] 	= 	(boolean)$scheduler_model->getIsAllDay();
								$status = 'ok';	
							}
							else
							{
								$status = 'err';							
								$json_arr = array('status' => 'err','msg' => $save_result['msg']);
								break;
							}
						}
					}
					if($status == 'ok')
					{
						$json_arr = array('status' => 'ok', 'data_result' => $posted_data['models'], 'posted_data' => $posted_data);
					}
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}				
			}	
		}
		else
		{
			$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $this->translator->translator('Member_Access_deny_desc'));
		}
		//Convert To JSON ARRAY	
		$res_value =  Zend_Json_Encoder::encode($json_arr);				
		$this->_response->setBody($res_value);		
	}	
	
	
	public function schedulerupdateAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
		
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{		
				try
				{				
					$posted_data	=	$this->_request->getParams();				
					$posted_data['models'] = (function_exists('get_magic_quotes_gpc')) ? Zend_Json_Decoder::decode(stripslashes($posted_data['models'])) : Zend_Json_Decoder::decode($posted_data['models']);
					if($posted_data['models'])
					{	
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{
							$scheduler_model = new Hotels_Model_Scheduler($posted_data_value);
							
							$save_result = $scheduler_model->saveScheduler();
							if($save_result['status'] == 'ok')
							{
								$posted_data['models'][$posted_data_key]['id'] 			= 	$save_result['id'];
								$posted_data['models'][$posted_data_key]['Start']		=	strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($scheduler_model->getStart()));
								$posted_data['models'][$posted_data_key]['End']			=	strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($scheduler_model->getEnd()));
								$posted_data['models'][$posted_data_key]['IsAllDay'] 	= 	(boolean)$scheduler_model->getIsAllDay();
								$status = 'ok';	
							}
							else
							{
								$status = 'err';							
								$json_arr = array('status' => 'err','msg' => $save_result['msg']);
								break;
							}
						}
					}
					if($status == 'ok')
					{
						$json_arr = array('status' => 'ok', 'data_result' => $posted_data['models'], 'posted_data' => $posted_data);
					}
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}				
			}	
		}
		else
		{
			$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $this->translator->translator('Member_Access_deny_desc'));
		}
		//Convert To JSON ARRAY	
		$res_value =  Zend_Json_Encoder::encode($json_arr);				
		$this->_response->setBody($res_value);		
	}
	
	public function schedulerdeleteAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
		
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{		
				try
				{			
					$posted_data	=	$this->_request->getParams();				
					$posted_data['models'] = (function_exists('get_magic_quotes_gpc')) ? Zend_Json_Decoder::decode(stripslashes($posted_data['models'])) : Zend_Json_Decoder::decode($posted_data['models']);
					if($posted_data['models'])
					{	
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{						
							$calendar_model = new Hotels_Model_Scheduler($posted_data_value);					
							$save_result = $calendar_model->deleteScheduler();
							if($save_result['status'] == 'ok')
							{
								$status = 'ok';	
							}
							else
							{
								$status = 'err';							
								$json_arr = array('status' => 'err','msg' => $save_result['msg']);
								break;
							}						
						}
					}
					if($status == 'ok')
					{
						$json_arr = array('status' => 'ok', 'data_result' => $posted_data['models'], 'posted_data' => $posted_data);
					}
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}			
			}
		}
		else
		{
			$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $this->translator->translator('Member_Access_deny_desc'));
		}
		//Convert To JSON ARRAY	
		$res_value =  Zend_Json_Encoder::encode($json_arr);				
		$this->_response->setBody($res_value);			
	}		
}

