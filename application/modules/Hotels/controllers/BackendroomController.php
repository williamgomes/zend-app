<?php

class Hotels_BackendroomController extends Zend_Controller_Action
{	
	private $hotelsRoomTypeForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
	
    public function init()
    {	
        /* Initialize action controller here */		
		$this->hotelsRoomTypeForm =  new Hotels_Form_RoomTypeForm ();
		$this->view->hotelsRoomTypeForm =  $this->hotelsRoomTypeForm;
				
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		
		if($getAction != 'uploadfile')
		{
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);	
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$list_mapper = new Hotels_Model_RoomTypeListMapper();	
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');				
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{					
					$list_datas =  $list_mapper->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{						
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_room_type_name'] = 	str_replace('_', '-', $entry_arr['room_type']);	
							$img_file_arr 								= 	explode(',',$entry_arr['room_type_image']);
							$entry_arr['room_type_image_format'] 		= 	($this->view->escape($entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
							$entry_arr['id_format']						=	$this->view->numbers($this->view->escape($entry_arr['id']));
							$entry_arr['max_people_format']				=	$this->view->numbers($this->view->escape($entry_arr['max_people']));
							$entry_arr['room_num_format']				=	$this->view->numbers($this->view->escape($entry_arr['room_num']));
							$entry_arr['basic_price_format']			=	$this->view->numbers($this->view->escape($entry_arr['basic_price']));
							$entry_arr['descount_price_format']			=	$this->view->numbers($this->view->escape($entry_arr['descount_price']));
							$entry_arr['price_per_night_format']		=	$this->view->numbers($this->view->escape($entry_arr['price_per_night']));
							$entry_arr['price_after_discount_format']	=	$this->view->numbers($this->view->escape($entry_arr['price_after_discount']));
							$entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($entry_arr['room_type']), 'Members');
							$entry_arr['edit_enable']					=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by']) ? true : false;						
							$view_datas['data_result'][$key]			=	$entry_arr;	
							$key++;						
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}
		
		$mem_db	=	new Members_Model_DbTable_MemberList();
		$this->view->assign('user_list', $mem_db->getAllMembers());
		
		$hotel_db = new Hotels_Model_DbTable_Hotels();
		$this->view->assign('hotel_list', $hotel_db->getHotelsInfoByGroup());
		
		$price_db = new Hotels_Model_DbTable_Price();
		$this->view->assign('price_options', $price_db->getOptions());
		
		$this->view->assign('people_list', $list_mapper->getDbTable()->maxPeopleListRoomTypeUnic('ASC'));
		
		//$this->view->assign('price_min_data', $list_mapper->getDbTable()->priceListRoomTypeUnic('ASC'));
		
		//$this->view->assign('price_max_data', $list_mapper->getDbTable()->priceListRoomTypeUnic('DESC'));
	}
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function uploadfileAction()
	{
		$theme = Zend_Registry::get('jtheme');
		$this->view->theme = $theme;
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
				
		
		$file_content = ($this->_getParam('file_content', 0)) ? $this->_getParam('file_content', 0) : null ;
				
		//Put Group Information in the form			
		$this->uploadForm =  new Hotels_Form_UploadForm();		
		
		$path = 'data/frontImages/hotels/room_type_image';
		
		
		if ($this->_request->getPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			
			if($this->uploadForm->isValid($this->_request->getPost())) 
			{				
				Members_Controller_Helper_FileRename::fileRename($this->uploadForm->upload_file, $path);
				$Filename = $this->uploadForm->upload_file->getFileName();
				$ext = Eicra_File_Utility::GetExtension($Filename);
				$option['file_type']	=	'jpg,png,gif';
				$file_obj = new Settings_Controller_Helper_File($path,$Filename,$ext,$option);
				if($file_obj->checkThumbExt())
				{									
					if($this->uploadForm->upload_file->receive())
					{
						$msg = $translator->translator('File_upload_success');
						$json_arr = array('status' => 'ok','msg' => $msg, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));						
					}
					else
					{
						$validatorMsg = $this->uploadForm->upload_file->getMessages();
						$vMsg = implode("\n", $validatorMsg);	
						$json_arr = array('status' => 'err','msg' => $vMsg, 'img_date' => date ("Y-m-d H:i:s", filemtime(APPLICATION_PATH.realpath('../'.$path.'/'.$Filename))), 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
					}	
				}
				else
				{
					$msg = $translator->translator('File_upload_ext_err',$ext);
					$json_arr = array('status' => 'err','msg' => $msg, 'newName' => $this->uploadForm->upload_file->getFileName(null,false));
				}			
			}
			else
			{
				$validatorMsg = $this->uploadForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errType)
				{					
					foreach($errType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'err','msg' => $vMsg);	
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);						
		}	
		else
		{				
			$this->view->file_content	=	$file_content;			
			$this->view->upload_path	=	$path;
			$this->view->uploadForm = $this->uploadForm;				
		}		
	}
	
	public function deletefileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		$this->view->translator	= $translator;
		
		if ($this->_request->isPost()) 
		{
			$file_info = $this->_request->getPost('file_info');
			if(empty($file_info))
			{
				$msg = $translator->translator("insert_selected_file_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			else
			{
				$each_file_arr = explode('; ',$file_info);
				$deleted_file_name = '';
				foreach($each_file_arr as $key => $each_file)
				{
					$file_info_arr = explode(',',$each_file);
					if($file_info_arr[1])
					{				
						$dir = $file_info_arr[0].DS.$file_info_arr[1];
						$res = Eicra_File_Utility::deleteRescursiveDir($dir);
					}
					
					if($res)
					{
						$deleted_file_name .= $file_info_arr[1].', ';
						$msg = $translator->translator("file_delete_success",$deleted_file_name);
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					else
					{
						$msg = $translator->translator("file_delete_err",$file_info_arr[1]);
						$json_arr = array('status' => 'err','msg' => $msg);
						break;
					}
				}
			}
		}
		else
		{
			$msg = $translator->translator("file_delete_err");
			$json_arr = array('status' => 'err','msg' => $msg);
		}		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}
	
	public function addAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = $this->translator;
			
			if($this->hotelsRoomTypeForm->isValid($this->_request->getPost())) 
			{	
				$room = new Hotels_Model_RoomType($this->hotelsRoomTypeForm->getValues());
								
				$perm = new Hotels_View_Helper_Allow();
				if($perm->allow())
				{
					$result = $room->saveRoomType();
					if($result['status'] == 'ok')
					{
						$last_id = $result['id'];		
						$hotel_id_arr = $this->_request->getPost('hotel_id');	
						$hotel_db = new Hotels_Model_DbTable_Hotels();
						$linkResult = $hotel_db->linkToHotels($last_id, $hotel_id_arr);		
						if($linkResult['status'] == 'ok')
						{					
							$msg = $translator->translator("hotels_room_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $linkResult['msg'];
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator("hotels_room_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
					$json_arr = array('status' => 'errP','msg' => $Msg);
				}			
			}
			else
			{
				$validatorMsg = $this->hotelsRoomTypeForm->getMessages();
				$vMsg = array();
				$i = 0;
				foreach($validatorMsg as $key => $errRoomType)
				{					
					foreach($errRoomType as $errkey => $value)
					{
						$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
						$i++;
					}
				}
				$json_arr = array('status' => 'errV','msg' => $vMsg);
			}			
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);
		}
		$this->userUploaderSettings();		
	}
	
	public function editAction()
	{
		$id = $this->_request->getParam('id');	
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			$price = $this->_request->getPost('descount_price');		
				
			$perm = new Hotels_View_Helper_Allow();
			if($perm->allow())
			{
				if($this->hotelsRoomTypeForm->isValid($this->_request->getPost())) 
				{	
					$room = new Hotels_Model_RoomType($this->hotelsRoomTypeForm->getValues());
					$room->setId($id);	
					$room->setRoom_available_information($id, $price);
					$result = $room->saveRoomType();
					if($result['status'] == 'ok')
					{								
						$hotel_id_arr = $this->_request->getPost('hotel_id');	
						$hotel_db = new Hotels_Model_DbTable_Hotels();
						$linkResult = $hotel_db->linkToHotels($id, $hotel_id_arr);		
						if($linkResult['status'] == 'ok')
						{
							$msg = $translator->translator("hotels_room_type_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = $linkResult['msg'];
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = $translator->translator("hotels_room_type_save_err");
						$json_arr = array('status' => 'err','msg' => $msg." ".$result['msg']);
					}	
				}
				else
				{
					$validatorMsg = $this->hotelsRoomTypeForm->getMessages();
					$vMsg = array();
					$i = 0;
					foreach($validatorMsg as $key => $errRoomType)
					{					
						foreach($errRoomType as $errkey => $value)
						{
							$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
							$i++;
						}
					}
					$json_arr = array('status' => 'errV','msg' => $vMsg);
				}
			}
			else
			{
				$Msg =  $translator->translator("hotels_room_add_action_deny_desc");
				$json_arr = array('status' => 'errP','msg' => $Msg);
			}			
						
			$res_value = Zend_Json_Encoder::encode($json_arr);			
			$this->_response->setBody($res_value);			
		}
		else
		{
			$room_type_db = new Hotels_Model_DbTable_RoomType();
			$room_type_info	=	$room_type_db->getRoomTypeInfo($id);
			if($room_type_info)
			{
				$this->hotelsRoomTypeForm->hotel_id->setIsArray(true); 
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$hotels_info= $hotels_db->getHotelsInfoByGroup(null);
				$file_type_arr = array();
				$t	=	0;
				foreach($hotels_info as $hotel_key =>$hotel_arr)
				{
					$hotel_room_type_id_arr = explode(',', $hotel_arr['room_type_id']);
					if(in_array($id, $hotel_room_type_id_arr))
					{
						$file_type_arr[$t] = $hotel_arr['id'];
						$t++;
					}				
				}
				$this->hotelsRoomTypeForm->hotel_id->setValue($file_type_arr);
				$this->userUploaderSettings();
				$this->hotelsRoomTypeForm->populate($room_type_info);
				$this->view->entry_by = $room_type_info['entry_by'];
				$this->view->id = $id;
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}			
	}
	
	private function userUploaderSettings($info = null)
	{
		$group_db = new Hotels_Model_DbTable_HotelsGroup();
		if($info && $info['group_id'])
		{
			$group_info = $group_db->getGroupName($info['group_id']);
		}
		else
		{
			$info['group_id'] = $group_db->getFirstGroupId();
			$group_info = $group_db->getGroupName($info['group_id']);
			$this->view->assign('first_group_id' ,	$info['group_id']);
		}
		$param_fields = array(
								'table_name' => 'hotels_group', 
								'primary_id_field'	=>	'id', 
								'primary_id_field_value'	=>	$info['group_id'],
								'file_path_field'	=>	'', 
								'file_extension_field'	=>	'file_type', 
								'file_max_size_field'	=>	'file_size_max'
						);		
		$portfolio_model = new Portfolio_Model_Portfolio($param_fields);
		$requested_data = $portfolio_model->getRequestedData();
		$settings_info = $group_info;
		
		/*****************************************************For Primary*************************************************/
			$primary_requested_data	=	$requested_data;
			$primary_requested_data['file_path_field'] = 'file_path_room_type_image';			
			
			$this->view->assign('primary_settings_info' ,	array_merge($primary_requested_data, $settings_info));		
			$this->view->assign('primary_settings_json_info' ,	Zend_Json_Encoder::encode($this->view->primary_settings_info));
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);	
			$id = $this->_request->getPost('id');
						
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			
			$check_num_pro = new Hotels_View_Helper_HotelsGroup();
					
			if($check_num_pro->getNumOfHotelsForRoomType($id) == '0')
			{				
				//Room Type Info
				$roomType_db = new Hotels_Model_DbTable_RoomType();
				$roomType_info = $roomType_db->getRoomTypeInfo($id);
				
				$rooms_image = explode(',',$roomType_info['room_type_image']);
				$rooms_image_path	=	 'data/frontImages/hotels/room_type_image';
				
				// Remove from Group
				$where = array();
				$where[] = 'id = '.$conn->quote($id);
				try
				{
					$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room_type', $where);
					foreach($rooms_image as $key=>$file)
					{
						if($file)
						{
							$dir = $rooms_image_path.DS.$file;
							$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						}
						if($res)
						{
							$msg = 	$translator->translator('hotels_room_type_delete_success',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('hotels_room_type_delete_err',$file);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
					}
					$msg = 	$translator->translator('hotels_room_type_delete_success');			
					$json_arr = array('status' => 'ok','msg' => $msg);
				}
				catch (Exception $e) 
				{
					$msg = 	$e->getMessage();		
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('hotels_room_type_for_page_delete_err');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_room_type_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id_st = $this->_request->getPost('id_st');
			if(!empty($id_st))
			{
				$id_arr = explode(',',$id_st);
						
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				$roomType_db = new Hotels_Model_DbTable_RoomType();
				$rooms_image_path	=	 'data/frontImages/hotels/room_type_image';
					
				$check_num_pro = new Hotels_View_Helper_HotelsGroup();
				$non_del_arr = array();
				$k = 0;
				foreach($id_arr as $id)
				{			
					if($check_num_pro->getNumOfHotelsForRoomType($id) == '0')
					{
						//Room Type Info				
						$roomType_info = $roomType_db->getRoomTypeInfo($id);
						$rooms_image = explode(',',$roomType_info['room_type_image']);
						
						// Remove from Group
						$where = array();
						$where[] = 'id = '.$conn->quote($id);
						try
						{
							$conn->delete(Zend_Registry::get('dbPrefix').'hotels_room_type', $where);
							foreach($rooms_image as $key=>$file)
							{
								if($file)
								{
									$dir = $rooms_image_path.DS.$file;
									$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								}
								if($res)
								{
									$msg = 	$translator->translator('hotels_room_type_delete_success',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('hotels_room_type_delete_err',$file);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
							$msg = 	$translator->translator('hotels_room_type_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
						catch (Exception $e) 
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$e->getMessage();			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
					else
					{
						$non_del_arr[$k] = $id;
						$k++;
						$msg = 	$translator->translator('hotels_room_type_delete_success');			
						$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
					}
				}
			}
			else
			{
				$msg = $translator->translator("hotels_room_type_delete_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('hotels_room_type_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function linkhotelsAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			$id_str	=	$this->_request->getPost('id_str');
			$id_str_arr = explode(',', $id_str);
			
			$hotels_db = new Hotels_Model_DbTable_Hotels();
			$result = $hotels_db->linkToHotels($id, $id_str_arr);
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_room_type_linked_success");
				$json_arr = array('status' => 'ok','msg' => $msg);
			}
			else
			{				
				$json_arr = array('status' => 'err','msg' => $result['msg']);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
		
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			$roomType_db = new Hotels_Model_DbTable_RoomType();
			$roomType_info = $roomType_db->getRoomTypeInfo($id);
			
			$hotels_db = new Hotels_Model_DbTable_Hotels();
			$hotels_info= $hotels_db->getHotelsInfoByGroup(null);
			
			$this->view->roomType_info	=	$roomType_info;
			$this->view->hotels_info	=	$hotels_info;
		}
	}
	
	private function linkhotelsdata($id)
	{
		$hotels_db = new Hotels_Model_DbTable_Hotels();
		$hotels_info= $hotels_db->getHotelsInfoByGroup(null);
		
		if($hotels_info)
		{
			$hotels_arr = array();
			$c = 0; 
			foreach($hotels_info as $key=>$entry)
			{
				$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
				$room_id_arr = explode(',',$entry_arr['room_type_id']);
				if(in_array($id,$room_id_arr))
				{
				  $hotels_arr[$c] = $entry_arr;
				  $c++;
				}
			}
		}
		else
		{
			$hotels_arr = null;
		}
		
		return $hotels_arr;
	}
	
	public function linkhotelsnameAction()
	{		
		if ($this->_request->isPost()) 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$id = $this->_request->getPost('id');
			if(!empty($id))
			{
				try
				{
					$roomType_db = new Hotels_Model_DbTable_RoomType();
					$roomType_info = $roomType_db->getRoomTypeInfo($id);
					$hotels_arr = $this->linkhotelsdata($id);						
					$json_arr = array('status' => 'ok','hotels_info' => $hotels_arr, 'roomType_info' => $roomType_info, 'msg' => $translator->translator('hotels_link_to_room_type_to_hotel'));
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err','msg' => $e->getMessage());
				}
			}
			else
			{				
				$json_arr = array('status' => 'err','msg' => $result['msg']);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function loadAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			$roomType_db = new Hotels_Model_DbTable_RoomType();
			$roomType_info = $roomType_db->getRoomTypeInfo($id);
			$res_value = $roomType_info['room_available_information'];			
		}
		$this->_response->setBody($res_value);
	}
	
	public function updateAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$id = $this->_request->getParam('id');
		if(!empty($id))
		{
			if ($this->_request->isPost()) 
			{
				$data = $this->_request->getPost('dop_booking_calendar');
				$data_arr = explode(',',$data);
				$price_arr = explode(';;',$data_arr[0]);
				$price	 = $price_arr[2];
				
				$roomType_db = new Hotels_Model_DbTable_RoomType();
				$roomType_db->updateAvailableInformation($id, $data);
				$roomType_db->updatePrice($id, $price);	
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);			
			}
		}
	}		
}

