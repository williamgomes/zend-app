<?php

/*
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 * *****************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Payment Gateway                                             *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 * *****************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 * *****************************************************************************
 */

class Hotels_HotelsprobackController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_group_form_id_field = 'dynamic_form';
    private $_group_table = 'hotels_group';
    private $_static_table = 'hotels_page';
    private $_controllerCache;
    private $_translator;
    private $_api_settings;

    public function init() {
        /* Initialize action controller here */
        $this->CategoryForm = new Hotels_Form_CategoryForm ();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);
        
        $global_conf = Zend_Registry::get('global_conf');

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
        
        
    }

    public function preDispatch() {
        
        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');


        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
        if ($getAction != 'uploadfile') {
            $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
            Eicra_Global_Variable::checkSession($this->_response, $url);

            /* Check Module License */
            $modules_license = new Administrator_Controller_Helper_ModuleLoader();
            $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
        }
    }
    
    
    public function xml2array($xmlObject){
        ini_set('memory_limit', '-1');
        $arr = array();
 
        foreach ($xmlObject->children() as $r)
        {
            $t = array();
            if(count($r->children()) == 0)
            {
                $arr[$r->getName()] = strval($r);
            }
            else
            {
                $arr[$r->getName()][] = $this->xml2array($r);
            }
        }
        return $arr;
    }

    public function xmlpopulateAction() {
        
        ini_set('memory_limit', '-1');
        $this->_api_settings = new Hotels_Model_HotelsproApiModel();
        $datas_per_iteration = $this->_api_settings->getHotelCount();
        $AffCode = $this->_api_settings->getAffiliateCode();
        
        
        
        //getting data count from text file
        $countDetails = 0;
        $totalHotelCount = 0;
        $countfile = MODULE_PATH . '/Hotels/hotelsproXML/xmlcountrecord.txt';
        if(file_exists($countfile)){
            $countDetails = file_get_contents($countfile);
        } else {
            $countDetails = 0;
        }
        
        
        $feedback = array();
        $feedback['type'] = '';
        $feedback['message'] = '';
        
        $listXmlFile = MODULE_PATH . '/Hotels/hotelsproXML/'. $AffCode .'hotellist.xml';
        $list = new XMLReader;
        $list->open($listXmlFile);
        //getting xml file modification dates
        $lastModifiedListXml = date("Y-m-d", filemtime($listXmlFile));

        $amenityXmlFile = MODULE_PATH . '/Hotels/hotelsproXML/'. $AffCode .'hotelamenities.xml';
        $amenities = new XMLReader;
        $amenities->open($amenityXmlFile);
        //getting xml file modification dates
        $lastModifiedAmenityXml = date("Y-m-d", filemtime($amenityXmlFile));

        $descXmlFile = MODULE_PATH . '/Hotels/hotelsproXML/'. $AffCode .'hoteldescr.xml';
        $desc = new XMLReader;
        $desc->open($descXmlFile);
        //getting xml file modification dates
        $lastModifiedDescXml = date("Y-m-d", filemtime($descXmlFile));
        
        
        $doc = new DOMDocument;

        // move to the first <Hotel /> node
        while ($list->read() && $list->name !== 'Hotel');

        // now that we're at the right depth, hop to the next <Hotel/> until the end of the tree
        // move to the first <Hotel /> node
        while ($amenities->read() && $amenities->name !== 'Hotel');
        // now that we're at the right depth, hop to the next <Hotel/> until the end of the tree
        // move to the first <Hotel /> node
        while ($desc->read() && $desc->name !== 'Hotel');
        // now that we're at the right depth, hop to the next <Hotel/> until the end of the tree
        // flag is fromtxt file
        //getting content from date text and converting text into value
        $datefile = MODULE_PATH . '/Hotels/hotelsproXML/xmldaterecord.txt';
        
        if(file_exists($datefile)){
            $dateDetails = file_get_contents($datefile);
            $arrDateDetails = explode(',', $dateDetails);
            $listDate = $arrDateDetails[0];
            $amenityDate = $arrDateDetails[1];
            $descDate = $arrDateDetails[2];
            if($lastModifiedListXml != $listDate || $lastModifiedAmenityXml != $amenityDate || $lastModifiedDescXml != $descDate){
                $countDetails = 0;
            }
        } else {
            file_put_contents($datefile, $lastModifiedListXml . ',' . $lastModifiedAmenityXml . ',' . $lastModifiedDescXml);
        }
        

        for ($k = 0; $k < $countDetails; $k++) {
            $list->next('Hotel');
            $amenities->next('Hotel');
            $desc->next('Hotel');
        }
        
        $count = 1;
        while ($list->name === 'Hotel') {
            
            $flag = 1;
            $listnode = simplexml_import_dom($doc->importNode($list->expand(), true));
            $amenitiesnode = simplexml_import_dom($doc->importNode($amenities->expand(), true));
            $descnode = simplexml_import_dom($doc->importNode($desc->expand(), true));
            $filtered_data ['hotel_code'] = (string) $descnode->HotelCode;
            $filtered_data ['info'] = (string) $descnode->HotelInfo;
            $filtered_data ['dest_id'] = (string) $listnode->DestinationId;
            $filtered_data ['dest'] = (string) $listnode->Destination;
            $filtered_data ['name'] = (string) $listnode->HotelName;
            $filtered_data ['rating'] = (string) $listnode->StarRating;
            $filtered_data ['address'] = (string) $listnode->HotelAddress;
            $filtered_data ['postal_code'] = (string) $listnode->HotelPostalCode;
            $filtered_data ['phone'] = (string) $listnode->HotelPhoneNumber;
            $filtered_data ['hotel_area'] = (string) $listnode->HotelArea;
            $filtered_data ['chain'] = (string) $listnode->Chain;
            $filtered_data ['coordinates'] = (string) $listnode->Coordinates->Latitude . ',' . (string) $listnode->Coordinates->Longitude;
            $imgs = $listnode->HotelImages->ImageURL;
            $total_img = count((array) $imgs);
            $imgUrl = '';
            for ($i = 0; $i < $total_img; $i++) {
                $imgUrl.=$imgs[$i] . ',';
            }
            $imgUrl = trim($imgUrl,',');
//            echo $imgUrl . "<br/>";
            
            //str_replace(find,replace,string,count);
            $imgUrl = str_replace(",#####", "", $imgUrl);
            $filtered_data ['image'] = $imgUrl;
            $filtered_data ['pamenities'] = (string) $amenitiesnode->PAmenities;
            $filtered_data ['ramenities'] = (string) $amenitiesnode->RAmenities;
            
            
            $list->next('Hotel');
            $amenities->next('Hotel');
            $desc->next('Hotel');
            

            $hotelspro = new Hotels_Model_Hotelspro($filtered_data);
            $result = $hotelspro->saveHotelspro();
            file_put_contents($countfile, ($countDetails + $count));
            
            if($datas_per_iteration == $count){
                break;
            }
            $count++;
        }
        
         
        $feedback['type'] = 'msg';
        $feedback['message'] = 'Data import in progress. Total ' . ($countDetails + $count) . ' Hotels data inserted into database.';
         
        $this->view->assign('feedback', $feedback);

    }
    
    
    


}

?>