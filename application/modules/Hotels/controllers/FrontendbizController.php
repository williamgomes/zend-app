<?php
class Hotels_FrontendbizController extends Zend_Controller_Action
{
    private $_page_id;
    private $_translator;
    private $_controllerCache;
	private $_modules_license;
	private $_auth_obj;	
	private $_ckLicense;
	private	$_snopphing_cart;
	private	$_category;
	private $currency;
    public function init ()
    {
        /* Initialize action controller here */
        $this->_translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->_translator);
        $this->view->setEscape('stripslashes');
			
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
		
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
		
    }
    public function preDispatch ()
    {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();

		$front_template = Zend_Registry::get('front_template');		
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
		$this->view->front_template = $front_template;
		
        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
			$page_id_arr = array_filter( $page_id_arr );	
            $this->_page_id = ($page_id_arr && !empty($page_id_arr[0])) ? implode(',', $page_id_arr) : null;
        } else {
            $this->_page_id = null;
        }
		
		if($this->_request->getParam('category'))
		{
			try
			{
				$category_url = $this->_request->getParam('category');	
				$category_db = new Hotels_Model_DbTable_Category();
				$category_info = $category_db->getCategoryInfoFromTitle($category_url);
				if($category_info)
				{
					$this->_category = $category_info['id'];
				}
				else
				{
					$this->_category = 0;
				}
			}
			catch(Exception $e)
			{
				$this->_category = 0;
			}
		}
		else
		{
			$this->_category = 0;
		}
    }
    private function breadcrumbArray ($breadcrumb_arr, $category_db, $parent_id, $route = 'All-B2b-Category-list/*')
    {
        $parent_info = $category_db->getCategoryName($parent_id);
        if ($parent_info) {
            if (! empty($parent_info['parent_id'])) {
                $breadcrumb_arr = $this->breadcrumbArray($breadcrumb_arr,
                        $category_db, $parent_info['parent_id'], $route);
            }
            $breadcrumb_arr[] = array($this->view->escape($parent_info['name']),
                    $this->view->url(
                            array('module' => $this->view->getModule,
                                    'controller' => $this->view->getController,
                                    'action' => $this->view->getAction,
                                    'category' => $parent_info['url_portion']), $route, true));
        }
        return $breadcrumb_arr;
    }	
	
	/*private function hotels_truncate($phrase,$start_words, $max_words, $char = false, $charset = 'UTF-8')
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				  $dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				  $phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
				}
		}
		else
		{
		   $phrase_array = explode(' ',$phrase);
		   if(count($phrase_array) > $max_words && $max_words > 0)
			  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
	   return $phrase;
	}*/
	
	public function groupAction()
    {
		try 
		{			
			$device = new Administrator_Controller_Helper_Device();
			// BreadCrumb
			/*Eicra_Global_Variable::getSession()->breadcrumb = array(
					array(
							$this->_translator->translator('b2b_front_product_list',
									'', 'B2b'), $this->view->url()),
					array(
							$this->_translator->translator(
									'b2b_front_product_list_active', '', 'B2b'),
							$this->view->url()));*/
			$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
			if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
			{
				foreach($postValue['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}
			$preferences_db = new Hotels_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('group_id') : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'groups', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			// action body
			if ($preferences_data && $preferences_data['hotels_list_by_group_other_page_sortby'] && $preferences_data['hotels_list_by_group_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['hotels_list_by_group_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['hotels_list_by_group_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Biz-Hotel-Group-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_postValue = Zend_Json_Encoder::encode($postValue);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
			$list_mapper = new Hotels_Model_HotelsListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('hotels_group' =>  array( 'group_name' => 'hg.group_name', 'review_id' => 'hg.review_id', 'file_thumb_width' => 'hg.file_thumb_width', 'file_thumb_height' => 'hg.file_thumb_height', 'file_thumb_resize_func' => 'hg.file_thumb_resize_func', 'group_meta_title' => 'hg.meta_title', 'group_meta_keywords' => 'hg.meta_keywords', 'group_meta_desc' => 'hg.meta_desc'), 'userChecking' => false));
						
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
				$review_helper = new Review_View_Helper_Review();
				$room_type_db = new Hotels_Model_DbTable_RoomType();
				$room_db = new Hotels_Model_DbTable_Room();
				$today = date('Y-m-d');					
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
							$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
							$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
							$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
							$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
							$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
							$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
							$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
							$entry_arr['hotels_image_no_format'] = $this->_translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
							//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
							$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
							$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
							
							$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
							$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
							$hotel_stars = '';
							for($i = 1; $i < $maximum_stars_digit; $i++)
							{
								$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
								
							}
							$entry_arr['hotel_stars_format']	= 	$hotel_stars;
							$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
							
							//Room Type Start
							if(!empty($entry_arr['room_type_id']))
							{	
								try
								{	
									$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
									$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
									$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
									if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
									{
										$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																						'filters' => array(																			  
																										 array(
																												'logic' => 'AND',
																												'filters' => array( 
																																	array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																	array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																)
																											),
																										 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																										)
																					 );	
									}
									$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
									//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
									if($room_type_info)
									{
										$room_type_info_key = 0;				
										foreach($room_type_info as $room_type_info_entry)
										{						
											$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
											$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
											$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
											$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
											$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
											$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
											$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
											$room_type_info_entry_arr['max_people_lang_format']	=	$this->_translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
											$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
											$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
											$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
											$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
											$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
											$room_type_info_entry_arr['form_data_view_title_format']	=	$this->_translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
											$save = 0;
											if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
											{
												$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
												$save = round($save);
											}
											$room_type_info_entry_arr['save_price'] = $save;
											$room_type_info_entry_arr['save_format'] = $this->_translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
											$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
											$room_type_info_entry_arr['just_booked'] = $booked;
											
											//Room Info Start
											if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
											{
												$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
											}
											else
											{
												 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
											}
											$room_type_info_entry_arr['room_info'] = $room_info;
											$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
											if($device->isMobile())
											{
												$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
												$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
											}
											else
											{
												$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_list_available_type_llast', count($room_info));
												$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_list_available_type_left', count($room_info));
											}
											$room_type_info_entry_arr['room_info_count_format2'] = $this->_translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
											$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
											//Room Info End
											
											$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
											$room_type_info_key++;						
										}
									}	
								}
								catch(Exception $e)
								{
																	
								}
							}								
							//Room Type End
																					
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;	
					}					
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			
			$view_datas['total'] = $list_datas->getTotalItemCount();	
			$view_datas['paginator'] =   $list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}
	
	public function categoryAction()
	{
		try 
		{
			$device = new Administrator_Controller_Helper_Device();		
			$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
			if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
			{
				foreach($postValue['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}
			$preferences_db = new Hotels_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? (($this->_category) ? $this->_category : $this->_request->getParam('category_id')) : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'categories', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			if($this->_request->getParam('country'))
			 {
				$posted_data['filter']['filters'][] = array( 'field' => 'country_id', 'operator' => 'eq', 'value' => $this->_request->getParam('country'));
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and'; 
			 }
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			//HEADER CATEGORY START
			 $parent_id = ($this->_category) ? $this->_category : ((count($this->view->page_id_arr) > 1) ? 0 : $this->view->page_id_arr[0]);
			 $this->view->category_db = new Hotels_Model_DbTable_Category();
			
			 $search_params['filter']['filters'][0] = array( 'field' => 'parent', 'operator' => 'eq', 'value' => $parent_id);
			 $search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';
			 
			 $this->view->category_info = $this->view->category_db->getListInfo('1', $search_params, false);
			 
			 /*if (!empty($parent_id)) 
			 {
				$search_params_parent['filter']['filters'][0] = array( 'field' => 'id', 'operator' => 'eq', 'value' => $parent_id);
				$search_params_parent['filter']['logic'] = ($search_params_parent['filter']['logic']) ? $search_params_parent['filter']['logic'] : 'and';
				
	
				$parent_category_info = $this->view->category_db->getListInfo('1', $search_params_parent, false);
				$parent_category_info = ($parent_category_info && ! is_array($parent_category_info)) ? $parent_category_info->toArray() : $parent_category_info;
				$this->view->parent_category_info = ($parent_category_info && $parent_category_info[0]) ? $parent_category_info[0] : null;
			}*/		 
			//HEADER CATEGORY END
			
			// action body
			if ($preferences_data && $preferences_data['hotels_list_by_category_other_page_sortby'] && $preferences_data['hotels_list_by_category_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['hotels_list_by_category_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['hotels_list_by_category_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Biz-Hotel-Category-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'category_id' => $this->_request->getParam('category_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_postValue = Zend_Json_Encoder::encode($postValue);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
			$list_mapper = new Hotels_Model_HotelsListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('hotels_category' =>  array( 'category_name' => 'hc.category_name', 'category_meta_title' => 'hc.meta_title', 'category_meta_keywords' => 'hc.meta_keywords', 'category_meta_desc' => 'hc.meta_desc'), 'countries' => array( 'country_name' => 'cnt.value', 'country_code' => 'cnt.code'), 'userChecking' => false));
						
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
				$review_helper = new Review_View_Helper_Review();
				$room_type_db = new Hotels_Model_DbTable_RoomType();
				$room_db = new Hotels_Model_DbTable_Room();
				$today = date('Y-m-d');					
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
						$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
						$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
						$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
						$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
						$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
						$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
						$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
						$img_thumb_arr = explode(',', $entry_arr['hotels_image']);	
						$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
						$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
						$entry_arr['hotels_image_no_format'] = $this->_translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
						
						$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
						$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
						
						$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
						$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
						$hotel_stars = '';
						for($i = 1; $i < $maximum_stars_digit; $i++)
						{
							$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
							
						}
						$entry_arr['hotel_stars_format']	= 	$hotel_stars;
						$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
						
						//Room Type Start
						if(!empty($entry_arr['room_type_id']))
						{	
							try
							{	
								$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
								$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
								$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
								if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
								{
									$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																					'filters' => array(																			  
																									 array(
																											'logic' => 'AND',
																											'filters' => array( 
																																array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																															)
																										),
																									 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																									)
																				 );	
								}
								$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
								
								if($room_type_info)
								{
									$room_type_info_key = 0;				
									foreach($room_type_info as $room_type_info_entry)
									{						
										$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
										$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
										$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
										$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
										$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
										$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
										$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
										$room_type_info_entry_arr['max_people_lang_format']	=	$this->_translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
										$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
										$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
										$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
										$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
										$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
										$room_type_info_entry_arr['form_data_view_title_format']	=	$this->_translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
										$save = 0;
										if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
										{
											$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
											$save = round($save);
										}
										$room_type_info_entry_arr['save_price'] = $save;
										$room_type_info_entry_arr['save_format'] = $this->_translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
										$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
										$room_type_info_entry_arr['just_booked'] = $booked;
										
										//Room Info Start
										if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
										{
											$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
										}
										else
										{
											 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
										}
										$room_type_info_entry_arr['room_info'] = $room_info;
										$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
										if($device->isMobile())
										{
											$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_room_available_type_last', count($room_info));
											$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_room_available_type_left', count($room_info));
										}
										else
										{
											$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_list_available_type_llast', count($room_info));
											$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_list_available_type_left', count($room_info));
										}
										$room_type_info_entry_arr['room_info_count_format2'] = $this->_translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
										$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
										//Room Info End
										
										$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
										$room_type_info_key++;						
									}												
								}	
							}
							catch(Exception $e)
							{
																
							}
						}								
						//Room Type End
																
						$view_datas['data_result'][$key]	=	$entry_arr;	
						$key++;
					}					
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			
			$view_datas['total'] = $list_datas->getTotalItemCount();	
			$view_datas['paginator'] =   $list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}	
	
	public function businesstypeAction()
	{
		try 
		{
			$device = new Administrator_Controller_Helper_Device();			
			$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
			if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
			{
				foreach($postValue['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}
			$preferences_db = new Hotels_Model_DbTable_Preferences();
			$preferences_data = $preferences_db->getOptions();
			$posted_data	=	$this->_request->getParams();		
			$this->view->assign('posted_data', $posted_data);
			$this->view->currency = $this->getCurrency();
			$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('type_id') : $this->_page_id;
		
			if (!empty($this->_page_id)) 
			{
				$posted_data['filter']['filters'][] = array('field' => 'businesstype', 'operator' => 'eq', 'value' => $this->_page_id);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';				
			}
			$this->view->page_id_arr = explode(',', $this->_page_id);
			
			// action body
			if ($preferences_data && $preferences_data['hotels_list_by_business_type_other_page_sortby'] && $preferences_data['hotels_list_by_business_type_other_page_sortby'] != '_') 
			{
				$sort_arr = explode('-', $preferences_data['hotels_list_by_business_type_other_page_sortby']);
				$posted_data['sort'][0] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
			}			
			$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				
			$getViewPageNum = $preferences_data['hotels_list_by_business_type_on_other_page']; 
			$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Biz-Hotel-Business-Type-List/*';
			$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'type_id' => $this->_request->getParam('type_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
			$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
			
			$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
			Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
									
			$encode_params = Zend_Json_Encoder::encode($posted_data);
			$encode_postValue = Zend_Json_Encoder::encode($postValue);
			$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
			$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
			$list_mapper = new Hotels_Model_HotelsListMapper();	
			$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('hotels_group' =>  array( 'group_name' => 'hg.group_name', 'review_id' => 'hg.review_id', 'file_thumb_width' => 'hg.file_thumb_width', 'file_thumb_height' => 'hg.file_thumb_height', 'file_thumb_resize_func' => 'hg.file_thumb_resize_func', 'group_meta_title' => 'hg.meta_title', 'group_meta_keywords' => 'hg.meta_keywords', 'group_meta_desc' => 'hg.meta_desc'), 'userChecking' => false));
						
			if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{	
				$maximum_stars_digit = $this->_translator->translator('maximum_stars_digit');
				$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
				$review_helper = new Review_View_Helper_Review();
				$room_type_db = new Hotels_Model_DbTable_RoomType();
				$room_db = new Hotels_Model_DbTable_Room();
				$today = date('Y-m-d');					
				$view_datas = array('data_result' => array());			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
						$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
						$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
						$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
						$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
						$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
						$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
						$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
						$img_thumb_arr = explode(',', $entry_arr['hotels_image']);	
						$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
						$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
						$entry_arr['hotels_image_no_format'] = $this->_translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
						
						$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
						$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->_translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->_translator->translator('common_review_no', $this->view->numbers(0));
						
						$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
						$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
						$hotel_stars = '';
						for($i = 1; $i < $maximum_stars_digit; $i++)
						{
							$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
							
						}
						$entry_arr['hotel_stars_format']	= 	$hotel_stars;
						$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
						
						//Room Type Start
						if(!empty($entry_arr['room_type_id']))
						{	
							try
							{	
								$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
								$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
								$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
								if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
								{
									$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																					'filters' => array(																			  
																									 array(
																											'logic' => 'AND',
																											'filters' => array( 
																																array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																															)
																										),
																									 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																									)
																				 );	
								}
								$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
								
								if($room_type_info)
								{
									$room_type_info_key = 0;				
									foreach($room_type_info as $room_type_info_entry)
									{						
										$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
										$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
										$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
										$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
										$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
										$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
										$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
										$room_type_info_entry_arr['max_people_lang_format']	=	$this->_translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
										$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
										$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
										$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
										$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
										$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
										$room_type_info_entry_arr['form_data_view_title_format']	=	$this->_translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
										$save = 0;
										if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
										{
											$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
											$save = round($save);
										}
										$room_type_info_entry_arr['save_price'] = $save;
										$room_type_info_entry_arr['save_format'] = $this->_translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
										$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
										$room_type_info_entry_arr['just_booked'] = $booked;
										
										//Room Info Start
										if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
										{
											$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
										}
										else
										{
											 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
										}
										$room_type_info_entry_arr['room_info'] = $room_info;
										$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
										if($device->isMobile())
										{
											$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_room_available_type_last', count($room_info));
											$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_room_available_type_left', count($room_info));
										}
										else
										{
											$room_type_info_entry_arr['room_info_count_format'] = $this->_translator->translator('hotels_front_page_list_available_type_llast', count($room_info));
											$room_type_info_entry_arr['room_info_count_format1'] = $this->_translator->translator('hotels_front_page_list_available_type_left', count($room_info));
										}
										$room_type_info_entry_arr['room_info_count_format2'] = $this->_translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
										$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
										//Room Info End
										
										$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
										$room_type_info_key++;						
									}												
								}	
							}
							catch(Exception $e)
							{
																
							}
						}								
						//Room Type End
																
						$view_datas['data_result'][$key]	=	$entry_arr;	
						$key++;
					}					
				}
				$this->_controllerCache->save($view_datas , $uniq_id);
			}
			
			$view_datas['total'] = $list_datas->getTotalItemCount();	
			$view_datas['paginator'] =   $list_datas;					
			$json_arr = array('status' => 'ok', 'total' => $view_datas['total'], 'data_result' => $view_datas['data_result'], 'paginator' => $view_datas['paginator'], 'posted_data' => $posted_data);
			
		} 
		catch (Exception $e) 
		{
            $json_arr = array('status' => 'err', 'data_result' => '','msg' => $e->getMessage());
        }
		$this->view->assign('json_arr', $json_arr);
	}
	
	private function getCurrency ()
	{
		
		if (empty($this->currency))
		{
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}		
		else 
		{
			return $this->currency;
		}
		
	}
	
	/*private function getNights($sStartDate, $sEndDate)
	{		
		$sStartDate =strtotime($sStartDate);
		$sEndDate = strtotime($sEndDate);
		
		$datediff = $sEndDate - $sStartDate;
		$aDays	= floor($datediff/(60*60*24));
		
		return $aDays;
	}*/
   
}