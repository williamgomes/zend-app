<?php
class Hotels_FrontendController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_page_id;
	private $_controllerCache;
	private $currency;
	private $translator;
	private $_auth_obj;	
	
    public function init()
    {			
        /* Initialize action controller here */				
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		
		/* Initialize action controller here */	
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();				
    }
	
	public function preDispatch() 
	{			
		$template_obj = new Eicra_View_Helper_Template();	
		$template_obj->setFrontendTemplate();			
		$front_template = Zend_Registry::get('front_template');		
		$this->_helper->layout->setLayout($template_obj->getLayout(false, array( 'controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template )));				
		$this->view->front_template = $front_template;

		
		if($this->_request->getParam('menu_id'))
		{				
			$viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
			$page_id_arr = $viewHelper->_getContentId();
			
			$this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null ;
		}
		else
		{
			$this->_page_id = null;
		}		
	}
	
	public function homeAction()
    {
		$group_id = (!empty($this->_page_id)) ? $this->_page_id : '1';
		if(!empty($group_id)) 
		{
			//Get Group Information
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_home_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($this->_page_id);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			//get Featured Data
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_home_featured_datas';
			if( ($view_featured_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$select_featured = $this->_DBconn->select()
									->from(array('pp' => Zend_Registry::get('dbPrefix').'hotels_page'), array('*'))
									->where('pp.group_id = ?', $group_id)
									->where('pp.active = ?', '1')
									->where('pp.featured = ?', '1')
									->order($group_info['featured_file_sort']." ".$group_info['featured_file_order'])
									->limit($group_info['featured_file_num_per_page'])
									->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'pp.state_id = st.state_id', array('state_name' => 'st.state_name'))
									->joinLeft(array('ct' => Zend_Registry::get('dbPrefix').'cities'), 'pp.area_id = ct.city_id', array('city' => 'ct.city'));
										
				$view_featured_datas =  $select_featured->query()->fetchAll();
				$this->_controllerCache->save($view_featured_datas, $uniq_id);
			}
			
			//get Latest Data
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_home_latest_datas';
			if( ($view_latest_datas = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$select_latest = $this->_DBconn->select()
									->from(array('pp' => Zend_Registry::get('dbPrefix').'hotels_page'), array('*'))
									->where('pp.group_id = ?', $group_id)
									->where('pp.active = ?', '1')
									->order($group_info['latest_file_sort']." ".$group_info['latest_file_order'])
									->limit($group_info['latest_file_num_per_page']);
										
				$view_latest_datas =  $select_latest->query()->fetchAll();
				$this->_controllerCache->save($view_latest_datas, $uniq_id);
			}
			
			//Assigned Information to View
			$this->view->group_datas = $group_info;
			$this->view->view_featured_datas = $view_featured_datas;
			$this->view->view_latest_datas = $view_latest_datas;			
			$this->view->currency = $this->getCurrency();
		}
	}
	
	public function groupAction()
    {			
		$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
		if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
		{
			foreach($postValue['filter']['filters'] as $fieldObj)
			{
				$postValue[$fieldObj['field']]  = $fieldObj['value'];
			}
		}
		
		$posted_data	=	$this->_request->getParams();		
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('group_id') : $this->_page_id;
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$group_db = new Hotels_Model_DbTable_HotelsGroup();
			$group_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';	
			$group_info = $group_db->getListInfo('1', $group_search_params, false);			
			
			if($group_info)
			{
				$group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
				$this->view->assign('group_info', $group_info[0] );
				$posted_data['sort'][] = array('field' => $group_info[0]['file_sort'],  'dir' => $group_info[0]['file_order']);				
			}
		}
		
		if($group_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					$device = new Administrator_Controller_Helper_Device();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
						
					$getViewPageNum = $this->_request->getParam('pageSize'); 
					$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'All-Hotel-List/*';
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);
					$encode_postValue = Zend_Json_Encoder::encode($postValue);
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
						$review_helper = new Review_View_Helper_Review();
						$room_type_db = new Hotels_Model_DbTable_RoomType();
						$room_db = new Hotels_Model_DbTable_Room();
						$today = date('Y-m-d');					
						$list_mapper = new Hotels_Model_HotelsListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
									$entry_arr['hotels_phone_format'] = $this->view->numbers($entry_arr['hotels_phone']);
									$entry_arr['feature_distance_from_airport_format'] = $this->view->numbers($entry_arr['feature_distance_from_airport']);
									$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
									$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
									$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
									$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
									$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
									$entry_arr['hotels_image_no_format'] = $this->translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
									//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$hotel_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
										
									}
									$entry_arr['hotel_stars_format']		= 	$hotel_stars;
									$entry_arr['vote_format']				= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
									
									$entry_arr['total_nights']				=	$list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]);
									$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
									$entry_arr['total_nights_format']		=	$this->view->numbers($entry_arr['total_nights']);
									$entry_arr['total_nights_format_lang']	=	$this->translator->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
									$entry_arr['room_type_info']			=	null; 
								
									//Room Type Start
									if(!empty($entry_arr['room_type_id']))
									{	
										try
										{	
											$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
											$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';
											$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
											if(!empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
											{
												$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																								'filters' => array(																			  
																												 array(
																														'logic' => 'AND',
																														'filters' => array( 
																																			array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																			array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																		)
																													),
																												 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																												)
																							 );	
											}					
											$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
											//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
											if($room_type_info)
											{
												$room_type_info_key = 0;				
												foreach($room_type_info as $room_type_info_entry)
												{	
													if($list_mapper->searchPrice($room_type_info_entry, $postValue, array('controller_obj' => array('view' => $this->view))))
													{					
														$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
														$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
														$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
														$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
														$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
														$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
														$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
														$room_type_info_entry_arr['max_people_lang_format']	=	$this->translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
														$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
														$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
														$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
														$room_type_info_entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
														$save = 0;
														if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
														{
															$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
															$save = round($save);
														}
														$room_type_info_entry_arr['save_price'] = $save;
														$room_type_info_entry_arr['save_format'] = $this->translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
														$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
														$room_type_info_entry_arr['just_booked'] = $booked;
														
														//Room Info Start
														if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
														{
															$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
														}
														else
														{
															 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
														}
														$room_type_info_entry_arr['room_info'] = $room_info;
														$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
														if($device->isMobile())
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
														}
														else
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_list_available_type_llast', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_list_available_type_left', $this->view->numbers(count($room_info)));
														}														
														$room_type_info_entry_arr['room_info_count_format2'] = $this->translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
														$room_type_info_entry_arr['room_info_count_format3'] = $this->translator->translator('hotels_search_sugges_room_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
														$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
														
														//Room Info End														
														$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
														$room_type_info_key++;
													}
												}
											}	
										}
										catch(Exception $e)
										{
																			
										}
									}								
									//Room Type End
																
									//$entry_arr['booking_suggestion']	=	$this->view->suggestion($postValue, $entry_arr, $this);						
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}					
		}
	}
		
	public function viewAction()
    {								
		$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
		if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
		{
			foreach($postValue['filter']['filters'] as $fieldObj)
			{
				$postValue[$fieldObj['field']]  = $fieldObj['value'];
			}
		}
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		$this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('type') : $this->_page_id;
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'hotels_type', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$business_type_db = new Hotels_Model_DbTable_BusinessType();
			
			$business_type_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$business_type_search_params['filter']['logic'] = ($business_type_search_params['filter']['logic']) ? $business_type_search_params['filter']['logic'] : 'and';	
			$business_type_info = $business_type_db->getListInfo(null, $business_type_search_params, false);
			
			if($business_type_info)
			{
				$business_type_info = (!is_array($business_type_info)) ? $business_type_info->toArray() : $business_type_info;
				$this->view->assign('business_type_info', $business_type_info[0] );
				$posted_data['sort'][] = array('field' => $business_type_info[0]['file_sort'],  'dir' => $business_type_info[0]['file_order']);
			}
		}		
		
		if($business_type_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					$device = new Administrator_Controller_Helper_Device();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
					$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Hotel-List-Type/*';	
					$getViewPageNum = $this->_request->getParam('pageSize'); 	
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'type' => $this->_request->getParam('type'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);
					$encode_postValue = Zend_Json_Encoder::encode($postValue);				
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
						$review_helper = new Review_View_Helper_Review();
						$room_type_db = new Hotels_Model_DbTable_RoomType();
						$room_db = new Hotels_Model_DbTable_Room();
						$today = date('Y-m-d');					
						$list_mapper = new Hotels_Model_HotelsListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
									$entry_arr['hotels_phone_format'] = $this->view->numbers($entry_arr['hotels_phone']);
									$entry_arr['feature_distance_from_airport_format'] = $this->view->numbers($entry_arr['feature_distance_from_airport']);
									$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
									$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
									$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
									$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
									$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
									$entry_arr['hotels_image_no_format'] = $this->translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
									//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$hotel_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
										
									}
									$entry_arr['hotel_stars_format']	= 	$hotel_stars;
									$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
									
									$entry_arr['total_nights']				=	$list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]);
									$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
									$entry_arr['total_nights_format']		=	$this->view->numbers($entry_arr['total_nights']);
									$entry_arr['total_nights_format_lang']	=	$this->translator->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
									$entry_arr['room_type_info']			=	null; 
									
									//Room Type Start
									if(!empty($entry_arr['room_type_id']))
									{	
										try
										{	
											$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
											$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
											$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
											if(!empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
											{
												$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																								'filters' => array(																			  
																												 array(
																														'logic' => 'AND',
																														'filters' => array( 
																																			array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																			array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																		)
																													),
																												 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																												)
																							 );	
											}
											$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
											//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
											if($room_type_info)
											{
												$room_type_info_key = 0;				
												foreach($room_type_info as $room_type_info_entry)
												{	
													if($list_mapper->searchPrice($room_type_info_entry, $postValue, array('controller_obj' => array('view' => $this->view))))
													{					
														$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
														$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
														$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
														$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
														$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
														$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
														$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
														$room_type_info_entry_arr['max_people_lang_format']	=	$this->translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
														$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
														$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
														$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
														$room_type_info_entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
														$save = 0;
														if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
														{
															$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
															$save = round($save);
														}
														$room_type_info_entry_arr['save_price'] = $save;
														$room_type_info_entry_arr['save_format'] = $this->translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
														$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
														$room_type_info_entry_arr['just_booked'] = $booked;
														
														//Room Info Start
														if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
														{
															$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
														}
														else
														{
															 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
														}
														$room_type_info_entry_arr['room_info'] = $room_info;
														$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
														if($device->isMobile())
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
														}
														else
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_list_available_type_llast', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_list_available_type_left', $this->view->numbers(count($room_info)));
														}
														$room_type_info_entry_arr['room_info_count_format2'] = $this->translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
														$room_type_info_entry_arr['room_info_count_format3'] = $this->translator->translator('hotels_search_sugges_room_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
														$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
														
														//Room Info End
														
														$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
														$room_type_info_key++;
													}
												}
											}	
										}
										catch(Exception $e)
										{
																			
										}
									}								
									//Room Type End
																
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}	
		}
	}
		
	public function categoryAction()
	{
		$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
		if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
		{
			foreach($postValue['filter']['filters'] as $fieldObj)
			{
				$postValue[$fieldObj['field']]  = $fieldObj['value'];
			}
		}
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		if (!empty($this->_page_id)) 
		{
			$posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $this->_page_id);
			$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
			
			$category_db = new Hotels_Model_DbTable_Category();
			
			$category_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
			$category_search_params['filter']['logic'] = ($category_search_params['filter']['logic']) ? $category_search_params['filter']['logic'] : 'and';	
			$category_info = $category_db->getListInfo('1', $category_search_params, false);
			
			if($category_info)
			{
				$category_info = (!is_array($category_info)) ? $category_info->toArray() : $category_info;
				$this->view->assign('category_info', $category_info[0] );
				$posted_data['sort'][] = array('field' => $category_info[0]['file_sort'],  'dir' => $category_info[0]['file_order']);
			}
		}		
		
		if($category_info)
		{
			if ($this->_request->isPost()) 
			{		
				try
				{
					$this->_helper->layout->disableLayout();
					$this->_helper->viewRenderer->setNoRender();
					$device = new Administrator_Controller_Helper_Device();
					
					// action body
					$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
						
					$getViewPageNum = $this->_request->getParam('pageSize'); 	
					$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), ($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page',    true);
					$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
					
					$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
					Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
											
					$encode_params = Zend_Json_Encoder::encode($posted_data);
					$encode_postValue = Zend_Json_Encoder::encode($postValue);
					$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
					$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
					if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
					{	
						$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
						$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
						$review_helper = new Review_View_Helper_Review();
						$room_type_db = new Hotels_Model_DbTable_RoomType();
						$room_db = new Hotels_Model_DbTable_Room();
						$today = date('Y-m-d');					
						$list_mapper = new Hotels_Model_HotelsListMapper();	
						$list_datas =  $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
						$view_datas = array('data_result' => array(), 'total' => 0);			
						if($list_datas)
						{						
							$key = 0;				
							foreach($list_datas as $entry)
							{
									$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
									$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
									$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
									$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
									$entry_arr['hotels_phone_format'] = $this->view->numbers($entry_arr['hotels_phone']);
									$entry_arr['feature_distance_from_airport_format'] = $this->view->numbers($entry_arr['feature_distance_from_airport']);
									$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
									$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
									$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
									$img_thumb_arr = explode(',', $entry_arr['hotels_image']);	
									$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
									$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
									$entry_arr['hotels_image_no_format'] = $this->translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
									
									$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
									$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
									
									$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
									$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
									$hotel_stars = '';
									for($i = 1; $i < $maximum_stars_digit; $i++)
									{
										$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
										
									}
									$entry_arr['hotel_stars_format']	= 	$hotel_stars;
									$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
									
									$entry_arr['total_nights']				=	$list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]);
									$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
									$entry_arr['total_nights_format']		=	$this->view->numbers($entry_arr['total_nights']);
									$entry_arr['total_nights_format_lang']	=	$this->translator->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
									$entry_arr['room_type_info']			=	null; 
									
									//Room Type Start
									if(!empty($entry_arr['room_type_id']))
									{	
										try
										{	
											$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
											$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
											$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
											if(!empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
											{
												$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																								'filters' => array(																			  
																												 array(
																														'logic' => 'AND',
																														'filters' => array( 
																																			array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																			array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																		)
																													),
																												 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																												)
																							 );	
											}
											$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
											
											if($room_type_info)
											{
												$room_type_info_key = 0;				
												foreach($room_type_info as $room_type_info_entry)
												{	
													if($list_mapper->searchPrice($room_type_info_entry, $postValue, array('controller_obj' => array('view' => $this->view))))
													{						
														$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
														$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
														$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
														$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
														$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
														$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
														$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
														$room_type_info_entry_arr['max_people_lang_format']	=	$this->translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
														$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
														$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
														$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
														$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
														$room_type_info_entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
														$save = 0;
														if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
														{
															$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
															$save = round($save);
														}
														$room_type_info_entry_arr['save_price'] = $save;
														$room_type_info_entry_arr['save_format'] = $this->translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
														$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
														$room_type_info_entry_arr['just_booked'] = $booked;
														
														//Room Info Start
														if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
														{
															$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
														}
														else
														{
															 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
														}
														$room_type_info_entry_arr['room_info'] = $room_info;
														$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
														if($device->isMobile())
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
														}
														else
														{
															$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_list_available_type_llast', $this->view->numbers(count($room_info)));
															$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_list_available_type_left', $this->view->numbers(count($room_info)));
														}
														$room_type_info_entry_arr['room_info_count_format2'] = $this->translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
														$room_type_info_entry_arr['room_info_count_format3'] = $this->translator->translator('hotels_search_sugges_room_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
														$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
														
														//Room Info End
														
														$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
														$room_type_info_key++;	
													}
												}												
											}	
										}
										catch(Exception $e)
										{
																			
										}
									}								
									//Room Type End
																
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;					
							}
							$view_datas['total']	=	$list_datas->getTotalItemCount();
						}
						$this->_controllerCache->save($view_datas , $uniq_id);
					}				
					$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
				}
				
				//Convert To JSON ARRAY	
				$res_value =  Zend_Json_Encoder::encode($json_arr);				
				$this->_response->setBody($res_value);	
			}	
		}
	}
	
	public function lastdealAction()
    {			
		$postValue  = (Eicra_Global_Variable::getSession()->hotel_search_info) ? Eicra_Global_Variable::getSession()->hotel_search_info : null ;
		if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
		{
			foreach($postValue['filter']['filters'] as $fieldObj)
			{
				$postValue[$fieldObj['field']]  = $fieldObj['value'];
			}
		}
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		$this->view->currency = $this->getCurrency();
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				$device = new Administrator_Controller_Helper_Device();
				
				if ($preferences_data && $preferences_data['hotels_list_by_last_deal_other_page_sortby'] && $preferences_data['hotels_list_by_last_deal_other_page_sortby'] != '_') 
				{
					$sort_arr = explode('-', $preferences_data['hotels_list_by_last_deal_other_page_sortby']);
					$posted_data['sort'][] = array('field' => $sort_arr[0],  'dir' => $sort_arr[1]);
				}	
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
					
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), ($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_postValue = Zend_Json_Encoder::encode($encode_postValue);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj.'_'.$encode_postValue));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{	
					$maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
					$vote = new Vote_Controller_Helper_ShowVoteButton('inline','hotels_page', false );
					$review_helper = new Review_View_Helper_Review();
					$room_type_db = new Hotels_Model_DbTable_RoomType();
					$room_db = new Hotels_Model_DbTable_Room();
					$list_mapper = new Hotels_Model_HotelsListMapper();	
					$today = date('Y-m-d');					
					$list_db = new Hotels_Model_DbTable_Hotels();	
					$list_datas =  $list_db->getListInfo('1', $posted_data, array('userChecking' => false));
					$view_datas = array('data_result' => array(), 'data_2nd_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['last_deal'] = false;
								$entry_arr['id_format'] =  $this->view->numbers($entry_arr['id']);
								$entry_arr['hotels_name_format'] = $list_mapper->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
								$entry_arr['hotels_phone_format'] = $this->view->numbers($entry_arr['hotels_phone']);
									$entry_arr['feature_distance_from_airport_format'] = $this->view->numbers($entry_arr['feature_distance_from_airport']);
								$entry_arr['hotels_desc_format'] = $this->view->escape(strip_tags( $entry_arr['hotels_desc']));
								$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
								$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
								$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
								$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
								$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
								$entry_arr['hotels_image_no_format'] = $this->translator->translator('hotels_front_page_hotels_photo_no', $this->view->numbers($hotels_image_no));
								//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
								$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
								$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));
								
								$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
								$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
								$hotel_stars = '';
								for($i = 1; $i < $maximum_stars_digit; $i++)
								{
									$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$this->view->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
									
								}
								$entry_arr['hotel_stars_format']	= 	$hotel_stars;
								$entry_arr['vote_format']			= 	$vote->getButton($entry_arr['id'] , $this->view->escape($entry_arr['hotels_name']));								
								
								$entry_arr['total_nights']				=	$list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]);
								$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
								$entry_arr['total_nights_format']		=	$this->view->numbers($entry_arr['total_nights']);
								$entry_arr['total_nights_format_lang']	=	$this->translator->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
								$entry_arr['room_type_info']			=	null; 
								
								
								//Room Type Start
								if(!empty($entry_arr['room_type_id']))
								{	
									try
									{	
										$search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
										$search_params['filter']['logic'] = ($search_params['filter']['logic']) ? $search_params['filter']['logic'] : 'and';					
										$search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
										if(!empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
										{
											$search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																							'filters' => array(																			  
																											 array(
																													'logic' => 'AND',
																													'filters' => array( 
																																		array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $list_mapper->getNights($postValue[Eicra_File_Constants::HOTELS_CHECK_IN], $postValue[Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																		array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																																	)
																												),
																											 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																											)
																						 );	
										}
										$room_type_info = $room_type_db->getListInfo('1', $search_params , false);
										//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
										if($room_type_info)
										{
											$room_type_info_key = 0;				
											foreach($room_type_info as $room_type_info_entry)
											{						
												$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
												$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
												$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
												$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
												$room_type_info_entry_arr['room_type_image_format'] = ($this->view->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$this->view->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
												$room_type_info_entry_arr['id_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['id']));
												$room_type_info_entry_arr['max_people_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people']));
												$room_type_info_entry_arr['max_people_lang_format']	=	$this->translator->translator('hotels_front_page_list_title_people', $this->view->numbers($this->view->escape($room_type_info_entry_arr['max_people'])));
												$room_type_info_entry_arr['room_num_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['room_num']));
												$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
												$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
												$room_type_info_entry_arr['basic_price_format']	=	$this->view->numbers($this->view->escape($room_type_info_entry_arr['basic_price']));
												$room_type_info_entry_arr['descount_price_format']	=	$this->view->numbers($this->view->price($this->view->escape($room_type_info_entry_arr['descount_price'])));
												$room_type_info_entry_arr['form_data_view_title_format']	=	$this->translator->translator('form_data_view_title', $this->view->escape($room_type_info_entry_arr['room_type']), 'Members');
												$save = 0;
												if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
												{
													$save = ( ($this->view->escape($room_type_info_entry_arr['basic_price']) - $this->view->price($this->view->escape($room_type_info_entry_arr['descount_price']))) / $this->view->escape($room_type_info_entry_arr['basic_price']) ) * 100;
													$save = round($save);
												}
												$room_type_info_entry_arr['save_price'] = $save;
												$room_type_info_entry_arr['save_format'] = $this->translator->translator('hotels_front_page_list_save_money', $this->view->numbers($save));
												$booked = ($room_db->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
												$room_type_info_entry_arr['just_booked'] = $booked;
												
												//Room Info Start
												if(!empty($postValue) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($postValue[Eicra_File_Constants::HOTELS_CHECK_OUT]))
												{
													$room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], $postValue);																	
												}
												else
												{
													 $room_info = $room_db->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
												}
												$last_time = ($preferences_data['hotels_list_by_last_deal_on_other_page']) ? $preferences_data['hotels_list_by_last_deal_on_other_page'] : 3;
												if(count($room_info) <= $last_time && count($room_info) > '0') 
												{
													$room_type_info_entry_arr['room_info'] = $room_info;
													$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
													if($device->isMobile())
													{
														$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_room_available_type_last', $this->view->numbers(count($room_info)));
														$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_room_available_type_left', $this->view->numbers(count($room_info)));
													}
													else
													{
														$room_type_info_entry_arr['room_info_count_format'] = $this->translator->translator('hotels_front_page_list_available_type_llast', $this->view->numbers(count($room_info)));
														$room_type_info_entry_arr['room_info_count_format1'] = $this->translator->translator('hotels_front_page_list_available_type_left', $this->view->numbers(count($room_info)));
													}
													$room_type_info_entry_arr['room_info_count_format2'] = $this->translator->translator('hotels_front_page_list_available_type_last_desc', $this->currency->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
													$room_type_info_entry_arr['room_info_count_format3'] = $this->translator->translator('hotels_search_sugges_room_left', $this->view->numbers($room_type_info_entry_arr['room_info_available_room']));
													$room_type_info_entry_arr['show_room_type'] = ($preferences_data['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
														
													//Room Info End
													$entry_arr['last_deal'] = (count($room_info) <= $last_time && count($room_info) > '0') ? true : $entry_arr['last_deal'];											
													$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
													$room_type_info_key++;	
												}					
											}
										}	
									}
									catch(Exception $e)
									{
										throw new Exception($e->getMessage());
										break;							
									}
								}								
								//Room Type End
								if($entry_arr['last_deal'] == true)
								{						
									$view_datas['data_result'][$key]	=	$entry_arr;	
									$key++;	
								}
						}
						
						//Pagination Starts
						$paginator = Zend_Paginator::factory($view_datas['data_result']);
						$paginator->setItemCountPerPage($viewPageNum);
						$paginator->setCurrentPageNumber($pageNumber);
						foreach($paginator as $paginator_key => $entry)
						{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$view_datas['data_2nd_result'][$paginator_key]	=	$entry_arr;
						}
						$view_datas['total']	=	$paginator->getTotalItemCount();						
						//Pagination End
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_2nd_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}					
		
	}
	
	public function detailsAction()
    {		
		$this->view->module = $this->_request->getModuleName();
		$this->view->controller = $this->_request->getControllerName();
		$this->view->action = $this->_request->getActionName();	
		
		$preferences_db = new Hotels_Model_DbTable_Preferences();
		$preferences_data = $preferences_db->getOptions();
		$this->view->assign('preferences_data', $preferences_data);    
			
		if (!empty($this->_page_id)) 
		{								
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details';					
			if( ($hotels_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{				
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				//$hotels_info = $hotels_db->getHotelsInfo($this->_page_id);
				
				$search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
				$hotels_info_obj = $hotels_db->getListInfo(null, $search_params,  array('hotels_page' => array( 'hp.*' ), 'userChecking' => false)); 
				if($hotels_info_obj)
				{
					$hotels_info_arr = ($hotels_info_obj) ? $hotels_info_obj->toArray() : null;
					$hotels_info = $hotels_info_arr[0];
					$hotels_info = (is_array($hotels_info)) ? array_map('stripslashes', $hotels_info) : stripslashes($hotels_info);
				}
				$this->_controllerCache->save($hotels_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($hotels_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Hotels_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($hotels_info['hotels_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}			
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$hotels_info	=	$dynamic_value_db->getFieldsValueInfo($hotels_info,$hotels_info['id']);										
			}
			$hotelsForm = new Hotels_Form_ProductForm ($group_info);
			$hotelsForm->populate($hotels_info);
			
			//Assign Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$hotels_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
			$postValue = Eicra_Global_Variable::getSession()->hotel_search_info;
			if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
			{
				foreach($postValue['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}
			$this->view->post_search_info = $postValue;	
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $hotels_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo; 
			$this->view->tab	= 0;
			$this->view->hotelsForm = $hotelsForm;
			$this->view->currency = $this->getCurrency();
			$this->view->room_type_db = new Hotels_Model_DbTable_RoomType();
			$this->view->hotels_db = new Hotels_Model_DbTable_Hotels();
			$this->view->roomCalendar_db = new Hotels_Model_DbTable_Calendar();	
		}
		else if($this->_request->getParam('hotels_title'))
		{
			
			$hotels_title = $this->_request->getParam('hotels_title');
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details_'.preg_replace('/[^a-zA-Z0-9_]/','_',$hotels_title);
			if( ($hotels_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$hotels_db = new Hotels_Model_DbTable_Hotels();				
				$search_params['filter']['filters'][0] = array('field' => 'hotels_title', 'operator' => 'eq', 'value' => $hotels_title);
				$hotels_info_obj = $hotels_db->getListInfo(null, $search_params,  array('hotels_page' => array( 'hp.*' ), 'userChecking' => false)); 
				if($hotels_info_obj)
				{
					$hotels_info_arr = ($hotels_info_obj) ? $hotels_info_obj->toArray() : null;
					$hotels_info = $hotels_info_arr[0];
					$hotels_info = (is_array($hotels_info)) ? array_map('stripslashes', $hotels_info) : stripslashes($hotels_info);
				}
				$this->_controllerCache->save($hotels_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($hotels_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Hotels_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($hotels_info['hotels_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			$this->view->view_datas = $hotels_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo;
			$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
						
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$hotels_info	=	$dynamic_value_db->getFieldsValueInfo($hotels_info,$hotels_info['id']);										
			}
			$hotelsForm = new Hotels_Form_ProductForm ($group_info);
			$hotelsForm->populate($hotels_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$hotels_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}	
			
			$postValue = Eicra_Global_Variable::getSession()->hotel_search_info;
			if($postValue && $postValue['filter'] && $postValue['filter']['filters'])
			{
				foreach($postValue['filter']['filters'] as $fieldObj)
				{
					$postValue[$fieldObj['field']]  = $fieldObj['value'];
				}
			}
			$this->view->post_search_info = $postValue;	
			$this->view->group_datas = $group_info;
			$this->view->hotelsForm = $hotelsForm;
			$this->view->currency = $this->getCurrency();
			$this->view->room_type_db = new Hotels_Model_DbTable_RoomType();
			$this->view->hotels_db = new Hotels_Model_DbTable_Hotels();	
			$this->view->roomCalendar_db = new Hotels_Model_DbTable_Calendar();			
		}
	}
	
	public function printAction()
    {	
		$this->_helper->layout->disableLayout();	
		$this->view->module = $this->_request->getModuleName();
		$this->view->controller = $this->_request->getControllerName();
		$this->view->action = $this->_request->getActionName();	    
			
		if (!empty($this->_page_id)) 
		{
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_print';
			if( ($hotels_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$hotels_info = $hotels_db->getHotelsInfo($this->_page_id);
				$this->_controllerCache->save($hotels_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($hotels_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Hotels_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($hotels_info['hotels_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}			
			
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$hotels_info	=	$dynamic_value_db->getFieldsValueInfo($hotels_info,$hotels_info['id']);										
			}
			$hotelsForm = new Hotels_Form_ProductForm ($group_info);
			$hotelsForm->populate($hotels_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$hotels_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}
			
			$this->view->group_datas = $group_info;
			$this->view->view_datas = $hotels_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo; 
			$this->view->tab	= 0;
			$this->view->hotelsForm = $hotelsForm;
			$this->view->currency = $this->getCurrency();
			$this->view->room_type_db = new Hotels_Model_DbTable_RoomType();
			$this->view->hotels_db = new Hotels_Model_DbTable_Hotels();
		}
		else if($this->_request->getParam('hotels_title'))
		{
			$hotels_title = $this->_request->getParam('hotels_title');
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_details_'.preg_replace('/[^a-zA-Z0-9_]/','_',$hotels_title);
			if( ($hotels_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$hotels_db = new Hotels_Model_DbTable_Hotels();
				$hotels_title_info = $hotels_db->getTitleToId($hotels_title);			
				$hotels_id = $hotels_title_info[0]['id'];
				$hotels_info = $hotels_db->getHotelsInfo($hotels_id);
				$this->_controllerCache->save($hotels_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_group_info';
			if( ($group_info = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($hotels_info['group_id']);
				$this->_controllerCache->save($group_info, $uniq_id);
			}
			
			$uniq_id = preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url()).'_BusinessTypeInfo';
			if( ($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false ) 
			{
				$type_db = new Hotels_Model_DbTable_BusinessType();
				$BusinessTypeInfo  = $type_db->getBusinessTypeInfo($hotels_info['hotels_type']);
				$this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
			}
			
			$this->view->view_datas = $hotels_info;
			$this->view->businessTypeInfo = $BusinessTypeInfo;
			$this->view->tab	= ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
						
			//Dynamic Form
			if(!empty($group_info['dynamic_form']))
			{   
				$group_info['form_id'] = $group_info['dynamic_form']; 					
				$dynamic_value_db = new Members_Model_DbTable_FieldsValue();	
				$hotels_info	=	$dynamic_value_db->getFieldsValueInfo($hotels_info,$hotels_info['id']);										
			}
			$hotelsForm = new Hotels_Form_ProductForm ($group_info);
			$hotelsForm->populate($hotels_info);
			
			//Assigh Review
			if(!empty($group_info['review_id']))
			{
				$review_helper = new Review_View_Helper_Review();
				$review_datas = $review_helper->getReviewList($group_info,$hotels_info['id']);				
				$this->view->review_datas = $review_datas;
				$this->view->review_helper = $review_helper;
			}	
			
			$this->view->group_datas = $group_info;
			$this->view->hotelsForm = $hotelsForm;
			$this->view->currency = $this->getCurrency();
			$this->view->room_type_db = new Hotels_Model_DbTable_RoomType();
			$this->view->hotels_db = new Hotels_Model_DbTable_Hotels();
		}
	}
	
	//For Ajax Save Hotels
	public function saveAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		if ($this->_request->isPost()) 
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$auth = Zend_Auth::getInstance ();
			if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$user_id = $globalIdentity->user_id;
				$hotels_id = $this->_request->getPost('id');
				try
				{
					$clause    = $this->_DBconn->quoteInto('user_id = ?', $user_id);
					$validator_saved_hotels = new Zend_Validate_Db_RecordExists(
								array(
									'table' 	=> Zend_Registry::get('dbPrefix').'hotels_saved',
									'field' 	=> 'hotels_id',
									'exclude' 	=> $clause
								)
							);
					if(!$validator_saved_hotels->isValid($hotels_id))
					{
						//Update category_id of the product of this category to zero
						$data = array(					
							'user_id' 		=> 	$user_id,
							'hotels_id' 	=>	$hotels_id										
						);
						try
						{
							$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'hotels_saved',$data);
							$msg = $translator->translator("hotels_recorded_success");
							$json_arr = array('status' => 'ok','msg' =>  $msg);
						}
						catch(Exception $e)
						{
							$msg = $translator->translator("hotels_recorded_err");
							$json_arr = array('status' => 'err','msg' =>  $msg.' '.$e->getMessage());
						}				
					}
					else
					{								
						$msg = $translator->translator("hotels_already_saved_err");
						$json_arr = array('status' => 'err','msg' =>  $msg);
					}				
				}
				catch(Exception $e)
				{
					$json_arr = array('status' => 'err','msg' =>  $e->getMessage());
				}				
			}
			else
			{
				$msg = $translator->translator("common_login_err");
				$json_arr = array('status' => 'err','msg' =>  $msg);
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
		
	//For Ajax Search
	public function searchAction()
	{
		if($this->_request->getPost('search_type') != 'post')
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
		}
		
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			
			$search_obj = new Hotels_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch();
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			if($this->_request->getPost('search_type') == 'post')
			{
				$group_db =  new Hotels_Model_DbTable_HotelsGroup();
				$group_info = $group_db->getGroupName($this->_request->getParam('group_id'));
				$this->view->group_datas = $group_info;
				$this->view->post_datas = $this->_request->getPost();
				if($result['status'] == 'ok')
				{					 
					 $this->view->view_datas = $result['result_data'];
				}
				else
				{
					$this->view->view_datas = null;
				}
			}
			else
			{
				//Convert To JSON ARRAY	
				$res_value = Zend_Json_Encoder::encode($json_arr);	
				$this->_response->setBody($res_value);
			}
		}
	}
	
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			$postData_arr = $this->_request->getPost();
			$i=0;
			foreach($postData_arr as $key=>$value)
			{
				$search_field[$i]	=	$key;
				$i++;
			}
			$search_field[$i]	= 'active';
			$postData_arr['active'] = 1;
			$search_obj = new Hotels_Model_Search($this->view,$search_field,$postData_arr);
			$result = $search_obj->doSearch('12');
			if($result['status'] == 'ok')
			{
				$msg = $translator->translator("hotels_search_success");
				$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $result['result_data'],'where' =>  $result['where']);
			}
			else
			{
				$json_arr = array('status' => 'err','msg' =>  $result['msg'],'where' =>  $result['where']);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function statesAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$country_id = $this->_request->getPost('id');
			$states = new Eicra_Model_DbTable_State();			
        	$states_options = $states->getOptions($country_id);
			if($states_options)
			{
				$states = array();
				$i = 0;
				foreach($states_options as $key=>$value)
				{
					$states[$i] = array('state_id' => $key,'state_name' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','states' => $states);				
			}
			else
			{
				$msg = $translator->translator("common_state_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function areasAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{
			$state_id = $this->_request->getPost('id');
			$cities = new Eicra_Model_DbTable_City();			
        	$cities_options = $cities->getOptions($state_id);
			if($cities_options)
			{
				$cities = array();
				$i = 0;
				foreach($cities_options as $key=>$value)
				{
					$cities[$i] = array('city_id' => $key,'city' => $value);
					$i++;	
				}
				$json_arr = array('status' => 'ok','msg' => '','cities' => $cities);				
			}
			else
			{
				$msg = $translator->translator("common_area_found_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
			$res_value =  Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}	
	
	private function getCurrency (){
		
		if (empty($this->currency)){
			$global_conf = Zend_Registry::get('global_conf');
			$this->currency = new Zend_Currency($global_conf['default_locale']);
			return $this->currency;
		}
		
		else {
			return $this->currency;
		}
		
	}
	
	public function loadAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		try
		{		
			$id = $this->_request->getParam('id');
			$res_value = '';			
			if(!empty($id))
			{
				
				$res_value_arr = array();
				$roomCalendar_db = new Hotels_Model_DbTable_Calendar();				
				$roomCalendar_info = $roomCalendar_db->getCalendarInfo($id);				
				if($roomCalendar_info)
				{					
					foreach($roomCalendar_info as $key => $value_arr)
					{
						$res_value_arr[$key] = $value_arr['calendar_date'].';;'.$value_arr['room_status'].';;'.$value_arr['room_price'];
					}
					$res_value = implode(',', $res_value_arr);					
				}						
			}
		}
		catch(Exception $e)
		{
			$res_value = '';
		}		
		$this->_response->setBody($res_value);
	}
	
	public function itinerarylistAction()
	{
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Frontend-Login';
		Eicra_Global_Variable::getSession()->returnLink = $this->view->url();	
		Eicra_Global_Variable::checkSession($this->_response,$url);
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$this->view->currency = $this->getCurrency();
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$status = $this->getRequest()->getParam('status');	
				
				$posted_data['filter']['filters'][] = array('field' => 'module_name', 'operator' => 'eq', 'value' => $this->view->getModule);
				$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';			
									
				$getViewPageNum = $this->_request->getParam('pageSize');	
				$frontend_route = 	($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id')  : $this->_request->getParam('menu_id').'/:page' ) : 'Hotel-Itinerary-List/*';
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'status' => $status, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route,    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;			
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;	
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
				
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$list_mapper = new Invoice_Model_InvoicesListMapper();
					$invoice_items_db = new Invoice_Model_DbTable_InvoiceItems();
					$hotels_db = new Hotels_Model_DbTable_Hotels();
					$roomType_db = new Hotels_Model_DbTable_RoomType();
					$room_db = new Hotels_Model_DbTable_Room();
					$list_datas =  $list_mapper->fetchAll($pageNumber, $status, $posted_data);
					
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;	
						$status_class = new Invoice_Controller_Helper_Status($this->translator);			
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_invoice'] = str_replace('_', '-', $entry_arr['id']);	
								unset($entry_arr['invoice_desc']); 
								$entry_arr['invoice_create_date_format']=  date('Y-m-d h:i:s A',strtotime($entry_arr['invoice_create_date']));
								$entry_arr['invoice_status_format']	=	$status_class->getStatus($entry_arr['status']);
								$entry_arr['invoice_now_paid_status_format']	=	$status_class->getNowPaidStatus();
								$entry_arr['invoice_now_unpaid_status_format']	=	$status_class->getNowUnPaidStatus();							
								$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;						
								
								//Invoice Items Start
								$invoice_items_info = $invoice_items_db->getInvoiceItems($entry_arr['id']);
								if($invoice_items_info)
								{
									foreach($invoice_items_info as $invoice_items_key => $invoice_items)
									{
										$object_value_decoded = Zend_Json_Decoder::decode($invoice_items['object_value']);
										$entry_arr['invoice_items'][$invoice_items_key]['item_id'] 			= 	$invoice_items['id'];
										$entry_arr['invoice_items'][$invoice_items_key]['check_in'] 		= 	$object_value_decoded['check_in'];
										$entry_arr['invoice_items'][$invoice_items_key]['check_out'] 		= 	$object_value_decoded['check_out'];
										$entry_arr['invoice_items'][$invoice_items_key]['hotels_id'] 		= 	$object_value_decoded['hotel_id'][0];
										$entry_arr['invoice_items'][$invoice_items_key]['hotels_info']		=	$hotels_db->getHotelsInfo($object_value_decoded['hotel_id'][0]);					
										$room_type_count = 0;
										foreach($object_value_decoded['room_type_id'] as $room_type_id_key => $room_type_id)
										{
											if(is_array($object_value_decoded['apartments_no'.$room_type_id_key]))
											{
												$entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_type_id'] = $room_type_id;
												$entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_id'] = $object_value_decoded['apartments_no'.$room_type_id_key];
												$entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_type_info'] = $roomType_db->getRoomTypeInfo($room_type_id);
												$entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_type_info']['max_people_format'] = $this->translator->translator('hotels_front_page_list_title_people',$this->view->escape($entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_type_info']['max_people']));
												foreach($entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_id'] as $room_id_key => $rooms_id)
												{
													$entry_arr['invoice_items'][$invoice_items_key]['room_type'][$room_type_count]['room_info'][$room_id_key] = $room_db->getRoomInfo($rooms_id);
												}
												$room_type_count++;
											}
										}
									}
								}
								//Invoice Items End
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);				
		}
	}
	
	
	public function googlemapAction(){
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$hotel = new Hotels_Model_DbTable_Hotels ();
		$hotelAddressMap = $hotel->getHotelMaps('30');
		/*
		echo ('<pre>');
		print_r($test);
		echo ('</pre>');
		exit;
		*/
		$this->view->assign('x', 'test me' );
		$this->view->assign('allAddresses', $hotelAddressMap);
	}
	
	public function schedulerAction()
	{
		$id = $this->_getParam('id', 0);
				
		//Get Room Info
		$room_db = new Hotels_Model_DbTable_Room();
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{				
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				if(!empty($id))
				{
					$posted_data['filter']['filters'][] = array('field' => 'room_id', 'operator' => 'eq', 'value' => $id);
					$posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';
				
										
					$list_db = new Hotels_Model_DbTable_Scheduler();	
					$list_datas =  $list_db->getListInfo(null, $posted_data, array(/*'hotels_page' => array('hotels_name' => 'vp.hotels_name'),*/ 'userChecking' => true));
					$view_datas = array('data_result' => array());			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								//$entry_arr['start'] = "/Date(".(strtotime($entry_arr['start_date']) * 1000 ).")/";
								//$entry_arr['end'] = "/Date(".(strtotime($entry_arr['end_date']) * 1000 ).")/";								
								$entry_arr['Start'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['Start']));
								$entry_arr['End'] = strftime('%Y-%m-%dT%H:%M:%SZ', strtotime($entry_arr['End']));
								$entry_arr['IsAllDay'] = (boolean)$entry_arr['IsAllDay'];
								
								$view_datas['data_result'][$key]	=	$entry_arr; //array('hotels_name' => $entry_arr['hotels_name'], 'start' => $entry_arr['start'], 'end' => $entry_arr['end'] );	
								$key++;					
						}					
					}
					$json_arr = array('id' => $id, 'status' => 'ok', 'data_result' => $view_datas['data_result'], 'posted_data' => $posted_data);
				}
				else
				{
					$json_arr = array('status' => 'ok', 'data_result' => '', 'posted_data' => $posted_data);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
		else
		{		
			if(!empty($id))
			{
				//Get Vacationrentals Info
				$roomInfo = $room_db->getRoomInfo($id);
				if($roomInfo)
				{				
					$this->view->id = $id;
					$this->view->entry_by = $roomInfo['entry_by'];	
					$this->view->roomInfo = $roomInfo;			
				}
				else
				{
					$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
				}
			}
			else
			{
				$this->_helper->redirector('add', $this->view->getController, $this->view->getModule, array());
			}
		}
	}
}
