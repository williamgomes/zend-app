<?php
class Hotels_ApiController extends Zend_Controller_Action
{
	private $apiForm;
	private $_controllerCache;
	private $_auth_obj;	
	private $translator;
    public function init()
    {
        /* Initialize action controller here */
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	public function preDispatch()
	{
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();
		$this->view->assign('getController', $getController);
		if($getAction != 'uploadfile')
		{
			$url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}
	}
	//LIST FUNCTION
	public function listAction()
    {
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$approve = $this->getRequest()->getParam('approve');				
				
				
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$api = new Hotels_Model_ApiListMapper();	
					$list_datas =  $api->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);			
					if($list_datas)
					{						
						$key = 0;				
						foreach($list_datas as $entry)
						{
								$entry_arr = $entry->toArray();
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
								$entry_arr['publish_status_api_name'] = str_replace('_', '-', $entry_arr['name']);							
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$list_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
    }
	public function updateAction()
	{
		$translator = Zend_Registry::get('translator');
		$id = $this->_request->getParam('id');
		$this->apiForm =  new Hotels_Form_ApiForm ($id);
		
		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$json_arr = $this->saveApiSettings($id);
			$res_value = Zend_Json_Encoder::encode($json_arr);
			$this->_response->setBody($res_value);
		}
		else
		{
		    $api_table = new Hotels_Model_DbTable_ApiSetting ();
		    $api_setting = $api_table->getInfo ($id);
			
		    $findValue = 0 ;
		    while ($findValue < count($api_setting))
			{
		    	$rowSet = $api_setting[$findValue];
		    	$fieldName =   $rowSet ['setting'];
		    	$fieldValue =  $rowSet ['value'] ;
		    	if (!empty($fieldName))
				{
		    		$this->apiForm->$fieldName->setValue($fieldValue);
		    	}
		    	$findValue++;
		    }
		    $this->view->apiForm = $this->apiForm;
		    $this->view->id	=	$id;
		    $this->render();
		}
	}
	private function saveApiSettings ($api_id)
	{
	    $translator = Zend_Registry::get('translator');
	    $json_arr = null;
	    try 
		{
	        if($this->apiForm->isValid($this->_request->getPost()))
	        {
	            $forms_Vaules = $this->_request->getPost();
				
				//print_r($expression);
	            $api_setting = new Hotels_Model_ApiSettings();
	            foreach ($forms_Vaules as $key => $val) 
				{
	                $apiSettingTable = new Hotels_Model_DbTable_ApiSetting ();
	                $id =  $apiSettingTable->getIdIfExists($key, $api_id);
	                if ($key == 'id') 
					{
	                    continue;
	                }
	                if (empty($id)) 
					{
	                    $api_setting->setApi_id($api_id);
	                    $api_setting->setSetting($key);
	                    $api_setting->setValue($val);
	                }
	                else 
					{
	                    $api_setting->setId($id);
	                    $api_setting->setApi_id($api_id);
	                    $api_setting->setSetting($key);
	                    $api_setting->setValue($val);
	                }
					
	                $result = $api_setting->saveApiSetting();
	            }
				
	            if($result['status'] == 'ok')
	            {
	                $json_arr = array('status' => 'ok','msg' => $translator->translator('hotels_api_save_success'));
	            }
	            else
	            {
	                $json_arr = array('status' => 'err','msg' => $result['msg'] .  $id);
	            }
	        }
	        else
	        {
	            $validatorMsg = $this->apiForm->getMessages();
	            $vMsg = array();
	            $i = 0;
	            foreach($validatorMsg as $key => $errType)
	            {
	                foreach($errType as $errkey => $value)
	                {
	                    $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
	                    $i++;
	                }
	            }
	            $json_arr = array('status' => 'errV','msg' => $vMsg);
	        }
	    } 
		catch (Exception $e) 
		{
	        $json_arr = array('status' => 'err','msg' => $e->getMessage() );
	    }
	    return $json_arr;
	}
	public function publishAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		if ($this->_request->isPost())
		{
			$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
			$id = $this->_request->getPost('id');
			$api_name = $this->_request->getPost('name');
			$paction = $this->_request->getPost('paction');
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;
			}
			//DB Connection
			$conn = Zend_Registry::get('msqli_connection');
			$conn->getConnection();
			// Update Article status
			$where = array();
			$where[] = 'id = '.$conn->quote($id);
			try
			{
				$conn->update(Zend_Registry::get('dbPrefix').'hotels_api_list',array('active' => $active), $where);
				$return = true;
			}
			catch (Exception $e)
			{				
				$return = false;
			}
			if($return)
			{
				$json_arr = array('status' => 'ok','active' => $active);
			}
			else
			{
				$msg = $translator->translator('hotels_api_list_publish_err',$api_name);
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		//Convert To JSON ARRAY
		$res_value = Zend_Json_Encoder::encode($json_arr);
		$this->_response->setBody($res_value);
	}
	public function publishallAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		if ($this->_request->isPost())
		{
			$id_str  = $this->_request->getPost('id_st');
			$paction = $this->_request->getPost('paction');
			switch($paction)
			{
				case 'publish':
					$active = '1';
					break;
				case 'unpublish':
					$active = '0';
					break;			
			}
			if(!empty($id_str))
			{
				$id_arr = explode(',',$id_str);
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				foreach($id_arr as $id)
				{
					// Update Article status
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{
						$conn->update(Zend_Registry::get('dbPrefix').'hotels_api_list',array('active' => $active), $where);
						$return = true;
					}
					catch (Exception $e)
					{
						$return = false;
					}
				}
				if($return)
				{
					$json_arr = array('status' => 'ok');
				}
				else
				{
					$msg = $translator->translator('hotels_api_list_publish_err');
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = $translator->translator("category_selected_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		
		//Convert To JSON ARRAY
		$res_value = Zend_Json_Encoder::encode($json_arr);
		$this->_response->setBody($res_value);
	}
}
