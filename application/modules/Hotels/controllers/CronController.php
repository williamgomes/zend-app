<?php
/*****
*
*	PLEASE RUN THE FOLLOWING CRON COMMAND IN CPANEL
*	wget -O - -q -t 1 http://demo.realestates.com.bd/Property/cron/expire
*
****/

class Hotels_CronController extends Zend_Controller_Action
{
	private $_DBconn;
	private $_translator;
	private $_controllerCache;
	
    public function init()
    {
        /* Initialize action controller here */				
		$translator = Zend_Registry::get('translator');
		$this->_translator = $translator;	
		
		//DB Connection
		$this->_DBconn = Zend_Registry::get('msqli_connection');
		$this->_DBconn->getConnection();
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();				
    }
	
	public function preDispatch() 
	{			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
	}
	
	public function expireAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$mem_db = new Members_Model_DbTable_MemberList();
		
		$today = date("Y-m-d");	
		$thisMonth = date("m");	
		$module = 'property';
		$controller = $this->_request->getControllerName();
		$action = $this->_request->getActionName();
		$packages = new Members_Controller_Helper_Packages();
		$field_name = $module.'_'.$controller.'_'.$action;
		
		$mem_info = $mem_db->getMembers();
		if($mem_info)
		{
			foreach($mem_info as $members)
			{
			    $user_id	= $members['user_id'];	
				$role_id	= $members['role_id'];	
				$register_date	=	$members['register_date'];
				$diff = abs(strtotime($register_date) - strtotime($today));
				$years = floor($diff / (365*60*60*24));
				$month = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
				$month_diff = $years*12 + $month;
				$package_info = $packages->getPackageFrontendFieldValue($module,$controller,$action,$user_id,$role_id);
				if($package_info)
				{
					$expire_date = $package_info;
					if(($month_diff > $expire_date) && $expire_date != '0')
					{
						try
						{
							$where = array();
							$where[] = 'entry_by = '.$this->_DBconn->quote($user_id).' OR hotels_agent = '.$this->_DBconn->quote($user_id);
							$data = array('active' => '0');
							$this->_DBconn->update(Zend_Registry::get('dbPrefix').'hotels_page',$data, $where);
						}
						catch(Exception $e)
						{
							echo $e->getMessage();
						}
					}
				}
			}			
		}
		echo "Cron Successfully Run";		
	}
	
	/*public function runAction()
    {	
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$today = date("Y-m-d");	
		$thisMonth = date("m");	
		$search_db = new Hotels_Model_DbTable_Search();
		$mem_db	=	new	Members_Model_DbTable_MemberList();
		
		//DEACTIVE PROPERTIES							
		$where = " available_expire_date < '$today' AND available_expire_date != '0000-00-00' AND available_expire_date != '' AND active = '1' ";	
		$order	=	" id ASC ";	
		
		$search_info = 	$search_db->getSearchInfo($where,$order);
		
		foreach($search_info as $property_info)
		{
			if(!$this->isnot_int($property_info['hotels_agent']))
			{				
				$mem_info	=	$mem_db->getMemberInfo($property_info['hotels_agent']);
				$to_mail	=	($mem_info['role_id'] != '100' && $mem_info['role_id'] != '101') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}			
			$active = '0';
			$letter_type = 'deactivation';
			$activation_never = '0';
			$available_activation_date = $property_info['available_activation_date'];
			$available_expire_date = $property_info['available_expire_date'];
			$data = array('active' => $active,  'activation_never' => $activation_never, 'available_expire_date' => '0000-00-00');
			$available_activation_date = ($activation_never == '1') ? $this->_translator->translator('common_never') : $available_activation_date;
			$datas = array('available_activation_date' => $available_activation_date, 'available_expire_date' => '0000-00-00');
			
			// Update Hotels status
			try
			{
				$whereS = array();
				$whereS[] = 'id = '.$this->_DBconn->quote($property_info['id']);
				$this->_DBconn->update(Zend_Registry::get('dbPrefix').'hotels_page',$data, $whereS);
				$email_obj = new Hotels_Controller_Helper_Emails();
				$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);
			}
			catch(Exception $e)
			{
				echo $e->getMessage();
			}
		}
		
		//ACTIVE PROPERTIES							
		$where = " available_activation_date <= '$today' AND available_activation_date != '0000-00-00' AND available_activation_date != '' AND active = '0' ";	
		$order	=	" id ASC ";	
		
		$search_info = 	$search_db->getSearchInfo($where,$order);
		
		foreach($search_info as $property_info)
		{
			if(!$this->isnot_int($property_info['hotels_agent']))
			{				
				$mem_info	=	$mem_db->getMemberInfo($property_info['hotels_agent']);
				$to_mail	=	($mem_info['role_id'] != '100' && $mem_info['role_id'] != '101') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}			
			$active = '1';
			$letter_type = 'activation';
			$activation_never = '0';
			$available_activation_date = $property_info['available_activation_date'];
			$available_expire_date = $property_info['available_expire_date'];
			$data = array('active' => $active,  'activation_never' => $activation_never, 'available_activation_date' => '0000-00-00');
			$available_activation_date = ($activation_never == '1') ? $this->_translator->translator('common_never') : $available_activation_date;
			$datas = array('available_activation_date' => '0000-00-00', 'available_expire_date' => $available_expire_date);
			
			// Update Hotels status
			try
			{
				$whereS = array();
				$whereS[] = 'id = '.$this->_DBconn->quote($property_info['id']);
				$this->_DBconn->update(Zend_Registry::get('dbPrefix').'hotels_page',$data, $whereS);
				$email_obj = new Hotels_Controller_Helper_Emails();
				$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);
			}
			catch(Exception $e)
			{
				echo $e->getMessage();
			}
		}
		
		//ELECTRIC EXPIRE PROPERTIES							
		$where = " meter_electric_expire <= '$today' AND meter_electric_expire != '0000-00-00' AND meter_electric_expire != '' ";	
		$order	=	" id ASC ";	
		
		$search_info = 	$search_db->getSearchInfo($where,$order);
		
		//Check Send Mail Previous
		$clause    = " MONTH(email_sent_date) = '$thisMonth' ";
		$validator = new Zend_Validate_Db_NoRecordExists(
			array(
				'table' => Zend_Registry::get('dbPrefix').'property_expire',
				'field' => 'electric_expire_id',
				'exclude' => $clause,
			)
		);
		
		foreach($search_info as $property_info)
		{
			if(!$this->isnot_int($property_info['hotels_agent']))
			{				
				$mem_info	=	$mem_db->getMemberInfo($property_info['hotels_agent']);
				$to_mail	=	($mem_info['role_id'] != '100' && $mem_info['role_id'] != '101') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}			
			
			$letter_type = 'electric_expire';
			$meter_electric_expire = $property_info['meter_electric_expire'];	
			$data = array('electric_expire_id' => $property_info['id'], 'email_sent_date' => date("Y-m-d"));						
			$datas = array('meter_electric_expire' => $meter_electric_expire);
			
			
			
			if($validator->isValid($property_info['id']))
			{
				// Update Hotels status	
				try
				{
					$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'property_expire',$data);
					$email_obj = new Hotels_Controller_Helper_Emails();
					$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);						
				}
				catch(Exception $e)
				{
					echo $e->getMessage();
				}				
			}
		}
		
		//GAS EXPIRE PROPERTIES							
		$where = " meter_gas_expire <= '$today' AND meter_gas_expire != '0000-00-00' AND meter_gas_expire != '' ";	
		$order	=	" id ASC ";	
		
		$search_info = 	$search_db->getSearchInfo($where,$order);
		
		//Check Send Mail Previous
		$clause    = " MONTH(email_sent_date) = '$thisMonth' ";
		$validator = new Zend_Validate_Db_NoRecordExists(
			array(
				'table' => Zend_Registry::get('dbPrefix').'property_expire',
				'field' => 'gas_expire_id',
				'exclude' => $clause,
			)
		);
		
		foreach($search_info as $property_info)
		{
			if(!$this->isnot_int($property_info['hotels_agent']))
			{				
				$mem_info	=	$mem_db->getMemberInfo($property_info['hotels_agent']);
				$to_mail	=	($mem_info['role_id'] != '100' && $mem_info['role_id'] != '101') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}			
			
			$letter_type = 'gas_expire';
			$meter_gas_expire = $property_info['meter_gas_expire'];	
			$data = array('gas_expire_id' => $property_info['id'],  'email_sent_date' => date("Y-m-d"));						
			$datas = array('meter_gas_expire' => $meter_gas_expire);
			
			
			
			if($validator->isValid($property_info['id']))
			{
				// Update Hotels status	
				try
				{
					$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'property_expire',$data);
					$email_obj = new Hotels_Controller_Helper_Emails();
					$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);						
				}
				catch(Exception $e)
				{
					echo $e->getMessage();
				}				
			}
		}
		
		//ENERGY EXPIRE PROPERTIES							
		$where = " meter_energy_expire <= '$today' AND meter_energy_expire != '0000-00-00' AND meter_energy_expire != '' ";	
		$order	=	" id ASC ";	
		
		$search_info = 	$search_db->getSearchInfo($where,$order);
		
		//Check Send Mail Previous
		$clause    = " MONTH(email_sent_date) = '$thisMonth' ";
		$validator = new Zend_Validate_Db_NoRecordExists(
			array(
				'table' => Zend_Registry::get('dbPrefix').'property_expire',
				'field' => 'energy_expire_id',
				'exclude' => $clause,
			)
		);
		
		foreach($search_info as $property_info)
		{
			if(!$this->isnot_int($property_info['hotels_agent']))
			{				
				$mem_info	=	$mem_db->getMemberInfo($property_info['hotels_agent']);
				$to_mail	=	($mem_info['role_id'] != '100' && $mem_info['role_id'] != '101') ? $mem_info['username'] : null;
			}
			else
			{
				$to_mail	=	null;
			}			
			
			$letter_type = 'energy_expire';
			$meter_energy_expire = $property_info['meter_energy_expire'];	
			$data = array('energy_expire_id' => $property_info['id'],  'email_sent_date' => date("Y-m-d"));						
			$datas = array('meter_energy_expire' => $meter_energy_expire);
			
			
			
			if($validator->isValid($property_info['id']))
			{
				// Update Hotels status	
				try
				{
					$this->_DBconn->insert(Zend_Registry::get('dbPrefix').'property_expire',$data);
					$email_obj = new Hotels_Controller_Helper_Emails();
					$result = $email_obj->sendEmail($datas,null,null,$to_mail,$letter_type);						
				}
				catch(Exception $e)
				{
					echo $e->getMessage();
				}				
			}
		}
		
		echo "SuccessFull..";
		exit;
	}*/
	
	public function isnot_int($hotels_agent)
	{
		$r = true;			
		if(is_numeric($hotels_agent))
		{ 
			if(is_int((int)$hotels_agent))
			{
				$r = false;
			}				
		}
		return $r;
	}
}

