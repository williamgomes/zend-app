<?php
class Hotels_View_Helper_Categorytree extends Zend_View_Helper_Abstract 
{
	public static function checkChild($parent,$group_id)
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		$select = $conn->select()
						->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_category'), array('g.category_name','g.category_title','g.category_order','g.parent'))						
						->where('g.parent = ?', $parent)
						->where('g.group_id = ?', $group_id);
		$rs = $select->query()->fetchAll();	
		if($rs)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function getSubCategory($parent,$view,$group_id,$selectCategory = null) 
	{
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$selectSubCategory = $conn->select()
						->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_category'), array('g.id','g.group_id','g.category_name','g.category_title','g.parent','g.category_order'))						
						->where('g.parent = ?', $parent)
						->where('g.group_id = ?', $group_id);
			$rs = $selectSubCategory->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $topic_datas)
				{
					$class = (!empty($topic_datas['parent'])) ? "file" : "folder";
					$v .= '<tr id="node-'.$topic_datas['id'].'"';
						 if(!empty($topic_datas['parent']) || ($topic_datas['parent'] == 0 )){  $v .= ' class="child-of-node-'.$topic_datas['parent'].'"'; } $v .= ' >';
							if($selectCategory == $topic_datas['id'])
							{
								$v .= '<td style="background-color:#D7D7D7">';
							}
							else
							{
								$v .= '<td>';
							}
								$v .= '<span class="'.$class.'">'.$view->escape($topic_datas['category_name']).'</span>';
							$v .= '</td>';
						 $v .= '</tr>';
					
					if(Hotels_View_Helper_Categorytree::checkChild($topic_datas['id'],$group_id))
					{
						$v .= Hotels_View_Helper_Categorytree::getSubCategory($topic_datas['id'],$view,$group_id,$selectCategory);
					}					
				}
				return $v;
			}
	}
	
	public static function getTreeDataSource($parent,$view,$group_id,$selectCategory = null, $expanded = false)
	{
		$category_db = new Hotels_Model_DbTable_Category();
		$category_info	=	$category_db->getListInfo(null, array('filter' => array('filters' => array(array('field' => 'group_id', 'operator' => 'eq', 'value' => $group_id)))), false);
		if($category_info && !is_array($category_info)){  $category_info = $category_info->toArray();} 
		
		if($parent == '' || $parent == null)
		{
			$data_obj = array(
								array(
										'id'	=> '0',
										'text' => html_entity_decode($view->translator->translator("common_tree_root"), ENT_QUOTES, 'UTF-8'), 
										'imageUrl'=> "vendor/scripts/js/images/folder.png",
										'expanded' => (bool)$expanded,
										'hasChildren'	=> true								
								)
						);			
			if(self::hasChild('0', $category_info))
			{
				$data_obj[0]['hasChildren'] =  true;
				$data_obj[0]['items'] = ($expanded === true) ? self::recursiveTree($parent, $view, $category_info, null, $expanded) : null  ;
			}
			else
			{
				$data_obj[0]['hasChildren'] =  false;
				$data_obj[0]['expanded'] =  false;
				$data_obj[0]['imageUrl'] =  'vendor/scripts/js/images/page_white_text.png';
			}
		}
		else
		{
				$data_obj = self::recursiveTree($parent, $view, $category_info, null, $expanded);  
		}
		
		return $data_obj;
	}
	
	private static function recursiveTree($parent, $view, $category_info, $selectCategory = null, $expanded = false)
	{
		if($category_info)
		{
			$arr_count = 0;
			$data_obj = array();			
			foreach($category_info as $key => $info)
			{
				if($info['parent'] == $parent)
				{					
					$data_obj[$arr_count] = array( 'id'	=> $info['id'], 'parent' => $info['parent'], 'text' => $view->escape($info['category_name']), 'imageUrl' => "vendor/scripts/js/images/page_white_text.png" );
					if(self::hasChild($info['id'], $category_info))
					{
						$data_obj[$arr_count]['expanded']	=	(bool)$expanded;
						$data_obj[$arr_count]['hasChildren']	=	true;
						$data_obj[$arr_count]['imageUrl']	=	'vendor/scripts/js/images/folder.png';
						$data_obj[$arr_count]['items'] = ($expanded === true) ? self::recursiveTree($data_obj[$arr_count]['id'], $view, $category_info, $selectCategory) : null;
					}
					else
					{
						$data_obj[$arr_count]['hasChildren']	=	false;
						$data_obj[$arr_count]['expanded']	=	false;
						$data_obj[$arr_count]['imageUrl']	=	'vendor/scripts/js/images/page_white_text.png';
					}
					$arr_count++;					
				}								
			}			
		}	
		return $data_obj;	
	}
	
	private static function hasChild($parent, $category_info)
	{
		$hasChild = false;
		foreach($category_info as $key => $info)
		{
			if($parent == $info['parent'])
			{
				$hasChild = true;
			}
		}
		return $hasChild;
	}	
}