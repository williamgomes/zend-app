<?php
class Hotels_View_Helper_Suggestion extends Zend_View_Helper_Abstract 
{
	public function suggestion($postValue, $entry_arr, $thisController)
	{
		$booking_suggestion_arr = array();
		if($postValue && !empty($postValue['search_adult']) && !empty($postValue['search_room']))
		{			
			if($entry_arr['room_type_info'])
			{						
				usort($entry_arr['room_type_info'], array("Hotels_View_Helper_Suggestion", "sortArrayByPeople"));				
				$abc = $this->getSuggestionRoom($entry_arr['room_type_info'], 	$postValue);				
				//file_put_contents('abc.txt', $abc);
				$booking_suggestion_arr = array();
				$cnt = 0;
				//$efg = file_get_contents('efg.txt')."\n\n\n";
				foreach($entry_arr['room_type_info'] as $key => $room_type_info)
				{
					if(!empty($room_type_info['room_plus']) && $room_type_info['room_plus'] > 0)
					{
						$booking_suggestion_arr[$cnt] = $room_type_info;
						$efg .= 'search_adult = '.$postValue['search_adult']. ', search_room '.$postValue['search_room'].',  max_people = '	.$room_type_info['max_people'].',  room_info_available_room = '	.$room_type_info['room_info_available_room'].', '.$room_type_info['room_plus'].' X '.$room_type_info['max_people'].', sub_total_people = '.($room_type_info['room_plus'] * $room_type_info['max_people'])."\n";
						$cnt++;
					}
				}
				//file_put_contents('efg.txt', $efg);							
			}
		}
		return $booking_suggestion_arr;
	}
	
	public function getSuggestionRoom(&$info_arr, $postValue, $decrement = 0)
	{
		$sum_search	= array( 'sum_people'  =>	$postValue['search_adult'],  'sum_room'	=>	$postValue['search_room']);		
		//$abc = file_get_contents('abc.txt')."\n\n\n";
		$total_room = 0;
		$total_people = 0;
		foreach($info_arr as $key => $info)
		{				
			if($sum_search['sum_room'] > 0)
			{
				if($info['room_info_available_room'] == $sum_search['sum_room'])
				{
					$info['room_plus'] = $info['room_info_available_room'];
					$sum_search['sum_room'] -= $info['room_plus'];
				}
				else if($info['room_info_available_room'] > $sum_search['sum_room'] )
				{
					$info['room_plus'] =  $sum_search['sum_room'];
					$sum_search['sum_room'] -= $info['room_plus'];
				}
				else if($info['room_info_available_room'] < $sum_search['sum_room'] )
				{
					$info['room_plus'] = $info['room_info_available_room'];
					$sum_search['sum_room'] = $sum_search['sum_room'] - $info['room_info_available_room'];					
				}
				$info_arr[$key]  =	$info;
				$total_people 	+= 	$info['room_plus'] * $info['max_people'];
				$total_room 	+= 	$info['room_plus'];
				$abc_con 		 = 	', '.$info['room_plus'].' X '.$info['max_people'].', sub_total_people = '.$sub_total_people.', sum_people = '.$sum_search['sum_people'].', sum_room = '.$sum_search['sum_room'];
				if($total_room >= $postValue['search_room'])
				{
					break;
				}
			}
			else
			{
				$abc_con = '';
			}
			$abc .= 'search_adult = '.$postValue['search_adult']. ', search_room '.$postValue['search_room'].',  max_people = '	.$info['max_people'].',  room_info_available_room = '	.$info['room_info_available_room'].$abc_con."\n"; 
		}
		
		return $abc;
	}
	
	public function getSuggestion(&$info_arr, $postValue, $decrement = 0)
	{
		$sum_search	= array( 'sum_people'  =>	$postValue['search_adult'],  'sum_room'	=>	$postValue['search_room']);		
		//$abc = file_get_contents('abc.txt')."\n\n\n";
		$total_room = 0;
		foreach($info_arr as $key => $info)
		{				
			if($sum_search['sum_people'] > 0)
			{
				$sub_total_people = $this->calculation($info, $postValue, $sum_search, $decrement); 				
				$sum_search['sum_people'] = $sum_search['sum_people'] - $sub_total_people;
				$sum_search['sum_room'] = $sum_search['sum_room'] - $info['room_plus'];
				$info_arr[$key] 	=	$info;
				$total_room += $info['room_plus'];
				$abc_con = ', '.$info['room_plus'].' X '.$info['max_people'].', sub_total_people = '.$sub_total_people.', sum_people = '.$sum_search['sum_people'].', sum_room = '.$sum_search['sum_room'];
			}
			else
			{
				$abc_con = '';
			}
			$abc .= 'search_adult = '.$postValue['search_adult']. ', search_room '.$postValue['search_room'].',  max_people = '	.$info['max_people'].',  room_info_available_room = '	.$info['room_info_available_room'].$abc_con."\n"; 
		}
		if($sum_search['sum_people'] > 0 && ($decrement+1) <= $info['room_info_available_room'])
		{
			$abc = $this->getSuggestion($info_arr, $postValue, ($decrement+1));
		}
		if(($sum_search['sum_room'] != 0 && $total_room != $postValue['search_room']) && ($decrement+1) <= $info['room_info_available_room'])
		{
			//$abc = $this->getSuggestion($info_arr, $postValue, ($decrement+1));
		}
		return $abc;
	}
	
	public function calculation(&$info, $postValue, $sum_search, $decrement = 0)
	{
		$info['room_plus']	=	($info['room_info_available_room'] - $decrement);
		$sub_total_people =  $info['max_people'] * $info['room_plus'];
		
		if($sub_total_people > $sum_search['sum_people'] && ($decrement+1) <= $info['room_info_available_room'])
		{
			$sub_total_people = $this->calculation($info, $postValue, $sum_search, ($decrement + 1));
		}
		return $sub_total_people;
	}
	
	public function sortArrayByPeople($a, $b)
	{
	  if ($a["max_people"] == $b["max_people"]) { return 0; }
	  return ($a["max_people"] < $b["max_people"]) ? -1 : 1;
	}
}