<?php

class Hotels_View_Helper_HotelsGroup extends Zend_View_Helper_Abstract 
{	
	public function getNumOfHotels($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_category'), array('COUNT(*) AS num_cat'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_cat = $row['num_cat'];
		}
		return $num_cat;
	}
	
	public function getNumOfHotelsType($group_id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_business_type'), array('COUNT(*) AS num_type'))
					   ->where('g.group_id = ?',$group_id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_type = $row['num_type'];
		}
		return $num_type;
	}
	
	public function getNumOfArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'cities'), array('COUNT(*) AS num_area'))
					   ->where('g.state_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_area = $row['num_area'];
		}
		return $num_area;
	}
	
	public function getNumOfProForArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.area_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfHotelsForType($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT(*) AS num_pro'))
					   ->where('g.hotels_type = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_pro = $row['num_pro'];
		}
		return $num_pro;
	}
	
	public function getNumOfHotelsForRoomType($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('room_type_id'));
		
		$rs = $select->query()->fetchAll();
		$count = 0;
		foreach($rs as $row)
		{
			$num_pro_arr = explode(',',$row['room_type_id']);
			if(in_array($id,$num_pro_arr))
			{
				$count++;
			}
		}
		return $count;
	}
	
	public function getNumOfProduct($group_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT(*) AS num_product'));
		if(!empty($group_id))
		{
					  $select ->where('g.group_id = ?',$group_id);
		}
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_product = $row['num_product'];
		}
		return $num_product;
	}
	
	public function getNumOfProductCountry($group_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'hotels_page'), array('COUNT( DISTINCT g.country_id ) AS num_country'));
		if(!empty($group_id))
		{
				$select->where('g.group_id = ?',$group_id);
		}
					   
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_product = $row['num_country'];
		}
		return $num_product;
	}
	
	public function getPopularHotels($group_id, $limit = 20)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('vv' => Zend_Registry::get('dbPrefix').'vote_voting'), array('item_id' => 'DISTINCT(vv.table_id)', 'votes' => 'SUM(vv.vote_value)'))
					   ->where('hp.group_id = ?',$group_id)
					   ->where('vv.table_name = ?','hotels_page')
					   ->group('vv.table_id')
					   ->order('votes DESC')
					   ->joinLeft(array('hp' => Zend_Registry::get('dbPrefix').'hotels_page'), 'vv.table_id = hp.id')
					   ->limit($limit);
		
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			$options = $rs;
		}
		else
		{
			$options = null;
		}
		return $options;
	}
	
}
