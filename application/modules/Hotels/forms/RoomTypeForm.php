<?php
class Hotels_Form_RoomTypeForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.RoomTypeForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.RoomTypeForm.ini', 'room') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/en_US.RoomTypeForm.ini', 'room');
            parent::__construct($config->room );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->setErrorMessage();	
			 $this->loadHotels();
			 $this->loadPricePlan();
        }
		
		private function setErrorMessage()
		{
			$this->primary_image->setErrorMessages(array('Primary Image is not set.'));
		}
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}	
		
		private function loadHotels()
		{
			$hotel_db = new Hotels_Model_DbTable_Hotels();
			$hotel_options = $hotel_db->getHotelsInfoByGroup();
			if($hotel_options)
			{
				foreach($hotel_options as $key => $hotels)
				{
					$this->hotel_id->addMultiOption($hotels['id'], stripslashes($hotels['hotels_name']));
				}
			}
			$this->hotel_id->setRegisterInArrayValidator(false);
		}
		
		private function loadPricePlan()
		{
			$price_db = new Hotels_Model_DbTable_Price();
			$price_options = $price_db->getOptions(true);
			$translator = Zend_Registry::get('translator');
			$this->price_plan_id->addMultiOption('', $translator->translator('common_select'));
			if($price_options)
			{
				foreach($price_options as $key => $price_plan_name)
				{
					$this->price_plan_id->addMultiOption($key, stripslashes($price_plan_name));
				}
			}
			$this->price_plan_id->setRegisterInArrayValidator(false);
		}
}