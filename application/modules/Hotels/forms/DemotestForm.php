<?php

class Hotels_Form_DemotestForm extends Zend_Form {

    protected $_editor;

    public function __construct($options = null) {
        $translator = Zend_Registry::get('translator');
        $config = (file_exists(APPLICATION_PATH . '/modules/Hotels/forms/source/' . $translator->getLangFile() . '.DemotestForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/Hotels/forms/source/' . $translator->getLangFile() . '.DemotestForm.ini', 'demo') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/Hotels/forms/source/en_US.DemotestForm.ini', 'demo');
        parent::__construct($config->demo);
    }

    public function init() {
        $this->createForm();
    }

    public function createForm() {
        $this->elementDecorator();
    }

    //Element Decorator
    private function elementDecorator() {
        $this->setElementDecorators(array(
            'ViewHelper', 'FormElements',
        ));
    }

    

}
