<?php
class Hotels_Form_PriceForm  extends Zend_Form {

		protected $_editor;
		protected $_options;
		
		public function __construct($options = null) 
		{
			$this->_options = $options;
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.PriceForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.PriceForm.ini', 'price') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/en_US.PriceForm.ini', 'price');
            parent::__construct($config->price );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->loadRoomTypes();				 	
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}	
		
		private function loadRoomTypes()
		{
			$room_type_db = new Hotels_Model_DbTable_RoomType();
			$room_type_options = $room_type_db->getListInfo(null, null, true);
			$translator = Zend_Registry::get('translator');
			$this->room_type_id->addMultiOption('', $translator->translator('common_select'));
			if($room_type_options)
			{
				$room_type_arr = array();
				$c = 0;
				foreach($room_type_options as $key => $room_types)
				{					
					if(!$this->hasPricePlan($room_types['price_plan_id'], $room_type_arr))
					{
						$room_type_arr[$c] = array(
												'price_plan_id'   	=> 	$room_types['price_plan_id'],
												'plan_name'   		=> 	(!empty($room_types['price_plan_id'])) ? '('.$translator->translator('hotels_price_plan_header_name').': '.$room_types['plan_name'].')' : '('.$translator->translator('hotels_room_type_price_plan_not_set').')',
												'room_type_arr'		=>	array( $room_types['id'] =>	stripslashes($room_types['room_type']) )
												);
						$c++;
					}
					else
					{
						foreach($room_type_arr as $room_type_arr_key => $room_type_arr_arr)
						{
							if($room_types['price_plan_id'] == $room_type_arr_arr['price_plan_id'])
							{
								$room_type_arr[$room_type_arr_key]['room_type_arr'][$room_types['id']] = stripslashes($room_types['room_type']) ;
							}
						}
					}				
				}
				
				foreach($room_type_arr as $room_type_arr_key => $room_type_arr_arr)
				{
					$this->room_type_id->addMultiOption($room_type_arr_arr['plan_name'], $room_type_arr_arr['room_type_arr']);					
				}
			}
			$this->room_type_id->setRegisterInArrayValidator(false);
		}
		
		private function hasPricePlan($price_plan_id, $room_type_arr)
		{
			$temp = false;
			foreach($room_type_arr as $key => $room_type_arr_arr)
			{
				if($price_plan_id == $room_type_arr_arr['price_plan_id'])
				{
					$temp = true;
					break;
				}
			}
			return $temp;
		}
		
		public function addDisplayGroups(array $groups_arr)
		{
			foreach($groups_arr as $groups)
			{
			
				foreach ($groups as $key => $spec) {
					$name = null;
					if (!is_numeric($key)) {
						$name = $key;
					}
		
					if ($spec instanceof Zend_Form_DisplayGroup) {
						$this->_addDisplayGroupObject($spec);
					}
		
					if (!is_array($spec) || empty($spec)) {
						continue;
					}
		
					$argc    = count($spec);
					$options = array();
		
					if (isset($spec['elements'])) {
						$elements = $spec['elements'];
						if (isset($spec['name'])) {
							$name = $spec['name'];
						}
						if (isset($spec['options'])) {
							$options = $spec['options'];
						}
						$this->addDisplayGroup($elements, $name, $options);
					} else {
						switch ($argc) {
							case (1 <= $argc):
								$elements = array_shift($spec);
								if (!is_array($elements) && (null !== $name)) {
									$elements = array_merge((array) $elements, $spec);
									$this->addDisplayGroup($elements, $name);
									break;
								}
							case (2 <= $argc):
								if (null !== $name) {
									$options = array_shift($spec);
									$this->addDisplayGroup($elements, $name, $options);
									break;
								}
								$name = array_shift($spec);
							case (3 <= $argc):
								$options = array_shift($spec);
							default:
								$this->addDisplayGroup($elements, $name, $options);
						}
					}
				}
			}
			return $this;
		}
}