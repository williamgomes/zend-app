<?php

class Hotels_Form_TypeForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.TypeForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/'.$translator->getLangFile().'.TypeForm.ini', 'type') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Hotels/forms/source/en_US.TypeForm.ini', 'type');
            parent::__construct($config->type );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->loadHotelsGroup();	
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}
		
		public function loadHotelsGroup ()
		{			
			$hotelsGroup = new Hotels_Model_DbTable_HotelsGroup();			
        	$hotelsGroup_options = $hotelsGroup->getGroupInfo();	
			$translator = Zend_Registry::get('translator');		
			$this->group_id->addMultiOption('',$translator->translator('hotels_group_add_edit_select'));
			$this->group_id->addMultiOptions($hotelsGroup_options);									 
		}
		 	  
}