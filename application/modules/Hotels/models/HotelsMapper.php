<?php
class Hotels_Model_HotelsMapper
{
	protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Hotels');
        }
        return $this->_dbTable;
    }

	public function save(Hotels_Model_Hotels $obj)
		{

			if ((null === ($id = $obj->getId())) || empty($id))
			{
				try
				{
					unset($data['id']);

					$data = array(
						'group_id' 						=> 	$obj->getGroup_id(),
						'category_id' 					=>	$obj->getCategory_id(),
						'room_type_id' 					=>	$obj->getRoom_type_id(),
						'hotels_name' 					=>	$obj->getHotels_name(),
						'hotels_title' 					=>	$obj->getHotels_title(),
						'hotels_agent' 					=>	$obj->getHotels_agent(),
						'hotels_type'	 				=>	$obj->getHotels_type(),
						'hotels_grade' 					=>	$obj->getHotels_grade(),
						'related_items'					=>	$obj->getRelated_items(),
						'hotels_primary_image'			=>	$obj->getHotels_primary_image(),
						'hotels_image' 					=>	$obj->getHotels_image(),
						'entry_by' 						=>	$obj->getEntry_by(),
						'hotels_desc' 					=>	$obj->getHotels_desc(),
						'hotels_order'					=>	$obj->getHotels_order(),
						'hotels_date' 					=>	date("Y-m-d h:i:s"),
						'featured' 						=>	$obj->getFeatured(),
						'active' 						=>	$obj->getActive(),
						'meta_title' 					=>	$obj->getMeta_title(),
						'meta_keywords' 				=>	$obj->getMeta_keywords(),
						'meta_desc' 					=>	$obj->getMeta_desc(),
						'country_id' 					=>	$obj->getCountry_id(),
						'state_id' 						=>	$obj->getState_id(),
						'area_id' 						=>	$obj->getArea_id(),
						'location_for_search'			=>	$obj->getLocation_for_search(),
						'hotels_building_no' 			=>	$obj->getHotels_building_no(),
						'hotels_building_name' 			=>	$obj->getHotels_building_name(),
						'hotels_phone'					=>	$obj->getHotels_phone(),
						'hotels_fax' 					=>	$obj->getHotels_fax(),
						'hotels_email' 					=>	$obj->getHotels_email(),
						'post_code' 					=>	$obj->getPost_code(),
						'hotels_address' 				=>	$obj->getHotels_address(),
						'feature_distance_from_airport'	=>	$obj->getFeature_distance_from_airport(),
						'feature_distance_from_city_market' =>	$obj->getFeature_distance_from_city_market(),
						'feature_pet' 					=>	$obj->getFeature_pet(),
						'feature_facilities' 			=>	$obj->getFeature_facilities(),
						'feature_sports_recreations'	=>	$obj->getFeature_sports_recreations(),
						'hotels_status' 				=>	$obj->getHotels_status(),
						'hotels_about_information' 		=>	$obj->getHotels_about_information(),
						'hotels_about_general' 			=>	$obj->getHotels_about_general(),
						'hotels_about_services' 		=>	$obj->getHotels_about_services(),
						'hotels_about_internet' 		=>	$obj->getHotels_about_internet(),
						'hotels_about_parking' 			=>	$obj->getHotels_about_parking(),
						'hotels_about_other' 			=>	$obj->getHotels_about_other(),
						'hotels_policy_policies' 		=>	$obj->getHotels_policy_policies(),
						'feature_primary_interior_image'=>	$obj->getFeature_primary_interior_image(),
						'feature_interior_plan_image' 	=>	$obj->getFeature_interior_plan_image(),
						'feature_google_map' 			=>	$obj->getFeature_google_map(),
						'feature_additional' 			=>	$obj->getFeature_additional(),
						'brochure_title' 				=>	$obj->getBrochure_title(),
						'brochure_desc' 				=>	$obj->getBrochure_desc(),
						'checkin_date' 					=>	$obj->getCheckin_date(),
						'checkin_time' 					=>	$obj->getCheckin_time(),
						'checkout_date' 				=>	$obj->getCheckout_date(),
						'checkout_time' 				=>	$obj->getCheckout_time(),
						'hotels_policy_terms_condition' =>	$obj->getHotels_policy_terms_condition(),
						'hotels_policy_privacy' 		=>	$obj->getHotels_policy_privacy(),
						'cancellation_policy'			=>	$obj->getCancellation_policy(),
						'payment_options' 				=>	$obj->getPayment_options(),
						'payment_desc' 					=>	$obj->getPayment_desc(),
						'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
						'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
						'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
						'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
						'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
						'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
						'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
						'billing_item_desc' 						=>	$obj->getBilling_item_desc()
					);

					$last_id = $this->getDbTable()->insert($data);

					$result = array('status' => 'ok' ,'id' => $last_id);
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());
				}

			}
			else
			{
				try
				{
					if(($obj->getCategory_id() == $obj->getPrev_category()) && ($obj->getGroup_id() == $obj->getPrev_group()))
					{
						$data = array(
							'group_id' 						=> 	$obj->getGroup_id(),
							'category_id' 					=>	$obj->getCategory_id(),
							'room_type_id' 					=>	$obj->getRoom_type_id(),
							'hotels_name' 					=>	$obj->getHotels_name(),
							'hotels_title' 					=>	$obj->getHotels_title(),
							'hotels_agent' 					=>	$obj->getHotels_agent(),
							'hotels_grade' 					=>	$obj->getHotels_grade(),
							'related_items'					=>	$obj->getRelated_items(),
							'hotels_type'	 				=>	$obj->getHotels_type(),
							'hotels_primary_image'			=>	$obj->getHotels_primary_image(),
							'hotels_image' 					=>	$obj->getHotels_image(),
							'hotels_desc' 					=>	$obj->getHotels_desc(),
							'meta_title' 					=>	$obj->getMeta_title(),
							'meta_keywords' 				=>	$obj->getMeta_keywords(),
							'meta_desc' 					=>	$obj->getMeta_desc(),
							'country_id' 					=>	$obj->getCountry_id(),
							'state_id' 						=>	$obj->getState_id(),
							'area_id' 						=>	$obj->getArea_id(),
							'location_for_search'			=>	$obj->getLocation_for_search(),
							'hotels_building_no' 			=>	$obj->getHotels_building_no(),
							'hotels_building_name' 			=>	$obj->getHotels_building_name(),
							'hotels_phone' 					=>	$obj->getHotels_phone(),
							'hotels_fax' 					=>	$obj->getHotels_fax(),
							'hotels_email' 					=>	$obj->getHotels_email(),
							'post_code' 					=>	$obj->getPost_code(),
							'hotels_address' 				=>	$obj->getHotels_address(),
							'feature_distance_from_airport' =>	$obj->getFeature_distance_from_airport(),
							'feature_distance_from_city_market' =>	$obj->getFeature_distance_from_city_market(),
							'feature_pet' 					=>	$obj->getFeature_pet(),
							'feature_facilities' 			=>	$obj->getFeature_facilities(),
							'feature_sports_recreations'	=>	$obj->getFeature_sports_recreations(),
							'hotels_status' 				=>	$obj->getHotels_status(),
							'hotels_about_information' 		=>	$obj->getHotels_about_information(),
							'hotels_about_general' 			=>	$obj->getHotels_about_general(),
							'hotels_about_services' 		=>	$obj->getHotels_about_services(),
							'hotels_about_internet' 		=>	$obj->getHotels_about_internet(),
							'hotels_about_parking' 			=>	$obj->getHotels_about_parking(),
							'hotels_about_other' 			=>	$obj->getHotels_about_other(),
							'hotels_policy_policies' 		=>	$obj->getHotels_policy_policies(),
							'feature_primary_interior_image'=>	$obj->getFeature_primary_interior_image(),
							'feature_interior_plan_image' 	=>	$obj->getFeature_interior_plan_image(),
							'feature_google_map' 			=>	$obj->getFeature_google_map(),
							'feature_additional' 			=>	$obj->getFeature_additional(),
							'brochure_title' 				=>	$obj->getBrochure_title(),
							'brochure_desc' 				=>	$obj->getBrochure_desc(),							
							'checkin_date' 					=>	$obj->getCheckin_date(),
							'checkin_time' 					=>	$obj->getCheckin_time(),
							'checkout_date' 				=>	$obj->getCheckout_date(),
							'checkout_time' 				=>	$obj->getCheckout_time(),
							'hotels_policy_terms_condition' =>	$obj->getHotels_policy_terms_condition(),
							'hotels_policy_privacy' 		=>	$obj->getHotels_policy_privacy(),
							'cancellation_policy'			=>	$obj->getCancellation_policy(),
							'hotels_policy_surroundings'	=>	$obj->getHotels_policy_surroundings(),
							'payment_options' 				=>	$obj->getPayment_options(),
							'payment_desc' 					=>	$obj->getPayment_desc(),
							'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
							'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
							'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
							'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
							'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
							'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
							'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
							'billing_item_desc' 						=>	$obj->getBilling_item_desc()
						);
					}
					else
					{
						$data = array(
							'group_id' 						=> 	$obj->getGroup_id(),
							'category_id' 					=>	$obj->getCategory_id(),
							'room_type_id' 					=>	$obj->getRoom_type_id(),
							'hotels_title' 					=>	$obj->getHotels_title(),
							'hotels_name' 					=>	$obj->getHotels_name(),
							'hotels_agent' 					=>	$obj->getHotels_agent(),
							'hotels_grade' 					=>	$obj->getHotels_grade(),
							'related_items'					=>	$obj->getRelated_items(),
							'hotels_type'	 				=>	$obj->getHotels_type(),
							'hotels_primary_image'			=>	$obj->getHotels_primary_image(),
							'hotels_image' 					=>	$obj->getHotels_image(),
							'hotels_desc' 					=>	$obj->getHotels_desc(),
							'hotels_order'	    			=>	$obj->getHotels_order(),
							'meta_title' 					=>	$obj->getMeta_title(),
							'meta_keywords' 				=>	$obj->getMeta_keywords(),
							'meta_desc' 					=>	$obj->getMeta_desc(),
							'country_id' 					=>	$obj->getCountry_id(),
							'state_id' 						=>	$obj->getState_id(),
							'area_id' 						=>	$obj->getArea_id(),
							'location_for_search'			=>	$obj->getLocation_for_search(),
							'hotels_building_no' 			=>	$obj->getHotels_building_no(),
							'hotels_building_name' 			=>	$obj->getHotels_building_name(),
							'hotels_phone' 					=>	$obj->getHotels_phone(),
							'hotels_fax' 					=>	$obj->getHotels_fax(),
							'hotels_email' 					=>	$obj->getHotels_email(),
							'post_code' 					=>	$obj->getPost_code(),
							'hotels_address' 				=>	$obj->getHotels_address(),
							'feature_distance_from_airport' =>	$obj->getFeature_distance_from_airport(),
							'feature_distance_from_city_market' =>	$obj->getFeature_distance_from_city_market(),
							'feature_pet' 					=>	$obj->getFeature_pet(),
							'feature_facilities' 			=>	$obj->getFeature_facilities(),
							'feature_sports_recreations' 	=>	$obj->getFeature_sports_recreations(),
							'hotels_status' 				=>	$obj->getHotels_status(),
							'hotels_about_information' 		=>	$obj->getHotels_about_information(),
							'hotels_about_general' 			=>	$obj->getHotels_about_general(),
							'hotels_about_services' 		=>	$obj->getHotels_about_services(),
							'hotels_about_internet' 		=>	$obj->getHotels_about_internet(),
							'hotels_about_parking' 			=>	$obj->getHotels_about_parking(),
							'hotels_about_other' 			=>	$obj->getHotels_about_other(),
							'hotels_policy_policies' 		=>	$obj->getHotels_policy_policies(),
							'feature_primary_interior_image'=>	$obj->getFeature_primary_interior_image(),
							'feature_interior_plan_image' 	=>	$obj->getFeature_interior_plan_image(),
							'feature_google_map' 			=>	$obj->getFeature_google_map(),
							'feature_additional' 			=>	$obj->getFeature_additional(),
							'brochure_title' 				=>	$obj->getBrochure_title(),
							'brochure_desc' 				=>	$obj->getBrochure_desc(),
							'checkin_date' 					=>	$obj->getCheckin_date(),
							'checkin_time' 					=>	$obj->getCheckin_time(),
							'checkout_date' 				=>	$obj->getCheckout_date(),
							'checkout_time' 				=>	$obj->getCheckout_time(),
							'hotels_policy_terms_condition' =>	$obj->getHotels_policy_terms_condition(),
							'hotels_policy_privacy' 		=>	$obj->getHotels_policy_privacy(),
							'hotels_policy_surroundings'	=>	$obj->getHotels_policy_surroundings(),
							'payment_options' 				=>	$obj->getPayment_options(),
							'payment_desc' 					=>	$obj->getPayment_desc(),
							'billing_user_places_order_email_enable' 	=>	$obj->getBilling_user_places_order_email_enable(),
							'billing_user_places_order_email_address' 	=>	$obj->getBilling_user_places_order_email_address(),
							'billing_user_pay_invoice_email_enable' 	=>	$obj->getBilling_user_pay_invoice_email_enable(),
							'billing_user_pay_invoice_email_address' 	=>	$obj->getBilling_user_pay_invoice_email_address(),
							'billing_user_cancel_order_email_enable' 	=>	$obj->getBilling_user_cancel_order_email_enable(),
							'billing_user_cancel_order_email_address' 	=>	$obj->getBilling_user_cancel_order_email_address(),
							'billing_order_payment_email_enable' 		=>	$obj->getBilling_order_payment_email_enable(),
							'billing_item_desc' 						=>	$obj->getBilling_item_desc()
						);

					}
					// Start the Update process
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}
			}
			return $result;
		}



}
?>