<?php
class Hotels_Model_ExpediaApiModel
{
    private $_apiKey;
    private $_cid;
    private $_locale;
    private $_minorRev;
    private $_currencyCode;
    private $_numberOfResults;
    private $_supplierType;
    private $_api_settings;
    private $_request_hotel_list;
    private $_request_hotel_info;
    private $_booking_url;
    private $_sorted_by;
    private $_request_availability;
	private $_reservation_request_url;
	private $_city;
	private $_cancellation_request_url;
	private $_isTest;
	
    public function __construct ()
    {
        $this->_api_settings = new Hotels_Controller_Helper_Api();
    }
    /**
     *
     * @return the $_apiKey
     */
    public function getApiKey ()
    {
        if (empty($this->_apiKey)) {
            $this->setApiKey();
            return $this->_apiKey;
        } else {
            return $this->_apiKey;
        }
    }
    /**
     *
     * @param field_type $_apiKey
     */
    private function setApiKey ()
    {
        $this->_apiKey = $this->_api_settings->getValue('api_key', 2);
    }
    /**
     *
     * @return the $_cid
     */
    public function getCid ()
    {
        if (empty($this->_cid)) {
            $this->setCid();
            return $this->_cid;
        } else {
            return $this->_cid;
        }
    }
    /**
     *
     * @param field_type $_cid
     */
    private function setCid ()
    {
        $this->_cid = $this->_api_settings->getValue('cid', 2);
    }
    /**
     *
     * @return the $_locale
     */
    public function getLocale ()
    {
        if (empty($this->_locale)) {
            $this->setLocale();
            return $this->_locale;
        } else {
            return $this->_locale;
        }
    }
    /**
     *
     * @param field_type $_locale
     */
    private function setLocale ()
    {
        $this->_locale = (string) $this->_api_settings->getValue('locale ', 2);
    }
    /**
     *
     * @return the $_minorRev
     */
    public function getMinorRev ()
    {
        if (empty($this->_minorRev)) {
            $this->setMinorRev();
            return $this->_minorRev;
        } else {
            return $this->_minorRev;
        }
    }
    /**
     *
     * @param field_type $_minorRev
     */
    private function setMinorRev ()
    {
        $this->_minorRev = $this->_api_settings->getValue('minorRev', 2);
    }
    /**
     *
     * @return the $_currencyCode
     */
    public function getCurrencyCode ()
    {
        if (empty($this->_currencyCode)) {
            $this->setCurrencyCode();
            return $this->_currencyCode;
        } else {
            return $this->_currencyCode;
        }
    }
    /**
     *
     * @param field_type $_currencyCode
     */
    private function setCurrencyCode ()
    {
        $this->_currencyCode = (string) $this->_api_settings->getValue('currencyCode', 2);
    }
    /**
     *
     * @return the $_numberOfResults
     */
    public function getNumberOfResults ()
    {
        if (empty($this->_numberOfResults)) {
            $this->setNumberOfResults();
            return $this->_numberOfResults;
        } else {
            return $this->_numberOfResults;
        }
    }
    /**
     *
     * @param field_type $_numberOfResults
     */
    private function setNumberOfResults ()
    {
        $this->_numberOfResults = $this->_api_settings->getValue(
                'numberOfResults', 2);
    }
    /**
     *
     * @return the $_supplierType
     */
    public function getSupplierType ()
    {
        if (empty($this->_supplierType)) {
            $this->setSupplierType();
            return $this->_supplierType;
        } else {
            return $this->_supplierType;
        }
    }
    /**
     *
     * @param field_type $_supplierType
     */
    private function setSupplierType ()
    {
        $this->_supplierType = $this->_api_settings->getValue('supplierType', 2);
    }
    /**
     *
     * @return the $_supplierType
     */
    public function getRequestHotelList ($isTest)
    {
    	if (empty($this->_request_hotel_list)) {
    		$this->setRequestHotelList($isTest);
    		return $this->_request_hotel_list;
    	} else {
    		return $this->_request_hotel_list;
    	}
    } 
    /**
     *
     * @param field_type $_supplierType
     */
    private function setRequestHotelList($isTest)
    {
		if ($isTest){
	    	$this->_request_hotel_list = 'http://api.ean.com/ean-services/rs/hotel/v3/list';
		}
		else {
			$this->_request_hotel_list = 'http://dev.api.ean.com/ean-services/rs/hotel/v3/list';
		}
		

    }
    /**
     *
     * @return the $_supplierType
     */
    public function getBookingUrl ($isTest)
    {
    	if (empty($this->_booking_url)) {
    		$this->setBookingUrl($isTest);
    		return $this->_booking_url;
    	} else {
    		return $this->_booking_url;
    	}
    }
    /**
     *
     * @param field_type $_supplierType
     */
    private function setBookingUrl ($isTest)
    {
		if ($isTest){
	    	$this->_booking_url = 'https://book.api.ean.com';
		}
		else {
			$this->_booking_url = 'https://book.api.ean.com';
		}		
		
    	
    }
    /**
     *
     * @return the $_supplierType
     */
    public function getRequestHotelInfo ($isTest)
    {
    	if (empty($this->_request_hotel_info)) {
    		$this->setRequestHotelInfo($isTest);
    		return $this->_request_hotel_info ;
    	} else {
    		return $this->_request_hotel_info ;
    	}
    }
    /**
     *
     * @param field_type $_supplierType
     */
    private function setRequestHotelInfo($isTest)
    {
		if ($isTest){
	    	$this->_request_hotel_info = 'http://api.ean.com/ean-services/rs/hotel/v3/info';
		}
		else {
			$this->_request_hotel_info = 'http://dev.api.ean.com/ean-services/rs/hotel/v3/info';
		}
		
    	
    }
    /**
     *
     * @return the $_supplierType
     */
    public function getSortedBy ()
    {
    	if (empty($this->_sorted_by)) {
    		$this->setSortedBy();
    		return $this->_sorted_by;
    	} else {
    		return $this->_sorted_by;
    	}
    }
    /**
     *
     * @param field_type $_supplierType
     */
    private function setSortedBy ()
    {
    	$this->_sorted_by = $this->_api_settings->getValue('sort', 2);
    }
    /**
     *
     * @return the $_supplierType
     */
    public function getAvailabilityUrl ($isTest)
    {
    	if (empty($this->_request_availability)) {
    		$this->setAvailabilityUrl($isTest);
    		return $this->_request_availability;
    	} else {
    		return $this->_request_availability;
    	}
    }
    /**
     *
     * @param field_type $_supplierType
     */
    private function setAvailabilityUrl ($isTest)
    {
		if ($isTest){
	    	$this->_request_availability = 'http://api.ean.com/ean-services/rs/hotel/v3/avail';
		}
		else {
			$this->_request_availability = 'http://dev.api.ean.com/ean-services/rs/hotel/v3/avail';
		}		
		
    	
    }
	
	public function getReservationUrl ($isTest)
    {
    	if (empty($this->_reservation_request_url)) {
    		$this->setReservationUrl($isTest);
    		return $this->_reservation_request_url;
    	} else {
    		return $this->_reservation_request_url;
    	}
    }
    
	
    private function setReservationUrl ($isTest)
    { 
		if ($isTest){
	    	$this->_reservation_request_url = 'https://book.api.ean.com/ean-services/rs/hotel/v3/res';
		}
		else {
			$this->_reservation_request_url = 'https://book.api.ean.com/ean-services/rs/hotel/v3/res';
		}	
	
    	
    }	
	
	
	public function getCityName ()
    {
    	if (empty($this->city)) {
    		$this->setCityName();
    		return $this->_city;
    	} else {
    		return $this->_city;
    	}
    }
    
	
    private function setCityName ()
    {
    	$this->_city= $this->_api_settings->getValue(
                'city_name', 2);
    }		
	
	public function getCancellationurl ($isTest)
    {
    	if (empty($this->_cancellation_request_url)) {
    		$this->setCancellationurl($isTest);
    		return $this->_cancellation_request_url;
    	} else {
    		return $this->_cancellation_request_url;
    	}
    }
    
	
    private function setCancellationurl ($isTest)
    {
		if ($isTest){
	    	$this->_cancellation_request_url=  'http://api.ean.com/ean-services/rs/hotel/v3/cancel';
		}
		else {
			$this->_cancellation_request_url=  'http://dev.api.ean.com/ean-services/rs/hotel/v3/cancel';
		}		
    	
    }	
	
	
	public function getisTest ()
    {
    	if (empty($this->_isTest)) {
    		$this->setIsTest();
    		return $this->_isTest;
    	} else {
    		return $this->_isTest;
    	}
    }
    
	
    private function setIsTest ()
    {
		$this->_isTest = $this->_api_settings->getValue('ApiMode', 2);
		return $this->_isTest;
    }			
	
}
?>