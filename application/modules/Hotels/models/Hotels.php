<?php
class Hotels_Model_Hotels {
	protected $_id;
	protected $_category_id;
	protected $_group_id;
	protected $_room_type_id;
	protected $_hotels_name;
	protected $_hotels_title;
	protected $_hotels_agent;
	protected $_hotels_type;
	protected $_hotels_primary_image;
	protected $_hotels_image;
	protected $_hotels_desc;
	protected $_entry_by;
	protected $_hotels_grade;
	protected $_hotels_order;
	protected $_featured;
	protected $_active;
	protected $_prev_category;
	protected $_prev_group;
	protected $_checkin_hour;
	protected $_checkin_min;
	protected $_checkin_time;
	protected $_checkout_hour;
	protected $_checkout_min;
	protected $_checkout_time;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;
	protected $_country_id;
	protected $_state_id;
	protected $_area_id;
	protected $_location_for_search;
	protected $_hotels_building_no;
	protected $_hotels_building_name;
	protected $_hotels_phone;
	protected $_hotels_fax;
	protected $_hotels_email;
	protected $_post_code;
	protected $_hotels_address;
	protected $_feature_distance_from_airport;
	protected $_feature_distance_from_city_market;
	protected $_feature_pet;
	protected $_feature_facilities;
	protected $_feature_sports_recreations;
	protected $_hotels_status;
	protected $_hotels_about_information;
	protected $_hotels_about_general;
	protected $_hotels_about_services;
	protected $_hotels_about_internet;
	protected $_hotels_about_parking;
	protected $_hotels_about_other;
	protected $_hotels_policy_policies;
	protected $_feature_primary_interior_image;
	protected $_feature_interior_plan_image;
	protected $_feature_google_map;
	protected $_feature_additional;
	protected $_brochure_title;
	protected $_brochure_desc;
	protected $_checkin_date;
	protected $_checkout_date;
	protected $_day_night_limit;
	protected $_hotels_policy_terms_condition;
	protected $_hotels_policy_privacy;
	protected $_hotels_policy_surroundings;
	protected $_cancellation_policy;
	protected $_payment_options;
	protected $_payment_desc;
	protected $_related_items;
	protected $_billing_user_places_order_email_enable;
	protected $_billing_user_places_order_email_address;
	protected $_billing_user_pay_invoice_email_enable;
	protected $_billing_user_pay_invoice_email_address;
	protected $_billing_user_cancel_order_email_enable;
	protected $_billing_user_cancel_order_email_address;
	protected $_billing_order_payment_email_enable;
	protected $_billing_item_desc;


	public function __construct(array $options = null) {
		if (is_array ( $options )) {
			$this->setOptions ( $options );
		}
	}
	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || ! method_exists ( $this, $method )) {
			throw new Exception ( 'Invalid Auth hotels' );
		}
		$this->$method ( $value );
	}
	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || ! method_exists ( $this, $method )) {
			throw new Exception ( 'Invalid Auth hotels' );
		}
		return $this->$method ();
	}
	public function setOptions(array $options) {
		$methods = get_class_methods ( $this );
		foreach ( $options as $key => $value ) {
			$method = 'set' . ucfirst ( $key );
			if (in_array ( $method, $methods )) {
				$this->$method ( $value );
			}
		}
		return $this;
	}
	public function saveHotels() {
		$mapper = new Hotels_Model_HotelsMapper ();
		$return = $mapper->save ( $this );
		return $return;
	}
	public function saveDynamicForm($option, $dynamicForm, $result, $request) {
		$translator = Zend_Registry::get ( 'translator' );
		$field_db = new Members_Model_DbTable_Fields ();
		$field_groups = $field_db->getGroupNames ( $option ['form_id'] );
		foreach ( $field_groups as $group ) {
			$group_name = $group->field_group;
			$displaGroup = $dynamicForm->getDisplayGroup ( $group_name );
			$elementsObj = $displaGroup->getElements ();

			foreach ( $elementsObj as $element ) {
				if (substr ( $element->getName (), - 5 ) != '_prev') {
					$table_id = $result ['id'];
					$form_id = $option ['form_id'];
					$field_id = $element->getAttrib ( 'rel' );
					$field_value = ($element->getType () == 'Zend_Form_Element_File') ? $request->getPost ( $element->getName () ) : $element->getValue ();
					if ($element->getType () == 'Zend_Form_Element_MultiCheckbox') {
						if (is_array ( $field_value )) {
							$field_value = '';
						}
					}
					if ($element->getType () == 'Zend_Form_Element_Multiselect') {
						$field_value = $request->getPost ( $element->getName () );
						if (is_array ( $field_value )) {
							$field_value = implode ( ',', $field_value );
						}
					}
					if (($element->getType () == 'Zend_Form_Element_File' && ! empty ( $field_value )) || ($element->getType () != 'Zend_Form_Element_File')) {
						try {
							$DBconn = Zend_Registry::get ( 'msqli_connection' );
							$DBconn->getConnection ();

							// Remove from Value
							$where = array ();
							$where [0] = 'table_id = ' . $DBconn->quote ( $table_id );
							$where [1] = 'form_id = ' . $DBconn->quote ( $form_id );
							$where [2] = 'field_id = ' . $DBconn->quote ( $field_id );
							$DBconn->delete ( Zend_Registry::get ( 'dbPrefix' ) . 'forms_fields_values', $where );

							// Add Value
							$data = array (
									'table_id' => $table_id,
									'form_id' => $form_id,
									'field_id' => $field_id,
									'field_value' => $field_value
							);
							$DBconn->insert ( Zend_Registry::get ( 'dbPrefix' ) . 'forms_fields_values', $data );

							$msg = $translator->translator ( "page_save_success" );
							$json_arr = array (
									'status' => 'ok',
									'msg' => $msg
							);
						} catch ( Exception $e ) {
							$msg = $translator->translator ( "page_save_err" );
							$json_arr = array (
									'status' => 'err',
									'msg' => $msg . ' ' . $e->getMessage ()
							);
						}
					} else {
						$msg = $translator->translator ( "page_save_success" );
						$json_arr = array (
								'status' => 'ok',
								'msg' => $msg
						);
					}
				} else {
					$msg = $translator->translator ( "page_save_success" );
					$json_arr = array (
							'status' => 'ok',
							'msg' => $msg
					);
				}
			}
		}
		return $json_arr;
	}
	public function setId($text) {
		$this->_id = $text;
		return $this;
	}
	public function setGroup_id($text) {
		$this->_group_id = $text;
		return $this;
	}
	public function setCategory_id($text) {
		$this->_category_id = $text;
		return $this;
	}
	public function setRoom_type_id($text) {
		if (is_array ( $text )) {
			$room_type = implode ( ',', $text );
		} else {
			$room_type = $text;
		}
		$this->_room_type_id = $room_type;
		return $this;
	}
	public function setHotels_name($text) {
		$this->_hotels_name = $text;
		return $this;
	}
	public function setHotels_title($text, $id = null) {
		$pattern = Eicra_File_Constants::TITLE_PATTERN;
		$replacement = Eicra_File_Constants::TITLE_REPLACEMENT;
		$text = preg_replace ( $pattern, $replacement, trim ( $text ) );
		// DB Connection
		$conn = Zend_Registry::get ( 'msqli_connection' );
		$conn->getConnection ();

		if (empty ( $id )) {
			$select = $conn->select ()->from ( array (
					'm' => Zend_Registry::get ( 'dbPrefix' ) . 'hotels_page'
			), array (
					'm.id'
			) )->where ( 'm.hotels_title = ?', $text );
		} else {
			$select = $conn->select ()->from ( array (
					'm' => Zend_Registry::get ( 'dbPrefix' ) . 'hotels_page'
			), array (
					'm.id'
			) )->where ( 'm.hotels_title = ?', $text )->where ( 'm.id != ?', $id );
		}
		$rs = $select->query ()->fetchAll ();
		if ($rs) {
			foreach ( $rs as $row ) {
				$ids = ( int ) $row ['id'];
			}
		}
		if (empty ( $ids )) {
			$this->_hotels_title = $text;
		} else {
			$select = $conn->select ()->from ( array (
					'm' => Zend_Registry::get ( 'dbPrefix' ) . 'hotels_page'
			), array (
					'm.id'
			) )->order ( array (
					'm.id DESC'
			) )->limit ( 1 );
			$rs = $select->query ()->fetchAll ();
			if ($rs) {
				foreach ( $rs as $row ) {
					$last_id = ( int ) $row ['id'];
				}
			}
			if (empty ( $id )) {
				$this->_hotels_title = $text . '-' . ($last_id + 1);
			} else {
				$this->_hotels_title = $text . '-' . $id;
			}
		}
		return $this;
	}
	public function setHotels_agent($text) {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$globalIdentity = $auth->getIdentity ();
			$this->_hotels_agent = ($globalIdentity->allow_to_change_ownership != '1') ? $globalIdentity->user_id : $text;
		} else {
			$this->_hotels_agent = $text;
		}
		return $this;
	}
	public function setRelated_items($text) {
		if (is_array ( $text )) {
			$related_items = implode ( ',', $text );
		} else {
			$related_items = $text;
		}
		$this->_related_items = $related_items;
		return $this;
	}
	public function setHotels_grade($text) {
		if (empty ( $text )) {
			$this->_hotels_grade = 0;
		} else {
			$this->_hotels_grade = $text;
		}
		return $this;
	}
	public function setEntry_by() {
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$globalIdentity = $auth->getIdentity ();
			$this->_entry_by = $globalIdentity->user_id;
		} else {
			$this->_entry_by = '1';
		}
		return $this;
	}
	public function setHotels_primary_image($text) {
		$this->_hotels_primary_image = $text;
		return $this;
	}
	public function setHotels_image($text) {
		$this->_hotels_image = $text;
		return $this;
	}
	public function setHotels_type($text) {
		$this->_hotels_type = $text;
		return $this;
	}
	public function setPrev_category($text) {
		$this->_prev_category = $text;
		return $this;
	}
	public function setPrev_group($text) {
		$this->_prev_group = $text;
		return $this;
	}
	public function setCheckin_hour($text) {
		$this->_checkin_hour = $text;
		return $this;
	}
	public function setCheckin_min($text) {
		$this->_checkin_min = $text;
		return $this;
	}
	public function setCheckin_time($text) {
		$this->_checkin_time = $text;
		return $this;
	}
	public function setCheckout_hour($text) {
		$this->_checkout_hour = $text;
		return $this;
	}
	public function setCheckout_min($text) {
		$this->_checkout_min = $text;
		return $this;
	}
	public function setCheckout_time($text) {
		$this->_checkout_time = $text;
		return $this;
	}
	public function setHotels_order($text) {
		if (! empty ( $text )) {
			$this->_hotels_order = $text;
		} else {
			$group_id = $this->getGroup_id ();
			$category_id = $this->getCategory_id ();
			if (empty ( $this->_id )) {
				$OrderObj = new Hotels_Controller_Helper_HotelsOrders ();
			} else {
				$OrderObj = new Hotels_Controller_Helper_HotelsOrders ( $this->_id );
			}
			$OrderObj->setHeighestOrder ( $group_id, $category_id );
			$last_order = $OrderObj->getHighOrder ();

			$this->_hotels_order = $last_order + 1;
		}
		return $this;
	}
	public function setHotels_desc($text) {
		$this->_hotels_desc = $text;
		return $this;
	}
	public function setFeatured($text) {
		$this->_featured = $text;
		return $this;
	}
	public function setActive($text) {
		$this->_active = $text;
		return $this;
	}
	public function setMeta_title($text) {
		$this->_meta_title = $text;
		return $this;
	}
	public function setMeta_keywords($text) {
		$this->_meta_keywords = $text;
		return $this;
	}
	public function setMeta_desc($text) {
		$this->_meta_desc = $text;
		return $this;
	}
	public function setCountry_id($text) {
		$this->_country_id = ($text) ? $text : '0';
		return $this;
	}
	public function setState_id($text) {
		$this->_state_id = ($text) ? $text : '0';
		return $this;
	}
	public function setArea_id($text) {
		$this->_area_id = ($text) ? $text : '0';
		return $this;
	}
	public function setLocation_for_search($text) {
		$this->_location_for_search = ($text) ? $text : '';
		return $this;
	}
	public function setHotels_building_no($text) {
		$this->_hotels_building_no = $text;
		return $this;
	}
	public function setHotels_building_name($text) {
		$this->_hotels_building_name = $text;
		return $this;
	}
	public function setHotels_phone($text) {
		$this->_hotels_phone = $text;
		return $this;
	}
	public function setHotels_fax($text) {
		$this->_hotels_fax = $text;
		return $this;
	}
	public function setHotels_email($text) {
		$this->_hotels_email = $text;
		return $this;
	}
	public function setPost_code($text) {
		$this->_post_code = str_replace ( " ", "", $text );
		return $this;
	}
	public function setHotels_address($text) {
		$this->_hotels_address = $text;
		return $this;
	}
	public function setFeature_distance_from_airport($text) {
		$this->_feature_distance_from_airport = ($text) ? $text : '0';
		return $this;
	}
	public function setFeature_distance_from_city_market($text) {
		$this->_feature_distance_from_city_market = ($text) ? $text : '0';
		return $this;
	}
	public function setFeature_pet($text) {
		$this->_feature_pet = $text;
		return $this;
	}
	public function setFeature_facilities($text) {
		if (is_array ( $text )) {
			$feature_facilities = implode ( ',', $text );
		} else {
			$feature_facilities = $text;
		}
		$this->_feature_facilities = $feature_facilities;
		return $this;
	}
	public function setFeature_sports_recreations($text) {
		if (is_array ( $text )) {
			$feature_sports_recreations = implode ( ',', $text );
		} else {
			$feature_sports_recreations = $text;
		}
		$this->_feature_sports_recreations = $feature_sports_recreations;
		return $this;
	}
	public function setHotels_status($text) {
		$this->_hotels_status = $text;
		return $this;
	}
	public function setHotels_about_information($text) {
		$this->_hotels_about_information = $text;
		return $this;
	}
	public function setHotels_about_general($text) {
		$this->_hotels_about_general = $text;
		return $this;
	}
	public function setHotels_about_services($text) {
		$this->_hotels_about_services = $text;
		return $this;
	}
	public function setHotels_about_internet($text) {
		$this->_hotels_about_internet = $text;
		return $this;
	}
	public function setHotels_about_parking($text) {
		$this->_hotels_about_parking = $text;
		return $this;
	}
	public function setHotels_about_other($text) {
		$this->_hotels_about_other = $text;
		return $this;
	}
	public function setHotels_policy_policies($text) {
		$this->_hotels_policy_policies = addslashes ( $text );
		return $this;
	}
	public function setFeature_primary_interior_image($text) {
		$this->_feature_primary_interior_image = $text;
		return $this;
	}
	public function setFeature_interior_plan_image($text) {
		$this->_feature_interior_plan_image = $text;
		return $this;
	}
	public function setFeature_google_map($text) {
		$this->_feature_google_map = ($text) ? addslashes ( $text ) : null;
		return $this;
	}
	public function setFeature_additional($text) {
		$this->_feature_additional = $text;
		return $this;
	}
	public function setBrochure_title($text) {
		$this->_brochure_title = addslashes ( $text );
		return $this;
	}
	public function setBrochure_desc($text) {
		$this->_brochure_desc = $text;
		return $this;
	}
	public function setCheckin_date($text) {
		$this->_checkin_date = ($text) ? $text : '00:00';
		return $this;
	}
	public function setCheckout_date($text) {
		$this->_checkout_date = ($text) ? $text : '00:00';
		return $this;
	}
	public function setDay_night_limit($text) {
		$this->_day_night_limit = ($text) ? $text : '0';
		return $this;
	}
	public function setHotels_policy_terms_condition($text) {
		$this->_hotels_policy_terms_condition = ($text) ? $text : '';
		return $this;
	}
	public function setHotels_policy_privacy($text) {
		$this->_hotels_policy_privacy = ($text) ? $text : '';
		return $this;
	}
	public function setHotels_policy_surroundings($text) {
		$this->_hotels_policy_surroundings = $text;
		return $this;
	}
	public function setPayment_options($text) {
		$this->_payment_options = $text;
		return $this;
	}
	public function setPayment_desc($text) {
		$this->_payment_desc = $text;
		return $this;
	}
	public function setBilling_user_places_order_email_enable($text) {
		$this->_billing_user_places_order_email_enable = $text;
		return $this;
	}
	public function setBilling_user_places_order_email_address($text) {
		$this->_billing_user_places_order_email_address = $text;
		return $this;
	}
	public function setBilling_user_pay_invoice_email_enable($text) {
		$this->_billing_user_pay_invoice_email_enable = $text;
		return $this;
	}
	public function setBilling_user_pay_invoice_email_address($text) {
		$this->_billing_user_pay_invoice_email_address = $text;
		return $this;
	}
	public function setBilling_user_cancel_order_email_enable($text) {
		$this->_billing_user_cancel_order_email_enable = $text;
		return $this;
	}
	public function setBilling_user_cancel_order_email_address($text) {
		$this->_billing_user_cancel_order_email_address = $text;
		return $this;
	}
	public function setBilling_order_payment_email_enable($text) {
		$this->_billing_order_payment_email_enable = $text;
		return $this;
	}
	public function setBilling_item_desc($text) {
		$this->_billing_item_desc = $text;
		return $this;
	}
	public function getId() {
		return $this->_id;
	}
	public function getGroup_id() {
		return $this->_group_id;
	}
	public function getCategory_id() {
		return $this->_category_id;
	}
	public function getRoom_type_id() {
		return $this->_room_type_id;
	}
	public function getHotels_name() {
		return $this->_hotels_name;
	}
	public function getHotels_title() {
		return $this->_hotels_title;
	}
	public function getHotels_agent() {
		return $this->_hotels_agent;
	}
	public function getHotels_grade() {
		return $this->_hotels_grade;
	}
	public function getRelated_items() {
		return $this->_related_items;
	}
	public function getEntry_by() {
		if (empty ( $this->_entry_by )) {
			$this->setEntry_by ();
		}
		return $this->_entry_by;
	}
	public function getHotels_primary_image() {
		return $this->_hotels_primary_image;
	}
	public function getHotels_image() {
		return $this->_hotels_image;
	}
	public function getHotels_type() {
		return $this->_hotels_type = ($this->_hotels_type) ? $this->_hotels_type : '0';
	}
	public function getPrev_category() {
		return $this->_prev_category;
	}
	public function getPrev_group() {
		return $this->_prev_group;
	}
	public function getHotels_order() {
		if (empty ( $this->_hotels_order )) {
			$this->setHotels_order ( '' );
		}
		return $this->_hotels_order;
	}
	public function getHotels_desc() {
		return $this->_hotels_desc;
	}
	public function getFeatured() {
		return $this->_featured;
	}
	public function getActive() {
		return $this->_active;
	}
	public function getMeta_title() {
		return $this->_meta_title;
	}
	public function getMeta_keywords() {
		return $this->_meta_keywords;
	}
	public function getMeta_desc() {
		return $this->_meta_desc;
	}
	public function getCountry_id() {
		return $this->_country_id = ($this->_country_id) ? $this->_country_id : '0';
	}
	public function getState_id() {
		return $this->_state_id = ($this->_state_id) ? $this->_state_id : '0';
	}
	public function getArea_id() {
		return $this->_area_id = ($this->_area_id) ? $this->_area_id : '0';
	}
	public function getLocation_for_search() {
		return $this->_location_for_search;
	}
	public function getHotels_building_no() {
		return $this->_hotels_building_no;
	}
	public function getHotels_building_name() {
		return $this->_hotels_building_name;
	}
	public function getHotels_phone() {
		return $this->_hotels_phone;
	}
	public function getHotels_fax() {
		return $this->_hotels_fax;
	}
	public function getHotels_email() {
		return $this->_hotels_email;
	}
	public function getPost_code() {
		return $this->_post_code;
	}
	public function getHotels_address() {
		return $this->_hotels_address;
	}
	public function getFeature_distance_from_airport() {
		return $this->_feature_distance_from_airport = ($this->_feature_distance_from_airport) ? $this->_feature_distance_from_airport : '0.00';
	}
	public function getFeature_distance_from_city_market() {
		return $this->_feature_distance_from_city_market = ($this->_feature_distance_from_city_market) ? $this->_feature_distance_from_city_market : '0.00';
	}
	public function getFeature_pet() {
		return $this->_feature_pet;
	}
	public function getFeature_facilities() {
		return $this->_feature_facilities;
	}
	public function getFeature_sports_recreations() {
		return $this->_feature_sports_recreations;
	}
	public function getHotels_status() {
		return $this->_hotels_status;
	}
	public function getHotels_about_information() {
		return $this->_hotels_about_information;
	}
	public function getHotels_about_general() {
		return $this->_hotels_about_general;
	}
	public function getHotels_about_services() {
		return $this->_hotels_about_services;
	}
	public function getHotels_about_internet() {
		return $this->_hotels_about_internet;
	}
	public function getHotels_about_parking() {
		return $this->_hotels_about_parking;
	}
	public function getHotels_about_other() {
		return $this->_hotels_about_other;
	}
	public function getHotels_policy_policies() {
		return $this->_hotels_policy_policies;
	}
	public function getFeature_primary_interior_image() {
		return $this->_feature_primary_interior_image;
	}
	public function getFeature_interior_plan_image() {
		return $this->_feature_interior_plan_image;
	}
	public function getFeature_google_map() {
		return $this->_feature_google_map;
	}
	public function getFeature_additional() {
		return $this->_feature_additional;
	}
	public function getBrochure_title() {
		return $this->_brochure_title;
	}
	public function getBrochure_desc() {
		return $this->_brochure_desc;
	}
	public function getDay_night_limit() {
		return ($this->_day_night_limit) ? $this->_day_night_limit : 0;
	}
	public function getCheckin_date() {
		return $this->_checkin_date = ($this->_checkin_date) ? $this->_checkin_date : $this->_checkin_hour . ':' . $this->_checkin_min . ':00';
	}
	public function getCheckin_time() {
		return $this->_checkin_time = ($this->_checkin_time) ? $this->_checkin_time : 'AM';
	}
	public function getCheckout_date() {
		return $this->_checkout_date = ($this->_checkout_date) ? $this->_checkout_date : $this->_checkout_hour . ':' . $this->_checkout_min . ':00';
	}
	public function getCheckout_time() {
		return $this->_checkout_time = ($this->_checkout_time) ? $this->_checkout_time : 'AM';
	}
	public function getHotels_policy_terms_condition() {
		return $this->_hotels_policy_terms_condition = ($this->_hotels_policy_terms_condition) ? $this->_hotels_policy_terms_condition : '';
	}
	public function getHotels_policy_privacy() {
		return $this->_hotels_policy_privacy = ($this->_hotels_policy_privacy) ? $this->_hotels_policy_privacy : '';
	}
	public function getHotels_policy_surroundings() {
		return $this->_hotels_policy_surroundings;
	}
	public function getPayment_options() {
		return $this->_payment_options;
	}
	public function getPayment_desc() {
		return $this->_payment_desc;
	}
	public function getBilling_user_places_order_email_enable() {
		return $this->_billing_user_places_order_email_enable;
	}
	public function getBilling_user_places_order_email_address() {
		return $this->_billing_user_places_order_email_address;
	}
	public function getBilling_user_pay_invoice_email_enable() {
		return $this->_billing_user_pay_invoice_email_enable;
	}
	public function getBilling_user_pay_invoice_email_address() {
		return $this->_billing_user_pay_invoice_email_address;
	}
	public function getBilling_user_cancel_order_email_enable() {
		return $this->_billing_user_cancel_order_email_enable;
	}
	public function getBilling_user_cancel_order_email_address() {
		return $this->_billing_user_cancel_order_email_address;
	}
	public function getBilling_order_payment_email_enable() {
		return $this->_billing_order_payment_email_enable;
	}
	public function getBilling_item_desc() {
		return $this->_billing_item_desc;
	}
	public function getCancellation_policy() {
		return $this->_cancellation_policy;
	}
	public function setCancellation_policy($_cancellation_policy) {
		$this->_cancellation_policy = $_cancellation_policy;
	}
}
?>