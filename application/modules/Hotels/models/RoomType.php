<?php
class Hotels_Model_RoomType
{
	protected $_id;
	protected $_entry_by;
	protected $_room_type;
	protected $_primary_image;
	protected $_room_type_image;
	protected $_max_people;
	protected $_price_plan_id;
	protected $_basic_price;
	protected $_descount_price;
	protected $_total_room_no;
	protected $_room_left;
	protected $_room_condition;
	protected $_room_desc;
	protected $_room_available_information;
	protected $_price_setting_type;
		
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveRoomType()
		{
			$mapper  = new Hotels_Model_RoomTypeMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setRoom_type($text)
		{
			$this->_room_type = $text;
			return $this;
		}
		
		public function setPrimary_image($text)
		{
			$this->_primary_image = $text;
			return $this;
		}
		
		public function setRoom_type_image($text)
		{
			$this->_room_type_image = $text;
			return $this;
		}
		
		public function setMax_people($text)
		{
			$this->_max_people = $text;
			return $this;
		}
		
		public function setPrice_setting_type($text)
		{
			$this->_price_setting_type = $text;
			return $this;
		}
		
		public function setPrice_plan_id($text)
		{
			$this->_price_plan_id = $text;
			return $this;
		}
		
		public function setBasic_price($text)
		{
			$this->_basic_price = $text;
			return $this;
		}
		
		public function setDescount_price($text)
		{
			$this->_descount_price = $text;
			return $this;
		}
		
		public function setTotal_room_no($text)
		{
			$this->_total_room_no = $text;
			return $this;
		}
		
		public function setRoom_left($text)
		{
			$this->_room_left = $text;
			return $this;
		}
		
		public function setRoom_condition($text)
		{
			$this->_room_condition = $text;
			return $this;
		}
		
		public function setRoom_desc($text)
		{
			$this->_room_desc = $text;
			return $this;
		}
		
		public function setRoom_available_information($id, $price)
		{
			if(!empty($id))
			{
				$roomType_db = new Hotels_Model_DbTable_RoomType();
				$roomType_info = $roomType_db->getRoomTypeInfo($id);
				if(!empty($roomType_info['room_available_information']))
				{
					$available_information_arr = explode(',',$roomType_info['room_available_information']);
					$price_arr = explode(';;',$available_information_arr[0]);
					$price_prev = $price_arr[2];
					$this->_room_available_information = str_replace($price_prev, $price, $roomType_info['room_available_information']);
				}
				else
				{
					$this->_room_available_information = '';
				}
			}
			else
			{
				$this->_room_available_information = '';
			}
			return $this;
		}
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getRoom_type()
		{
			return $this->_room_type;
		}
		
		public function getPrice_setting_type()
		{
			return $this->_price_setting_type;
		}
		
		public function getPrimary_image()
		{
			return $this->_primary_image;
		}
		
		public function getRoom_type_image()
		{
			return $this->_room_type_image;
		}
		
		public function getMax_people()
		{
			return $this->_max_people;
		}
		
		public function getPrice_plan_id()
		{
			return $this->_price_plan_id;
		}
		
		public function getBasic_price()
		{
			return $this->_basic_price;
		}
		
		public function getDescount_price()
		{
			return $this->_descount_price;
		}
		
		public function getTotal_room_no()
		{
			return $this->_total_room_no;
		}
		
		public function getRoom_left()
		{
			return $this->_room_left;
		}
		
		public function getRoom_condition()
		{
			return $this->_room_condition;
		}
		
		public function getRoom_desc()
		{
			return $this->_room_desc;
		}	
		
		public function getRoom_available_information()
		{
			return $this->_room_available_information;
		}		
}
?>