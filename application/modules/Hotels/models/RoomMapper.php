<?php
class Hotels_Model_RoomMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Room');
        }
        return $this->_dbTable;
    }
	
	public function save(Hotels_Model_Room $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{				
				try 
				{
					unset($data['id']);
				
					$data = array(					
						'entry_by' 					=> 	$obj->getEntry_by(),
						'room_type_id' 				=>	$obj->getRoom_type_id(),	
						'hotel_id' 					=>	$obj->getHotel_id(),					
						'room_name' 				=>	$obj->getRoom_name(),
						'room_condition' 			=>	$obj->getRoom_condition(),
						'room_desc' 				=>	$obj->getRoom_desc()										
					);
					
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{					
				try 
				{
					$data = array(		
						'room_type_id' 				=>	$obj->getRoom_type_id(),	
						'hotel_id' 					=>	$obj->getHotel_id(),						
						'room_name' 				=>	$obj->getRoom_name(),
						'room_condition' 			=>	$obj->getRoom_condition(),
						'room_desc' 				=>	$obj->getRoom_desc()						
					);				
					// Start the Update process
					
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>