<?php
/**
* This is the DbTable class for the guestbook table.
*/
class Hotels_Model_DbTable_Hotels extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_page';		
	protected $_cols	=	null;
	
		
	//Get Datas
	public function getHotelsInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }
	
	
	public function getOptions()
    {
       
		$select = $this->select()
				   ->from($this->_name, array('id', 'hotels_name'))
				   ->order('hotels_name ASC');	 
        $options = $this->getAdapter()->fetchPairs($select);
		if($options) 
		{
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);            
        }
		else
		{
			//throw new Exception("Count not find rows $id"); 
			$options = null;
		}
        return $options;
    }	
	
	//Get Datas
	public function getTitleToId($hotels_title) 
    {
		$hotels_title = addslashes($hotels_title);
		try
		{
			$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.hotels_title =? ',$hotels_title)
							   ->limit(1); 			
			$options = $this->fetchAll($select);
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	
	
	//Get Datas By Region
	public function getHotelsMatchRegion($current_id,$state_id,$limit) 
    {		
		try
		{
			$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.id != ?',$current_id)
							   ->where('gp.state_id = ?',$state_id)
							   ->where('gp.active = ?','1')
							   ->limit($limit); 			
			$options = $this->fetchAll($select);
			if($options)
			{
				return $options; 
			}
			else
			{
				return null;
			}
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	//Get Datas By Agent
	public function getHotelsMatchAgent($current_id,$hotels_agent,$limit) 
    {		
		try
		{
			$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.id != ?',$current_id)
							   ->where('gp.hotels_agent = ?',$hotels_agent)
							   ->where('gp.active = ?','1')
							   ->limit($limit); 			
			$options = $this->fetchAll($select);
			if($options)
			{
				return $options; 
			}
			else
			{
				return null;
			}
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	//Get Next Hotels
	public function getNextHotels($hotels_id,$group_id,$type_id = null) 
    {		
		try
		{
			$select = ($type_id)?
							$this->select()
								   ->from(array('gp' => $this->_name),array('*'))
								   ->where('gp.id >? ',$hotels_id)
								   ->where('gp.hotels_type =? ',$type_id)
								   ->where('gp.group_id =? ',$group_id)
								   ->order('gp.id ASC')
								   ->limit(1)
							:
							$this->select()
								   ->from(array('gp' => $this->_name),array('*'))
								   ->where('gp.id > ? ',$hotels_id)
								   ->where('gp.group_id =? ',$group_id)
								   ->order('gp.id ASC')
								   ->limit(1); 
		
			$row = $this->getAdapter()->fetchAll($select);
			if($row)
			{
				$options = $row[0];
			}
			else
			{
				$options = '';
			}
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	//Get Previous Hotels
	public function getPrevHotels($hotels_id,$group_id,$type_id = null) 
    {		
		try
		{
			$select = ($type_id)?
							$this->select()
								   ->from(array('gp' => $this->_name),array('*'))
								   ->where('gp.id <? ',$hotels_id)
								   ->where('gp.hotels_type =? ',$type_id)
								   ->where('gp.group_id =? ',$group_id)
								   ->order('gp.id DESC')
								   ->limit(1)
							:
							$this->select()
								   ->from(array('gp' => $this->_name),array('*'))
								   ->where('gp.id <? ',$hotels_id)
								   ->where('gp.group_id =? ',$group_id)
								   ->order('gp.id DESC')
								   ->limit(1); 
			$row = $this->getAdapter()->fetchAll($select);			
			if($row)
			{
				$options = $row[0];
			}
			else
			{
				$options = '';
			}
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	//Get All Datas
	public function getHotelsInfoByGroup($group_id = null,$approve = null) 
    {	
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$approve = ($approve == null) ? '1=1' : 'gp.active = "'.$approve.'"';
		
		if(empty($group_id))
		{			
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.entry_by = ?',$user_id)
							   ->orwhere('gp.hotels_agent = ? ',$user_id)
						   	   ->where($approve)
							   ->order('gp.group_id ASC')
							   ->order("gp.category_id ASC")                       
							   ->order('gp.hotels_order ASC'); 	
			}
			else
			{
				$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
						   	   ->where($approve)
							   ->order('gp.group_id ASC')
							   ->order("gp.category_id ASC")                       
							   ->order('gp.hotels_order ASC'); 
			}	
		}
		else
		{
			if($user_id && $auth->getIdentity()->access_other_user_article != '1')
			{
				$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.entry_by = ?',$user_id)
							   ->orwhere('gp.hotels_agent = ? ',$user_id)
							   ->where('gp.group_id =?',$group_id)
						   	   ->where($approve)
							   ->order("gp.category_id ASC")                       
							   ->order('gp.hotels_order ASC'); 	
			}
			else
			{
				$select = $this->select()
							   ->from(array('gp' => $this->_name),array('*'))
							   ->where('gp.group_id =?',$group_id)
						   	   ->where($approve)
							   ->order("gp.category_id ASC")                       
							   ->order('gp.hotels_order ASC'); 
			}
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	public function getBorderDistance($field = 'feature_distance_from_airport', $range = 'DESC', $active = '1') 
    {		
		try
		{
			$select = $this->select()
							   ->from(array('hp' => $this->_name),array($field => 'hp.'.$field))
							   ->where('hp.active =? ', $active)
							   ->order('hp.'.$field.' '.$range)
							   ->limit(1); 			
			$options = $this->fetchAll($select);
			if($options)
			{
				return $options[0][$field]; 
			}
			else
			{
				return '0';
			}
		}
		catch(Exception $e)
		{
			return '0';
		}		 		  
    }
	
	public function numOfHotels($condition = null, $userChecking = true)
	{
		$auth = Zend_Auth::getInstance ();
		
		$select = $this->select()
					   ->from(array('p' => $this->_name),array('COUNT(*) AS num_hotels'));
		if($auth->hasIdentity () && ($userChecking == true ))
		{					
			$identity = $auth->getIdentity ();
			$user_id = $identity->user_id;
			$select->where('( p.entry_by = '.$user_id.' OR p.hotels_agent = '.$user_id.' )');
		}
		
		if($condition && is_array($condition))
		{
			$select->where('p.'.$condition['field'].' '.$condition['operator'].' '.$condition['value']);			
		}
					   //->orwhere('p.hotels_agent =?',$user_id); 
		 $options = $this->fetchAll($select);			
		 return ($options)? $options[0]['num_hotels'] : 0;	
	}
	
	public function getHotelsByGroup($group_id = null, $active = '1') 
    {
		if(empty($group_id))
		{
			$select = $this->select()
                       ->from(array('gp' => $this->_name),array('*'))
					   ->where('gp.active =?',$active); 	
		}
		else
		{
       		$select = $this->select()
                       ->from(array('gp' => $this->_name),array('*'))
					   ->where('gp.group_id =?',$group_id)
					   ->where('gp.active =?',$active); 	
		}	
		
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            $options = null;
        }	
        return $options;    
    }
	
	public function getNumInActiveHotels($group_id = null) 
    {
		if(empty($group_id))
		{
			$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.active =?','0'); 	
		}
		else
		{
       		$select = $this->select()
                       ->from(array('gp' => $this->_name),array('COUNT(gp.id) as num'))
					   ->where('gp.group_id =?',$group_id)
					   ->where('gp.active =?','0'); 	
		}	
		
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            $num_of_news = 0;
        }
		else
		{
			foreach($options as $row)
			{
				$num_of_news = $row['num'];
			}
		}	
        return $num_of_news;    
    }	
	
	//Get Related Items
	public function getRelatedItems() 
    {
       $select = $this->select()
                       ->from($this->_name, array('id', 'hotels_name'))
					   ->where('active = ?','1')
                       ->order('hotels_name ASC'); 
		 $options = $this->getAdapter()->fetchPairs($select);
		if ($options) 
		{
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);            
        }
		else
		{
			//throw new Exception("Count not find rows $id"); 
			$options = null;
		}		
        return $options;   
    }
	
	//Get DistanceFromAirport
	public function getDistanceFromAirport($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_distance_from_airport' => 'DISTINCT(feature_distance_from_airport)'))
                       ->order('feature_distance_from_airport '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Fuel Type
	public function getFuelType($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_fuel_type' => 'DISTINCT(feature_fuel_type)'))
                       ->order('feature_fuel_type '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Exterior Color
	public function getExteriorColor($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_exterior_color' => 'DISTINCT(feature_exterior_color)'))
                       ->order('feature_exterior_color '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Doors
	public function getDoors($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_doors' => 'DISTINCT(feature_doors)'))
                       ->order('feature_doors '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Doors
	public function getBodyStyle($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_bodystyle' => 'DISTINCT(feature_bodystyle)'))
                       ->order('feature_bodystyle '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Doors
	public function getDriveType($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_drive_type' => 'DISTINCT(feature_drive_type)'))
                       ->order('feature_drive_type '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Engine
	public function getEngineSize($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_engine' => 'DISTINCT(feature_engine)'))
                       ->order('feature_engine '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Transmission
	public function getTransmission($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('feature_transmission' => 'DISTINCT(feature_transmission)'))
                       ->order('feature_transmission '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Price
	public function getPrice($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('hotels_price' => 'DISTINCT(hotels_price)'))
                       ->order('hotels_price '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Get Models For Frontend
	public function getFrontendModels($order = 'ASC') 
    {
       $select = $this->select()
                       ->from($this->_name, array('*'))
					   ->where('active =?','1')
                       ->order('hotels_name '.$order); 
		 $options = $this->getAdapter()->fetchAll($select);
		if (!$options) 
		{
           $options = null;
        }
        return $options;   
    }
	
	//Link Room Type to Hotels
	public function linkToHotels($room_type_id, $hotels_id_arr) 
    {
		try
		{
			if(!empty($room_type_id) && !empty($hotels_id_arr[0]))
			{				
				$hotels_info = $this->fetchAll();
				if($hotels_info)
				{
					foreach($hotels_info as $key => $hotels)
					{
						if(in_array($hotels->id, $hotels_id_arr))
						{
							$select = $this->select()
								   ->from($this->_name, array('room_type_id' => 'room_type_id'))
								   ->where('id =?',$hotels->id);
							$options = $this->fetchAll($select);
							if($options)
							{
								$room_type_id_arr = explode(',', $options[0]['room_type_id']);
								if(!in_array($room_type_id, $room_type_id_arr))
								{
										array_push($room_type_id_arr, $room_type_id);
										$this->update(array('room_type_id' => implode(',', $room_type_id_arr)), array('id = ?' => $hotels->id));								
								}						
							}
						}
						else
						{
							$select = $this->select()
								   ->from($this->_name, array('room_type_id' => 'room_type_id'))
								   ->where('id =?',$hotels->id);
							$options = $this->fetchAll($select);
							if($options)
							{
								$room_type_id_arr = explode(',', $options[0]['room_type_id']);
								if(in_array($room_type_id, $room_type_id_arr))
								{
										foreach($room_type_id_arr as $room_type_id_key => $room_type_id_value )
										{
											if($room_type_id_value == $room_type_id )
											{
												unset($room_type_id_arr[$room_type_id_key]);
											}
										}
										$this->update(array('room_type_id' => implode(',', $room_type_id_arr)), array('id = ?' => $hotels->id));								
								}						
							}
						}
					}
				}
				$json_arr = array('status' => 'ok');
			}
			else
			{
				$json_arr = array('status' => 'err');
			}
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}
		return $json_arr;
	}	
	
	//Link Hotels To Room Type
	public function linkToRoomType($hotels_id, $room_type_id) 
    {
		try
		{
			if(!empty($hotels_id) && !empty($room_type_id))
			{				
				$this->update(array('room_type_id' => $room_type_id), array('id = ?' => $hotels_id));
				$json_arr = array('status' => 'ok');
			}
			else
			{
				$json_arr = array('status' => 'err');
			}
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}
		return $json_arr;
	}
	
	public function limitValidator($total) 
    {
		
			$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'id',
						'exclude' => '(day_night_limit >= '.$total.' OR day_night_limit = 0)',
					)
				);
		
		return $validator;
	}
	
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;	
		
		$hotels_page_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_page'] && is_array($tableColumns['hotels_page'])) ? $tableColumns['hotels_page'] : array('hp.*');
        $hotels_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_business_type'] && is_array($tableColumns['hotels_business_type'])) ? $tableColumns['hotels_business_type'] : array( 'business_type' => 'hbt.business_type');
        $hotels_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_category'] && is_array($tableColumns['hotels_category'])) ? $tableColumns['hotels_category'] : array( 'category_name' => 'hc.category_name');
        $cities_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['cities'] && is_array($tableColumns['cities'])) ? $tableColumns['cities'] : array( 'city' => 'ct.city');
        $countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array( 'country_name' => 'cnt.value');
        $states_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['states'] && is_array($tableColumns['states'])) ? $tableColumns['states'] : array( 'state_name' => 'st.state_name');
		$vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
		$hotels_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_group'] && is_array($tableColumns['hotels_group'])) ? $tableColumns['hotels_group'] : array( 'group_name' => 'hg.group_name', 'review_id' => 'hg.review_id', 'file_thumb_width' => 'hg.file_thumb_width', 'file_thumb_height' => 'hg.file_thumb_height', 'file_thumb_resize_func' => 'hg.file_thumb_resize_func');
		$user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$owner_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_profile'] && is_array($tableColumns['owner_profile'])) ? $tableColumns['owner_profile'] : array( 'owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) ", 'owner_email' => 'ups.username', 'owner_id' => 'ups.user_id', 'owner_phone' => 'ups.phone', 'owner_postalCode' => 'ups.postalCode', 'owner_address' => 'ups.address', 'country' => 'ups.country');
		$owner_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_country'] && is_array($tableColumns['owner_country'])) ? $tableColumns['owner_country'] : null;
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('hp' => $this->_name), $hotels_page_column_arr);
						   
		if (($hotels_group_column_arr &&  is_array($hotels_group_column_arr) && count( $hotels_group_column_arr) > 0)) 
		{
			$select->joinLeft(array('hg' => Zend_Registry::get('dbPrefix').'hotels_group'), 'hp.group_id = hg.id', $hotels_group_column_arr);
		}
		
		if (($hotels_business_type_arr &&  is_array($hotels_business_type_arr) && count( $hotels_business_type_arr) > 0)) 
		{
			$select->joinLeft(array('hbt' =>  Zend_Registry::get('dbPrefix').'hotels_business_type'), 'hbt.id = hp.hotels_type', $hotels_business_type_arr);
		}
		
		if (($hotels_category_column_arr &&  is_array($hotels_category_column_arr) && count( $hotels_category_column_arr) > 0)) 
		{
			$select->joinLeft(array('hc' =>  Zend_Registry::get('dbPrefix').'hotels_category'), 'hc.id = hp.category_id', $hotels_category_column_arr);
		}
		
		if (($cities_column_arr &&  is_array($cities_column_arr) && count( $cities_column_arr) > 0)) 
		{
			$select->joinLeft(array('ct' =>  Zend_Registry::get('dbPrefix').'cities'), 'ct.city_id = hp.area_id', $cities_column_arr);
		}
		
		if (($states_column_arr &&  is_array($states_column_arr) && count( $states_column_arr) > 0)) 
		{
			$select->joinLeft(array('st' =>  Zend_Registry::get('dbPrefix').'states'), 'st.state_id = hp.state_id', $states_column_arr);
		}
		
		if (($countries_column_arr &&  is_array($countries_column_arr) && count( $countries_column_arr) > 0)) 
		{
			$select->joinLeft(array('cnt' =>  Zend_Registry::get('dbPrefix').'countries'), 'cnt.id = hp.country_id', $countries_column_arr);
		}
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'hp.entry_by = up.user_id', $user_profile_column_arr);				   
		}
		
		if (($owner_profile_column_arr &&  is_array($owner_profile_column_arr) && count( $owner_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('ups' => Zend_Registry::get('dbPrefix').'user_profile'), 'hp.hotels_agent = ups.user_id', $owner_profile_column_arr);
		}
		
		if (($owner_country_column_arr &&  is_array($owner_country_column_arr) && count( $owner_country_column_arr) > 0)) 
		{
			$select->joinLeft(array('ocut' =>  Zend_Registry::get('dbPrefix').'countries'), 'ocut.id = ups.country', $owner_country_column_arr);
		}
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('hp.entry_by = ' . $user_id . ' OR hp.hotels_agent = ' . $user_id);
		}

		if (($vote_column_arr &&  is_array($vote_column_arr) && count( $vote_column_arr) > 0)) 
		{
            $select ->joinLeft(array( 'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting' ), 'hp.id  = vt.table_id', $vote_column_arr);
        }
		
		if($approve != null)
		{
			$select->where("hp.active = ?", $approve);
		}

        if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}
				}
			}
			else
			{
				$select->order("hp.category_id ASC")                       
						->order('hp.hotels_order ASC'); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("hp.category_id ASC")                       
						->order('hp.hotels_order ASC'); 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'hp.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'hotel_id_string' :
					$id_arr = explode(',', $operator_arr['value']);			
					$id_arr = array_filter( $id_arr );				
					$ids = implode(',', $id_arr);
					$operatorFirstPart =  " hp.id IN(".$ids.") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'hotels_grade_arr':
					$hotels_grade_arr = explode(',', $operator_arr['value']);	
					$hotels_grade_arr = array_filter( $hotels_grade_arr );
					$hotels_grade_string = implode(',', $hotels_grade_arr);
					$operatorFirstPart =  " hp.hotels_grade IN(".$hotels_grade_string.") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'hotels_status_arr':
					$hotels_status_arr = explode(',', $operator_arr['value']);	
					$hotels_status_arr = array_filter( $hotels_status_arr );
					foreach($hotels_status_arr as $key => $value)
					{
						$hotels_status_arr[$key] = '"'.$value.'"';
					}
					$hotels_status_string = implode(',', $hotels_status_arr);
					$operatorFirstPart =  " hp.hotels_status IN(".$hotels_status_string.") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'feature_facilities':
					$feature_facilities_arr = explode(',', $operator_arr['value']);	
					$feature_facilities_arr = array_filter( $feature_facilities_arr );
					foreach($feature_facilities_arr as $key => $value)
					{
						$operatorFirstPartArr[$key] = 'hp.feature_facilities LIKE "%'.$value.'%"';
					}
					$operatorFirstPart =  " (".implode(' AND ', $operatorFirstPartArr).") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'feature_sports_recreations':
					$feature_sports_recreations_arr = explode(',', $operator_arr['value']);	
					$feature_sports_recreations_arr = array_filter( $feature_sports_recreations_arr );
					foreach($feature_sports_recreations_arr as $key => $value)
					{
						$operatorFirstPartArr[$key] = 'hp.feature_sports_recreations LIKE "%'.$value.'%"';
					}
					$operatorFirstPart =  " (".implode(' AND ', $operatorFirstPartArr).") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'feature_distance_from_airport-gte' :
					$operator_arr['field'] = 'feature_distance_from_airport';
					$operator_arr['operator'] = 'gte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'feature_distance_from_airport-lte' :
					$operator_arr['field'] = 'feature_distance_from_airport';
					$operator_arr['operator'] = 'lte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;		
			case 'feature_distance_from_city_market-gte' :
					$operator_arr['field'] = 'feature_distance_from_city_market';
					$operator_arr['operator'] = 'gte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'feature_distance_from_city_market-lte' :
					$operator_arr['field'] = 'feature_distance_from_city_market';
					$operator_arr['operator'] = 'lte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'search_price-lte' :
					$operator_arr['value'] = '1';
					$operator_arr['operator'] = 'eq';
					$operatorFirstPart = '1';
				break;	
			case 'search_price-gte' :
					$operator_arr['value'] = '1';
					$operator_arr['operator'] = 'eq';
					$operatorFirstPart = '1';
				break;	
			case 'search_room' :
					$operator_arr['value'] = '1';
					$operator_arr['operator'] = 'eq';
					$operatorFirstPart = '1';
				break;	
			case 'search_adult' :
					$operator_arr['value'] = '1';
					$operator_arr['operator'] = 'eq';
					$operatorFirstPart = '1';
				break;	
			case 'search_child' :
					$operator_arr['value'] = '1';
					$operator_arr['operator'] = 'eq';
					$operatorFirstPart = '1';
				break;
			case Eicra_File_Constants::HOTELS_CHECK_IN :
					$operatorFirstPart = " 1 ";
					$operator_arr['value'] = '1';
				break;
			case Eicra_File_Constants::HOTELS_CHECK_OUT :
					$operatorFirstPart = " 1 ";
					$operator_arr['value'] = '1';
				break;
			case 'groups' :
					$operatorFirstPart = " hp.group_id IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'categories' :
					$operatorFirstPart = " hp.category_id IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'businesstype' :
					$operatorFirstPart = " hp.hotels_type IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'city' :
					$operatorFirstPart = " ct.city ";
				break;
			case 'state_name' :
					$operatorFirstPart = " st.state_name ";
				break;
			case 'country_name' :
					$operatorFirstPart = " cnt.value ";
				break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE hp.id  = vts.table_id) AS total_votes ";
                break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(hp.hotels_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(hp.hotels_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(hp.hotels_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
	
	public function  getHotelMaps ($limit){

		$select = $this->select()
		->setIntegrityCheck(false)
		->from(array('hp' => $this->_name),array('hotels_name', 'hotels_title','hotels_address' ))
		->joinLeft(array('ct' =>  Zend_Registry::get('dbPrefix').'cities'), 'ct.city_id = hp.area_id', array( 'city' => 'ct.city'))
		->joinLeft(array('st' =>  Zend_Registry::get('dbPrefix').'states'), 'st.state_id = hp.state_id', array( 'state_name' => 'st.state_name'))
		->joinLeft(array('cy' =>  Zend_Registry::get('dbPrefix').'countries'), 'cy.id = hp.country_id', array( 'country' => 'cy.value'));

		$select->where("hp.active != ?", '0');

		if ($limit) {
			$select->limit($limit);
		}
		$select->order('rand()');

		$options = $this->fetchAll($select);

		if(!$options)
		{
			$options = null;
		}
		return $options->toArray();

		//$select->where('hp.entry_by = ' . $user_id . ' OR hp.hotels_agent = ' . $user_id);
	}
	
}

?>