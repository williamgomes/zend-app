<?php

/**
 * This is the DbTable class for the hotels_brand table.
 */
class Hotels_Model_DbTable_HotelsproData extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'invoice_hotelspro_data';
    protected $_cols = null;
    
    
    
    public function getListInfo($invoice_id = null) {
        
        $select = $this->select()
                ->from(array('ihdata' => $this->_name), 'ihdata.*');

        if ($invoice_id && $invoice_id != null) {
            $select->where('ihdata.invoice_id = ?', $invoice_id);
        }
        $options = $this->getAdapter()->fetchAll($select);
        file_put_contents('options.txt', $select);
        return $options;
    }

}

?>