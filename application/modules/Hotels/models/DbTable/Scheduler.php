<?php
/**
* This is the DbTable class for the hotels_room_scheduler table.
*/
class Hotels_Model_DbTable_Scheduler extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_room_scheduler';			
	protected $_cols	=	null;	

	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options; 
    }
	
	public function availableValidator($start_date, $end_date, $available_status)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'hotels_id',
						'exclude' => 'start_date <= "'.$start_date.'" AND end_date >= "'.$end_date.'" AND available_status = "'.$available_status.'"',
					)
				);
		return $validator;
	}
	
	public function available($hotels_id, $available_status)
	{
		$select = $this->select()
						   ->from(array('hrs' => $this->_name), array('hrs.*'))
						   ->where("hrs.hotels_id = ?", $hotels_id)
						   ->where("hrs.available_status = ?", $available_status);
		$options = $this->fetchAll($select);
		$date_arr = array();
		if($options)
		{		
			$cnt = 0;	
			foreach($options as $key => $value_arr)
			{
				for($d = strtotime($value_arr['start_date']); $d <= strtotime($value_arr['end_date']); $d += 86400)
				{
					$date_arr[$cnt] = date("Y-m-d", $d);
					$cnt++;
				}
			}
		}
		return $date_arr;
	}
	
	public function updateScheduler($room_id,$data_arr)
	{		
		try
		{
			$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'room_id',
						'exclude' => 'Start <= "'.$data_arr['Start'].'" AND End >= "'.$data_arr['End'].'" ',
					)
				);
			
			$uniqValidator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'room_id',
						'exclude' => 'Start = "'.$data_arr['Start'].'" AND End = "'.$data_arr['End'].'" ',
					)
				);
			if($validator->isValid($room_id))
			{
				if($uniqValidator->isValid($room_id))
				{
					$data = array('book_status'			=>		$data_arr['book_status']);
					$where[] = 'Start = "'.$data_arr['Start'].'" AND End = "'.$data_arr['End'].'" AND room_id = "'.$room_id.'"';
					$this->update($data, $where);
					$result = array('status' => 'ok' );
				}
				else
				{
					$result = array('status' => 'err' );
				}
			}
			else
			{
				$last_id = $this->insert($data_arr);
				$result = array('status' => 'ok' ,'id' => $last_id);
			}				
		}
		catch(Exception $e)
		{
			$result = array('status' => 'ok' ,'id' => $e->getMessage());
		}
		return $result;
	}
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
		
		$hotels_room_scheduler_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_room_scheduler'] && is_array($tableColumns['hotels_room_scheduler'])) ? $tableColumns['hotels_room_scheduler'] : array('hrs.*');
       /* $hotels_page_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_page'] && is_array($tableColumns['hotels_page'])) ? $tableColumns['hotels_page'] : array('hotels_name' => 'vp.hotels_name');
       	$hotels_business_type_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_business_type'] && is_array($tableColumns['hotels_business_type'])) ? $tableColumns['hotels_business_type'] : array( 'business_type' => 'vbt.business_type');
        $hotels_category_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_category'] && is_array($tableColumns['hotels_category'])) ? $tableColumns['hotels_category'] : array( 'category_name' => 'vc.category_name');
        $cities_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['cities'] && is_array($tableColumns['cities'])) ? $tableColumns['cities'] : array( 'city' => 'ct.city');
        $countries_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['countries'] && is_array($tableColumns['countries'])) ? $tableColumns['countries'] : array( 'country_name' => 'cut.value');
        $states_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['states'] && is_array($tableColumns['states'])) ? $tableColumns['states'] : array( 'state_name' => 'st.state_name');
		$vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
		$hotels_group_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_group'] && is_array($tableColumns['hotels_group'])) ? $tableColumns['hotels_group'] : array( 'group_name' => 'tg.hotels_name', 'review_id' => 'tg.review_id', 'file_thumb_width' => 'tg.file_thumb_width', 'file_thumb_height' => 'tg.file_thumb_height', 'file_thumb_resize_func' => 'tg.file_thumb_resize_func');
		$user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$owner_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_profile'] && is_array($tableColumns['owner_profile'])) ? $tableColumns['owner_profile'] : array( 'owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) ", 'owner_email' => 'ups.username', 'owner_id' => 'ups.user_id', 'owner_phone' => 'ups.phone', 'owner_postalCode' => 'ups.postalCode', 'owner_address' => 'ups.address', 'country' => 'ups.country');
		$owner_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_country'] && is_array($tableColumns['owner_country'])) ? $tableColumns['owner_country'] : null;
		*/$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('hrs' => $this->_name), $hotels_room_scheduler_column_arr);
			
		/*if (($hotels_page_column_arr &&  is_array($hotels_page_column_arr) && count( $hotels_page_column_arr) > 0)) 
		{
			$select->joinLeft(array('vp' => Zend_Registry::get('dbPrefix').'hotels_page'), 'vp.id = hrs.hotels_id', $hotels_page_column_arr);
		}			  
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('vp.entry_by = ' . $user_id . ' OR vp.hotels_owner = ' . $user_id);
		}
		
		if (($hotels_group_column_arr &&  is_array($hotels_group_column_arr) && count( $hotels_group_column_arr) > 0)) 
		{
			$select->joinLeft(array('tg' => Zend_Registry::get('dbPrefix').'hotels_group'), 'vp.group_id = tg.id', $hotels_group_column_arr);
		}
		
		if (($hotels_business_type_arr &&  is_array($hotels_business_type_arr) && count( $hotels_business_type_arr) > 0)) 
		{
			$select->joinLeft(array('vbt' =>  Zend_Registry::get('dbPrefix').'hotels_business_type'), 'vbt.id = vp.hotels_type', $hotels_business_type_arr);
		}
		
		if (($hotels_category_column_arr &&  is_array($hotels_category_column_arr) && count( $hotels_category_column_arr) > 0)) 
		{
			$select->joinLeft(array('vc' =>  Zend_Registry::get('dbPrefix').'hotels_category'), 'vc.id = vp.category_id', $hotels_category_column_arr);
		}
		
		if (($cities_column_arr &&  is_array($cities_column_arr) && count( $cities_column_arr) > 0)) 
		{
			$select->joinLeft(array('ct' =>  Zend_Registry::get('dbPrefix').'cities'), 'ct.city_id = vp.area_id', $cities_column_arr);
		}
		
		if (($states_column_arr &&  is_array($states_column_arr) && count( $states_column_arr) > 0)) 
		{
			$select->joinLeft(array('st' =>  Zend_Registry::get('dbPrefix').'states'), 'st.state_id = vp.state_id', $states_column_arr);
		}
		
		if (($countries_column_arr &&  is_array($countries_column_arr) && count( $countries_column_arr) > 0)) 
		{
			$select->joinLeft(array('cut' =>  Zend_Registry::get('dbPrefix').'countries'), 'cut.id = vp.country_id', $countries_column_arr);
		}		
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'vp.entry_by = up.user_id', $user_profile_column_arr);				   
		}
		
		if (($owner_profile_column_arr &&  is_array($owner_profile_column_arr) && count( $owner_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('ups' => Zend_Registry::get('dbPrefix').'user_profile'), 'vp.hotels_owner = ups.user_id', $owner_profile_column_arr);
		}
		
		if (($owner_country_column_arr &&  is_array($owner_country_column_arr) && count( $owner_country_column_arr) > 0)) 
		{
			$select->joinLeft(array('ocut' =>  Zend_Registry::get('dbPrefix').'countries'), 'ocut.id = ups.country', $owner_country_column_arr);
		}
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('vp.entry_by = ' . $user_id . ' OR vp.hotels_owner = ' . $user_id);
		}

		if (($vote_column_arr &&  is_array($vote_column_arr) && count( $vote_column_arr) > 0)) 
		{
            $select ->joinLeft(array( 'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting' ), 'vp.id  = vt.table_id', $vote_column_arr);
        }*/
		
		if($approve != null)
		{
			$select->where("hrs.available_status = ?", $approve);
		}

        if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("hrs.Start ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("hrs.Start ASC") ; 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'hrs.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(vp.title, ' ', vp.firstName, ' ', vp.lastName) ";
				break;
			case 'check_in' :
					$operatorFirstPart = " hrs.start_date ";
				break;
			case 'check_out' :
					$operatorFirstPart = " hrs.end_date ";
				break;
			case 'hotels_id_string' :
					$id_arr = explode(',', $operator_arr['value']);			
					$id_arr = array_filter( $id_arr );				
					$ids = implode(',', $id_arr);
					$operatorFirstPart =  " vp.id IN(".$ids.") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'groups' :
					$operatorFirstPart = " vp.group_id IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'categories' :
					$operatorFirstPart = " vp.category_id IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'businesstype' :
					$operatorFirstPart = " vp.hotels_type IN(".$operator_arr['value'].") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE vp.id  = vts.table_id) AS total_votes ";
                break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(hrs.start_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(hrs.start_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(hrs.start_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}    

}
?>