<?php

class Hotels_Model_DbTable_Expedia extends Eicra_Abstract_DbTable
{
    protected $_name    =  'hotels_api_expedia';
	protected $_cols	=	null;


    public function getReservationInfo($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);

        if (!$row)
        {            
			$options = null;
        }
        else
        {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
        return $options;
    }

    public function getReservationByItineraryId($itinerary)
    {
        $hotels_title = addslashes($hotels_title);
       
	    try
        {

            $select = $this->select()
            ->from(array('gp' => $this->_name),array('*'))
            ->where('gp.itineraryId =? ',$itinerary)
            ->limit(1);

            $options = $this->fetchAll($select);

            return $options;
        }
        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }

    public function getReservationByUserID($userId)
    {
        try
        {

            $select = $this->select()
            ->from(array('gp' => $this->_name),array('*'))
            ->where('gp.user_id =? ',$userId)
            ->limit(1);
            $options = $this->fetchAll($select);
            return $options;
        }
        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }
	
    public function getAllItineraries()
    {
        try
        {
            $select = $this->select()
            ->from($this->_name,'*')
            ->order('arrivalDate');
            $options = $this->fetchAll($select);
            return $options;
        }
        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }


	//Get All Datas
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;	
		
		$hotels_api_expedia_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_api_expedia'] && is_array($tableColumns['hotels_api_expedia'])) ? $tableColumns['hotels_api_expedia'] : array('hae.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('hae' => $this->_name), $hotels_api_expedia_column_arr);
						   
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('hae.user_id = ' . $user_id );
		}
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'hae.user_id = up.user_id', $user_profile_column_arr);				   
		}		

        if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("hae.id DESC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("hae.id DESC"); 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'hae.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array) || ($operator_arr['field'] == 'arrivalDate') || ($operator_arr['field'] == 'departureDate'))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(hae.departureDate)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(hae.departureDate, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(hae.departureDate, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}

}