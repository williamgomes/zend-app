<?php
/**
* This is the DbTable class for the hotels_brand table.
*/
class Hotels_Model_DbTable_Price extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_price_plan';			
	protected $_cols	=	null;
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
		   $options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ; 
    }
	
	public function getOptions($userChecking = false)
    {
        $auth = Zend_Auth::getInstance ();
		$select = $this->select()
				   ->from($this->_name, array('id', 'plan_name'))
				   ->order('plan_name ASC');
		if($auth->hasIdentity () && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true )
		{					
			$identity = $auth->getIdentity ();
			$user_id = $identity->user_id;
			$select->where('entry_by = '.$user_id);
		}
			 
        $options = $this->getAdapter()->fetchPairs($select);
		if($options) 
		{
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);            
        }
		else
		{
			//throw new Exception("Count not find rows $id"); 
			$options = null;
		}
        return $options;
    }
	
	
	
	public function getListInfo($approve = null, $search_params = null, $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$hotels_price_plan_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['hotels_price_plan'] && is_array($tableColumns['hotels_price_plan'])) ? $tableColumns['hotels_price_plan'] : array('hpp.*');
        $user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('hpp' => $this->_name), $hotels_price_plan_column_arr);
		
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'hpp.entry_by = up.user_id', $user_profile_column_arr);				   
		}
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('hpp.entry_by = ?', $user_id);
		}	
		
		if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }	
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("hpp.id ASC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("hpp.id ASC"); 
		}
		
		if(!empty($dataLimit))
		{
			$select->limit($dataLimit);
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = array();
		}
        return $options; 
	}	
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'hpp.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(rt.cat_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(rt.cat_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(rt.cat_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}		
		
}

?>