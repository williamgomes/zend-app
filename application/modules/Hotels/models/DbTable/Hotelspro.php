<?php

/**
 * This is the DbTable class for the hotels_brand table.
 */
class Hotels_Model_DbTable_Hotelspro extends Eicra_Abstract_DbTable {

    /** Table name */
    protected $_name = 'hotels_hotelspro_api';
    protected $_cols = null;

    //Get Datas
    public function getHotelsproInfo($hotelCode) {

        $row = $this->fetchRow('hotel_code = "' . $hotelCode . '"');
        if (!$row) {
            $options = null;
        } else {
            $options = $row->toArray();
            $options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
        return $options;
    }

    public function renew_table() {


        $sql = "ALTER TABLE " . $this->_name . "  RENAME TO new_table_name";
        $this->getAdapter()->query($sql);

        $sql = "CREATE TABLE " . $this->_name . " AS (SELECT *  FROM new_table_name LIMIT 0)";
        $this->getAdapter()->query($sql);

        $sql = "ALTER TABLE " . $this->_name . " DROP COLUMN id";
        $this->getAdapter()->query($sql);

        $sql = "ALTER TABLE " . $this->_name . " ADD id INT PRIMARY KEY AUTO_INCREMENT";
        $this->getAdapter()->query($sql);


        // file_put_contents('a.txt',  $sql);
    }

    public function delete_old_table() {

        $sql = "DROP TABLE new_table_name";
        $this->getAdapter()->query($sql);
    }

    public function fetchAllWithRoomType($room_type_id = null, $where = null) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';

        $show_arr1 = array('id' => 'hr.id', 'room_name' => 'hr.room_name', 'room_condition' => 'hr.room_condition',
            'room_entry_date' => 'hr.room_entry_date', 'entry_by' => 'hr.entry_by');
        $show_arr2 = array('room_type' => 'hrt.room_type');

        if (empty($room_type_id)) {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                if (empty($where)) {
                    $select = $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('hr' => $this->_name), $show_arr1)
                            ->where('hr.entry_by = ?', $user_id)
                            ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
                } else {
                    $select = $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('hr' => $this->_name), $show_arr1)
                            ->where('hr.entry_by = ?', $user_id)
                            ->where($where)
                            ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
                }
            } else {
                if (empty($where)) {
                    $select = $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('hr' => $this->_name), $show_arr1)
                            ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
                } else {
                    $select = $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('hr' => $this->_name), $show_arr1)
                            ->where($where)
                            ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
                }
            }
        } else {
            if ($user_id && $auth->getIdentity()->access_other_user_article != '1') {
                $select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('hr' => $this->_name), $show_arr1)
                        ->where('hr.entry_by = ?', $user_id)
                        ->where('hr.room_type_id = ? ', $room_type_id)
                        ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
            } else {
                $select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('hr' => $this->_name), $show_arr1)
                        ->where('hr.room_type_id = ? ', $room_type_id)
                        ->joinLeft(array('hrt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = hrt.id', $show_arr2);
            }
        }

        $options = $this->fetchAll($select);
        if (!$options) {
            $options = null;
        }
        return $options;
    }

    public function getAvailableRooms($room_type_id, $post_search_info) {
        try {
            $check_in_date = $post_search_info[Eicra_File_Constants::HOTELS_CHECK_IN];
            $check_out_date = $post_search_info[Eicra_File_Constants::HOTELS_CHECK_OUT];
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('hr' => $this->_name), array('hr.*, (SELECT COUNT(hrs.id) FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler AS hrs WHERE hrs.room_id = hr.id AND ((hrs.Start >= "' . $check_in_date . '" AND hrs.End <= "' . $check_out_date . '" ) OR (hrs.Start <= "' . $check_in_date . '" AND hrs.End >= "' . $check_in_date . '" ) OR (hrs.Start <= "' . $check_out_date . '" AND hrs.End >= "' . $check_out_date . '")) AND ( hrs.book_status = "2" OR hrs.book_status = "3" ) ) AS book_status_count'))
                    ->where('hr.room_type_id = ? ', $room_type_id)
            // ->joinLeft(array('hrs' =>  Zend_Registry::get('dbPrefix').'hotels_room_scheduler'), 'hrs.room_id = hr.id', array('book_status' => 'COUNT(hrs.id)'))
            // ->where(' (hrs.Start <= "'.$check_in_date .'" AND hrs.End >= "'.$check_in_date .'") OR (hrs.Start >= "'.$check_out_date .'" AND hrs.End <= "'.$check_out_date .'") ')
            //  ->group('hr.id')
            ;

            $options = $this->getAdapter()->fetchAll($select);

            if (!$options) {
                $options = null;
            } else {
                $data_arr = array();
                $i = 0;
                foreach ($options as $key => $value_arr) {
                    if ((int) $value_arr['book_status_count'] <= 0) {
                        $data_arr[$i] = $value_arr;
                        $i++;
                    }
                }
                $options = $data_arr;
            }
        } catch (Exception $e) {
            $options = null;
        }
        return $options;
    }

    public function isBooked($today_date, $room_type_id) {
        $return = false;
        $select = $this->select()
                ->from(array('hr' => $this->_name), array('id' => 'hr.id'))
                ->where('hr.room_type_id = ?', $room_type_id);
        $options = $this->getAdapter()->fetchAll($select);
        if ($options) {
            $return = true;
            $calendar_db = new Hotels_Model_DbTable_Calendar();
            foreach ($options as $key => $value_arr) {
                if (!$calendar_db->isBooked($today_date, $value_arr['id'])) {
                    $return = false;
                }
            }
        }
        return $return;
    }

    public function isJustBooked($room_type_id) {
        //try
        //	{
        $return = false;
        if (!empty($room_type_id)) {
            $translator = Zend_Registry::get('translator');
            $booked_time_diff = $translator->translator('hotels_front_page_just_booked_time');
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('hr' => $this->_name), array('hr.id, (SELECT COUNT(hrs.id) FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler AS hrs WHERE hrs.room_id = hr.id AND ( hrs.booking_date BETWEEN DATE_SUB( NOW() , INTERVAL ' . $booked_time_diff . ' MINUTE) AND NOW()) AND ( hrs.book_status = "2" ) ) AS NUM'))
                    ->where('hr.room_type_id = ?', $room_type_id);
            // ->joinLeft(array('hrc' => Zend_Registry::get('dbPrefix').'hotels_room_calendar'), 'hrc.room_id = hr.id')
            //->where('hrc.room_status = "2" AND ( hrc.modify_date BETWEEN DATE_SUB( NOW() , INTERVAL '.$booked_time_diff.' MINUTE) AND NOW())'); 

            $options = $this->getAdapter()->fetchAll($select);
            if ($options) {
                foreach ($options as $row) {
                    if ($row['NUM'] > '0') {
                        $return = true;
                    }
                }
            }
        }
        /* }
          catch(Exception $e)
          {
          $msg = $e->getMessage();
          $return = false;
          } */
        return $return;
    }

    public function getTotalRooms($room_type_id) {
        if ($room_type_id) {
            $select = $this->select()
                    ->from(array('hr' => $this->_name), array('room_number' => 'COUNT(*)'))
                    ->where('hr.room_type_id = ?', $room_type_id);
            $options = $this->getAdapter()->fetchAll($select);
            if ($options) {
                foreach ($options as $key => $value_arr) {
                    $room_number = $value_arr['room_number'];
                }
            } else {
                $room_number = 0;
            }
        } else {
            $room_number = 0;
        }
        return $room_number;
    }

    public function updateAvailableInformation($id, $data) {
        $this->update(array('room_available_information' => $data), array('id = ?' => $id));
    }

    public function updatePrice($id, $data) {
        $this->update(array('descount_price' => $data), array('id = ?' => $id));
    }

    public function getListInfo_backup($approve = null, $search_params = null, $userChecking = true) {
        $auth = Zend_Auth::getInstance();
        $role_id = ($auth->hasIdentity()) ? $auth->getIdentity()->role_id : '';
        $user_id = ($auth->hasIdentity()) ? $auth->getIdentity()->user_id : '';
        $today = strftime('%Y-%m-%dT%H:%M:%SZ', time());

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('hr' => $this->_name), array('hr.*', 'case when ((SELECT hrs.book_status FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler as hrs WHERE hrs.room_id = hr.id AND hrs.Start <= "' . $today . '" AND hrs.End >= "' . $today . '") != "") then (SELECT hrs.book_status FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler as hrs WHERE hrs.room_id = hr.id AND hrs.Start <= "' . $today . '" AND hrs.End >= "' . $today . '") else "1" end AS available_num'))
                ->joinLeft(array('rt' => Zend_Registry::get('dbPrefix') . 'hotels_room_type'), 'hr.room_type_id = rt.id', array('rt.room_type'))
                ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'hr.entry_by = up.user_id', array('username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "));


        if ($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true) {
            $select->where('hr.entry_by = ?', $user_id);
        }

        if ($search_params != null) {
            if ($search_params['sort']) {
                foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                    if ($sort_value_arr['dir'] == 'exp') {
                        $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                    } else {
                        $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                    }
                }
            } else {
                $select->order("hr.id ASC");
            }

            if ($search_params['filter'] && $search_params['filter']['filters']) {
                $where = '';
                $where_arr = array();
                $i = 0;
                foreach ($search_params['filter']['filters'] as $filter_key => $filter_obj) {
                    if ($filter_obj['field']) {
                        $where_arr[$i] = ' ' . $this->getOperatorString($filter_obj);
                        $i++;
                    } else if ($filter_obj['filters']) {
                        $where_sub_arr = array();
                        $sub = 0;
                        foreach ($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj) {
                            $where_sub_arr[$sub] = ' ' . $this->getOperatorString($sub_filter_obj);
                            $sub++;
                        }
                        $where_arr[$i] = ' (' . implode(strtoupper($filter_obj['logic']), $where_sub_arr) . ') ';
                        $i++;
                    }
                }
                $where = implode(strtoupper($search_params['filter']['logic']), $where_arr);
                if (!empty($where)) {
                    $select->where($where);
                }
            }
        } else {
            $select->order("hr.id ASC");
        }

        $options = $this->fetchAll($select);

        if (!$options) {
            $options = array();
        }
        return $options;
    }

    public function getListInfo() {
        try {
            $select = $this->select()
                    ->from($this->_name, array('*'))
                    ->limit(10);

            $options = $this->fetchAll($select);
            if ($options) {
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'hr.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']); //explode('GMT', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'full_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "title, ' ', " . $table_prefix . "firstName, ' ', " . $table_prefix . "lastName) ";
                break;
            case 'hotel_id' :
                $id_arr = explode(',', $operator_arr['value']);
                $id_arr = array_filter($id_arr);
                $ids = implode(',', $id_arr);
                $operatorFirstPart = " room_type_id IN(" . $ids . ") AND 1";
                $operator_arr['value'] = '1';
                break;
            case 'available_num' :
                $today = strftime('%Y-%m-%dT%H:%M:%SZ', time());
                $operatorFirstPart = ' ( SELECT case when ((SELECT hrss.book_status FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler as hrss WHERE hrss.room_id = hr.id AND hrss.Start <= "' . $today . '" AND hrss.End >= "' . $today . '") != "") then (SELECT hrss.book_status FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room_scheduler as hrss WHERE hrss.room_id = hr.id AND hrss.Start <= "' . $today . '" AND hrss.End >= "' . $today . '") else "1" end  AS available_num FROM ' . Zend_Registry::get('dbPrefix') . 'hotels_room as hrt WHERE hrt.id = hr.id  ) ';
                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(rt.cat_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(rt.cat_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(rt.cat_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    private function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }

}

?>