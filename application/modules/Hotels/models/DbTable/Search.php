<?php
/**
* This is the DbTable class for the hotels_page table.
*/
class Hotels_Model_DbTable_Search extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_page';
		
	//Get Datas
	public function getSearchInfo($where,$order) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		if($user_id && $auth->getIdentity()->access_other_user_article != '1')
		{
			$where .= (!empty($where)) ? ' AND ( entry_by = '.$user_id.' OR hotels_agent = '.$user_id.' ) ' : ' ( entry_by = '.$user_id.' OR hotels_agent = '.$user_id.' ) ';
		}
	   if($where)
	   {
		   $select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where($where)
						   ->order($order);
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->order($order);
		}
	
		$options = $select->query()->fetchAll();
		if (!$options) 
		{
            $options = null;
        }		
        return $options;   
    }
	
}

?>