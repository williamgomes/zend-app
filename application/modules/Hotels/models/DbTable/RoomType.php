<?php
/**
* This is the DbTable class for the hotels_brand table.
*/
class Hotels_Model_DbTable_RoomType extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_room_type';			
	protected $_cols	=	null;
	
	//Get Datas
	public function getRoomTypeInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
		   $options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ; 
    }	
	
	public function maxPeopleListRoomTypeUnic($sort = 'ASC')
	{
		$select = $this->select()
						->from($this->_name, array('max_people' => 'DISTINCT(max_people)'))
                       	->order('max_people '.$sort);
		$options = $this->getAdapter()->fetchAll($select);							
		if (!$options) 
		{
		   $options = null;
		}
		return $options;
	}
	
	public function priceListRoomTypeUnic($sort = 'ASC')
	{
		$select = $this->select()
						->from($this->_name, array('descount_price' => 'DISTINCT(descount_price)'))
                       	->order('descount_price '.$sort);
		$options = $this->getAdapter()->fetchAll($select);							
		if (!$options) 
		{
		   $options = null;
		}
		return $options;
	}
	
	public function fetchAllRoomType($where = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
	
		if($user_id  && $auth->getIdentity()->access_other_user_article != '1')
		{
			if(empty($where))
			{
				$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('hrt' => $this->_name ), array('*'))
						   ->where('hrt.entry_by = ?', $user_id);
			}
			else
			{
				$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('hrt' => $this->_name ), array('*'))
						   ->where($where)
						   ->where('hrt.entry_by = ?', $user_id);
			}
		}
		else
		{
			if(empty($where))
			{
			 	$select = $this->select()
						   ->from(array('hrt' => $this->_name ), array('*')); 
			}
			else
			{
				$select = $this->select()
						   ->from(array('hrt' => $this->_name ), array('*'))
						   ->where($where); 
			}
		}
		
		$options = $this->fetchAll($select);							
		if (!$options) 
		{
		   $options = null;
		}
		return $options;
	}
	
	public function updateAvailableInformation($id,$data)
	{
		$this->update(array('room_available_information' => $data), array('id = ?' => $id));
	}
	
	public function updatePrice($id,$data)
	{
		$this->update(array('descount_price' => $data), array('id = ?' => $id));
	}
	
	//Get Datas
	public function getAllRoomType()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'room_type'))
                       ->order('room_type ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }
	
	//Get Datas
	public function getAllRoomTypeByIds($ids)
    {
		try
		{
			$options = null;
			if($ids)
			{
				$id_arr = explode(',', $ids);			
				$id_arr = array_filter( $id_arr );				
				$ids = implode(',', $id_arr);
				$select = $this->select()
							   ->from($this->_name, array('*'))
							   ->order('room_type ASC')
							   ->where('id IN ('.$ids.')');
				$options = $this->getAdapter()->fetchAll($select);	
			}
		}
		catch(Exception $e)
		{
			$options = null;
		}
        return $options;
    }	
		
	//Get Datas
	public function getOptions($entry_by = null)
    {
		if($entry_by)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'room_type', 'primary_image'))
						   ->where('entry_by =?',$entry_by)
						   ->order('id ASC');			
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'room_type', 'primary_image'))
						   ->order('id ASC');
		}
		$options = $this->getAdapter()->fetchAll($select);        
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($entry_by = null)
    {
		if($entry_by)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'room_type'))
						   ->where('entry_by =?',$entry_by)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'room_type'))
						   ->order('id ASC');			
		} 
		$options = $this->getAdapter()->fetchPairs($select);       
        return $options;
    }
	
	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		$hotel_id = '';
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('rt' => $this->_name),array('rt.*', '(SELECT COUNT(hr.id) FROM '.Zend_Registry::get('dbPrefix').'hotels_room as hr WHERE rt.id = hr.room_type_id) AS room_num'))
						->joinLeft(array('hpp' => Zend_Registry::get('dbPrefix').'hotels_price_plan'), 'rt.price_plan_id = hpp.id', array( 'plan_name' => 'hpp.plan_name', 'minimum_stay' => "hpp.minimum_stay", 'minimum_stay_night' => "hpp.minimum_stay_night", "price_per_night" => "hpp.price_per_night", "price_after_discount" => "hpp.price_after_discount", "room_tax" => "hpp.room_tax", "room_tax_type" => "hpp.room_tax_type", "seasonal_price_per_night" => "hpp.seasonal_price_per_night", "season_start_date" => "hpp.season_start_date", "season_end_date" => "hpp.season_end_date", "stop_temporary" => "hpp.stop_temporary", "search_price" => "case when (price_after_discount > 0 and price_after_discount != '') then price_after_discount else price_per_night end"))
						->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'rt.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "));
							
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('rt.entry_by = ?', $user_id);
		}		
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("rt.id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{	
						if($filter_obj['field'] == 'hotel_id')
						{
							$hotel_id = $filter_obj['value'];
						}
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							if($sub_filter_obj['filters'])
							{
								$where_sub_sub_arr = array();
								$sub_sub = 0;
								foreach($sub_filter_obj['filters'] as $sub_sub_filter_key => $sub_sub_filter_obj)
								{
									if($sub_sub_filter_obj['field'] == 'hotel_id')
									{
										$hotel_id = $sub_sub_filter_obj['value'];
									}
									$where_sub_sub_arr[$sub_sub] = ' '.$this->getOperatorString($sub_sub_filter_obj);
									$sub_sub++;
								}
								$where_sub_arr[$sub] = ' ('.implode(strtoupper($sub_filter_obj['logic']), $where_sub_sub_arr).') ';
								$sub++;
							}
							else
							{
								if($sub_filter_obj['field'] == 'hotel_id')
								{
									$hotel_id = $sub_filter_obj['value'];
								}
								$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
								$sub++;
							}
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);	
					
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("rt.id ASC"); 
		}
		
		$options = $this->fetchAll($select);
		
		if(($hotel_id != 'any' && $hotel_id != '' && $options) || ($hotel_id == '0' && $options))
		{					
			$hotel_link = new Hotels_Controller_Helper_HotelLink($options);
			$options = $hotel_link->getHotelLinkedRoomType($hotel_id);
		}
		
        if(!$options)
		{
			$options = array();
		}
        return $options; 
	}	
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'rt.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'room_num' :
						$operatorFirstPart = ' (SELECT COUNT(hrs.id) FROM '.Zend_Registry::get('dbPrefix').'hotels_room as hrs WHERE rt.id = hrs.room_type_id) '; 
				break;
			case 'room_type_id' :
					$id_arr = explode(',', $operator_arr['value']);			
					$id_arr = array_filter( $id_arr );				
					$ids = implode(',', $id_arr);
					$operatorFirstPart =  " rt.id IN(".$ids.") AND 1";
					$operator_arr['value'] = '1';
				break;
			case 'descount_price_min' :
			case 'descount_price_max' :
					$operatorFirstPart = 'hpp.price_after_discount';
				break;
			case 'hotel_id' :
					$operatorFirstPart = ' 1 ';
					$operator_arr['value'] = ' 1 ';
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(rt.cat_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(rt.cat_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(rt.cat_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}	
}

?>