<?php
/**
* This is the DbTable class for the hotels_brand table.
*/
class Hotels_Model_DbTable_Calendar extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'hotels_room_calendar';
	
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
		   $options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ; 
    }
	
	public function getCalendarInfo($room_id,$check_in_date = null , $check_out_date = null) 
    {
		try
		{
			if(!empty($check_in_date) && !empty($check_out_date))
			{
				$select = $this->select()
							   ->from($this->_name, array('*'))
							   ->where('room_id = ? ', $room_id)
							   ->where('room_price >= ? ', 1)
							   ->where('calendar_date >= ? ', $check_in_date)
							   ->where('calendar_date <= ? ', $check_out_date); 
			}
			else
			{
			   $select = $this->select()
							   ->from($this->_name, array('*'))
							   ->where('room_id = ? ', $room_id); 	
			}
						   								   
			$options = $this->getAdapter()->fetchAll($select);			
			if (!$options) 
			{
			   $options = null;
			}
		}
		catch(Exception $e)
		{
			$options = null;
		}
        return $options;   
    }
	
	public function isAvailable($check_in_date, $check_out_date, $room_id)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' 	=> 		$this->_name,
						'field' 	=> 		'room_id',
						'exclude' 	=>  	" calendar_date >= '" . $check_in_date . "' AND calendar_date <= '" . $check_out_date . "' AND room_status != '1' "
					)
				);	
		if($validator->isValid($room_id))
		{
			return false;
		}
		else
		{
			return true;
		}		
	}
	
	public function isBooked($today_date, $room_id)
	{
		$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' 	=> 		$this->_name,
						'field' 	=> 		'room_id',
						'exclude' 	=>  	" calendar_date = '" . $today_date .  "' AND room_status = '2' "
					)
				);	
		if($validator->isValid($room_id))
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	public function updateCalendar($room_id,$data_string)
	{		
		try
		{
			$where = $this->getAdapter()->quoteInto('room_id = ?', $room_id);
			$this->delete($where);
			
			$data_arr = explode(',', $data_string);
			if($data_arr[0])
			{
				$i =0;
				$data_simple_arr	=	array();
				foreach($data_arr as $key => $value_string)
				{
					$value_arr = explode(';;', $value_string);
					$data[$i] = array(
								'room_id'			=>		$room_id,
								'calendar_date'		=>		$value_arr[0],
								'room_status'		=>		$value_arr[1],
								'room_price'		=>		$value_arr[2]
								);
					$data_simple_arr	=	($i	==	0)	?	array(	$room_id,	$value_arr[0],	$value_arr[1],	$value_arr[2])	:	array_merge(	$data_simple_arr,	array(	$room_id,	$value_arr[0],	$value_arr[1],	$value_arr[2])	);
					$i++;								
				}
				$values = implode(',',array_fill(0,count($data),'(?,?,?,?)'));
				$sql = 'INSERT INTO '.$this->_name.' (room_id, calendar_date, room_status, room_price) VALUES '.$values;
				$stmt = $this->getAdapter()->prepare($sql);
				$id = $stmt->execute( $data_simple_arr );								
			}		
		}
		catch(Exception $e)
		{
			file_put_contents('status.txt',$e->getMessage());
		}
	}
	

	
	/*public function updatePrice($id,$data)
	{
		$this->update(array('descount_price' => $data), array('id = ?' => $id));
	}*/	
		
}

?>