<?php


class Hotels_Model_WegoApiModel
{
    
    private $_apiKey;
    
    private $_baseUri;
    
    private $_tsCode;
    
    private $_username;
    
    private $_password;
    
    private $_api_settings;
    
    private $_api_name;
    
    private $_api_display_name;
    
    
    
    public function __construct ()
    {
        $this->_api_settings = new Hotels_Controller_Helper_Api();
    }
    
	/**
     * @return the $_apiKey
     */
    
    
    public function getApiKey ()
    {
        
        
        if (empty($this->_apiKey)) {
            $this->setApiKey();
        } 
        
        return $this->_apiKey;
    }

	/**
     * @param field_type $_apiKey
     */
    public function setApiKey ()
    {
        $this->_apiKey = $this->_api_settings->getValue('api_key', 1);
    }

	/**
     * @return the $_baseUri
     */
    public function getBaseUri ()
    {
        if (empty($this->_baseUri)) {
            $this->setBaseUri();
        }
        
        return $this->_baseUri;
    }

	/**
     * @param field_type $_baseUri
     */
    public function setBaseUri ()
    {
        $this->_baseUri = $this->_api_settings->getValue('api_base_uri', 1);
    }

	/**
     * @return the $_tsCode
     */
    public function getTsCode ()
    {
        
        if (empty($this->_tsCode)) {
            $this->setTsCode();
        }
        return $this->_tsCode;
    }

	/**
     * @param field_type $_tsCode
     */
    public function setTsCode ()
    {
        $this->_tsCode = $this->_api_settings->getValue('ts_code', 1);
    }

	/**
     * @return the $_username
     */
    public function getUsername ()
    {

        if (empty($this->_username)) {
            $this->setUsername();
        }
        
        return $this->_username;
    }

	/**
     * @param field_type $_username
     */
    public function setUsername ()
    {
        $this->_username = $this->_api_settings->getValue('api_username', 1);
    }

	/**
     * @return the $_password
     */
    public function getPassword ()
    {
        
        if (empty($this->_password)) {
            $this->setPassword();
        }
        return $this->_password;
    }

	/**
     * @param field_type $_password
     */
    public function setPassword ()
    {
        
        $this->_password = $this->_api_settings->getValue('api_password', 1);
    }
	/**
     * @return the $_api_name
     */
    public function getApi_name ()
    {
        if (empty($this->_api_name)) {
            $this->setApi_name();
        }
        
        return $this->_api_name;
    }

	/**
     * @param field_type $_api_name
     */
    public function setApi_name ()
    {
        $this->_api_name = $this->_api_settings->getValue('api_name', 1);
    }

	/**
     * @return the $_api_display_name
     */
    public function getApi_display_name ()
    {
        if (empty($this->_api_display_name)) {
            $this->setApi_display_name();
        }
        
        return $this->_api_display_name;
    }

	/**
     * @param field_type $_api_display_name
     */
    public function setApi_display_name ()
    {
        $this->_api_display_name = $this->_api_settings->getValue('api_title', 1);
    }


    
    
    
}