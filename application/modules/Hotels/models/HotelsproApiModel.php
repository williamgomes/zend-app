<?php
class Hotels_Model_HotelsproApiModel
{
    public $_apiKey;
    public $_apiUri;
    public $_aff_id;
    public $_api_settings;
    public $_hotelCount;
    public $_currency;
   
    public function __construct ()
    {
        $this->_api_settings = new Hotels_Controller_Helper_Api();
    }
    /**
     *
     * @return the $_apiKey
     */
    public function getApiKey ()
    {
        if (empty($this->_apiKey)) {
            $this->setApiKey();
            return $this->_apiKey;
        } else {
            return $this->_apiKey;
        }
    }
    /**
     *
     * @param field_type $_apiKey
     */
    private function setApiKey ()
    {
        $this->_apiKey = $this->_api_settings->getValue('api_key', 3);        
    }
    /**
     *
     * @return the $_apiUri
     */
    public function getApiUri ()
    {
        if (empty($this->_apiUri)) {
            $this->setApiUri();
            return $this->_apiUri;
        } else {
            return $this->_apiUri;
        }
    }
    /**
     *
     * @param field_type $_apiUri
     */
    private function setApiUri ()
    {
        $this->_apiUri = $this->_api_settings->getValue('api_uri', 3);
    }	
    /**
     *
     * @return the $_apiUri
     */
    public function getAffiliateCode ()
    {
        if (empty($this->_aff_id)) {
            $this->setAffiliateCode();
            return $this->_aff_id;
        } else {
            return $this->_aff_id;
        }
    }
    /**
     *
     * @param field_type $_apiUri
     */
    private function setAffiliateCode ()
    {
        $this->_aff_id = $this->_api_settings->getValue('aff_id', 3);
    }	
    /**
     *
     * @return the $_hotelCount
     */
    public function getHotelCount ()
    {
        if (empty($this->_hotelCount)) {
            $this->setHotelCount();
            return $this->_hotelCount;
        } else {
            return $this->_hotelCount;
        }
    }
    /**
     *
     * @param field_type $_hotelCount
     */
    private function setHotelCount ()
    {
        $this->_hotelCount = $this->_api_settings->getValue('per_count', 3);
    }	
    /**
     *
     * @return the $_hotelCount
     */
    public function getCurrency ()
    {
        if (empty($this->_currency)) {
            $this->setCurrency();
            return $this->_currency;
        } else {
            return $this->_currency;
        }
    }
    /**
     *
     * @param field_type $_hotelCount
     */
    private function setCurrency ()
    {
        $this->_currency = $this->_api_settings->getValue('currency', 3);
    }	
}
?>