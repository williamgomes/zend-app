<?php
class Hotels_Model_PriceMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Price');
        }
        return $this->_dbTable;
    }
	
	public function save(Hotels_Model_Price $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{				
				try 
				{
					unset($data['id']);
				
					$data = array(					
						'entry_by' 								=>		$obj->getEntry_by(),					
						'plan_name' 							=>		$obj->getPlan_name(),
						'minimum_stay' 							=>		$obj->getMinimum_stay(),
						'minimum_stay_night' 					=>		$obj->getMinimum_stay_night(),
						'price_per_night' 						=>		$obj->getPrice_per_night(),
						'price_after_discount' 					=>		$obj->getPrice_after_discount(),
						'room_tax' 								=>		$obj->getRoom_tax(),
						'room_tax_type' 						=>		$obj->getRoom_tax_type(),
						'seasonal_price_per_night' 				=>		$obj->getSeasonal_price_per_night(),
						'season_start_date' 					=>		$obj->getSeason_start_date(),
						'season_end_date' 						=>		$obj->getSeason_end_date(),
						'stop_temporary' 						=>		$obj->getStop_temporary()								
					);
					
					$last_id = $this->getDbTable()->insert($data);	
					$room_type_id_str = $obj->getRoom_type_id();
					if(!empty($room_type_id_str) && !empty($last_id))
					{
						$room_type_db = new Hotels_Model_DbTable_RoomType();
						$room_type_db->update(array('price_plan_id' => '0'), 'price_plan_id = '.$last_id);
						$room_type_db->update(array('price_plan_id' => $last_id), 'id IN('.$room_type_id_str.')');
					}
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{					
				try 
				{
					$data = array(		
						'plan_name' 							=>		$obj->getPlan_name(),
						'minimum_stay' 							=>		$obj->getMinimum_stay(),
						'minimum_stay_night' 					=>		$obj->getMinimum_stay_night(),
						'price_per_night' 						=>		$obj->getPrice_per_night(),
						'price_after_discount' 					=>		$obj->getPrice_after_discount(),
						'room_tax' 								=>		$obj->getRoom_tax(),
						'room_tax_type' 						=>		$obj->getRoom_tax_type(),
						'seasonal_price_per_night' 				=>		$obj->getSeasonal_price_per_night(),
						'season_start_date' 					=>		$obj->getSeason_start_date(),
						'season_end_date' 						=>		$obj->getSeason_end_date(),
						'stop_temporary' 						=>		$obj->getStop_temporary()						
					);				
					// Start the Update process
					
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$room_type_id_str = $obj->getRoom_type_id();
					$room_type_db = new Hotels_Model_DbTable_RoomType();
					if(!empty($id))
					{						
						$room_type_db->update(array('price_plan_id' => '0'), 'price_plan_id = '.$id);
					}
					if(!empty($room_type_id_str))
					{						
						$room_type_db->update(array('price_plan_id' => $id), 'id IN('.$room_type_id_str.')');
					}
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>