<?php

class Hotels_Model_Demotest {
    protected $_id;
    protected $_demo_name;
    protected $_demo_title;
  
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveDemotest() {
        $mapper = new Hotels_Model_DemotestMapper();
        $return = $mapper->save($this);
        return $return;
    }

    public function setDemo_name($text) {
        $this->_demo_name = $text;
        return $this;
    }
    
    public function setid($text) {
        $this->_id = $text;
        return $this;
    }

    public function setDemo_title($text) {
        $this->_demo_title= $text;
        return $this;
    }

   
  public function getid() {
        return $this->_id;
    }

    public function getDemo_name() {
        return $this->_demo_name;
    }

    public function getDemo_title() {
        return $this->_demo_title;
    }

 

}

?>