<?php
class Hotels_Model_RoomTypeMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_RoomType');
        }
        return $this->_dbTable;
    }
	
	public function save(Hotels_Model_RoomType $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{				
				try 
				{
					unset($data['id']);
				
					$data = array(					
						'entry_by' 					=> 	$obj->getEntry_by(),
						'room_type' 				=>	$obj->getRoom_type(),
						'primary_image' 			=>	$obj->getPrimary_image(),
						'room_type_image' 			=>	$obj->getRoom_type_image(),
						'max_people' 				=>	$obj->getMax_people(),
						'price_plan_id' 			=>	$obj->getPrice_plan_id(),
						'total_room_no' 			=>	$obj->getTotal_room_no(),
						'room_left' 				=>	$obj->getRoom_left(),
						'room_condition' 			=>	$obj->getRoom_condition(),
						'room_desc' 				=>	$obj->getRoom_desc()									
					);
					
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{					
				try 
				{
					$data = array(		
						'room_type' 				=>	$obj->getRoom_type(),
						'primary_image' 			=>	$obj->getPrimary_image(),
						'room_type_image' 			=>	$obj->getRoom_type_image(),
						'max_people' 				=>	$obj->getMax_people(),
						'price_plan_id' 			=>	$obj->getPrice_plan_id(),
						'total_room_no' 			=>	$obj->getTotal_room_no(),
						'room_left' 				=>	$obj->getRoom_left(),
						'room_condition' 			=>	$obj->getRoom_condition(),
						'room_desc' 				=>	$obj->getRoom_desc(),
						'room_available_information'=>	$obj->getRoom_available_information()						
					);				
					// Start the Update process
					
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>