<?php
class Hotels_Model_Areas
{
	protected $_city_id;
	protected $_state_id;
	protected $_city;	
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth property');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveAreas()
		{
			$mapper  = new Hotels_Model_AreasMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setCity_id($text)
		{
			$this->_city_id = $text;
			return $this;
		}
		
		public function setState_id($text)
		{
			$this->_state_id = $text;
			return $this;
		}
		
		public function setCity($text)
		{
			$this->_city = addslashes($text);
			return $this;
		}
		
		
		public function getCity_id()
		{         
			return $this->_city_id;
		}
		
		public function getState_id()
		{         
			return $this->_state_id;
		}
		
		public function getCity()
		{
			return $this->_city;
		}		
}
?>