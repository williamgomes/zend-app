<?php

class Hotels_Model_HotelsproMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Hotelspro');
        }
        return $this->_dbTable;
    }

    public function save(Hotels_Model_Hotelspro $obj) {

        if ((null === ($hotel_code = $obj->getHotel_code())) || empty($hotel_code)) {
            try {
                unset($data['id']);

                $data = array(
                    'hotel_code' => $obj->getHotel_code(),
                    'info' => $obj->getInfo(),
                    'dest_id' => $obj->getDest_id(),
                    'dest' => $obj->getDest(),
                    'name' => $obj->getName(),
                    'rating' => $obj->getRating(),
                    'address' => $obj->getAddress(),
                    'postal_code' => $obj->getPostal_code(),
                    'phone' => $obj->getPhone(),
                    'hotel_area' => $obj->getHotel_area(),
                    'chain' => $obj->getChain(),
                    'coordinates' => $obj->getCoordinates(),
                    'image' => $obj->getImage(),
                    'pamenities' => $obj->getPamenities(),
                    'ramenities' => $obj->getRamenities()
                        //'roomnumber' 				    =>	$obj->getRoomnumber(),
                        //'dest_country' 				    =>	$obj->getDest_country(),
                        //'dest_city' 				    =>	$obj->getDest_city(),
                        //'dest_state' 				    =>	$obj->getDest_state()
                );
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            try {
                $data = array(
                    'hotel_code' => $obj->getHotel_code(),
                    'info' => $obj->getInfo(),
                    'dest_id' => $obj->getDest_id(),
                    'dest' => $obj->getDest(),
                    'name' => $obj->getName(),
                    'rating' => $obj->getRating(),
                    'address' => $obj->getAddress(),
                    'postal_code' => $obj->getPostal_code(),
                    'phone' => $obj->getPhone(),
                    'hotel_area' => $obj->getHotel_area(),
                    'chain' => $obj->getChain(),
                    'coordinates' => $obj->getCoordinates(),
                    'image' => $obj->getImage(),
                    'pamenities' => $obj->getPamenities(),
                    'ramenities' => $obj->getRamenities()
                        //	'roomnumber' 				    =>	$obj->getRoomnumber()
                        //	'dest_country' 				    =>	$obj->getDest_country(),
                        //	'dest_city' 				    =>	$obj->getDest_city(),
                        //	'dest_state' 				    =>	$obj->getDest_state()				
                );
                // Start the Update process
                $obj = new Hotels_Model_DbTable_Hotelspro();
                $result = $obj->getHotelsproInfo($hotel_code);
                if ($result == null) {
                    $last_id = $this->getDbTable()->insert($data);
                    $result = array('status' => 'ok', 'id' => $last_id);
                } else {
                    $this->getDbTable()->update($data, array('hotel_code = ?' => $hotel_code));
                    $result = array('status' => 'ok', 'hotel_code' => $hotel_code);
                }
            } catch (Exception $e) {
                $result = array('status' => 'err', 'hotel_code' => $hotel_code, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>