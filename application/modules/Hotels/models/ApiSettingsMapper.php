<?php

/**
 * ******************************************************************************
 * Eicra Soft Ltd - is the developer of this application
 * Copyright (c) Eicra.com  2007-2012  All Rights Reserved
 *
 ******************************************************************************
 * @Email   : info@eicra.com                                              *
 * @Modules : Hotel API                                                   *
 * @author  : Eicra Soft Limited                                          *
 * @Website : http://www.eicra.com                                        *
 ******************************************************************************
 *                                                                       *
 * This software is furnished under a license and may be used and copied *
 * only  in  accordance  with  the  terms  of such  license and with the *
 * inclusion of the above copyright notice.  This software  or any other *
 * copies thereof may not be provided or otherwise made available to any *
 * other person.  No title to and  ownership of the  software is  hereby *
 * transferred.                                                          *
 *                                                                       *
 * You may not reverse  engineer, decompile, defeat  license  encryption *
 * mechanisms, or  disassemble this software product or software product *
 * license.  We  may terminate this license if you don't *
 * comply with any of the terms and conditions set forth in our end user *
 * license agreement (EULA).  In such event,  licensee  agrees to return *
 * licensor  or destroy  all copies of software  upon termination of the *
 * license.                                                              *
 *                                                                       *
 * Please see the EULA file for the full End User License Agreement.     *
 ******************************************************************************
 */

class Hotels_Model_ApiSettingsMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_ApiSetting');
        }
        return $this->_dbTable;
    }
	
	public function save(Hotels_Model_ApiSettings $obj)
		{
		    
		    if ((null === ($id = $obj->getId())) || empty($id))
		    {
		        try
		        {
		            unset($data['id']);
		    
		            $data = array(
		    
		                    'api_id' 			=> $obj->getApi_id(),
		                    'setting'		    => $obj->getSetting(),
		                    'value'				=> $obj->getValue()
		            );
		    
		            $last_id = $this->getDbTable()->insert($data);
		            $result = array('status' => 'ok' ,'id' => $last_id);
		        }
		        catch (Exception $e)
		        {
		            $result = array('status' => 'err' , 'msg' => $e->getMessage() . var_export($data)); 
		        }
		    
		    }
		    else
		    {
		        try
		        {
		            $data = array(
		    
		                    'value'				=> $obj->getValue()
		            );
		            // Start the Update process
		    
		            $this->getDbTable()->update($data, array('setting = ?' => $obj->getSetting() ,  'api_id = ?' => $obj->getApi_id()));
		    
		            $result = array('status' => 'ok' ,'id' => $id);
		        }
		        catch (Exception $e)
		        {
		            $result = array('status' => 'err' , 'msg' => $e->getMessage());
		        }
		    }
		    return $result;
		    
		}
   	
	

}
?>