<?php

class Hotels_Model_HotelsproData {

    protected $_id;
    protected $_invoice_id;
    protected $_tracking_id;
    protected $_response_id;
    protected $_data_object;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth brand');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth brand');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveReservationInfo(array $arrayData = NULL) {
        $mapper = new Hotels_Model_HotelsproDataMapper();
        $result = $mapper->save($arrayData);
        return $result;
    }
    
    
    
    

    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setInvoiceID($text) {
        $this->_invoice_id = $text;
        return $this;
    }

    public function setTrackingID($text) {
        $this->_tracking_id = $text;
        return $this;
    }

    public function setResponseID($text) {
        $this->_response_id = $text;
        return $this;
    }

    public function setDataObject($text) {
        $this->_data_object = $text;
        return $this;
    }

    
    
    

    public function getId() {
        return $this->_id;
    }

    public function getInvoiceID() {
        return $this->_invoice_id;
    }

    public function getTrackingID() {
        return $this->_tracking_id;
    }

    public function getResponseID() {
        return $this->_response_id;
    }

    public function getDataObject() {
        return $this->_data_object;
    }

}

?>