<?php

class Hotels_Model_Hotelspro {

    protected $_id;
    protected $_hotel_code;
    protected $_info;
    protected $_dest_id;
    protected $_dest;
    protected $_name;
    protected $_rating;
    protected $_address;
    protected $_postal_code;
    protected $_phone;
    protected $_coordinates;
    protected $_image;
    protected $_pamenities;
    protected $_ramenities;
    protected $_roomnumber;
    protected $_dest_country;
    protected $_dest_city;
    protected $_dest_state;
    protected $_chain;
    protected $_hotel_area;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth brand');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth brand');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveHotelspro() {
        $mapper = new Hotels_Model_HotelsproMapper();
        $result = $mapper->save($this);
        return $result;
    }

    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setHotel_code($text) {
        $this->_hotel_code = $text;
        return $this;
    }

    public function setInfo($text) {
        $this->_info = $text;
        return $this;
    }

    public function setDest_id($text) {
        $this->_dest_id = $text;
        return $this;
    }

    public function setDest($text) {
        $this->_dest = $text;
        return $this;
    }

    public function setName($text) {
        $this->_name = $text;
        return $this;
    }

    public function setRating($text) {
        $this->_rating = $text;
        return $this;
    }

    public function setAddress($text) {
        $this->_address = $text;
        return $this;
    }

    public function setPostal_code($text) {
        $this->_postal_code = $text;
        return $this;
    }

    public function setPhone($text) {
        $this->_phone = $text;
        return $this;
    }

    public function setCoordinates($text) {
        $this->_coordinates = $text;
        return $this;
    }

    public function setImage($text) {
        $this->_image = $text;
        return $this;
    }

    public function setPamenities($text) {
        $this->_pamenities = $text;
        return $this;
    }

    public function setRamenities($text) {
        $this->_ramenities = $text;
        return $this;
    }

    public function setRoomnumber($text) {
        $this->_roomnumber = $text;
        return $this;
    }

    public function setDest_country($text) {
        $this->_dest_country = $text;
        return $this;
    }

    public function setDest_city($text) {
        $this->_dest_city = $text;
        return $this;
    }

    public function setDest_state($text) {
        $this->_dest_city = $text;
        return $this;
    }

    public function setChain($text) {
        $this->_chain = $text;
        return $this;
    }

    public function setHotel_area($text) {
        $this->_hotel_area = $text;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getHotel_code() {
        return $this->_hotel_code;
    }

    public function getInfo() {
        return $this->_info;
    }

    public function getDest_id() {
        return $this->_dest_id;
    }

    public function getDest() {
        return $this->_dest;
    }

    public function getName() {
        return $this->_name;
    }

    public function getRating() {
        return $this->_rating;
    }

    public function getAddress() {
        return $this->_address;
    }

    public function getPostal_code() {
        return $this->_postal_code;
    }

    public function getPhone() {
        return $this->_phone;
    }

    public function getCoordinates() {
        return $this->_coordinates;
    }

    public function getImage() {
        return $this->_image;
    }

    public function getPamenities() {
        return $this->_pamenities;
    }

    public function getRamenities() {
        return $this->_ramenities;
    }

    public function getRoomnumber() {
        return $this->_roomnumber;
    }

    public function getDest_country() {
        return $this->_dest_country;
    }

    public function getDest_city() {
        return $this->_dest_city;
    }

    public function getDest_state() {
        return $this->_dest_city;
    }

    public function getChain() {
        return $this->_chain;
    }

    public function getHotel_area() {
        return $this->_hotel_area;
    }

}

?>