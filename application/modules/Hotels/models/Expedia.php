<?php

class Hotels_Model_Expedia {

protected $_id;
protected $_itineraryId;
protected $_arrivalDate;
protected $_departureDate;
protected $_hotelId;
protected $_user_id;
protected $_confirmationNumbers;
protected $_email;
protected $_rateCurrencyCode;
protected $_resultSetObject;
protected $_price;



    public function __construct(array $options = null)

		{

			if (is_array($options)) {

				$this->setOptions($options);

			}

		}



		 public function __set($name, $value)

		{

			$method = 'set' . $name;

			if (('mapper' == $name) || !method_exists($this, $method)) {

				throw new Exception('Invalid Auth hotels');

			}

			$this->$method($value);

		}





		public function __get($name)

		{

			$method = 'get' . $name;

			if (('mapper' == $name) || !method_exists($this, $method)) {

				throw new Exception('Invalid Auth hotels');

			}

			return $this->$method();

		}



		public function setOptions(array $options)

		{

			$methods = get_class_methods($this);

			foreach ($options as $key => $value) {

				$method = 'set' . ucfirst($key);

				if (in_array($method, $methods)) {

					$this->$method($value);

				}

			}

			return $this;

		}

		public function saveReservation( ) {

		    $mapper  = new Hotels_Model_ExpediaMapper();
		    $result= $mapper->saveReservationInfo($this);
		    return $result;

        }
		/**
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }

		/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }

		/**
     * @return the $_itineraryId
     */
    public function getItineraryId ()
    {
        return $this->_itineraryId;
    }

		/**
     * @param field_type $_itineraryId
     */
    public function setItineraryId ($_itineraryId)
    {
        $this->_itineraryId = $_itineraryId;
    }

		/**
     * @return the $_arrivalDate
     */
    public function getArrivalDate ()
    {
        return $this->_arrivalDate;
    }

		/**
     * @param field_type $_arrivalDate
     */
    public function setArrivalDate ($_arrivalDate)
    {
        $this->_arrivalDate = $_arrivalDate;
    }

		/**
     * @return the $_departureDate
     */
    public function getDepartureDate ()
    {
        return $this->_departureDate;
    }

		/**
     * @param field_type $_departureDate
     */
    public function setDepartureDate ($_departureDate)
    {
        $this->_departureDate = $_departureDate;
    }

		/**
     * @return the $_hotelId
     */
    public function getHotelId ()
    {
        return $this->_hotelId;
    }

		/**
     * @param field_type $_hotelId
     */
    public function setHotelId ($_hotelId)
    {
        $this->_hotelId = $_hotelId;
    }

		/**
     * @return the $_user_id
     */
    public function getUser_id ()
    {
        return $this->_user_id;
    }

		/**
     * @param field_type $_user_id
     */
    public function setUser_id ($_user_id)
    {
        $this->_user_id = $_user_id;
    }

		/**
     * @return the $_confirmationNumbers
     */
    public function getConfirmationNumbers ()
    {
        return $this->_confirmationNumbers;
    }

		/**
     * @param field_type $_confirmationNumbers
     */
    public function setConfirmationNumbers ($_confirmationNumbers)
    {
        $this->_confirmationNumbers = $_confirmationNumbers;
    }

		/**
     * @return the $_email
     */
    public function getEmail ()
    {
        return $this->_email;
    }

		/**
     * @param field_type $_email
     */
    public function setEmail ($_email)
    {
        $this->_email = $_email;
    }

		/**
     * @return the $_rateCurrencyCode
     */
    public function getRateCurrencyCode ()
    {
        return $this->_rateCurrencyCode;
    }

		/**
     * @param field_type $_rateCurrencyCode
     */
    public function setRateCurrencyCode ($_rateCurrencyCode)
    {
        $this->_rateCurrencyCode = $_rateCurrencyCode;
    }

		/**
     * @return the $_resultSetObject
     */
    public function getResultSetObject ()
    {
        return $this->_resultSetObject;
    }

		/**
     * @param field_type $_resultSetObject
     */
    public function setResultSetObject ($json_resultSet)
    {
        $this->_resultSetObject = $json_resultSet;
    }
	/**
     * @return the $_price
     */
    public function getPrice ()
    {
        return $this->_price;
    }

	/**
     * @param field_type $_price
     */
    public function setPrice ($_price)
    {
        $this->_price = $_price;
    }




}
