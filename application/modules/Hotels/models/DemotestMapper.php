<?php

class Hotels_Model_DemotestMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Demotest');
        }
        return $this->_dbTable;
    }

    public function save(Hotels_Model_Demotest $obj) {

        if ((null === ($id = $obj->getid())) || empty($id)) {
            try {
                unset($data['id']);

                $data = array(
                    'demo_name' => $obj->getDemo_name(),
                    'demo_title' => $obj->getDemo_title()
                );
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            try {
                $data = array(
                    'demo_name' => $obj->getDemo_name(),
                    'demo_title' => $obj->getDemo_title()
                );

                // Start the Update process					
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>