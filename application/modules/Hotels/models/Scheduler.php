<?php
class Hotels_Model_Scheduler
{
	protected $_id;	
	protected $_room_id;	
	protected $_Title;
	protected $_Start;
	protected $_End;	
	protected $_StartTimezone;		
	protected $_EndTimezone;
	protected $_IsAllDay;
	protected $_Description;	
	protected $_RecurrenceID;		
	protected $_RecurrenceRule;
	protected $_RecurrenceException;
	protected $_book_status;
	protected $_available_status;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Auth hotels. ');
		}
		$this->$method($value);
	}

	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Auth hotels');
		}
		return $this->$method();
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function saveScheduler()
	{
		$mapper  = new Hotels_Model_SchedulerMapper();
		$return = $mapper->save($this);
		return $return;
	}
	
	public function deleteScheduler()
	{
		$mapper  = new Hotels_Model_SchedulerMapper();
		$return = $mapper->delete($this);
		return $return;
	}

	public function setId($text)
	{
		$this->_id = $text;
		return $this;
	}

	public function setRoom_id($text)		
	{
		$this->_room_id = $text;
		return $this;
	}

	public function setTitle($text)		
	{
		$this->_Title = $text;
		return $this;
	}

	public function setStart($text)
	{
		$this->_Start = ($text)? $text : '0000-00-00';
		return $this;
	}
	
	public function setEnd($text)
	{
		$this->_End = ($text)? $text : '0000-00-00';
		return $this;
	}

	public function setStartTimezone($text)		
	{
		$this->_StartTimezone = $text;
		return $this;
	}

	public function setEndTimezone($text)		
	{
		$this->_EndTimezone = $text;
		return $this;
	}

	public function setIsAllDay($text)
	{
		$this->_IsAllDay = $text;
		return $this;
	}
	
	public function setDescription($text)
	{
		$this->_Description = $text;
		return $this;
	}

	public function setRecurrenceID($text)		
	{
		$this->_RecurrenceID = $text;
		return $this;
	}

	public function setRecurrenceRule($text)		
	{
		$this->_RecurrenceRule = $text;
		return $this;
	}

	public function setRecurrenceException($text)
	{
		$this->_RecurrenceException = $text;
		return $this;
	}
	
	public function setBook_status($text)
	{
		$this->_book_status = $text;
		return $this;
	}

	public function setAvailable_status($text)		
	{
		$this->_available_status = $text;
		return $this;
	}
	
	public function getId()
	{
		return $this->_id;
	}

	public function getRoom_id()		
	{
		return $this->_room_id;		
	}

	public function getTitle()
	{
		return $this->_Title;
	}

	public function getStart()
	{
		return $this->_Start = ($this->_Start)? $this->_Start : '0000-00-00';
	}

	public function getEnd()
	{
		return $this->_End = ($this->_End)? $this->_End : '0000-00-00';
	}

	public function getStartTimezone()
	{
		return $this->_StartTimezone;
	}

	public function getEndTimezone()
	{
		return $this->_EndTimezone;
	}

	public function getIsAllDay()
	{
		return $this->_IsAllDay;
	}

	public function getDescription()
	{
		return $this->_Description;
	}

	public function getRecurrenceID()
	{
		return $this->_RecurrenceID;
	}

	public function getRecurrenceRule()
	{
		return $this->_RecurrenceRule;
	}

	public function getRecurrenceException()
	{
		return $this->_RecurrenceException;
	}

	public function getBook_status()
	{
		return $this->_book_status;
	}

	public function getAvailable_status()
	{
		return $this->_available_status;
	}
}
?>