<?php

class Hotels_Model_HotelsproDataMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_HotelsproData');
        }
        return $this->_dbTable;
    }

    public function save(array $arrayData = NULL) {
//        print_r($arrayData);
        $Data = $arrayData['datas'];
        if ($Data['id'] == NULL OR $Data['id'] == 0) {
            try {
                unset($data['id']);

                $data = array(
                    'invoice_id' => $Data['invoice_id'],
                    'tracking_id' => $Data['tracking_id'],
                    'response_id' => $Data['response_id'],
                    'data_object' => $Data['data_object']
                );
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            try {
                $data = array(
                    'invoice_id' => $Data['invoice_id'],
                    'tracking_id' => $Data['tracking_id'],
                    'response_id' => $Data['response_id'],
                    'data_object' => $Data['data_object']		
                );
                // Start the Update process
                 try {
                    $this->getDbTable()->update($data, array('id = ?' => $id));
                    $result = array('status' => 'ok', 'id' => $id);
                } catch (Exception $e) {
                    $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
                }
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>