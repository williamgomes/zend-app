<?php
class Hotels_Model_SchedulerMapper
{
	protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
		{
            $dbTable = new $dbTable();
        }

        if (!$dbTable instanceof Zend_Db_Table_Abstract)
		{
            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) 
		{
            $this->setDbTable('Hotels_Model_DbTable_Scheduler');
        }
        return $this->_dbTable;
    }

	public function delete(Hotels_Model_Scheduler $obj)
	{
		try
		{
			$id = $obj->getId();
			if(!empty($id))
			{
				$this->getDbTable()->delete(array('id = ?' => $id));
			}
			$result = array('status' => 'ok');
		}
		catch(Exception $e)
		{
			$result = array('status' => 'err' , 'msg' => $e->getMessage());
		}
		return $result;
	}
	
	public function save(Hotels_Model_Scheduler $obj)
	{
		if ((null === ($id = $obj->getId())) || empty($id))
		{
			if(!empty($data['id']) && !empty($data))
			{
				unset($data['id']);
			}

			$data = array(	
				'room_id'					=>	$obj->getRoom_id(),			
				'Title' 					=>	$obj->getTitle(),
				'Start' 					=>	$obj->getStart(),
				'End' 						=>	$obj->getEnd(),
				'StartTimezone' 			=> 	$obj->getStartTimezone(),
				'EndTimezone' 				=>	$obj->getEndTimezone(),
				'IsAllDay' 					=>	$obj->getIsAllDay(),
				'Description' 				=>	$obj->getDescription(),
				'RecurrenceID' 				=> 	$obj->getRecurrenceID(),
				'RecurrenceRule' 			=>	$obj->getRecurrenceRule(),
				'RecurrenceException' 		=>	$obj->getRecurrenceException(),
				'book_status' 				=>	$obj->getBook_status()
			);


			try
			{
				$last_id = $this->getDbTable()->insert($data);
				$result = array('status' => 'ok' ,'id' => $last_id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());
			}
		}
		else
		{
			
			$data = array(		
				'room_id'					=>	$obj->getRoom_id(),		
				'Title' 					=>	$obj->getTitle(),
				'Start' 					=>	$obj->getStart(),
				'End' 						=>	$obj->getEnd(),
				'StartTimezone' 			=> 	$obj->getStartTimezone(),
				'EndTimezone' 				=>	$obj->getEndTimezone(),
				'IsAllDay' 					=>	$obj->getIsAllDay(),
				'Description' 				=>	$obj->getDescription(),
				'RecurrenceID' 				=> 	$obj->getRecurrenceID(),
				'RecurrenceRule' 			=>	$obj->getRecurrenceRule(),
				'RecurrenceException' 		=>	$obj->getRecurrenceException(),
				'book_status' 				=>	$obj->getBook_status()
			);
			
			
			// Start the Update process
			try
			{
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
			}
		}
		
		return $result;
	}
}
?>