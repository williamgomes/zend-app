<?php
class Hotels_Model_ApiSettings
{
    protected $_id;
    protected $_api_id;
    protected $_setting;
    protected $_value;  
    public function __construct (array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    public function __set ($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Method '.$method);
			}
			$this->$method($value);
		}		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Method '.$method);
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveApiSetting()
		{
			try
			{
				$mapper  = new Hotels_Model_ApiSettingsMapper();
				$return = $mapper->save($this);
			}
			catch(Exception $e)
			{
				$return = array('status' => 'err', 'msg' => $e->getMessage());
			}
			return $return;
		}
		/**
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }
		/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }
		/**
     * @return the $_api_id
     */
    public function getApi_id ()
    {
        return $this->_api_id;
    }
		/**
     * @param field_type $_api_id
     */
    public function setApi_id ($_api_id)
    {
        $this->_api_id = $_api_id;
    }
		/**
     * @return the $_setting
     */
    public function getSetting ()
    {
        return $this->_setting;
    }
		/**
     * @param field_type $_setting
     */
    public function setSetting ($_setting)
    {
        $this->_setting = $_setting;
    }
		/**
     * @return the $_value
     */
    public function getValue ()
    {
        return $this->_value;
    } 
		/**
     * @param field_type $_value
     */
    public function setValue ($_value)
    {
        $this->_value = $_value;
    }
		
		
		
			
}
?>