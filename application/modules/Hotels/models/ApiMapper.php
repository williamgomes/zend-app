<?php

class Hotels_Model_ApiMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Api');
        }
        return $this->_dbTable;
    }

    public function save(Hotels_Model_Api $obj) {

        if ((null === ($id = $obj->getId())) || empty($id)) {
            try {
                unset($data['id']);

                $data = array(
                    'api_name' => $obj->getApi_name(),
                    'api_username' => $obj->getApi_username(),
                    'api_title' => $obj->getApi_title(),
                    'api_password' => $obj->getApi_password(),
                    'api_key' => $obj->getApi_key(),
                    'api_base_uri' => $obj->getApi_base_uri(),
                    'api_ts_code' => $obj->getApi_ts_code()
                );
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            try {
                $data = array(
                    'api_name' => $obj->getApi_name(),
                    'api_username' => $obj->getApi_username(),
                    'api_title' => $obj->getApi_title(),
                    'api_password' => $obj->getApi_password(),
                    'api_key' => $obj->getApi_key(),
                    'api_base_uri' => $obj->getApi_base_uri(),
                    'api_ts_code' => $obj->getApi_ts_code()
                );

                // Start the Update process					
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>