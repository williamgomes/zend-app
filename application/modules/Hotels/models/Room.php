<?php
class Hotels_Model_Room
{
	protected $_id;
	protected $_entry_by;
	protected $_room_type_id;
	protected $_hotel_id;
	protected $_room_name;
	protected $_room_condition;		
	protected $_room_desc;
	
		
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveRoom()
		{
			$mapper  = new Hotels_Model_RoomMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setRoom_type_id($text)
		{
			$this->_room_type_id = $text;
			return $this;
		}
		
		public function setHotel_id($text)
		{
			$this->_hotel_id = $text;
			return $this;
		}
		
		public function setRoom_name($text)
		{
			$this->_room_name = $text;
			return $this;
		}
		
		public function setRoom_condition($text)
		{
			$this->_room_condition = $text;
			return $this;
		}
		
		public function setRoom_desc($text)
		{
			$this->_room_desc = $text;
			return $this;
		}
		
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getRoom_type_id()
		{
			return $this->_room_type_id;
		}
		
		public function getHotel_id()
		{
			return $this->_hotel_id;
		}
		
		public function getRoom_name()
		{
			return $this->_room_name;
		}
		
		public function getRoom_condition()
		{
			return $this->_room_condition;
		}
		
		public function getRoom_desc()
		{
			return $this->_room_desc;
		}		
}
?>