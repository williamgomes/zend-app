<?php
class Hotels_Model_Price
{
	protected $_id;
	protected $_entry_by;
	protected $_room_type_id;
	protected $_plan_name;
	protected $_minimum_stay;		
	protected $_minimum_stay_night;
	protected $_price_per_night;		
	protected $_price_after_discount;
	protected $_room_tax;		
	protected $_room_tax_type;
	protected $_seasonal_price_per_night;		
	protected $_season_start_date;
	protected $_season_end_date;		
	protected $_stop_temporary;
	
		
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth brand');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function savePrice()
		{
			$mapper  = new Hotels_Model_PriceMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setRoom_type_id($text)
		{
			if(is_array($text))
			{
				$text_arr = array_filter($text);
				$this->_room_type_id = implode(',', $text_arr);
			}
			else
			{
				$this->_room_type_id = $text;
			}
			return $this;
		}	
		
		
		public function setPlan_name($text)
		{
			$this->_plan_name = $text;
			return $this;
		}
		
		public function setMinimum_stay($text)
		{
			$this->_minimum_stay = $text;
			return $this;
		}
		
		public function setMinimum_stay_night($text)
		{
			$this->_minimum_stay_night = $text;
			return $this;
		}
		
		public function setPrice_per_night($text)
		{
			$this->_price_per_night = $text;
			return $this;
		}
		
		public function setPrice_after_discount($text)
		{
			$this->_price_after_discount = $text;
			return $this;
		}
		
		public function setRoom_tax($text)
		{
			$this->_room_tax = $text;
			return $this;
		}
		
		public function setRoom_tax_type($text)
		{
			$this->_room_tax_type = $text;
			return $this;
		}
		
		public function setSeasonal_price_per_night($text)
		{
			$this->_seasonal_price_per_night = $text;
			return $this;
		}
		
		public function setSeason_start_date($text)
		{
			$this->_season_start_date = $text;
			return $this;
		}
		
		public function setSeason_end_date($text)
		{
			$this->_season_end_date = $text;
			return $this;
		}
		
		public function setStop_temporary($text)
		{
			$this->_stop_temporary = $text;
			return $this;
		}
		
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		
		public function getEntry_by()
		{     
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}      
			return $this->_entry_by;
		}
		
		public function getRoom_type_id()
		{
			return $this->_room_type_id;
		}		
		
		
		public function getPlan_name()
		{
			return $this->_plan_name;
		}
		
		public function getMinimum_stay()
		{
			return $this->_minimum_stay;
		}
		
		public function getMinimum_stay_night()
		{
			return $this->_minimum_stay_night;
		}	
		
		public function getPrice_per_night()
		{
			return $this->_price_per_night;
		}
		
		public function getPrice_after_discount()
		{
			return $this->_price_after_discount;
		}
		
		public function getRoom_tax()
		{
			return $this->_room_tax;
		}
		
		public function getRoom_tax_type()
		{
			return $this->_room_tax_type;
		}
		
		public function getSeasonal_price_per_night()
		{
			return $this->_seasonal_price_per_night;
		}
		
		public function getSeason_start_date()
		{
			return $this->_season_start_date;
		}
		
		public function getSeason_end_date()
		{
			return $this->_season_end_date;
		}
		
		public function getStop_temporary()
		{
			return $this->_stop_temporary;
		}	
}
?>