<?php
class Hotels_Model_AreasMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Area');
        }
        return $this->_dbTable;
    }
	
	public function save(Hotels_Model_Areas $obj)
	{			 
		if ((null === ($id = $obj->getCity_id())) || empty($id)) 
		{
			unset($data['city_id']);
			
			$data = array(					
				'state_id' 			=> 	$obj->getState_id(),
				'city' 			=>	$obj->getCity()										
			);
			try 
			{
				$last_id = $this->getDbTable()->insert($data);	
				$result = array('status' => 'ok' ,'id' => $last_id);	
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
			}
						
		} 
		else 
		{				
			$data = array(					
				'state_id' 			=> 	$obj->getState_id(),
				'city' 			=>	$obj->getCity()						
			);				
			// Start the Update process
			try 
			{	
				$this->getDbTable()->update($data, array('city_id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);	
			} 
			catch (Exception $e) 
			{
				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
			}				
		}
		return $result;
	}
}
?>