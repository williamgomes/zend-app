<?php
class Hotels_Model_ExpediaMapper
{
    protected $_dbTable;

    public function setDbTable ($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }

        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable ()   
	{
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Expedia');
        }
        return $this->_dbTable;
    }

    public function saveReservationInfo (Hotels_Model_Expedia $obj)
    {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);
            $data = array('itineraryId' => $obj->getItineraryId() ,
                    'arrivalDate' => $obj->getArrivalDate(),
                    'departureDate' => $obj->getDepartureDate(),
                    'hotelId' => $obj->getHotelId(),
                    'user_id' => $obj->getUser_id(),
                    'confirmationNumbers' => $obj->getConfirmationNumbers(),
                    'email' => $obj->getEmail(),
                    'rateCurrencyCode' => $obj->getRateCurrencyCode(),
                    'price' => $obj->getPrice(),
                    'resultSetObject' => $obj->getResultSetObject(),
                    'cancel' => '0'
                     );
            try {
                $id_res = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $id_res);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id_res,
                        'msg' => $e->getMessage());
            }
        } else {
             $data = array('itineraryId' => $obj->getItineraryId() ,
                    'arrivalDate' => $obj->getArrivalDate(),
                    'departureDate' => $obj->getDepartureDate(),
                    'hotelId' => $obj->getHotelId(),
                    'user_id' => $obj->getUser_id(),
                    'confirmationNumbers' => $obj->getConfirmationNumbers(),
                    'email' => $obj->getEmail(),
                    'rateCurrencyCode' => $obj->getRateCurrencyCode(),
                    'price' => $obj->getPrice(),
                    'resultSetObject' => $obj->getResultSetObject(),
                     'cancel' => '0'                     
					 );

            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id,
                        'msg' => $e->getMessage());
            }
        }        
		return $result;    
	}

	public function fetchAll($pageNumber, $approve = null, $search_params = null, $tableColumns = array('userChecking' => true))
    {		
        $resultSet = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns); 		     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }

	public function cancelItinerary($id)
    {
		if(empty ($id)){
			 $result = array('status' => 'err', 'id' => $id,
                        'msg' => null);	
		}
		else {		

            try {	
                $this->getDbTable()->update(array('cancel' => '1'), array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
		}
		return  $result;
    }
	
	public function deleteItinerary($id)
    {
		if(empty ($id))
		{
			 $result = array('status' => 'err', 'id' => $id,  'msg' => null);
		}
		else 
		{
            try {	

                $this->getDbTable()->delete(array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } 
			catch (Exception $e) 
			{                
				$result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
		}

		return  $result;
    }
}