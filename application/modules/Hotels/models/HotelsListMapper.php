<?php
class Hotels_Model_HotelsListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Hotels_Model_DbTable_Hotels');
        }
        return $this->_dbTable;
    } 
	
	public function hotels_truncate($phrase,$start_words, $max_words, $char = false, $charset = 'UTF-8')
	{
		if($char)
		{
			if(mb_strlen($phrase, $charset) > $length) {
				  $dot = (strlen($phrase) > ($max_words - $start_words)) ? '...' : '';
				  $phrase = mb_substr($phrase, $start_words, $max_words, $charset) . $dot;
				}
		}
		else
		{
		   $phrase_array = explode(' ',$phrase);
		   if(count($phrase_array) > $max_words && $max_words > 0)
			  $phrase = implode(' ',array_slice($phrase_array, $start_words, $max_words)).'...';  
		}
	   return $phrase;
	}
	
	public function getNights($sStartDate, $sEndDate)
	{		
		$sStartDate =strtotime($sStartDate);
		$sEndDate = strtotime($sEndDate);
		
		$datediff = $sEndDate - $sStartDate;
		$aDays	= floor($datediff/(60*60*24));
		
		return $aDays;
	}  
	
	public function searchPrice($room_type_info_entry, $postValue, $tableColumns)
	{
		
		if(!empty($postValue) && ($postValue['search_price-gte'] != '') && ($postValue['search_price-lte'] != ''))
		{			 
			$room_type_info_entry['search_price']  =  (!empty($room_type_info_entry['price_after_discount'])) ? $room_type_info_entry['price_after_discount'] : $room_type_info_entry['price_per_night'];
			
			if($tableColumns['controller_obj']['view']->price($room_type_info_entry['search_price']) >= $postValue['search_price-gte'] && $tableColumns['controller_obj']['view']->price($room_type_info_entry['search_price']) <= $postValue['search_price-lte'])
			{
				$return = true;
			}
			else
			{
				$return = false;	
			}
		}
		else
		{
			$return = true;
		}
		return $return;
	}	
	
	public function fetchAll($pageNumber, $approve = null, $search_params = null, $tableColumns = array('userChecking' => true))
    {
		$list_datas = $this->getDbTable()->getListInfo($approve, $search_params, $tableColumns); 		     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
			
		if($tableColumns['controller_obj'] && is_array($tableColumns['controller_obj']))
		{
			$view_datas = array('data_result' => array(), 'total' => 0);
			if($list_datas)
			{
				$today = date('Y-m-d');							
				$key = 0;							
				foreach($list_datas as $entry)
				{
						$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
						$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
						$entry_arr['id_format']=  $tableColumns['controller_obj']['view']->numbers($entry_arr['id']);
						$entry_arr['hotels_name_format'] = $this->hotels_truncate( $entry_arr['hotels_name'], 0, 4);
						$entry_arr['hotels_phone_format'] = $tableColumns['controller_obj']['view']->numbers($entry_arr['hotels_phone']);
						$entry_arr['feature_distance_from_airport_format'] = $tableColumns['controller_obj']['view']->numbers($entry_arr['feature_distance_from_airport']);
						$entry_arr['hotels_desc']		=	strip_tags( $entry_arr['hotels_desc']);
						
						$entry_arr['hotels_desc_format'] = $tableColumns['controller_obj']['view']->escape(strip_tags( $entry_arr['hotels_desc']));
						$entry_arr['hotels_date_lang_format']=  $tableColumns['controller_obj']['view']->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
						$entry_arr['hotels_date_format']=  $tableColumns['controller_obj']['view']->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
						$img_thumb_arr = explode(',',$entry_arr['hotels_image']);	
						$hotels_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);							
						$entry_arr['hotels_image_format'] = ($tableColumns['controller_obj']['view']->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$tableColumns['controller_obj']['view']->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
						$entry_arr['hotels_image_no_format'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_hotels_photo_no', $tableColumns['controller_obj']['view']->numbers($hotels_image_no));
						//$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['entry_by'] || $this->_auth_obj->user_id == $entry_arr['hotels_agent']) ? true : false;						
						$entry_arr['review_no']	=  (!empty($entry_arr['review_id'])) ? $tableColumns['controller_obj']['review_helper']->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';					
						$entry_arr['review_no_format']	=  (!empty($entry_arr['review_no'])) ? $tableColumns['controller_obj']['translator']->translator('common_review_no', $tableColumns['controller_obj']['view']->numbers($entry_arr['review_no'])) : $tableColumns['controller_obj']['translator']->translator('common_review_no', $tableColumns['controller_obj']['view']->numbers(0));
						
						$entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="'.$entry_arr['file_thumb_width'].'"' : '';
						$entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="'.$entry_arr['file_thumb_height'].'"' : '';
						$hotel_stars = '';
						for($i = 1; $i < $tableColumns['controller_obj']['maximum_stars_digit']; $i++)
						{
							$hotel_stars .= ($i <= $entry_arr['hotels_grade']) ? '<img src="application/layouts/scripts/'.$tableColumns['controller_obj']['view']->front_template['theme_folder'].'/images/mod_hotel_img/star-active.png" />' : '<img src="application/layouts/scripts/'.$tableColumns['controller_obj']['view']->front_template['theme_folder'].'/images/mod_hotel_img/star-inactive.png" />';
							
						}
						$entry_arr['hotel_stars_format']		= 	$hotel_stars;
						$entry_arr['vote_format']				= 	$tableColumns['controller_obj']['vote']->getButton($entry_arr['id'] , $tableColumns['controller_obj']['view']->escape($entry_arr['hotels_name']));								
						
						$entry_arr['total_nights']				=	$this->getNights($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_IN], $tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_OUT]);
						$entry_arr['total_nights']				=	(empty($entry_arr['total_nights'])) ? 1 : $entry_arr['total_nights'];
						$entry_arr['total_nights_format']		=	$tableColumns['controller_obj']['view']->numbers($entry_arr['total_nights']);
						$entry_arr['total_nights_format_lang']	=	$tableColumns['controller_obj']['translator']->translator('total_nights_format_lang', $entry_arr['total_nights_format']);
						$entry_arr['room_type_info']			=	null; 
						$entry_arr['adult_info']				=	$tableColumns['controller_obj']['postValue']['search_adult'];
						$entry_arr['child_info']				=	($tableColumns['controller_obj']['postValue']['search_child']) ? $tableColumns['controller_obj']['postValue']['search_child'] : 0;
						
						//Room Type Start
						if(!empty($entry_arr['room_type_id']))
						{	
							try
							{	
								$room_type_search_params['filter']['filters'][0] = array('field' => 'room_type_id', 'operator' => 'eq', 'value' => $entry_arr['room_type_id']);
								$room_type_search_params['filter']['logic'] = ($room_type_search_params['filter']['logic']) ? $room_type_search_params['filter']['logic'] : 'and';	
								$room_type_search_params['filter']['filters'][1] = array('field' => 'stop_temporary', 'operator' => 'eq', 'value' => '0');
								if(!empty($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_OUT]))
								{
									$room_type_search_params['filter']['filters'][2]= array( 	'logic' => 'OR', 
																					'filters' => array(																			  
																									 array(
																											'logic' => 'AND',
																											'filters' => array( 
																																array('field' => 'minimum_stay_night', 'operator' => 'lte', 'value' => $this->getNights($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_IN], $tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_OUT])),
																																array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '2')
																															)
																										),
																									 array('field' => 'minimum_stay', 'operator' => 'eq', 'value' => '1')
																									)
																				 );	
								}
								$room_type_info = $tableColumns['controller_obj']['room_type_db']->getListInfo('1', $room_type_search_params , false);
								//$view_datas[$view_datas_key]['room_type_info'] = $room_type_info;
								
								if($room_type_info)
								{
									$room_type_info_key = 0;				
									foreach($room_type_info as $room_type_info_entry)
									{	
										if($this->searchPrice($room_type_info_entry, $tableColumns['controller_obj']['postValue'], $tableColumns))
										{					
											$room_type_info_entry_arr = (!is_array($room_type_info_entry)) ? $room_type_info_entry->toArray() : $room_type_info_entry;
											$room_type_info_entry_arr = (is_array($room_type_info_entry_arr)) ? array_map('stripslashes', $room_type_info_entry_arr) : stripslashes($room_type_info_entry_arr);
											$room_type_info_entry_arr['publish_status_room_type_name'] = str_replace('_', '-', $room_type_info_entry_arr['room_type']);	
											$img_file_arr = explode(',',$room_type_info_entry_arr['room_type_image']);
											$room_type_info_entry_arr['room_type_image_format'] = ($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['primary_image'])) ? 'data/frontImages/hotels/room_type_image/'.$tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['primary_image']) : 'data/frontImages/hotels/room_type_image/'.$img_file_arr[0];
											$room_type_info_entry_arr['id_format']	=	$tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['id']));
											$room_type_info_entry_arr['max_people_format']	=	$tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['max_people']));
											$room_type_info_entry_arr['max_people_lang_format']	=	$tableColumns['controller_obj']['translator']->translator('hotels_front_page_list_title_people', $tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['max_people'])));
											$room_type_info_entry_arr['room_num_format']	=	$tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['room_num']));
											$room_type_info_entry_arr['basic_price']		=	$room_type_info_entry_arr['price_per_night'];
											$room_type_info_entry_arr['descount_price']		=	(!empty($room_type_info_entry_arr['price_after_discount'])) ? $room_type_info_entry_arr['price_after_discount'] : $room_type_info_entry_arr['price_per_night'];
											$room_type_info_entry_arr['basic_price_format']	=	$tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['basic_price']));
											$room_type_info_entry_arr['descount_price_format']	=	$tableColumns['controller_obj']['view']->numbers($tableColumns['controller_obj']['view']->price($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['descount_price'])));
											$room_type_info_entry_arr['form_data_view_title_format']	=	$tableColumns['controller_obj']['translator']->translator('form_data_view_title', $tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['room_type']), 'Members');
											$save = 0;
											if(!empty($room_type_info_entry_arr['basic_price']) && !empty($room_type_info_entry_arr['descount_price']) && $room_type_info_entry_arr['descount_price'] < $room_type_info_entry_arr['basic_price'])
											{
												$save = ( ($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['basic_price']) - $tableColumns['controller_obj']['view']->price($tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['descount_price']))) / $tableColumns['controller_obj']['view']->escape($room_type_info_entry_arr['basic_price']) ) * 100;
												$save = round($save);
											}
											$room_type_info_entry_arr['save_price'] = $save;
											$room_type_info_entry_arr['save_format'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_list_save_money', $tableColumns['controller_obj']['view']->numbers($save));
											$booked = ($tableColumns['controller_obj']['room_db']->isJustBooked($room_type_info_entry_arr['id'])) ? true : false;
											$room_type_info_entry_arr['just_booked'] = $booked;
											
											//Room Info Start 
											if(!empty($tableColumns['controller_obj']['postValue']) && !empty($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_IN]) && !empty($tableColumns['controller_obj']['postValue'][Eicra_File_Constants::HOTELS_CHECK_OUT]))
											{															
												$room_info = $tableColumns['controller_obj']['room_db']->getAvailableRooms($room_type_info_entry_arr['id'], $tableColumns['controller_obj']['postValue']);																	
											}
											else
											{
												 $room_info = $tableColumns['controller_obj']['room_db']->getAvailableRooms($room_type_info_entry_arr['id'], array(Eicra_File_Constants::HOTELS_CHECK_IN => $today, Eicra_File_Constants::HOTELS_CHECK_OUT => $today));
											}
											$room_type_info_entry_arr['room_info'] = $room_info;
											$room_type_info_entry_arr['room_info_available_room']	=	count($room_info);
											if($tableColumns['controller_obj']['device']->isMobile())
											{
												$room_type_info_entry_arr['room_info_count_format'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_room_available_type_last', count($room_info));
												$room_type_info_entry_arr['room_info_count_format1'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_room_available_type_left', count($room_info));
											}
											else
											{
												$room_type_info_entry_arr['room_info_count_format'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_list_available_type_llast', $tableColumns['controller_obj']['view']->numbers($room_type_info_entry_arr['room_info_available_room']));
												$room_type_info_entry_arr['room_info_count_format1'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_list_available_type_left', $tableColumns['controller_obj']['view']->numbers($room_type_info_entry_arr['room_info_available_room']));
											}
											$room_type_info_entry_arr['room_info_count_format2'] = $tableColumns['controller_obj']['translator']->translator('hotels_front_page_list_available_type_last_desc', $tableColumns['controller_obj']['currency']->getShortName().'&nbsp;'.$room_type_info_entry_arr['descount_price_format']);
											$room_type_info_entry_arr['room_info_count_format3'] = $tableColumns['controller_obj']['translator']->translator('hotels_search_sugges_room_left', $tableColumns['controller_obj']['view']->numbers($room_type_info_entry_arr['room_info_available_room']));
											$room_type_info_entry_arr['show_room_type'] = ($tableColumns['controller_obj']['preferences_data']['show_unavailable_room_type'] == 'NO' && empty($room_type_info_entry_arr['room_info_available_room'])) ? false : true;
											//Room Info End
											if($room_type_info_entry_arr['show_room_type'] == true)
											{
												$entry_arr['room_type_info'][$room_type_info_key]	=	$room_type_info_entry_arr;	
												$room_type_info_key++;	
											}
										}
									}
								}	
							}
							catch(Exception $e)
							{
																
							}
						}								
						//Room Type End
						if(count($entry_arr['room_type_info']) > 0)
						{					
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;	
						}
				}				
			}
			$paginator = Zend_Paginator::factory($view_datas['data_result']);	
		}
		else
		{        
			$paginator = Zend_Paginator::factory($list_datas);		
		}
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
}
?>