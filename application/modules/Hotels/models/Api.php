<?php

class Hotels_Model_Api {

    protected $_id;
    protected $_api_name;
    protected $_api_title;
    protected $_api_username;
    protected $_api_password;
    protected $_api_key;
    protected $_api_base_uri;
    protected $_api_ts_code;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Method ' . $method);
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Method ' . $method);
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function saveApi() {
        try {
            $mapper = new Hotels_Model_ApiMapper();
            $return = $mapper->save($this);
        } catch (Exception $e) {
            $return = array('status' => 'err', 'msg' => $e->getMessage());
        }
        return $return;
    }

    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setApi_name($text) {
        $this->_api_name = $text;
        return $this;
    }

    public function setApi_title($text) {
        $this->_api_title = $text;
        return $this;
    }

    public function setApi_username($text) {
        $this->_api_username = $text;
        return $this;
    }

    public function setApi_password($text) {
        $this->_api_password = $text;
        return $this;
    }

    public function setApi_key($text) {
        $this->_api_key = $text;
        return $this;
    }

    public function setApi_base_uri($text) {
        $this->_api_base_uri = $text;
        return $this;
    }

    public function setApi_ts_code($text) {
        $this->_api_ts_code = $text;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getApi_name() {
        return $this->_api_name;
    }

    public function getApi_title() {
        return $this->_api_title;
    }

    public function getApi_username() {
        return $this->_api_username;
    }

    public function getApi_password() {
        return $this->_api_password;
    }

    public function getApi_key() {
        return $this->_api_key;
    }

    public function getApi_base_uri() {
        return $this->_api_base_uri;
    }

    public function getApi_ts_code() {
        return $this->_api_ts_code;
    }

}

?>