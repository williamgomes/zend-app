<?php
class Hotels_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initAutoload() 
	{		
		//THIS IS NESESSARY FOR NEW FOLDER PATH
		$this->_resourceLoader->addResourceTypes(array( 
		'controllerhelper' => array(
                'namespace' => 'Controller_Helper',
                'path'      => 'controllers/helpers',
            ),
		));		
	}
}

