<?php

class Hotels_Service_Invoice {

    protected $_invoice_obj;

    public function __construct($invoice_obj = null) {
        $this->_invoice_obj = $invoice_obj;
        $this->_translator = Zend_Registry::get('translator');
        $this->_api_settings = new Hotels_Model_HotelsproApiModel();
    }

    public function generateDateArray($startDate, $endDate) {
        $date_array = array();

        for ($i = $startDate, $j = 0; $i <= $endDate; $i = date("Y-m-d", strtotime("+1 day", strtotime($i))), $j++) {
            $date_array[$j] = $i;
        }
        return $date_array;
    }

    public function createItinerary() {
        if ($this->_invoice_obj['invoice_items']) {
            try {
                $hotel_id_arr = array();
                $room_type_id_arr = array();
                foreach ($this->_invoice_obj['invoice_items'] as $key => $item_arr) {
                    if ($item_arr['object_value']) {
                        $obj_info = Zend_Json::decode($item_arr['object_value']);
                        $hotel_id_arr[$key] = $obj_info['hotel_id'][0];
                        $room_type_id_arr[$key] = $obj_info['room_type_id'];
                    }
                }
                $hotel_id_string = implode(',', $hotel_id_arr);

                $hotels_db = new Hotels_Model_DbTable_Hotels();
                $search_params['filter']['filters'][0] = array('field' => 'hotel_id_string', 'operator' => 'eq', 'value' => $hotel_id_string);

                $list_info = $hotels_db->getListInfo(null, $search_params, array('userChecking' => false));
                if ($list_info) {
                    $invoice_info = array();
                    $key_id = 0;
                    $key_id1 = 0;
                    foreach ($list_info as $list_info_key => $info_arr) {
                        if ($info_arr['billing_user_places_order_email_enable'] == 'yes' && !empty($info_arr['billing_user_places_order_email_address'])) {
                            $invoice_info['billing_user_places_order_email_address'][$key_id1] = $info_arr['billing_user_places_order_email_address'];
                            $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                            $key_id1++;
                        }
                        if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                            $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                            $invoice_info['item_details'][$key_id] = '<table><tr><td colspan="2" style="height:8px"></td></tr>
																				 <tr>
																					<td colspan="2" id="invoiceitemsrow" style="font-size:17px; margin:10px auto 2px auto; padding:10px 0px 10px 5px; background-color:#FFC; border-top:1px solid #FC0; border-bottom:1px solid #FC0;">
																					' . stripslashes($info_arr['hotels_name']) . '
																					</td>
																				</tr></table>';
                            $key_id++;
                        }
                    }

                    $email_class = new Invoice_View_Helper_Email();

                    //Email Template Array Start
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	 	= 			$invoice_info['billing_user_places_order_email_address'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_places_order_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_places_order_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_places_order_email_address'] );
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TO];
                    $email_data_arr[Eicra_File_Constants::PAY_TO] = $this->_invoice_obj[Eicra_File_Constants::PAY_TO];
                    $email_data_arr[Eicra_File_Constants::INVOICE_LOGO] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_LOGO];
                    $email_data_arr[Eicra_File_Constants::INVOICE_CREATE_DATE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_CREATE_DATE];
                    $email_data_arr[Eicra_File_Constants::INVOICE_DATE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_DATE];
                    $email_data_arr[Eicra_File_Constants::INVOICE_TABLE] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TABLE];

                    $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_PAYMENT_STATUS];
                    $email_data_arr['title'] = $this->_invoice_obj['title'];
                    $email_data_arr['firstName'] = $this->_invoice_obj['firstName'];
                    $email_data_arr['lastName'] = $this->_invoice_obj['lastName'];
                    $email_data_arr[Eicra_File_Constants::INVOICE_ID] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_ID];
                    $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $this->_invoice_obj[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT];
                    //Email Template Array End
                    //Initialize Email Template
                    $template_id_field = 'default_template_id';
                    $settings_db = new Invoice_Model_DbTable_Setting();
                    $template_info = $settings_db->getInfoByModule($this->_invoice_obj[Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                    $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("hotels_invoice_template_id");

                    $email_class->sendCommonMail($email_data_arr);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function deleteItinerary() {
        
    }

    public function paidItinerary() {
//        print_r($this->_invoice_obj);
        try {
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $roomCalendar_db = new Hotels_Model_DbTable_Scheduler();
            $room_db = new Hotels_Model_DbTable_Room();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            $hotel_id_arr = array();
            foreach ($invoice_items_arr as $invoice_items_arr_key => $invoice_items) {
                $object_value = Zend_Json::decode($invoice_items['object_value']);
                $check_in = $object_value[Eicra_File_Constants::HOTELS_CHECK_IN];
                $check_in .= ' 06:00:00';
                $check_out = $object_value[Eicra_File_Constants::HOTELS_CHECK_OUT];
                $check_out .= ' 23:59:00';
                //$date_array = $this->generateDateArray($check_in, $check_out);
                $room_type_id_arr = $object_value['room_type_id'];

                if ($invoice_items['object_value']) {
                    $hotel_id_arr[$invoice_items_arr_key] = $object_value['hotel_id'][0];
                }
                if ($room_type_id_arr) {
                    foreach ($room_type_id_arr as $room_type_id_arr_key => $room_type_id_value) {
                        if (!empty($object_value['apartments_no' . $room_type_id_arr_key]) && !empty($object_value['apartments_no' . $room_type_id_arr_key][0])) {
                            foreach ($object_value['apartments_no' . $room_type_id_arr_key] as $apartments_no_key => $room_id) {
                                /* $res_value = '';
                                  $res_value_arr = array();
                                  $roomCalendar_info = $roomCalendar_db->getCalendarInfo($room_id);
                                  if($roomCalendar_info)
                                  {
                                  foreach($roomCalendar_info as $key => $value_arr)
                                  {
                                  if(in_array($value_arr['calendar_date'], $date_array))
                                  {
                                  $res_value_arr[$key] = $value_arr['calendar_date'].';;2;;'.$value_arr['room_price'];
                                  }
                                  else
                                  {
                                  $res_value_arr[$key] = $value_arr['calendar_date'].';;'.$value_arr['room_status'].';;'.$value_arr['room_price'];
                                  }
                                  }
                                  $res_value = implode(',', $res_value_arr);
                                  $roomCalendar_db->updateCalendar($room_id,$res_value);
                                  } */
                                $room_info = $room_db->getRoomInfo($room_id);
                                $data_arr = array(
                                    'room_id' => $room_id,
                                    'Title' => ($room_info) ? $room_info['room_name'] : '',
                                    'Start' => $check_in,
                                    'End' => $check_out,
                                    'IsAllDay' => '1',
                                    'book_status' => '2'
                                );
                                $roomCalendar_db->updateScheduler($room_id, $data_arr);
                            }
                        }
                    }
                }
            }
            // Extra START
            $email_class = new Invoice_View_Helper_Email();
            $invoice_later = $email_class->generateInvoiceLater($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            if ($invoice_later['status'] == 'ok') {
                $email_data_arr = $invoice_later['invoice_arr'];

                $hotel_id_string = implode(',', $hotel_id_arr);

                $hotels_db = new Hotels_Model_DbTable_Hotels();
                $search_params['filter']['filters'][0] = array('field' => 'hotel_id_string', 'operator' => 'eq', 'value' => $hotel_id_string);

                $list_info = $hotels_db->getListInfo(null, $search_params, array('userChecking' => false));
                if ($list_info) {
                    $invoice_info = array();
                    $key_id = 0;
                    $key_id1 = 0;
                    foreach ($list_info as $list_info_key => $info_arr) {
                        $full_name = stripslashes($info_arr['full_name']);
                        if ($info_arr['billing_user_pay_invoice_email_enable'] == 'yes' && !empty($info_arr['billing_user_pay_invoice_email_enable'])) {
                            $invoice_info['billing_user_pay_invoice_email_address'][$key_id1] = $info_arr['billing_user_pay_invoice_email_address'];
                            $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                            $key_id1++;
                        }
                        if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                            $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                            $key_id++;
                        }
                    }

                    $email_class = new Invoice_View_Helper_Email();

                    //Email Template Array Start
                    $global_conf = Zend_Registry::get('global_conf');
                    $currency = new Zend_Currency($global_conf['default_locale']);
                    $currencySymbol = $currency->getSymbol();
                    $currencyShortName = $currency->getShortName();
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	= 			$invoice_info['billing_user_pay_invoice_email_address'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_pay_invoice_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_pay_invoice_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_pay_invoice_email_address'] );

                    $email_data_arr['title'] = '';
                    $email_data_arr['firstName'] = $full_name;
                    $email_data_arr['lastName'] = '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_SUBJECT] = '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoice_later['invoice_info']['total'] + $invoice_later['invoice_info']['service_charge'] + $invoice_later['invoice_info']['tax']), 2, '.', ',') . ' ' . $currencyShortName;
                    $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = '<h1 style="border:1px solid #87D97B; background-color:#D5FFCE; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#006600">' . $this->_translator->translator("common_paid_language") . '</h1>';
                    //Email Template Array End
                    //Initialize Email Template
                    $template_id_field = 'paid_template_id';
                    $settings_db = new Invoice_Model_DbTable_Setting();
                    $template_info = $settings_db->getInfoByModule($invoice_later['invoice_info'][Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                    $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("hotels_invoice_template_id");

                    $email_class->sendCommonMail($email_data_arr);
                }
            }
            // Extra END		
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }

    public function unpaidItinerary() {
        try {
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $roomCalendar_db = new Hotels_Model_DbTable_Scheduler();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            foreach ($invoice_items_arr as $invoice_items_arr_key => $invoice_items) {
                $object_value = Zend_Json::decode($invoice_items['object_value']);
                $check_in = $object_value[Eicra_File_Constants::HOTELS_CHECK_IN];
                $check_in .= ' 06:00:00';
                $check_out = $object_value[Eicra_File_Constants::HOTELS_CHECK_OUT];
                $check_out .= ' 23:59:00';
                //$date_array = $this->generateDateArray($check_in, $check_out);
                $room_type_id_arr = $object_value['room_type_id'];
                foreach ($room_type_id_arr as $room_type_id_arr_key => $room_type_id_value) {
                    if (!empty($object_value['apartments_no' . $room_type_id_arr_key]) && !empty($object_value['apartments_no' . $room_type_id_arr_key][0])) {
                        foreach ($object_value['apartments_no' . $room_type_id_arr_key] as $apartments_no_key => $room_id) {
                            /* $res_value = '';
                              $res_value_arr = array();
                              $roomCalendar_info = $roomCalendar_db->getCalendarInfo($room_id);
                              if($roomCalendar_info)
                              {
                              foreach($roomCalendar_info as $key => $value_arr)
                              {
                              if(in_array($value_arr['calendar_date'], $date_array))
                              {
                              $res_value_arr[$key] = $value_arr['calendar_date'].';;1;;'.$value_arr['room_price'];
                              }
                              else
                              {
                              $res_value_arr[$key] = $value_arr['calendar_date'].';;'.$value_arr['room_status'].';;'.$value_arr['room_price'];
                              }
                              }
                              $res_value = implode(',', $res_value_arr);
                              $roomCalendar_db->updateCalendar($room_id,$res_value);
                              } */

                            $where = 'Start = "' . $check_in . '" AND End = "' . $check_out . '" AND room_id = "' . $room_id . '"';
                            $roomCalendar_db->delete($where);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }

    public function cancelItinerary() {
        try {
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $roomCalendar_db = new Hotels_Model_DbTable_Scheduler();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            $hotel_id_arr = array();
            foreach ($invoice_items_arr as $invoice_items_arr_key => $invoice_items) {
                $object_value = Zend_Json::decode($invoice_items['object_value']);
                $check_in = $object_value[Eicra_File_Constants::HOTELS_CHECK_IN];
                $check_in .= ' 06:00:00';
                $check_out = $object_value[Eicra_File_Constants::HOTELS_CHECK_OUT];
                $check_out .= ' 23:59:00';
                //$date_array = $this->generateDateArray($check_in, $check_out);

                if ($invoice_items['object_value']) {
                    $hotel_id_arr[$invoice_items_arr_key] = $object_value['hotel_id'][0];
                }

                $room_type_id_arr = $object_value['room_type_id'];
                foreach ($room_type_id_arr as $room_type_id_arr_key => $room_type_id_value) {
                    if (!empty($object_value['apartments_no' . $room_type_id_arr_key]) && !empty($object_value['apartments_no' . $room_type_id_arr_key][0])) {
                        foreach ($object_value['apartments_no' . $room_type_id_arr_key] as $apartments_no_key => $room_id) {
                            /* $res_value = '';
                              $res_value_arr = array();
                              $roomCalendar_info = $roomCalendar_db->getCalendarInfo($room_id);
                              if($roomCalendar_info)
                              {
                              foreach($roomCalendar_info as $key => $value_arr)
                              {
                              if(in_array($value_arr['calendar_date'], $date_array))
                              {
                              $res_value_arr[$key] = $value_arr['calendar_date'].';;1;;'.$value_arr['room_price'];
                              }
                              else
                              {
                              $res_value_arr[$key] = $value_arr['calendar_date'].';;'.$value_arr['room_status'].';;'.$value_arr['room_price'];
                              }
                              }
                              $res_value = implode(',', $res_value_arr);
                              $roomCalendar_db->updateCalendar($room_id,$res_value);
                              } */
                            $where = 'Start = "' . $check_in . '" AND End = "' . $check_out . '" AND room_id = "' . $room_id . '"';
                            $roomCalendar_db->delete($where);
                        }
                    }
                }
            }

            // Extra START
            $email_class = new Invoice_View_Helper_Email();
            $invoice_later = $email_class->generateInvoiceLater($this->_invoice_obj[Eicra_File_Constants::INVOICE_ID]);
            if ($invoice_later['status'] == 'ok') {
                $email_data_arr = $invoice_later['invoice_arr'];

                $hotel_id_string = implode(',', $hotel_id_arr);

                $hotels_db = new Hotels_Model_DbTable_Hotels();
                $search_params['filter']['filters'][0] = array('field' => 'hotel_id_string', 'operator' => 'eq', 'value' => $hotel_id_string);

                $list_info = $hotels_db->getListInfo(null, $search_params, array('userChecking' => false));
                if ($list_info) {
                    $invoice_info = array();
                    $key_id = 0;
                    $key_id1 = 0;
                    foreach ($list_info as $list_info_key => $info_arr) {
                        $full_name = stripslashes($info_arr['full_name']);
                        if ($info_arr['billing_user_cancel_order_email_enable'] == 'yes' && !empty($info_arr['billing_user_cancel_order_email_enable'])) {
                            $invoice_info['billing_user_cancel_order_email_address'][$key_id1] = $info_arr['billing_user_cancel_order_email_address'];
                            $invoice_info['billing_item_desc'][$key_id1] = ($info_arr['billing_item_desc']) ? stripslashes($info_arr['billing_item_desc']) : '';
                            $key_id1++;
                        }
                        if ($info_arr['billing_order_payment_email_enable'] == 'yes') {
                            $invoice_info['owner_email'][$key_id] = $info_arr['owner_email'];
                            $key_id++;
                        }
                    }

                    $email_class = new Invoice_View_Helper_Email();

                    //Email Template Array Start
                    $global_conf = Zend_Registry::get('global_conf');
                    $currency = new Zend_Currency($global_conf['default_locale']);
                    $currencySymbol = $currency->getSymbol();
                    $currencyShortName = $currency->getShortName();
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_EMAIL] = (is_array($invoice_info['owner_email'])) ? $invoice_info['owner_email'][0] : $invoice_info['owner_email'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TO_CC_EMAIL]	= 			$invoice_info['billing_user_pay_invoice_email_address'];
                    //$email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_EXTRA]	=			($invoice_info['billing_item_desc']) ? '<br />'.implode(' ', $invoice_info['billing_item_desc']) : '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_TO_BCC_EMAIL] = (is_array($invoice_info['billing_user_cancel_order_email_address']) && is_array($invoice_info['owner_email'])) ? array_merge($invoice_info['billing_user_cancel_order_email_address'], $invoice_info['owner_email']) : ( ($invoice_info['owner_email']) ? $invoice_info['owner_email'] : $invoice_info['billing_user_cancel_order_email_address'] );
                    $email_data_arr['title'] = '';
                    $email_data_arr['firstName'] = $full_name;
                    $email_data_arr['lastName'] = '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_SUBJECT] = '';
                    $email_data_arr[Eicra_File_Constants::INVOICE_TEMPLATE_AMOUNT] = $currencySymbol . ' ' . number_format(($invoice_later['invoice_info']['total'] + $invoice_later['invoice_info']['service_charge'] + $invoice_later['invoice_info']['tax']), 2, '.', ',') . ' ' . $currencyShortName;
                    $email_data_arr[Eicra_File_Constants::INVOICE_PAYMENT_STATUS] = '<h1 style="border:1px solid #E4D949; background-color:#FFFBCC; font-weight:bold; font-size:55px; text-align:left; text-transform:uppercase; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; padding:5px 20px; float:left; color:#FF9900">' . $this->_translator->translator("common_cancelled_language") . '</h1>';
                    //Email Template Array End
                    //Initialize Email Template
                    $template_id_field = 'cancel_template_id';
                    $settings_db = new Invoice_Model_DbTable_Setting();
                    $template_info = $settings_db->getInfoByModule($invoice_later['invoice_info'][Eicra_File_Constants::INVOICE_SERVICE_MODULE]);
                    $email_data_arr['letter_id'] = ($template_info && !empty($template_info[$template_id_field])) ? $template_info[$template_id_field] : $this->_translator->translator("hotels_invoice_template_id");

                    $email_class->sendCommonMail($email_data_arr);
                }
            }
            // Extra END
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }
    
    
    
    
    
    
    
    public function createHotelspro(){
        
    }
    
    
    public function updateHotelspro(){
        
    }
    
    
    public function cancelHotelspro(){
         $invoiceID = $this->_invoice_obj;
        try{
            $hotelsproDB = new Hotels_Model_DbTable_HotelsproData();
            $hotelsproDBInfo = $hotelsproDB->getListInfo($invoiceID['invoice_id']);
            
            if($hotelsproDBInfo[0]['tracking_id'] == "" OR !isset($hotelsproDBInfo[0]['tracking_id'])){
//                echo "The booking must be paid in order to cancel it.";
                
            } else {
                /*Calling API for cancelHotelBooking method*/
                $httpData = new Zend_Http_Client(null, array(
                    'timeout' => 60
                ));

                $httpData->setUri($this->_api_settings->getApiUri());
                $httpData->setParameterGet('method', 'cancelHotelBooking');
                $httpData->setParameterGet('apiKey', $this->_api_settings->getApiKey());
                $httpData->setParameterGet('trackingId', $hotelsproDBInfo[0]['tracking_id']);

                $httpData->setHeaders(array(
                    'Accept' => 'application/json',
                    'Accept-encoding' => 'gzip,deflate',
                    'X-Powered-By' => $siteName));
                $resultSet = $httpData->request('GET');

                $responseValue = $resultSet->getBody();
                $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);


                if($responseValue_arr['bookingStatus'] != 'Cancelled'){
                    echo $responseValue_arr[1];
                }
            }
           
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }
    
    
    public function paidHotelspro(){
        
        $invoiceID = $this->_invoice_obj;
        try{
            $invoice_item_db = new Invoice_Model_DbTable_InvoiceItems();
            $invoice_items_arr = $invoice_item_db->getInvoiceItems($invoiceID);
            $objectInfo = Zend_Json::decode($invoice_items_arr[0]['object_value']);
            
            /*Calling API for makeHotelBooking method*/
            $httpData = new Zend_Http_Client(null, array(
                'timeout' => 60
            ));

            $httpData->setUri($this->_api_settings->getApiUri());
            $httpData->setParameterGet('method', 'makeHotelBooking');
            $httpData->setParameterGet('apiKey', $this->_api_settings->getApiKey());
            $httpData->setParameterGet('processId', $objectInfo['processID']);
            $httpData->setParameterGet('agencyReferenceNumber', '');
            $httpData->setParameterGet('leadTravellerInfo[paxInfo][paxType]', 'Adult');
            $httpData->setParameterGet('leadTravellerInfo[nationality]', $objectInfo['clientCountry']);
            $countPax = (count($objectInfo['paxInfo']) / 3);
            for($i = 0; $i < $countPax; $i++){
                if($i == 0){
                    $httpData->setParameterGet('leadTravellerInfo[paxInfo][title]', $objectInfo['paxInfo']['title_' . $i]);
                    $httpData->setParameterGet('leadTravellerInfo[paxInfo][firstName]', $objectInfo['paxInfo']['fname_' . $i]);
                    $httpData->setParameterGet('leadTravellerInfo[paxInfo][lastName]', $objectInfo['paxInfo']['lname_' . $i]);
                } else {
                    $httpData->setParameterGet('otherTravellerInfo['. ($i-1) .'][title]', $objectInfo['paxInfo']['title_' . $i]);
                    $httpData->setParameterGet('otherTravellerInfo['. ($i-1) .'][firstName]', $objectInfo['paxInfo']['fname_' . $i]);
                    $httpData->setParameterGet('otherTravellerInfo['. ($i-1) .'][lastName]', $objectInfo['paxInfo']['lname_' . $i]);
                }
            }
            $httpData->setParameterGet('note', $objectInfo['specialNote']);
            $httpData->setParameterGet('hotelCode', $objectInfo['hotelID']);
            
            $httpData->setHeaders(array(
                'Accept' => 'application/json',
                'Accept-encoding' => 'gzip,deflate',
                'X-Powered-By' => $siteName));
            $resultSet = $httpData->request('GET');

            $responseValue = $resultSet->getBody();
            $responseValue_arr = Zend_Json::decode($responseValue, Zend_Json::TYPE_ARRAY);
            
            //creating HotelsproData object
            $arrayInvoice = array();
            $arrayInvoice['id'] = 0;
            $arrayInvoice['invoice_id'] = $invoiceID['invoice_id'];
            $arrayInvoice['tracking_id'] = $responseValue_arr['trackingId'];
            $arrayInvoice['response_id'] = $responseValue_arr['responseId'];
            $arrayInvoice['data_object'] = Zend_Json_Encoder::encode($responseValue_arr);
            
            $hotelsproData = new Hotels_Model_HotelsproData();
            $saveResponseData = $hotelsproData->saveReservationInfo(array("datas" => $arrayInvoice));
            
            if($saveResponseData['status'] != "ok"){
                echo $saveResponseData['msg'];
            }
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

}

?>