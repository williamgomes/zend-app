<?php
class Portfolio_Model_PortfolioListMapper 
{
	public function getSorting($settings_info, $data_arr)
	{
		if(!empty($settings_info['sort']))
		{
			foreach($settings_info['sort'] as $sort_arr)
			{
				if(!empty($sort_arr['field']) && !empty($sort_arr['dir']))
				{
					$field = $sort_arr['field'];
					$dir = $sort_arr['dir'];
					
					// Obtain a list of columns
					foreach ($data_arr as $key => $row) 
					{
						$volume[$key]  = $row[$field];
						
						//$edition[$key] = $row['edition'];
					}
					if($dir == 'asc')
					{
						array_multisort($volume, SORT_ASC, $data_arr);
					}
					else
					{
						array_multisort($volume, SORT_DESC, $data_arr);
					}
				}
			}
		}
		return $data_arr;
	}
	
	public function fetchAll($settings_info)
    {		
       	$path = $settings_info[$settings_info['file_path_field']];
		$file_arr = Eicra_File_Utility::getAllFiles($path);
		
		$json_arr = array('total' => 0, 'data' => array());
		if($file_arr)
		{	
			$data_arr = array();			
			$i = 0;
			foreach($file_arr as $files)
			{
				$ext = Eicra_File_Utility::GetExtension($files['name']);
				switch(strtolower($ext))
				{
					case 'flv':
					case 'swf':
					case 'FLV':
					case 'SWF':
						$img_path = 'application/modules/Administrator/layouts/scripts/images/common/flash.png';
						break;
					case 'mp3':
					case 'MP3':
						$img_path = 'application/modules/Administrator/layouts/scripts/images/common/mp3.png';
						break;
					case 'avi':
					case 'wmv':
					case 'wma':
					case 'WMA':
					case 'AVI':
					case 'WMV':
						$img_path = 'application/modules/Administrator/layouts/scripts/images/common/avi_thumb.png';
						break;
					case 'pdf':
					case 'doc':
					case 'docx':
					case 'xls':
					case 'xlsx':
					case 'ppt':
					case 'pptx':
					case 'PDF':
					case 'DOC':
					case 'DOCX':
					case 'XLS':
					case 'XLSX':
					case 'PPT':
					case 'PPTX':
						$img_path = 'application/modules/Administrator/layouts/scripts/images/common/'.strtolower($ext).'.png';
						break;
					case 'jpg':
					case 'png':
					case 'gif':
					case 'bmp':
					case 'ico':
						$img_path = $files['path'].'/'.$files['name']; 
						break;
					default:
						$img_path = 'application/modules/Administrator/layouts/scripts/images/common/all_file_thumb.png';
						break;
				}
				
				$data_arr[$i] = array('name' => $files['name'], 'type' => 'f'/*filetype($files['path'].'/'.$files['name'])*/, 'size' => filesize($files['path'].'/'.$files['name']) , 'image_field' => $img_path, 'image_file_name' => $files['name'], 'image_file_size' => filesize($files['path'].'/'.$files['name']), 'image_date' =>  date ("Y-m-d H:i:s A", filectime(APPLICATION_PATH.realpath('../'.$files['path'].'/'.$files['name']))));
				$i++;
			}
			
			$data_arr = $this->getSorting($settings_info, $data_arr);
			$paginator = Zend_Paginator::factory($data_arr);
			$paginator->setItemCountPerPage($settings_info['pageSize']);
			$paginator->setCurrentPageNumber($settings_info['page']);
			$paginator_data = $paginator->getIterator();
			if($paginator_data)
			{
				foreach($paginator_data as $paginator_data_key => $paginator_data_arr)
				{
					$json_arr['data'][$paginator_data_key]	=	$paginator_data_arr;
				}
			}
			$json_arr['total'] = $paginator->getTotalItemCount();
			$json_arr['post'] = $settings_info;
		}		
		return $json_arr;
    }
}
?>