<?php
class Portfolio_Model_Watermark
{
	protected $_file_name;
	protected $_db_table;
	protected $_file_path;
	
	
 
    public function __construct(array $options = null)
	{
		if (is_array($options)) 
		{
			$this->setOptions($options);			
		}			
	}		
	 
	public function setOptions(array $options)
	{			
		$this->_file_name = 	$options['file_name'];	
		$this->_file_path = 	$options['file_path'];	
	}
	
	public function setWatermark()
	{
		$global_conf = Zend_Registry::get('global_conf');
		
		if(!empty($global_conf['watermark_enabled']) && $global_conf['watermark_enabled'] == '1')
		{
			// Create a new Watermark_My_Image object with no parameters as we will set the file path later.
			$watermark = new Portfolio_Controller_Helper_Watermark();
			
			// Customize the watermark, add the text and icon.
			$watermark->set_background_color($global_conf['watermark_background_color'])
					  ->set_background_transparency($global_conf['watermark_background_transparency'])
					  ->set_height($global_conf['watermark_height'])
					  ->set_position($global_conf['watermark_position'])
					  ->set_place_inside($global_conf['watermark_place_inside']);
							
			if(!empty($global_conf['watermark_icons_show']) && $global_conf['watermark_icons_show'] == 'yes')
			{	
				if(!empty($global_conf['watermark_icons_files']))
				{
					$icon_arr = explode(',', $global_conf['watermark_icons_files']);
					$icon_path = 'data/frontImages/portfolio/watermark/icons/';
					if($icon_arr)
					{
						foreach($icon_arr as $arr_key=>$icon_img)
						{	
							if(file_exists($icon_path.$icon_img))
							{
								$ext = Eicra_File_Utility::GetExtension($icon_path.$icon_img);	
								$ext = strtolower($ext);
								if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif')
								{
									$ext_suffix = ($ext == 'jpg') ? 'jpeg' : $ext;
									$img_create_func = 'imagecreatefrom'.$ext_suffix;
									// Create a gd resource.
									$watermark_icon_resource = $img_create_func($icon_path.$icon_img);
									
									// Colorize the image.
									imagefilter($watermark_icon_resource, IMG_FILTER_COLORIZE, 211, 211, 211);
								
									// Create an icon object using the colorized image.
									$watermark_icon = new Portfolio_Controller_Helper_WatermarkIcon($watermark_icon_resource);
									$watermark_icon->set_max_height_percentage($global_conf['watermark_icons_max_height_percentage']);
									
									$watermark->add_icon($watermark_icon)
											  ->set_icons_align($global_conf['watermark_icons_align'])
											  ->set_icons_vertical_align($global_conf['watermark_icons_vertical_align'])
											  ->set_icons_offset_y($global_conf['watermark_icons_offset_y'])
											  ->set_icons_offset_x($global_conf['watermark_icons_offset_x'])
											  ->set_icons_spacing($global_conf['watermark_icons_spacing']);
									if($ext == 'jpg' || $ext == 'jpeg')
									{
										$watermark->set_jpeg_quality($global_conf['watermark_icons_jpeg_quality']);
									}
								}
							}
						}
					}
				}
			}
			
			
			
			if(!empty($global_conf['watermark_text_show']) && $global_conf['watermark_text_show'] == 'yes')
			{
				$tex_value = ($global_conf['watermark_text_value']) ? $global_conf['watermark_text_value'] : ' ';
				
				// Create a new Watermark_My_Image_text object passing a string as a parameter.
				$watermark_text = new Portfolio_Controller_Helper_WatermarkText($tex_value);
				
				// Customize the text.
				$watermark_text->set_color($global_conf['watermark_text_color'])
							   ->set_size($global_conf['watermark_text_size'])
							   ->set_fonts_directory($global_conf['watermark_text_fonts_directory'])
							   ->set_font($global_conf['watermark_text_font']);
								
								
				$watermark->add_text($watermark_text)
						  ->set_text_offset_x($global_conf['watermark_text_offset_x'])
						  ->set_text_offset_y($global_conf['watermark_text_offset_y'])
						  ->set_text_align($global_conf['watermark_text_align'])
						  ->set_text_spacing($global_conf['watermark_text_spacing']);		
			}			
						
			$watermark_src_dir = $this->_file_path;
			$watermark_dst_dir = $this->_file_path;
			$entry = $this->_file_name;
			
			if (is_file($watermark_src_dir .'/'. $entry)) 
			{	
				$extension = Eicra_File_Utility::GetExtension($entry);
				$extension = strtolower($extension);
				if($extension == 'jpg' || $extension == 'png' || $extension == 'gif')
				{			
				  // Set the file path to the source image and save the watermarked image to the destination directori.
				  $watermark->set_file_path($watermark_src_dir .'/'. $entry)
							->save($watermark_dst_dir .'/'. $entry);
			
				  // Get and display errors.
				  $errors = $watermark->get_errors();
				 
				  if ($errors)
				  {
					$json_arr = array('status' => 'err','msg' => $errors);
				  }
				  else
				  {
					$json_arr = array('status' => 'ok');  
				  }	
				}
				else
				{
					$json_arr = array('status' => 'ok');  
				}
			  return $json_arr;
			}
		}
	}
}
?>