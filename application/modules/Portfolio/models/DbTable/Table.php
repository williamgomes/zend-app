<?php
/**
* This is the DbTable class for the guestbook table.
*/
class Portfolio_Model_DbTable_Table extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'global_setting';	
	
	
	//Get Datas
	public function getInfos($requested_data) 
    {
		$setting_info = null;
		if(is_array($requested_data) && isset($requested_data['table_name']) && !empty($requested_data['primary_id_field']) && !empty($requested_data['primary_id_field_value']))
		{
			$select = $this->select()
						->setIntegrityCheck(false)
                        ->from(array('np' => Zend_Registry::get('dbPrefix').$requested_data['table_name'] ), array('*'));
						if($requested_data['preferences_table'] != true)
						{					   					   
					    	$select->where('np.'.$requested_data['primary_id_field'].' = ?',$requested_data['primary_id_field_value']);
						}
			
			$options = $this->getAdapter()->fetchAll($select);
			if($options)
			{
				
				if($requested_data['preferences_table'] && $requested_data['preferences_table'] === true)
				{
					$options_arr = array();
					foreach($options as $datas)
					{	
						if($datas['name'])
						{					
							$options_arr[0][$datas['name']] = stripslashes($datas['value']);
						}
					}
				}
				else
				{
					$options_arr = $options;	
				}
				
				foreach($options_arr as $datas)
				{
					foreach($datas as $key => $value)
					{
						if(in_array($key,$requested_data) )
						{
							$setting_info[$key] = $value;
						}
					}
					break;
				}
				
			}
		}
		return $setting_info;
	}
	
	public function reAssignPostValue($postValue)
	{	
		if(is_array($postValue) && isset($postValue['table_name']) && ($postValue['table_name'] == 'global_setting'))
		{
			$postValue[$postValue['file_path_field']] = 'data/adminImages/headerImages';
			$postValue[$postValue['file_extension_field']] = 'jpg,png,gif,ico';
			$postValue[$postValue['file_max_size_field']] = '233434340';
		}
		else
		{
			if(is_array($postValue) && isset($postValue['table_name']) && !empty($postValue['primary_id_field']) && !empty($postValue['primary_id_field_value']))
			{
				$select = $this->select()
						   ->from(array('np' => Zend_Registry::get('dbPrefix').$postValue['table_name'] ), array('*'))
						   ->setIntegrityCheck(false)					   
						   ->where('np.'.$postValue['primary_id_field'].' = ?',$postValue['primary_id_field_value']);
				
				$options = $this->getAdapter()->fetchAll($select);
				if($options)
				{
					foreach($options as $datas)
					{					
						$postValue[$postValue['file_extension_field']] = ($postValue['file_extension_field'] && $datas[$postValue['file_extension_field']]) ? $datas[$postValue['file_extension_field']] : '';
						$postValue[$postValue['file_max_size_field']] = ($postValue['file_max_size_field'] && is_numeric($datas[$postValue['file_max_size_field']])) ?  $datas[$postValue['file_max_size_field']] : '200';
						break;
					}
				}
			}
			else
			{
				if(is_array($postValue) && isset($postValue['table_name']) && ($postValue['table_name'] == 'site_template'))
				{
					$postValue[$postValue['file_path_field']] = 'temp';
					$postValue[$postValue['file_extension_field']] = 'zip';
					$postValue[$postValue['file_max_size_field']] = '0';
				}
				else
				{
					$postValue[$postValue['file_extension_field']] = 'jpg,png,gif';
					$postValue[$postValue['file_max_size_field']] = '200';
				}
			}
	}
		return $postValue;
	}
}

?>