<?php

class Portfolio_Model_Portfolio {

    protected $_requested_data;
    protected $_db_table;
    protected $_setting_info;
    protected $_fields = array(
        'table_name',
        'primary_id_field',
        'primary_id_field_value',
        'file_path_field',
        'file_extension_field',
        'file_max_size_field',
        'watermark_enable_field',
        'watermark_edit_enable_field',
        'watermark_image_path_field',
        'watermark_image_size_field',
        'watermark_image_x_position_field',
        'watermark_image_y_position_field',
        'watermark_image_edge_padding_field',
        'watermark_image_quantity_field',
        'watermark_font_path_field'
    );

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
            $this->createSettings();
        }
    }

    public function createSettings() {
        if ($this->_requested_data && is_array($this->_requested_data)) {
            if (isset($this->_requested_data['table_name']) && $this->_requested_data['table_name'] == 'global_setting') {
                $this->_setting_info[$this->_requested_data['file_path_field']] = 'data/adminImages/headerImages';
                $this->_setting_info[$this->_requested_data['file_extension_field']] = 'jpg,png,gif,ico';
                $this->_setting_info[$this->_requested_data['file_max_size_field']] = '233434340';
            } else {
                if (isset($this->_requested_data['table_name']) && isset($this->_requested_data['primary_id_field']) && isset($this->_requested_data['primary_id_field_value'])) {
                    $this->_db_table = new Portfolio_Model_DbTable_Table();
                    $setting_info = $this->_db_table->getInfos($this->_requested_data);
                    $this->_setting_info = $setting_info;
                } else {
                    $this->_setting_info = null;
                }
            }
        } else {
            $this->_setting_info = null;
        }

        if (empty($this->_setting_info) || empty($this->_setting_info[$this->_requested_data['file_path_field']])) {
            $this->_setting_info[$this->_requested_data['file_path_field']] = 'data/frontImages/portfolio';
        }
        if (empty($this->_setting_info) || empty($this->_setting_info[$this->_requested_data['file_extension_field']])) {
            $this->_setting_info[$this->_requested_data['file_extension_field']] = 'jpg,png,gif';
        }
        if (empty($this->_setting_info) || empty($this->_setting_info[$this->_requested_data['file_max_size_field']])) {
            $this->_setting_info[$this->_requested_data['file_max_size_field']] = '0';
        }
    }

    /* public function __set($name, $value)
      {
      $method = 'set' . $name;
      if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid Auth hotels');
      }
      $this->$method($value);
      }


      public function __get($name)
      {
      $method = 'get' . $name;
      if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid Auth hotels');
      }
      return $this->$method();
      } */

    public function setOptions(array $options) {
        foreach ($options as $key => $value) {
            if (in_array($key, $this->_fields)) {
                $this->_requested_data[$key] = $value;
            }
        }

        if (empty($this->_requested_data) || empty($this->_requested_data['file_path_field'])) {
            $this->_requested_data['file_path_field'] = 'file_path';
        }
        if (empty($this->_requested_data) || empty($this->_requested_data['file_extension_field'])) {
            $this->_requested_data['file_extension_field'] = 'file_type';
        }
        if (empty($this->_requested_data) || empty($this->_requested_data['file_max_size_field'])) {
            $this->_requested_data['file_max_size_field'] = 'file_max_size';
        }
    }

    public function savePortfolio() {
        $mapper = new Portfolio_Model_PortfolioMapper();
        $return = $mapper->save($this);
        return $return;
    }

    public function getRequestedData() {
        return $this->_requested_data;
    }

    public function getSettingInfo() {
        if (empty($this->_setting_info)) {
            
        }
        return $this->_setting_info;
    }

}

?>