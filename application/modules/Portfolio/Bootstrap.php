<?php
class Portfolio_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initAutoload() 
	{
		//Portfolio Bootstrap auto initialization					 
		$this->_resourceLoader->addResourceTypes(array( 
		'controllerhelper' => array(
                'namespace' => 'Controller_Helper',
                'path'      => 'controllers/helpers',
            ),
		));
		
	}	
}

