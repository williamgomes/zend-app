<?php

//Portfolio Frontend controller
class Portfolio_FrontendController extends Zend_Controller_Action {

    private $ckLicense = true;
    private $curlObj;
    private $_controllerCache;

    public function init() {
        $template_obj = new Eicra_View_Helper_Template;
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath(APPLICATION_PATH . '/layouts/scripts/' . $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout());


        /* Initialize action controller here */
        $this->view->assign('translator', Zend_Registry::get('translator'));

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {
        $this->view->setEscape('stripslashes');
    }

    public function managerAction() {
        try {
            $this->_helper->layout->disableLayout();
            //$this->_helper->viewRenderer->setNoRender();
            $upload_form = new Portfolio_Form_UploadForm();
            $this->view->assign('upload_form', $upload_form);

            $portfolio_model = new Portfolio_Model_Portfolio($this->_request->getParams());
            $requested_data = $portfolio_model->getRequestedData();
            $settings_info = $portfolio_model->getSettingInfo();
            $this->view->assign('posted_data', $this->_request->getParams());
            $this->view->assign('requested_data', $requested_data);
            $this->view->assign('settings_info', $settings_info);

            $merge_data = array_merge($requested_data, $settings_info);
            $settings_json_info = Zend_Json_Encoder::encode($merge_data);
            $this->view->assign('settings_json_info', $settings_json_info);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function gridAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost()) {
            $settings_info = $this->_request->getPost();
            $list_mapper = new Portfolio_Model_PortfolioListMapper();
            $json_arr = $list_mapper->fetchAll($settings_info);

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function showAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $file_path = $this->_request->getParam('file_path');
        $file_path = base64_decode($file_path);
        $file_name = $this->_request->getParam('file_name');


        $this->_response->setBody($this->view->serverUrl() . $this->view->baseUrl() . '/' . $file_path . '/' . $file_name);
    }

    public function uploadAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost()) {
            $fileInfo = $_FILES;
            $postValue = $this->_request->getPost();
            $upload_form = ($postValue['upload_file_name']) ? new Portfolio_Form_UploadForm($postValue) : new Portfolio_Form_UploadForm();
            $path = $postValue[$postValue['file_path_field']];
            $upload_file_element = ($postValue['upload_file_name']) ? $upload_form->getElement($postValue['upload_file_name']) : $upload_form->upload_file;

            $upload_file_element->setDestination($path);
            $file_obj = new Portfolio_Controller_Helper_File();
            $check_result = $file_obj->isValid($upload_file_element, $postValue, $fileInfo);

            if ($check_result['status'] == 'ok') {
                $rename_result = $file_obj->fileRename($upload_file_element, $path, $postValue);
                if ($rename_result['status'] == 'ok') {
                    if ($upload_file_element->receive()) {
                        $watermark_model = new Portfolio_Model_Watermark(array('file_path' => $path, 'file_name' => $upload_file_element->getFileName(null, false)));
                        $watermark_result = $watermark_model->setWatermark();
                        $archive_test = $file_obj->isSupportedArchive($upload_file_element, $postValue, $fileInfo);

                        if ($archive_test['status'] == 'ok') {
                            $msg = $this->view->translator->translator('File_upload_success', $upload_file_element->getFileName(null, false));
                            $json_arr = array('status' => 'ok', 'msg' => $msg, 'file_path' => $path, 'image_date' => date("Y-m-d H:i:s A", filectime(APPLICATION_PATH . realpath('../' . $path . '/' . $upload_file_element->getFileName(null, false)))), 'newName' => $upload_file_element->getFileName(null, false));
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $archive_test['msg'], 'file_path' => $path, 'image_date' => date("Y-m-d H:i:s A", filectime(APPLICATION_PATH . realpath('../' . $path . '/' . $upload_file_element->getFileName(null, false)))), 'newName' => $upload_file_element->getFileName(null, false));
                        }
                    } else {
                        $validatorMsg = $upload_file_element->getMessages();
                        $vMsg = implode("\n", $validatorMsg);
                        $json_arr = array('status' => 'err', 'msg' => $vMsg, 'file_path' => $path, 'image_date' => date("Y-m-d H:i:s A", filectime(APPLICATION_PATH . realpath('../' . $path . '/' . $upload_file_element->getFileName(null, false)))), 'newName' => $upload_file_element->getFileName(null, false));
                    }
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $rename_result['msg'], 'file_path' => $path, 'image_date' => date("Y-m-d H:i:s A", filectime(APPLICATION_PATH . realpath('../' . $path . '/' . $upload_file_element->getFileName(null, false)))), 'newName' => $upload_file_element->getFileName(null, false));
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $check_result['msg'], 'file_path' => $path, 'image_date' => date("Y-m-d H:i:s A", filectime(APPLICATION_PATH . realpath('../' . $path . '/' . $upload_file_element->getFileName(null, false)))), 'newName' => $upload_file_element->getFileName(null, false));
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function removeAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        try {
            if ($this->_request->isPost()) {
                $postValue = $this->_request->getPost();
                $path = $postValue[$postValue['file_path_field']];
                if ($postValue['fileNames']) {
                    $dir = $path . '/' . $postValue['fileNames'];
                    $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                    $json_arr = ($res) ? array('status' => 'ok') : array('status' => 'err', 'msg' => $this->view->translator->translator('file_delete_err', $postValue['fileNames']));
                } else {
                    $json_arr = array('status' => 'err', 'msg' => $this->view->translator->translator('file_delete_err'));
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $this->view->translator->translator('file_delete_err'));
            }
        } catch (Exception $e) {
            $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
        }
        //Convert To JSON ARRAY	
        $res_value = Zend_Json_Encoder::encode($json_arr);
        $this->_response->setBody($res_value);
    }

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost()) {
            $postValues = $this->_request->getPost();

            if ($postValues['image_field']) {
                $deleted_file_name_arr = $postValues['image_file_name'];
                $deleted_file_name = $postValues['image_file_name'];
                $dir_arr = explode(',', $postValues['image_field']);
                if ($dir_arr[0]) {
                    foreach ($dir_arr as $dir_key => $dir_value) {
                        $res = Eicra_File_Utility::deleteRescursiveDir($dir_value);
                        if (!$res) {
                            $deleted_file_name = $deleted_file_name_arr[$dir_key];
                            break;
                        }
                    }
                }
            } else if ($postValues['models']) {
                if ($postValues['models'][0]) {
                    foreach ($postValues['models'] as $dir_key => $dir_value) {
                        $dir = $dir_value['file_path'] . '/' . $dir_value['image_file_name'];
                        $deleted_file_name .= '&nbsp;' . $dir_value['image_file_name'];
                        $res = Eicra_File_Utility::deleteRescursiveDir($dir);
                        if (!$res) {
                            $deleted_file_name = $dir_value['image_file_name'];
                            break;
                        }
                    }
                }
            }

            if ($res) {
                $msg = $this->view->translator->translator("file_delete_success", $deleted_file_name);
                $json_arr = array('status' => 'ok', 'msg' => $msg, 'postValues' => $postValues);
            } else {
                $msg = $this->view->translator->translator("file_delete_err", $deleted_file_name);
                $json_arr = array('status' => 'err', 'msg' => $msg, 'postValues' => $postValues);
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

}
