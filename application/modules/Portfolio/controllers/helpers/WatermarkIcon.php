<?php

/**
 * Watermark My Image Standalone Class enables you to add a simple, yet very flexible and customizable watermark to your images.
 *
 * @package Watermark_My_Image
 * @author Marian Bucur <thebigman@marianbucur.com>
 * @version 1.0
 * @copyright Marian Bucur
 */



/**
 * The icon class.
 *
 * @package Watermark_My_Image
 */
class Portfolio_Controller_Helper_WatermarkIcon extends Portfolio_Controller_Helper_WatermarkBase
{

  /**
   * The height of the watermark.
   *
   * @var int
   * @access protected
   */
  protected $watermark_height;

  /**
   * Max height percentage out of the watermark's height.
   *
   * @var int
   * @access protected
   */
  protected $max_height_percentage = 80;

  /**
   * The initial width of the image.
   *
   * @var int
   * @access protected
   */
  protected $original_width;

  /**
   * The initial height of the image.
   *
   * @var int
   * @access protected
   */
  protected $original_height;

  /**
   * The calculated width of the image, based on the watermark's height and the $max_height_percentage.
   *
   * @var int
   * @access protected
   */
  protected $width;

  /**
   * The calculated height of the image, based on the watermark's height and the $max_height_percentage.
   *
   * @var int
   * @access protected
   */
  protected $height;

  /**
   * Calculate the image dimensions based on the watermark's height and the $max_height_percentage.
   *
   * @access protected
   * @return mixed
   */
  protected function calculate_dimensions()
  {
    // Set an error if the watermark's height was not set before calculating the dimensions.
    if (!$this->watermark_height) {
      return $this->set_error('You need to set the watermark height first!', __FILE__, __LINE__);
    }

    // Check if the dimensions were already calculated.
    if (!$this->width) {

      // Get the original image resource if it was not already stored in the original_image variable.
      if (!$this->original_image)
        $this->get_image_resource();

      // The file is not supported.
      if (!$this->original_image)
        return;


      // Get the dimensions of the image.
      $this->original_width = imagesx($this->original_image);
      $this->original_height = imagesy($this->original_image);

      // Calculate the dimensions.
      if ($this->watermark_height *  $this->max_height_percentage / 100 < $this->original_height) {
        $this->height = $this->watermark_height * $this->max_height_percentage / 100;
        $this->width = $this->original_width * $this->height / $this->original_height;
      } else {
        $this->height = $this->original_height;
        $this->width = $this->original_width;
      }
    }

    return true;
  }

  /**
   * Get the calculated width of the image.
   *
   * @access public
   * @return int
   */
  public function get_width()
  {
    // Calculate the dimensions if the width is not set.
    if (!$this->width)
      $this->calculate_dimensions();

    return $this->width;
  }

  /**
   * Get the calculated height of the image.
   *
   * @access public
   * @return int
   */
  public function get_height()
  {
    // Calculate the dimensions if the height is not set.
    if (!$this->height)
      $this->calculate_dimensions();

    return $this->height;
  }

  /**
   * Set the height of the watermark.
   *
   * @param int $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkIcon
   */
  public function set_watermark_height($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->watermark_height = $value;
    } else {
      $this->set_error('The height of the watermark must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the max height percentage.
   *
   * @param int $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkIcon
   */
  public function set_max_height_percentage($value)
  {
    // Check if the value is numeric, >= 0 and <= 100 and set an error if otherwise.
    if (!is_numeric($value)) {
      $this->set_error('The height percentage must be numeric!', __FILE__, __LINE__);
    } else if ($value < 0 || $value > 100) {
      $this->set_error('The height percentage must be >= 0 and <= 100!', __FILE__, __LINE__);
    } else {
      $this->max_height_percentage = $value;
    }

    return $this;
  }

  /**
   * Add the icon to the watermark.
   *
   * @param resource $dst_img
   * @param int $dst_img_x
   * @param int $dst_img_y
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkIcon
   */
  public function add_icon(&$dst_img, $dst_img_x, $dst_img_y)
  {
    // Check if the icon was loaded from a file
    if ($this->file_path && !$this->original_image) {
      // Check if the file is readable.
      if (is_readable($this->file_path)) {
        // Don't proceed if the icon is an animated gif.
        if ($this->is_animated_gif()) {
          // Set an error if the icon is an animated gif.
          $this->set_error('Animated gif icons are not supported!', __FILE__, __LINE__);
          return $this;
        }

        // Get the original image resource if it was not already stored in the original_image variable.
        if (!$this->original_image)
          $this->get_image_resource();

        // File type not supported. No need to set an error here as get_image_resource() will do that.
        if (!$this->original_image)
          return $this;

      } else {
        // Set an error if the file is unreadable.
        $this->set_error('File does not exist or is unreadable!', __FILE__, __LINE__);
        return $this;
      }
    }

    // Calculate the dimensions if they were not already calculated.
    if (!$this->width && !$this->calculate_dimensions())
      return $this;


    if ($this->width < $this->original_width || $this->height < $this->original_height) {
      // Copy the icon resampled if its dimensions have changed.
      imagecopyresampled($dst_img, $this->original_image, $dst_img_x, $dst_img_y, 0, 0, $this->width, $this->height, $this->original_width, $this->original_height);
    } else {
      // Copy the icon without resampling if its dimension haven't changed.
      imagecopy($dst_img, $this->original_image, $dst_img_x, $dst_img_y, 0, 0, $this->width, $this->height);
    }

    return $this;
  }

  /**
   * Get the array used for calculating the cache file names.
   *
   * @access public
   * @return array
   */
  public function get_cache_array()
  {
    $cache_array = array(
      'max_height_percentage' => $this->max_height_percentage,
    );

    if ($this->file_path) {
      // Calculate the crc32 from the file contents if know the path of the file.
      $this->load_file_contents();
      $cache_array['crc32'] = crc32($this->file_contents);
    } else if ($this->original_image) {

      // Calculate the crc32 from a gd2 image if the icon is a gd resource.

      // Check if the output buffering was already started and save the previous contents.
      if (ob_get_level() > 1) {
        $prev_contents = ob_get_clean();
      } else {
        ob_start();
      }

      // Get the contents of the gd2 image.
      imagegd2($this->original_image);
      $contents = ob_get_clean();

      // Check if the output buffering was already started and output the previous contents.
      if (isset($prev_contents)) {
        echo $prev_contents;
      } else {
        // End the output buffering if it was started by Watermark My Image.
        ob_end_clean();
      }

      // Calculate the crc32 from the gd2 image.
      $cache_array['crc32'] = crc32($contents);
    }

    return $cache_array;
  }
}
