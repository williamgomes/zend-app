<?php

/**
 * Watermark My Image Standalone Class enables you to add a simple, yet very flexible and customizable watermark to your images.
 *
 * @package Watermark_My_Image
 * @author Marian Bucur <thebigman@marianbucur.com>
 * @version 1.0
 * @copyright Marian Bucur
 */



/**
 * The main class.
 *
 * @package Watermark_My_Image
 */
class Portfolio_Controller_Helper_Watermark extends Portfolio_Controller_Helper_WatermarkBase
{

  /**
   * Debug mode.
   *
   * @var bool
   * @access protected
   */
  protected $debug_mode = false;

  /**
   * The cache directory.
   *
   * @var string
   * @access protected
   */
  protected $cache_directory = 'temp/cache/';

  /**
   * The cache watermark file name - used in watermarking animated gifs.
   *
   * @var string
   * @access protected
   */
  protected $cache_watermark_file_name;

  /**
   * The prefix added to the cached files.
   *
   * This is useful to avoid deleting the wrong files from the cache directory.
   *
   * @var string
   * @access protected
   */
  protected $cache_prefix = 'wmisc_';

  /**
   * Place the watermark inside or outside the original canvas?
   *
   * @var bool
   * @access protected
   */
  protected $place_inside = false;

  /**
   * The background color of the watermark.
   *
   * Do NOT add a # in front of the value.
   *
   * @var string
   * @access protected
   */
  protected $background_color = 'F4F4F4';

  /**
   * Transparency - from 0 (completely opaque) to 127 (completely transparent).
   *
   * @var int
   * @access protected
   */
  protected $background_transparency = 0;

  /**
   * The height of the watermark.
   *
   * @var int
   * @access protected
   */
  protected $height = 30;

  /**
   * The width of the preview.
   *
   * @var int
   * @access protected
   */
  protected $preview_width = 500;

  /**
   * The position of the watermark.
   *
   * @var string 'bottom', 'top'.
   * @access protected
   */
  protected $position = 'bottom';

  /**
   * The array which stores the icon objects.
   *
   * @var array
   * @access protected
   */
  protected $icons = array();

  /**
   * The icons align.
   *
   * @var string 'left', 'right'.
   * @access protected
   */
  protected $icons_align = 'right';

  /**
   * The icons vertical align.
   *
   * @var string 'top', 'center', 'bottom'.
   * @access protected
   */
  protected $icons_vertical_align = 'center';

  /**
   * The offset of the icons on the X axis.
   *
   * @var int
   * @access protected
   */
  protected $icons_offset_x = 9;

  /**
   * The offset of the icons on the Y axis.
   *
   * @var int
   * @access protected
   */
  protected $icons_offset_y = 4;

  /**
   * The spacing between the icons.
   *
   * @var int
   * @access protected
   */
  protected $icons_spacing = 9;

  /**
   * The array which stores the text objects.
   *
   * @var array
   * @access protected
   */
  protected $texts = array();

  /**
   * The text align.
   *
   * @var string 'left', 'right'.
   * @access protected
   */
  protected $text_align = 'left';

  /**
   * The offset of the text on the X axis.
   *
   * @var int
   * @access protected
   */
  protected $text_offset_x = 9;

  /**
   * The offset of the text on the Y axis.
   *
   * @var int
   * @access protected
   */
  protected $text_offset_y = 9;

  /**
   * The spacing between texts.
   *
   * @var int
   * @access protected
   */
  protected $text_spacing = 9;

  /**
   * The JPEG quality.
   *
   * @var int
   * @access protected
   */
  protected $jpeg_quality = 80;

  /**
   * Are the conditions to run ImageMagick satisfied ?
   *
   * @var bool
   * @access protected
   */
  protected $image_magick_check;

  /**
   * Set debug mode.
   *
   * Activating the debug mode is useful when: 'The image “xxx” cannot be displayed because it contains errors.'.
   *
   * @param bool $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_debug_mode($value)
  {
    $this->debug_mode = (bool)$value;

    return $this;
  }

  /**
   * Set the file path.
   *
   * @param mixed $value The file path or false/null for just a preview of the watermark.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_file_path($value = false)
  {
    parent::set_file_path($value);

    // Reset some variables.
    $this->cache_watermark_file_name = null;

    return $this;
  }

  /**
   * Set the cache directory.
   *
   * @param string $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_cache_directory($value)
  {
    // Check if a directory path was provided and if the directory exists and is writable and set an error if otherwise.
    if (trim($value) == '') {
      $this->set_error('Please provide a cache directory!', __FILE__, __LINE__);
    } else if (!is_dir($value)) {
      $this->set_error('The cache directory you provided does not exist!', __FILE__, __LINE__);
    } else if (!is_writable($value)) {
      $this->set_error('The cache directory you provided is not writable!', __FILE__, __LINE__);
    } else {
      $this->cache_directory = $value;
    }

    return $this;
  }

  /**
   * Set the value for $place_inside.
   *
   * @param bool $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_place_inside($value)
  {
    $this->place_inside = (bool)$value;

    return $this;
  }

  /**
   * Set the background color of the watermark.
   *
   * No need to add a # in front of the value.
   *
   * @param string $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_background_color($value)
  {
    // Remove the # as it is not required.
    $value = str_replace('#', '', $value);

    // Check if the value is hex and set an error if otherwise.
    if (preg_match('/^[a-f0-9]{6}$/i', $value)) {
      $this->background_color = $value;
    } else {
      $this->set_error('Invalid color code provided!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the background transparency.
   *
   * @param string $value From 0 (completely opaque) to 127 (completely transparent).
   * @access public
   * @return Watermark_My_Image
   */
  public function set_background_transparency($value)
  {
    // Check if the value is a number between 0 and 127 and set an error if otherwise.
    if (is_numeric($value) && $value >= 0 && $value <= 127) {
      $this->background_transparency = $value;
    } else {
      $this->set_error('The background transparency must be a number between 0 and 127!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the height of the watermark.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_height($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->height = $value;
    } else {
      $this->set_error('The height of the watermark must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the width of the preview.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_preview_width($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->preview_width = $value;
    } else {
      $this->set_error('The width of the preview must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the position of the watermark.
   *
   * @param string $value 'bottom', 'top'.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_position($value)
  {
    // Check if the value is valid and set an error if otherwise.
    if (in_array($value, array('top', 'bottom'))) {
      $this->position =  $value;
    } else {
      $this->set_error('Invalid position! Accepted: "top", "bottom".', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Add icons to the watermark.
   *
   * @param Portfolio_Controller_Helper_WatermarkIcon $icon
   * @access public
   * @return Watermark_My_Image
   */
  public function add_icon($icon)
  {
    // Check if the variable is a Portfolio_Controller_Helper_WatermarkIcon object and set an error if otherwise.
    if (is_object($icon) && get_class($icon) == 'Portfolio_Controller_Helper_WatermarkIcon') {
      // Check if the icon object contains errors and add them to the main class errors array.
      if ($errors = $icon->get_errors()) {
        $this->errors = array_merge($this->errors, $errors);
      } else {
        // Add the icon object to the icons array if everything was OK.
        $this->icons[] = $icon;
      }
    } else {
      $this->set_error('You need to provide a Portfolio_Controller_Helper_WatermarkIcon object!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the icons align.
   *
   * @param string $value 'left', 'right'.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_icons_align($value)
  {
    // Check if the value is valid and set an error if otherwise.
    if (in_array($value, array('left', 'right'))) {
      $this->icons_align = $value;
    } else {
      $this->set_error('Invalid icons align! Accepted: "left", "right".', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the icons vertical align.
   *
   * @param string $value 'top', 'center', 'bottom'.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_icons_vertical_align($value)
  {
    // Check if the value is valid and set an error if otherwise.
    if (in_array($value, array('top', 'center', 'bottom'))) {
      $this->icons_vertical_align = $value;
    } else {
      $this->set_error('Invalid icons vertical align! Accepted: "top", "center", "bottom".', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the X axis offset for the icons.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_icons_offset_x($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->icons_offset_x = $value;
    } else {
      $this->set_error('The icons X axis offset must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the Y axis offset for the icons.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_icons_offset_y($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->icons_offset_y = $value;
    } else {
      $this->set_error('The icons Y axis offset must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the spacing between icons.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_icons_spacing($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->icons_spacing = $value;
    } else {
      $this->set_error('The spacing between the icons must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Add text to the watermark.
   *
   * @param Portfolio_Controller_Helper_WatermarkText $text
   * @access public
   * @return Watermark_My_Image
   */
  public function add_text($text)
  {
    // Check if the variable is a Portfolio_Controller_Helper_WatermarkText object and set an error if otherwise.
    if (is_object($text) && get_class($text) == 'Portfolio_Controller_Helper_WatermarkText') {
      // Check if the text object contains errors and add them to the main class errors array.
      if ($errors = $text->get_errors()) {
        $this->errors = array_merge($this->errors, $errors);
      } else {
        // Add the text object to the texts array if everything was OK.
        $this->texts[] = $text;
      }
    } else {
      $this->set_error('You need to provide a Portfolio_Controller_Helper_WatermarkText object!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the text align.
   *
   * @param string $value 'left', 'right'.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_text_align($value)
  {
    // Check if the value is valid and set an error if otherwise.
    if (in_array($value, array('left', 'right'))) {
      $this->text_align = $value;
    } else {
      $this->set_error('Invalid text align! Accepted: "left", "right".', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the X axis offset for the text.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_text_offset_x($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->text_offset_x = $value;
    } else {
      $this->set_error('The text X axis offset must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the Y axis offset for the text.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_text_offset_y($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->text_offset_y = $value;
    } else {
      $this->set_error('The text Y axis offset must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the spacing between texts.
   *
   * @param int $value
   * @access public
   * @return Watermark_My_Image
   */
  public function set_text_spacing($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->text_spacing = $value;
    } else {
      $this->set_error('The spacing between the texts must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the JPEG quality.
   *
   * @param int $value 0 - 100.
   * @access public
   * @return Watermark_My_Image
   */
  public function set_jpeg_quality($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      // Check if the value is in the 0 - 100 range and set an error if otherwise.
      if ($value >= 0 && $value <= 100) {
        $this->jpeg_quality = $value;
      } else {
        $this->set_error('The JPEG quality must be >= 0 and <= 100!', __FILE__, __LINE__);
      }
    } else {
      $this->set_error('The JPEG quality must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Calculate and get the cache (watermark) file name.
   *
   * This function must be called only in generate(), save(), display() and get_resource().
   *
   * @param string $mode 'file', 'watermark'.
   * @param array $extra Build the cache file name with extra elements.
   * @access protected
   * @return mixed
   */
  protected function get_cache_file_name($mode = 'file', $extra = array())
  {
    // Return the cache watermark file name if it was already "calculated".
    if ($mode == 'watermark') {
      if ($this->cache_watermark_file_name)
        return $this->cache_watermark_file_name;
    }

    $cache_array = array(
      'background_color' => $this->background_color,
      'background_transparency' => $this->background_transparency,
      'height' => $this->height,
      'icons_align' => $this->icons_align,
      'icons_vertical_align' => $this->icons_vertical_align,
      'icons_offset_x' => $this->icons_offset_x,
      'icons_offset_y' => $this->icons_offset_y,
      'icons_spacing' => $this->icons_spacing,
      'text_align' => $this->text_align,
      'text_offset_x' => $this->text_offset_x,
      'text_offset_y' => $this->text_offset_y,
      'text_spacing' => $this->text_spacing,
    );

    if ($mode == 'file') {
      $cache_array['place_inside'] = $this->place_inside;
      $cache_array['position'] = $this->position;
      $cache_array['jpeg_quality'] = $this->jpeg_quality;

      if ($this->file_path) {
        // Calculate the crc32 from the file contents if know the path of the file.
        $this->load_file_contents();
        $cache_array['crc32'] = crc32($this->file_contents);
      } else if ($this->original_image) {

        // Calculate the crc32 from a gd2 image if we are watermarking a gd resource.

        // Check if the output buffering was already started and save the previous contents.
        if (ob_get_level() > 1) {
          $prev_contents = ob_get_clean();
        } else {
          ob_start();
        }

        // Get the contents of the gd2 image.
        imagegd2($this->original_image);
        $contents = ob_get_clean();

        // Check if the output buffering was already started and output the previous contents.
        if (isset($prev_contents)) {
          echo $prev_contents;
        } else {
          // End the output buffering if it was started by Watermark My Image.
          ob_end_clean();
        }

        // Calculate the crc32 from the gd2 image.
        $cache_array['crc32'] = crc32($contents);
      }

    } else {
      $cache_array['preview_width'] = $this->preview_width;
    }

    if (!empty($this->icons)) {
      foreach ($this->icons as $key => $icon) {
        $cache_array["icon$key"] = $icon->get_cache_array();
      }
    }

    if (!empty($this->texts)) {
      foreach ($this->texts as $key => $text) {
        $cache_array["text$key"] = $text->get_cache_array();
      }
    }

    // Add any extra elements to the acache array.
    if (is_array($extra) && !empty($extra)) {
      $cache_array = array_merge($cache_array, $extra);
    }

    $file_name = $this->cache_prefix . hash('md5', serialize($cache_array));

    if ($mode == 'file') {
      // Check the type of the file and set the file name with the proper extension.
      if ($this->get_mime_type()) {
        switch ($this->mime_type) {
          case 'image/jpeg':
            return $file_name . '.jpg';
          case 'image/png':
            return $file_name . '.png';
          case 'image/gif':
            return $file_name . '.gif';
          default:
            return $file_name;
        }
      }
    } else {
      // We are saving the watermark with the .png extension.
      return $this->cache_watermark_file_name = $file_name . '.png';
    }
  }

  /**
   * Check if the conditions to run ImageMagick are satisfied.
   *
   * @access public
   * @return bool
   */
  public function image_magick_check()
  {
    // Return true or false if we already know if the conditions to run ImageMagick are satisfied.
    if ($this->image_magick_check !== null)
      return $this->image_magick_check;

    // Check if the system function is enabled and set an error if otherwise.
    if (function_exists('system')) {
      // Check if PHP is not in safe mode and set an error if otherwise.
      if (!ini_get('safe_mode')) {
        // Check if convert exists in the system path and set an error if otherwise.

        // Check if the output buffering was already started and save the previous contents.
        if (ob_get_level() > 1) {
          $prev_contents = ob_get_clean();
        } else {
          ob_start();
        }

        // Try and execute the convert app.
        system("convert -version");

        // Get the output from the execution of the above.
        $output = ob_get_contents();

        // Check if the output buffering was already started and output the previous contents.
        if (isset($prev_contents)) {
          echo $prev_contents;
        } else {
          // End the output buffering if it was started by Watermark My Image.
          ob_end_clean();
        }


        if (stripos($output, 'ImageMagick') !== false) {
          return $this->image_magick_check = true;
        } else {
          $this->set_error('ImageMagick not found in the system path!', __FILE__, __LINE__);
        }
      } else {
        $this->set_error('ImageMagick cannot be executed while PHP is in safe mode!', __FILE__, __LINE__);
      }
    } else {
      $this->set_error('ImageMagick cannot be executed while the "system" function is disabled!', __FILE__, __LINE__);
    }

    return $this->image_magick_check = false;
  }

  /**
   * Generate the watermark and display, save or return an image resource.
   *
   * @param string $mode 'display', 'resource', 'save'.
   * @param mixed $file_path This should be set to a valid file path if saving the file.
   * @param mixed $type Supported file types: 'jpg', 'png', 'gif. Set false/null to automatically get the file type. Animated gifs will automatically have the the file type set to 'gif'. If you are watermarking a gd resource, you will need to specify a file type.
   * @param bool $save_cache Enable or disable saving to cache.
   * @param mixed $resource Used by get_resource()
   * @access protected
   */
  protected function generate($mode = 'display', $file_path = null, $type = null, $save_cache = true, &$resource = null)
  {
    // Stop the execution if there are errors.
    if (!empty($this->errors))
      return;

    // Check the PHP version.
    if ((int)substr(PHP_VERSION, 0, 1) != 5) {
      return $this->set_error('You need PHP 5 to use Watermark My Image Standalone Class!', __FILE__, __LINE__);
    }

    // Don't do anything if GD is not available or if FreeType isn't supported.
    if ( !extension_loaded( 'gd' ) ) {
      return $this->set_error('The GD extension is not available!', __FILE__, __LINE__);
    } else {
      $gd_info = gd_info();

      if ( !$gd_info['FreeType Support'] )
        return $this->set_error('FreeType isn\'t supported!', __FILE__, __LINE__);
    }

    // Check if $mode is a valid value and set an error if otherwise.
    if (!in_array($mode, array('display', 'resource', 'save'))) {
      return $this->set_error('Invalid mode used for handlin the watermark!', __FILE__, __LINE__);
    }

    if ($file_path) {

      // If file path is set, get the extension from it.
      $extension = substr($file_path, strrpos($file_path, '.') + 1);

      // Check if the extension is one of the allowed file types.
      if (!in_array($extension, array('jpg', 'png', 'gif'))) {
        return $this->set_error('The allowed file extensions are: ".jpg", ".png", ".gif"!', __FILE__, __LINE__);
      }

      // Set the type from the extension if the type was not already set.
      if (!$type) {
        $type = $extension;
      }
    }

    // If $type is set check if it's a valid value.
    if ($type && !in_array($type, array('jpg', 'png', 'gif'))) {
      return $this->set_error('The allowed file types are: "jpg", "png", "gif"!', __FILE__, __LINE__);
    }

    // We are watermarking a gd resource without saving it to the cache, so we don't need to know the file type.
    if ($mode == 'resource' && !$this->file_path && $this->original_image && !$save_cache && !$type);
    else {
      // Check if we are watermarking a gd resource.
      if (!$this->file_path && $this->original_image) {
        if (!$type) {
          // We need to know the file type when watermarking a gd resource and saving the file to the cache.
          return $this->set_error('When watermarking a gd resource, you must set one of the following file types: "jpg", "png", "gif"!', __FILE__, __LINE__);
        }

        // When watermarking a gd resource, we need to manually set the mime type because we cannot determine it from the resource.
        switch ($type) {
          case 'jpg':
            $this->mime_type = 'image/jpeg';
            break;
          case 'png':
            $this->mime_type = 'image/png';
            break;
          case 'gif':
            $this->mime_type = 'image/gif';
            break;
        }
      }


      // If $type is not set get it from the mime type.
      if (!$type) {
        $type = str_replace('image/', '', $this->get_mime_type());
        $type = str_replace('jpeg', 'jpg', $type);
      }


      if ($this->is_animated_gif()) {
        // Build the cache file name for an animated gif.
        $cache_file_path = $this->cache_directory . $this->get_cache_file_name();
      } else {
        // Build the cache file name.
        $cache_file_path = $this->cache_directory . $this->get_cache_file_name('file', array('type' => $type));
      }

      if ($mode == 'display' || $mode == 'resource') {

        // Check if the file exists in the cache and if it's readable.
        if (is_file($cache_file_path) && is_readable($cache_file_path)) {

          // Get the file contents from cache.
          $image_contents = file_get_contents($cache_file_path);

          // Check if the file contents were successfully read.
          if ($image_contents) {
            if ($mode == 'display') {
              // Get the image size.
              @$image_size = getimagesize($cache_file_path);

              // If the image size was successfully retrieved, display the file and stop the execution of the script.
              if ($image_size) {
                if (!$this->debug_mode) {
                  header('Content-Type: ' . $image_size['mime']);
                  echo $image_contents;
                }
                return;
              }
            } else {
              // Set the resource parameter and stop the execution of the script.
              $resource = imagecreatefromstring($image_contents);
              return;
            }
          }
        }
        // Check if the file exists in the cache so we can move it from there to the $file_path.
      } 
	  else if (file_exists($cache_file_path) && is_file($cache_file_path) && file_exists($file_path)) 
	  {
		 return rename($cache_file_path, $file_path);
      }
    }


    // If $file_path is false we will generate only the watermark (preview) or we are watermarking a gd resource.
    if ($this->file_path) {
      // Check if the file is readable and set an error if otherwise.
      if (is_readable($this->file_path)) {
        // Check if the file is is not an animated gif.
        if (!$this->is_animated_gif()) {

          // Get the image resource.
          $image = $this->get_image_resource();

          // File type not supported.
          if (!$image)
            return null;
        } else if (!$this->image_magick_check()) {
          // Don't proceed if the file is an animated gif and ImageMagick cannot be executed.
          return $this->set_error('Animated gifs are not supported!', __FILE__, __LINE__);
        } else if (!is_writable($this->cache_directory)) {
          // Don't proceed if the cache directory is not writable.
          return $this->set_error('Cannot watermark animated gifs because the cache directory is not writable!', __FILE__, __LINE__);
        }
      } else {
        return $this->set_error('File does not exist or is unreadable!', __FILE__, __LINE__);
      }
    } else if ($this->original_image) {
      $image = $this->original_image;
    }


    // Check if we are watermarking an image or just generating a preview and calculate the width, height and new_height accordingly.
    if (isset($image)) {
      // Get the width of the image.
      $width = imagesx($image);
      // Get the height of the file.
      $height = imagesy($image);

      imagesavealpha($image, true);
      imagealphablending($image, false);

      // If we are not placing the watermark inside the original image, we`ll need a taller canvas.
      if (!$this->place_inside) {
        // Create a taller temporary image.
        $tmp_img = imagecreatetruecolor($width, $height + $this->height);

        // Allocate a color with transparency.
        $background_color = imagecolorallocatealpha($tmp_img, 0, 0, 0, 127);

        // Fill the new image with the background color.
        imagefill($tmp_img, 0, 0, $background_color);

        // Calculate the $dst_y based on the watermark position.
        $dst_y = $this->position == 'bottom' ? 0 : $this->height;

        // Copy the original image to the temporary image.
        imagecopy($tmp_img, $image, 0, $dst_y, 0, 0, $width, $height);

        // Assign the content of the temporary image to the image.
        $image = $tmp_img;
      }

      imagealphablending($image, true);
    } else {
      // We are watermarking an animated gif.
      if ($this->is_animated_gif) {
        // Get the image size.
        $this->get_image_size();

        // Check if the image size was retrieved correctly and stop the generation if otherwise.
        if (!$this->image_size)
          return;

        // Get the width and height of the gif file.
        list($width, $height) = $this->image_size;

        $new_height = $this->height;
      } else {
        // Set the dimensions of the preview.
        $width = $this->preview_width;
        $height = 0;
        $new_height = $this->height;
      }
    }


    // Create the watermark image resource, which will contain the background color, texts and icons.
    $watermark = imagecreatetruecolor($width, $this->height);


    // Convert the hex background color to rgb.
    $rgb = $this->hex_to_rgb($this->background_color);

    // Do this if the watermark is not completely opaque.
    if ($this->background_transparency > 0) {
      imagesavealpha($watermark, true);
      imagealphablending($watermark, false);
    }

    // Fill the new image with the background color.
    $background_color = imagecolorallocatealpha($watermark, $rgb[0], $rgb[1], $rgb[2], $this->background_transparency);
    imagefill($watermark, 0, 0, $background_color);

    imagealphablending($watermark, true);

    // Add icons to the watermark, if any.
    if (!empty($this->icons)) {

      // Initialize a few variables.
      $widths = array();
      $heights = array();
      $x = array();
      $max_height = 0;

      // Calculate the X coordinates based on the icons align.
      switch ($this->icons_align) {

        case 'left':

          foreach ($this->icons as $key => $icon) {

            // Let the icon object know what is the height of the watermark so we can calculate its dimensions.
            $icon->set_watermark_height($this->height);

            $widths[] = $icon->get_width();

            $heights[] = $icon->get_height();

            // We need to know the maximum height of the icons for calculating the Y coordinates, based on the vertical align.
            $max_height = $heights[$key] > $max_height ? $heights[$key] : $max_height;


            if ($key == 0) {
              // Calculate the X coordinate for the 1st icon.
              $x[] = $this->icons_offset_x;
              $icons_offset_x = $x[0];
            } else {
              // Calculate the X coordinate for the rest of the icons.
              $x[] = $icons_offset_x + $widths[$key - 1] + $this->icons_spacing;
              $icons_offset_x += $widths[$key - 1] + $this->icons_spacing;
            }
          }

          break;

        default:

          $count = count($this->icons) - 1;

          for ($i = $count; $i >= 0; $i--) {

            // Let the icon object know what is the height of the watermark so we can calculate its dimensions.
            $this->icons[$i]->set_watermark_height($this->height);

            $width_aux = $this->icons[$i]->get_width();

            $heights[$i] = $this->icons[$i]->get_height();

            // We need to know the maximum height of the icons for calculating the Y coordinates, based on the vertical align.
            $max_height = $heights[$i] > $max_height ? $heights[$i] : $max_height;


            if ($i == $count) {
              // Calculate the X coordinate for the last icon.
              $x[$i] = $width - $width_aux - $this->icons_offset_x;
              $icons_offset_x = $x[$i];
            } else {
              // Calculate the X coordinate for the rest of the icons.
              $x[$i] = $icons_offset_x - $width_aux - $this->icons_spacing;
              $icons_offset_x -= $width_aux + $this->icons_spacing;
            }

          }

          break;
      }


      // Calculate the Y coordinates based on the max height and the vertical align of the icons.
      foreach ($heights as $key => $h) {

        switch ($this->icons_vertical_align) {

          case 'top':
            $y = $this->icons_offset_y;
            break;

          case 'bottom':
            $y = $this->icons_offset_y + $max_height - $h;
            break;

          default:
            $y = $this->icons_offset_y + ($max_height / 2) - ($h / 2);
            break;
        }

        // Add the icon to the image.
        $this->icons[$key]->add_icon($watermark, $x[$key], $y);

        // Check for errors and stop execution if any.
        if ($errors = $this->icons[$key]->get_errors()) {
          $this->errors = array_merge($this->errors, $errors);
          return;
        }
      }
    }


    // Add text to the watermark, if any.
    if (!empty($this->texts)) {

      $bboxes = array();
      $x = array();
      $max_top = 0;


      // Calculate the X coordinates based on the text align.
      switch ($this->text_align) {

        case 'left':

          // Calculate the text box for each text.
          foreach ($this->texts as $key => $text) {

            $bboxes[] = $text->calculate_text_box();

            if ($key == 0) {
              // Calculate the X coordinate for the 1st text.
              $x[] = $this->text_offset_x;
              $text_offset_x = $x[0];
            } else {
              // Calculate the X coordinate for the rest of the texts.
              $x[] = $text_offset_x + $bboxes[$key - 1]['width'] + $this->text_spacing;
              $text_offset_x += $bboxes[$key - 1]['width'] + $this->text_spacing;
            }

            // We need to know the biggest 'top' value for calculating the Y coordinates of all the texts.
            if ($bboxes[$key]['top'] > $max_top) {
              $max_top = $bboxes[$key]['top'];
            }
          }

          break;

        default:

          $count = count($this->texts) - 1;

          // Calculate the text box for each text
          for ($i = $count; $i >= 0; $i--) {

            $bboxes[$i] = $this->texts[$i]->calculate_text_box();

            if ($i == $count) {
              // Calculate the X coordinate for the last text.
              $x[$i] = $width - $bboxes[$i]['width'] - $this->text_offset_x;
              $text_offset_x = $x[$i];
            } else {
              // Calculate the X coordinate for the rest of the texts.
              $x[$i] = $text_offset_x - $bboxes[$i]['width'] - $this->text_spacing;
              $text_offset_x -= $bboxes[$i]['width'] + $this->text_spacing;
            }

            // We need to know the biggest 'top' value for calculating the Y coordinates of all the texts.
            if ($bboxes[$i]['top'] > $max_top) {
              $max_top = $bboxes[$i]['top'];
            }
          }

          break;
      }

      // Calculate the Y coordinates based on the biggest 'top' value.
      foreach ($bboxes as $key => $bbox) {

        $y = $this->text_offset_y + $max_top;

        // Add the text to the image.
        $this->texts[$key]->add_text($watermark, $x[$key], $y);
      }
    }


    // If we are generating a preview, don't save the watermark to the cache.
    if (!$this->file_path && !$this->original_image) {
      $save_cache = false;
    }


    // Do this if the file is an animated gif.
    if ($this->is_animated_gif) {

      // Save the watermark to the cache directory so it can be loaded by ImageMagick.
      $cache_watermark_file_path = $this->cache_directory . $this->get_cache_file_name('watermark');
      imagepng($watermark, $cache_watermark_file_path);

      // Build the command
      $cmd = $this->file_path . " -coalesce -extent {$width}x" . ($this->place_inside ? $height : $height + $this->height) .
            ($this->position == 'top' && !$this->place_inside ? " -repage +0+" + $this->height : '').
            " -gravity " . ($this->position == 'bottom' ? 'southeast' : 'northeast') . " -geometry +0+0 null: $cache_watermark_file_path -layers composite -layers optimize";

      // Display, set an image resource or save the file.
      if ($mode == 'display' || $mode == 'resource') {

        // Check if the output buffering was already started and save the previous contents.
        if (ob_get_level() > 1) {
          $prev_contents = ob_get_clean();
        } else {
          ob_start();
        }

        // Execute the command.
        system("convert $cmd -");

        // Get the ouput.
        $output = ob_get_flush();

        // Check if the output buffering was already started and output the previous contents.
        if (isset($prev_contents)) {
          echo $prev_contents;
        } else {
          // End the output buffering if it was started by Watermark My Image.
          ob_end_clean();
        }


        // If the caching feature is enabled, save the file to the cache.
        if ($save_cache) {
          file_put_contents($this->cache_directory . $this->get_cache_file_name(), $output);
        }


        if ($mode == 'display') {
          // Display the file and stop the execution of the script.
          if (!$this->debug_mode) {
            header('Content-Type: image/gif');
            echo $output;
          }
          return;
        } else {
          // Set the file as an image resource and stop the execution of the script.
          $resource = imagecreatefromstring($output);
          return;
        }

      } else {
        // Mute the ouput from the system function as there is no need for it when saving the file.

        // Check if the output buffering was already started and save the previous contents.
        if (ob_get_level() > 1) {
          $prev_contents = ob_get_clean();
        } else {
          ob_start();
        }

        // Execute the command.
        system("convert $cmd $file_path");

        // Check if the output buffering was already started and output the previous contents.
        if (isset($prev_contents)) {
          echo $prev_contents;
        } else {
          // End the output buffering if it was started by Watermark My Image.
          ob_end_clean();
        }
      }

    } else {
      // Copy the original image over the watermarked image if we are not using the preview mode.
      if (isset($image)) {

        if ($this->place_inside) {
          $dst_y = $this->position == 'bottom' ? $height - $this->height : 0;
        } else {
          $dst_y = $this->position == 'bottom' ? $height : 0;
        }

        imagecopy($image, $watermark, 0, $dst_y, 0, 0, $width, $this->height);
      } else {
        // If we are using the preview mode, then the main image becomes the watermark.
        $image = $watermark;
      }


      // Display, set an image resource or save the file.
      if ($mode == 'display' || $mode == 'resource') {

        // If the caching feature is enabled, save the file to the cache.
        if ($save_cache) {
          switch ($type) {
            case 'jpg':
              imagejpeg($image, $cache_file_path, $this->jpeg_quality);
              break;
            case 'png':
              imagepng($image, $cache_file_path);
              break;
            case 'gif':
              imagegif($image, $cache_file_path);
              break;
          }
        }

        // Display the image.
        if ($mode == 'display') {
          switch ($type) {
            case 'jpg':
              if (!$this->debug_mode) {
                header('Content-type: image/jpeg');
                return imagejpeg($image, null, $this->jpeg_quality);
              }
            case 'png':
              if (!$this->debug_mode) {
                header('Content-type: image/png');
                return imagepng($image);
              }
            case 'gif':
              if (!$this->debug_mode) {
                header('Content-type: image/gif');
                return imagegif($image);
              }
          }
        } else {
          $resource = $image;
          return;
        }
      } else {
        // Save the image.
        switch ($type) {
          case 'jpg':
            return imagejpeg($image, $file_path, $this->jpeg_quality);
          case 'png':
            return imagepng($image, $file_path);
          case 'gif':
            return imagegif($image, $file_path);
        }
      }
    }
  }

  /**
   * Save the file to disk.
   *
   * @param string $file_path
   * @param string $type Supported types: 'jpg', 'png', 'gif'.
   * @access public
   */
  public function save($file_path, $type = null)
  {
    // Check if a file path was specified and set an error if otherwise.
    if (trim($file_path) == '') {
      return $this->set_error('Please specify a file path to save the file!', __FILE__, __LINE__);
    }

    // Check if the directory is writable and set an error if otherwise.
    if (!is_writable( dirname($file_path) )) {
      return $this->set_error('The directory you are trying to save the file to is not writable!', __FILE__, __LINE__);
    }

    // Generate and save the file to disk.
    $this->generate('save', $file_path, $type);
  }

  /**
   * Display the image.
   *
   * @param string $type Supported types: 'jpg', 'png', 'gif'.
   * @param bool $save_cache Enable or disable saving to cache.
   * @access public
   */
  public function display($type = null, $save_cache = true)
  {
    $this->generate('display', null, $type, $save_cache);
  }

  /**
   * Get the image resource.
   *
   * @param string $type Supported types: 'jpg', 'png', 'gif'. This is used when saving the gd resource to cache. You don't need to set this if $save_cache is false.
   * @param bool $save_cache Enable or disable saving to cache.
   * @access public
   * @return resource
   */
  public function get_resource($type = null, $save_cache = true)
  {
    $this->generate('resource', null, $type, $save_cache, $resource);

    return $resource;
  }

  /**
   * Delete all the files that contain the cache_prefix from the cache directory.
   *
   * @access public
   */
  public function clear_cache()
  {
    if ($handle = opendir($this->cache_directory)) {
      while (false !== ($entry = readdir($handle))) {
        if (strpos($entry, $this->cache_prefix) !== false && is_file($this->cache_directory . $entry)) {
          unlink($this->cache_directory . $entry);
        }
      }
    }
  }
}
