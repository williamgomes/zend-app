<?php

/**
 * Watermark My Image Standalone Class enables you to add a simple, yet very flexible and customizable watermark to your images.
 *
 * @package Watermark_My_Image
 * @author Marian Bucur <thebigman@marianbucur.com>
 * @version 1.0
 * @copyright Marian Bucur
 */



/**
 * The text class.
 *
 * @package Watermark_My_Image
 */
class Portfolio_Controller_Helper_WatermarkText extends Portfolio_Controller_Helper_WatermarkBase
{

  /**
   * The text.
   *
   * @var string
   * @access protected
   */
  protected $text = 'Enjoy using Watermark My Image Standalone Class';

  /**
   * The path to the font files directory.
   *
   * @var string
   * @access protected
   */
  protected $fonts_directory = 'data/frontImages/portfolio/watermark/fonts/';

  /**
   * The font file.
   *
   * @var string
   * @access protected
   */
  protected $font = 'Designosaur-Regular.ttf';

  /**
   * The font size.
   *
   * @var int
   * @access protected
   */
  protected $size = 12;

  /**
   * The text color.
   * Do NOT add a # in front of the value.
   *
   * @var int
   * @access protected
   */
  protected $color = '4A4A4A';

  /**
   * The class constructor.
   *
   * @param string $text The text
   * @access public
   */
  public function __construct($text = false)
  {
    if (trim($text) != '') {
      $this->text = $text;
    }

    // Check if the font file exists and is readable and set an error if otherwise
    if (!is_file($this->fonts_directory . $this->font)) {
      $this->set_error('The font file does not exist!', __FILE__, __LINE__);
    } else if (!is_readable($this->fonts_directory . $this->font)) {
      $this->set_error('The font file is unreadable!', __FILE__, __LINE__);
    }
  }

  /**
   * Set the text.
   *
   * @param string $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function set_text($value)
  {
    // Check if the text is blank and set an error if so
    if (trim($value) == '') {
      $this->set_error('Please provide a text!', __FILE__, __LINE__);
    } else {
      $this->text = $value;
    }

    return $this;
  }

  /**
   * Set the font files directory.
   *
   * @param string $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function set_fonts_directory($value)
  {
    // Check if a directory path was provided and if the directory exists and is readable and set an error if otherwise.
    if (trim($value) == '') {
      $this->set_error('Please provide a fonts directory!', __FILE__, __LINE__);
    } else if (!is_dir($value)) {
      $this->set_error('The fonts directory you provided does not exist!', __FILE__, __LINE__);
    } else {
      $this->fonts_directory = $value;
    }

    return $this;
  }

  /**
   * Set the font file name.
   *
   * @param string $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function set_font($value)
  {
    // Check if a file name was provided and if the file exists and is readable and set an error if otherwise.
    if (trim($value) == '') {
      $this->set_error('Please provide a font file name!', __FILE__, __LINE__);
    } else if (!is_file($this->fonts_directory . $value)) {
      $this->set_error('The font file you provided does not exist!', __FILE__, __LINE__);
    } else if (!is_readable($this->fonts_directory . $value)) {
      $this->set_error('The font file you provided is unreadable!', __FILE__, __LINE__);
    } else {
      $this->font = $value;
    }

    return $this;
  }

  /**
   * Set the font size.
   *
   * @param int $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function set_size($value)
  {
    // Check if the value is numeric and set an error if otherwise.
    if (is_numeric($value)) {
      $this->size = $value;
    } else {
      $this->set_error('The font size must be numeric!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Set the text color.
   *
   * No need to add a # in front of the value.
   *
   * @param string $value
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function set_color($value)
  {
    // Remove the # as it is not required.
    $value = str_replace('#', '', $value);

    // Check if the value is hex and set an error if otherwise.
    if (preg_match('/^[a-f0-9]{6}$/i', $value)) {
      $this->color = $value;
    } else {
      $this->set_error('The color code must be in hexadecimal format!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Calculates the top, left, width, height and baseline of the text.
   *
   * @access public
   * @return array
   */
  public function calculate_text_box()
  {
    $bbox = imagettfbbox($this->size, 0, $this->fonts_directory . $this->font, $this->text);

    $tmp_bbox['left'] = abs($bbox[0]);
    $tmp_bbox['top'] = abs($bbox[5]);
    $tmp_bbox['width'] = abs($tmp_bbox['left'] - $bbox[2]) + 2;
    $tmp_bbox['height'] = abs($tmp_bbox['top'] - $bbox[1]);

    if ($bbox[1] > 0) {
      $letters = array_unique(str_split($this->text));

      $max = 0;

      foreach ($letters as $letter) {
        $aux_bbox = imagettfbbox($this->size, 0, $this->fonts_directory . $this->font, $letter);

        $max = abs($aux_bbox[5]) > $max ? abs($aux_bbox[5]) : $max;
      }

      $bbox2 = imagettfbbox($this->size, 0, $this->fonts_directory . $this->font, strtoupper($this->text));

      $tmp_bbox['baseline'] = abs($max - $bbox2[1]);

    } else {
      $tmp_bbox['baseline'] = 0;
    }

    return $tmp_bbox;
  }

  /**
   * Handle unicode text.
   *
   * @param string $text
   * @access public
   * @return string
   */
  public function unicode($text)
  {
    // Check if the required functions exist and return the text unprocessed if otherwise
    if (function_exists('mb_detect_encoding') && function_exists('mb_convert_encoding') && function_exists('mb_encode_numericentity')) {
      // Detect if the string was passed in as unicode
      $text_encoding = mb_detect_encoding($text, 'UTF-8, ISO-8859-1');
      // Make sure it's in unicode
      if ($text_encoding != 'UTF-8') {
        $text = mb_convert_encoding($text, 'UTF-8', $text_encoding);
      }

      // HTML numerically-escape everything (&#[dec];)
      $text = mb_encode_numericentity($text, array (0x0, 0xffff, 0, 0xffff), 'UTF-8');
    }

    return $text;
  }

  /**
   * Add the text to the watermark.
   *
   * @param resource $dst_img
   * @param int $dst_img_x
   * @param int $dst_img_y
   * @access public
   * @return Portfolio_Controller_Helper_WatermarkText
   */
  public function add_text(&$dst_img, $dst_img_x, $dst_img_y)
  {
    // Get the rgb values for the text color.
    $rgb = $this->hex_to_rgb($this->color);
    // Allocate the color.
    $color = imagecolorallocate($dst_img, $rgb[0], $rgb[1], $rgb[2]);
    // Add the text to the watermark.
    imagettftext($dst_img, $this->size, 0, $dst_img_x, $dst_img_y, $color, $this->fonts_directory . $this->font, $this->unicode($this->text));
  }

  /**
   * Get the array used for calculating the cache file names.
   *
   * @access public
   * @return array
   */
  public function get_cache_array()
  {
    $cache_array = array(
      'text' => $this->text,
      'size' => $this->size,
      'color' => $this->color,
    );

    // Check if the font file is readable.
    if (is_readable($this->fonts_directory . $this->font)) {
      // Calculate its crc32.
      $contents = file_get_contents($this->fonts_directory . $this->font);
      $cache_array['crc32'] = crc32($contents);
      return $cache_array;
    }

    $this->set_error('The font file is unreadable!', __FILE__, __LINE__);
  }
}
