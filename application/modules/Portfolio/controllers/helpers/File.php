<?php
class Portfolio_Controller_Helper_File
{		
	public function fileRename(&$fileObj, $path, $postValue = null) //Reference file object &$fileObj
	{
		try
		{
			if($postValue == null || ($postValue['table_name'] != 'site_template'))
			{			
				$fileObj->setDestination($path);	
				$global_conf = Zend_Registry::get('global_conf');
				if($global_conf['image_renaming_enable'] == '1')
				{		
					$fileInfo 				=		pathinfo($fileObj->getFileName());			
					$newFileName			=		time().'_'.str_replace(' ', '_', $fileInfo['filename']).'.'.$fileInfo['extension'];			
					$fileObj->addFilter('Rename', $newFileName);
				}
				else if($global_conf['image_renaming_enable'] == '2' && !empty($global_conf['image_renaming_prefix']))
				{
					$fileInfo 				=		pathinfo($fileObj->getFileName());			
					$newFileName			=		preg_replace('/[^a-zA-Z0-9_]/','',strip_tags($global_conf['image_renaming_prefix'])).str_replace(' ', '_', $fileInfo['filename']).'.'.$fileInfo['extension'];			
					$fileObj->addFilter('Rename', $newFileName);
				}
			}
			$json_arr = array('status' => 'ok');		
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}
		return $json_arr;
    }
	
	public function isValid($upload_file, $postValue, $fileInfo)
	{			
		try
		{	
			$core_not_permitted_arr = array('php', 'php3', 'php4', 'asp', 'jsp', 'pl', 'py', 'htm', 'shtml', 'sh', 'aspx', 'pjpg', 'bat', 'bin', 'class', 'cmd', 'cnm', 'com', 'cpl', 'dev', 'ocx', 'sys', 'vbe', 'vbs', 'vbx', 'wmf', 'msg', 'nws', 'scr', 'shs', 'smm', 'pcx', 'pgm', 'drv', 'eml', 'exe', 'hlp');		
			//$core_not_permitted_arr = array();
			$translator = Zend_Registry::get('translator');	
			$global_conf = Zend_Registry::get('global_conf');	
			$table_db = new Portfolio_Model_DbTable_Table();
			$postValue = ($postValue['table_name'] == 'file_system') ? $postValue : $table_db->reAssignPostValue($postValue);
			if($this->hasSpecialChar($upload_file->getFileName(null,false)))
			{
				$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_special_char_err', $upload_file->getFileName(null,false)) );
			}
			else
			{
				if(((float)$upload_file->getFileSize() <= (float)$postValue[$postValue['file_max_size_field']]) || (empty($postValue[$postValue['file_max_size_field']])))
				{
					$json_arr = array('status' => 'ok');
					$file_type_arr = ($postValue[$postValue['file_extension_field']]) ? explode(',',$postValue[$postValue['file_extension_field']]) : array();
					$file_name = $upload_file->getFileName();
					$ext = Eicra_File_Utility::GetExtension($file_name);
					
					if($ext)
					{
						if(in_array($ext, $file_type_arr) && !in_array($ext, $core_not_permitted_arr))
						{						
							$mime_type = $fileInfo[$upload_file->getName()]['type'];				
							$mime_type_arr = explode(',', $global_conf['file_mime_type']);
							if(in_array($mime_type, $mime_type_arr) || $mime_type_arr[0] == 'All')
							{
								
								$json_arr = $this->isParfectName($upload_file->getFileName(null,false), $global_conf, $translator);
							}
							else
							{
								$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_mime_type_err', $mime_type));
							}					
						}
						else
						{
							$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_extension_err',  $ext).' '.$translator->translator('portfolio_file_extension_support', str_replace(',', ' | ', $postValue[$postValue['file_extension_field']])));
						}
					}
					else
					{
						$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_no_ext_dy_support'));
					}
				}
				else
				{
					$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_size_err', $upload_file->getFileSize()) . $postValue[$postValue['file_max_size_field']]. ' KB');
				}
			}
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}
		return $json_arr;
	}
	
	public function hasSpecialChar($file_name)
	{
		if(preg_match(Eicra_File_Constants::SPECIAL_CHAR_PATTERN, $file_name))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function isParfectName($file_name, $global_conf, $translator)
	{
		$file_archive_not_supported_file_type_arr = explode(',', $global_conf['file_archive_not_supported_file_type']);
		$perm = true;
		foreach($file_archive_not_supported_file_type_arr as $key => $value)
		{
			if(preg_match('/(\w*[\.]+'.$value.'\w*)\b/', strtolower($file_name)))
			{
				$perm = false;
				break;
			}
		}
		
		$json_arr = ($perm) ? array('status' => 'ok') : array('status' => 'err','msg' => $translator->translator('portfolio_file_support_err', $value).'&lsquo;'.$file_name.'&rsquo;');
		return $json_arr;
	}
	
	public function isSupportedArchive($upload_file, $postValue, $fileInfo)
	{
		$translator = Zend_Registry::get('translator');	
		$file_name = $upload_file->getFileName(null,false);
		$file_name_with_path = $upload_file->getFileName();
		$ext =  Eicra_File_Utility::GetExtension($file_name);
		$mime_type = $upload_file->getMimeType();
		$global_conf = Zend_Registry::get('global_conf');
		$file_archive_type_arr = explode(',', $global_conf['file_archive_type']);
		$file_archive_not_supported_file_type_arr = explode(',', $global_conf['file_archive_not_supported_file_type']);
		if(in_array($ext, $file_archive_type_arr))
		{
			$unzip_result = $this->unzip($file_name_with_path, $ext);
			if($unzip_result['status'] == 'ok')
			{				
				$it = new RecursiveDirectoryIterator(BASE_PATH.DS.'temp'.DS.'extract');
				$check = true;
				foreach(new RecursiveIteratorIterator($it) as $file) 
				{			
					$extension = pathinfo($file, PATHINFO_EXTENSION);						
					if (in_array($extension, $file_archive_not_supported_file_type_arr)) 
					{
						$check = false;		
						break;			
					}			
				}
				$json_arr = ($check === true) ? array('status' => 'ok')  : array('status' => 'err','msg' => $translator->translator('portfolio_file_archive_not_supported_err', $extension));
				if(file_exists($file_name_with_path) && ($check === false))
				{
					Eicra_File_Utility::deleteRescursiveDir($file_name_with_path);	
				}
				Eicra_File_Utility::recursive_remove_directory(BASE_PATH.DS.'temp'.DS.'extract', '', true);
			}
			else
			{
				if(file_exists($file_name_with_path))
				{
					Eicra_File_Utility::deleteRescursiveDir($file_name_with_path);	
				}
				$json_arr = array('status' => 'err','msg' => $translator->translator('portfolio_file_archive_extract_err', $file_name).' '.$unzip_result['msg']);
			}
		}
		else
		{
			$json_arr = array('status' => 'ok');
		}
		return $json_arr;
	}
	
	public function unzip($file, $ext)
	{
		try
		{
			 $dir = $file;
			 $ext = ucfirst( $ext );						 
			 $filter = new Zend_Filter_Decompress(array(
					 'adapter' => $ext, //'Zip', Or 'Tar', or 'Gz'
					 'options' => array(
									 'target' => BASE_PATH.DS.'temp'.DS.'extract'
									 )
			 ));		
			 $filter->filter($dir);
			 $json_arr = array('status' => 'ok');
		}
		catch(Exception $e)
		{
			$json_arr = array('status' => 'err','msg' => $e->getMessage());
		}
		return $json_arr;
	}
	
	public function getFiles($path)
	{
		$files_arr = Eicra_File_Utility::getAllFiles($path);
		
		return $files_arr;
	}
	
	public function getFilePath($file_name, $info)
	{
		$ext = Eicra_File_Utility::GetExtension($file_name);
		$img_path  = "application/modules/Administrator/layouts/scripts/images/common/all_file_thumb.png";		
		switch(strtolower($ext))
		{
			case 'flv':
			case 'swf':
			case 'FLV':
			case 'SWF':
				$img_path = 'application/modules/Administrator/layouts/scripts/images/common/flash.png';
				break;
			case 'mp3':
			case 'MP3':
				$img_path = 'application/modules/Administrator/layouts/scripts/images/common/mp3.png';
				break;
			case 'avi':
			case 'wmv':
			case 'wma':
			case 'WMA':
			case 'AVI':
			case 'WMV':
				$img_path = 'application/modules/Administrator/layouts/scripts/images/common/avi_thumb.png';
				break;
			case 'pdf':
			case 'doc':
			case 'docx':
			case 'xls':
			case 'xlsx':
			case 'ppt':
			case 'pptx':
			case 'PDF':
			case 'DOC':
			case 'DOCX':
			case 'XLS':
			case 'XLSX':
			case 'PPT':
			case 'PPTX':
				$img_path = 'application/modules/Administrator/layouts/scripts/images/common/'.strtolower($ext).'.png';
				break;
			default:
				if($this->isImage($file_name))
				{				
					$img_path = $info[$info['file_path_field']].'/'.$file_name; 
				}
				else
				{	
					$img_path = "application/modules/Administrator/layouts/scripts/images/common/all_file_thumb.png";
				}	
				break;
		}
		return $img_path;
	}
	
	public function isImage($file_name)
	{
		$ext = Eicra_File_Utility::GetExtension($file_name);
		$ext = strtolower($ext);
		if($ext == 'jpg' || $ext == 'png' || $ext == 'gif')
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
}
?>