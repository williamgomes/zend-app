<?php

/**
 * Watermark My Image Standalone Class enables you to add a simple, yet very flexible and customizable watermark to your images.
 *
 * @package Watermark_My_Image
 * @author Marian Bucur <thebigman@marianbucur.com>
 * @version 1.0
 * @copyright Marian Bucur
 */

/**
 * The base class.
 *
 * @package Watermark_My_Image
 */
class Portfolio_Controller_Helper_WatermarkBase
{

  /**
   * The path to the file.
   *
   * @var string
   * @access protected
   */
  protected $file_path;

  /**
   * The file contents.
   *
   * @var string
   * @access protected
   */
  protected $file_contents;

  /**
   * Is the file an animated gif?
   *
   * @var bool
   * @access protected
   */
  protected $is_animated_gif;

  /**
   * Image size.
   *
   * @var array
   * @access protected
   */
  protected $image_size;

  /**
   * The mime type of the image.
   *
   * @var string
   * @access protected
   */
  protected $mime_type;

  /**
   * The original image resource.
   *
   * @var mixed
   * @access protected
   */
  protected $original_image;

  /**
   * The errors array.
   *
   * @var array
   * @access protected
   */
  protected $errors = array();

  /**
   * The class constructor.
   *
   * @param mixed $value The file path, an image resource or false/null for just a preview of the watermark.
   * @access public
   */
  public function __construct($value = null)
  {
    if (is_resource($value) && get_resource_type($value) == 'gd') {

      $this->original_image = $value;

      // We know for sure that the resource is not an animated gif because gd does not support animated gifs.
      $this->is_animated_gif = false;
    } else if ($value) {
      // Check if the path points to a file and set an error if otherwise.
      if (!is_file($value)) {
        return $this->set_error('The specified path does not point to a file!', __FILE__, __LINE__);
      }

      // Check if the file is readable and set an error if otherwise.
      if (!is_readable($value)) {
        return $this->set_error('The file is not readable!', __FILE__, __LINE__);
      }

      $this->file_path = $value;
    } else {
      // We are generating a preview so we know it's not an animated gif.
      $this->is_animated_gif = false;
      // Set the default mime type for the preview to image/png.
      $this->mime_type = 'image/png';
    }
  }

  /**
   * Set the file path.
   *
   * @param mixed $value The file path or false/null for just a preview of the watermark.
   * @access public
   * @return Watermark_My_Image_Base
   */
  public function set_file_path($value = false)
  {
    if ($value) {
      // Check if the path points to a file and set an error if otherwise.
      if (!is_file($value)) {
        return $this->set_error('The specified path does not point to a file!', __FILE__, __LINE__);
      }

      // Check if the file is readable and set an error if otherwise.
      if (!is_readable($value)) {
        return $this->set_error('The file is not readable!', __FILE__, __LINE__);
      }
    }

    $this->file_path = $value;

    // Reset some variables.
    $this->file_contents = null;
    $this->is_animated_gif = null;
    $this->image_size = null;
    $this->mime_type = null;
    $this->original_image = null;
    $this->errors = array();

    return $this;
  }

  /**
   * Set a gd resource to be watermarked.
   *
   * @param resource $value A gd resource.
   * @access public
   * @return Watermark_My_Image_Base
   */
  public function set_resource($value)
  {
    // Check if the value is a gd resource and set an error if otherwise.
    if (is_resource($value) && get_resource_type($value) == 'gd') {

      $this->original_image = $value;

      // We know for sure that resource is not an animated gif because gd does not support animated gifs.
      $this->is_animated_gif = false;

      // Reset some variables.
      $this->file_contents = null;
      $this->image_size = null;
      $this->mime_type = null;
      $this->errors = array();
    } else {
      return $this->set_error('You need to set a gd resource!', __FILE__, __LINE__);
    }

    return $this;
  }

  /**
   * Load the file contents.
   *
   * @access protected
   * @return mixed
   */
  protected function load_file_contents()
  {
    // Check if the file contents were already stored in a variable.
    if (!$this->file_contents) {
      // Check if the file is readable.
      if (is_readable($this->file_path)) {
        // Load the file contents into a variable.
        $this->file_contents = file_get_contents($this->file_path);
      } else {
        // Set an error if the file is not readable.
        return $this->set_error('File does not exist or is unreadable!', __FILE__, __LINE__);
      }
    }

    return true;
  }

  /**
   * Check if the file is an animated gif.
   *
   * @access protected
   * @return mixed
   */
  protected function is_animated_gif()
  {
    // Return true or false if we already know if the file is an animated gif or not.
    if ($this->is_animated_gif !== null)
      return $this->is_animated_gif;

    // We need the file contents to proceed, so we return null if the file cannot be read.
    if (!$this->load_file_contents())
      return null;


    $str_loc = 0;
    $count = 0;

    // There is no point in continuing after we find a 2nd frame.
    while ($count < 2) {

      $where1 = strpos($this->file_contents, "\x00\x21\xF9\x04", $str_loc);

      if ($where1 === false) {
        break;
      } else {

        $str_loc = $where1 + 1;
        $where2 = strpos($this->file_contents, "\x00\x2C", $str_loc);

        if ($where2 === false) {
          break;
        } else {
          if ($where1 + 8 == $where2) {
            $count++;
          }
          $str_loc = $where2 + 1;
        }

      }
    }

    return $this->is_animated_gif = $count > 1 ? true : false;
  }

  /**
   * Get the image size.
   *
   * @access protected
   * @return mixed
   */
  protected function get_image_size()
  {
    if ($this->image_size)
      return $this->image_size;

    // Get the image info
    @$this->image_size = getimagesize($this->file_path);

    // An error occurred while trying to get the image info.
    if (!$this->image_size)
      return $this->set_error('File type not supported!', __FILE__, __LINE__);

    return $this->image_size;
  }

  /**
   * Get the mime type of the file.
   *
   * @access protected
   * @return mixed
   */
  protected function get_mime_type()
  {
    // Return the mime type of the file if we already know it.
    if ($this->mime_type)
      return $this->mime_type;

    // Check if the file is readable.
    if (is_readable($this->file_path)) {
      // Get the image info
      $image_size = $this->get_image_size();

      if ($image_size) {
        // Set the mime type.
        return $this->mime_type = $image_size['mime'];
      }

    } else {
      // Set an error if the file is not readable.
      return $this->set_error('File does not exist or is unreadable!', __FILE__, __LINE__);
    }
  }

  /**
   * Creates a gd resource from the file found at $this->file_path and returns it.
   *
   * If the file is not a supported file type, the function will set an error and return null.
   *
   * @access protected
   * @return mixed
   */
  protected function get_image_resource()
  {
    // Check if the original image was already stored in a variable and return it if so.
    if ($this->original_image)
      return $this->original_image;

    if ($this->get_mime_type()) {
      switch ($this->mime_type) {
        case 'image/jpeg':
          return $this->original_image = imagecreatefromjpeg($this->file_path);
        case 'image/png':
          return $this->original_image = imagecreatefrompng($this->file_path);
        case 'image/gif':
          return $this->original_image = imagecreatefromgif($this->file_path);
        default:
          return $this->set_error('File type not supported!', __FILE__, __LINE__);
      }
    }
  }

  /**
   * Get the errors.
   *
   * @access public
   * @return array
   */
  public function get_errors()
  {
    return $this->errors;
  }

  /**
   * Converts the color from hex to rgb.
   *
   * @param string $hex
   * @access protected
   * @return array
   */
  protected function hex_to_rgb($hex)
  {
    $dec = hexdec($hex);

    return array(0xFF & ($dec >> 0x10), 0xFF & ($dec >> 0x8), 0xFF & $dec);
  }

  /**
   * Set the errors.
   *
   * @param string $error The error message
   * @param string $file
   * @param int $line
   * @access protected
   * @return null
   */
  protected function set_error($error, $file = null, $line = null)
  {
    if ($file) {
      $error .= ' File: ' . $file;
    }

    if ($line) {
      $error .= ' Line: ' . $line;
    }

    // Check if the message already exists in the errors array and add if it does not
    if (!in_array($error, $this->errors)) {
      $this->errors[] = $error;
    }

    return null;
  }
}
