<?php
class Autos_Model_StateListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_State');
        }
        return $this->_dbTable;
    }
	 	
	
	
	public function fetchAll($pageNumber,$country_id = null)
    {		
        $resultSet = $this->getDbTable()->getAllStateInfo($country_id); 		     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);		
        return $paginator;
    }
}
?>