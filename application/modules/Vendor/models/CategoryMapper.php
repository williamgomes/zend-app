<?php
class Autos_Model_CategoryMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_Category');
        }
        return $this->_dbTable;
    }
	
	public function save(Autos_Model_Catagories $obj)
	{	
		$data = array(); 
		if ((null === ($id = $obj->getId())) || empty($id)) 
		{			
			try 
			{
				unset($data['id']);
			
				$data = array(					
					'group_id' 			=> 	$obj->getGroup_id(),
					'entry_by' 			=> 	$obj->getEntry_by(),
					'category_name' 	=>	$obj->getCategory_name(),
					'category_title' 	=>	$obj->getCategory_title(),
					'category_thumb' 	=>	$obj->getCategory_thumb(),
					'category_desc' 	=>	$obj->getCategory_desc(),
					'parent' 			=>	$obj->getParent(),
					'meta_title'		=>	$obj->getMeta_title(),
					'meta_keywords'		=>	$obj->getMeta_keywords(),
					'meta_desc'			=>	$obj->getMeta_desc(),
					'category_order'	=>	$obj->getCategory_order(),	
					'cat_date' 			=>	date("Y-m-d h:i:s"),				
					'featured' 			=>	$obj->getFeatured(),
					'active' 			=>	$obj->getActive()					
				);
			
				$last_id = $this->getDbTable()->insert($data);	
				$result = array('status' => 'ok' ,'id' => $last_id);	
			}
			catch (Exception $e)
			{
				$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
			}						
		} 
		else 
		{			
			try 
			{	
				if(($obj->getParent() == $obj->getPrev_parent()) && ($obj->getGroup_id() == $obj->getPrev_group()))
				{
					$data = array(					
						'group_id' 			=> 	$obj->getGroup_id(),
						'category_name' 	=>	$obj->getCategory_name(),
						'category_title' 	=>	$obj->getCategory_title(),
						'category_thumb' 	=>	$obj->getCategory_thumb(),
						'meta_title'		=>	$obj->getMeta_title(),
						'meta_keywords'		=>	$obj->getMeta_keywords(),
						'meta_desc'			=>	$obj->getMeta_desc(),
						'category_desc' 	=>	$obj->getCategory_desc(),
						'parent' 			=>	$obj->getParent()					
					);
				}
				else
				{
					$data = array(					
						'group_id' 			=> 	$obj->getGroup_id(),
						'category_name' 	=>	$obj->getCategory_name(),
						'category_title' 	=>	$obj->getCategory_title(),
						'category_thumb' 	=>	$obj->getCategory_thumb(),
						'category_desc' 	=>	$obj->getCategory_desc(),
						'meta_title'		=>	$obj->getMeta_title(),
						'meta_keywords'		=>	$obj->getMeta_keywords(),
						'meta_desc'			=>	$obj->getMeta_desc(),
						'parent' 			=>	$obj->getParent(),
						'category_order'	=>	$obj->getCategory_order()						
					);
				}
				
				// Start the Update process		
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$result = array('status' => 'ok' ,'id' => $id);	
			} 
			catch (Exception $e) 
			{
				$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
			}				
		}
		return $result;
	}
}
?>