<?php
class Autos_Model_PreferencesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Oops ! Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_Preferences');
        }
        return $this->_dbTable;
    }

    public function save(Autos_Model_Preferences $obj)
    {
        $data_arr = $obj->getPreferences();
        $sql = null;
        if($data_arr && is_array($data_arr))
        {
            // Start the Update process
            try
            {
                foreach($data_arr as $key => $value)
                {

                        $data = array('value' => $value);
                        $where = array('name = ?' => $key);
                        $this->getDbTable()->update($data, $where);
                        $isSuccess = $this->getDbTable()->fetchRow($where);
                        if (empty($isSuccess)){
                        	$data['name'] = $key;
                        	$this->getDbTable()->insert($data, $where);
                        }
                }
                $json_arr = array('status' => 'ok');
            }
            catch (Exception $e)
            {

                $json_arr = array('status' => 'err','msg' => $e->getMessage() );
            }
        }
        else
        {

            $json_arr = array('status' => 'err','msg' => 'Oops : Invalid Data inputs');
        }
        return $json_arr;
    }

}