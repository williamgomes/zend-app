<?php
/**
* This is the DbTable class for the autos_spare_parts_saved table.
*/
class Autos_Model_DbTable_SavedSpareParts extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_spare_parts_saved';	
	protected $_cols	=	null;	
		
	//Get All Datas
	public function getInfoByUser($user_id = null) 
    {		
		if(empty($user_id))
		{
			$select = $this->select()
						   ->from(array('gp' => $this->_name),array('*'))
						   ->order("gp.id DESC"); 		
		}
		else
		{
			$select = $this->select()
						   ->from(array('gp' => $this->_name),array('*'))
						   ->where('gp.user_id =?',$user_id)
						   ->order("gp.id DESC"); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('asps' => $this->_name),array('asps.*'))
						   ->joinLeft(array('asp' => Zend_Registry::get('dbPrefix').'autos_spare_parts'), 'asp.id = asps.spare_parts_id', array( 'spare_parts_name' => 'asp.spare_parts_name', 'spare_parts_title' => 'asp.spare_parts_title', 'spare_parts_image_primary' => 'asp.spare_parts_image_primary', 'spare_parts_image' => 'asp.spare_parts_image', 'entry_date' => 'asp.entry_date', 'spare_parts_base_price' => 'asp.spare_parts_base_price', 'spare_parts_promotion_price' => 'asp.spare_parts_promotion_price', 'autos_agent' => 'asp.autos_agent' ))
						   ->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'asp.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))						   
						   ->joinLeft(array('ups' => Zend_Registry::get('dbPrefix').'user_profile'), 'asp.autos_agent = ups.user_id', array( 'owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) "))						   
						   ->joinLeft(array('upf' => Zend_Registry::get('dbPrefix').'user_profile'), 'asps.user_id = upf.user_id', array( 'who_save' => " CONCAT(upf.title, ' ', upf.firstName, ' ', upf.lastName) "));
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('asps.user_id = ?' , $user_id );
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("asps.id DESC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{										
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("asps.id DESC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'asps.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'who_save' :
					$operatorFirstPart = " CONCAT(upf.title, ' ', upf.firstName, ' ', upf.lastName) ";
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(asp.entry_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(asp.entry_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(asp.entry_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}

?>