<?php
class Autos_Model_DbTable_Preferences  extends Eicra_Abstract_DbTable
{
	protected $_name    =  'autos_preferences';

	//Get Datas
	public function getInfo($id)
	{
		$row = $this->fetchRow('id = ' . $id);
		if (!$row)
		{
			$options = null;
		}
		else
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
		return   $options ;
	}

	public function getSettingByKey ($key)
    {
        try {
             $select = $this->select()
    		            ->from($this->_name, array('*'))
    		            ->where('name =?', $key) ;

            $row = $this->fetchRow($select);
            $options = $row->toArray();

            if (!empty($options) && count ($options) > 0 ) {
                $options = is_array($options) ? array_map('stripslashes',
                        $options) : stripslashes($options);
                return $options;
            } else {
                return null;
            }
        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }


	//Get Datas
	public function getOptions()
	{
		$data_arr = array();
		$fetchData = $this->fetchAll();
		if($fetchData)
		{
			foreach($fetchData as $datas)
			{
				$data_arr[$datas['name']] = stripslashes($datas['value']);
			}
		}
		return $data_arr;
	}

	public function setDatabaseForm($elements)
	{
		try
		{
			if($elements)
			{
				foreach($elements as $element)
				{
					$key	=	$element->getName();
					$value	=	$element->getValue();

					if(!$this->fieldExists($key))
					{
						$this->insert(array('name' => $key, 'value' => $value));
					}
				}
			}
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	}

	private function fieldExists($key)
	{
		$validator = new Zend_Validate_Db_RecordExists(
				array(
						'table' 	=> 		$this->_name,
						'field' 	=> 		'name'
				)
		);
		return $validator->isValid($key);
	}



}