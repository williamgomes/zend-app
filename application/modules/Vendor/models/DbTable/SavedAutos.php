<?php
/**
* This is the DbTable class for the property_saved table.
*/
class Autos_Model_DbTable_SavedAutos extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_saved';	
	protected $_cols	=	null;	
		
	//Get All Datas
	public function getAutosInfoByUser($user_id = null) 
    {		
		if(empty($user_id))
		{
			$select = $this->select()
						   ->from(array('gp' => $this->_name),array('*'))
						   ->order("gp.id DESC"); 		
		}
		else
		{
			$select = $this->select()
						   ->from(array('gp' => $this->_name),array('*'))
						   ->where('gp.user_id =?',$user_id)
						   ->order("gp.id DESC"); 
		}
		
        $options = $this->fetchAll($select);		
        if (!$options) 
		{
            //throw new Exception("Count not find row Category");
			$options = null;
        }		
        return $options;    
    }
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('as' => $this->_name),array('as.*'))
						   ->joinLeft(array('ap' => Zend_Registry::get('dbPrefix').'autos_page'), 'ap.id = as.autos_id', array( 'group_id' => 'ap.group_id', 'autos_model_name' => 'ap.autos_model_name', 'autos_model_title' => 'ap.autos_model_title', 'autos_primary_image' => 'ap.autos_primary_image', 'autos_image' => 'ap.autos_image', 'autos_date' => 'ap.autos_date', 'post_code' => 'ap.post_code', 'autos_price' => 'ap.autos_price', 'feature_mileage' => 'ap.feature_mileage' ))
						   ->joinLeft(array('ag' => Zend_Registry::get('dbPrefix').'autos_group'), 'ap.group_id = ag.id', array( 'group_name' => 'ag.autos_name'))
						   ->joinLeft(array('ac' =>  Zend_Registry::get('dbPrefix').'autos_category'), 'ac.id = ap.category_id', array( 'category_name' => 'ac.category_name'))
						   ->joinLeft(array('ab' =>  Zend_Registry::get('dbPrefix').'autos_brand'), 'ab.id = ap.brand_id', array( 'brand_name' => 'ab.brand_name'))
						   ->joinLeft(array('ct' =>  Zend_Registry::get('dbPrefix').'cities'), 'ct.city_id = ap.area_id', array( 'city' => 'ct.city'))
						   ->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'ap.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "))						   
						   ->joinLeft(array('ups' => Zend_Registry::get('dbPrefix').'user_profile'), 'ap.autos_agent = ups.user_id', array( 'owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) "))						   
						   ->joinLeft(array('upf' => Zend_Registry::get('dbPrefix').'user_profile'), 'as.user_id = upf.user_id', array( 'who_save' => " CONCAT(upf.title, ' ', upf.firstName, ' ', upf.lastName) "));
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('as.user_id = ?' , $user_id );
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("as.id DESC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{										
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("as.id DESC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'as.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'who_save' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(ap.autos_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(ap.autos_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(ap.autos_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}

?>