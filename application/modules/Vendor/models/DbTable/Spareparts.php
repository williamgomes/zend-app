<?php
/**
* This is the DbTable class for the autos_spare_parts table.
*/
class Autos_Model_DbTable_Spareparts extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_spare_parts';			
	protected $_cols	=	null;	
		
	//Get Datas
	public function getInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row=$id");
		   $options = null;
        }
		else 
		{
			$options = $row->toArray();
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return $options;  		  
    }
	
	
	
	//Get Datas
	public function getTitleToId($spare_parts_title) 
    {
		$spare_parts_title = addslashes($spare_parts_title);
		try
		{
			$select = $this->select()
							   ->from(array('asp' => $this->_name),array('*'))
							   ->where('asp.spare_parts_title = ? ',$spare_parts_title)
							   ->limit(1); 			
			$options = $this->fetchAll($select);
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	public function getBorderPrice($field = 'spare_parts_base_price', $range = 'DESC') 
    {		
		try
		{
			$select = $this->select()
							   ->from(array('asp' => $this->_name),array($field => 'asp.'.$field))
							   ->where('asp.active =? ', '1')
							   ->order('asp.'.$field.' '.$range)
							   ->limit(1); 			
			$options = $this->fetchAll($select);
			if($options)
			{
				return $options[0][$field]; 
			}
			else
			{
				return '0';
			}
		}
		catch(Exception $e)
		{
			return '0';
		}		 		  
    }
	
	public function numOfSpareParts()
	{
		$auth = Zend_Auth::getInstance ();
		if($auth->hasIdentity ())
		{
			$identity = $auth->getIdentity ();
			$user_id = $identity->user_id;
			
			$select = $this->select()
						   ->from(array('asp' => $this->_name),array('COUNT(*) AS num_spare_parts'))
						   ->where('asp.autos_agent =?',$user_id)
						   ->orwhere('asp.entry_by =?',$user_id); 
			 $options = $this->fetchAll($select);			
			 return ($options)? $options[0]['num_spare_parts'] : 0;
		}
		else
		{
			$select = $this->select()
						   ->from(array('asp' => $this->_name),array('COUNT(*) AS num_spare_parts')); 
			 $options = $this->fetchAll($select);			
			 return ($options)? $options[0]['num_spare_parts'] : 0;
		}
	}
	
	//Get Next Spare Part
	public function getNextPart($part_id) 
    {		
		try
		{			
		 	$select = $this->select()
							 ->from(array('asp' => $this->_name),array('asp.*'))
							 ->where('asp.id > ? ',$part_id)				 
							 ->order('asp.id ASC')
							 ->limit(1);						
		
			$row = $this->getAdapter()->fetchAll($select); 	
			
			if($row)
			{
				$options = ($row[0]) ? $row[0] : '';
			}
			else
			{
				$options = '';
			}
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }
	
	//Get Previous Autos
	public function getPrevPart($part_id) 
    {		
		try
		{					
			$select = $this->select()
							   ->from(array('asp' => $this->_name),array('asp.*'))
							   ->where('asp.id < ? ',$part_id)
							   ->order('asp.id DESC')
							   ->limit(1); 
			$row = $this->getAdapter()->fetchAll($select);			
			if($row)
			{
				$options = $row[0];
			}
			else
			{
				$options = '';
			}
			return $options; 
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		 		  
    }	
		
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null,  $tableColumns = null) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
		
		$autos_spare_parts_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['autos_spare_parts'] && is_array($tableColumns['autos_spare_parts'])) ? $tableColumns['autos_spare_parts'] : array('asp.*');
        $vote_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['vote_voting'] && is_array($tableColumns['vote_voting'])) ? $tableColumns['vote_voting'] : null;
		$user_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) ", 'phone' => 'up.phone', 'postalCode' => 'up.postalCode', 'address' => 'up.address');
		$owner_profile_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_profile'] && is_array($tableColumns['owner_profile'])) ? $tableColumns['owner_profile'] : array( 'owner_name' => " CONCAT(ups.title, ' ', ups.firstName, ' ', ups.lastName) ", 'owner_email' => 'ups.username', 'owner_id' => 'ups.user_id', 'owner_phone' => 'ups.phone', 'owner_postalCode' => 'ups.postalCode', 'owner_address' => 'ups.address', 'country' => 'ups.country');
		$owner_country_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['owner_country'] && is_array($tableColumns['owner_country'])) ? $tableColumns['owner_country'] : null;
		$dataLimit = ($tableColumns && is_array($tableColumns) && $tableColumns['dataLimit']) ? $tableColumns['dataLimit'] : null;
		$userChecking	=  ($tableColumns && is_array($tableColumns) && is_bool($tableColumns['userChecking'])) ? $tableColumns['userChecking'] : true;
		$whereClause = ($tableColumns && is_array($tableColumns) && $tableColumns['where'] && is_array($tableColumns['where'])) ? $tableColumns['where'] : null;	
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('asp' => $this->_name), $autos_spare_parts_column_arr);
						  		
		
		if (($user_profile_column_arr &&  is_array($user_profile_column_arr) && count( $user_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'asp.entry_by = up.user_id', $user_profile_column_arr);				   
		}
		
		if (($owner_profile_column_arr &&  is_array($owner_profile_column_arr) && count( $owner_profile_column_arr) > 0)) 
		{
			$select->joinLeft(array('ups' => Zend_Registry::get('dbPrefix').'user_profile'), 'asp.autos_agent = ups.user_id', $owner_profile_column_arr);
		}
		
		if (($owner_country_column_arr &&  is_array($owner_country_column_arr) && count( $owner_country_column_arr) > 0)) 
		{
			$select->joinLeft(array('ocut' =>  Zend_Registry::get('dbPrefix').'countries'), 'ocut.id = ups.country', $owner_country_column_arr);
		}

		if (($vote_column_arr &&  is_array($vote_column_arr) && count( $vote_column_arr) > 0)) 
		{
            $select ->joinLeft(array( 'vt' => Zend_Registry::get('dbPrefix') . 'vote_voting' ), 'asp.id  = vt.table_id', $vote_column_arr);
        }
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('asp.entry_by = ' . $user_id . ' OR asp.autos_agent = ' . $user_id);
		}
		
		if($approve != null)
		{
			$select->where("asp.active = ?", $approve);
		}

        if (($whereClause &&  is_array($whereClause) && count( $whereClause) > 0)) 
		{

            foreach ($whereClause as $filter => $param)
			{
                $select->where($filter , $param);
            }
        }
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['dir'] == 'exp')
					{
						$select->order(new Zend_Db_Expr($sort_value_arr['field']));
					}
					else
					{
						$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
					}					
				}
			}
			else
			{
				$select->order("asp.entry_date DESC"); 	
			}
			
			if ($search_params['filter'] && $search_params['filter']['filters']) 
			{
                $search_services = new Search_Service_Services($this);
                $where = $search_services->getSearchWhereClause($search_params);
                if (! empty($where)) 
				{
                    $select->where($where);
                }
            }
		}
		else
		{
			$select->order("asp.entry_date DESC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	public function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'asp.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); 
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'autos_id_string' :
					$id_arr = explode(',', $operator_arr['value']);			
					$id_arr = array_filter( $id_arr );				
					$ids = implode(',', $id_arr);
					$operatorFirstPart =  " asp.id IN(".$ids.") AND 1";
					$operator_arr['value'] = '1';
				break;	
			case 'spare_parts_base_price-gte' :
					$operator_arr['field'] = 'spare_parts_base_price';
					$operator_arr['operator'] = 'gte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'spare_parts_base_price-lte' :
					$operator_arr['field'] = 'spare_parts_base_price';
					$operator_arr['operator'] = 'lte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;		
			case 'spare_parts_promotion_price-gte' :
					$operator_arr['field'] = 'spare_parts_promotion_price';
					$operator_arr['operator'] = 'gte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'spare_parts_promotion_price-lte' :
					$operator_arr['field'] = 'spare_parts_promotion_price';
					$operator_arr['operator'] = 'lte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;		
			case 'spare_parts_order_qty_min-gte' :
					$operator_arr['field'] = 'spare_parts_order_qty_min';
					$operator_arr['operator'] = 'gte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;	
			case 'spare_parts_order_qty_min-lte' :
					$operator_arr['field'] = 'spare_parts_order_qty_min';
					$operator_arr['operator'] = 'lte';
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
			case 'total_votes':
                $operatorFirstPart = " (SELECT SUM(vts.vote_value) FROM ".Zend_Registry::get('dbPrefix')."vote_voting as vts WHERE asp.id  = vts.table_id) AS total_votes ";
                break;
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(asp.entry_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(asp.entry_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(asp.entry_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	public function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}  
}

?>