<?php
/**
* This is the DbTable class for the autos_page table.
*/
class Autos_Model_DbTable_Search extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_page';
		
	//Get Datas
	public function getSearchInfo($where,$order) 
    {
	   if($where)
	   {
		   $select = $this->select()
						   ->from($this->_name, array('*'))
						   ->where($where)
						   ->order($order);
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('*'))
						   ->order($order);
		}
		
		$options = $select->query()->fetchAll();
		if (!$options) 
		{
            $options = null;
        }		
        return $options;   
    }
	
}

?>