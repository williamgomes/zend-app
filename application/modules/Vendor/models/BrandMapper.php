<?php
class Autos_Model_BrandMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_Brand');
        }
        return $this->_dbTable;
    }
	
	public function save(Autos_Model_Brand $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				unset($data['id']);
				
				$data = array(					
					'entry_by' 					=> 	$obj->getEntry_by(),
					'brand_name' 				=>	$obj->getBrand_name(),	
					'entry_date' 				=>	date("Y-m-d h:i:s")										
				);
				try 
				{
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{				
				$data = array(					
					'entry_by' 					=> 	$obj->getEntry_by(),
					'brand_name' 			=>	$obj->getBrand_name()						
				);				
				// Start the Update process
				try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>