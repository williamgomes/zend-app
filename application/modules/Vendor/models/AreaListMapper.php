<?php
class Autos_Model_AreaListMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Autos_Model_DbTable_Area');
        }
        return $this->_dbTable;
    }
	 	
	
	
	public function fetchAll($pageNumber,$state_id = null)
    {		
        $resultSet = $this->getDbTable()->getAllAreaInfo($state_id); 		     
		$viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
		$paginator = Zend_Paginator::factory($resultSet);
		$paginator->setItemCountPerPage($viewPageNum);
    	$paginator->setCurrentPageNumber($pageNumber);		
        return $paginator;
    }
}
?>