<?php
class Autos_Model_Groups
{
	protected $_id;
	protected $_entry_by;
	protected $_autos_name;
	protected $_role_id;
	protected $_review_id;
	protected $_group_type;
	protected $_calendar_on_off;
	protected $_category_panel;
	protected $_category_on_off;
	protected $_horizontal_panel;
	protected $_num_of_pic_show;
	protected $_dynamic_form;
	protected $_meta_title;
	protected $_meta_keywords;
	protected $_meta_desc;
	protected $_cat_num_per_page;
	protected $_cat_col_num;
	protected $_cat_sort;
	protected $_cat_order;
	protected $_latest_cat_on;
	protected $_latest_cat_num_per_page;
	protected $_latest_cat_sort;
	protected $_latest_cat_order;
	protected $_fearured_cat_on;
	protected $_fearured_cat_num_per_page;
	protected $_fearured_cat_sort;
	protected $_fearured_cat_order;
	protected $_file_num_per_page;
	protected $_file_col_num;
	protected $_file_sort;
	protected $_file_order;
	protected $_light_box_on;
	protected $_latest_file_on;
	protected $_latest_file_num_per_page;
	protected $_latest_file_sort;
	protected $_latest_file_order;
	protected $_featured_file_on;
	protected $_featured_file_num_per_page;
	protected $_featured_file_sort;
	protected $_featured_file_order;
	protected $_file_type;
	protected $_file_size_max;
	protected $_file_cat_thumb_width;
	protected $_file_cat_thumb_height;
	protected $_file_cat_thumb_resize_func;
	protected $_file_thumb_width;
	protected $_file_thumb_height;
	protected $_file_thumb_resize_func;
	protected $_img_big_width;
	protected $_img_big_height;
	protected $_img_big_resize_func;	
	protected $_active;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth autos');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveAutosGroup()
		{
			$mapper  = new Autos_Model_GroupMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setEntry_by()
		{
			$auth = Zend_Auth::getInstance ();
		   	if ($auth->hasIdentity ())
			{
				$globalIdentity = $auth->getIdentity ();
				$this->_entry_by = $globalIdentity->user_id;
			}
			else
			{
				$this->_entry_by = '1';
			}			
			return $this;
		}
		
		public function setAutos_name($text)
		{
			$this->_autos_name =  $text;
			return $this;
		}
		
		public function setRole_id($text)
		{
			$this->_role_id = $text;
			return $this;
		}
		
		public function setReview_id($text)
		{
			$this->_review_id = $text;
			return $this;
		}
		
		public function setGroup_type($text)
		{
			$this->_group_type = $text;
			return $this;
		}
		
		public function setDynamic_form($text)
		{
			$this->_dynamic_form = $text;
			return $this;
		}
		
		public function setMeta_title($text)
		{
			$this->_meta_title = $text;
			return $this;
		}
		
		public function setMeta_keywords($text)
		{
			$this->_meta_keywords = $text;
			return $this;
		}
		
		public function setMeta_desc($text)
		{
			$this->_meta_desc = $text;
			return $this;
		}
		
		public function setCalendar_on_off($text)
		{
			$this->_calendar_on_off = $text;
			return $this;
		}
		
		public function setCategory_on_off($text)
		{
			$this->_category_on_off = $text;
			return $this;
		}
		
		public function setCategory_panel($text)
		{
			$this->_category_panel = $text;
			return $this;
		}
		
		public function setHorizontal_panel($text)
		{
			$this->_horizontal_panel = $text;
			return $this;
		}
		
		public function setNum_of_pic_show($text)
		{
			$this->_num_of_pic_show = $text;
			return $this;
		}
		
		public function setCat_num_per_page($text)
		{
			$this->_cat_num_per_page = $text;
			return $this;
		}
		
		public function setCat_col_num($text)
		{
			$this->_cat_col_num = $text;
			return $this;
		}
		
		public function setCat_sort($text)
		{
			$this->_cat_sort = $text;
			return $this;
		}
		
		public function setCat_order($text)
		{
			$this->_cat_order = $text;
			return $this;
		}
		
		public function setLatest_cat_on($text)
		{
			$this->_latest_cat_on = $text;
			return $this;
		}
		
		public function setLatest_cat_num_per_page($text)
		{
			$this->_latest_cat_num_per_page = $text;
			return $this;
		}
		
		public function setLatest_cat_sort($text)
		{
			$this->_latest_cat_sort = $text;
			return $this;
		}
		
		public function setLatest_cat_order($text)
		{
			$this->_latest_cat_order = $text;
			return $this;
		}
		
		public function setFearured_cat_on($text)
		{
			$this->_fearured_cat_on = $text;
			return $this;
		}
		
		public function setFearured_cat_num_per_page($text)
		{
			$this->_fearured_cat_num_per_page = $text;
			return $this;
		}
		
		public function setFearured_cat_sort($text)
		{
			$this->_fearured_cat_sort = $text;
			return $this;
		}
		
		public function setFearured_cat_order($text)
		{
			$this->_fearured_cat_order = $text;
			return $this;
		}
		
		public function setFile_num_per_page($text)
		{
			$this->_file_num_per_page = $text;
			return $this;
		}
		
		public function setFile_col_num($text)
		{
			$this->_file_col_num = $text;
			return $this;
		}
		
		public function setFile_sort($text)
		{
			$this->_file_sort = $text;
			return $this;
		}
		
		public function setFile_order($text)
		{
			$this->_file_order = $text;
			return $this;
		}
		
		public function setLight_box_on($text)
		{
			$this->_light_box_on = $text;
			return $this;
		}
		
		public function setLatest_file_on($text)
		{
			$this->_latest_file_on = $text;
			return $this;
		}
		
		public function setLatest_file_num_per_page($text)
		{
			$this->_latest_file_num_per_page = $text;
			return $this;
		}
		
		public function setLatest_file_sort($text)
		{
			$this->_latest_file_sort = $text;
			return $this;
		}
		
		public function setLatest_file_order($text)
		{
			$this->_latest_file_order = $text;
			return $this;
		}
		
		public function setFeatured_file_on($text)
		{
			$this->_featured_file_on = $text;
			return $this;
		}
		
		public function setFeatured_file_num_per_page($text)
		{
			$this->_featured_file_num_per_page = $text;
			return $this;
		}
		
		public function setFeatured_file_sort($text)
		{
			$this->_featured_file_sort = $text;
			return $this;
		}
		
		public function setFeatured_file_order($text)
		{
			$this->_featured_file_order = $text;
			return $this;
		}
		
		public function setFile_type($text)
		{
			if(is_array($text))
			{					
				$file_type = implode(',',$text);
			}
			else
			{
				$file_type = $text;
			}
			$this->_file_type = $file_type;
			return $this;
		}		
		
		
		public function setFile_size_max($text)
		{
			$this->_file_size_max  = $text;
			return $this;
		}
				
		public function setFile_cat_thumb_width($text)
		{
			$this->_file_cat_thumb_width = $text;
			return $this;
		}
		
		public function setFile_cat_thumb_height($text)
		{
			$this->_file_cat_thumb_height = $text;
			return $this;
		}
		
		public function setFile_cat_thumb_resize_func($text)
		{
			$this->_file_cat_thumb_resize_func = $text;
			return $this;
		}
		
		public function setFile_thumb_width($text)
		{
			$this->_file_thumb_width = $text;
			return $this;
		}
		
		public function setFile_thumb_height($text)
		{
			$this->_file_thumb_height = $text;
			return $this;
		}
		
		public function setFile_thumb_resize_func($text)
		{
			$this->_file_thumb_resize_func = $text;
			return $this;
		}
		
		public function setImg_big_width($text)
		{
			$this->_img_big_width = $text;
			return $this;
		}
		
		public function setImg_big_height($text)
		{
			$this->_img_big_height = $text;
			return $this;
		}
		
		public function setImg_big_resize_func($text)
		{
			$this->_img_big_resize_func = $text;
			return $this;
		}
		
		public function setActive($text)
		{
			$this->_active = $text;
			return $this;
		}
		
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getEntry_by()
		{   
			if(empty($this->_entry_by))
			{
				$this->setEntry_by(); 
			}     
			return $this->_entry_by;
		}
		
		public function getAutos_name()
		{         
			return $this->_autos_name;
		}
		
		public function getRole_id()
		{         
			return $this->_role_id;
		}
		
		public function getReview_id()
		{         
			return $this->_review_id;
		}
		
		public function getGroup_type()
		{         
			return $this->_group_type;
		}
		
		public function getCalendar_on_off()
		{         
			return $this->_calendar_on_off;
		}
		
		public function getCategory_on_off()
		{         
			return $this->_category_on_off;
		}
		
		public function getCategory_panel()
		{         
			return $this->_category_panel;
		}
		
		public function getHorizontal_panel()
		{         
			return $this->_horizontal_panel;
		}
		
		public function getDynamic_form()
		{         
			return $this->_dynamic_form;
		}
		
		public function getNum_of_pic_show()
		{         
			return $this->_num_of_pic_show;
		}
		
		public function getMeta_title()
		{         
			return $this->_meta_title;
		}
		
		public function getMeta_keywords()
		{         
			return $this->_meta_keywords;
		}
		
		public function getMeta_desc()
		{         
			return $this->_meta_desc;
		}
		
		public function getCat_num_per_page()
		{         
			return $this->_cat_num_per_page;
		}
		
		public function getCat_col_num()
		{         
			return $this->_cat_col_num;
		}
		
		public function getCat_sort()
		{         
			return $this->_cat_sort;
		}
		
		public function getCat_order()
		{         
			return $this->_cat_order;
		}
		
		public function getLatest_cat_on()
		{         
			return $this->_latest_cat_on;
		}
		
		public function getLatest_cat_num_per_page()
		{         
			return $this->_latest_cat_num_per_page;
		}
		
		public function getLatest_cat_sort()
		{         
			return $this->_latest_cat_sort;
		}
		
		public function getLatest_cat_order()
		{         
			return $this->_latest_cat_order;
		}
		
		public function getFearured_cat_on()
		{         
			return $this->_fearured_cat_on;
		}
		
		public function getFearured_cat_num_per_page()
		{         
			return $this->_fearured_cat_num_per_page;
		}
		
		public function getFearured_cat_sort()
		{         
			return $this->_fearured_cat_sort;
		}
		
		public function getFearured_cat_order()
		{         
			return $this->_fearured_cat_order;
		}
		
		public function getFile_num_per_page()
		{         
			return $this->_file_num_per_page;
		}
		
		public function getFile_col_num()
		{         
			return $this->_file_col_num;
		}
		
		public function getFile_sort()
		{         
			return $this->_file_sort;
		}				
		
		public function getFile_order()
		{         
			return $this->_file_order;
		}
		
		public function getLight_box_on()
		{         
			return $this->_light_box_on;
		}
		
		public function getLatest_file_on()
		{         
			return $this->_latest_file_on;
		}
		
		public function getLatest_file_num_per_page()
		{         
			return $this->_latest_file_num_per_page;
		}
		
		public function getLatest_file_sort()
		{         
			return $this->_latest_file_sort;
		}
		
		public function getLatest_file_order()
		{         
			return $this->_latest_file_order;
		}
		
		public function getFeatured_file_on()
		{         
			return $this->_featured_file_on;
		}
		
		public function getFeatured_file_num_per_page()
		{         
			return $this->_featured_file_num_per_page;
		}
		
		public function getFeatured_file_sort()
		{         
			return $this->_featured_file_sort;
		}
		
		public function getFeatured_file_order()
		{         
			return $this->_featured_file_order;
		}
		
		public function getFile_type()
		{         
			return $this->_file_type;
		}
		
		public function getFile_size_max()
		{         
			return $this->_file_size_max ;
		}
		
		public function getFile_cat_thumb_width()
		{         
			return $this->_file_cat_thumb_width;
		}		
		
		public function getFile_cat_thumb_height()
		{         
			return $this->_file_cat_thumb_height;
		}
		
		public function getFile_cat_thumb_resize_func()
		{         
			return $this->_file_cat_thumb_resize_func;
		}
		
		public function getFile_thumb_width()
		{         
			return $this->_file_thumb_width;
		}
		
		public function getFile_thumb_height()
		{         
			return $this->_file_thumb_height;
		}
		
		public function getFile_thumb_resize_func()
		{         
			return $this->_file_thumb_resize_func;
		}
		
		public function getImg_big_width()
		{         
			return $this->_img_big_width;
		}
		
		public function getImg_big_height()
		{         
			return $this->_img_big_height;
		}
		
		public function getImg_big_resize_func()
		{         
			return $this->_img_big_resize_func;
		}
		
		public function getActive()
		{
			return $this->_active;
		}
		
}
?>