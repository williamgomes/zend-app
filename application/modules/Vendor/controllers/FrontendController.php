<?php

class Vendor_FrontendController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_controllerCache;
    private $_auth_obj;
    private $currency;
    private $translator;
    private $_snopphing_cart;
    private $_ckLicense = true;
    private $vendorRegForm;

    public function init() {
        /*Initializing vendor registration form*/
        $this->vendorRegForm = new Vendor_Form_RegistrationForm ();
        
        /* Initialize action controller here */
        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        //DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        /* Check Module License */
        //$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
        if ($this->_ckLicense == true) {

            $license = new Zend_Session_Namespace('License');
            if (!$license || !$license->license_data || !$license->license_data['modules']) {
                $curlObj = new Eicra_License_Version();
                $curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
                $license->license_data = $curlObj->getArrayResult();
            }
        }
        $this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
        $this->_snopphing_cart = ($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;

        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();

            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }
    }

    public function groupAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->view->currency = $this->getCurrency();
        $preferences_db = new Autos_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();
        $this->view->assign('preferences_data', $preferences_data);

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('group_id') : $this->_page_id;

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'group_id', 'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $group_db = new Autos_Model_DbTable_AutosGroup();
            $group_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
            $group_search_params['filter']['logic'] = ($group_search_params['filter']['logic']) ? $group_search_params['filter']['logic'] : 'and';
            $group_info = $group_db->getListInfo('1', $group_search_params, false);

            if ($group_info) {
                $group_info = (!is_array($group_info)) ? $group_info->toArray() : $group_info;
                $this->view->assign('group_info', $group_info[0]);
                $posted_data['sort'][] = array('field' => $group_info[0]['file_sort'], 'dir' => $group_info[0]['file_order']);
            }
        }

        if ($group_info) {
            if ($this->_request->isPost()) {
                try {
                    $this->_helper->layout->disableLayout();
                    $this->_helper->viewRenderer->setNoRender();

                    // action body
                    $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                    $getViewPageNum = $this->_request->getParam('pageSize');
                    $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id') : $this->_request->getParam('menu_id') . '/:page' ) : 'All-Auto-List/*';
                    $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'group_id' => $this->_request->getParam('group_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                    $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                    $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                    Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                    $encode_params = Zend_Json_Encoder::encode($posted_data);
                    $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                    $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                    if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                        $maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
                        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'autos_page', false);
                        $review_helper = new Review_View_Helper_Review();

                        $list_mapper = new Autos_Model_AutosListMapper();
                        $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
                        $view_datas = array('data_result' => array(), 'total' => 0);
                        if ($list_datas) {
                            $key = 0;
                            foreach ($list_datas as $entry) {
                                $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                                $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                                $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                                $entry_arr['autos_model_name_format'] = $this->autos_truncate($entry_arr['autos_model_name'], 0, 4);
                                $entry_arr['feature_mileage_format'] = $this->view->numbers($entry_arr['feature_mileage']);
                                $entry_arr['feature_doors_format'] = $this->view->numbers($entry_arr['feature_doors']);
                                $entry_arr['autos_price_format'] = $this->view->numbers($entry_arr['autos_price']);
                                $entry_arr['autos_strickthrow_price_format'] = ($entry_arr['autos_strickthrow_price']) ? $this->view->numbers($entry_arr['autos_strickthrow_price']) : null;

                                $entry_arr['autos_desc_format'] = $this->view->escape(strip_tags($entry_arr['autos_desc']));
                                $entry_arr['autos_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['autos_date'])));
                                $entry_arr['autos_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['autos_date'])));
                                $img_thumb_arr = explode(',', $entry_arr['autos_image']);
                                $autos_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);
                                $entry_arr['autos_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                                $entry_arr['autos_image_no_format'] = $this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));

                                $entry_arr['review_no'] = (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';
                                $entry_arr['review_no_format'] = (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));

                                $entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="' . $entry_arr['file_thumb_width'] . '"' : '';
                                $entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="' . $entry_arr['file_thumb_height'] . '"' : '';

                                $list_stars = '';
                                for ($i = 1; $i < $maximum_stars_digit; $i++) {
                                    $list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-active.png" />' : '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-inactive.png" />';
                                }

                                $entry_arr['original_price'] = $entry_arr['autos_price'];
                                $entry_arr['price'] = $entry_arr['original_price'];
                                $entry_arr['primary_file_field_format'] = $entry_arr['autos_image_format'];
                                $entry_arr['name'] = $entry_arr['autos_model_name'];

                                $entry_arr['list_stars_format'] = $list_stars;
                                $entry_arr['vote_format'] = $vote->getButton($entry_arr['id'], $this->view->escape($entry_arr['autos_model_name']));
                                $entry_arr['table_name'] = 'autos_page';
                                $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                                $entry_arr['eCommerce'] = $preferences_data['eCommerce'];

                                $view_datas['data_result'][$key] = $entry_arr;
                                $key++;
                            }
                            $view_datas['total'] = $list_datas->getTotalItemCount();
                        }
                        $this->_controllerCache->save($view_datas, $uniq_id);
                    }
                    $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
                }

                //Convert To JSON ARRAY	
                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function autos_truncate($phrase, $start_words, $max_words) {
        $phrase_array = explode(' ', $phrase);
        if (count($phrase_array) > $max_words && $max_words > 0)
            $phrase = implode(' ', array_slice($phrase_array, $start_words, $max_words)) . '...';
        return $phrase;
    }

    public function brandAction() {
        $this->_page_id = ($this->_request->getParam('brand_id')) ? $this->_request->getParam('brand_id') : $this->_page_id;
        if (!empty($this->_page_id)) {
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_brand_info';
            if (($brand_info = $this->_controllerCache->load($uniq_id)) === false) {
                $brand_db = new Autos_Model_DbTable_Brand();
                $brand_info = $brand_db->getBrandInfo($this->_page_id);
                $this->_controllerCache->save($brand_info, $uniq_id);
            }

            $viewPageNum = 10;
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_' . $viewPageNum;
            if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                $selectCategory = $this->_DBconn->select()
                        ->from(array('pp' => Zend_Registry::get('dbPrefix') . 'autos_page'), array('*'))
                        ->where('pp.brand_id = ?', $this->_page_id)
                        ->where('pp.active = ?', '1')
                        ->order(" pp.autos_date  DESC ");
                $view_datas = $selectCategory->query()->fetchAll();
                $this->_controllerCache->save($view_datas, $uniq_id);
            }

            $pageNumber = $this->_request->getParam('page');
            $paginator = Zend_Paginator::factory($view_datas);
            $paginator->setItemCountPerPage($viewPageNum);
            $paginator->setCurrentPageNumber($pageNumber);

            $this->view->brand_info = $brand_info;
            $this->view->view_datas = $paginator;
        }
    }

    public function viewAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->view->currency = $this->getCurrency();
        $preferences_db = new Autos_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();
        $this->view->assign('preferences_data', $preferences_data);

        $this->_page_id = (empty($this->_page_id)) ? $this->_request->getParam('type') : $this->_page_id;

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'autos_type', 'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $business_type_db = new Autos_Model_DbTable_BusinessType();

            $business_type_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
            $business_type_search_params['filter']['logic'] = ($business_type_search_params['filter']['logic']) ? $business_type_search_params['filter']['logic'] : 'and';
            $business_type_info = $business_type_db->getListInfo(null, $business_type_search_params, false);

            if ($business_type_info) {
                $business_type_info = (!is_array($business_type_info)) ? $business_type_info->toArray() : $business_type_info;
                $this->view->assign('business_type_info', $business_type_info[0]);
                $posted_data['sort'][] = array('field' => $business_type_info[0]['file_sort'], 'dir' => $business_type_info[0]['file_order']);
            }
        }

        if ($business_type_info) {
            if ($this->_request->isPost()) {
                try {
                    $this->_helper->layout->disableLayout();
                    $this->_helper->viewRenderer->setNoRender();

                    // action body
                    $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                    $getViewPageNum = $this->_request->getParam('pageSize');
                    $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id') : $this->_request->getParam('menu_id') . '/:page' ) : 'Auto-List-Type/*';
                    $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'type' => $this->_request->getParam('type'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                    $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                    $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                    Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                    $encode_params = Zend_Json_Encoder::encode($posted_data);
                    $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                    $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                    if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                        $maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
                        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'autos_page', false);
                        $review_helper = new Review_View_Helper_Review();

                        $list_mapper = new Autos_Model_AutosListMapper();
                        $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
                        $view_datas = array('data_result' => array(), 'total' => 0);
                        if ($list_datas) {
                            $key = 0;
                            foreach ($list_datas as $entry) {
                                $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                                $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                                $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                                $entry_arr['autos_model_name_format'] = $this->autos_truncate($entry_arr['autos_model_name'], 0, 4);
                                $entry_arr['feature_mileage_format'] = $this->view->numbers($entry_arr['feature_mileage']);
                                $entry_arr['feature_doors_format'] = $this->view->numbers($entry_arr['feature_doors']);
                                $entry_arr['autos_price_format'] = $this->view->numbers($entry_arr['autos_price']);
                                $entry_arr['autos_strickthrow_price_format'] = ($entry_arr['autos_strickthrow_price']) ? $this->view->numbers($entry_arr['autos_strickthrow_price']) : null;

                                $entry_arr['autos_desc_format'] = $this->view->escape(strip_tags($entry_arr['autos_desc']));
                                $entry_arr['autos_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['autos_date'])));
                                $entry_arr['autos_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['autos_date'])));
                                $img_thumb_arr = explode(',', $entry_arr['autos_image']);
                                $autos_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);
                                $entry_arr['autos_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                                $entry_arr['autos_image_no_format'] = $this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));

                                $entry_arr['review_no'] = (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';
                                $entry_arr['review_no_format'] = (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));

                                $entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="' . $entry_arr['file_thumb_width'] . '"' : '';
                                $entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="' . $entry_arr['file_thumb_height'] . '"' : '';

                                $list_stars = '';
                                for ($i = 1; $i < $maximum_stars_digit; $i++) {
                                    $list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-active.png" />' : '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-inactive.png" />';
                                }

                                $entry_arr['original_price'] = $entry_arr['autos_price'];
                                $entry_arr['price'] = $entry_arr['original_price'];
                                $entry_arr['primary_file_field_format'] = $entry_arr['autos_image_format'];
                                $entry_arr['name'] = $entry_arr['autos_model_name'];

                                $entry_arr['list_stars_format'] = $list_stars;
                                $entry_arr['vote_format'] = $vote->getButton($entry_arr['id'], $this->view->escape($entry_arr['autos_model_name']));
                                $entry_arr['table_name'] = 'autos_page';
                                $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                                $entry_arr['eCommerce'] = $preferences_data['eCommerce'];

                                $view_datas['data_result'][$key] = $entry_arr;
                                $key++;
                            }
                            $view_datas['total'] = $list_datas->getTotalItemCount();
                        }
                        $this->_controllerCache->save($view_datas, $uniq_id);
                    }
                    $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
                }

                //Convert To JSON ARRAY	
                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function categoryAction() {
        $global_conf = Zend_Registry::get('global_conf');
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);
        $this->view->currency = $this->getCurrency();
        $preferences_db = new Autos_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();
        $this->view->assign('preferences_data', $preferences_data);

        $this->_page_id = ($this->_page_id) ? $this->_page_id : $this->_request->getParam('category_id');

        if (!empty($this->_page_id)) {
            $posted_data['filter']['filters'][] = array('field' => 'category_id', 'operator' => 'eq', 'value' => $this->_page_id);
            $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

            $category_db = new Autos_Model_DbTable_Category();

            $category_search_params['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $this->_page_id);
            $category_search_params['filter']['logic'] = ($category_search_params['filter']['logic']) ? $category_search_params['filter']['logic'] : 'and';
            $category_info = $category_db->getListInfo('1', $category_search_params, false);

            if ($category_info) {
                $category_info = (!is_array($category_info)) ? $category_info->toArray() : $category_info;
                $this->view->assign('category_info', $category_info[0]);
                $posted_data['sort'][] = array('field' => $category_info[0]['cat_sort'], 'dir' => $category_info[0]['cat_order']);
            }
        }

        if ($category_info) {
            if ($this->_request->isPost()) {
                try {
                    $this->_helper->layout->disableLayout();
                    $this->_helper->viewRenderer->setNoRender();

                    // action body
                    $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');

                    $getViewPageNum = $this->_request->getParam('pageSize');
                    $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id') : $this->_request->getParam('menu_id') . '/:page' ) : 'Autos-Category-List/*';
                    $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'category_id' => $this->_request->getParam('category_id'), 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                    $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                    $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                    Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                    $encode_params = Zend_Json_Encoder::encode($posted_data);
                    $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                    $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                    if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                        $maximum_stars_digit = $this->translator->translator('maximum_stars_digit');
                        $vote = new Vote_Controller_Helper_ShowVoteButton('inline', 'autos_page', false);
                        $review_helper = new Review_View_Helper_Review();

                        $list_mapper = new Autos_Model_AutosListMapper();
                        $list_datas = $list_mapper->fetchAll($pageNumber, '1', $posted_data, array('userChecking' => false));
                        $view_datas = array('data_result' => array(), 'total' => 0);
                        if ($list_datas) {
                            $key = 0;
                            foreach ($list_datas as $entry) {
                                $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                                $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                                $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                                $entry_arr['autos_model_name_format'] = $this->autos_truncate($entry_arr['autos_model_name'], 0, 4);
                                $entry_arr['feature_mileage_format'] = $this->view->numbers($entry_arr['feature_mileage']);
                                $entry_arr['feature_doors_format'] = $this->view->numbers($entry_arr['feature_doors']);
                                $entry_arr['autos_price_format'] = $this->view->numbers($entry_arr['autos_price']);
                                $entry_arr['autos_strickthrow_price_format'] = ($entry_arr['autos_strickthrow_price']) ? $this->view->numbers($entry_arr['autos_strickthrow_price']) : null;

                                $entry_arr['autos_desc_format'] = $this->view->escape(strip_tags($entry_arr['autos_desc']));
                                $entry_arr['autos_date_lang_format'] = $this->view->numbers(date('d/m/Y', strtotime($entry_arr['autos_date'])));
                                $entry_arr['autos_date_format'] = $this->view->numbers(date('Y-m-d h:i:s A', strtotime($entry_arr['autos_date'])));
                                $img_thumb_arr = explode(',', $entry_arr['autos_image']);
                                $autos_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);
                                $entry_arr['autos_image_format'] = ($this->view->escape($entry_arr['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                                $entry_arr['autos_image_no_format'] = $this->translator->translator('autos_front_page_autos_photo_no', $this->view->numbers($autos_image_no));

                                $entry_arr['review_no'] = (!empty($entry_arr['review_id'])) ? $review_helper->getNumOfReviews($entry_arr['id'], $entry_arr['review_id']) : '0';
                                $entry_arr['review_no_format'] = (!empty($entry_arr['review_no'])) ? $this->translator->translator('common_review_no', $this->view->numbers($entry_arr['review_no'])) : $this->translator->translator('common_review_no', $this->view->numbers(0));

                                $entry_arr['thumb_width'] = ($entry_arr['file_thumb_width'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToWidth' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'width="' . $entry_arr['file_thumb_width'] . '"' : '';
                                $entry_arr['thumb_height'] = ($entry_arr['file_thumb_height'] && ($entry_arr['file_thumb_resize_func'] == 'resizeToHeight' || $entry_arr['file_thumb_resize_func'] == 'resize' )) ? 'height="' . $entry_arr['file_thumb_height'] . '"' : '';

                                $list_stars = '';
                                for ($i = 1; $i < $maximum_stars_digit; $i++) {
                                    $list_stars .= ($i <= $entry_arr['stars']) ? '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-active.png" />' : '<img src="application/layouts/scripts/' . $this->view->front_template['theme_folder'] . '/images/mod_autos_img/star-inactive.png" />';
                                }

                                $entry_arr['original_price'] = $entry_arr['autos_price'];
                                $entry_arr['price'] = $entry_arr['original_price'];
                                $entry_arr['primary_file_field_format'] = $entry_arr['autos_image_format'];
                                $entry_arr['name'] = $entry_arr['autos_model_name'];

                                $entry_arr['list_stars_format'] = $list_stars;
                                $entry_arr['vote_format'] = $vote->getButton($entry_arr['id'], $this->view->escape($entry_arr['autos_model_name']));
                                $entry_arr['table_name'] = 'autos_page';
                                $entry_arr['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                                $entry_arr['eCommerce'] = $preferences_data['eCommerce'];

                                $view_datas['data_result'][$key] = $entry_arr;
                                $key++;
                            }
                            $view_datas['total'] = $list_datas->getTotalItemCount();
                        }
                        $this->_controllerCache->save($view_datas, $uniq_id);
                    }
                    $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
                }

                //Convert To JSON ARRAY	
                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function printAction() {
        $this->_helper->layout->disableLayout();
        $this->view->module = $this->_request->getModuleName();
        $this->view->controller = $this->_request->getControllerName();
        $this->view->action = $this->_request->getActionName();

        if (!empty($this->_page_id)) {
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_details';
            if (($autos_info = $this->_controllerCache->load($uniq_id)) === false) {
                $autos_db = new Autos_Model_DbTable_Autos();
                $autos_info = $autos_db->getAutosInfo($this->_page_id);
                $this->_controllerCache->save($autos_info, $uniq_id);
            }
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';
            if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getGroupName($autos_info['group_id']);
                $this->_controllerCache->save($group_info, $uniq_id);
            }
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_BusinessTypeInfo';
            if (($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false) {
                $type_db = new Autos_Model_DbTable_BusinessType();
                $BusinessTypeInfo = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
                $this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
            }

            //Dynamic Form
            if (!empty($group_info['dynamic_form'])) {
                $group_info['form_id'] = $group_info['dynamic_form'];
                $dynamic_value_db = new Members_Model_DbTable_FieldsValue();
                $autos_info = $dynamic_value_db->getFieldsValueInfo($autos_info, $autos_info['id']);
            }
            $propertyForm = new Autos_Form_ProductForm($group_info);
            $propertyForm->populate($autos_info);

            //Assigh Review
            if (!empty($group_info['review_id'])) {
                $review_helper = new Review_View_Helper_Review();
                $review_datas = $review_helper->getReviewList($group_info, $autos_info['id']);
                $this->view->review_datas = $review_datas;
                $this->view->review_helper = $review_helper;
            }


            $this->view->propertyForm = $propertyForm;
            $this->view->group_datas = $group_info;
            $this->view->view_datas = $autos_info;
            $this->view->businessTypeInfo = $BusinessTypeInfo;
            $this->view->tab = 0;
        } else if ($this->_request->getParam('autos_model_title')) {
            $autos_model_title = $this->_request->getParam('autos_model_title');

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_details_' . preg_replace('/[^a-zA-Z0-9_]/', '_', $autos_model_title);
            if (($autos_info = $this->_controllerCache->load($uniq_id)) === false) {
                $autos_db = new Autos_Model_DbTable_Autos();
                $autos_model_title_info = $autos_db->getTitleToId($autos_model_title);
                $autos_id = $autos_model_title_info[0]['id'];
                $autos_info = $autos_db->getAutosInfo($autos_id);
                $this->_controllerCache->save($autos_info, $uniq_id);
            }

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';
            if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getGroupName($autos_info['group_id']);
                $this->_controllerCache->save($group_info, $uniq_id);
            }

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_BusinessTypeInfo';
            if (($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false) {
                $type_db = new Autos_Model_DbTable_BusinessType();
                $BusinessTypeInfo = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
                $this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
            }

            //Dynamic Form
            if (!empty($group_info['dynamic_form'])) {
                $group_info['form_id'] = $group_info['dynamic_form'];
                $dynamic_value_db = new Members_Model_DbTable_FieldsValue();
                $autos_info = $dynamic_value_db->getFieldsValueInfo($autos_info, $autos_info['id']);
            }
            $propertyForm = new Autos_Form_ProductForm($group_info);
            $propertyForm->populate($autos_info);

            //Assigh Review
            if (!empty($group_info['review_id'])) {
                $review_helper = new Review_View_Helper_Review();
                $review_datas = $review_helper->getReviewList($group_info, $autos_info['id']);
                $this->view->review_datas = $review_datas;
                $this->view->review_helper = $review_helper;
            }


            $this->view->propertyForm = $propertyForm;
            $this->view->group_datas = $group_info;
            $this->view->view_datas = $autos_info;
            $this->view->businessTypeInfo = $BusinessTypeInfo;
            $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        }
    }

    public function detailsAction() {
        $this->view->module = $this->_request->getModuleName();
        $this->view->controller = $this->_request->getControllerName();
        $this->view->action = $this->_request->getActionName();

        $global_conf = Zend_Registry::get('global_conf');
        $preferences_db = new Autos_Model_DbTable_Preferences();
        $preferences_data = $preferences_db->getOptions();

        if (!empty($this->_page_id)) {
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_details';
            if (($autos_info = $this->_controllerCache->load($uniq_id)) === false) {
                $autos_db = new Autos_Model_DbTable_Autos();
                $autos_info = $autos_db->getAutosInfo($this->_page_id);
                if ($autos_info) {
                    $autos_info['name'] = $autos_info['autos_model_name'];
                    $autos_info['original_price'] = $autos_info['autos_price'];
                    $autos_info['price'] = $autos_info['original_price'];
                    $img_thumb_arr = explode(',', $autos_info['autos_image']);
                    $autos_info['autos_image_format'] = ($this->view->escape($autos_info['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($autos_info['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                    $autos_info['primary_file_field_format'] = $autos_info['autos_image_format'];
                    $autos_info['table_name'] = 'autos_page';
                    $autos_info['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                    $autos_info['eCommerce'] = $preferences_data['eCommerce'];
                }
                $this->_controllerCache->save($autos_info, $uniq_id);
            }
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';
            if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getGroupName($autos_info['group_id']);
                $this->_controllerCache->save($group_info, $uniq_id);
            }
            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_BusinessTypeInfo';
            if (($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false) {
                $type_db = new Autos_Model_DbTable_BusinessType();
                $BusinessTypeInfo = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
                $this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
            }

            //Dynamic Form
            if (!empty($group_info['dynamic_form'])) {
                $group_info['form_id'] = $group_info['dynamic_form'];
                $dynamic_value_db = new Members_Model_DbTable_FieldsValue();
                $autos_info = $dynamic_value_db->getFieldsValueInfo($autos_info, $autos_info['id']);
            }
            $propertyForm = new Autos_Form_ProductForm($group_info);
            $propertyForm->populate($autos_info);

            //Assigh Review
            if (!empty($group_info['review_id'])) {
                $review_helper = new Review_View_Helper_Review();
                $review_datas = $review_helper->getReviewList($group_info, $autos_info['id']);
                $this->view->review_datas = $review_datas;
                $this->view->review_helper = $review_helper;
            }


            $this->view->propertyForm = $propertyForm;
            $this->view->group_datas = $group_info;
            $this->view->view_datas = $autos_info;
            $this->view->businessTypeInfo = $BusinessTypeInfo;
            $this->view->tab = 0;
        } else if ($this->_request->getParam('autos_model_title')) {
            $autos_model_title = $this->_request->getParam('autos_model_title');

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_details_' . preg_replace('/[^a-zA-Z0-9_]/', '_', $autos_model_title);
            if (($autos_info = $this->_controllerCache->load($uniq_id)) === false) {
                $autos_db = new Autos_Model_DbTable_Autos();
                $autos_model_title_info = $autos_db->getTitleToId($autos_model_title);
                $autos_id = $autos_model_title_info[0]['id'];
                $autos_info = $autos_db->getAutosInfo($autos_id);
                if ($autos_info) {
                    $autos_info['name'] = $autos_info['autos_model_name'];
                    $autos_info['original_price'] = $autos_info['autos_price'];
                    $autos_info['price'] = $autos_info['original_price'];
                    $img_thumb_arr = explode(',', $autos_info['autos_image']);
                    $autos_info['autos_image_format'] = ($this->view->escape($autos_info['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($autos_info['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                    $autos_info['primary_file_field_format'] = $autos_info['autos_image_format'];
                    $autos_info['table_name'] = 'autos_page';
                    $autos_info['shopping_cart_enable'] = (($global_conf['payment_system'] == 'On') && ($global_conf['marchent_payment_system'] == 'On') && ($preferences_data['eCommerce'] == 'YES' && $preferences_data['eCommerce_autos'] == 'YES') && ($this->_snopphing_cart == true)) ? true : false;
                    $autos_info['eCommerce'] = $preferences_data['eCommerce'];
                }
                $this->_controllerCache->save($autos_info, $uniq_id);
            }

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_group_info';
            if (($group_info = $this->_controllerCache->load($uniq_id)) === false) {
                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getGroupName($autos_info['group_id']);
                $this->_controllerCache->save($group_info, $uniq_id);
            }

            $uniq_id = preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url()) . '_BusinessTypeInfo';
            if (($BusinessTypeInfo = $this->_controllerCache->load($uniq_id)) === false) {
                $type_db = new Autos_Model_DbTable_BusinessType();
                $BusinessTypeInfo = $type_db->getBusinessTypeInfo($autos_info['autos_type']);
                $this->_controllerCache->save($BusinessTypeInfo, $uniq_id);
            }

            //Dynamic Form
            if (!empty($group_info['dynamic_form'])) {
                $group_info['form_id'] = $group_info['dynamic_form'];
                $dynamic_value_db = new Members_Model_DbTable_FieldsValue();
                $autos_info = $dynamic_value_db->getFieldsValueInfo($autos_info, $autos_info['id']);
            }
            $propertyForm = new Autos_Form_ProductForm($group_info);
            $propertyForm->populate($autos_info);

            //Assigh Review
            if (!empty($group_info['review_id'])) {
                $review_helper = new Review_View_Helper_Review();
                $review_datas = $review_helper->getReviewList($group_info, $autos_info['id']);
                $this->view->review_datas = $review_datas;
                $this->view->review_helper = $review_helper;
            }


            $this->view->propertyForm = $propertyForm;
            $this->view->group_datas = $group_info;
            $this->view->view_datas = $autos_info;
            $this->view->businessTypeInfo = $BusinessTypeInfo;
            $this->view->tab = ($this->_request->getParam('tab')) ? $this->_request->getParam('tab') : 0;
        }
    }

    //For Ajax Save Autos
    public function saveAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');
        if ($this->_request->isPost()) {
            $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $globalIdentity = $auth->getIdentity();
                $user_id = $globalIdentity->user_id;
                $autos_id = $this->_request->getPost('id');
                try {
                    $clause = $this->_DBconn->quoteInto('user_id = ?', $user_id);
                    $validator_saved_autos = new Zend_Validate_Db_RecordExists(
                            array(
                        'table' => Zend_Registry::get('dbPrefix') . 'autos_saved',
                        'field' => 'autos_id',
                        'exclude' => $clause
                            )
                    );
                    if (!$validator_saved_autos->isValid($autos_id)) {
                        //Update category_id of the product of this category to zero
                        $data = array(
                            'user_id' => $user_id,
                            'autos_id' => $autos_id
                        );
                        try {
                            $this->_DBconn->insert(Zend_Registry::get('dbPrefix') . 'autos_saved', $data);
                            $msg = $translator->translator("autos_recorded_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } catch (Exception $e) {
                            $msg = $translator->translator("autos_recorded_err");
                            $json_arr = array('status' => 'err', 'msg' => $msg . ' ' . $e->getMessage());
                        }
                    } else {
                        $msg = $translator->translator("autos_already_saved_err");
                        $json_arr = array('status' => 'err', 'msg' => $msg);
                    }
                } catch (Exception $e) {
                    $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
                }
            } else {
                $msg = $translator->translator("common_login_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    //For Ajax Search
    public function searchAction() {
        if ($this->_request->getPost('search_type') != 'post') {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
        }

        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $postData_arr = $this->_request->getPost();
            $i = 0;
            foreach ($postData_arr as $key => $value) {
                $search_field[$i] = $key;
                $i++;
            }

            $search_obj = new Autos_Model_Search($this->view, $search_field, $postData_arr);
            $result = $search_obj->doSearch();
            if ($result['status'] == 'ok') {
                $msg = $translator->translator("autos_search_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg, 'search_data' => $result['result_data'], 'where' => $result['where']);
            } else {
                $json_arr = array('status' => 'err', 'msg' => $result['msg'], 'where' => $result['where']);
            }
            if ($this->_request->getPost('search_type') == 'post') {
                $group_db = new Autos_Model_DbTable_AutosGroup();
                $group_info = $group_db->getGroupName($this->_request->getParam('group_id'));
                $this->view->group_datas = $group_info;
                $this->view->post_datas = $this->_request->getPost();
                if ($result['status'] == 'ok') {
                    $this->view->view_datas = $result['result_data'];
                } else {
                    $this->view->view_datas = null;
                }
            } else {
                //Convert To JSON ARRAY	
                $res_value = Zend_Json_Encoder::encode($json_arr);
                $this->_response->setBody($res_value);
            }
        }
    }

    public function autosuggessAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $postData_arr = $this->_request->getPost();
            $search_params['filter']['logic'] = 'or';
            $i = 0;
            foreach ($postData_arr as $key => $value) {
                $search_params['filter']['filters'][$i] = array('field' => $key, 'operator' => 'startswith', 'value' => $value);
                $i++;
            }

            $search_db = new Autos_Model_DbTable_Autos();
            $list_datas = $search_db->getListInfo('1', $search_params, array('userChecking' => false));
            if ($list_datas) {
                $currency = $this->getCurrency();
                $view_datas = array();
                $key = 0;
                foreach ($list_datas as $entry) {
                    $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                    $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);

                    $entry_arr['brand_name'] = $this->view->escape($entry_arr['brand_name']);
                    $entry_arr['autos_desc'] = $this->autos_truncate($this->view->escape($entry_arr['autos_desc']), 0, 300);
                    $entry_arr['autos_date_short'] = strftime('%Y-%m-%d', strtotime($entry_arr['autos_date']));
                    $entry_arr['autos_model_name_short'] = $this->autos_truncate($entry_arr['autos_model_name'], 0, 6);
                    $entry_arr['autos_date_title'] = '<strong>' . $this->translator->translator('common_date') . ' : </strong>' . $this->view->escape($entry_arr['autos_date']) . '<br /><strong>' . $this->translator->translator('common_entry') . ' :</strong>' . $this->view->escape($entry_arr['username']);
                    $entry_arr['category_name'] = ($entry_arr['category_name']) ? $this->view->escape($entry_arr['category_name']) : $this->translator->translator('common_no_catagory');
                    $entry_arr['owner_name'] = '<br /><strong>' . $this->translator->translator('autos_agent') . ' : </strong>' . $this->view->escape($entry_arr['owner_name']);
                    $entry_arr['price'] = $entry_arr['autos_price'];
                    $entry_arr['autos_price'] = ($entry_arr['autos_price']) ? '&nbsp;<strong>' . $this->translator->translator('autos_price') . ' : </strong>' . $entry_arr['autos_price'] . '&nbsp;' . $currency->getSymbol() : '';
                    $entry_arr['autos_postcode'] = ($entry_arr['post_code']) ? '<br /><strong>' . $this->translator->translator('autos_post_code') . ' : </strong>' . $entry_arr['post_code'] : '';
                    $entry_arr['autos_mileage'] = ($entry_arr['feature_mileage']) ? '<br /><strong>' . $this->translator->translator('autos_mileage') . ' : </strong>' . $entry_arr['feature_mileage'] . ' ' . $this->translator->translator('autos_spare_milage') : '';
                    $entry_arr['autos_location'] = ($entry_arr['city']) ? '<br /><strong>' . $this->translator->translator('autos_location') . ' : </strong>' . $this->view->escape($entry_arr['city']) : '';
                    $entry_arr['autos_location'] .= ($entry_arr['state_name']) ? ', ' . $this->view->escape($entry_arr['state_name']) : '';
                    $entry_arr['autos_location'] .= ($entry_arr['country_name']) ? ', ' . $this->view->escape($entry_arr['country_name']) : '';

                    $view_datas['data_result'][$key] = $entry_arr;
                    $key++;
                }
                if ($view_datas['data_result']) {
                    $msg = $translator->translator("autos_search_success");
                    $json_arr = array('status' => 'ok', 'msg' => $msg, 'search_data' => $view_datas['data_result']);
                } else {
                    $json_arr = array('status' => 'err', 'msg' => '');
                }
            } else {
                $json_arr = array('status' => 'err', 'msg' => $result['msg']);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function searchnameAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $postData_arr = $this->_request->getPost();
            $i = 0;
            foreach ($postData_arr as $key => $value) {
                $search_field[$i] = $key;
                $i++;
            }
            $search_field[$i] = 'active';
            $postData_arr['active'] = 1;
            $search_obj = new Autos_Model_Search($this->view, $search_field, $postData_arr);
            $result = $search_obj->doSearch('12');
            if ($result['status'] == 'ok') {
                $msg = $translator->translator("autos_search_success");
                $json_arr = array('status' => 'ok', 'msg' => $msg, 'search_data' => $result['result_data'], 'where' => $result['where']);
            } else {
                $json_arr = array('status' => 'err', 'msg' => $result['msg'], 'where' => $result['where']);
            }
            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function categoriesAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $brand_id = $this->_request->getPost('brand_id');
            $autos = new Autos_Model_DbTable_Autos();
            $autos_options = $autos->getOptionsByBrand($brand_id);
            if ($autos_options) {
                $categories = array();
                $i = 0;
                foreach ($autos_options as $key => $value) {
                    $categories[$i] = array('category_id' => $value['category_id'], 'category_name' => stripslashes($value['category_name']));
                    $i++;
                }
                $json_arr = array('status' => 'ok', 'msg' => '', 'categories' => $categories);
            } else {
                $msg = $translator->translator("autos_category_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function modelsAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $category_id = $this->_request->getPost('category_id');
            $brand_id = $this->_request->getPost('brand_id');
            $autos = new Autos_Model_DbTable_Autos();
            $autos_options = $autos->getOptionsByCategory($category_id, $brand_id);
            if ($autos_options) {
                $models = array();
                $i = 0;
                foreach ($autos_options as $key => $value) {
                    $models[$i] = array('id' => $value['id'], 'autos_model_name' => stripslashes($value['autos_model_name']), 'autos_model_name_raw' => $value['autos_model_name']);
                    $i++;
                }
                $json_arr = array('status' => 'ok', 'msg' => '', 'models' => $models);
            } else {
                $msg = $translator->translator("autos_model_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function statesAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $country_id = $this->_request->getPost('id');
            $states = new Eicra_Model_DbTable_State();
            $states_options = $states->getOptions($country_id);
            if ($states_options) {
                $states = array();
                $i = 0;
                foreach ($states_options as $key => $value) {
                    $states[$i] = array('state_id' => $key, 'state_name' => $value);
                    $i++;
                }
                $json_arr = array('status' => 'ok', 'msg' => '', 'states' => $states);
            } else {
                $msg = $translator->translator("common_state_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function areasAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $translator = Zend_Registry::get('translator');

        if ($this->_request->isPost()) {
            $state_id = $this->_request->getPost('id');
            $cities = new Eicra_Model_DbTable_City();
            $cities_options = $cities->getOptions($state_id);
            if ($cities_options) {
                $cities = array();
                $i = 0;
                foreach ($cities_options as $key => $value) {
                    $cities[$i] = array('city_id' => $key, 'city' => $value);
                    $i++;
                }
                $json_arr = array('status' => 'ok', 'msg' => '', 'cities' => $cities);
            } else {
                $msg = $translator->translator("common_area_found_err");
                $json_arr = array('status' => 'err', 'msg' => $msg);
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    public function itinerarylist1Action() {
        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        Eicra_Global_Variable::checkSession($this->_response, $url);

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $globalIdentity = $auth->getIdentity();
            $role_id = $globalIdentity->role_id;
            $user_id = ($globalIdentity->access_other_user_article == '1') ? null : $globalIdentity->user_id;

            $invoices_db = new Invoice_Model_DbTable_Invoices();
            $invoices_info = $invoices_db->getInvoiceInfo($user_id);
            $invoice_items_db = new Invoice_Model_DbTable_InvoiceItems();
            $itinerary_info = array();
            $i = 0;
            foreach ($invoices_info as $invoices) {
                if ($invoices['module_name'] == 'Autos') {
                    $itinerary_info[$i]['user_id'] = $invoices['user_id'];
                    $itinerary_info[$i]['invoice_status'] = $invoices['status'];
                    $invoice_items_info = $invoice_items_db->getInvoiceItems($invoices['id']);
                    foreach ($invoice_items_info as $invoice_items_key => $invoice_items) {
                        $object_value_decoded = Zend_Json::decode($invoice_items['object_value']);
                        $itinerary_info[$i]['invoice_items'][$invoice_items_key] = $object_value_decoded;
                    }
                    $i++;
                }
            }
            $this->view->assign('itinerary_info', $itinerary_info);
            $this->view->assign('currency', $this->getCurrency());
        }
    }

    public function itinerarylistAction() {
        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Frontend-Login';
        Eicra_Global_Variable::getSession()->returnLink = $this->view->url();
        Eicra_Global_Variable::checkSession($this->_response, $url);

        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        $this->view->currency = $this->getCurrency();

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $status = $this->getRequest()->getParam('status');

                $posted_data['filter']['filters'][] = array('field' => 'module_name', 'operator' => 'eq', 'value' => $this->view->getModule);
                $posted_data['filter']['logic'] = ($posted_data['filter']['logic']) ? $posted_data['filter']['logic'] : 'and';

                $getViewPageNum = $this->_request->getParam('pageSize');
                $frontend_route = ($this->_request->getParam('menu_id')) ? (($pageNumber == '1' || empty($pageNumber)) ? $this->_request->getParam('menu_id') : $this->_request->getParam('menu_id') . '/:page' ) : 'autos-Itinerary-List/*';
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'status' => $status, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), $frontend_route, true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    $list_mapper = new Invoice_Model_InvoicesListMapper();
                    $invoice_items_db = new Invoice_Model_DbTable_InvoiceItems();
                    $autos_db = new Autos_Model_DbTable_Autos();
                    $list_datas = $list_mapper->fetchAll($pageNumber, $status, $posted_data);

                    $view_datas = array('data_result' => array(), 'total' => 0);

                    if ($list_datas) {
                        $key = 0;
                        $status_class = new Invoice_Controller_Helper_Status($this->translator);
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['publish_status_invoice'] = str_replace('_', '-', $entry_arr['id']);
                            unset($entry_arr['invoice_desc']);
                            $entry_arr['invoice_create_date_format'] = date('Y-m-d h:i:s A', strtotime($entry_arr['invoice_create_date']));
                            $entry_arr['invoice_status_format'] = $status_class->getStatus($entry_arr['status']);
                            $entry_arr['invoice_now_paid_status_format'] = $status_class->getNowPaidStatus();
                            $entry_arr['invoice_now_unpaid_status_format'] = $status_class->getNowUnPaidStatus();
                            $entry_arr['edit_enable'] = ($this->_auth_obj->access_other_user_article == '1' || $this->_auth_obj->user_id == $entry_arr['user_id']) ? true : false;

                            //Invoice Items Start
                            $invoice_items_info = $invoice_items_db->getInvoiceItems($entry_arr['id']);
                            if ($invoice_items_info) {
                                foreach ($invoice_items_info as $invoice_items_key => $invoice_items) {
                                    $object_value_decoded = Zend_Json_Decoder::decode($invoice_items['object_value']);
                                    $entry_arr['invoice_items'][$invoice_items_key]['item_id'] = $invoice_items['id'];
                                    $entry_arr['invoice_items'][$invoice_items_key]['autos_id'] = $object_value_decoded['autos_id'];
                                    $entry_arr['invoice_items'][$invoice_items_key]['autos_info'] = $autos_db->getautosInfo($object_value_decoded['autos_id']);
                                    $entry_arr['invoice_items'][$invoice_items_key]['autos_desc_format'] = strip_tags($this->view->escape($entry_arr['invoice_items'][$invoice_items_key]['autos_info']['autos_desc']));
                                    $img_thumb_arr = explode(',', $entry_arr['invoice_items'][$invoice_items_key]['autos_info']['autos_image']);
                                    $autos_image_no = (empty($img_thumb_arr[0])) ? '0' : count($img_thumb_arr);
                                    $entry_arr['invoice_items'][$invoice_items_key]['autos_image_format'] = ($this->view->escape($entry_arr['invoice_items'][$invoice_items_key]['autos_info']['autos_primary_image'])) ? 'data/frontImages/autos/autos_image/' . $this->view->escape($entry_arr['invoice_items'][$invoice_items_key]['autos_info']['autos_primary_image']) : 'data/frontImages/autos/autos_image/' . $img_thumb_arr[0];
                                }
                            }
                            //Invoice Items End
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }
                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }

    private function getCurrency() {

        if (empty($this->currency)) {
            $global_conf = Zend_Registry::get('global_conf');
            $this->currency = new Zend_Currency($global_conf['default_locale']);
            return $this->currency;
        } else {
            return $this->currency;
        }
    }

    
    public function regvendorAction(){
        if ($this->_request->isPost()) {
            //for now do nothing
        } else {
            $this->view->venregForm = $this->vendorRegForm;
        }
    }
}
