<?php
class Autos_Controller_Helper_File
{
	private $_path;
	private $_fileNmae;
	private	$_fileExt;
	private	$_option;
	
	public function __construct($path = null ,$fileName = null ,$fileExt = null,$option	= null) 
	{
		$this->_path		=	$path;
		$this->_fileName	=	$fileName;
		$this->_fileExt		=	$fileExt;
		$this->_option		=	$option;
	}
	
	public function cleanFile($path = null ,$fileName = null ,$fileExt = null) 
	{
				if(!empty($path))
					$this->_path	=	$path;
				if(!empty($fileName))
					$this->_fileName=	$fileName;
				if(!empty($fileExt))
					$this->_fileExt	=	$fileExt;
						
                $seconds_old = 1;
                $directory 	 = $this->_path;
				$filename	 = $this->_fileName;

                if( !$dirhandle = @opendir($directory))
				{
                       return false;
				}

              
				if( $filename != "." && $filename != ".." && $filename!="index.php") 
				{
					$filename = $directory. "/". $filename;
					if( @filemtime($filename) < (time()-$seconds_old)  && ($filename != "index.html"))
					{
					   if(@unlink($filename))
					   {
							return true;
					   }
					   else
					   {
							return false;
					   }
					}
				}
              
       }	
	   
	public function checkCategoryThumbExt($fileName = null ,$option	= null)
	{
		if(!empty($option))
			$this->_option	=	$option;
		if(!empty($fileName))
			$this->_fileName	=	$fileName;
		
		
		$ext = explode(',',$this->_option['file_type']);
		
		if(in_array($this->_fileExt,$ext))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function checkPropertyThumbExt($fileName = null ,$option	= null)
	{
		if(!empty($option))
			$this->_option	=	$option;
		if(!empty($fileName))
			$this->_fileName	=	$fileName;
		
		
		$ext = explode(',',$this->_option['file_type']);
		
		if(in_array($this->_fileExt,$ext))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function checkPropertyFileExt($fileName = null ,$option	= null)
	{
		if(!empty($option))
			$this->_option	=	$option;
		if(!empty($fileName))
			$this->_fileName	=	$fileName;
	
		
		$ext = explode(',',$this->_option['file_type']);
		
		if(in_array($this->_fileExt,$ext))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>