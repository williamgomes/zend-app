<?php
class Autos_Controller_Helper_CategoryOrders
{
	protected $_id;
	protected $_group_id;
	protected $_parent;
	protected $_high_order;
	protected $_translator;
	protected $_category_order;
	protected $_conn;
	
	public function __construct($id = null)
	{		
		//DB Connection
		$this->_conn = Zend_Registry::get('msqli_connection');
		$this->_translator 	=   Zend_Registry::get('translator');
		if($id != null && $id != '')
		{
			$this->setCatInfo($id);
			$this->setHeighestOrder($this->_group_id,$this->_parent);
		}		
	}
	
	public function setCatInfo($id) 
	{
		$this->_conn->getConnection();
		$this->_id = $id;
		//Get It's Info
		$select = $this->_conn->select()
					   ->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_category'), array('gc.group_id','gc.parent','gc.category_order'))
					   ->where('gc.id = ?',$id);
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_group_id = (int)$row['group_id'];
				$this->_category_order = (int)$row['category_order'];
				$this->_parent = (int)$row['parent'];
			}
		}
	}
	
	public function setHeighestOrder($group_id,$parent) 
	{
		$this->_conn->getConnection();
		//Get Heighest Order
		if(!empty($this->_id))
		{
			$select = $this->_conn->select()
						   ->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_category'), array('gc.category_order'))
						   ->where('gc.group_id = ?',$group_id)
						   ->where('gc.id != ?',$this->_id)
						   ->where('gc.parent = ?',$parent)
						   ->order(array('gc.category_order DESC'))
						   ->limit(1);
		}
		else
		{
			$select = $this->_conn->select()
						   ->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_category'), array('gc.category_order'))
						   ->where('gc.group_id = ?',$group_id)
						   ->where('gc.parent = ?',$parent)
						   ->order(array('gc.category_order DESC'))
						   ->limit(1);
		}
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_high_order = (int)$row['category_order'];				
			}
		}
		else
		{
			$this->_high_order = 0;
		}
	}
	
	public function setNewOrder($id) 
	{
		$this->_conn->getConnection();
		$this->_high_order = $this->_high_order + 1;
		//Get Heighest Order
		$where = array();
		$where[] = 'id = '.$this->_conn->quote($id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $this->_high_order),$where);	
	}
	
	public function decreaseOrder($id = null,$category_order = null)
	{
		if(empty($this->_id))
		{
			$this->_id = $id;
		}
		if(empty($this->_category_order))
		{
			$this->_category_order = $category_order;
		}
		
		if($this->_category_order > 1)
		{
			$up_order = $this->_category_order - 1;
			
			$this->_conn->getConnection();
			
			//Get Category which Have New Order
			$select = $this->_conn->select()
							   ->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_category'), array('gc.id'))
							   ->where('gc.group_id = ?',$this->_group_id)
							   ->where('gc.parent = ?',$this->_parent)
							   ->where('gc.category_order = ?',$up_order);
			$rs = $select->query()->fetchAll();
			
			if($rs)
			{	
				$id_arr = array();		
				$i=0;
				foreach($rs as $row)
				{
					$id_arr[$i] = (int)$row['id'];	
					$i++;			
				}
			}
			else
			{
				$id_arr = null;
			}
			//Update existing Categories with new Category category_order
			$where = array();
			$where[] = 'category_order = '.$this->_conn->quote($up_order);
			$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
			$where[] = 'parent = '.$this->_conn->quote($this->_parent);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $this->_category_order),$where);	
			
			//Decrease Category Order
			$whereI = array();
			$whereI[] = 'id = '.$this->_conn->quote($this->_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $up_order),$whereI);
			$return_arr = array('status' => 'ok','id_arr' => $id_arr, 'msg' => '');	
		}
		else
		{
			$id_arr = null;
			$return_arr = array('status' => 'err','id_arr' => $id_arr, 'msg' => $this->_translator->translator('common_order_up_error'));
		}
		
		return $return_arr;
	}
	
	public function increaseOrder($id = null,$category_order = null)
	{
		if(empty($this->_id))
		{
			$this->_id = $id;
		}
		if(empty($this->_category_order))
		{
			$this->_category_order = $category_order;
		}
		
		if($this->_category_order)
		{
			$new_order = $this->_category_order + 1;
			
			$this->_conn->getConnection();
			
			//Get Categories which Have New Category Order
			$select = $this->_conn->select()
							   ->from(array('gc' => Zend_Registry::get('dbPrefix').'autos_category'), array('gc.id'))
							   ->where('gc.group_id = ?',$this->_group_id)
							   ->where('gc.parent = ?',$this->_parent)
							   ->where('gc.category_order = ?',$new_order);
			$rs = $select->query()->fetchAll();
			
			if($rs)
			{	
				$id_arr = array();		
				$i=0;
				foreach($rs as $row)
				{
					$id_arr[$i] = (int)$row['id'];	
					$i++;			
				}
			}
			else
			{
				$id_arr = null;
			}
			//Update existing categories with new category category_order
			$where = array();
			$where[] = 'category_order = '.$this->_conn->quote($new_order);
			$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
			$where[] = 'parent = '.$this->_conn->quote($this->_parent);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $this->_category_order),$where);	
			
			//Increase Category Order
			$whereI = array();
			$whereI[] = 'id = '.$this->_conn->quote($this->_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $new_order),$whereI);	
			$return_arr = array('status' => 'ok','id_arr' => $id_arr, 'msg' => '');
		}
		else
		{
			$id_arr = null;
			$return_arr = array('status' => 'err','id_arr' => $id_arr, 'msg' => $this->_translator->translator('common_order_down_error'));
		}
		
		return $return_arr;
	}
	
	public function saveOrder($new_order)
	{
		//Update existing categories with new category category_order
		$where = array();
		$where[] = 'category_order = '.$this->_conn->quote($new_order);
		$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
		$where[] = 'parent = '.$this->_conn->quote($this->_parent);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $this->_category_order),$where);
		
		//Save new Category Order
		$whereI = array();
		$whereI[] = 'id = '.$this->_conn->quote($this->_id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_category',array('category_order' => $new_order),$whereI);	
	}
	
	public function getGroup_Id()
	{
		return $this->_group_id;
	}
	
	public function getParent()
	{
		return $this->_parent;
	}
	
	public function getHighOrder()
	{
		return $this->_high_order;
	}
}
?>