<?php
class Autos_Controller_Helper_Emails
{	
	public function getLetterBody($datas,$letter_id = null)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		if(!empty($letter_id))
		{
			$select = $conn->select()
								->from(array('nt' => Zend_Registry::get('dbPrefix').'newsletter_templates'), array('*'))
								->where('nt.templates_page = ?',$letter_id)
								->limit(1);		
							
			$rs = $select->query()->fetchAll();
			if($rs)
			{
				foreach($rs as $row)
				{
					$templates_arr = $row ;
				}
			}
			else
			{
				$templates_arr['templates_desc'] = '';
			}			
			foreach($datas as $key=>$value)
			{
				$templates_arr['templates_desc'] = str_replace('%'.$key.'%',$value,$templates_arr['templates_desc']);
			}
		}
		else
		{
			$templates_arr = null;
		}
		return $templates_arr;
	}
	
	public function attachFiles($mailing,$form_info = null,$attach_file_arr = null)
	{
	  if($attach_file_arr != null)
	  {
		foreach($attach_file_arr as $key => $value)
		{
			if(!empty($value))
			{
				$fileType = Eicra_File_Utility::GetExtension($value);
				$myFile = file_get_contents(BASE_PATH.DS.$form_info['attach_file_path'].DS.$value);
				$at = new Zend_Mime_Part($myFile);
				$at->type        = 'application/'.$fileType;
				$at->disposition = Zend_Mime::DISPOSITION_INLINE;
				$at->encoding    = Zend_Mime::ENCODING_BASE64;
				$at->filename    = $value;			 
				$mailing->addAttachment($at);
			}
		}
	  }
	}	
	
	public function sendEmail($datas,$form_info = null,$attach_file_arr = null,$to_mail = null,$letter_type = null)
	{
		$translator = Zend_Registry::get('translator');
		$letter_id = ($form_info) ? $form_info['email_template_set'] : $letter_type;
		$global_conf = Zend_Registry::get('global_conf');	
		$config = array(
					'auth'      =>'login',
                    'port'      => $global_conf['email_smtp_port'],
                    'username'   =>$global_conf['email_smtp_username'],
                    'password'   =>$global_conf['email_smtp_password']);

          $transport = new Zend_Mail_Transport_Smtp($global_conf['email_smtp_host'], $config);
     	 
		  
		  $datas['site_name'] = $global_conf['site_name'];
		  $datas['site_url'] = $global_conf['site_url'];
		  $templates_arr =  self::getLetterBody($datas,$letter_id);
		  if($templates_arr)
		  {
			  $newsletter = stripslashes($templates_arr['templates_desc']);
			  $subject = stripslashes($templates_arr['templates_title']);
			 
			  $email_name_admin = $global_conf['global_email'];				  
			  $email_name = ($to_mail) ? $to_mail : $email_name_admin;
			  $mailing = new Zend_Mail();
			  self::attachFiles($mailing,$form_info,$attach_file_arr);
			  $mailing->setFrom($email_name_admin, $global_conf['site_name']);
			  $mailing->addTo($email_name, $global_conf['site_name']);
			  $mailing->setSubject($subject);
			  $mailing->setBodyHtml($newsletter);
			  
			  try
			  {
				$mailing->send($transport);
				$return = array('status' => 'ok');
			  }
			  catch (Exception $e) 
			  {
				try
				{
					$mailing->send();
					$return = array('status' => 'ok');
				}
				catch (Exception $e) 
				{
					$return = array('status' => 'err','msg' => $e->getMessage());
				}
			  } 
		}
		else
		{
			$return = array('status' => 'err','msg' => $translator->translator('common_newsletter_not_found'));
		}		
		return $return;
	}
}
?>