<?php
//Members Packages controller
class Database_CsvController extends Zend_Controller_Action
{	
	private $_controllerCache;
	private $_translator;
	private $_auth_obj;	
	
    public function init()
    {	
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->_translator);	
		$this->view->setEscape('stripslashes'); 
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();					
    }
	
	public function preDispatch() 
	{
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
			
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);			
			
		
		if($getAction != 'uploadfile')
		{
			$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);	
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}
				
	}
	
	public function listAction()
	{
		try
		{
			$sql_files = Eicra_File_Utility::getAllFiles('data/adminImages/csv');
		}
		catch(Exception $e)
		{
			$err_msg = $e->getMessage();
		}	
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);	
		
		$table_name = $this->getRequest()->getParam('table_name');	
		$this->view->assign('table_name', $table_name);	
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				if(!empty($table_name))
				{
					if($sql_files)
					{
						foreach($sql_files as $sql_files_key => $sql_files_arr)
						{
							if(!preg_match("/".$table_name."/",$sql_files_arr['name']))
							{
								unset($sql_files[$sql_files_key]);
							}
						}
					}
				}
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
											
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'table_name' => $table_name, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
													
				$list_datas = Zend_Paginator::factory($sql_files);
				$list_datas->setItemCountPerPage($viewPageNum);
				$list_datas->setCurrentPageNumber($pageNumber);
				
				$view_datas = array('data_result' => array(), 'total' => 0);			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;	
							$entry_arr['name_format'] = str_replace('_', '-', $entry_arr['name']);						
							/*$entry_arr['id_format']=  $this->view->numbers($entry_arr['id']);
							$entry_arr['publish_status_hotels_name'] = str_replace('_', '-', $entry_arr['hotels_name']);
							$entry_arr['hotels_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['hotels_date'])));	
							$entry_arr['hotels_date_format']=  $this->view->numbers(date('Y-m-d h:i:s A',strtotime($entry_arr['hotels_date'])));
							$img_thumb_arr = explode(',',$entry_arr['hotels_image']);								
							$entry_arr['hotels_image_format'] = ($this->view->escape($entry_arr['hotels_primary_image'])) ? 'data/frontImages/hotels/hotels_image/'.$this->view->escape($entry_arr['hotels_primary_image']) :  'data/frontImages/hotels/hotels_image/'.$img_thumb_arr[0] ;
							*/
							$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1') ? true : false;						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}				
							
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	public function uploadAction()
	{
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		$uploadForm = new Database_Form_UploadForm();
		$this->view->assign('uploadForm', $uploadForm);
	}
	
	public function importAction()
	{		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				
				$file_name 			= 	$this->_request->getPost('file_name');
				$host_name 			= 	$this->_request->getPost('host_name');
				$user_name 			= 	$this->_request->getPost('user_name');
				$user_password 		= 	$this->_request->getPost('user_password');
				$db_name			= 	$this->_request->getPost('db_name');
				$table_name			=	$this->_request->getPost('table_name');	
				
				$file = new SplFileObject("data/adminImages/csv/".$file_name);
				$file->setFlags(SplFileObject::READ_CSV);
				
				$field_num = 0;
				foreach ($file as $key => $row) 
				{	
					if($key == 0)
					{
						//$sql 	=		"DROP TABLE IF EXISTS `".$table_name."`;";
						$field_num  = count($row);
						$sql 	=   	"INSERT INTO `".$table_name."` (`".implode('`,`', $row)."`) VALUES ";
					}
					else if($key == 1)
					{
						$r_sql = "";
						if(count($row) > 1)
						{
							$i =1;
							foreach($row as $row_key => $value)
							{
								$r_sql .= ($row_key == 0) ? '""' : ', "'.addslashes(str_replace(';;;', ',', $value)).'"';
								if($i >= $field_num)
								{
									break;
								}
								$i++;
							}
						}
						$sql .= (!empty($r_sql)) ? "(".$r_sql.")" : "";
					}
					else
					{	
						$r_sql = "";
						if(count($row) > 1)
						{
							$i =1;
							foreach($row as $row_key => $value)
							{
								$r_sql .= ($row_key == 0) ? '""' : ', "'.addslashes(str_replace(';;;', ',', $value)).'"';
								if($i >= $field_num)
								{
									break;
								}
								$i++;
							}
						}
						$sql .= (!empty($r_sql)) ? ",(".$r_sql.")" : "";
					}
				}
				$sql .= ";";
				
				if($sql)
				{
					$link = mysql_connect($host_name,$user_name,$user_password);
					mysql_select_db($db_name,$link);
					if(mysql_query($sql))
					{
						$json_arr = array('status' => 'ok', 'data_result' => $sql, 'msg' => $this->_translator->translator('database_csv_file_import_successfull', $file_name));
					}
					else
					{
						$json_arr = array('status' => 'err', 'data_result' => $sql, 'msg' => mysql_error());
					}
				}
				else
				{
					$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $this->_translator->translator('database_mysql_query_not_generated'));
				}				
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	public function exportAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		
		if ($this->_request->isPost() && $this->_request->getPost('post_type')) 
		{				
			try
			{
				$table_name = $this->_request->getPost('table_name');
				$csv_class = new Database_Controller_Helper_Csv($table_name, $this->_request->getPost('post_type'));				
				$result = $csv_class->getCSVformat();
				
				if($result['status'] == 'ok')
				{
					$CSVformat = $result['out'];
					$date = date("Y_m_d_h_i_s",time());
					$file_name = $table_name."_".$date.".csv";
					$file_path = "data/adminImages/csv";
					if(!is_dir($file_path)){ mkdir($file_path); }
					$file = $file_path.'/'.$file_name;
					Eicra_File_Utility::writeOnFile($file,$CSVformat);
					$msg = $this->_translator->translator('database_csv_export_successfull',$file_name);
					$json_arr = array('status' => 'ok','data' => array('name' => $file_name,'path' => $file_path), 'msg' => $msg);
				}
				else
				{
					$json_arr = array('status' => 'err', 'msg' => $result['msg']);
				}				
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'msg' => $e->getMessage());
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$perm = new Database_View_Helper_Allow();			
			if ($perm->allow('delete','csv','Database'))
			{
				$file_name = str_replace('-', '_', $this->_request->getPost('name'));			
							
				
				$file_path = BASE_PATH.DS.'data'.DS.'adminImages'.DS.'csv';
				
				
				try
				{
					if($file_name)
					{
						$dir = $file_path.DS.$file_name;
						$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						if($res)
						{
							$msg = 	$translator->translator('database_file_delete_success',$file_name);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('database_file_delete_error',$file_name);			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = 	$translator->translator('database_file_name_empty');			
						$json_arr = array('status' => 'err','msg' => $msg);
					}					
										
				}
				catch (Exception $e) 
				{			
					$json_arr = array('status' => 'err','msg' => $e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('daatabase_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Database_View_Helper_Allow();			
			if ($perm->allow('delete','csv','Database'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					$file_path = BASE_PATH.DS.'data'.DS.'adminImages'.DS.'csv';
					
					foreach($id_arr as $file_name)
					{				
						try
						{
							if($file_name)
							{
								$dir = $file_path.DS.$file_name;
								$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								if($res)
								{
									$msg = 	$translator->translator('database_file_delete_success',$file_name);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('database_file_delete_error',$file_name);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
							else
							{
								$msg = 	$translator->translator('database_file_name_empty');			
								$json_arr = array('status' => 'ok','msg' => $msg);
							}					
												
						}
						catch (Exception $e) 
						{			
							$json_arr = array('status' => 'ok','msg' => $e->getMessage());
						}
					}
				}
				else
				{
					$msg = $translator->translator("common_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('daatabase_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}		
}
?>