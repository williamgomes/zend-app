<?php
//Members Registers Helpers Class
class Database_Controller_Helper_Csv
{	
	private $_table_name;
	private $_post_type;
	private $_conn;
	public function __construct($table_name, $post_type = 'db')
	{ 
		$this->_table_name = $table_name;
		$this->_post_type = $post_type;
		//DB Connection
		$this->_conn = Zend_Registry::get('msqli_connection');	
		$this->_conn->getConnection();	
	}
	
	public function getCSVformat()
	{
		try
		{	
			if($this->_post_type == 'db')
			{		
				$select = $this->_conn->select()->from($this->_table_name);	
				$content_arr = $select->query()->fetchAll();				
				
				$metadata = $this->_conn->describeTable($this->_table_name);				
				$columnNames = array_keys($metadata);			
			}
			else if($this->_post_type == 'form')
			{
				$form_db = new Members_Model_DbTable_Forms();
				$form_info = $form_db->getFormsInfo($this->_table_name);	
				$generalForm =  new Members_Form_GeneralForm ($form_info);
			
				$dynamic_value_db = new Members_Model_DbTable_General();
				$fields_db = new Members_Model_DbTable_FieldsValue();  
				$field_db = new Members_Model_DbTable_Fields();
				$fieldsInfo = $field_db->getFieldsInfo($this->_table_name);
				$datas_arr	=	$dynamic_value_db->getAllValues($this->_table_name);
				$content_arr = array();
				$i = 0;
				if($datas_arr)
				{
					foreach($datas_arr as $entry)
					{
						foreach($fieldsInfo as $fields)
                        {
							$value_info = $fields_db->getFieldsValue($entry->id, $entry->form_id,$fields->id);														
							 if($value_info)
							 {
								if(!empty($value_info[0]) && !empty($value_info[0]['field_value']))
								{
									$content_arr[$i][$fields['field_name']] = stripslashes($value_info[0]['field_value']);									
								}
								else
								{
									$content_arr[$i][$fields['field_name']] =  '';
								}
							 }
							 else
							 {
								$content_arr[$i][$fields['field_name']] =  '';
							 }
						}
						$i++;
					}
				}
				
				$dynamic_field_db = new Members_Model_DbTable_Fields();				
				$metadata = $dynamic_field_db->getFieldsInfo($this->_table_name, null);	
				if($metadata)
				{ 
					$columnNames = array();
					$cn = 0;
					foreach($metadata as $meta_arr)
					{	
						$element = $generalForm->getElement($meta_arr['field_name']);					
						$columnNames[$cn] = str_replace(':', '', $element->getLabel());
						$cn++;	
					}
				}
				
			}	
				
			$csv_terminated = "\n";
			$csv_separator = ",";
			$csv_enclosed = '"';
			$csv_escaped = "\\";        
		
			$schema_insert = "";
			foreach($columnNames as $key => $value)
			{
				$l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, stripslashes($value)) . $csv_enclosed;
				$schema_insert .= $l;
				$schema_insert .= $csv_separator;
			}
		
			$out = trim(substr($schema_insert, 0, -1));
			$out .= $csv_terminated;
		
			if($content_arr)
			{               
				foreach($content_arr as $content)
				{   
					 $schema_insert = '';					  
					 $item_array =    $content;  
					 foreach($item_array as $item)
					 { 
					 	$item  = str_replace(',', ';;;', str_replace($csv_terminated, '', stripslashes($item)));                
						$schema_insert .= $csv_enclosed . 
						str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $item) . $csv_enclosed;	
						$schema_insert .= $csv_separator;
					}			
					$schema_insert .= $csv_separator;
					$out .= $schema_insert;
					$out .= $csv_terminated;		
				}
			}
			$result = array('status' => 'ok', 'out' => $out);			
		}
		catch(Exception $e)
		{
			$result = array('status' => 'err', 'msg' => $e->getMessage());
		}
		return $result;
	}	
}
?>