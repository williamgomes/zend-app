<?php

class Database_BackendController extends Zend_Controller_Action
{
	private $_translator;
	private $_auth_obj;	
		
    public function init()
    {
     	//INITIALIZE FUNCTION
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);
    }
	
	public function preDispatch() 
	{		
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		$this->_translator = Zend_Registry::get('translator');
		$this->view->assign('translator' , $this->_translator);	
		$this->view->setEscape('stripslashes');
		
		/* Initialize action controller here */
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);	
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);	
			
		if($getAction != 'uploadfile')
		{
			$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
			Eicra_Global_Variable::checkSession($this->_response,$url);	
			
			/*Check Module License*/
			$modules_license = new Administrator_Controller_Helper_ModuleLoader();
			$modules_license->getModulesLicenseMsg($this->_request->getModuleName());
		}		
	}	
	
	public function listAction()
	{
		try
		{
			$sql_files = Eicra_File_Utility::getAllFiles('data/adminImages/backupDb');
		}
		catch(Exception $e)
		{
			$err_msg = $e->getMessage();
		}
		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);		
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
											
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'draft' => $draft_page, 'link'	=> $link_menu, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;				
				Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;
													
				$list_datas = Zend_Paginator::factory($sql_files);
				$list_datas->setItemCountPerPage($viewPageNum);
				$list_datas->setCurrentPageNumber($pageNumber);
				
				$view_datas = array('data_result' => array(), 'total' => 0);			
				if($list_datas)
				{						
					$key = 0;				
					foreach($list_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;	
							$entry_arr['name_format'] = str_replace('_', '-', $entry_arr['name']);								
							$entry_arr['edit_enable']	=   ($this->_auth_obj->access_other_user_article == '1') ? true : false;						
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}
					$view_datas['total']	=	$list_datas->getTotalItemCount();
				}				
							
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);	
		}
	}
	
	//BACKUP FUNCTION
	public function backupAction()
	{		
		if ($this->_request->isPost() && $this->_request->getPost('post_type') == 'backup') 
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->_helper->layout->disableLayout();
			$translator = Zend_Registry::get('translator');
			
			$allConfig  = Zend_Registry::get('allConfig');
			$DB_HOST = $allConfig->dbConfig->eicra->params->host;
			$DB_USER =	$allConfig->dbConfig->eicra->params->username;
			$DB_PASS =	$allConfig->dbConfig->eicra->params->password;
			$DB_NAME =	$allConfig->dbConfig->eicra->params->dbname;
			try
			{
				$date = date("Y_m_d_h_i_s",time());
				$file_name = "sql_".$date.".sql";
				$file_path = "data/adminImages/backupDb";
				if(!is_dir($file_path)){ mkdir($file_path); }
				$file = $file_path.'/'.$file_name;
				
 				/*$command = sprintf("mysqldump -h %s -u %s --password=%s -d %s --skip-no-data > %s",
					escapeshellcmd($DB_HOST),
					escapeshellcmd($DB_USER),
					escapeshellcmd($DB_PASS),
					escapeshellcmd($DB_NAME),           
					escapeshellcmd($file)
				);
							
				exec($command, $ret_arr, $ret_code);
				switch($ret_code)
				{
						case 0:						
							 $msg = $translator->translator('database_file_backup_export_successfull', $DB_NAME).' <b>~/'.$file.'</b>';
							 $st = 'ok';
							break;
						case 1:
								//echo 'There was a warning during the export of <b>' .$mysqlDatabaseName .'</b> to <b>~/' .$mysqlExportPath .'</b>';
								$msg = $translator->translator('database_file_backup_export_warning', $DB_NAME).' <b>~/'.$file.'</b>';
								$st = 'ok';
							break;
						case 2:
								//echo '<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr></table>';
								$msg = $translator->translator('database_file_backup_export_error').' '.$translator->translator('database_file_backup_export_check_value').'<br/><br/><table><tr><td>'.$translator->translator('database_file_backup_export_check_value_db_name').'</td><td><b>' .$DB_NAME .'</b></td></tr><tr><td>'.$translator->translator('database_file_backup_export_check_value_user_name').'</td><td><b>' .$DB_USER .'</b></td></tr><tr><td>'.$translator->translator('database_file_backup_export_check_value_pass_name').'</td><td><b>'.$translator->translator('database_file_backup_export_check_value_pass_noshow').'</b></td></tr><tr><td>'.$translator->translator('database_file_backup_export_check_value_host_name').'</td><td><b>' .$DB_HOST .'</b></td></tr></table>';
								$st = 'err';
							break;
				}*/
								
				$link = mysql_connect($DB_HOST,$DB_USER,$DB_PASS);
				mysql_select_db($DB_NAME,$link);
				
				//get all of the tables
				$mem_db 	= 	new Members_Model_DbTable_MemberList();
				$tables 	= 	$mem_db->getAdapter()->listTables();
				
				//cycle through
				foreach($tables as $table)
				{
					if($table)
					{
						$result = mysql_query('SELECT * FROM '.$table);
						$num_fields = mysql_num_fields($result);
						
						$return.= 'DROP TABLE IF EXISTS '.$table.';';
						$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
						$return.= "\n\n".str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $row2[1]).";\n\n";
						
						for ($i = 0; $i < $num_fields; $i++) 
						{
							while($row = mysql_fetch_row($result))
							{
								$return.= 'INSERT INTO '.$table.' VALUES(';
								for($j=0; $j<$num_fields; $j++) 
								{
									$row[$j] = addslashes($row[$j]);
									$row[$j] = str_replace("\n","\\n",$row[$j]);
									if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
									if ($j<($num_fields-1)) { $return.= ','; }
								}
								$return.= ");\n";
							}
						}
						$return.="\n\n\n";
					}
				}
				
				//save file
				$handle = fopen($file,'w+');
				fwrite($handle,$return);
				fclose($handle);
				
				$msg = $translator->translator('database_file_backup_successfull',$file_name);
				$json_arr = array('status' => 'ok','data' => array('name' => $file_name,'path' => $file_path), 'msg' => $msg);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}		
	}
	
	public function restoreAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
			
		if ($this->_request->isPost()) 
		{
			$file_name = $this->_request->getPost('file_name');
			$host_name = $this->_request->getPost('host_name');
			$user_name = $this->_request->getPost('user_name');
			$user_password = $this->_request->getPost('user_password');
			$db_name = $this->_request->getPost('db_name');
			try
			{
				$file_content = file_get_contents(BASE_PATH.DS.'data'.DS.'adminImages'.DS.'backupDb'.DS.$file_name);
				$_DBconn =Zend_Db::factory ('Pdo_Mysql', array('host' => $host_name, 'username' => $user_name, 'password' => $user_password, 'dbname' => $db_name) );
				if($file_content)
				{
					$_DBconn->getConnection()->exec($file_content);
					/*$_DBconn->beginTransaction();
					$_DBconn->query($file_content);
 					$_DBconn->commit();*/
					$msg = $translator->translator('database_file_restore_successfull',$file_name);
					$json_arr = array('status' => 'ok', 'msg' => $msg);
				}
				else
				{
					$msg = $translator->translator('database_file_restore_err',$file_name);
					$json_arr = array('status' => 'ok', 'msg' => $msg);
				}
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err','msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$perm = new Database_View_Helper_Allow();			
			if ($perm->allow('delete','backend','Database'))
			{
				$file_name = str_replace('-', '_', $this->_request->getPost('name'));					
				$file_path = BASE_PATH.DS.'data'.DS.'adminImages'.DS.'backupDb';				
				
				try
				{
					if($file_name)
					{
						$dir = $file_path.DS.$file_name;
						$res = Eicra_File_Utility::deleteRescursiveDir($dir);
						if($res)
						{
							$msg = 	$translator->translator('database_file_delete_success',$file_name);			
							$json_arr = array('status' => 'ok','msg' => $msg);
						}
						else
						{
							$msg = 	$translator->translator('database_file_delete_error',$file_name);			
							$json_arr = array('status' => 'err','msg' => $msg);
						}
					}
					else
					{
						$msg = 	$translator->translator('database_file_name_empty');			
						$json_arr = array('status' => 'err','msg' => $msg);
					}					
										
				}
				catch (Exception $e) 
				{			
					$json_arr = array('status' => 'err','msg' => $e->getMessage());
				}
			}
			else
			{
				$msg = 	$translator->translator('daatabase_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('page_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
		//Convert To JSON ARRAY	
		$res_value = Zend_Json_Encoder::encode($json_arr);	
		$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			$id_str = $this->_request->getPost('id_st');
			$perm = new Database_View_Helper_Allow();			
			if ($perm->allow('delete','backend','Database'))
			{
				if(!empty($id_str))
				{	
					$id_arr = explode(',',$id_str);		
					$file_path = BASE_PATH.DS.'data'.DS.'adminImages'.DS.'backupDb';
					
					foreach($id_arr as $file_name)
					{				
						try
						{
							if($file_name)
							{
								$dir = $file_path.DS.$file_name;
								$res = Eicra_File_Utility::deleteRescursiveDir($dir);
								if($res)
								{
									$msg = 	$translator->translator('database_file_delete_success',$file_name);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
								else
								{
									$msg = 	$translator->translator('database_file_delete_error',$file_name);			
									$json_arr = array('status' => 'ok','msg' => $msg);
								}
							}
							else
							{
								$msg = 	$translator->translator('database_file_name_empty');			
								$json_arr = array('status' => 'ok','msg' => $msg);
							}					
												
						}
						catch (Exception $e) 
						{			
							$json_arr = array('status' => 'ok','msg' => $e->getMessage());
						}
					}
				}
				else
				{
					$msg = $translator->translator("common_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = 	$translator->translator('daatabase_delete_perm');			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('category_list_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
}

