<?php
class Database_Form_UploadForm  extends Zend_Form 
{
	protected $_editor;
	
	public function __construct($options = null) 
	{
		$translator = Zend_Registry::get('translator');
        $config = (file_exists( APPLICATION_PATH.'/modules/Database/forms/source/'.$translator->getLangFile().'.UploadForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Database/forms/source/'.$translator->getLangFile().'.UploadForm.ini', 'database') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Database/forms/source/en_US.UploadForm.ini', 'database');
		parent::__construct($config->database );
	}

	public function init()
	{
		 $this->createForm();			 
	}

	public function createForm ()
	{ 		 	 
	 	$this->elementDecorator();			 		
	}
	
	//Element Decorator
	private function elementDecorator()
	{
		$this->setElementDecorators(array(
				'ViewHelper','FormElements',										
				
			));	
		$this->upload_file->setDecorators(array(
				'file','file',										
				
			));	
		$this->upload_file->setMaxFileSize(0);	
	}	
}