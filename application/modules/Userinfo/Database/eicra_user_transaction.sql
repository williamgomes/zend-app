-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2014 at 07:37 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project1`
--

-- --------------------------------------------------------

--
-- Table structure for table `eicra_user_transaction`
--

CREATE TABLE IF NOT EXISTS `eicra_user_transaction` (
  `UT_id` int(11) NOT NULL AUTO_INCREMENT,
  `UT_user_id` int(11) NOT NULL,
  `UT_transaction_date` date NOT NULL,
  `UT_invoice_no` varchar(255) NOT NULL,
  `UT_purchase_amount` decimal(10,2) NOT NULL,
  `UT_dispute_amount` decimal(10,2) NOT NULL,
  `UT_paid_amount` decimal(10,2) NOT NULL,
  `UT_due_amount` decimal(10,2) NOT NULL,
  `UT_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UT_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='UT = user transaction' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `eicra_user_transaction`
--

INSERT INTO `eicra_user_transaction` (`UT_id`, `UT_user_id`, `UT_transaction_date`, `UT_invoice_no`, `UT_purchase_amount`, `UT_dispute_amount`, `UT_paid_amount`, `UT_due_amount`, `UT_updated`) VALUES
(6, 161, '2014-09-01', 'INV124578', '1245.00', '1245.00', '1245.00', '1245.00', '2014-09-15 10:28:39'),
(7, 161, '2014-09-06', 'INV125678', '2345.00', '1245.00', '2345.00', '2345.00', '2014-09-15 10:29:00'),
(8, 162, '2014-09-10', 'INV128956', '5689.00', '5689.00', '5689.00', '5689.00', '2014-09-15 10:29:37'),
(9, 162, '2014-09-15', 'INV128956', '4589.00', '4589.00', '4589.00', '4589.00', '2014-09-15 10:29:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
