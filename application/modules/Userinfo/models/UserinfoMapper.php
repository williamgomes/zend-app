<?php

class Userinfo_Model_UserinfoMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Userinfo_Model_DbTable_Userinfo');
        }
        return $this->_dbTable;
    }

    public function saveUser(Userinfo_Model_Userinfo $obj) {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['user_id']);

            $data = array(
                'user_first_name' => $obj->getUser_first_name(),
                'user_last_name' => $obj->getUser_last_name(),
                'user_email' => $obj->getUser_email(),
                'user_company' => $obj->getUser_company(),
                'user_type' => $obj->getUser_type()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            $data = array(
                'user_first_name' => $obj->getUser_first_name(),
                'user_last_name' => $obj->getUser_last_name(),
                'user_email' => $obj->getUser_email(),
                'user_company' => $obj->getUser_company(),
                'user_type' => $obj->getUser_type()
            );
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('user_id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }
    
    
    public function saveVendor(Userinfo_Model_Userinfo $obj) {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['user_id']);

            $data = array(
                'user_first_name' => $obj->getVendor_first_name(),
                'user_last_name' => $obj->getVendor_last_name(),
                'user_email' => $obj->getVendor_email(),
                'user_company' => $obj->getVendor_company(),
                'user_type' => $obj->getUser_type()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            $data = array(
                'user_first_name' => $obj->getVendor_first_name(),
                'user_last_name' => $obj->getVendor_last_name(),
                'user_email' => $obj->getVendor_email(),
                'user_company' => $obj->getVendor_company(),
                'user_type' => $obj->getUser_type()
            );
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('user_id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }
    
    
    public function saveManager(Userinfo_Model_Userinfo $obj) {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['user_id']);

            $data = array(
                'user_first_name' => $obj->getManager_first_name(),
                'user_last_name' => $obj->getManager_last_name(),
                'user_email' => $obj->getManager_email(),
                'user_company' => $obj->getManager_company(),
                'user_skype' => $obj->getManager_skype(),
                'user_phone' => $obj->getManager_phone(),
                'user_type' => $obj->getUser_type()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            $data = array(
                'user_first_name' => $obj->getManager_first_name(),
                'user_last_name' => $obj->getManager_last_name(),
                'user_email' => $obj->getManager_email(),
                'user_company' => $obj->getManager_company(),
                'user_skype' => $obj->getManager_skype(),
                'user_phone' => $obj->getManager_phone(),
                'user_type' => $obj->getUser_type()
            );
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('user_id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }

}

?>