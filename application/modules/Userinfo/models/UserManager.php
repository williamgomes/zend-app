<?php

class Userinfo_Model_UserManager {

    protected $_Id;
    protected $_Select_user;
    protected $_Select_manager;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setiId($id) {
        $this->_Id = $id;
        return $this;
    }
    
    public function setSelect_user($id) {
        $this->_Select_user = $id;
        return $this;
    }
    
    public function setSelect_manager($id) {
        $this->_Select_manager = $id;
        return $this;
    }
    
    
    
    
    public function getId() {
        return $this->_Id;
    }
    
    public function getSelect_user() {
        return $this->_Select_user;
    }
    
    public function getSelect_manager() {
        return $this->_Select_manager;
    }
    
    
    
    
    public function saveUserManager(){
        $mapper = new Userinfo_Model_UserManagerMapper();
        $return = $mapper->saveUserManager($this);
        return $return;
    }
}

?>