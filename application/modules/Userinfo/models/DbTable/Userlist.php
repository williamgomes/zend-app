<?php

/**
 * This is the DbTable class for the autos_page table.
 */
class Userinfo_Model_DbTable_Userlist extends Eicra_Abstract_DbTable {

    /* Table name */
    protected $_name = 'user_profile';
    protected $_cols = null;

    
    
    public function getAllUser($pageNumber, $search_params = null, $tableColumns = null) {
        
        $users_list_column_arr = ($tableColumns && is_array($tableColumns) && $tableColumns['user_profile'] && is_array($tableColumns['user_profile'])) ? $tableColumns['user_profile'] : array('upro.*', 'user_name' => 'CONCAT(upro.firstName, " ", upro.lastName)');

        try {
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('upro' => $this->_name), $users_list_column_arr)
                    ->where("upro.role_id=111")
                    ->orWhere("upro.role_id=112")
                    ->joinLeft(array('ro' => Zend_Registry::get('dbPrefix') . 'roles'), 'ro.role_id = upro.role_id', array('user_type'=>'ro.role_name'));;
            
            
            if ($search_params != null) {
                if ($search_params['sort']) {
                    foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                        if ($sort_value_arr['dir'] == 'exp') {
                            $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                        } else {
                            $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                        }
                    }
                } else {
                    $select->order("upro.user_id ASC")
                            ->order('upro.firstName ASC');
                }

                if ($search_params['filter'] && $search_params['filter']['filters']) {
                    $search_services = new Search_Service_Services($this);
                    $where = $search_services->getSearchWhereClause($search_params);
                    if (!empty($where)) {
                        $select->where($where);
                    }
                }
            } else {
                $select->order('upro.user_id ASC');
            }
            
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
    
    
    
    public function getOperatorString($operator_arr) {
        $table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'upro.' : '';
        $field_array = explode('_', $operator_arr['field']);
        if (in_array('date', $field_array)) {
            $data_arr = preg_split('/[- :]/', $operator_arr['value']);
            if ($data_arr[0]) {
                $time = strtotime($data_arr[0] . ' ' . $data_arr[1] . ' ' . $data_arr[2] . ' ' . $data_arr[3] . ' ' . $data_arr[4] . ':' . $data_arr[5] . ':' . $data_arr[6]);
                $operator_arr['value'] = date("Y-m-d H:i:s", $time);
            }
        }

        $operatorFirstPart = '';
        switch ($operator_arr['field']) {
            case 'user_name' :
                $operatorFirstPart = " CONCAT(" . $table_prefix . "user_first_name, ' ', " . $table_prefix . "user_last_name) ";
                break;
//            case 'autos_id_string' :
//                $id_arr = explode(',', $operator_arr['value']);
//                $id_arr = array_filter($id_arr);
//                $ids = implode(',', $id_arr);
//                $operatorFirstPart = " upro.id IN(" . $ids . ") AND 1";
//                $operator_arr['value'] = '1';
//                break;
            default:
                $operatorFirstPart = $table_prefix . $operator_arr['field'];
                break;
        }
        $operatorString = '';
        switch ($operator_arr['operator']) {
            case 'eq':
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'neq':
                $operatorString = $operatorFirstPart . ' != "' . $operator_arr['value'] . '" ';
                break;
            case 'startswith':
                $operatorString = $operatorFirstPart . ' LIKE "' . $operator_arr['value'] . '%" ';
                break;
            case 'contains':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'doesnotcontain':
                $operatorString = $operatorFirstPart . ' NOT LIKE "%' . $operator_arr['value'] . '%" ';
                break;
            case 'endswith':
                $operatorString = $operatorFirstPart . ' LIKE "%' . $operator_arr['value'] . '" ';
                break;
            case 'gte':
                $operatorString = $operatorFirstPart . ' >=  "' . $operator_arr['value'] . '" ';
                break;
            case 'gt':
                $operatorString = $operatorFirstPart . ' > "' . $operator_arr['value'] . '" ';
                break;
            case 'lte':
                $operatorString = $operatorFirstPart . ' <= "' . $operator_arr['value'] . '" ';
                break;
            case 'lt':
                $operatorString = $operatorFirstPart . ' < "' . $operator_arr['value'] . '" ';
                break;
            case 'eqy':
                $operatorString = 'YEAR(upro.autos_date)' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqm':
                $operatorString = 'date_format(upro.autos_date, "%M")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            case 'eqd':
                $operatorString = 'date_format(upro.autos_date, "%d")' . ' = "' . $operator_arr['value'] . '" ';
                break;
            default:
                $operatorString = $operatorFirstPart . ' = "' . $operator_arr['value'] . '" ';
                break;
        }
        return $operatorString;
    }

    public function isColumnExists($column) {
        $this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;
        return in_array($column, $this->_cols);
    }
    
}

?>