<?php

/**
 * This is the DbTable class for the autos_page table.
 */
class Userinfo_Model_DbTable_Userinfo extends Eicra_Abstract_DbTable {

    /* Table name */
    protected $_name = 'user_profile';
    protected $_cols = null;

    
    
    public function getAllUser() {

        try {
            $select = $this->select()
                    ->from(array('upro' => $this->_name), array('id' => 'upro.user_id', 'full_name' => 'CONCAT(upro.firstName," ",upro.lastName)'))
                    ->where('upro.role_id = 111')
                    ->orWhere('upro.role_id = 112');
            $select->order('upro.firstName ASC');
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
    public function getAllManager(){
        try {
            $select = $this->select()
                    ->from(array('upro' => $this->_name), array('*'))
                    ->where('upro.role_id ="103"')
                    ->order('upro.firstName ASC');
            
            $options = $this->getAdapter()->fetchAll($select);
          
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
}

?>