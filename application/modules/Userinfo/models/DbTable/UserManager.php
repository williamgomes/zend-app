<?php

/**
 * This is the DbTable class for the autos_page table.
 */
class Userinfo_Model_DbTable_UserManager extends Eicra_Abstract_DbTable {

    /* Table name */
    protected $_name = 'user_manager';
    protected $_cols = null;

    
    public function getManagerInfo($userID) {

        try {
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('um' => $this->_name), array(''))
                    ->where('um.user_id =?', $userID)
                    ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'up.user_id = um.manager_id', array('*'));
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
    
    
    public function showConInfo($pageNumber, $search_params = null) {

        try {
            $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('um' => $this->_name), array('updated' => 'um.updated', 'id' => 'um.id'))
                    ->joinLeft(array('up' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'up.user_id = um.manager_id', array('up.*', 'managerName' => 'CONCAT(up.firstName, " ", up.lastName)'))
                    ->joinLeft(array('upro' => Zend_Registry::get('dbPrefix') . 'user_profile'), 'upro.user_id = um.user_id', array('upro.*', 'userName' => 'CONCAT(upro.firstName, " ", upro.lastName)'));
            
            
            if ($search_params != null) {
                if ($search_params['sort']) {
                    foreach ($search_params['sort'] as $sort_key => $sort_value_arr) {
                        if ($sort_value_arr['dir'] == 'exp') {
                            $select->order(new Zend_Db_Expr($sort_value_arr['field']));
                        } else {
                            $select->order($sort_value_arr['field'] . ' ' . $sort_value_arr['dir']);
                        }
                    }
                } else {
                    $select->order("um.id ASC");
                }

                if ($search_params['filter'] && $search_params['filter']['filters']) {
                    $search_services = new Search_Service_Services($this);
                    $where = $search_services->getSearchWhereClause($search_params);
                    if (!empty($where)) {
                        $select->where($where);
                    }
                }
            } else {
                $select->order('um.id ASC');
            }
            
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    
}

?>