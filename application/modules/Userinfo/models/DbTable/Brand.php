<?php
/**
* This is the DbTable class for the autos_brand table.
*/
class Autos_Model_DbTable_Brand extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'autos_brand';
	protected $_cols	=	null;
	
	//Get Datas
	public function getBrandInfo($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) 
		{
           // throw new Exception("Count not find row $id");
		   $options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
		}
        return   $options ; 
    }
	
	//Get Datas
	public function getAllBrand()
    {
        $select = $this->select()
                       ->from($this->_name, array('id', 'brand_name'))
                       ->order('brand_name ASC');
        $options = $this->getAdapter()->fetchPairs($select);
        return $options;
    }	
		
	//Get Datas
	public function getOptions($entry_by = null)
    {
		if($entry_by)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'brand_name'))
						   ->where('entry_by =?',$entry_by)
						   ->order('id ASC');			
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'brand_name'))
						   ->order('id ASC');
		}
		$options = $this->getAdapter()->fetchAll($select);        
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($entry_by = null)
    {
		if($entry_by)
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'brand_name'))
						   ->where('entry_by =?',$entry_by)
						   ->order('id ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('id', 'brand_name'))
						   ->order('id ASC');			
		} 
		$options = $this->getAdapter()->fetchPairs($select);       
        return $options;
    }
	
	public function isDuplicate($value, $group_id = null, $id = null)
    {
		if(empty($group_id))
		{
			if(empty($id))
			{
				$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' => $this->_name,
							'field' => 'brand_name'
						)
					);
			}
			else
			{
				$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' => $this->_name,
						'field' => 'brand_name',
						'exclude' => array('field' => 'id', 'value' => $id)
					)
				);
			}
		} 
		else
		{			
			if(empty($id))
			{	
				$validator = new Zend_Validate_Db_RecordExists(
						array(
							'table' 	=> 		$this->_name,
							'field' 	=> 		'brand_name',
							'exclude' 	=> 		'group_id ='. $group_id
						)
					);
			}
			else
			{
				$validator = new Zend_Validate_Db_RecordExists(
					array(
						'table' 	=> 		$this->_name,
						'field' 	=> 		'brand_name',
						'exclude' 	=>  	'group_id =' . $group_id . ' AND id != ' . $id
					)
				);
			}				
		}
		return $validator->isValid($value);
	}
	
	//Get All Datas
	public function getListInfo($approve = null, $search_params = null, $userChecking = true) 
    {
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;		
		
		$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('ab' => $this->_name),array('ab.*'))
						   ->joinLeft(array('up' => Zend_Registry::get('dbPrefix').'user_profile'), 'ab.entry_by = up.user_id', array( 'username' => 'up.username', 'full_name' => " CONCAT(up.title, ' ', up.firstName, ' ', up.lastName) "));
		
		if($user_id && $auth->getIdentity()->access_other_user_article != '1' && $userChecking == true)
		{
			$select->where('ab.entry_by = ?' . $user_id);
		}
		
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['field'] == 'group')
					{
						$select->order(' group_name '.$sort_value_arr['dir']);
					}
					else
					{
						if($sort_value_arr['dir'] == 'exp')
						{
							$select->order(new Zend_Db_Expr($sort_value_arr['field']));
						}
						else
						{
							$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
						}
					}
				}
			}
			else
			{
				$select->order("ab.id ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{										
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("ab.id ASC"); 
		}
		
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options;   
	}
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ab.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			

				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;			
			default:
				$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(ab.entry_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(ab.entry_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(ab.entry_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}
	
}

?>