<?php

/**
 * This is the DbTable class for the autos_page table.
 */
class Userinfo_Model_DbTable_ManagerInfo extends Eicra_Abstract_DbTable {

    /* Table name */
    protected $_name = 'user_manager';
    protected $_cols = null;
    
    
    
    
    public function getManagerInfo($userID) {

        try {
            $select = $this->select()
                    ->from(array('um' => $this->_name), array(''))
                    ->where('um.user_id =?', $userID)
                    ->joinLeft(array('ac' => Zend_Registry::get('dbPrefix') . 'autos_category'), 'ac.id = ap.category_id', array('category_id' => 'ac.id', 'category_name' => 'ac.category_name'));
            
            
            if ($isClient == true AND $isVendor == false) {
                $select->where('uinfo.user_type ="client"');
            } elseif (isClient == false AND $isVendor == true) {
                $select->where('uinfo.user_type ="vendor"');
            } elseif (isClient == true AND $isVendor == true) {
                $select->where('uinfo.user_type ="client"')
                        ->orWhere('uinfo.user_type ="vendor"');
            }
            $select->order('uinfo.user_first_name ASC');
            $options = $this->getAdapter()->fetchAll($select);
            return $options;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
}

?>