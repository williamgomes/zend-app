<?php

class Userinfo_Model_Usertransaction {

    protected $_id;
    protected $_user_id;
    protected $_Invoice_no;
    protected $_Date;
    protected $_Purchase_amount;
    protected $_Dispute_amount;
    protected $_Paid_amount;
    protected $_Due_amount;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    
    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setUser_id($text) {
        $this->_user_id = $text;
        return $this;
    }
    
    public function setInvoice_no($text) {
        $this->_Invoice_no = $text;
        return $this;
    }
    
    public function setDate($text) {
        $this->_Date = $text;
        return $this;
    }
    
    public function setPurchase_amount($text) {
        $this->_Purchase_amount = $text;
        return $this;
    }
    
    public function setDispute_amount($text) {
        $this->_Dispute_amount = $text;
        return $this;
    }
    
    public function setPaid_amount($text) {
        $this->_Paid_amount = $text;
        return $this;
    }
    
    public function setDue_amount($text) {
        $this->_Due_amount = $text;
        return $this;
    }
    
    
    

    public function getId() {
        return $this->_id;
    }

    public function getUser_id() {
        return $this->_user_id;
    }
    
    public function getInvoice_no() {
        return $this->_Invoice_no;
    }
    
    public function getDate() {
        return $this->_Date;
    }
    
    public function getPurchase_amount() {
        return $this->_Purchase_amount;
    }
    
    public function getdispute_amount() {
        return $this->_Dispute_amount;
    }
    
    public function getPaid_amount() {
        return $this->_Paid_amount;
    }
    
    public function getDue_amount() {
        return $this->_Due_amount;
    }
    
    
    
    public function saveTransaction() {
        $mapper = new Userinfo_Model_UsertransactionMapper();
        $return = $mapper->saveTransaction($this);
        return $return;
    }
}

?>