<?php

class Userinfo_Model_ManagerInfoViewer {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Userinfo_Model_DbTable_UserManager');
        }
        return $this->_dbTable;
    }

    public function getUserManager($userID) {
        $resultSet = $this->getDbTable()->getManagerInfo($userID);
        return $resultSet;
    }

}

?>