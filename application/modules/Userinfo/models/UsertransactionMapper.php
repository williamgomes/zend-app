<?php

class Userinfo_Model_UsertransactionMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Userinfo_Model_DbTable_Usertransaction');
        }
        return $this->_dbTable;
    }

    public function saveTransaction(Userinfo_Model_Usertransaction $obj) {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['UT_id']);

            $data = array(
                'UT_user_id' => $obj->getUser_id(),
                'UT_transaction_date' => $obj->getDate(),
                'UT_invoice_no' => $obj->getInvoice_no(),
                'UT_purchase_amount' => $obj->getPurchase_amount(),
                'UT_dispute_amount' => $obj->getdispute_amount(),
                'UT_paid_amount' => $obj->getPaid_amount(),
                'UT_due_amount' => $obj->getDue_amount()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
//            $data = array(
//                'user_first_name' => $obj->getUser_first_name(),
//                'user_last_name' => $obj->getUser_last_name(),
//                'user_email' => $obj->getUser_email(),
//                'user_company' => $obj->getUser_company(),
//                'user_type' => $obj->getUser_type()
//            );
//            // Start the Update process
//            try {
//                $this->getDbTable()->update($data, array('user_id = ?' => $id));
//                $result = array('status' => 'ok', 'id' => $id);
//            } catch (Exception $e) {
//                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
//            }
        }
        return $result;
    }
   
}

?>