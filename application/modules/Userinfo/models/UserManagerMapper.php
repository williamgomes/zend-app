<?php

class Userinfo_Model_UserManagerMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Userinfo_Model_DbTable_UserManager');
        }
        return $this->_dbTable;
    }

    public function saveUserManager(Userinfo_Model_UserManager $obj) {
        if ((null === ($id = $obj->getId())) || empty($id)) {
            unset($data['id']);

            $data = array(
                'user_id' => $obj->getSelect_user(),
                'manager_id' => $obj->getSelect_manager()
            );
            try {
                $last_id = $this->getDbTable()->insert($data);
                $result = array('status' => 'ok', 'id' => $last_id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $last_id, 'msg' => $e->getMessage());
            }
        } else {
            $data = array(
                'user_id' => $obj->getSelect_user(),
                'manager_id' => $obj->getSelect_manager()
            );
            // Start the Update process
            try {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                $result = array('status' => 'ok', 'id' => $id);
            } catch (Exception $e) {
                $result = array('status' => 'err', 'id' => $id, 'msg' => $e->getMessage());
            }
        }
        return $result;
    }
    
    
    
    
    
    public function showConnection($pageNumber, $search_params = null) {
        $resultSet = $this->getDbTable()->showConInfo($pageNumber, $search_params);
        $viewPageNum = Eicra_Global_Variable::getSession()->viewPageNum;
        $paginator = Zend_Paginator::factory($resultSet);
        $paginator->setItemCountPerPage($viewPageNum);
        $paginator->setCurrentPageNumber($pageNumber);
        return $paginator;
    }
}

?>