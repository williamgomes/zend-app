<?php

class Userinfo_Model_Userinfo {

    protected $_id;
    protected $_User_first_name;
    protected $_User_last_name;
    protected $_User_email;
    protected $_User_company;
    protected $_User_type;
    protected $_Vendor_first_name;
    protected $_Vendor_last_name;
    protected $_Vendor_email;
    protected $_Vendor_company;
    protected $_Manager_first_name;
    protected $_Manager_last_name;
    protected $_Manager_email;
    protected $_Manager_company;
    protected $_Manager_skype;
    protected $_Manager_phone;
    protected $_Select_user;
    protected $_Select_manager;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Auth autos');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    
    public function setId($text) {
        $this->_id = $text;
        return $this;
    }

    public function setUser_first_name($text) {
        $this->_User_first_name = $text;
        return $this;
    }
    
    public function setUser_last_name($text) {
        $this->_User_last_name = $text;
        return $this;
    }
    
    public function setUser_email($text) {
        $this->_User_email = $text;
        return $this;
    }
    
    public function setUser_company($text) {
        $this->_User_company = $text;
        return $this;
    }
    
    public function setVendor_first_name($text) {
        $this->_Vendor_first_name = $text;
        return $this;
    }
    
    public function setVendor_last_name($text) {
        $this->_Vendor_last_name = $text;
        return $this;
    }
    
    public function setVendor_email($text) {
        $this->_Vendor_email = $text;
        return $this;
    }
    
    public function setVendor_company($text) {
        $this->_Vendor_company = $text;
        return $this;
    }
    
    public function setManager_first_name($text) {
        $this->_Manager_first_name = $text;
        return $this;
    }
    
    public function setManager_last_name($text) {
        $this->_Manager_last_name = $text;
        return $this;
    }
    
    public function setManager_email($text) {
        $this->_Manager_email = $text;
        return $this;
    }
    
    public function setManager_company($text) {
        $this->_Manager_company = $text;
        return $this;
    }
    
    public function setManager_skype($text) {
        $this->_Manager_skype = $text;
        return $this;
    }
    
    public function setManager_phone($text) {
        $this->_Manager_phone = $text;
        return $this;
    }
    
    public function setUser_type($text) {
        $this->_User_type = $text;
        return $this;
    }
    
    public function setSelect_user($id) {
        $this->_Select_user = $id;
        return $this;
    }
    
    public function setSelect_manager($id) {
        $this->_Select_manager = $id;
        return $this;
    }
    
    

    public function getId() {
        return $this->_id;
    }

    public function getUser_first_name() {
        return $this->_User_first_name;
    }
    
    public function getUser_last_name() {
        return $this->_User_last_name;
    }
    
    public function getUser_email() {
        return $this->_User_email;
    }
    
    public function getUser_company() {
        return $this->_User_company;
    }
    
    public function getVendor_first_name() {
        return $this->_Vendor_first_name;
    }
    
    public function getVendor_last_name() {
        return $this->_Vendor_last_name;
    }
    
    public function getVendor_email() {
        return $this->_Vendor_email;
    }
    
    public function getVendor_company() {
        return $this->_Vendor_company;
    }
    
    public function getManager_first_name() {
        return $this->_Manager_first_name;
    }
    
    public function getManager_last_name() {
        return $this->_Manager_last_name;
    }
    
    public function getManager_email() {
        return $this->_Manager_email;
    }
    
    public function getManager_company() {
        return $this->_Manager_company;
    }
    
    public function getManager_skype() {
        return $this->_Manager_skype;
    }
    
    public function getManager_phone() {
        return $this->_Manager_phone;
    }
    
    public function getUser_type() {
        return $this->_User_type;
    }
    
    public function getSelect_user() {
        return $this->_Select_user;
    }
    
    public function getSelect_manager() {
        return $this->_Select_manager;
    }
    
    
    
    
    
    public function saveUserinfo() {
        $mapper = new Userinfo_Model_UserinfoMapper();
        $return = $mapper->saveUser($this);
        return $return;
    }
    
    
    
    public function saveVendorinfo() {
        $mapper = new Userinfo_Model_UserinfoMapper();
        $return = $mapper->saveVendor($this);
        return $return;
    }
    
    
    public function saveManagerinfo() {
        $mapper = new Userinfo_Model_UserinfoMapper();
        $return = $mapper->saveManager($this);
        return $return;
    }
    
}

?>