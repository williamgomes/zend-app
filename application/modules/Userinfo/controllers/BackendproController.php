<?php

class Userinfo_BackendproController extends Zend_Controller_Action {

    private $assignForm;
    private $addManagerForm;
    private $addTransactionForm;
    private $_controllerCache;
    private $_auth_obj;
    private $translator;

    public function init() {
        /* Initialize action controller here */
        $this->assignForm = new Userinfo_Form_AssignManager ();
        $this->addManagerForm = new Userinfo_Form_AddManager ();
        $this->addTransactionForm = new Userinfo_Form_AddTransaction ();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch() {

        $this->_helper->layout->setLayout('layout');
        $this->_helper->layout->setLayoutPath(MODULE_PATH . '/Administrator/layouts/scripts');

        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);


        $url = $this->view->serverUrl() . $this->view->baseUrl() . '/Administrator/login';
        Eicra_Global_Variable::checkSession($this->_response, $url);
        /* if($getAction != 'uploadfile')
          {
          $url = Zend_Registry::get('config')->eicra->params->domain.$this->view->baseUrl().'/Administrator/login';
          Eicra_Global_Variable::checkSession($this->_response,$url);

          //Check Module License
          $modules_license = new Administrator_Controller_Helper_ModuleLoader();
          $modules_license->getModulesLicenseMsg($this->_request->getModuleName());
          } */
    }

    public function assignmanagerAction() {
        
        $id = $this->_request->getParam('id');
        
        
        if ($this->_request->isPost()) {
            try {

                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->assignForm->isValid($this->_request->getPost())) {
                    $checkArr = array();
                    $saveUserManager = new Userinfo_Model_UserManager($this->assignForm->getValues());
                    $userHelper = new Userinfo_Controller_Helper_User();
                    $userID = $this->_request->getPost('Select_user');
                    $checkArr = $userHelper->checkUser($userID);

                    if ($checkArr[0]['row_count'] > 0) {
                        $json_arr = array('status' => 'err', 'msg' => "Manager already assigned to this User");
                    } else {
                        $result = $saveUserManager->saveUserManager();
                        if ($result['status'] == 'ok') {
                            $msg = $this->view->translator->translator("page_save_success");
                            $json_arr = array('status' => 'ok', 'msg' => $msg);
                        } else {
                            $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                        }
                    }
                } else {
                    $validatorMsg = $this->assignForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            
            if(!empty($id)){
                $assignModel = new Userinfo_Model_UserManager($this->assignForm->getValues());
                
            } else {
                $this->view->assignManagerForm = $this->assignForm;
            }
        }
    }
    
    
    
    

    public function addmanagerAction() {
        if ($this->_request->isPost()) {
            try {

                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();
//
                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->addManagerForm->isValid($this->_request->getPost())) {
                    $userinfo = new Userinfo_Model_Userinfo($this->addManagerForm->getValues());
                    $result = $userinfo->saveManagerinfo();
                    if ($result['status'] == 'ok') {
                        $msg = $this->view->translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->addManagerForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->addManagerForm = $this->addManagerForm;
        }
    }

    
    
    
    public function listAction() {
        $user_type = $this->_request->getParam('user_type');
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');
                
                
               if ($user_type) {
                    $posted_data['filter']['filters'][] = array('field' => 'user_type', 'operator' => 'eq', 'value' => $this->_request->getParam('user_type'));
                }
                //$posted_data['hasChild'] = false;

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'user_type' => $user_type, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $list_mapper = new Userinfo_Model_UserListMapper();
                    $list_datas = $list_mapper->getAllUser($pageNumber,$posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['user_id']);
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }
    
    
    
    
    public function transactionAction() {
        if ($this->_request->isPost()) {
            try {

                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();

                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->addTransactionForm->isValid($this->_request->getPost())) {
                    $userinfo = new Userinfo_Model_Usertransaction($this->addTransactionForm->getValues());
                    $result = $userinfo->saveTransaction();
                    if ($result['status'] == 'ok') {
                        $msg = $this->view->translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->addTransactionForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $user_id = $this->_request->getParam('user_id');
            $this->addTransactionForm->user_id->setValue($user_id);
            $this->view->addTransactionForm = $this->addTransactionForm;
        }
    }
    
    
    
    
    
    
    public function translistAction() {
        
        $user_id = $this->_request->getParam('user_id');
//        $this->view->group_id = $group_id;
//
//        $group_db = new Autos_Model_DbTable_AutosGroup();
//        $group_info = ($group_id) ? $group_db->getGroupName($group_id) : null;
//        $this->view->group_info = $group_info;
//
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');
                
                
               if ($user_id) {
                    $posted_data['filter']['filters'][] = array('field' => 'UT_user_id', 'operator' => 'eq', 'value' => $this->_request->getParam('user_id'));
                }
                //$posted_data['hasChild'] = false;

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'user_id' => $user_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $list_mapper = new Userinfo_Model_TransListMapper();
                    $list_datas = $list_mapper->getUserTransaction($pageNumber,$posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['UT_id']);
                            $entry_arr['UT_transaction_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['UT_transaction_date'])));	
                            $entry_arr['UT_transaction_date_format']=  $this->view->numbers(date('d M, Y',strtotime($entry_arr['UT_transaction_date'])));
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }
    
    
    
    
    
    
    
    public function connectionAction() {

      if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');
                
                
//               if ($user_id) {
//                    $posted_data['filter']['filters'][] = array('field' => 'UT_user_id', 'operator' => 'eq', 'value' => $this->_request->getParam('user_id'));
//                }
                //$posted_data['hasChild'] = false;

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $connection_mapper = new Userinfo_Model_UserManagerMapper();
                    $list_datas = $connection_mapper->showConnection($pageNumber,$posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['id']);
                            $entry_arr['update_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['updated'])));	
                            $entry_arr['update_date_format']=  $this->view->numbers(date('d M, Y',strtotime($entry_arr['updated'])));
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }
    
    
    
    
}
    