<?php

class Userinfo_FrontendController extends Zend_Controller_Action {

    private $_DBconn;
    private $_page_id;
    private $_controllerCache;
    private $_auth_obj;
    private $currency;
    private $translator;
    private $_snopphing_cart;
    private $_ckLicense = true;
    private $userRegForm;
    private $vendorRegForm;
    private $usertype;

    public function init() {
        /*Initializing vendor registration form*/
        $this->userRegForm = new Userinfo_Form_RegistrationFormUser ();
        $this->vendorRegForm = new Userinfo_Form_RegistrationFormVendor ();
        $this->usertype = new Userinfo_Form_UserType ();
        
        /* Initialize action controller here */
        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');

        //DB Connection
        $this->_DBconn = Zend_Registry::get('msqli_connection');
        $this->_DBconn->getConnection();

        $auth = Zend_Auth::getInstance();
        $this->_auth_obj = ($auth->hasIdentity()) ? $auth->getIdentity() : '';
        $this->view->assign('auth', $auth);

        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);

        //Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();

        /* Check Module License */
        //$this->_ckLicense = Eicra_License_Version::checkModulesLicense();
        if ($this->_ckLicense == true) {

            $license = new Zend_Session_Namespace('License');
            if (!$license || !$license->license_data || !$license->license_data['modules']) {
                $curlObj = new Eicra_License_Version();
                $curlObj->sendInfo(array('dm' => $this->view->serverUrl()));
                $license->license_data = $curlObj->getArrayResult();
            }
        }
        $this->_modules_license = new Administrator_Controller_Helper_ModuleLoader();
        $this->_snopphing_cart = ($this->_modules_license->checkModulesLicense('Ecommerce') || $this->_ckLicense == false) ? true : false;
    }

    public function preDispatch() {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayout($template_obj->getLayout(false, array('controller_helper' => $this->_helper, 'view' => $this->view, 'front_template' => $front_template)));
        $this->view->front_template = $front_template;

        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();

            $this->_page_id = (!empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }
    }

    
    public function reguserAction() {
        if ($this->_request->isPost()) {
            try {
                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();

                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->userRegForm->isValid($this->_request->getPost())) {
                    $userinfo = new Userinfo_Model_Userinfo($this->userRegForm->getValues());
                    $result = $userinfo->saveUserinfo();
                    if ($result['status'] == 'ok') {
                        $msg = $this->view->translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->userRegForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->userregForm = $this->userRegForm;
        }
    }

    public function regvendorAction(){
        if ($this->_request->isPost()) {
            try {
                //for now do nothing
                $this->_helper->viewRenderer->setNoRender();
                $this->_helper->layout->disableLayout();

                $this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);

                if ($this->vendorRegForm->isValid($this->_request->getPost())) {
                    $userinfo = new Userinfo_Model_Userinfo($this->vendorRegForm->getValues());
                    $result = $userinfo->saveVendorinfo();
                    if ($result['status'] == 'ok') {
                        $msg = $this->view->translator->translator("page_save_success");
                        $json_arr = array('status' => 'ok', 'msg' => $msg);
                    } else {
                        $json_arr = array('status' => 'err', 'msg' => $result['msg']);
                    }
                } else {
                    $validatorMsg = $this->vendorRegForm->getMessages();
                    $vMsg = array();
                    $i = 0;
                    foreach ($validatorMsg as $key => $errType) {
                        foreach ($errType as $errkey => $value) {
                            $vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
                            $i++;
                        }
                    }
                    $json_arr = array('status' => 'errV', 'msg' => $vMsg);
                }
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'msg' => $e->getMessage());
            }
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        } else {
            $this->view->venregForm = $this->vendorRegForm;
        }
    }
    
    
    public function usertypeAction(){
        if ($this->_request->isPost()) {
            //for now do nothing
        } else {
            $this->view->selectUserType = $this->usertype;
        }
    }
    
    
    
    public function translistAction() {
        
        $user_id = $this->_request->getParam('user_id');
//        $this->view->group_id = $group_id;
//
//        $group_db = new Autos_Model_DbTable_AutosGroup();
//        $group_info = ($group_id) ? $group_db->getGroupName($group_id) : null;
//        $this->view->group_info = $group_info;
//
        $posted_data = $this->_request->getParams();
        $this->view->assign('posted_data', $posted_data);

        if ($this->_request->isPost()) {
            try {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                // action body
                $pageNumber = ($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');
                $approve = $this->getRequest()->getParam('approve');
                
                
               if ($user_id) {
                    $posted_data['filter']['filters'][] = array('field' => 'UT_user_id', 'operator' => 'eq', 'value' => $this->_request->getParam('user_id'));
                }
                //$posted_data['hasChild'] = false;

                $getViewPageNum = $this->_request->getParam('pageSize');
                $posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'approve' => $approve, 'user_id' => $user_id, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout', true);
                $viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;

                $viewPageNum = (!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
                Eicra_Global_Variable::getSession()->viewPageNum = $viewPageNum;

                $encode_params = Zend_Json_Encoder::encode($posted_data);
                $encode_auth_obj = Zend_Json_Encoder::encode($this->_auth_obj);
                $uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/', '_', $this->view->url() . '_' . $encode_params . '_' . $encode_auth_obj));
                if (($view_datas = $this->_controllerCache->load($uniq_id)) === false) {
                    //Call Class
                    $list_mapper = new Userinfo_Model_TransListMapper();
                    $list_datas = $list_mapper->getUserTransaction($pageNumber,$posted_data);
                    $view_datas = array('data_result' => array(), 'total' => 0);
                    if ($list_datas) {
                        $key = 0;
                        foreach ($list_datas as $entry) {
                            $entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
                            $entry_arr = (is_array($entry_arr)) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
                            $entry_arr['id_format'] = $this->view->numbers($entry_arr['UT_id']);
                            $entry_arr['UT_transaction_date_lang_format']=  $this->view->numbers(date('d/m/Y',strtotime($entry_arr['UT_transaction_date'])));	
                            $entry_arr['UT_transaction_date_format']=  $this->view->numbers(date('d M, Y',strtotime($entry_arr['UT_transaction_date'])));
                            $view_datas['data_result'][$key] = $entry_arr;
                            $key++;
                        }
                        $view_datas['total'] = $list_datas->getTotalItemCount();
                    }
                    $this->_controllerCache->save($view_datas, $uniq_id);
                }


                $json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
            } catch (Exception $e) {
                $json_arr = array('status' => 'err', 'data_result' => '', 'msg' => $e->getMessage());
            }

            //Convert To JSON ARRAY	
            $res_value = Zend_Json_Encoder::encode($json_arr);
            $this->_response->setBody($res_value);
        }
    }
}
