<?php
class Userinfo_Controller_Helper_AutosOrders
{
	protected $_id;
	protected $_group_id;
	protected $_category_id;
	protected $_high_order;
	protected $_translator;
	protected $_autos_order;
	protected $_conn;
	
	public function __construct($id = null)
	{		
		//DB Connection
		$this->_conn = Zend_Registry::get('msqli_connection');
		$this->_translator 	=   Zend_Registry::get('translator');
		if($id != null && $id != '')
		{
			$this->setAutosInfo($id);
			$this->setHeighestOrder($this->_group_id,$this->_category_id);
		}		
	}
	
	public function setAutosInfo($id) 
	{
		$this->_conn->getConnection();
		$this->_id = $id;
		//Get It's Info
		$select = $this->_conn->select()
					   ->from(array('gp' => Zend_Registry::get('dbPrefix').'autos_page'), array('gp.group_id','gp.category_id','gp.autos_order'))
					   ->where('gp.id = ?',$id);
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_group_id = (int)$row['group_id'];
				$this->_autos_order = (int)$row['autos_order'];
				$this->_category_id = (int)$row['category_id'];
			}
		}
	}
	
	public function setHeighestOrder($group_id,$category_id) 
	{
		$this->_conn->getConnection();
		//Get Heighest Order
		if(!empty($this->_id))
		{
			$select = $this->_conn->select()
						   ->from(array('gp' => Zend_Registry::get('dbPrefix').'autos_page'), array('gp.autos_order'))
						   ->where('gp.group_id = ?',$group_id)
						   ->where('gp.id != ?',$this->_id)
						   ->where('gp.category_id = ?',$category_id)
						   ->order(array('gp.autos_order DESC'))
						   ->limit(1);
		}
		else
		{
			$select = $this->_conn->select()
						   ->from(array('gp' => Zend_Registry::get('dbPrefix').'autos_page'), array('gp.autos_order'))
						   ->where('gp.group_id = ?',$group_id)
						   ->where('gp.category_id = ?',$category_id)
						   ->order(array('gp.autos_order DESC'))
						   ->limit(1);
		}
		$rs = $select->query()->fetchAll();
		if($rs)
		{
			foreach($rs as $row)
			{
				$this->_high_order = (int)$row['autos_order'];				
			}
		}
		else
		{
			$this->_high_order = 0;
		}		
	}
	
	public function setNewOrder($id) 
	{
		$this->_conn->getConnection();
		$this->_high_order = $this->_high_order + 1;
		//Get Heighest Order
		$where = array();
		$where[] = 'id = '.$this->_conn->quote($id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $this->_high_order),$where);	
	}
	
	public function decreaseOrder($id = null,$autos_order = null)
	{
		if(empty($this->_id))
		{
			$this->_id = $id;
		}
		if(empty($this->_autos_order))
		{
			$this->_autos_order = $autos_order;
		}
		
		if($this->_autos_order > 1)
		{
			$up_order = $this->_autos_order - 1;
			
			$this->_conn->getConnection();
			
			//Get Autos which Have New Order
			$select = $this->_conn->select()
							   ->from(array('gp' => Zend_Registry::get('dbPrefix').'autos_page'), array('gp.id'))
							   ->where('gp.group_id = ?',$this->_group_id)
							   ->where('gp.category_id = ?',$this->_category_id)
							   ->where('gp.autos_order = ?',$up_order);
			$rs = $select->query()->fetchAll();
			
			if($rs)
			{	
				$id_arr = array();		
				$i=0;
				foreach($rs as $row)
				{
					$id_arr[$i] = (int)$row['id'];	
					$i++;			
				}
			}
			else
			{
				$id_arr = null;
			}
			//Update existing Categories with new Autos autos_order
			$where = array();
			$where[] = 'autos_order = '.$this->_conn->quote($up_order);
			$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
			$where[] = 'category_id = '.$this->_conn->quote($this->_category_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $this->_autos_order),$where);	
			
			//Decrease Autos Order
			$whereI = array();
			$whereI[] = 'id = '.$this->_conn->quote($this->_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $up_order),$whereI);	
			$return_arr = array('status' => 'ok','id_arr' => $id_arr, 'msg' => '');	
		}
		else
		{
			$id_arr = null;
			$return_arr = array('status' => 'err','id_arr' => $id_arr, 'msg' => $this->_translator->translator('common_order_up_error'));
		}
		
		return $return_arr;
	}
	
	public function increaseOrder($id = null,$autos_order = null)
	{
		if(empty($this->_id))
		{
			$this->_id = $id;
		}
		if(empty($this->_autos_order))
		{
			$this->_autos_order = $autos_order;
		}
		
		if($this->_autos_order)
		{
			$new_order = $this->_autos_order + 1;
			
			$this->_conn->getConnection();
			
			//Get Categories which Have New Autos Order
			$select = $this->_conn->select()
							   ->from(array('gp' => Zend_Registry::get('dbPrefix').'autos_page'), array('gp.id'))
							   ->where('gp.group_id = ?',$this->_group_id)
							   ->where('gp.category_id = ?',$this->_category_id)
							   ->where('gp.autos_order = ?',$new_order);
			$rs = $select->query()->fetchAll();
			
			if($rs)
			{	
				$id_arr = array();		
				$i=0;
				foreach($rs as $row)
				{
					$id_arr[$i] = (int)$row['id'];	
					$i++;			
				}
			}
			else
			{
				$id_arr = null;
			}
			//Update existing categories with new category autos_order
			$where = array();
			$where[] = 'autos_order = '.$this->_conn->quote($new_order);
			$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
			$where[] = 'category_id = '.$this->_conn->quote($this->_category_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $this->_autos_order),$where);	
			
			//Increase Autos Order
			$whereI = array();
			$whereI[] = 'id = '.$this->_conn->quote($this->_id);
			$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $new_order),$whereI);	
			$return_arr = array('status' => 'ok','id_arr' => $id_arr, 'msg' => '');
		}
		else
		{
			$id_arr = null;
			$return_arr = array('status' => 'err','id_arr' => $id_arr, 'msg' => $this->_translator->translator('common_order_down_error'));
		}
		
		return $return_arr;
	}
	
	public function saveOrder($new_order)
	{		
		//Update existing categories with new category autos_order
		$where = array();
		$where[] = 'autos_order = '.$this->_conn->quote($new_order);
		$where[] = 'group_id = '.$this->_conn->quote($this->_group_id);
		$where[] = 'category_id = '.$this->_conn->quote($this->_category_id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $this->_autos_order),$where);
		
		//Save new Autos Order
		$whereI = array();
		$whereI[] = 'id = '.$this->_conn->quote($this->_id);
		$this->_conn->update(Zend_Registry::get('dbPrefix').'autos_page',array('autos_order' => $new_order),$whereI);			
		
	}
	
	public function getGroup_Id()
	{
		return $this->_group_id;
	}
	
	public function getCategory_id()
	{
		return $this->_category_id;
	}
	
	public function getHighOrder()
	{
		return $this->_high_order;
	}
}
?>