<?php

class Userinfo_Form_AddManager extends Zend_Form {

    protected $_editor;
    protected $_options;
    protected $_form_info;
    protected $_tagsAllowed;
    protected $_allowAttribs;

    public function __construct($options = null) {
        $translator = Zend_Registry::get('translator');
        $this->_options = $options;
        $config = (file_exists(APPLICATION_PATH . '/modules/Userinfo/forms/source/' . $translator->getLangFile() . '.RegistrationForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/Userinfo/forms/source/' . $translator->getLangFile() . '.RegistrationForm.ini', 'AddManager') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/Userinfo/forms/source/en_US.RegistrationForm.ini', 'AddManager');
     
        parent::__construct($config->AddManager);
    }

    public function init() {
        $this->createForm();
    }

    public function createForm() {
        $this->elementDecorator();
//        $this->loadUser();
    }
    
    
    //Element Decorator
    private function elementDecorator() {
        $this->setElementDecorators(array(
            'ViewHelper', 'FormElements',
        ));
    }

}
