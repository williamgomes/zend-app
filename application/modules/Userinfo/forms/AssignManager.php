<?php

class Userinfo_Form_AssignManager extends Zend_Form {

    protected $_editor;
    protected $_options;
    protected $_form_info;
    protected $_tagsAllowed;
    protected $_allowAttribs;

    public function __construct($options = null) {
        $translator = Zend_Registry::get('translator');
        $this->_options = $options;
        $config = (file_exists(APPLICATION_PATH . '/modules/Userinfo/forms/source/' . $translator->getLangFile() . '.RegistrationForm.ini')) ? new Zend_Config_Ini(APPLICATION_PATH . '/modules/Userinfo/forms/source/' . $translator->getLangFile() . '.RegistrationForm.ini', 'AssignManager') : new Zend_Config_Ini(APPLICATION_PATH . '/modules/Userinfo/forms/source/en_US.RegistrationForm.ini', 'AssignManager');
     
        parent::__construct($config->AssignManager);
    }

    public function init() {
        $this->createForm();
    }

    public function createForm() {
        
        $this->loadUser();
        $this->loadManager();
        $this->elementDecorator();
    }
    
    
    public function loadUser() {
        $AllUser = new Userinfo_Model_DbTable_Userinfo();
        $AllUser_options = $AllUser->getAllUser($isClient = true, $isVendor = true);
        $translator = Zend_Registry::get('translator');
        $this->Select_user->addMultiOption('', ': Select User :');
        $countUser = count($AllUser_options);
        $userID = 0;
        $userName = '';
        $userType = '';
        foreach($AllUser_options as $AllUser_arr) 
        {
            $userID = $AllUser_arr['id'];
            $userName = $AllUser_arr['full_name'];
            $userType = $AllUser_arr['user_type'];
            $this->Select_user->addMultiOption($userID, $userName . '');
        }
    }
    
    
    public function loadManager() {
        $AllManager = new Userinfo_Model_DbTable_Userinfo();
        $AllManager_options = $AllManager->getAllManager();
        $translator = Zend_Registry::get('translator');
        $this->Select_manager->addMultiOption('', ': Select Manager :');
        $managerID = 0;
        $managerName = '';
        foreach($AllManager_options as $AllManager_arr) 
        {
           $managerID = $AllManager_arr['user_id'];
           $managerName = $AllManager_arr['firstName'] . " " . $AllManager_arr['lastName'];
           $this->Select_manager->addMultiOption($managerID, $managerName);
        }
    }

    //Element Decorator
    private function elementDecorator() {
        $this->setElementDecorators(array(
            'ViewHelper', 'FormElements',
        ));
    }

}
