<?php
/**
* This is the DbTable class for the cities table.
*/
class Geo_Model_DbTable_City extends Eicra_Abstract_DbTable
{
    /** Table name */
    protected $_name    =  'cities';
	protected $_cols	=	null;
	
	//Get Datas
	public function getCityInfo($city_id) 
    {
        $row = $this->fetchRow('city_id = ' . $city_id);
        if (!$row) 
		{
			$options = null;
        }
		else
		{
			$options = $row->toArray(); 
			$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);
        }
		return   $options ; 
    }	
	
		
	//Get Datas
	public function getAllCityInfo($state_id = null)
    {
		if(empty($state_id))
		{
			$select = $this->select()
							->setIntegrityCheck(false)
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->order('ct.city_id ASC')
						   ->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'st.state_id = ct.state_id', array('state_name' => 'st.state_name'));			
		}
		else
		{
			$select = $this->select()
							->setIntegrityCheck(false)
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->where('ct.state_id = ?',$state_id)
						   ->order('ct.city_id ASC')
						   ->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'st.state_id = ct.state_id', array('state_name' => 'st.state_name'));	
		}
		$options = $this->fetchAll($select);        
        return $options;
    }
	
	//Get Datas
	public function getSelectOptions($state_id = null)
    {
		if($state_id)
		{
			$select = $this->select()
						   ->from($this->_name, array('city_id', 'city'))
						   ->where('state_id =?',$state_id)
						   ->order('city ASC');
		}
		else
		{
			$select = $this->select()
						   ->from($this->_name, array('city_id', 'city'))
						   ->order('city ASC');			
		} 
		$options = $this->getAdapter()->fetchPairs($select); 
		$options = is_array($options) ? array_map('stripslashes', $options) : stripslashes($options);       
        return $options;
    }
	
	
	public function searchCity($city, $limit = 10, $order = 'ASC', $sort = 'city')
    {
		if(empty($city))
		{
			$select = $this->select()
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->order('ct.'.$sort.' '.$order)
						   ->limit ($limit);			
		}
		else
		{
			$select = $this->select()
						   ->from(array('ct' => $this->_name), array('ct.city_id','ct.state_id', 'ct.city'))
						   ->where('ct.city LIKE "'.$city.'%"')
						   ->order('ct.'.$sort.' '.$order)
						   ->limit ($limit);	
		}
		$options = $this->fetchAll($select); 		     
        return $options;
    }
	
	public function getListInfo($approve = null, $search_params = null) 
    {		
		$auth = Zend_Auth::getInstance ();	
		$role_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->role_id : '' ;
		$user_id = ($auth->hasIdentity ()) ? $auth->getIdentity()->user_id : '' ;
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('ct' => $this->_name), array('ct.*'))
						->joinLeft(array('st' => Zend_Registry::get('dbPrefix').'states'), 'st.state_id = ct.state_id', array( 'st.*'))	
						->joinLeft(array('cu' => Zend_Registry::get('dbPrefix').'countries'), 'st.country_id = cu.id', array( 'cu.*'));
			
		if($search_params != null)
		{
			if($search_params['sort'])
			{				
				foreach($search_params['sort'] as $sort_key => $sort_value_arr)
				{
					if($sort_value_arr['field'] == 'state')
					{
						$select->order(' state_name '.$sort_value_arr['dir']);
					}
					else
					{
						if($sort_value_arr['dir'] == 'exp')
						{
							$select->order(new Zend_Db_Expr($sort_value_arr['field']));
						}
						else
						{
							$select->order($sort_value_arr['field'].' '.$sort_value_arr['dir']);
						}
					}
				}
			}
			else
			{
				$select->order("ct.city ASC"); 	
			}
			
			if($search_params['filter'] && $search_params['filter']['filters'])
			{ 
				$where = '';
				$where_arr = array();
				$i = 0;
				foreach($search_params['filter']['filters'] as $filter_key => $filter_obj)
				{
					if($filter_obj['field'])
					{						
						$where_arr[$i] = ' '.$this->getOperatorString($filter_obj);
						$i++;
					}
					else if($filter_obj['filters'])
					{
						$where_sub_arr = array();
						$sub = 0;
						foreach($filter_obj['filters'] as $sub_filter_key => $sub_filter_obj)
						{
							$where_sub_arr[$sub] = ' '.$this->getOperatorString($sub_filter_obj);
							$sub++;
						}
						$where_arr[$i] = ' ('.implode(strtoupper($filter_obj['logic']), $where_sub_arr).') ';
						$i++;
					}
				}
				$where = implode(strtoupper($search_params['filter']['logic']), $where_arr);				
				if(!empty($where))
				{
					$select->where($where);	
				}
			}
		}
		else
		{
			$select->order("ct.city ASC"); 
		}
				
		$options = $this->fetchAll($select);
		
        if(!$options)
		{
			$options = null;
		}
        return $options; 
	}	
	
	private function getOperatorString($operator_arr)
	{
		$table_prefix = ($this->isColumnExists($operator_arr['field'])) ? 'ct.' : '';
		$field_array = explode('_', $operator_arr['field']);
		if(in_array('date', $field_array))
		{
			$data_arr = preg_split('/[- :]/',$operator_arr['value']); //explode('GMT', $operator_arr['value']);
			if($data_arr[0])
			{
				$time = strtotime($data_arr[0].' '.$data_arr[1].' '.$data_arr[2].' '.$data_arr[3].' '.$data_arr[4].':'.$data_arr[5].':'.$data_arr[6]);			
				$operator_arr['value'] = date("Y-m-d H:i:s", $time);
			}
		}
		
		$operatorFirstPart = '';
		switch($operator_arr['field'])
		{
			case 'full_name' :
					$operatorFirstPart = " CONCAT(".$table_prefix."title, ' ', ".$table_prefix."firstName, ' ', ".$table_prefix."lastName) ";
				break;
			case 'state':
					$operatorFirstPart = " state_name ";
				break;
			default:
					$operatorFirstPart = $table_prefix.$operator_arr['field'];
				break;
		}
		$operatorString= '';
		switch($operator_arr['operator'])
		{
			case 'eq':
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;
			case 'neq':
					$operatorString = $operatorFirstPart.' != "'.$operator_arr['value'].'" ';
				break;
			case 'startswith':
					$operatorString = $operatorFirstPart.' LIKE "'.$operator_arr['value'].'%" ';
				break;
			case 'contains':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'doesnotcontain':
					$operatorString = $operatorFirstPart.' NOT LIKE "%'.$operator_arr['value'].'%" ';
				break;
			case 'endswith':
					$operatorString = $operatorFirstPart.' LIKE "%'.$operator_arr['value'].'" ';
				break;
			case 'gte':
					$operatorString = $operatorFirstPart.' >=  "'.$operator_arr['value'].'" ';
				break;
			case 'gt':
					$operatorString = $operatorFirstPart.' > "'.$operator_arr['value'].'" ';
				break;
			case 'lte':
					$operatorString = $operatorFirstPart.' <= "'.$operator_arr['value'].'" ';
				break;
			case 'lt':
					$operatorString = $operatorFirstPart.' < "'.$operator_arr['value'].'" ';
				break;
			case 'eqy':
					$operatorString = 'YEAR(m.cat_date)'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqm':
					$operatorString = 'date_format(m.cat_date, "%M")'.' = "'.$operator_arr['value'].'" ';
				break;
			case 'eqd':
					$operatorString = 'date_format(m.cat_date, "%d")'.' = "'.$operator_arr['value'].'" ';
				break;
			default:
					$operatorString = $operatorFirstPart.' = "'.$operator_arr['value'].'" ';
				break;				
		}
		return $operatorString;
	}
	
	private function isColumnExists($column)
	{
		$this->_cols = ($this->_cols == null) ? $this->info(Zend_Db_Table_Abstract::COLS) : $this->_cols;		
		return in_array($column, $this->_cols);
	}	
	
}

?>