<?php
class Geo_Model_CountriesMapper 
{
	protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Geo_Model_DbTable_Country');
        }
        return $this->_dbTable;
    }
	
	public function save(Geo_Model_Countries $obj)
		{
				 
			if ((null === ($id = $obj->getId())) || empty($id)) 
			{
				unset($data['id']);
				
				$data = array(					
					'value' 			=> 	$obj->getValue(),
					'code' 				=> 	$obj->getCode()										
				);
				try 
				{
					$last_id = $this->getDbTable()->insert($data);	
					$result = array('status' => 'ok' ,'id' => $last_id);	
				}
				catch (Exception $e)
				{
					$result = array('status' => 'err' ,'id' => $last_id, 'msg' => $e->getMessage());	
				}
							
			} 
			else 
			{				
				$data = array(					
					'value' 			=> 	$obj->getValue(),
					'code' 				=> 	$obj->getCode()						
				);				
				// Start the Update process
				try 
				{	
					$this->getDbTable()->update($data, array('id = ?' => $id));
					$result = array('status' => 'ok' ,'id' => $id);	
				} 
				catch (Exception $e) 
				{
					$result = array('status' => 'err' ,'id' => $id, 'msg' => $e->getMessage());
				}				
			}
			return $result;
		}
   	
	

}
?>