<?php
class Geo_Model_Countries
{
	protected $_id;
	protected $_value;
	protected $_code;
	
 
    public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}			
		}
		
		 public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth Method');
			}
			$this->$method($value);
		}
		
		
		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid Auth Method');
			}
			return $this->$method();
		}
	 
		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}
		
		public function saveCountries()
		{
			$mapper  = new Geo_Model_CountriesMapper();
			$return = $mapper->save($this);
			return $return;
		}
		
		public function setId($text)
		{
			$this->_id = $text;
			return $this;
		}
		
		public function setValue($text)
		{
			$this->_value = $text;
			return $this;
		}
		
		public function setCode($text)
		{
			$this->_code = $text;
			return $this;
		}
		
		
		public function getId()
		{         
			return $this->_id;
		}
		
		public function getValue()
		{         
			return $this->_value;
		}
		
		public function getCode()
		{         
			return $this->_code;
		}	
}
?>