<?php

class Geo_Form_StateForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Geo/forms/source/'.$translator->getLangFile().'.StateForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Geo/forms/source/'.$translator->getLangFile().'.StateForm.ini', 'state') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Geo/forms/source/en_US.StateForm.ini', 'state');
            parent::__construct($config->state );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
			 $this->loadCountries($this->country_id);
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}
		
		public function loadCountries($element)
		{
			$translator = Zend_Registry::get('translator');
			$element->setRegisterInArrayValidator(false);
			$global_conf = Zend_Registry::get('global_conf');
			$countries = new Eicra_Model_DbTable_Country();
        	$countries_options = $countries->getOptions();
			$selected = $global_conf['default_country'];
			$element->addMultiOption('',$translator->translator('common_select_country'));
			foreach($countries_options as $key=>$value)
			{
				if($selected == $key)
				{
					$element->addMultiOption($key,$value);
					$element->setValue($selected);
				}
				else
				{
					$element->addMultiOption($key,$value);
				}
			}						 
		}
		 	  
}