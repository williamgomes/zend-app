<?php

class Geo_Form_CountryForm  extends Zend_Form {

		protected $_editor;
		
		public function __construct($options = null) 
		{
			$translator = Zend_Registry::get('translator');
            $config = (file_exists( APPLICATION_PATH.'/modules/Geo/forms/source/'.$translator->getLangFile().'.CountryForm.ini')) ? new Zend_Config_Ini( APPLICATION_PATH.'/modules/Geo/forms/source/'.$translator->getLangFile().'.CountryForm.ini', 'country') : new Zend_Config_Ini( APPLICATION_PATH.'/modules/Geo/forms/source/en_US.CountryForm.ini', 'country');
            parent::__construct($config->country );
        }

        public function init()
        {
             $this->createForm();			 
        }

        public function createForm ()
		{ 		 	 
			 $this->elementDecorator();	
        }
		
		//Element Decorator
		private function elementDecorator()
		{
			$this->setElementDecorators(array(
					'ViewHelper','FormElements',										
					
				));			
		}		 	  
}