<?php
class Geo_View_Helper_GeoGroup extends Zend_View_Helper_Abstract 
{		
	public function getNumOfArea($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'cities'), array('COUNT(*) AS num_area'))
					   ->where('g.state_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_area = $row['num_area'];
		}
		return $num_area;
	}
	
	public function getNumOfStates($id)
	{
		//DB Connection
		$conn = Zend_Registry::get('msqli_connection');
		$conn->getConnection();
		
		$select = $conn->select()
					   ->from(array('g' => Zend_Registry::get('dbPrefix').'states'), array('COUNT(*) AS num_state'))
					   ->where('g.country_id = ?',$id);
		
		$rs = $select->query()->fetchAll();
		foreach($rs as $row)
		{
			$num_state = $row['num_state'];
		}
		return $num_state;
	}	
	
}