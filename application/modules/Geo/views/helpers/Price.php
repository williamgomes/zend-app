<?php
class Geo_View_Helper_Price extends Zend_View_Helper_Abstract 
{
	public function price($price, $global_conf = null) 
	{
		return Settings_Service_Price::price($price, $global_conf);
	}	
}