<?php
class Geo_FrontendController extends Zend_Controller_Action
{
	private $translator;
    private $_controllerCache;

    public function init ()
    {
        /* Initialize action controller here */
        $this->translator = Zend_Registry::get('translator');
        $this->view->assign('translator', $this->translator);
        $this->view->setEscape('stripslashes');
        // Initialize Cache
        $cache = new Eicra_View_Helper_Cache();
        $this->_controllerCache = $cache->getCache();
    }

    public function preDispatch ()
    {
        $template_obj = new Eicra_View_Helper_Template();
        $template_obj->setFrontendTemplate();
        $front_template = Zend_Registry::get('front_template');
        $this->_helper->layout->setLayoutPath( APPLICATION_PATH . '/layouts/scripts/' . $front_template['theme_folder']);
        $this->_helper->layout->setLayout($template_obj->getLayout(false));
        $this->view->assign('front_template', $front_template);
		
        /* Initialize action controller here */
        $getModule = $this->_request->getModuleName();
        $this->view->assign('getModule', $getModule);
        $getAction = $this->_request->getActionName();
        $this->view->assign('getAction', $getAction);
        $getController = $this->_request->getControllerName();
        $this->view->assign('getController', $getController);
		
        if ($this->_request->getParam('menu_id')) {
            $viewHelper = new Eicra_VHelper_ViewHelper($this->_request);
            $page_id_arr = $viewHelper->_getContentId();
            $this->_page_id = (! empty($page_id_arr[0])) ? $page_id_arr[0] : null;
        } else {
            $this->_page_id = null;
        }
    }
	
	public function countrylistAction()
	{
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body					
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'create' => $create, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
								
										
				$countries = new Geo_Model_DbTable_Country();	
				$country_datas =  $countries->getListInfo('1', $posted_data);
				$view_datas = array('data_result' => array(), 'total' => 0);
				if($country_datas)
				{						
					$key = 0;				
					foreach($country_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							$entry_arr['publish_status_country_name'] = str_replace('_', '-', $entry_arr['country_name']);
							$entry_arr['hasImg'] = file_exists('data/adminImages/flagsImage/'.$entry_arr['code'].'.gif');
							$entry_arr['country']	=	array('id' => $entry_arr['id'], 'value' => $entry_arr['value'], 'code' => $entry_arr['code'], 'hasImg' => $entry_arr['hasImg']);	
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}					
				}				
							
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'total' => 0, 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}	
	}
	
	public function searchnameAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');	
		
		if ($this->_request->isPost()) 
		{
			try
			{
				$posted_data	=	$this->_request->getPost();
				$this->view->assign('posted_data', $posted_data);
				
				$i=0;
				foreach($posted_data as $key=>$value)
				{
					$search_field['filter']['filters'][] = array('field' => $key, 'operator' => 'startswith', 'value' => $value);
					$search_field['filter']['logic'] = ($search_field['filter']['logic']) ? $search_field['filter']['logic'] : 'and';
					//$search_field['filter']['filters'][$i]	=	$key;
					$i++;
				}
				//$search_field[$i]	= 'active';
				$postData_arr['active'] = 1;			
					
				$cities = new Geo_Model_DbTable_City();	
				$city_datas =  $cities->getListInfo('1', $search_field);
				$view_datas = array('data_result' => array(), 'total' => 0);
				if($city_datas)
				{						
					$key = 0;				
					foreach($city_datas as $entry)
					{
							$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
							$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
							/*$entry_arr['publish_status_country_name'] = str_replace('_', '-', $entry_arr['country_name']);
							$entry_arr['hasImg'] = file_exists('data/adminImages/flagsImage/'.$entry_arr['code'].'.gif');
							$entry_arr['country']	=	array('id' => $entry_arr['id'], 'value' => $entry_arr['value'], 'code' => $entry_arr['code'], 'hasImg' => $entry_arr['hasImg']);	
							*/
							$view_datas['data_result'][$key]	=	$entry_arr;	
							$key++;					
					}					
				}
				
					$msg = $translator->translator("hotels_search_success");
					$json_arr = array('status' => 'ok','msg' => $msg,'search_data' => $view_datas['data_result'] );
			}
			catch(Exception $e)
			{
					$json_arr = array('status' => 'err','msg' =>  $e->getMessage(),'where' =>  $search_field);
			}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
		}
	}
}
?>