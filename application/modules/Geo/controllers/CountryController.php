<?php

class Geo_CountryController extends Zend_Controller_Action
{	
	private $geoCountryForm;
	private $_controllerCache;
	private $_auth_obj;
	private $translator;
	
    public function init()
    {		
        /* Initialize action controller here */		
		$this->geoCountryForm =  new Geo_Form_CountryForm ();
		$this->view->geoCountryForm =  $this->geoCountryForm;
		
		$auth = Zend_Auth::getInstance ();
		$this->_auth_obj = ($auth->hasIdentity ()) ? $auth->getIdentity() : '' ;	
		$this->view->assign('auth', $auth);	
		
		//Initialize Cache
		$cache = new Eicra_View_Helper_Cache();
		$this->_controllerCache = 	$cache->getCache();
    }
	
	public function preDispatch() 
	{	
		$this->_helper->layout->setLayout('layout');
		$this->_helper->layout->setLayoutPath(MODULE_PATH.'/Administrator/layouts/scripts');
		
		
		$this->translator = Zend_Registry::get('translator');
		$this->view->assign('translator', $this->translator);	
		$this->view->setEscape('stripslashes');
		
		$getModule = $this->_request->getModuleName();
		$this->view->assign('getModule', $getModule);
		$getAction = $this->_request->getActionName();
		$this->view->assign('getAction', $getAction);
		$getController = $this->_request->getControllerName();	
		$this->view->assign('getController', $getController);
		$url = $this->view->serverUrl().$this->view->baseUrl().'/Administrator/login';
		Eicra_Global_Variable::checkSession($this->_response,$url);	
		
		/*Check Module License*/
		$modules_license = new Administrator_Controller_Helper_ModuleLoader();
		$modules_license->getModulesLicenseMsg($getModule);
	}	
	
	//PROPERTY GROUP LIST FUNCTION
	
	public function listAction()
    {		
		// action body		
		$posted_data	=	$this->_request->getParams();
		$this->view->assign('posted_data', $posted_data);
		
		if ($this->_request->isPost()) 
		{		
			try
			{
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
				
				// action body
				$pageNumber 	= 	($this->_request->getPost('page')) ? $this->_request->getPost('page') : $this->_request->getParam('page');				
				$create			=	$this->_request->getParam('create');	
				$request_from	=	$this->_request->getParam('request_from');		
								
				$getViewPageNum = $this->_request->getParam('pageSize'); 	
				$posted_data['browser_url'] = $this->view->url(array('module' => $this->view->getModule, 'controller' => $this->view->getController, 'action' => $this->view->getAction, 'create' => $create, 'page' => ($pageNumber == '1' || empty($pageNumber)) ? null : $pageNumber), 'adminrout',    true);
				$viewPageNumSes = Eicra_Global_Variable::getSession()->viewPageNum;
				
				$viewPageNum	=	(!empty($getViewPageNum)) ? $getViewPageNum : $viewPageNumSes;
				$backup 		= 	Eicra_Global_Variable::getSession()->viewPageNum;		
				Eicra_Global_Variable::getSession()->viewPageNum =  $viewPageNum;
										
				$encode_params = Zend_Json_Encoder::encode($posted_data);
				$encode_auth_obj =	Zend_Json_Encoder::encode($this->_auth_obj);
				$uniq_id = md5(preg_replace('/[^a-zA-Z0-9_]/','_',$this->view->url().'_'.$encode_params.'_'.$encode_auth_obj));	
				if( ($view_datas = $this->_controllerCache->load($uniq_id)) === false ) 
				{						
					$countries = new Geo_Model_CountryListMapper();	
					$country_datas =  $countries->fetchAll($pageNumber, $approve, $posted_data);
					$view_datas = array('data_result' => array(), 'total' => 0);
					if($country_datas)
					{						
						$key = 0;				
						foreach($country_datas as $entry)
						{
								$entry_arr = (!is_array($entry)) ? $entry->toArray() : $entry;
								$entry_arr = is_array($entry_arr) ? array_map('stripslashes', $entry_arr) : stripslashes($entry_arr);
								$entry_arr['publish_status_country_name'] = str_replace('_', '-', $entry_arr['country_name']);
								$entry_arr['hasImg'] = file_exists('data/adminImages/flagsImage/'.$entry_arr['code'].'.gif');
								$entry_arr['country']	=	array('id' => $entry_arr['id'], 'value' => $entry_arr['value'], 'code' => $entry_arr['code'], 'hasImg' => $entry_arr['hasImg']);	
								$view_datas['data_result'][$key]	=	$entry_arr;	
								$key++;					
						}
						$view_datas['total']	=	$country_datas->getTotalItemCount();
					}
					$this->_controllerCache->save($view_datas , $uniq_id);
				}
				if($request_from == 'widget') {    Eicra_Global_Variable::getSession()->viewPageNum =  $backup;    }				
				$json_arr = array('status' => 'ok', 'data_result' => $view_datas['data_result'], 'total' => $view_datas['total'], 'posted_data' => $posted_data);
			}
			catch(Exception $e)
			{
				$json_arr = array('status' => 'err', 'data_result' => '', 'total' => 0, 'msg' => $e->getMessage());
			}
			
			//Convert To JSON ARRAY	
			$res_value =  Zend_Json_Encoder::encode($json_arr);				
			$this->_response->setBody($res_value);
		}	
	}
	
	
	//PROPERTY GROUP FUNCTIONS
	
	public function addAction()
	{
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;	
		
		if($this->view->allow())
		{	
			if ($this->_request->isPost()) 
			{	
				$posted_data	=	$this->_request->getParams();
				if($posted_data['models'])
				{	
					$countriesInfo = new Geo_Model_DbTable_Country();
					foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
					{		
						if($this->geoCountryForm->isValid($posted_data_value)) 
						{	
							$countries = new Geo_Model_Countries($this->geoCountryForm->getValues());				
							
							$save_result = $countries->saveCountries();						
							if($save_result['status'] == 'ok')
							{
								if($save_result['id'])
								{
									$posted_data['filter']['filters'][0] = array('field' => 'id', 'operator' => 'eq', 'value' => $save_result['id']);
								}
								$list_data = $countriesInfo->getListInfo(null, $posted_data);
								$posted_data['models'][$posted_data_key]['id'] = $save_result['id'];
								foreach($list_data as $row_data)
								{
									$posted_data['models'][$posted_data_key]['value'] = stripslashes($row_data['value']);
									$posted_data['models'][$posted_data_key]['country_name'] = stripslashes($row_data['country_name']);									
								}																
								$status = 'ok';								
							}
							else
							{
								$status = 'err';
								$msg = $translator->translator("geo_country_save_err");
								$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
								break;
							}	
										
						}
						else
						{
							$status = 'err';
							$validatorMsg = $this->geoCountryForm->getMessages();
							$vMsg = array();
							$i = 0;
							foreach($validatorMsg as $key => $errType)
							{					
								foreach($errType as $errkey => $value)
								{
									$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
									$i++;
								}
							}
							$json_arr = array('status' => 'errV','msg' => $vMsg);
							break;
						}				
					}	
					
					if($status == 'ok')
					{
						$msg = $translator->translator("geo_country_save_success");
						$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
					}
				}
				else
				{
					$msg = $translator->translator("geo_country_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			else
			{
				$msg = $translator->translator("geo_country_save_err");
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$Msg =  $translator->translator("geo_country_add_action_deny_desc");
			$json_arr = array('status' => 'errP','msg' => $Msg);
		}
		
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);
	}
	
	public function editAction()
	{	
		$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = $this->translator;
		
		if($this->view->allow())
		{
			if ($this->_request->isPost()) 
			{			
				$posted_data	=	$this->_request->getParams();			
				if($posted_data['models'])
				{						
						foreach($posted_data['models'] as $posted_data_key => $posted_data_value)
						{							
							if($this->geoCountryForm->isValid($posted_data_value)) 
							{
								$countries = new Geo_Model_Countries($this->geoCountryForm->getValues());	
								$countries->setId($posted_data_value['id']);
								$save_result = $countries->saveCountries();
								if($save_result['status'] == 'ok')
								{
									$status = 'ok';
								}
								else
								{
									$status = 'err';
									$msg = $translator->translator("geo_country_save_err");
									$json_arr = array('status' => 'err','msg' => $msg." ".$save_result['msg']);
								}
							}
							else
							{
								$validatorMsg = $this->geoCountryForm->getMessages();
								$vMsg = array();
								$i = 0;
								foreach($validatorMsg as $key => $errType)
								{					
									foreach($errType as $errkey => $value)
									{
										$vMsg[$i] = array('key' => $key, 'errKey' => $errkey, 'value' => $value);
										$i++;
									}
								}
								$status = 'err';
								$json_arr = array('status' => 'errV','msg' => $vMsg);
								break;
							}
						}
						
						if($status == 'ok')
						{
							$msg = $translator->translator("geo_country_save_success");
							$json_arr = array('status' => 'ok','msg' => $msg, 'data_result' => $posted_data['models']);				
						}
				}
				else
				{
					$msg = $translator->translator("geo_country_save_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}							
				
			}
			else
			{	
				$msg = $translator->translator("geo_country_save_err");
				$json_arr = array('status' => 'err','msg' => $msg);		
			}	
		}
		else
		{
			$Msg =  $translator->translator("geo_country_edit_action_deny_desc");
			$json_arr = array('status' => 'errP','msg' => $Msg);
		}
		$res_value = Zend_Json_Encoder::encode($json_arr);			
		$this->_response->setBody($res_value);			
	}	
	
	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			try
			{
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$id = $this->_request->getPost('id');
							
				//DB Connection
				$conn = Zend_Registry::get('msqli_connection');
				$conn->getConnection();
				
				$check_num_area = new Geo_View_Helper_GeoGroup();
						
				if($check_num_area->getNumOfStates($id) == '0')
				{
					// Remove from Group
					$where = array();
					$where[] = 'id = '.$conn->quote($id);
					try
					{
						$conn->delete(Zend_Registry::get('dbPrefix').'countries', $where);
						$msg = 	$translator->translator('geo_country_delete_success');			
						$json_arr = array('status' => 'ok','msg' => $msg);
					}
					catch (Exception $e) 
					{
						$msg = 	$translator->translator('geo_country_delete_err');			
						$json_arr = array('status' => 'err','msg' => $msg);
					}
				}
				else
				{
					$msg = 	$translator->translator('geo_country_for_page_delete_err');			
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			catch(Exception $e)
			{
				$msg = 	$e->getMessage();			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('geo_country_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
	
	public function deleteallAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$translator = Zend_Registry::get('translator');
		
		if ($this->_request->isPost()) 
		{	
			try
			{
				$this->_controllerCache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$id_st = $this->_request->getPost('id_st');
				if(!empty($id_st))
				{
					$id_arr = explode(',',$id_st);
							
					//DB Connection
					$conn = Zend_Registry::get('msqli_connection');
					$conn->getConnection();
						
					$check_num_area = new Geo_View_Helper_GeoGroup();
					$non_del_arr = array();
					$k = 0;
					foreach($id_arr as $id)
					{			
						if($check_num_area->getNumOfStates($id) == '0')
						{
							// Remove from Group
							$where = array();
							$where[] = 'id = '.$conn->quote($id);
							try
							{
								$conn->delete(Zend_Registry::get('dbPrefix').'countries', $where);
								$msg = 	$translator->translator('geo_country_delete_success');			
								$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
							catch (Exception $e) 
							{
								$non_del_arr[$k] = $id;
								$k++;
								$msg = 	$translator->translator('geo_country_delete_success');			
								$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
							}
						}
						else
						{
							$non_del_arr[$k] = $id;
							$k++;
							$msg = 	$translator->translator('geo_country_delete_success');			
							$json_arr = array('status' => 'ok', 'msg' => $msg, 'non_del_arr' => $non_del_arr);
						}
					}
				}
				else
				{
					$msg = $translator->translator("geo_selected_err");
					$json_arr = array('status' => 'err','msg' => $msg);
				}
			}
			catch(Exception $e)
			{
				$msg = 	$e->getMessage();			
				$json_arr = array('status' => 'err','msg' => $msg);
			}
		}
		else
		{
			$msg = 	$translator->translator('geo_country_delete_err');			
			$json_arr = array('status' => 'err','msg' => $msg);
		}
			//Convert To JSON ARRAY	
			$res_value = Zend_Json_Encoder::encode($json_arr);	
			$this->_response->setBody($res_value);
	}
		
}

