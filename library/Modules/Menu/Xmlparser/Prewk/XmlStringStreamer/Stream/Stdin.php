<?php 

//namespace Prewk\XmlStringStreamer\Stream;

//use Prewk\XmlStringStreamer\StreamInterface;

class Eicra_Menu_Xmlparser_Prewk_XmlStringStreamer_Stream_Stdin extends Eicra_Menu_Xmlparser_Prewk_XmlStringStreamer_Stream_File
{
    public function __construct($chunkSize = 1024, $chunkCallback = null)
    {
        parent::__construct(fopen("php://stdin", "r"), $chunkSize, $chunkCallback);
    }
}