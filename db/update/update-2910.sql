--	Comment
--	Previous Version = 2.9.6
--	Working Version = 2.9.10


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Hotels
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_page`
--

ALTER TABLE  `$prefix_hotels_page` ADD  `day_night_limit` INT NOT NULL DEFAULT  '0' COMMENT  '0 means infinity night' AFTER  `checkout_time`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_preferences`
--

INSERT INTO `$prefix_hotels_preferences` (`id`, `name`, `value`) VALUES
(14, 'hotels_price_search_limit_min', '0'),
(15, 'hotels_price_search_limit_max', '3000'),
(16, 'hotels_price_search_room_limit', '30'),
(17, 'hotels_price_search_adult_limit', '30'),
(18, 'hotels_price_search_child_limit', '10'),
(19, 'show_unavailable_room_type', 'NO');


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Tours
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_tours_calendar`
--

-- --------------------------------------------------------

DROP TABLE IF EXISTS `$prefix_tours_calendar`;
CREATE TABLE `$prefix_tours_calendar` (                                              
                     `id` int(11) NOT NULL AUTO_INCREMENT,                                         
                     `tours_id` int(11) NOT NULL,                                                  
                     `start_date` datetime NOT NULL,                                               
                     `end_date` datetime NOT NULL,                                                 
                     `available_status` int(11) NOT NULL,                                          
                     PRIMARY KEY (`id`)                                                            
                   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;
                   

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_tours_page`
--  

-- --------------------------------------------------------
                
ALTER TABLE  `$prefix_tours_page` 	ADD  `infant_price` DOUBLE NOT NULL DEFAULT  '00.00' AFTER  `tours_price` ,
									ADD  `price_type` ENUM(  'person',  'package' ) NOT NULL DEFAULT  'package' AFTER  `infant_price`;



-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Flight
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_flight_api`
--

-- --------------------------------------------------------

ALTER TABLE  `$prefix_flight_api` ADD  `locales` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL  AFTER  `file_size_max`;


ALTER TABLE  `$prefix_flight_api` ADD  `currency` VARCHAR( 5 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT  'USD' ;



-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Vacationrentals
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_vacationrentals_calendar`
--

-- --------------------------------------------------------

DROP TABLE IF EXISTS `$prefix_vacationrentals_calendar`;
CREATE TABLE `$prefix_vacationrentals_calendar` (                                              
                     `id` int(11) NOT NULL AUTO_INCREMENT,                                         
                     `vacationrentals_id` int(11) NOT NULL,                                                  
                     `start_date` datetime NOT NULL,                                               
                     `end_date` datetime NOT NULL,                                                 
                     `available_status` int(11) NOT NULL,                                          
                     PRIMARY KEY (`id`)                                                            
                   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;
                   

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_vacationrentals_page`
--

-- --------------------------------------------------------
                   
ALTER TABLE  `$prefix_vacationrentals_page` ADD  `vacationrentals_strickthrow_price` DOUBLE NOT NULL DEFAULT  '0' AFTER  `vacationrentals_price`;

ALTER TABLE  `$prefix_vacationrentals_page` CHANGE  `feature_bedroom`  `feature_bedroom` INT NOT NULL DEFAULT  '0';



-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Autos
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_spare_parts_saved`
--

-- --------------------------------------------------------

DROP TABLE IF EXISTS `$prefix_autos_spare_parts_saved`;
CREATE TABLE IF NOT EXISTS `$prefix_autos_spare_parts_saved` (                                                 
								  `id` int(11) NOT NULL AUTO_INCREMENT,                                         
								  `spare_parts_id` int(11) DEFAULT NULL,                                              
								  `user_id` int(11) DEFAULT NULL,                                               
								  PRIMARY KEY (`id`)                                                            
								) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;
								

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_page`
--

-- --------------------------------------------------------

ALTER TABLE  `$prefix_autos_page` ADD  `autos_strickthrow_price` DOUBLE NOT NULL DEFAULT  '0' AFTER  `autos_price`;


-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_preferences`
--

-- --------------------------------------------------------


insert into `$prefix_autos_preferences` values  ('','eCommerce_autos','YES'), ('','eCommerce_spare_parts','YES');
