--	Comment
--	Previous Version = 2.10.00
--	Working Version = 2.10.05


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Flight
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_flight_api_list`
--

DROP TABLE IF EXISTS `$prefix_flight_api_list`;
CREATE TABLE IF NOT EXISTS `$prefix_flight_api_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `file_path_airlines_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'data/frontImages/flight/airlines',
  `file_path_airlines_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'data/frontImages/flight/airlines',
  `file_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'jpg,png,gif',
  `file_size_max` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '900',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;
													

--
-- Dumping data for table `$prefix_flight_api_list`
--

INSERT INTO `$prefix_flight_api_list` (`id`, `logo`, `name`, `title`, `active`, `file_path_airlines_logo`, `file_path_airlines_pic`, `file_type`, `file_size_max`) VALUES
(1, 'wego-api.jpg', 'wego', 'WeGo Hotel API', '1', 'data/frontImages/flight/airlines', 'data/frontImages/flight/airlines', 'jpg,png,gif', '900');


-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_flight_api_settings`
--

DROP TABLE IF EXISTS `$prefix_flight_api_settings`;				
CREATE TABLE IF NOT EXISTS `$prefix_flight_api_settings` (                         
													  `id` int(11) NOT NULL AUTO_INCREMENT,
													  `api_id` int(11) DEFAULT NULL,
													  `setting` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
													  `value` text COLLATE utf8_unicode_ci,
													  PRIMARY KEY (`id`)                                          
													) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;		
																												