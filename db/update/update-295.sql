--	Comment
--	Previous Version = 2.9.3
--	Working Version = 2.9.5


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Members
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_membership_packages`
--

ALTER TABLE  `$prefix_membership_packages` ADD  `eCommerce` ENUM(  'YES',  'NO' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT  'NO' AFTER `gallery`;


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Invoice
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_invoice_table`
--

ALTER TABLE `$prefix_invoice_table` ADD `payment_user_id` INT NOT NULL DEFAULT '2' AFTER `user_id` ;


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Paymentgatway
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_payment_gateway_settings`
--

ALTER TABLE  `$prefix_payment_gateway_settings` ADD  `user_id` INT NOT NULL DEFAULT  '2' AFTER  `gateway_id`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_payment_gateway`
--

ALTER TABLE  `$prefix_payment_gateway` CHANGE  `payment_order`  `payment_order` INT NULL DEFAULT  '0';

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_payment_gateway_mapper`
--

DROP TABLE IF EXISTS `$prefix_payment_gateway_mapper`;
CREATE TABLE IF NOT EXISTS `$prefix_payment_gateway_mapper` (
  `id` int(99) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `active` enum('0','1') NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_payment_gateway_mapper`
--

DROP TABLE IF EXISTS `$prefix_currency_converter`;
CREATE TABLE IF NOT EXISTS `$prefix_currency_converter` (
  `id` int(99) NOT NULL AUTO_INCREMENT,
  `base_unit` double NOT NULL,
  `base_currency` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `target_unit` double NOT NULL,
  `target_currency` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `target_currency_code` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


INSERT INTO `$prefix_currency_converter` (`id`, `base_unit`, `base_currency`, `target_unit`, `target_currency`, `target_currency_code`) VALUES
(1, 1, 'en_US', 76.85, 'bn_BD', 'BDT - বাংলাদেশী টাকা'),
(2, 1, 'en_US', 0.94, 'de_CH', 'CHF - Schweizer Franken'),
(3, 1, 'en_US', 13.17, 'es_MX', 'MXN - peso mexicano'),
(4, 1, 'en_US', 6.08, 'nb_NO', 'NOK - norske kroner'),
(5, 1, 'en_US', 0.64, 'en_GB', 'GBP - British Pound Sterling'),
(6, 1, 'en_US', 1.25, 'en_NZ', 'NZD - New Zealand Dollar'),
(7, 1, 'en_US', 1.27, 'en_SG', 'SGD - Singapore Dollar'),
(8, 1, 'en_US', 32.24, 'th_TH', 'THB - บาทไทย'),
(9, 1, 'en_US', 2.16, 'gaa_GH', 'GHS'),
(10, 1, 'en_US', 1.48, 'bs_BA', 'BAM - Konvertibilna marka'),
(11, 1, 'en_US', 29.75, 'trv_TW', 'TWD - pila Taiwan'),
(12, 1, 'en_US', 0.76, 'el_CY', 'EUR - Ευρώ'),
(13, 1, 'en_US', 104.6, 'pa_PK', 'PKR - روپئیہ'),
(14, 1, 'en_US', 1, 'es_EC', 'USD - dólar estadounidense'),
(15, 1, 'en_US', 15.29, 'dv_MV', 'MVR'),
(16, 1, 'en_US', 1.09, 'en_AU', 'AUD - Australian Dollar'),
(17, 1, 'en_US', 7.76, 'en_HK', 'HKD - Hong Kong Dollar');


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: B2b
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_products`
--

ALTER TABLE `$prefix_b2b_products` ADD `price_currency_locale` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en_US' AFTER `product_images` ;


-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_selling_leads`
--

ALTER TABLE `$prefix_b2b_selling_leads` ADD `price_currency_locale` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en_US' AFTER `qty_per_unit` ;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_buying_leads`
--

ALTER TABLE `$prefix_b2b_buying_leads` ADD `price_currency_locale` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en_US' AFTER `qty_per_unit` ;



-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Hotel
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_preferences`
--

DROP TABLE IF EXISTS `$prefix_hotels_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_hotels_preferences` ( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL, `value` text COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

--
-- Dumping data for table `$prefix_hotels_preferences`
--

INSERT INTO `$prefix_hotels_preferences` (`id`, `name`, `value`) VALUES
(1, 'hotels_featured_on_home', '4'),
(2, 'hotels_featured_home_sortby', 'hotels_date-DESC'),
(3, 'popular_hotels_on_home', '20'),
(4, 'popular_hotels_home_sortby', 'total_votes-DESC'),
(5, 'latest_hotels_on_home', '6'),
(6, 'latest_hotels_home_sortby', 'hotels_date-DESC'),
(7, 'eCommerce', 'YES'),
(8, 'hotels_list_by_group_on_other_page', '15'),
(9, 'hotels_list_by_group_other_page_sortby', 'hotels_date-DESC'),
(10, 'hotels_list_by_category_on_other_page', '15'),
(11, 'hotels_list_by_category_other_page_sortby', 'hotels_name-ASC'),
(12, 'hotels_list_by_business_type_on_other_page', '15'),
(13, 'hotels_list_by_business_type_other_page_sortby', 'hotels_name-ASC');

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_page`
--

ALTER TABLE  `$prefix_hotels_page` ADD  `cancellation_policy` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER  `hotels_policy_surroundings`;

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Vacationrentals
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_vacationrentals_preferences`
--

DROP TABLE IF EXISTS `$prefix_vacationrentals_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_vacationrentals_preferences` (                                          
												 `id` int(11) NOT NULL AUTO_INCREMENT,                                         
												 `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL,                         
												 `value` text COLLATE utf8_unicode_ci NOT NULL,                                
												 PRIMARY KEY (`id`)                                                            
											   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_vacationrentals_preferences`
--

-- --------------------------------------------------------

INSERT INTO `$prefix_vacationrentals_preferences` (`id`, `name`, `value`) VALUES
(1, 'vacationrentals_featured_on_home', '4'),
(2, 'vacationrentals_featured_home_sortby', 'vacationrentals_date-DESC'),
(3, 'popular_vacationrentals_on_home', '20'),
(4, 'popular_vacationrentals_home_sortby', 'total_votes-DESC'),
(5, 'latest_vacationrentals_on_home', '6'),
(6, 'latest_vacationrentals_home_sortby', 'vacationrentals_date-DESC'),
(7, 'eCommerce', 'YES'),
(8, 'vacationrentals_list_by_group_on_other_page', '15'),
(9, 'vacationrentals_list_by_group_other_page_sortby', 'vacationrentals_date-DESC'),
(10, 'vacationrentals_list_by_category_on_other_page', '15'),
(11, 'vacationrentals_list_by_category_other_page_sortby', 'vacationrentals_name-ASC'),
(12, 'vacationrentals_list_by_business_type_on_other_page', '15'),
(13, 'vacationrentals_list_by_business_type_other_page_sortby', 'vacationrentals_name-ASC');

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Tours
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_tours_preferences`
--

DROP TABLE IF EXISTS `$prefix_tours_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_tours_preferences` (                                 
                                  `id` int(11) NOT NULL AUTO_INCREMENT,                                         
                                  `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL,                         
                                  `value` text COLLATE utf8_unicode_ci NOT NULL,                                
                                  PRIMARY KEY (`id`)                                                            
                                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_tours_preferences`
--

-- --------------------------------------------------------                               
                                
insert into `$prefix_tours_preferences` values 
(1,'tours_featured_on_home','4'),
(2,'tours_featured_home_sortby','tours_date-DESC'),
(3,'popular_tours_on_home','20'),
(4,'popular_tours_home_sortby','total_votes-DESC'),
(5,'latest_tours_on_home','6'),
(6,'latest_tours_home_sortby','tours_date-DESC'),
(7,'eCommerce','YES'),
(8,'tours_list_by_group_on_other_page','15'),
(9,'tours_list_by_group_other_page_sortby','tours_date-DESC'),
(10,'tours_list_by_category_on_other_page','15'),
(11,'tours_list_by_category_other_page_sortby','tours_name-ASC'),
(12,'tours_list_by_business_type_on_other_page','15'),
(13,'tours_list_by_business_type_other_page_sortby','tours_name-ASC');

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Property
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_property_preferences`
--

DROP TABLE IF EXISTS `$prefix_property_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_property_preferences` (                                 
                                  `id` int(11) NOT NULL AUTO_INCREMENT,                                         
                                  `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL,                         
                                  `value` text COLLATE utf8_unicode_ci NOT NULL,                                
                                  PRIMARY KEY (`id`)                                                            
                                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_property_preferences`
--

-- --------------------------------------------------------  

INSERT INTO `$prefix_property_preferences` (`id`, `name`, `value`) VALUES
(1,'autos_featured_on_home','4'),
(2,'autos_featured_home_sortby','autos_date-DESC'),
(3,'popular_autos_on_home','20'),
(4,'popular_autos_home_sortby','total_votes-DESC'),
(5,'latest_autos_on_home','6'),
(6,'latest_autos_home_sortby','autos_date-DESC'),
(7,'eCommerce','YES'),
(8,'autos_list_by_group_on_other_page','15'),
(9,'autos_list_by_group_other_page_sortby','autos_date-DESC'),
(10,'autos_list_by_category_on_other_page','15'),
(11,'autos_list_by_category_other_page_sortby','autos_model_name-ASC'),
(12,'autos_list_by_business_type_on_other_page','15'),
(13,'autos_list_by_business_type_other_page_sortby','autos_model_name-ASC'),(14,'autos_spare_parts_list_on_other_page','15'),
(15,'autos_spare_parts_list_other_page_sortby','entry_date-DESC'),
(16,'spare_parts_img_width','200'),
(17,'spare_parts_img_height','113'),
(18,'spare_parts_img_resize_func','resizeToHeight'),
(19,'spare_parts_review_id','10');

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Autos
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_preferences`
--

DROP TABLE IF EXISTS `$prefix_autos_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_autos_preferences` (                                 
                                  `id` int(11) NOT NULL AUTO_INCREMENT,                                         
                                  `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL,                         
                                  `value` text COLLATE utf8_unicode_ci NOT NULL,                                
                                  PRIMARY KEY (`id`)                                                            
                                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_preferences`
--

-- --------------------------------------------------------  

INSERT INTO `$prefix_autos_preferences` (`id`, `name`, `value`) VALUES
(1, 'autos_featured_on_home', '4'),
(2, 'autos_featured_home_sortby', 'autos_date-DESC'),
(3, 'popular_autos_on_home', '20'),
(4, 'popular_autos_home_sortby', 'total_votes-DESC'),
(5, 'latest_autos_on_home', '6'),
(6, 'latest_autos_home_sortby', 'autos_date-DESC'),
(7, 'eCommerce', 'YES'),
(8, 'autos_list_by_group_on_other_page', '15'),
(9, 'autos_list_by_group_other_page_sortby', 'autos_date-DESC'),
(10, 'autos_list_by_category_on_other_page', '15'),
(11, 'autos_list_by_category_other_page_sortby', 'autos_model_name-ASC'),
(12, 'autos_list_by_business_type_on_other_page', '15'),
(13, 'autos_list_by_business_type_other_page_sortby', 'autos_model_name-ASC');

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_group`
--

-- --------------------------------------------------------  

ALTER TABLE  `$prefix_autos_group` ADD  `file_path_spare_parts_image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'data/frontImages/autos/spare_parts_image' AFTER  `file_path_feature_interior_plan_image`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_autos_spare_parts`
--

-- -------------------------------------------------------- 

DROP TABLE IF EXISTS `$prefix_autos_spare_parts`;
CREATE TABLE IF NOT EXISTS `$prefix_autos_spare_parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spare_parts_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_manufacturer_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_description` text COLLATE utf8_unicode_ci,
  `spare_parts_item_condition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_base_price` double NOT NULL,
  `spare_parts_promotion_price` double NOT NULL,
  `spare_parts_image_primary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_image` text CHARACTER SET utf32 COLLATE utf32_unicode_ci,
  `spare_parts_delivery` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_returns_refund` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_order_qty_min` int(11) NOT NULL,
  `spare_parts_origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_mechine_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_brand_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_material` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_protection_guarantee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autos_id` text COLLATE utf8_unicode_ci,
  `autos_agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts_specifications` longtext COLLATE utf8_unicode_ci,
  `spare_parts_terms_condition` longtext COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `featured` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `entry_by` int(11) NOT NULL DEFAULT '1',
  `entry_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `$prefix_autos_spare_parts`
--

INSERT INTO `$prefix_autos_spare_parts` (`id`, `spare_parts_name`, `spare_parts_title`, `spare_parts_number`, `spare_parts_manufacturer_number`, `spare_parts_description`, `spare_parts_item_condition`, `spare_parts_base_price`, `spare_parts_promotion_price`, `spare_parts_image_primary`, `spare_parts_image`, `spare_parts_delivery`, `spare_parts_payment_method`, `spare_parts_returns_refund`, `spare_parts_order_qty_min`, `spare_parts_origin`, `spare_parts_mechine_type`, `spare_parts_brand_name`, `spare_parts_material`, `spare_parts_type`, `spare_parts_protection_guarantee`, `autos_id`, `autos_agent`, `spare_parts_specifications`, `spare_parts_terms_condition`, `meta_title`, `meta_keywords`, `meta_desc`, `featured`, `active`, `entry_by`, `entry_date`) VALUES
(1, 'WHEEL BEARING, SOLD INDIVIDUALLY', 'WHEEL-BEARING', 'SP000001', 'M000001', ' 1.57 in. bore; 2.84 in. outer diameter; 1.46 in. width; A high quality, OE replacement wheel bearing; Backed by 1-year, unlimited-mileage warranty.\r\nLocation:   Front\r\nProduct Fit:   Direct fit\r\nQuantity Sold:   Sold individually\r\nAnticipated Ship Out Time:   1-2 business days\r\nNotes:   1.57 in. bore; 2.84 in. outer diameter; 1.46 in. width\r\nWarranty:   1-year, unlimited-mileage warranty', 'Refurbished', 38.75, 30.75, '1384539961_Injection-Molding-Part.jpg', '1384539961_Injection-Molding-Part.jpg,1384539962_repv288401_4.jpg', 'FOB', 'Credit Card or Cash', '30 Day Money back', 2, 'Japan', 'Walton', 'BMW', '1.57 in. bore; 2.84 in. outer diameter; 1.46 in. width; A high quality, OE', 'Good', '1-year, unlimited-mileage warranty', NULL, '1', '1.57 in. bore; 2.84 in. outer diameter; 1.46 in. width', 'You Terms and Conditions', '', '', '', '1', '1', 1, '2013-11-15 06:23:48'),
(2, 'SONY IN-CAR X-PLOD 4 CHANNEL AMPLIFIER - 600W', 'SONY-In-car-X-Plod-4-Channel-Amplifier-600W', 'SP000002', 'M000002', '4 Channel GTX Series Amplifier. Maximum Output Power: 110W x 4 (at 4 ohms), 150W x 2 (at 2 ohms) + 300W x 1 (BTL, at 4 ohms), 300W x 2 (BTL, at 4 ohms) -Rated Output Power: 60W x 4 (at 4 ohms), 70W x 4 (at 2 ohms) -Channel Configuration: 4 Ch Stereo Power Amplifier -Low Pass Filter: 80 Hz, 18 dB/oct -Frequency Response: 5-50kHz -Pulse Power Supply', 'New', 169, 150, '1384540444_ME14118_1.jpg', '1384540196_Injection-Molding-Part.jpg,1384540444_ME14118_1.jpg', '01 Week', 'Cash or Cheque', 'No Refund', 1, 'USA', 'Audio Amplifier', 'SONY', 'ME14118', 'Audio Amplifier', '1 YEar', NULL, '13', 'SONY In-car X-Plod 4 Channel Amplifier - 600W', 'Your Terms and Conditions.\r\n', '', '', '', '0', '1', 1, '2013-11-15 06:30:15'),
(3, 'USAUDIO MONO 1X300W RMS 800W MAX AMP', 'USAUDIO-Mono-1X300W-Rms-800W-Max-Amp', 'SP000003', 'M000003', 'SPECIFICATION MAX Power: 800 Watts RMS power @ 4Î©,THD 1% : 300 W x 1 @ full channel driven RMS Power @ 2Î©: 400 W X 1 @ full channel driven MAX Power @ 4Î© S/N Ratio: 90dB Variable Input Level Control Input Sensitivity : 250mV 9.0V RCA Line and speaker input, Speaker output Three way protection: thermal, short circuit and overload Soft delayed power on Two mode speaker connection stable X-over switch: HPF/FULL / LPF Variable Low pass crossover: 50Hz 300Hz Variable high pass crossover: 50Hz 300Hz Frequency response; 20Hz 300Hz Bass EQ switched: 0dB - 12dB Fuse :30A X 2 Dimension:312mm( L) X 245 mm ( W ) X 46mm ( H)', 'Used', 200, 200, '1384540514_ME10285_1.jpg', '1384540514_ME10285_1.jpg,1384540444_AU00873_1.jpg', 'FOB', 'Credit Card / TT / WIRE', '30 Days', 50, 'Japan', 'Audio Amplifier', 'USAUDIO', 'ME10285', 'Audio Amplifier', '02 Years', '17,11,19', '1', 'SPECIFICATION MAX Power: 800 Watts RMS power @ 4Î©,THD 1% : 300 W x 1 @ full channel driven RMS Power @ 2Î©: 400 W X 1 @ full channel driven MAX Power @ 4Î© S/N Ratio: 90dB Variable Input Level Control Input Sensitivity : 250mV 9.0V RCA Line and speaker input, Speaker output Three way protection: thermal, short circuit and overload Soft delayed power on Two mode speaker connection stable X-over switch: HPF/FULL / LPF Variable Low pass crossover: 50Hz 300Hz Variable high pass crossover: 50Hz 300Hz Frequency response; 20Hz 300Hz Bass EQ switched: 0dB - 12dB Fuse :30A X 2 Dimension:312mm( L) X 245 mm ( W ) X 46mm ( H)', 'SPECIFICATION MAX Power: 800 Watts RMS power @ 4Î©,THD 1% : 300 W x 1 @ full channel driven RMS Power @ 2Î©: 400 W X 1 @ full channel driven MAX Power @ 4Î© S/N Ratio: 90dB Variable Input Level Control Input Sensitivity : 250mV 9.0V RCA Line and speaker input, Speaker output Three way protection: thermal, short circuit and overload Soft delayed power on Two mode speaker connection stable X-over switch: HPF/FULL / LPF Variable Low pass crossover: 50Hz 300Hz Variable high pass crossover: 50Hz 300Hz Frequency response; 20Hz 300Hz Bass EQ switched: 0dB - 12dB Fuse :30A X 2 Dimension:312mm( L) X 245 mm ( W ) X 46mm ( H)', '', '', '', '1', '1', 1, '2013-11-15 06:35:38'),
(4, 'T462115 Door Handle - Primered, Plastic, OE comparable, Exterior, Direct fit', 'Door-Handle', 'SP000004', 'M000004', 'This OE comparable door handle replaces your old or damaged factory unit; covered by 1-year, unlimited-mileage warranty.', 'New', 26.88, 26.88, '1384540976_t_1.jpg', '1384540975_ttt462115_3.jpg,1384540974_t4tt5.jpg,1384540976_t_1.jpg', 'Ships out in 1-2 business days', 'TT / Bank Wire', 'Replacement brand parts are the most affordable solution for your replacement needs!', 50, 'USA', 'APW Bestseller', 'SONY', 'Plastic', 'Exterior', '1-year, unlimited-mileage warranty', '13,11,19', '1', 'Front, Driver Side, Exterior\r\nColor/Finish:Primered\r\nProduct Fit:Direct fit\r\nQuantity Sold:Sold individually\r\nDoor Lock/Key Hole Provision:With keyhole\r\nStyle:OE comparable\r\nAnticipated Ship Out Time:1-2 business days\r\nReplaces OE Number:6922033041C0, 6922033040, 6922033041, 69220AA010\r\nReplaces Partslink Number:TO1310114\r\n', 'Your Terms and Conditions', '', '', '', '0', '1', 1, '2013-11-15 06:43:08'),
(5, 'Streetwires Side To Top Mount Converter, 1 Pair', 'Side-To-Top-Mount-Converter', 'SP000005', 'M000005', 'Streetwires Side To Top Mount Converter, 1 Pair', 'New', 15, 12, '1384541257_ProductImage_764_073018_FS.jpg', '1384541257_ProductImage_764_073018_FS.jpg', 'FREE SHIPPING!', 'Credit Card', '30 Days', 5, 'UK', 'ABC', 'BMW', 'XYZ', 'my car', '5 YEAR WARRANTY! 2 year full replacement + 3 prorated', NULL, '59', 'The Monitor 60i is a bookshelf speaker with undeniable performance in home theater and other custom residential applications. Through intelligent design and its use of high grade materials, the Monitor 60i produces superior sound quality from a surprisingly small source. It features two 6.5" polypropylene woofers with rubber surrounds and a 19mm PEI dome tweeter in a D''appolito array, a unique design for a high output bookshelf.', 'The Monitor 60i is a bookshelf speaker with undeniable performance in home theater and other custom residential applications. Through intelligent design and its use of high grade materials, the Monitor 60i produces superior sound quality from a surprisingly small source. It features two 6.5" polypropylene woofers with rubber surrounds and a 19mm PEI dome tweeter in a D''appolito array, a unique design for a high output bookshelf.', '', '', '', '1', '1', 1, '2013-11-15 06:47:58'),
(6, 'Replacement Alternator', 'Alternator', 'SP000006', 'M000006', 'This brand new Replacement OE Comparable Alternator is constructed from top quality components and designed to withstand extreme heat and high electrical demands. Best of all, no core charges - eliminates the hassle of returning your old part for core refund!', 'Used', 133, 105, '1384541625_repf330101_is.jpg', '1384541624_repf330101_2.jpg,1384541623_repf330101_3.jpg,1384541625_repf330101_is.jpg', '1-2 business days', 'T/T  or Bank Wire', '30 Day', 5, 'Italy', 'Factory finish', 'REPF330103', 'REPM330105', 'Good', 'Replacement brand items are backed by 1-year, unlimited-mileage warranty.', NULL, '6', 'Condition: New\r\nProduct Fit: Direct fit\r\nQuantity Sold: Sold individually\r\nProduct Color Finish: Factory finish\r\nAmperage Output: 110\r\nNumberof Grooves: 6\r\nAnticipated Ship Out Time: 1-2 business days\r\nPart Number:REPM330105\r\nAvailability:In Stock', 'Replacement brand parts are the most affordable solution for your replacement needs! Replacement specializes in a wide range of OE comparable auto parts including body parts, lighting, electrical, brakes, suspension, intake, exhaust, cooling and heating parts. All Replacement brand items are backed by 1-year, unlimited-mileage warranty.', '', '', '', '0', '1', 1, '2013-11-15 06:53:53'),
(7, 'Cordura Seat Cover - Black, Ballistic, Solid, Direct fit', 'Ballistic-Cordura-Seat-Cover', 'SP000007', 'M000007', 'COVERKING â€“ BALLISTIC CORDURA SEAT COVERS \r\nIf you need to get a dirty job done in the great outdoors, Coverkingâ€™s Ballistic Cordura seat covers are what you need. Perfect fit, virtually indestructible canvas-like fabric and simple style are yours, combined with the utilitarian look and feel of cordura. Coverking, started in 1986, is based in southern California and has become regarded as a leading manufacturer and distributor of automotive aftermarket accessories, particularly with regard to vehicle covers.\r\n\r\n10 times stronger than cotton canvas and twice as tough as nylon\r\nCome in front/middle/back row configurations and for headrests/armrests and consoles\r\nDirect fit\r\nInner lining is waterproof\r\nMade for outdoor adventurers\r\nFit snugly like a factory seat cover\r\nResistant to sun and water damage, mildew and stains\r\nFit right over factory seats\r\nEasy maintenance - spot-clean and air-dry\r\nBacked by a 3-year warranty\r\nTools are not required for installation\r\n\r\nWith Remov. Headrest; With Armrest; For Vehicles With Floor-Mounted Transmission\r\n', 'Refurbished', 159, 148, '1384542002_csmrc98401b_is.jpg', '1384542003_c37csc1e1tt7008_2.jpg,1384542002_csmrc98401b_is.jpg,1384542001_c37csc1e1tt7008_5.jpg,1384542004_c37csc1e1tt7008_4.jpg', '3-5 business days', 'Cash, Cheque, Credit Card, T*T or L/C', '15 Days', 1, 'Bangladesh', 'Ballistic', 'Coverking', 'Coverking Ballistic/Cordura', '60/40 Split', 'APW warrants this automotive part to be free from defects in material or workmanship for 12 months. You will be able to select this extended warranty plan during checkout', '13,17,2,5,1', '8', 'Coverking was founded in 1986. Since then, Coverking purchased various automotive companies, some of which dated back to 1974. Quality, Technology, and Service lead to our stellar Customer Satisfaction, are the basis of all product lines. Coverking was founded in Southern California by an Industrial and Electrical Engineer who worked in Southern California''s Aerospace Defense industry. The experience and knowledge acquired over decades of work on the most sophisticated defense weapons systems was applied to the relatively backward industry of cut-and-sew custom automotive accessories. Coverking continues to expand into markets where quality and efficiency are rewarded, while maintaining it''s main manufacturing base in California, and it''s commitment to our employees, suppliers and customers.', 'Coverking was founded in 1986. Since then, Coverking purchased various automotive companies, some of which dated back to 1974. Quality, Technology, and Service lead to our stellar Customer Satisfaction, are the basis of all product lines. Coverking was founded in Southern California by an Industrial and Electrical Engineer who worked in Southern California''s Aerospace Defense industry. The experience and knowledge acquired over decades of work on the most sophisticated defense weapons systems was applied to the relatively backward industry of cut-and-sew custom automotive accessories. Coverking continues to expand into markets where quality and efficiency are rewarded, while maintaining it''s main manufacturing base in California, and it''s commitment to our employees, suppliers and customers.', '', '', '', '1', '1', 1, '2013-11-15 07:00:22'),
(8, 'Garage Pro C13462-1G Fold & Tumble Seat - Direct fit', 'Fold-Tumble-Seat', 'SP000008', 'M000008', 'GARAGE PRO FOLD AND TUMBLE BENCH SEAT, GRAY -- CJ style; Features a rugged construction for extreme durability; A high quality, direct fit fold and tumble bench seat; With 1-year Garage Pro limited warranty.', 'New', 159, 100, '1384542314_gapc134621g_4.jpg', '1384542313_gapc13402s_is.jpg,1384542312_gapc98604yellow_is.jpg,1384542314_gapc134621g_4.jpg', '1-2 business days', 'L/C', '30 Days', 15, 'Japan', 'Vinyl', 'C13462-1G', 'Steel', 'Garage Pro Fold & Tumble', 'APW warrants this automotive part to be free from defects in material or workmanship for 12 months. You will be able to select this extended warranty plan during checkout.', '5,11,19,3,14', '13', 'Series:Garage Pro Fold & Tumble\r\nLocation:Rear\r\nProduct Fit:Direct fit\r\nQuantity Sold:Sold individually\r\nAnticipated Ship Out Time:1-2 business days\r\nConfiguration:Fold and tumble\r\nFrame Material:Steel\r\nNotes:CJ style\r\nReclining:no\r\nSeat Cover Color:Gray\r\nSeat Cover Material:Vinyl\r\nWarranty:1-year Garage Pro limited warranty\r\nWith Arm Rest:no', 'To the extent allowed by law, this warranty sets out your exclusive remedies with respect to the product covered by it, whether for negligence or otherwise. Neither APW nor its affiliated companies will be liable for incidental or consequential damages, such as telephone calls, loss of time, inconvenience, commercial loss, personal injury damages, etc. THIS WARRANTY IS IN LIEU OF ALL OTHER EXPRESS WARRANTIES. ANY WARRANTY IMPLIED BY LAW, WHETHER OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR OTHERWISE, IS LIMITED TO THE PERIOD THAT THIS EXPRESS WARRANTY IS EFFECTIVE. No attempt to alter, modify, or amend this warranty shall be effective unless authorized in writing by an officer of APW.', '', '', '', '0', '1', 1, '2013-11-15 07:05:29');
