--	Comment
--	Previous Version = 2.9.5
--	Working Version = 2.9.6


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: B2b
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_group`
--

ALTER TABLE  `$prefix_b2b_group` ADD  `file_path_brochures` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'data/frontImages/b2b/brochures' AFTER  `file_path_profile_image`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_products`
--

ALTER TABLE  `$prefix_b2b_products` CHANGE  `brochures`  `brochures` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_products`
--

ALTER TABLE  `$prefix_b2b_products` ADD  `brochures_primary` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER  `brochures`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_products`
--

ALTER TABLE  `$prefix_b2b_products` CHANGE  `video`  `video` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

