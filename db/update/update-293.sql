--	Comment
--	Previous Version = 2.9.2
--	Working Version = 2.9.3


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Members
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_user_profile`
--

ALTER TABLE  `$prefix_user_profile` ADD  `package_id` INT NOT NULL DEFAULT  '0' AFTER  `role_id`;


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Paymentgateway
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_payment_gateway`

ALTER TABLE `$prefix_payment_gateway` ADD `method` ENUM( '1', '2', '3', '' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' AFTER `active` ;


