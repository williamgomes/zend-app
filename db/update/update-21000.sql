--	Comment
--	Previous Version = 2.9.10
--	Working Version = 2.10.00


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Hotels
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_room_type`
--

ALTER TABLE  `$prefix_hotels_room_type` ADD  `price_plan_id` INT NOT NULL DEFAULT  '0' AFTER  `room_type`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_price_plan`
--

DROP TABLE IF EXISTS `$prefix_hotels_price_plan`;
CREATE TABLE IF NOT EXISTS `$prefix_hotels_price_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minimum_stay` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `minimum_stay_night` int(11) NOT NULL DEFAULT '0',
  `price_per_night` double NOT NULL DEFAULT '0',
  `price_after_discount` double NOT NULL DEFAULT '0',
  `room_tax` double NOT NULL DEFAULT '0',
  `room_tax_type` enum('1','2') COLLATE utf8_unicode_ci DEFAULT '1',
  `seasonal_price_per_night` double NOT NULL DEFAULT '0',
  `season_start_date` date NOT NULL,
  `season_end_date` date NOT NULL,
  `stop_temporary` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `entry_by` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `$prefix_hotels_price_plan`
--

INSERT INTO `$prefix_hotels_price_plan` (`id`, `plan_name`, `minimum_stay`, `minimum_stay_night`, `price_per_night`, `price_after_discount`, `room_tax`, `room_tax_type`, `seasonal_price_per_night`, `season_start_date`, `season_end_date`, `stop_temporary`, `entry_by`) VALUES
(1, 'Winter Season Price', '1', 0, 645, 123, 0, '1', 99, '2014-12-01', '2014-12-31', '0', '1'),
(2, 'Price For Autumn', '1', 0, 934, 143, 0, '1', 96, '2014-10-15', '2014-11-20', '0', '1'),
(3, 'Seasonal Price for Summer', '2', 10, 834, 142, 0.05, '2', 100, '2014-04-14', '2014-04-30', '0', '1'),
(4, 'Spring Price', '2', 0, 1465, 353, 0, '1', 115, '2015-02-14', '2015-03-15', '0', '1'),
(5, 'Regular Price Plan', '1', 0, 1500, 0, 0.06, '2', 0, '0000-00-00', '0000-00-00', '0', '1'),
(6, 'Best Discount Price Plan', '2', 30, 1500, 88, 1.5, '1', 0, '0000-00-00', '0000-00-00', '0', '1'),
(7, 'Golden Discount Price Plan', '2', 30, 1500, 85, 1, '1', 0, '0000-00-00', '0000-00-00', '0', '1'),
(8, 'Premium Discount Price Plan', '2', 40, 1500, 80, 0, '2', 0, '0000-00-00', '0000-00-00', '1', '1'),
(9, 'No Discount Price Plan', '1', 0, 699, 0, 0.08, '2', 0, '0000-00-00', '0000-00-00', '0', '1'),
(10, 'Carnival Price Plan', '2', 10, 900, 70, 0.1, '2', 69, '2014-07-01', '2014-07-10', '0', '1'),
(11, '31st Price Plan', '2', 3, 222, 22, 0.9, '2', 20, '2014-12-30', '2015-01-02', '0', '1'),
(12, 'Valentine Price Plan', '2', 14, 1500, 100, 0, '1', 80, '2014-02-01', '2015-02-15', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `$prefix_hotels_room_scheduler`
--

DROP TABLE IF EXISTS `$prefix_hotels_room_scheduler`;
CREATE TABLE IF NOT EXISTS `$prefix_hotels_room_scheduler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Start` timestamp NULL DEFAULT NULL,
  `End` timestamp NULL DEFAULT NULL,
  `StartTimezone` varchar(255) DEFAULT NULL,
  `EndTimezone` varchar(255) DEFAULT NULL,
  `IsAllDay` tinyint(1) DEFAULT NULL,
  `Description` text,
  `RecurrenceID` varchar(255) DEFAULT NULL,
  `RecurrenceRule` text,
  `RecurrenceException` text,
  `book_status` enum('0','1','2','3') DEFAULT '2',
  `booking_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `$prefix_hotels_room_type`
--

DROP TABLE IF EXISTS `$prefix_hotels_room_type`;
CREATE TABLE IF NOT EXISTS `$prefix_hotels_room_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_plan_id` int(11) NOT NULL DEFAULT '0',
  `primary_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room_type_image` text COLLATE utf8_unicode_ci,
  `max_people` int(11) DEFAULT '1',
  `price_setting_type` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2' COMMENT '1-single price, 2-date wise price',
  `basic_price` double DEFAULT '0',
  `descount_price` double DEFAULT '0',
  `total_room_no` int(11) DEFAULT NULL,
  `room_left` int(11) DEFAULT NULL,
  `room_condition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room_desc` tinytext COLLATE utf8_unicode_ci,
  `room_available_information` longtext COLLATE utf8_unicode_ci,
  `entry_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `$prefix_hotels_room_type`
--

INSERT INTO `$prefix_hotels_room_type` (`id`, `room_type`, `price_plan_id`, `primary_image`, `room_type_image`, `max_people`, `price_setting_type`, `basic_price`, `descount_price`, `total_room_no`, `room_left`, `room_condition`, `room_desc`, `room_available_information`, `entry_by`) VALUES
(1, 'One-Bedroom Apartment', 1, '1-big-bear-dining-room.jpg', '1-big-bear-dining-room.jpg', 2, '2', 600, 345, NULL, NULL, 'Condition Applied', 'One-Bedroom Apartment', '2011-10-26;;1;;345,2011-10-27;;1;;345,2011-10-28;;1;;345,2011-10-29;;1;;0,2011-10-30;;1;;0,2011-10-31;;1;;0', 1),
(2, 'Two-Bedroom Apartment', 1, '3-big-bear-fireplace.jpg', '3-big-bear-fireplace.jpg', 4, '2', 800, 478, 2, 0, 'tfrhj jgfjf ff jfgj', 'jgfg jgj gjfgjfg', '2011-10-04;;1;;478,2011-10-05;;1;;478,2011-10-06;;1;;478,2011-10-07;;1;;478,2011-10-08;;1;;478,2011-10-09;;1;;478,2011-10-10;;1;;478,2011-10-11;;1;;478,2011-10-12;;1;;478,2011-10-13;;1;;478,2011-10-14;;1;;478,2011-10-15;;1;;478,2011-10-16;;1;;478,2011-10-17;;1;;478,2011-10-18;;1;;478,2011-10-19;;1;;478,2011-10-20;;1;;478,2011-10-21;;1;;478,2011-10-22;;1;;478,2011-10-23;;1;;478,2011-10-24;;1;;478,2011-10-25;;1;;478,2011-10-26;;1;;478,2011-10-27;;1;;478,2011-10-28;;1;;478,2011-10-29;;1;;478,2011-10-30;;1;;478,2011-10-31;;1;;478', 1),
(3, 'Studio Apartment with Creek View', 3, '3-big-bear-livingroom2.jpg', '3-big-bear-livingroom2.jpg,350_281_400x200-villa1.jpg,hermosa vacation rental.jpg', 4, '2', 200, 155, 10, 1, 'Prices are per room. Not included in room price: 10 % city tax, 10 % service charge.', 'This spacious Suite overlooks Dubai Creek, and features a balcony, a seating/dining area, a full kitchen and a washing machine.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Iron, Suit Press, Work Desk, Ironing Facilities, Sitting Area, Washing Machine, Sof', '2011-10-19;;1;;155,2011-10-20;;1;;155,2011-10-21;;1;;155,2011-10-22;;1;;155,2011-10-23;;1;;155,2011-10-24;;1;;155,2011-10-25;;1;;155,2011-10-26;;1;;155,2011-10-27;;1;;155,2011-10-28;;1;;155,2011-10-29;;1;;155,2011-10-30;;1;;155,2011-10-31;;1;;155,2011-11-11 00:00:00', 1),
(4, 'Executive Two-Bedroom Apartment (5 Adults)', 2, 'vacation-rental-how-to-main_Full.jpg', 'vacation-rental-how-to-main_Full.jpg,kitchen_poipu_vacation_rental.jpg', 5, '2', 1400, 1200, 12, 2, 'Prices are per room. Not included in room price: 20 % city tax, 20 % service charge.', 'This spacious Suite overlooks Dubai Creek, and features a balcony, a seating/dining area, a full kitchen and a washing machine.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Iron, Suit Press, Work Desk, Ironing Facilities, Sitting Area, Washing Machine, Sof', '2011-10-03;;1;;1200,2011-10-04;;1;;1200,2011-10-05;;1;;1200,2011-10-06;;1;;1200,2011-10-07;;1;;1200,2011-10-08;;1;;1200,2011-10-09;;1;;1200,2011-10-10;;1;;1200,2011-10-11;;1;;1200,2011-10-12;;1;;1200,2011-10-13;;1;;1200,2011-10-14;;1;;1200,2011-10-15;;1;;1200,2011-10-16;;1;;1200,2011-10-17;;1;;1200,2011-10-18;;1;;1200,2011-10-19;;1;;1200,2011-10-20;;1;;1200,2011-10-21;;1;;1200,2011-10-22;;1;;1200,2011-10-23;;1;;1200,2011-10-24;;1;;1200,2011-10-25;;1;;1200,2011-10-26;;1;;1200,2011-10-27;;1;;1200,2011-10-28;;1;;1200,2011-10-29;;1;;1200,2011-10-30;;1;;1200,2011-10-31;;1;;1200', 1),
(5, 'Double or Twin Room', 2, '1.jpg', '1.jpg', 2, '2', 1350, 1205, 2, 1, 'Prices are per room for 3 nights. Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night. Non refundable . Breakfast included', 'No extra beds are available in this room.\r\n\r\nPlease specify preferred bed type when booking.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Work Desk, Heating, Shower, Bathtub, Hairdryer, Bathrobe, Bathroom Amenities, Toilet, Bathroom, Slippers, Pay-Per-View', NULL, 1),
(6, 'Triple Room', 2, '2.jpg', '2.jpg', 7, '2', 2700, 2160, 2, 1, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night.', 'This large suite has a separate living room, a marble bathroom with jacuzzi, and offers free Wi-Fi. \r\n\r\nPlease specify preferred bed type when booking.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Hot Tub, Work Desk, Sitting Area, Heating, Shower, Bathtub,', '', 1),
(7, 'Superior Double (with breakfast)', 3, '3.jpg', '3.jpg', 2, '2', 1560, 1269, 2, 1, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night.Non refundable', 'This large room has views of the internal courtyard or across Rome. \r\n\r\nPlease specify preferred bed type when booking.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Work Desk, Heating, Shower, Hairdryer, Bathrobe, Bathroom Amenities, Toilet, Bathroom, Slip', '', 1),
(8, 'Junior Suites', 3, 'Chaika Park.jpg', 'Chaika Park.jpg', 2, '2', 50, 30, 5, 3, 'payment at reception', 'large rooms', '2011-10-26;;1;;30,2011-10-27;;1;;20,2011-10-28;;1;;125,2011-10-30;;1;;125,2011-10-31;;2;;125', 1),
(9, 'Classic Double or Twin Room', 3, '10.jpg', '10.jpg', 2, '2', 2300, 1120, 2, 2, 'Prices are per room.  Not included in room price: 10 % city tax, 10 % service charge. FREE cancellation', 'Room Facilities: Safe, Air Conditioning, Work Desk, Ironing Facilities, Sofa, Alarm Clock, Shower, Bathtub, Hairdryer, Bathrobe, Bathroom Amenities, Toilet, Bathroom, Slippers, Bathtub or Shower, TV, Telephone, Radio, Satellite TV, Cable TV, Laptop Safe ,', NULL, 1),
(10, 'Interconnecting Classic Room Prices are per room.', 4, '11.jpg', '11.jpg', 4, '2', 2300, 1120, 3, 1, 'Prices are per room.  Not included in room price: 10 % city tax, 10 % service charge. FREE cancellation', 'The rate is for 2 Classic Rooms, one with a king size bed and the other with twin beds.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Work Desk, Interconnecting Room(s) available, Sofa, Alarm Clock, Shower, Bathtub, Hairdryer, Bathrobe, Bathroom Amenities, ', NULL, 1),
(11, 'One-Bedroom Suite', 5, '13.jpg', '13.jpg', 1, '2', 350, 175, 2, 1, 'Prices are per room.  Not included in room price: 10 % city tax, 10 % service charge.FREE cancellation', 'Features ceiling-to-floor windows offering panoramic city views.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Work Desk, Ironing Facilities, Sitting Area, Sofa, Alarm Clock, Shower, Bathtub, Hairdryer, Bathrobe, Bathroom Amenities, Toilet, Bathroom, Slippe', '', 1),
(12, 'Deluxe Room', 5, '14.jpg', '14.jpg', 2, '2', 1260, 680, 2, 1, 'Prices are per room.  Not included in room price: 10 % city tax, 10 % service charge.FREE cancellation', 'This corner room provides panoramic views of Sheikh Zayed Road or the Arabian Sea.\r\n\r\nRoom Facilities: Safe, Air Conditioning, Work Desk, Ironing Facilities, Sofa, Alarm Clock, Shower, Bathtub, Hairdryer, Bathrobe, Bathroom Amenities, Toilet, Bathroom, Sl', '', 1),
(13, 'Double Deluxe Room', 6, '009.jpg', '006.jpg,005.jpg,001.jpg,009.jpg,001.jpg', 2, '2', 1900, 1000, 4, 2, 'Pet is not allow. Free booking.', 'Deluxe rooms are a haven for the weary traveller. Wrap yourself in a waffle bathrobe, flick on the TV and make yourself a hot drink, or order something special from room service. Snuggly duvets with hand made mattresses together with non allergenic pillow', '', 1),
(14, 'Royal Platinum Suite', 6, '0025.jpg', '0010.jpg,0025.jpg,0024.jpg,005.jpg', 3, '2', 890, 400, 3, 1, 'No smoking.', 'Even K West Hotel\\''s standard single rooms are spacious. Featuring a Queen bed for one, with a snuggly duvet and hand made mattress together with non allergenic pillows and luxurious bed linen, you\\''ll get one of the best night\\''s sleep you can have anywh', '', 1),
(15, 'Standard Room', 7, 'p4.jpg', 'p4.jpg,p3.jpg,p6.jpg', 3, '2', 700, 350, 3, 1, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night.FREE cancellation', 'The room offers one double bed with all modern amenities.', '', 1),
(16, 'One-bedroom Executive', 7, 'p16.jpg', 'p16.jpg,p15.jpg,p17.jpg', 2, '2', 1700, 1200, 5, 2, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night.Pet is not allowed', 'The apartment offers one king bed. It includes alarm clock radio, bathroom with towels & toiletries, bedroom with linen, broadband Internet access, fully-equipped kitchen with cooker hob & hood, glassware, crockery, cutlery & utensils, microwave oven, ref', NULL, 1),
(17, 'Studio Premier', 8, 'p3.jpg', 'p6.jpg,p3.jpg,p15.jpg', 4, '2', 2000, 1300, NULL, NULL, 'FREE cancellation . Breakfast included. No Smoking in the room', 'The apartment offers one king or two super single beds. It includes alarm clock radio, bathroom with towels & toiletries, bedroom with linen, broadband Internet access, fully-equipped kitchen with cooker hob & hood, glassware, crockery, cutlery & utensils', '', 1),
(18, 'Executive Suite', 8, 'p25.jpg', 'p25.jpg,p24.jpg,p16.jpg', 2, '2', 1500, 1000, 4, 2, 'Pet is not allowed. No smoking in the room.Free cancellation', 'A standard room with one double bed or 1 queen bed. Color televisions with remote control, pay-per-view movies. Rollway bed is availabe at $20.00 charge.', '', 1),
(19, 'Extra Bed / Child', 9, 'thumb-concierge-80x80.jpg', 'thumb-concierge-80x80.jpg,thumb-luxury-80x80.jpg,thumb-dolphin-80x80.jpg,thumb-atrium-80x80.jpg', 2, '2', 45, 5, 10, 5, 'Valid from 01 Dec 12  - Peak period surcharge will be applied.', 'To learn more about our Assisted by Consultants booking option for Park View Hotel, and its terms and conditions, please Click Here.', NULL, 1),
(20, 'Presidential Suite', 9, 'thumb-luxury-80x80.jpg', 'thumb-luxury-80x80.jpg,1.jpg,006.jpg,thumb-dolphin-80x80.jpg', 6, '2', 2200, 1800, 8, 5, 'Cancellation and prepayment policies vary by room type. Please enter the dates of your stay and check the conditions of your required room.', 'The air-conditioned rooms at Thomson Value Hotel come with modern d�cor. Room amenities include a telephone and tea/coffee maker. En suite bathrooms are fitted with hot shower facilities and toiletries.\r\n\r\nGuests can laze by the pool or get their daily', '', 1),
(21, 'Family Room & Twin / Large Superior', 10, 'room1.jpg', 'room1.jpg,room4.jpg,room2.jpg,room3.jpg', 8, '2', 2500, 1800, 7, 4, 'Room rate is applied to per room per night basis - Check in time is 14:00 and check out time is 12:00', 'The Standard and Superior Double Rooms are 18 - 20sq. m (194 - 215 sq. ft) and are located on the lower floors of the hotel. Room features and amenities include:\r\n\r\n \r\n�High speed wireless broadband Internet access with full VPN support\r\n\r\n�Mo', NULL, 1),
(22, 'Garden View Room', 10, 'a5.jpg', 'a5.jpg,a4.jpg,a6.jpg', 3, '2', 1200, 800, 5, 2, 'Pet is not allow.Free cancellation .', 'natural scenic garden view, teak wood floor, balcony, separate shower, king/twin, teak wood floor, private balcony, air-conditioning unit, IDD telephone, satellite TV, coffee/tea making facility, hair dryer, mini bar, refrigerator, personal safety deposit', NULL, 1),
(23, 'Ocean View Room', 11, '11.jpg', 'a6.jpg,11.jpg,room2.jpg', 3, '2', 1100, 750, 4, 1, 'Smoking is not allow.', 'spectacular ocean view, teak wood floor, balcony, separate shower, king/twin, teak wood floor, private balcony, air-conditioning unit, IDD telephone, satellite TV, coffee/tea making facility, hair dryer, mini bar, refrigerator, personal safety deposit box', NULL, 1),
(24, 'Classic Double Room', 11, 'a29.jpg', 'a29.jpg,a27.jpg,a25.jpg', 2, '2', 900, 550, 4, 2, 'No smoking . Guest  is allow', 'Classic guestroom with 1 double bed or 2 twin beds. Modern luxury, stylish design, fully air-conditioned, free high-speed Internet access via LAN, in-room safe, minibar, hairdryer, vanity mirror, satellite TV, radio, ISDN phone.', NULL, 1),
(25, 'Classic Single Room', 4, 'a7.jpg', 'a7.jpg,a4.jpg', 1, '2', 800, 450, NULL, NULL, 'Free cancellation and 10% discount for summer vacation', 'Guestroom with 1 single bed. Modern luxury, stylish design, fully air-conditioned, complimentary high-speed Internet access via LAN, in-room safe, minibar, hairdryer, vanity mirror, satellite TV, radio, ISDN phone.', '', 1),
(26, 'Superior City View', 12, 'a49.jpg', 'a63.jpg,a49.jpg,a45.jpg,a44.jpg', 4, '2', 900, 500, 6, 2, 'Pet is not allow . Smoking is allowed out side of the room', 'This room offers two single beds or one double bed.The rooms are bright and comfortably furnished, and feature complementary toiletries, air conditioning/heating, bathrobes, in-room safe, iron & ironing board, fully-stocked mini bar, satellite TV, high-sp', NULL, 1),
(27, 'Superior Park View', 12, 'a49.jpg', 'a49.jpg,a48.jpg,a38.jpg,a14.jpg', 3, '2', 700, 450, 6, 3, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night. Non refundable .', 'This room offers two single beds or one double bed. They all feature complementary toiletries, air conditioning/heating, bathrobes, in-room safe, iron & ironing board, fully-stocked mini bar, satellite TV, high-speed internet access, direct-dial telephone', '', 1),
(28, 'Single', 12, 'p25.jpg', '1.jpg,p25.jpg,thumb-dolphin-80x80.jpg', 2, '2', 400, 250, 4, 2, 'Prices are per room for 3 nights.  Included in room price: 10 % VAT. Not included in room price: EUR 3 city tax per person per night.', 'Room offers 1 Large bed with ensuite-bathroom (bath or shower), with all amenities, including direct-dial phone, hairdryer, mini-bar,satellite TV, radio and safe.', NULL, 1),
(29, 'Double', 12, 'a68.jpg', 'a68.jpg,a66.jpg,a47.jpg', 3, '2', 300, 150, 3, 1, 'No Smoking. FREE cancellation . Breakfast included', 'Room offers 1 Large bed or 1 twin with ensuite-bathroom (bath or shower), with all amenities, including direct-dial phone, hairdryer, mini-bar,satellite TV, radio and safe.', NULL, 1),
(30, 'Guest Rooms', 12, '111.jpg', 'room2.jpg,111.jpg,a48.jpg', 1, '2', 500, 220, 3, 1, 'Pet is allow on request and Smoking not allow', 'Guest rooms feature stunning views of the city. Business travelers will appreciate our oversized work desk with ergonomic chair, laptop size in-room safe, cordless telephone and personal voice mail, while all guests of our landmark San Francisco, Californ', NULL, 1),
(31, 'Accessible Rooms', 12, '114.jpg', 'a6.jpg,114.jpg,a7.jpg', 2, '2', 400, 200, 4, 2, 'Free cancellation. Vat include for other purpose', 'All guestrooms and public space are compliant with current ADA laws. Hearing impaired accommodations and TDD services are available.', NULL, 1);
