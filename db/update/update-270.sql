--	Comment
--	Previous Version = 2.4.5
--	Working Version = 2.7.0

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Autos
--

-- --------------------------------------------------------

-- 
-- 	Table structure for table `autos_page`
--

ALTER TABLE `$prefix_autos_page` ENGINE = MYISAM;

ALTER TABLE `$prefix_autos_page` CHANGE `feature_doors` `feature_doors` INT( 11 ) NULL DEFAULT '0'; 

ALTER TABLE `$prefix_autos_page` ADD `feature_at_car_id` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_mileage`;

ALTER TABLE `$prefix_autos_page` ADD `feature_cargo_area` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_at_car_id`;

ALTER TABLE `$prefix_autos_page` ADD `feature_seats` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_cargo_area`;

ALTER TABLE `$prefix_autos_page` ADD `feature_gauges` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_seats`;

ALTER TABLE `$prefix_autos_page` ADD `feature_lighting_elements` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_gauges`;

ALTER TABLE `$prefix_autos_page` ADD `feature_engine_cylinder` INT( 11 ) NULL DEFAULT '0' AFTER `feature_interior_color`;

ALTER TABLE `$prefix_autos_page` ADD `feature_body_type` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_lighting_elements`;

ALTER TABLE `$prefix_autos_page` ADD `feature_kilometers` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_mileage`;

ALTER TABLE `$prefix_autos_page` ADD `feature_vin` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_at_car_id`;

ALTER TABLE `$prefix_autos_page` ADD `feature_wheelbase` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_transmission` , ADD `feature_drivetrain` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `feature_wheelbase`;

ALTER TABLE `$prefix_autos_page` ADD `body_exterior` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `brochure_desc`;

ALTER TABLE `$prefix_autos_page` ADD `body_bonnet` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_exterior`;

ALTER TABLE `$prefix_autos_page` ADD `body_bumper` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_bonnet`;

ALTER TABLE `$prefix_autos_page` ADD `body_doors` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_bumper`;

ALTER TABLE `$prefix_autos_page` ADD `body_windows` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_doors`;

ALTER TABLE `$prefix_autos_page` ADD `body_radiator` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_windows`;

ALTER TABLE `$prefix_autos_page` ADD `body_roof_rack` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_radiator`;

ALTER TABLE `$prefix_autos_page` ADD `body_spoiler` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_roof_rack`;

ALTER TABLE `$prefix_autos_page` ADD `body_carpet` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_spoiler`;

ALTER TABLE `$prefix_autos_page` ADD `body_center_console` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_carpet`;

ALTER TABLE `$prefix_autos_page` ADD `body_seats` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_center_console`;

ALTER TABLE `$prefix_autos_page` ADD `body_speakers` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_seats`; 

ALTER TABLE `$prefix_autos_page` ADD `body_entertainment` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_speakers`;

ALTER TABLE `$prefix_autos_page` ADD `body_convenience` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_entertainment`;

ALTER TABLE `$prefix_autos_page` ADD `body_lighting` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_convenience`;

ALTER TABLE `$prefix_autos_page` ADD `body_instrumentation` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_lighting`;

ALTER TABLE `$prefix_autos_page` ADD `body_safety` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_instrumentation`;

ALTER TABLE `$prefix_autos_page` ADD `body_suspension` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_safety`;

ALTER TABLE `$prefix_autos_page` ADD `body_short_desc` TEXT NULL DEFAULT NULL AFTER `body_suspension`;

ALTER TABLE `$prefix_autos_page` ADD `power_braking_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `body_short_desc`;

ALTER TABLE `$prefix_autos_page` ADD `power_adjusting_mechanism` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_braking_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_engine_cooling_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_adjusting_mechanism`;

ALTER TABLE `$prefix_autos_page` ADD `power_engine_oil_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_engine_cooling_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_engine_components` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_engine_oil_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_engine_parts` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_engine_components`;

ALTER TABLE `$prefix_autos_page` ADD `power_engine_others` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_engine_parts`;

ALTER TABLE `$prefix_autos_page` ADD `power_fuel_supply_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_engine_others`;

ALTER TABLE `$prefix_autos_page` ADD `power_chassis_frame` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_fuel_supply_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_suspension` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_chassis_frame`;

ALTER TABLE `$prefix_autos_page` ADD `power_steering_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_suspension`;

ALTER TABLE `$prefix_autos_page` ADD `power_transmission_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_steering_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_camshaft` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_transmission_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_cylinder` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_camshaft`;

ALTER TABLE `$prefix_autos_page` ADD `power_wheels` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_cylinder`;

ALTER TABLE `$prefix_autos_page` ADD `power_tire_parts` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_wheels`;

ALTER TABLE `$prefix_autos_page` ADD `power_connecting_rod` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_tire_parts`;

ALTER TABLE `$prefix_autos_page` ADD `power_distributor` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_connecting_rod`;

ALTER TABLE `$prefix_autos_page` ADD `power_harmonic_balancer` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_distributor`;

ALTER TABLE `$prefix_autos_page` ADD `power_piston` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_harmonic_balancer`;

ALTER TABLE `$prefix_autos_page` ADD `power_rocker_details` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_piston`;

ALTER TABLE `$prefix_autos_page` ADD `power_exhaust_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_rocker_details`;

ALTER TABLE `$prefix_autos_page` ADD `power_gear` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_exhaust_system`;

ALTER TABLE `$prefix_autos_page` ADD `power_clutch_assembly` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_gear`;

ALTER TABLE `$prefix_autos_page` ADD `power_train_desc` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_clutch_assembly`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_audio_video_device` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `power_train_desc`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_charging_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `elelectrical_audio_video_device`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_window_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `elelectrical_charging_system`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_alternator` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `elelectrical_window_system`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_supply_system` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `elelectrical_alternator`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_battery` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `elelectrical_supply_system`;

ALTER TABLE `$prefix_autos_page` CHANGE `elelectrical_battery` `elelectrical_battery` TINYTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_gauges_meters` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_battery`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_ignition_system` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_gauges_meters`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_signaling_system` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_ignition_system`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_sensors` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_signaling_system`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_starting_system` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_sensors`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_switches` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_starting_system`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_wiring_harnesses` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_switches`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_engine_control_unit` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_wiring_harnesses`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_internet_facilities` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_engine_control_unit`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_temperature_control` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_internet_facilities`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_system` TINYTEXT NULL DEFAULT NULL AFTER `elelectrical_temperature_control`;

ALTER TABLE `$prefix_autos_page` ADD `elelectrical_short_desc` TEXT NULL DEFAULT NULL AFTER `elelectrical_system`;

ALTER TABLE `$prefix_autos_page` ADD `misc_ac_system` TINYTEXT NULL DEFAULT NULL AFTER `meter_energy_expire`;

ALTER TABLE `$prefix_autos_page` ADD `misc_bearings` TINYTEXT NULL DEFAULT NULL AFTER `misc_ac_system`;

ALTER TABLE `$prefix_autos_page` ADD `misc_hoses` TINYTEXT NULL DEFAULT NULL AFTER `misc_bearings`;

ALTER TABLE `$prefix_autos_page` ADD `misc_wiper_system` TINYTEXT NULL DEFAULT NULL AFTER `misc_hoses`;

ALTER TABLE `$prefix_autos_page` ADD `misc_other_parts` TINYTEXT NULL DEFAULT NULL AFTER `misc_wiper_system`;

ALTER TABLE `$prefix_autos_page` ADD `misc_lubrication_system` TINYTEXT NULL DEFAULT NULL AFTER `misc_other_parts`;

ALTER TABLE `$prefix_autos_page` ADD `misc_seals` TINYTEXT NULL DEFAULT NULL AFTER `misc_lubrication_system`;

ALTER TABLE `$prefix_autos_page` ADD `misc_safety_security` TINYTEXT NULL DEFAULT NULL AFTER `misc_seals`;

ALTER TABLE `$prefix_autos_page` ADD `misc_disclaimer` TEXT NULL DEFAULT NULL AFTER `misc_safety_security`;

ALTER TABLE `$prefix_autos_page` ADD `payment_terms_policy` TEXT NULL DEFAULT NULL AFTER `payment_desc`;

ALTER TABLE `$prefix_autos_page` ADD `misc_terms_condition` TEXT NULL DEFAULT NULL AFTER `misc_disclaimer`;

ALTER TABLE `$prefix_autos_page` ADD `stars` INT( 11 ) NULL DEFAULT '0' AFTER `autos_order`;

ALTER TABLE `$prefix_autos_page` ADD `equipment_installed` LONGTEXT NULL DEFAULT NULL AFTER `elelectrical_short_desc` , ADD `equipment_options` LONGTEXT NULL DEFAULT NULL AFTER `equipment_installed`;

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Flight
--

-- --------------------------------------------------------

-- 
-- 	Table structure for table `flight_airport`
--

ALTER TABLE `$prefix_flight_airport` ADD UNIQUE (
`iata_code`
);

ALTER TABLE `$prefix_flight_airport` ADD UNIQUE (
`icao_code`
);

-- --------------------------------------------------------

-- 
-- 	Table structure for table `flight_api`
--

ALTER TABLE `$prefix_flight_api` ADD `api_ts_code` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `api_base_uri`;


-- --------------------------------------------------------

-- 
-- 	Table structure for table `flight_flights`
--

ALTER TABLE `$prefix_flight_flights` ADD `flight_code` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `flight_route_title`;

ALTER TABLE `$prefix_flight_flights` ADD `flight_number` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `flight_code`;

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Hotels
--

-- --------------------------------------------------------

-- 
-- 	Table structure for table `hotels_api`
--

DROP TABLE IF EXISTS `$prefix_hotels_api`;

CREATE TABLE `$prefix_hotels_api` (                         
                 `id` int(11) NOT NULL AUTO_INCREMENT,                
                 `api_name` varchar(255) DEFAULT NULL,                
                 `api_username` varchar(255) DEFAULT NULL,            
                 `api_password` varchar(255) DEFAULT NULL,            
                 `api_key` varchar(255) DEFAULT NULL,                 
                 `api_base_uri` varchar(255) DEFAULT NULL,            
                 `api_ts_code` varchar(255) DEFAULT NULL,             
                 PRIMARY KEY (`id`)                                   
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `$prefix_hotels_api` (
				`id` ,
				`api_name` ,
				`api_username` ,
				`api_password` ,
				`api_key` ,
				`api_base_uri` ,
				`api_ts_code`
				)
				VALUES (
				'1', NULL , NULL , NULL , NULL , NULL , NULL
				);

-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Tours
--

-- --------------------------------------------------------

-- 
-- 	Table structure for table `tours_api`
--

DROP TABLE IF EXISTS `$prefix_tours_api`;

CREATE TABLE `$prefix_tours_api` (                         
                 `id` int(11) NOT NULL AUTO_INCREMENT,                
                 `api_name` varchar(255) DEFAULT NULL,                
                 `api_username` varchar(255) DEFAULT NULL,            
                 `api_password` varchar(255) DEFAULT NULL,            
                 `api_key` varchar(255) DEFAULT NULL,                 
                 `api_base_uri` varchar(255) DEFAULT NULL,            
                 `api_ts_code` varchar(255) DEFAULT NULL,             
                 PRIMARY KEY (`id`)                                   
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `$prefix_tours_api` (
				`id` ,
				`api_name` ,
				`api_username` ,
				`api_password` ,
				`api_key` ,
				`api_base_uri` ,
				`api_ts_code`
				)
				VALUES (
				'1', NULL , NULL , NULL , NULL , NULL , NULL
				);
				
-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Vacationrentals
--

-- --------------------------------------------------------

-- 
-- 	Table structure for table `vacationrentals_api`
--

DROP TABLE IF EXISTS `$prefix_vacationrentals_api`;

CREATE TABLE `$prefix_vacationrentals_api` (                         
                 `id` int(11) NOT NULL AUTO_INCREMENT,                
                 `api_name` varchar(255) DEFAULT NULL,                
                 `api_username` varchar(255) DEFAULT NULL,            
                 `api_password` varchar(255) DEFAULT NULL,            
                 `api_key` varchar(255) DEFAULT NULL,                 
                 `api_base_uri` varchar(255) DEFAULT NULL,            
                 `api_ts_code` varchar(255) DEFAULT NULL,             
                 PRIMARY KEY (`id`)                                   
               ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `$prefix_vacationrentals_api` (
				`id` ,
				`api_name` ,
				`api_username` ,
				`api_password` ,
				`api_key` ,
				`api_base_uri` ,
				`api_ts_code`
				)
				VALUES (
				'1', NULL , NULL , NULL , NULL , NULL , NULL
				);