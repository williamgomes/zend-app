--	Comment
--	Previous Version = 2.10.10
--	Working Version = 2.10.15


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Articles
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_articles_preferences`
--

DROP TABLE IF EXISTS `$prefix_articles_preferences`;
CREATE TABLE IF NOT EXISTS `$prefix_articles_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(299) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `$prefix_articles_preferences`
--

INSERT INTO `$prefix_articles_preferences` (`id`, `name`, `value`) VALUES
(1, 'file_type', 'jpg,png,gif'),
(2, 'file_size_max', '0'),
(3, 'file_path_articles_image', 'data/frontImages/articleImages');




-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Costcalculator
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_costcalculator_package`
--

ALTER TABLE  `$prefix_costcalculator_package` ADD  `service_type` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER  `package_name`;

