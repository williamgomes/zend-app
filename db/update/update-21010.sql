--	Comment
--	Previous Version = 2.10.05
--	Working Version = 2.10.10


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: B2b
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_business_type`
--

ALTER TABLE  `$prefix_b2b_business_type` 	ADD  `entry_by` INT NOT NULL DEFAULT  '1',
											ADD  `entry_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_b2b_business_type`
--

ALTER TABLE  `$prefix_b2b_inquiries` ADD  `parent` INT NOT NULL DEFAULT  '0';


-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Hotels
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_hotels_api_settings`
--

INSERT INTO `$prefix_hotels_api_settings` (`id`, `api_id`, `setting`, `value`) VALUES (NULL, '2', 'ApiMode', '0');



-- --------------------------------------------------------
-- --------------------------------------------------------

--
--	Module Name: Costcalculator
--

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_costcalculator_package`
--	

ALTER TABLE  `$prefix_costcalculator_package` 	ADD  `vehicle_image_primary` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
												ADD  `vehicle_image` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
												ADD  `feature_passenger` INT NOT NULL DEFAULT  '0',
												ADD  `feature_suitcase_large` INT NOT NULL DEFAULT  '0',
												ADD  `feature_suitcase_small` INT NOT NULL DEFAULT  '0',
												ADD  `feature_amenities` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
	

ALTER TABLE  `$prefix_costcalculator_package` 	ADD  `price_by_day` DOUBLE NOT NULL DEFAULT  '0' AFTER  `not_available_date` ,
												ADD  `price_by_week` DOUBLE NOT NULL DEFAULT  '0' AFTER  `price_by_day` ,
												ADD  `price_by_month` DOUBLE NOT NULL DEFAULT  '0' AFTER  `price_by_week` ,
												ADD  `price_by_km` DOUBLE NOT NULL DEFAULT  '0' AFTER  `price_by_month`;

ALTER TABLE  `$prefix_costcalculator_package` 	ADD  `car_type` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `package_name`;

-- --------------------------------------------------------

--
-- Dumping data for table `$prefix_costcalculator_settings`
--	

ALTER TABLE  `$prefix_costcalculator_settings` ADD  `file_type` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
												ADD  `file_size_max` INT NOT NULL DEFAULT  '0',
												ADD  `file_path_vehicle_image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT  'data/frontImages/costcalculator/vehicle_image';																														